/* GCC MELT GENERATED FILE warmelt-modes+02.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #2 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f2[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-modes+02.c declarations ****/


/***************************************************
***
    Copyright (C) 2011, 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_modes_INSTALL_MELT_MODE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_modes_RUNFILE_DOCMD (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_modes_RUNDEBUG_DOCMD (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_modes_EVAL_DOCMD (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un *meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_modes_REPL_PROCESSOR (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_modes_REPL_DOCMD (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un *meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_modes_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_modes_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_modes_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_modes_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_modes_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_modes_INCREMENT_MKDOC_COUNTER (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_modes_MAKEDOC_SCANINPUT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_modes_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_modes_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_modes_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_modes_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_modes_MAKEDOC_OUTDEFLOC (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_modes_MAKEDOC_OUTFORMALS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_modes_MAKEDOC_OUTDOC (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_modes_MAKEDOC_OUTCLASSDEF (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_modes_MAKEDOC_GENMACRO (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_modes_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_modes_MAKEDOC_GENPATMACRO (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_modes_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_modes_MAKEDOC_GENCLASS (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_modes_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_modes_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_modes_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_modes_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_modes_MAKEDOC_OUTPRIMITIVEDEF (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_modes_MAKEDOC_GENPRIMITIVE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_modes_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_modes_MAKEDOC_OUTFUNCTIONDEF (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_modes_MAKEDOC_GENFUNCTION (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_modes_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_modes_MAKEDOC_GENCITERATOR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_modes_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_modes_MAKEDOC_GENCMATCHER (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_modes_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_modes_MAKEDOC_GENOUTPUT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_modes_MAKEDOC_DOCMD (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_modes_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_modes_SHOWVAR_DOCMD (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_modes_HELP_DOCMD (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_modes_NOP_DOCMD (meltclosure_ptr_t meltclosp_,
				     melt_ptr_t meltfirstargp_,
				     const melt_argdescr_cell_t
				     meltxargdescr_[],
				     union meltparam_un *meltxargtab_,
				     const melt_argdescr_cell_t
				     meltxresdescr_[],
				     union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_modes_GENERATE_RUNTYPESUPPORT_ENUM_OBJMAGIC
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_modes_GENERATE_RUNTYPESUPPORT_GTY (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_modes_GENERATE_RUNTYPESUPPORT_FORWCOPY_FUN
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_modes_GENERATE_RUNTYPESUPPORT_CLONING_FUN
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_modes_GENERATE_RUNTYPESUPPORT_SCANNING (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_modes_GENERATE_RUNTYPESUPPORT_PARAM (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_modes_GENERATE_RUNTYPESUPPORT_COD2CTYPE (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAG2STR (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_modes_GENERATE_RUNTYPESUPPORT_BOXINGFUN (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAPFUN (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_modes_GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_modes_RUNTYPESUPPORT_DOCMD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_modes_TRANSLATE_TO_FLAVORED_MODULE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_modes_TRANSLATETOMODULE_DOCMD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_modes_TRANSLATEDEBUG_DOCMD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_modes_TRANSLATEQUICKLY_DOCMD (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_modes_TRANSLATEOPTIMIZED_DOCMD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_modes_TRANSLATEFILE_DOCMD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_modes_TRANSLATEINIT_DOCMD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_modes_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_modes_TRANSLATEINIT_FLAVORED (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_modes_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_modes_TRANSLATEINITMODULE_DOCMD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_modes_TRANSLATEINITMODULEQUICKLYBUILT_DOCMD
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_modes_TRANSLATEINITMODULEDEBUGNOLINE_DOCMD
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_modes_TRANSLATEINITMODULEOPTIMIZED_DOCMD
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_modes_REGISTER_INPUT_CHANNEL_HANDLER (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_modes_HANDLE_ALARM_SIGNAL (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_modes_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_modes_REGISTER_ALARM_TIMER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_modes_UNREGISTER_ALARM_TIMER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_modes_CHILD_PROCESS_SIGCHLD_HANDLER (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_modes_REGISTER_CHILD_PROCESS_HANDLER (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_modes_UNREGISTER_CHILD_PROCESS_HANDLER (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_modes__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_modes__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_modes__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_modes__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_0 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_1 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_2 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_3 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_4 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_5 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_6 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_7 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_8 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_modes__initialmeltchunk_9 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_modes__initialmeltchunk_10 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_modes__initialmeltchunk_11 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_modes__forward_or_mark_module_start_frame (struct
							    melt_callframe_st
							    *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_modes__forward_or_mark_module_start_frame



/**** warmelt-modes+02.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_modes_GENERATE_RUNTYPESUPPORT_CLONING_FUN
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un *meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_50_warmelt_modes_GENERATE_RUNTYPESUPPORT_CLONING_FUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_50_warmelt_modes_GENERATE_RUNTYPESUPPORT_CLONING_FUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 59
    melt_ptr_t mcfr_varptr[59];
#define MELTFRAM_NBVARNUM 19
    long mcfr_varnum[19];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_50_warmelt_modes_GENERATE_RUNTYPESUPPORT_CLONING_FUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_50_warmelt_modes_GENERATE_RUNTYPESUPPORT_CLONING_FUN_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 59; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_50_warmelt_modes_GENERATE_RUNTYPESUPPORT_CLONING_FUN nbval 59*/
  meltfram__.mcfr_nbvar = 59 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GENERATE_RUNTYPESUPPORT_CLONING_FUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:2309:/ getarg");
 /*_.CTYGTYTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VALDESCTUP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTNAME__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTNAME__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTBUF__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2313:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:2313:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2313:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2313;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_cloning_fun start outname=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTNAME__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2313:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:2313:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2313:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2314:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2]))
	 == MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2314:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2314:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check valdesctup"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2314) ? (2314) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2314:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2315:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CTYGTYTUP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2315:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2315:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctygtytup"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2315) ? (2315) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2315:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2316:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-modes.melt:2316:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2316:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check outbuf"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2316) ? (2316) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2316:/ clear");
	     /*clear *//*_#IS_STRBUF__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2317:/ locexp");
      if (65000 > 0)
	meltgc_strbuf_reserve ((melt_ptr_t) /*_.OUTBUF__V5*/ meltfptr[4],
			       (unsigned) 65000);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2319:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2320:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2321:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("/** generated by generate_runtypesupport_cloning_fun **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2322:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:2324:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " /* generated cloning routine head */\
\nmelt_ptr_t\nmeltgc_clone_with_discriminant (melt_ptr_t srcval_p,\
 melt_ptr_t newdiscr_p)\n{\n  unsigned srcmagic = 0;\
\n  unsigned newmagic = 0;\n  MELT_ENTERFRAME (5, NULL);\
\n#define resv       meltfram__.mcfr_varptr[0]\
\n#define srcvalv    meltfram__.mcfr_varptr[1]\
\n#define newdiscrv  meltfram__.mcfr_varptr[2]\
\n#define srcdiscrv  meltfram__.mcfr_varptr[3]\
\n#define compv      meltfram__.mcfr_varptr[4]\
\n  srcvalv = srcval_p;\n  newdiscrv = newdiscr_p;\
\n  resv = srcvalv;\n  if (!srcvalv) \
\n     goto end;\n  srcdiscrv = ((melt_ptr_t)srcvalv)->u_discr;\
\n  if (!newdiscrv)\n    newdiscrv = srcdiscrv;\
\n  if (melt_magic_discr((melt_ptr_t)newdiscrv) != MELTOBMAG_OBJECT\
\
\n      || ((meltobject_ptr_t)newdiscrv)->obj_len < MELTLENGTH_CLASS_DISCRIMI\
NANT)\n    goto end;\n  if (!melt_is_instance_of((melt_ptr_t)newdiscrv\
, \n\t\t\t   MELT_PREDEF (CLASS_DISCRIMINANT)))\
\n    goto end;\n  srcmagic = melt_magic_discr ((melt_ptr_t)srcvalv\
);\n  newmagic = ((meltobject_ptr_t)newdiscrv)->meltobj_magic;\
\n  if (srcmagic != newmagic) \n    goto end;\
\n  switch (srcmagic) { /* end cloning heeade\
r */\n";
      /*_.ADD2OUT__V16*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2357:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:2360:/ quasiblock");


 /*_#NBCTYGTYTUP__L6*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.CTYGTYTUP__V2*/ meltfptr[1])));;

    {
      MELT_LOCATION ("warmelt-modes.melt:2362:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:2363:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/*** cloning ";
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#NBCTYGTYTUP__L6*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " GTY-ed ctypes ***/";
      /*_.ADD2OUT__V17*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2364:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CTYGTYTUP__V2*/ meltfptr[1]);
      for ( /*_#TIX__L7*/ meltfnum[1] = 0;
	   ( /*_#TIX__L7*/ meltfnum[1] >= 0)
	   && ( /*_#TIX__L7*/ meltfnum[1] < meltcit1__EACHTUP_ln);
	/*_#TIX__L7*/ meltfnum[1]++)
	{
	  /*_.CURCTYP__V18*/ meltfptr[17] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.CTYGTYTUP__V2*/ meltfptr[1]),
			       /*_#TIX__L7*/ meltfnum[1]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2369:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L8*/ meltfnum[7] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2369:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:2369:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2369;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "runtypesupport_cloning curctyp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCTYP__V18*/ meltfptr[17];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " tix=";
		    /*^apply.arg */
		    argtab[6].meltbp_long = /*_#TIX__L7*/ meltfnum[1];
		    /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V20*/ meltfptr[19] =
		    /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:2369:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V20*/ meltfptr[19] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:2369:/ quasiblock");


	    /*_.PROGN___V22*/ meltfptr[20] = /*_.IF___V20*/ meltfptr[19];;
	    /*^compute */
	    /*_.IFCPP___V19*/ meltfptr[18] = /*_.PROGN___V22*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2369:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V20*/ meltfptr[19] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V22*/ meltfptr[20] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2370:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L10*/ meltfnum[8] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURCTYP__V18*/ meltfptr[17]),
				   (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */
						  meltfrout->tabval[2])));;
	    MELT_LOCATION ("warmelt-modes.melt:2370:/ cond");
	    /*cond */ if ( /*_#IS_A__L10*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V24*/ meltfptr[20] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:2370:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curctygty"),
					("warmelt-modes.melt")
					? ("warmelt-modes.melt") : __FILE__,
					(2370) ? (2370) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V24*/ meltfptr[20] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V23*/ meltfptr[19] = /*_.IFELSE___V24*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2370:/ clear");
	      /*clear *//*_#IS_A__L10*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V24*/ meltfptr[20] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V23*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2371:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V18*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V18*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.CTYPNAME__V26*/ meltfptr[25] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPNAME__V26*/ meltfptr[25] = NULL;;
	    }
	  ;
	  /*^compute */
  /*_#TIXSUCC__L11*/ meltfnum[7] =
	    ((1) + ( /*_#TIX__L7*/ meltfnum[1]));;
	  MELT_LOCATION ("warmelt-modes.melt:2373:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V18*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V18*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "CTYPE_CNAME");
    /*_.TYCNAME__V27*/ meltfptr[26] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.TYCNAME__V27*/ meltfptr[26] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2374:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V18*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V18*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 12, "CTYPG_MAPMAGIC");
    /*_.MAPMAGIC__V28*/ meltfptr[27] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.MAPMAGIC__V28*/ meltfptr[27] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2375:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V18*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V18*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 11, "CTYPG_BOXEDMAGIC");
    /*_.BOXMAGIC__V29*/ meltfptr[28] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.BOXMAGIC__V29*/ meltfptr[28] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2376:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V18*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V18*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 13, "CTYPG_BOXEDSTRUCT");
    /*_.BOXSTRUCT__V30*/ meltfptr[29] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.BOXSTRUCT__V30*/ meltfptr[29] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2377:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V18*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V18*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 16, "CTYPG_MAPSTRUCT");
    /*_.MAPSTRUCT__V31*/ meltfptr[30] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.MAPSTRUCT__V31*/ meltfptr[30] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2378:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V18*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V18*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 15, "CTYPG_ENTRYSTRUCT");
    /*_.ENTRYSTRUCT__V32*/ meltfptr[31] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.ENTRYSTRUCT__V32*/ meltfptr[31] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2380:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2381:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[5];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "/*cloning gtyctype #";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#TIXSUCC__L11*/ meltfnum[7];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " ";
	    /*^apply.arg */
	    argtab[3].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CTYPNAME__V26*/ meltfptr[25];
	    /*^apply.arg */
	    argtab[4].meltbp_cstring = " */";
	    /*_.ADD2OUT__V33*/ meltfptr[32] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[1])),
			  (melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2382:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2383:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[15];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "case ";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXMAGIC__V29*/ meltfptr[28];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " : { /* cloning boxed value ";
	    /*^apply.arg */
	    argtab[3].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CTYPNAME__V26*/ meltfptr[25];
	    /*^apply.arg */
	    argtab[4].meltbp_cstring = " */\n\t  struct ";
	    /*^apply.arg */
	    argtab[5].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXSTRUCT__V30*/ meltfptr[29];
	    /*^apply.arg */
	    argtab[6].meltbp_cstring = " *src = (struct ";
	    /*^apply.arg */
	    argtab[7].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXSTRUCT__V30*/ meltfptr[29];
	    /*^apply.arg */
	    argtab[8].meltbp_cstring = " *) srcvalv;\n\t  struct ";
	    /*^apply.arg */
	    argtab[9].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXSTRUCT__V30*/ meltfptr[29];
	    /*^apply.arg */
	    argtab[10].meltbp_cstring = " *dst = (struct ";
	    /*^apply.arg */
	    argtab[11].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXSTRUCT__V30*/ meltfptr[29];
	    /*^apply.arg */
	    argtab[12].meltbp_cstring = " *) meltgc_allocate (sizeof(struct ";
	    /*^apply.arg */
	    argtab[13].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXSTRUCT__V30*/ meltfptr[29];
	    /*^apply.arg */
	    argtab[14].meltbp_cstring = "), 0);";
	    /*_.ADD2OUT__V34*/ meltfptr[33] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[1])),
			  (melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2387:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2388:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring =
	      "\n            *dst = *src;\n\t    dst->discr = (meltobject_ptr_t) newdiscrv\
; \n\t    resv = (melt_ptr_t) dst;\
\n          }\n\t  break;";
	    /*_.ADD2OUT__V35*/ meltfptr[34] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[1])),
			  (melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2395:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2396:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.MAPSTRUCT__V31*/ meltfptr[30])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.ENTRYSTRUCT__V32*/ meltfptr[31])	/*then */
		  {
		    /*^cond.then */
		    /*_.IF___V37*/ meltfptr[36] =
		      /*_.MAPMAGIC__V28*/ meltfptr[27];;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-modes.melt:2396:/ cond.else");

     /*_.IF___V37*/ meltfptr[36] = NULL;;
		  }
		;
		/*^compute */
		/*_.IF___V36*/ meltfptr[35] = /*_.IF___V37*/ meltfptr[36];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:2396:/ clear");
	      /*clear *//*_.IF___V37*/ meltfptr[36] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V36*/ meltfptr[35] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2396:/ cond");
	  /*cond */ if ( /*_.IF___V36*/ meltfptr[35])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:2397:/ quasiblock");


		MELT_LOCATION ("warmelt-modes.melt:2399:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[17];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "case ";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.MAPMAGIC__V28*/ meltfptr[27];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = " : { /* cloning map value ";
		  /*^apply.arg */
		  argtab[3].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CTYPNAME__V26*/ meltfptr[25];
		  /*^apply.arg */
		  argtab[4].meltbp_cstring = " */\n\t  struct ";
		  /*^apply.arg */
		  argtab[5].meltbp_aptr =
		    (melt_ptr_t *) & /*_.MAPSTRUCT__V31*/ meltfptr[30];
		  /*^apply.arg */
		  argtab[6].meltbp_cstring = " *src = (struct ";
		  /*^apply.arg */
		  argtab[7].meltbp_aptr =
		    (melt_ptr_t *) & /*_.MAPSTRUCT__V31*/ meltfptr[30];
		  /*^apply.arg */
		  argtab[8].meltbp_cstring =
		    " *) srcvalv;\n\t  unsigned oldlen =  melt_primtab[src->lenix];\
\n\t  unsigned newlen = 4*src->count/3 + 5;\
\n\t  struct ";
		  /*^apply.arg */
		  argtab[9].meltbp_aptr =
		    (melt_ptr_t *) & /*_.MAPSTRUCT__V31*/ meltfptr[30];
		  /*^apply.arg */
		  argtab[10].meltbp_cstring =
		    " *dst = \n            (struct ";
		  /*^apply.arg */
		  argtab[11].meltbp_aptr =
		    (melt_ptr_t *) & /*_.MAPSTRUCT__V31*/ meltfptr[30];
		  /*^apply.arg */
		  argtab[12].meltbp_cstring =
		    " *) meltgc_raw_new_mappointers((meltobject_ptr_t)newdiscrv, newlen\
);\n\t  unsigned ix = 0;\n\t  dst->meltmap_aux = src->meltmap_aux;\
\
\n\t  if (src->entab)\n            for (ix=0; ix<oldlen; \
\n\t\t ix++) {\n\t       melt_ptr_t curva = src->entab[ix].e_va;\
\n\t \
      ";
		  /*^apply.arg */
		  argtab[13].meltbp_aptr =
		    (melt_ptr_t *) & /*_.TYCNAME__V27*/ meltfptr[26];
		  /*^apply.arg */
		  argtab[14].meltbp_cstring =
		    " curat = src->entab[ix].e_at;\n\t       if (curva != NULL && curat\
 != (";
		  /*^apply.arg */
		  argtab[15].meltbp_aptr =
		    (melt_ptr_t *) & /*_.TYCNAME__V27*/ meltfptr[26];
		  /*^apply.arg */
		  argtab[16].meltbp_cstring =
		    ") HTAB_DELETED_ENTRY)\n\t         meltgc_raw_put_mappointers((void\
*)dst, (const void*)curat, curva);\
\n\t    }\n\t  resv = (melt_ptr_t) dst;\
\n         };\n         break; ";
		  /*_.ADD2OUT__V40*/ meltfptr[39] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[1])),
				(melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.LET___V39*/ meltfptr[38] =
		  /*_.ADD2OUT__V40*/ meltfptr[39];;

		MELT_LOCATION ("warmelt-modes.melt:2397:/ clear");
	      /*clear *//*_.ADD2OUT__V40*/ meltfptr[39] = 0;
		/*_.IF___V38*/ meltfptr[36] = /*_.LET___V39*/ meltfptr[38];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:2396:/ clear");
	      /*clear *//*_.LET___V39*/ meltfptr[38] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V38*/ meltfptr[36] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.LET___V25*/ meltfptr[20] = /*_.IF___V38*/ meltfptr[36];;

	  MELT_LOCATION ("warmelt-modes.melt:2371:/ clear");
	    /*clear *//*_.CTYPNAME__V26*/ meltfptr[25] = 0;
	  /*^clear */
	    /*clear *//*_#TIXSUCC__L11*/ meltfnum[7] = 0;
	  /*^clear */
	    /*clear *//*_.TYCNAME__V27*/ meltfptr[26] = 0;
	  /*^clear */
	    /*clear *//*_.MAPMAGIC__V28*/ meltfptr[27] = 0;
	  /*^clear */
	    /*clear *//*_.BOXMAGIC__V29*/ meltfptr[28] = 0;
	  /*^clear */
	    /*clear *//*_.BOXSTRUCT__V30*/ meltfptr[29] = 0;
	  /*^clear */
	    /*clear *//*_.MAPSTRUCT__V31*/ meltfptr[30] = 0;
	  /*^clear */
	    /*clear *//*_.ENTRYSTRUCT__V32*/ meltfptr[31] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V33*/ meltfptr[32] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V34*/ meltfptr[33] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V35*/ meltfptr[34] = 0;
	  /*^clear */
	    /*clear *//*_.IF___V36*/ meltfptr[35] = 0;
	  /*^clear */
	    /*clear *//*_.IF___V38*/ meltfptr[36] = 0;
	  if ( /*_#TIX__L7*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:2366:/ clear");
	    /*clear *//*_.CURCTYP__V18*/ meltfptr[17] = 0;
      /*^clear */
	    /*clear *//*_#TIX__L7*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V23*/ meltfptr[19] = 0;
      /*^clear */
	    /*clear *//*_.LET___V25*/ meltfptr[20] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-modes.melt:2360:/ clear");
	   /*clear *//*_#NBCTYGTYTUP__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V17*/ meltfptr[16] = 0;
    MELT_LOCATION ("warmelt-modes.melt:2424:/ quasiblock");


 /*_#NBVALDESC__L12*/ meltfnum[8] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2])));;

    {
      MELT_LOCATION ("warmelt-modes.melt:2426:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2427:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:2428:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/******* cloning the ";
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#NBVALDESC__L12*/ meltfnum[8];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " value descriptors *******/";
      /*_.ADD2OUT__V41*/ meltfptr[39] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.VALDESCTUP__V3*/ meltfptr[2]);
      for ( /*_#VALDIX__L13*/ meltfnum[7] = 0;
	   ( /*_#VALDIX__L13*/ meltfnum[7] >= 0)
	   && ( /*_#VALDIX__L13*/ meltfnum[7] < meltcit2__EACHTUP_ln);
	/*_#VALDIX__L13*/ meltfnum[7]++)
	{
	  /*_.CURVALDESC__V42*/ meltfptr[38] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.VALDESCTUP__V3*/ meltfptr[2]),
			       /*_#VALDIX__L13*/ meltfnum[7]);




	  {
	    MELT_LOCATION ("warmelt-modes.melt:2432:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2433:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURVALDESC__V42*/
					       meltfptr[38]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURVALDESC__V42*/ meltfptr[38]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMVALDESC__V44*/ meltfptr[26] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMVALDESC__V44*/ meltfptr[26] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2434:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURVALDESC__V42*/
					       meltfptr[38]),
					      (melt_ptr_t) (( /*!CLASS_VALUE_DESCRIPTOR */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURVALDESC__V42*/ meltfptr[38]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 10, "VALDESC_CLONECHUNK");
    /*_.CLONEVALCHK__V45*/ meltfptr[27] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CLONEVALCHK__V45*/ meltfptr[27] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2435:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURVALDESC__V42*/
					       meltfptr[38]),
					      (melt_ptr_t) (( /*!CLASS_VALUE_DESCRIPTOR */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURVALDESC__V42*/ meltfptr[38]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 3, "VALDESC_STRUCT");
    /*_.VALSTRUCT__V46*/ meltfptr[28] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.VALSTRUCT__V46*/ meltfptr[28] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2436:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURVALDESC__V42*/
					       meltfptr[38]),
					      (melt_ptr_t) (( /*!CLASS_VALUE_DESCRIPTOR */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURVALDESC__V42*/ meltfptr[38]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 2, "VALDESC_OBJMAGIC");
    /*_.VALOBJMAGIC__V47*/ meltfptr[29] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.VALOBJMAGIC__V47*/ meltfptr[29] = NULL;;
	    }
	  ;
	  /*^compute */
  /*_#VALDIXSUCC__L14*/ meltfnum[0] =
	    ((1) + ( /*_#VALDIX__L13*/ meltfnum[7]));;
	  MELT_LOCATION ("warmelt-modes.melt:2439:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[5];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "/** cloning value descriptor #";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#VALDIXSUCC__L14*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " ";
	    /*^apply.arg */
	    argtab[3].meltbp_aptr =
	      (melt_ptr_t *) & /*_.NAMVALDESC__V44*/ meltfptr[26];
	    /*^apply.arg */
	    argtab[4].meltbp_cstring = " **/";
	    /*_.ADD2OUT__V48*/ meltfptr[30] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[1])),
			  (melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2440:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2442:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#__L15*/ meltfnum[14] =
	    (( /*_.CLONEVALCHK__V45*/ meltfptr[27]) ==
	     (( /*!konst_6_TRUE */ meltfrout->tabval[6])));;
	  MELT_LOCATION ("warmelt-modes.melt:2442:/ cond");
	  /*cond */ if ( /*_#__L15*/ meltfnum[14])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:2443:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[15];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = " /*default cloning for ";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.NAMVALDESC__V44*/ meltfptr[26];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = "*/\n\t\t   case ";
		  /*^apply.arg */
		  argtab[3].meltbp_aptr =
		    (melt_ptr_t *) & /*_.VALOBJMAGIC__V47*/ meltfptr[29];
		  /*^apply.arg */
		  argtab[4].meltbp_cstring = ": {\n\t\t     struct ";
		  /*^apply.arg */
		  argtab[5].meltbp_aptr =
		    (melt_ptr_t *) & /*_.VALSTRUCT__V46*/ meltfptr[28];
		  /*^apply.arg */
		  argtab[6].meltbp_cstring = " *src = (struct ";
		  /*^apply.arg */
		  argtab[7].meltbp_aptr =
		    (melt_ptr_t *) & /*_.VALSTRUCT__V46*/ meltfptr[28];
		  /*^apply.arg */
		  argtab[8].meltbp_cstring = "*) srcvalv;\n\t\t     struct ";
		  /*^apply.arg */
		  argtab[9].meltbp_aptr =
		    (melt_ptr_t *) & /*_.VALSTRUCT__V46*/ meltfptr[28];
		  /*^apply.arg */
		  argtab[10].meltbp_cstring =
		    " *dst = \n                        (struct ";
		  /*^apply.arg */
		  argtab[11].meltbp_aptr =
		    (melt_ptr_t *) & /*_.VALSTRUCT__V46*/ meltfptr[28];
		  /*^apply.arg */
		  argtab[12].meltbp_cstring =
		    "*) meltgc_allocate (sizeof(struct ";
		  /*^apply.arg */
		  argtab[13].meltbp_aptr =
		    (melt_ptr_t *) & /*_.VALSTRUCT__V46*/ meltfptr[28];
		  /*^apply.arg */
		  argtab[14].meltbp_cstring =
		    "), 0);\n\t\t     *dst = *src;\n\t\t     dst->discr = (meltobject_ptr_t\
)newdiscrv;\n\t\t     resv = (melt_ptr_t) dst;\
\n\t\t   }\n\t\t \
  break; ";
		  /*_.ADD2OUT__V50*/ meltfptr[32] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[1])),
				(melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V49*/ meltfptr[31] =
		  /*_.ADD2OUT__V50*/ meltfptr[32];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:2442:/ clear");
	      /*clear *//*_.ADD2OUT__V50*/ meltfptr[32] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:2455:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#IS_STRING__L16*/ meltfnum[15] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.CLONEVALCHK__V45*/ meltfptr[27])) ==
		   MELTOBMAG_STRING);;
		MELT_LOCATION ("warmelt-modes.melt:2455:/ cond");
		/*cond */ if ( /*_#IS_STRING__L16*/ meltfnum[15])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-modes.melt:2456:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[15];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_cstring = " /*explicit cloning for ";
			/*^apply.arg */
			argtab[1].meltbp_aptr =
			  (melt_ptr_t *) & /*_.NAMVALDESC__V44*/ meltfptr[26];
			/*^apply.arg */
			argtab[2].meltbp_cstring = "*/\n\t\t   case ";
			/*^apply.arg */
			argtab[3].meltbp_aptr =
			  (melt_ptr_t *) & /*_.VALOBJMAGIC__V47*/
			  meltfptr[29];
			/*^apply.arg */
			argtab[4].meltbp_cstring = ": {\n\t\t     struct ";
			/*^apply.arg */
			argtab[5].meltbp_aptr =
			  (melt_ptr_t *) & /*_.VALSTRUCT__V46*/ meltfptr[28];
			/*^apply.arg */
			argtab[6].meltbp_cstring = " *src = (struct ";
			/*^apply.arg */
			argtab[7].meltbp_aptr =
			  (melt_ptr_t *) & /*_.VALSTRUCT__V46*/ meltfptr[28];
			/*^apply.arg */
			argtab[8].meltbp_cstring =
			  "*) srcvalv;\n\t\t     struct ";
			/*^apply.arg */
			argtab[9].meltbp_aptr =
			  (melt_ptr_t *) & /*_.VALSTRUCT__V46*/ meltfptr[28];
			/*^apply.arg */
			argtab[10].meltbp_cstring =
			  " *dst = NULL;\n\t\t     /* clone chunk for ";
			/*^apply.arg */
			argtab[11].meltbp_aptr =
			  (melt_ptr_t *) & /*_.NAMVALDESC__V44*/ meltfptr[26];
			/*^apply.arg */
			argtab[12].meltbp_cstring = ":*/\n\t\t     ";
			/*^apply.arg */
			argtab[13].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CLONEVALCHK__V45*/
			  meltfptr[27];
			/*^apply.arg */
			argtab[14].meltbp_cstring =
			  ";\n\t\t     if (dst) \n\t\t        resv = (melt_ptr_t) dst;\
\n\t\t   };\n\t\t \
  break;";
			/*_.ADD2OUT__V52*/ meltfptr[34] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!ADD2OUT */ meltfrout->tabval[1])),
				      (melt_ptr_t) ( /*_.OUTBUF__V5*/
						    meltfptr[4]),
				      (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				       MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				       MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				       MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				       MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				       MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				       MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				       MELTBPARSTR_CSTRING ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IFELSE___V51*/ meltfptr[33] =
			/*_.ADD2OUT__V52*/ meltfptr[34];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-modes.melt:2455:/ clear");
		/*clear *//*_.ADD2OUT__V52*/ meltfptr[34] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-modes.melt:2469:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#NULL__L17*/ meltfnum[16] =
			(( /*_.CLONEVALCHK__V45*/ meltfptr[27]) == NULL);;
		      MELT_LOCATION ("warmelt-modes.melt:2469:/ cond");
		      /*cond */ if ( /*_#NULL__L17*/ meltfnum[16])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-modes.melt:2470:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[5];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_cstring = " /*no cloning for ";
			      /*^apply.arg */
			      argtab[1].meltbp_aptr =
				(melt_ptr_t *) & /*_.NAMVALDESC__V44*/
				meltfptr[26];
			      /*^apply.arg */
			      argtab[2].meltbp_cstring = "*/\n\t\t   case ";
			      /*^apply.arg */
			      argtab[3].meltbp_aptr =
				(melt_ptr_t *) & /*_.VALOBJMAGIC__V47*/
				meltfptr[29];
			      /*^apply.arg */
			      argtab[4].meltbp_cstring = ": break;";
			      /*_.ADD2OUT__V54*/ meltfptr[36] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!ADD2OUT */ meltfrout->
					      tabval[1])),
					    (melt_ptr_t) ( /*_.OUTBUF__V5*/
							  meltfptr[4]),
					    (MELTBPARSTR_CSTRING
					     MELTBPARSTR_PTR
					     MELTBPARSTR_CSTRING
					     MELTBPARSTR_PTR
					     MELTBPARSTR_CSTRING ""), argtab,
					    "", (union meltparam_un *) 0);
			    }
			    ;
			    /*_.IFELSE___V53*/ meltfptr[35] =
			      /*_.ADD2OUT__V54*/ meltfptr[36];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-modes.melt:2469:/ clear");
		  /*clear *//*_.ADD2OUT__V54*/ meltfptr[36] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {


#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-modes.melt:2475:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	  /*_#MELT_NEED_DBG__L18*/ meltfnum[17] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-modes.melt:2475:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[17])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	    /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-modes.melt:2475:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[7];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L19*/
					meltfnum[18];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-modes.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 2475;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"invalid cloning in curvaldesc=";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.CURVALDESC__V42*/
					meltfptr[38];
				      /*^apply.arg */
				      argtab[5].meltbp_cstring = " valdix=";
				      /*^apply.arg */
				      argtab[6].meltbp_long =
					/*_#VALDIX__L13*/ meltfnum[7];
				      /*_.MELT_DEBUG_FUN__V57*/ meltfptr[34] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V56*/ meltfptr[32] =
				      /*_.MELT_DEBUG_FUN__V57*/ meltfptr[34];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-modes.melt:2475:/ clear");
		      /*clear *//*_#THE_MELTCALLCOUNT__L19*/
				      meltfnum[18] = 0;
				    /*^clear */
		      /*clear *//*_.MELT_DEBUG_FUN__V57*/
				      meltfptr[34] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	   /*_.IF___V56*/ meltfptr[32] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-modes.melt:2475:/ quasiblock");


			      /*_.PROGN___V58*/ meltfptr[36] =
				/*_.IF___V56*/ meltfptr[32];;
			      /*^compute */
			      /*_.IFCPP___V55*/ meltfptr[16] =
				/*_.PROGN___V58*/ meltfptr[36];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-modes.melt:2475:/ clear");
		    /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[17]
				= 0;
			      /*^clear */
		    /*clear *//*_.IF___V56*/ meltfptr[32] = 0;
			      /*^clear */
		    /*clear *//*_.PROGN___V58*/ meltfptr[36] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V55*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;

			    {
			      MELT_LOCATION
				("warmelt-modes.melt:2476:/ locexp");
			      error ("MELT ERROR MSG [#%ld]::: %s - %s",
				     melt_dbgcounter,
				     ("invalid cloning for "),
				     melt_string_str ((melt_ptr_t)
						      ( /*_.NAMVALDESC__V44*/
						       meltfptr[26])));
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-modes.melt:2474:/ quasiblock");


			    /*epilog */

			    MELT_LOCATION ("warmelt-modes.melt:2469:/ clear");
		  /*clear *//*_.IFCPP___V55*/ meltfptr[16] = 0;
			  }
			  ;
			}
		      ;
		      /*_.IFELSE___V51*/ meltfptr[33] =
			/*_.IFELSE___V53*/ meltfptr[35];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-modes.melt:2455:/ clear");
		/*clear *//*_#NULL__L17*/ meltfnum[16] = 0;
		      /*^clear */
		/*clear *//*_.IFELSE___V53*/ meltfptr[35] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V49*/ meltfptr[31] =
		  /*_.IFELSE___V51*/ meltfptr[33];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:2442:/ clear");
	      /*clear *//*_#IS_STRING__L16*/ meltfnum[15] = 0;
		/*^clear */
	      /*clear *//*_.IFELSE___V51*/ meltfptr[33] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.LET___V43*/ meltfptr[25] = /*_.IFELSE___V49*/ meltfptr[31];;

	  MELT_LOCATION ("warmelt-modes.melt:2433:/ clear");
	    /*clear *//*_.NAMVALDESC__V44*/ meltfptr[26] = 0;
	  /*^clear */
	    /*clear *//*_.CLONEVALCHK__V45*/ meltfptr[27] = 0;
	  /*^clear */
	    /*clear *//*_.VALSTRUCT__V46*/ meltfptr[28] = 0;
	  /*^clear */
	    /*clear *//*_.VALOBJMAGIC__V47*/ meltfptr[29] = 0;
	  /*^clear */
	    /*clear *//*_#VALDIXSUCC__L14*/ meltfnum[0] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V48*/ meltfptr[30] = 0;
	  /*^clear */
	    /*clear *//*_#__L15*/ meltfnum[14] = 0;
	  /*^clear */
	    /*clear *//*_.IFELSE___V49*/ meltfptr[31] = 0;
	  if ( /*_#VALDIX__L13*/ meltfnum[7] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:2429:/ clear");
	    /*clear *//*_.CURVALDESC__V42*/ meltfptr[38] = 0;
      /*^clear */
	    /*clear *//*_#VALDIX__L13*/ meltfnum[7] = 0;
      /*^clear */
	    /*clear *//*_.LET___V43*/ meltfptr[25] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2481:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-modes.melt:2424:/ clear");
	   /*clear *//*_#NBVALDESC__L12*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V41*/ meltfptr[39] = 0;
    MELT_LOCATION ("warmelt-modes.melt:2485:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "\n/* generated cloning routine trailer */\
\n    default: ;\n  } /*end switch srcmagic for cloning */\
\n end:\n  MELT_EXITFRAME();\n  return (melt_ptr_t) resv;\
\n}  /* end of generated meltgc_clone_with_discriminant */\
\n#undef resv\n#undef srcvalv\n#undef newdiscrv\
\n#undef discrv\n#undef compv\n";
      /*_.ADD2OUT__V59*/ meltfptr[34] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2499:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:2309:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V59*/ meltfptr[34] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GENERATE_RUNTYPESUPPORT_CLONING_FUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_50_warmelt_modes_GENERATE_RUNTYPESUPPORT_CLONING_FUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_50_warmelt_modes_GENERATE_RUNTYPESUPPORT_CLONING_FUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_modes_GENERATE_RUNTYPESUPPORT_SCANNING (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_51_warmelt_modes_GENERATE_RUNTYPESUPPORT_SCANNING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_51_warmelt_modes_GENERATE_RUNTYPESUPPORT_SCANNING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 42
    melt_ptr_t mcfr_varptr[42];
#define MELTFRAM_NBVARNUM 14
    long mcfr_varnum[14];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_51_warmelt_modes_GENERATE_RUNTYPESUPPORT_SCANNING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_51_warmelt_modes_GENERATE_RUNTYPESUPPORT_SCANNING_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 42; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_51_warmelt_modes_GENERATE_RUNTYPESUPPORT_SCANNING nbval 42*/
  meltfram__.mcfr_nbvar = 42 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GENERATE_RUNTYPESUPPORT_SCANNING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:2505:/ getarg");
 /*_.CTYGTYTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VALDESCTUP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTNAME__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTNAME__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTBUF__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2506:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:2506:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2506:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2506;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_scanning start outname=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTNAME__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2506:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:2506:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2506:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2508:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CTYGTYTUP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2508:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2508:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctygtytup"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2508) ? (2508) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2508:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2509:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2]))
	 == MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2509:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2509:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check valdesctup"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2509) ? (2509) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2509:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2510:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-modes.melt:2510:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2510:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check outbuf"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2510) ? (2510) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2510:/ clear");
	     /*clear *//*_#IS_STRBUF__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2511:/ locexp");
      if (65000 > 0)
	meltgc_strbuf_reserve ((melt_ptr_t) /*_.OUTBUF__V5*/ meltfptr[4],
			       (unsigned) 65000);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2512:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2513:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2514:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("/** start of code generated by generate_runtypesupport_scanning **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2516:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2518:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   melt_string_str ((melt_ptr_t)
					    (( /*!konst_1 */ meltfrout->
					      tabval[1]))));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2539:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.VALDESCTUP__V3*/ meltfptr[2]);
      for ( /*_#VIX__L6*/ meltfnum[0] = 0;
	   ( /*_#VIX__L6*/ meltfnum[0] >= 0)
	   && ( /*_#VIX__L6*/ meltfnum[0] < meltcit1__EACHTUP_ln);
	/*_#VIX__L6*/ meltfnum[0]++)
	{
	  /*_.CURVALDESC__V16*/ meltfptr[14] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.VALDESCTUP__V3*/ meltfptr[2]),
			       /*_#VIX__L6*/ meltfnum[0]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2543:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L7*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2543:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:2543:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2543;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "generate_runtypesupport_scanning curvaldesc=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURVALDESC__V16*/ meltfptr[14];
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V18*/ meltfptr[17] =
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:2543:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V18*/ meltfptr[17] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:2543:/ quasiblock");


	    /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2543:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[1] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2544:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L9*/ meltfnum[7] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURVALDESC__V16*/ meltfptr[14]),
				   (melt_ptr_t) (( /*!CLASS_VALUE_DESCRIPTOR */ meltfrout->tabval[2])));;
	    MELT_LOCATION ("warmelt-modes.melt:2544:/ cond");
	    /*cond */ if ( /*_#IS_A__L9*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V22*/ meltfptr[18] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:2544:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curvaldesc"),
					("warmelt-modes.melt")
					? ("warmelt-modes.melt") : __FILE__,
					(2544) ? (2544) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V21*/ meltfptr[17] = /*_.IFELSE___V22*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2544:/ clear");
	      /*clear *//*_#IS_A__L9*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2545:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2546:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("/*valdesc #"));
	  }
	  ;
  /*_#I__L10*/ meltfnum[1] =
	    ((1) + ( /*_#VIX__L6*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2547:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.OUTBUF__V5*/ meltfptr[4]),
				   ( /*_#I__L10*/ meltfnum[1]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2548:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (" "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2549:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURVALDESC__V16*/
					       meltfptr[14]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURVALDESC__V16*/ meltfptr[14]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V23*/ meltfptr[18] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V23*/ meltfptr[18] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2549:/ locexp");
	    meltgc_add_strbuf_ccomment ((melt_ptr_t)
					( /*_.OUTBUF__V5*/ meltfptr[4]),
					melt_string_str ((melt_ptr_t)
							 ( /*_.NAMED_NAME__V23*/ meltfptr[18])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2550:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2551:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2552:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("case "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2553:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURVALDESC__V16*/
					       meltfptr[14]),
					      (melt_ptr_t) (( /*!CLASS_VALUE_DESCRIPTOR */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURVALDESC__V16*/ meltfptr[14]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 2, "VALDESC_OBJMAGIC");
    /*_.VALDESC_OBJMAGIC__V24*/ meltfptr[23] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.VALDESC_OBJMAGIC__V24*/ meltfptr[23] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2553:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.VALDESC_OBJMAGIC__V24*/ meltfptr[23])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2554:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (": {"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2555:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURVALDESC__V16*/
					       meltfptr[14]),
					      (melt_ptr_t) (( /*!CLASS_VALUE_DESCRIPTOR */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURVALDESC__V16*/ meltfptr[14]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 9, "VALDESC_FORWCHUNK");
    /*_.FWCHK__V25*/ meltfptr[24] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.FWCHK__V25*/ meltfptr[24] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2557:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.FWCHK__V25*/ meltfptr[24])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2559:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.OUTBUF__V5*/ meltfptr[4]),
					    (2), 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2560:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("struct "));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2561:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURVALDESC__V16*/
						     meltfptr[14]),
						    (melt_ptr_t) (( /*!CLASS_VALUE_DESCRIPTOR */ meltfrout->tabval[2])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURVALDESC__V16*/ meltfptr[14])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 3, "VALDESC_STRUCT");
      /*_.VALDESC_STRUCT__V26*/ meltfptr[25] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.VALDESC_STRUCT__V26*/ meltfptr[25] = NULL;;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2561:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       melt_string_str ((melt_ptr_t)
							( /*_.VALDESC_STRUCT__V26*/ meltfptr[25])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2562:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("*src = (struct "));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2563:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURVALDESC__V16*/
						     meltfptr[14]),
						    (melt_ptr_t) (( /*!CLASS_VALUE_DESCRIPTOR */ meltfrout->tabval[2])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURVALDESC__V16*/ meltfptr[14])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 3, "VALDESC_STRUCT");
      /*_.VALDESC_STRUCT__V27*/ meltfptr[26] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.VALDESC_STRUCT__V27*/ meltfptr[26] = NULL;;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2563:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       melt_string_str ((melt_ptr_t)
							( /*_.VALDESC_STRUCT__V27*/ meltfptr[26])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2564:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("*) p;"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2565:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.OUTBUF__V5*/ meltfptr[4]),
					    (2), 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2566:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       melt_string_str ((melt_ptr_t)
							( /*_.FWCHK__V25*/
							 meltfptr[24])));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2558:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:2557:/ clear");
	      /*clear *//*_.VALDESC_STRUCT__V26*/ meltfptr[25] = 0;
		/*^clear */
	      /*clear *//*_.VALDESC_STRUCT__V27*/ meltfptr[26] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;

	  MELT_LOCATION ("warmelt-modes.melt:2555:/ clear");
	    /*clear *//*_.FWCHK__V25*/ meltfptr[24] = 0;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2568:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2569:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("break; }"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2570:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;
	  if ( /*_#VIX__L6*/ meltfnum[0] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:2540:/ clear");
	    /*clear *//*_.CURVALDESC__V16*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_#VIX__L6*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
      /*^clear */
	    /*clear *//*_#I__L10*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V23*/ meltfptr[18] = 0;
      /*^clear */
	    /*clear *//*_.VALDESC_OBJMAGIC__V24*/ meltfptr[23] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2574:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2575:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("/* GTY-ed ctypes scan forward for melt_scanning  */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2578:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CTYGTYTUP__V2*/ meltfptr[1]);
      for ( /*_#TIX__L11*/ meltfnum[7] = 0;
	   ( /*_#TIX__L11*/ meltfnum[7] >= 0)
	   && ( /*_#TIX__L11*/ meltfnum[7] < meltcit2__EACHTUP_ln);
	/*_#TIX__L11*/ meltfnum[7]++)
	{
	  /*_.CURCTYP__V28*/ meltfptr[25] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.CTYGTYTUP__V2*/ meltfptr[1]),
			       /*_#TIX__L11*/ meltfnum[7]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2582:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2582:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:2582:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2582;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "generate_runtypesupport_scanning curctyp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCTYP__V28*/ meltfptr[25];
		    /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V30*/ meltfptr[24] =
		    /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:2582:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V30*/ meltfptr[24] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:2582:/ quasiblock");


	    /*_.PROGN___V32*/ meltfptr[30] = /*_.IF___V30*/ meltfptr[24];;
	    /*^compute */
	    /*_.IFCPP___V29*/ meltfptr[26] = /*_.PROGN___V32*/ meltfptr[30];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2582:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V30*/ meltfptr[24] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V32*/ meltfptr[30] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V29*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2583:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("/*gtyctype #"));
	  }
	  ;
  /*_#I__L14*/ meltfnum[12] =
	    ((1) + ( /*_#TIX__L11*/ meltfnum[7]));;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2584:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.OUTBUF__V5*/ meltfptr[4]),
				   ( /*_#I__L14*/ meltfnum[12]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2585:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (" "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2586:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V28*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V28*/ meltfptr[25]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V33*/ meltfptr[24] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V33*/ meltfptr[24] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2586:/ locexp");
	    meltgc_add_strbuf_ccomment ((melt_ptr_t)
					( /*_.OUTBUF__V5*/ meltfptr[4]),
					melt_string_str ((melt_ptr_t)
							 ( /*_.NAMED_NAME__V33*/ meltfptr[24])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2587:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2588:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2590:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("case "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2591:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V28*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V28*/ meltfptr[25]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 11, "CTYPG_BOXEDMAGIC");
    /*_.CTYPG_BOXEDMAGIC__V34*/ meltfptr[30] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPG_BOXEDMAGIC__V34*/ meltfptr[30] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2591:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CTYPG_BOXEDMAGIC__V34*/ meltfptr[30])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2592:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (":"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2593:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2594:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("break;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2595:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2597:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("case "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2598:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V28*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V28*/ meltfptr[25]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 12, "CTYPG_MAPMAGIC");
    /*_.CTYPG_MAPMAGIC__V35*/ meltfptr[34] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPG_MAPMAGIC__V35*/ meltfptr[34] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2598:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CTYPG_MAPMAGIC__V35*/
						   meltfptr[34])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2599:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (": {"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2600:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (3),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2601:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("struct "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2602:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V28*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V28*/ meltfptr[25]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 16, "CTYPG_MAPSTRUCT");
    /*_.CTYPG_MAPSTRUCT__V36*/ meltfptr[35] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPG_MAPSTRUCT__V36*/ meltfptr[35] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2602:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CTYPG_MAPSTRUCT__V36*/
						   meltfptr[35])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2603:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (" *src = (struct "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2604:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V28*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V28*/ meltfptr[25]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 16, "CTYPG_MAPSTRUCT");
    /*_.CTYPG_MAPSTRUCT__V37*/ meltfptr[36] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPG_MAPSTRUCT__V37*/ meltfptr[36] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2604:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CTYPG_MAPSTRUCT__V37*/
						   meltfptr[36])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2605:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("*) p;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2606:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2607:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("int siz=0, ix=0;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2608:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2609:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("MELT_FORWARDED(src->meltmap_aux);"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2610:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2611:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("if (!src->entab) break;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2612:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2613:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("siz = melt_primtab[src->lenix];"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2614:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2615:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("gcc_assert (siz>0);"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2616:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2617:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("if (melt_is_young (src->entab)) {"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2618:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (4),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2619:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("struct "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2620:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V28*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V28*/ meltfptr[25]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 15, "CTYPG_ENTRYSTRUCT");
    /*_.CTYPG_ENTRYSTRUCT__V38*/ meltfptr[37] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPG_ENTRYSTRUCT__V38*/ meltfptr[37] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2620:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CTYPG_ENTRYSTRUCT__V38*/ meltfptr[37])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2621:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("* newtab = ggc_alloc_vec_"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2622:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V28*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V28*/ meltfptr[25]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 15, "CTYPG_ENTRYSTRUCT");
    /*_.CTYPG_ENTRYSTRUCT__V39*/ meltfptr[38] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPG_ENTRYSTRUCT__V39*/ meltfptr[38] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2622:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CTYPG_ENTRYSTRUCT__V39*/ meltfptr[38])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2623:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (" (siz);"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2624:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (4),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2625:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("memcpy (newtab, src->entab, siz * sizeof (struct "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2626:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V28*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V28*/ meltfptr[25]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 15, "CTYPG_ENTRYSTRUCT");
    /*_.CTYPG_ENTRYSTRUCT__V40*/ meltfptr[39] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPG_ENTRYSTRUCT__V40*/ meltfptr[39] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2626:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CTYPG_ENTRYSTRUCT__V40*/ meltfptr[39])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2627:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("));"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2628:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (4),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2629:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("src->entab = newtab;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2630:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (4),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2631:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("} /*end if young entab */"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2632:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2633:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("for (ix = 0; ix < siz; ix++) {"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2634:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (4),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2635:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V28*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[5])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V28*/ meltfptr[25]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "CTYPE_CNAME");
    /*_.CTYPE_CNAME__V41*/ meltfptr[40] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPE_CNAME__V41*/ meltfptr[40] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2635:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CTYPE_CNAME__V41*/
						   meltfptr[40])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2636:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (" at = src->entab[ix].e_at;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2637:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (4),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2638:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("if (!at || (void*) at == (void*) HTAB_DELETED_ENTRY) {"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2639:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (6),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2640:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("src->entab[ix].e_va = NULL;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2641:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (6),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2642:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("continue;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2643:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (4),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2644:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("} /*end if empty at */"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2645:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (4),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2646:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("MELT_FORWARDED (src->entab[ix].e_va);"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2647:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2648:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("} /*end for ix*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2649:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2650:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("};    /* end case "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2651:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V28*/
					       meltfptr[25]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V28*/ meltfptr[25]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 12, "CTYPG_MAPMAGIC");
    /*_.CTYPG_MAPMAGIC__V42*/ meltfptr[41] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPG_MAPMAGIC__V42*/ meltfptr[41] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2651:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CTYPG_MAPMAGIC__V42*/
						   meltfptr[41])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2652:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (" */"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2653:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2654:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("break;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2655:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;
	  if ( /*_#TIX__L11*/ meltfnum[7] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:2579:/ clear");
	    /*clear *//*_.CURCTYP__V28*/ meltfptr[25] = 0;
      /*^clear */
	    /*clear *//*_#TIX__L11*/ meltfnum[7] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V29*/ meltfptr[26] = 0;
      /*^clear */
	    /*clear *//*_#I__L14*/ meltfnum[12] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V33*/ meltfptr[24] = 0;
      /*^clear */
	    /*clear *//*_.CTYPG_BOXEDMAGIC__V34*/ meltfptr[30] = 0;
      /*^clear */
	    /*clear *//*_.CTYPG_MAPMAGIC__V35*/ meltfptr[34] = 0;
      /*^clear */
	    /*clear *//*_.CTYPG_MAPSTRUCT__V36*/ meltfptr[35] = 0;
      /*^clear */
	    /*clear *//*_.CTYPG_MAPSTRUCT__V37*/ meltfptr[36] = 0;
      /*^clear */
	    /*clear *//*_.CTYPG_ENTRYSTRUCT__V38*/ meltfptr[37] = 0;
      /*^clear */
	    /*clear *//*_.CTYPG_ENTRYSTRUCT__V39*/ meltfptr[38] = 0;
      /*^clear */
	    /*clear *//*_.CTYPG_ENTRYSTRUCT__V40*/ meltfptr[39] = 0;
      /*^clear */
	    /*clear *//*_.CTYPE_CNAME__V41*/ meltfptr[40] = 0;
      /*^clear */
	    /*clear *//*_.CTYPG_MAPMAGIC__V42*/ meltfptr[41] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2658:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   melt_string_str ((melt_ptr_t)
					    (( /*!konst_6 */ meltfrout->
					      tabval[6]))));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2666:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2667:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("/**end of code generated by generate_runtypesupport_scanning **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2669:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:2505:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GENERATE_RUNTYPESUPPORT_SCANNING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_51_warmelt_modes_GENERATE_RUNTYPESUPPORT_SCANNING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_51_warmelt_modes_GENERATE_RUNTYPESUPPORT_SCANNING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_modes_GENERATE_RUNTYPESUPPORT_PARAM (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un *
							 meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un *
							 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_52_warmelt_modes_GENERATE_RUNTYPESUPPORT_PARAM_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_52_warmelt_modes_GENERATE_RUNTYPESUPPORT_PARAM_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 39
    melt_ptr_t mcfr_varptr[39];
#define MELTFRAM_NBVARNUM 30
    long mcfr_varnum[30];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_52_warmelt_modes_GENERATE_RUNTYPESUPPORT_PARAM is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_52_warmelt_modes_GENERATE_RUNTYPESUPPORT_PARAM_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 39; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_52_warmelt_modes_GENERATE_RUNTYPESUPPORT_PARAM nbval 39*/
  meltfram__.mcfr_nbvar = 39 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GENERATE_RUNTYPESUPPORT_PARAM", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:2677:/ getarg");
 /*_.CTYTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VALDESCTUP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTNAME__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTNAME__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTBUF__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2678:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:2678:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2678:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2678;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_param start outname=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTNAME__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2678:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:2678:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2678:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2679:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2679:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2679:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctytup"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2679) ? (2679) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2679:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2680:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2]))
	 == MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2680:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2680:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check valdesctup"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2680) ? (2680) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2680:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2681:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-modes.melt:2681:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2681:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check outbuf"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2681) ? (2681) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2681:/ clear");
	     /*clear *//*_#IS_STRBUF__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2682:/ locexp");
      if (65000 > 0)
	meltgc_strbuf_reserve ((melt_ptr_t) /*_.OUTBUF__V5*/ meltfptr[4],
			       (unsigned) 65000);;
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:2683:/ quasiblock");


 /*_#NUMDELTA__L6*/ meltfnum[0] = 1;;
    /*^compute */
 /*_#LASTNUM__L7*/ meltfnum[1] = 0;;

    {
      MELT_LOCATION ("warmelt-modes.melt:2689:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2690:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2691:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("/** start of code generated by generate_runtypesupport_param **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2693:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2694:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("/* support for MELT parameter passing*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2697:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2699:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("enum /* generated enumeration for MELT parameters */ {"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2700:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2701:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("MELTBPAR__NONE=0,"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2702:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(1), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CTYTUP__V2*/ meltfptr[1]);
      for ( /*_#TIX__L8*/ meltfnum[7] = 0;
	   ( /*_#TIX__L8*/ meltfnum[7] >= 0)
	   && ( /*_#TIX__L8*/ meltfnum[7] < meltcit1__EACHTUP_ln);
	/*_#TIX__L8*/ meltfnum[7]++)
	{
	  /*_.CURCTYP__V16*/ meltfptr[14] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1]),
			       /*_#TIX__L8*/ meltfnum[7]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2706:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L9*/ meltfnum[8] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2706:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:2706:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2706;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "runtypesupport_param curctyp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCTYP__V16*/ meltfptr[14];
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V18*/ meltfptr[17] =
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:2706:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V18*/ meltfptr[17] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:2706:/ quasiblock");


	    /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2706:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2707:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("/*gtyctype #"));
	  }
	  ;
  /*_#I__L11*/ meltfnum[9] =
	    ((1) + ( /*_#TIX__L8*/ meltfnum[7]));;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2708:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.OUTBUF__V5*/ meltfptr[4]),
				   ( /*_#I__L11*/ meltfnum[9]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2709:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (" "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2710:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V16*/
					       meltfptr[14]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V21*/ meltfptr[17] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V21*/ meltfptr[17] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2710:/ locexp");
	    meltgc_add_strbuf_ccomment ((melt_ptr_t)
					( /*_.OUTBUF__V5*/ meltfptr[4]),
					melt_string_str ((melt_ptr_t)
							 ( /*_.NAMED_NAME__V21*/ meltfptr[17])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2711:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2712:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2713:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V16*/
					       meltfptr[14]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 4, "CTYPE_PARCHAR");
    /*_.CTYPCHAR__V22*/ meltfptr[18] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPCHAR__V22*/ meltfptr[18] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2714:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V16*/
					       meltfptr[14]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 5, "CTYPE_PARSTRING");
    /*_.CTYPSTR__V23*/ meltfptr[22] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPSTR__V23*/ meltfptr[22] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2718:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CTYPCHAR__V22*/ meltfptr[18])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2719:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       melt_string_str ((melt_ptr_t)
							( /*_.CTYPCHAR__V22*/
							 meltfptr[18])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2720:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("   /*="));
		}
		;
    /*_#I__L12*/ meltfnum[8] =
		  (( /*_#NUMDELTA__L6*/ meltfnum[0]) +
		   ( /*_#TIX__L8*/ meltfnum[7]));;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2721:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.OUTBUF__V5*/ meltfptr[4]),
					 ( /*_#I__L12*/ meltfnum[8]));
		}
		;
    /*_#I__L13*/ meltfnum[12] =
		  (( /*_#NUMDELTA__L6*/ meltfnum[0]) +
		   ( /*_#TIX__L8*/ meltfnum[7]));;
		MELT_LOCATION ("warmelt-modes.melt:2722:/ compute");
		/*_#LASTNUM__L7*/ meltfnum[1] =
		  /*_#SETQ___L14*/ meltfnum[13] = /*_#I__L13*/ meltfnum[12];;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2723:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("*/,"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2718:/ quasiblock");


		/*epilog */

		/*^clear */
	      /*clear *//*_#I__L12*/ meltfnum[8] = 0;
		/*^clear */
	      /*clear *//*_#I__L13*/ meltfnum[12] = 0;
		/*^clear */
	      /*clear *//*_#SETQ___L14*/ meltfnum[13] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2726:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("   /*-- non parameter --*/"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2725:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2727:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2730:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CTYPSTR__V23*/ meltfptr[22])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2731:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("#define "));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2732:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       melt_string_str ((melt_ptr_t)
							( /*_.CTYPSTR__V23*/
							 meltfptr[22])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2733:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("  \"\\x"));
		}
		;
    /*_#I__L16*/ meltfnum[12] =
		  (( /*_#NUMDELTA__L6*/ meltfnum[0]) +
		   ( /*_#TIX__L8*/ meltfnum[7]));;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2734:/ locexp");
		  meltgc_add_strbuf_hex ((melt_ptr_t)
					 ( /*_.OUTBUF__V5*/ meltfptr[4]),
					 ( /*_#I__L16*/ meltfnum[12]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2735:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("\""));
		}
		;
    /*_#I__L17*/ meltfnum[13] =
		  (( /*_#NUMDELTA__L6*/ meltfnum[0]) +
		   ( /*_#TIX__L8*/ meltfnum[7]));;
		MELT_LOCATION ("warmelt-modes.melt:2736:/ compute");
		/*_#LASTNUM__L7*/ meltfnum[1] =
		  /*_#SETQ___L18*/ meltfnum[17] = /*_#I__L17*/ meltfnum[13];;
		MELT_LOCATION ("warmelt-modes.melt:2730:/ quasiblock");


		/*_#PROGN___L19*/ meltfnum[18] =
		  /*_#SETQ___L18*/ meltfnum[17];;
		/*^compute */
		/*_#IFELSE___L15*/ meltfnum[8] =
		  /*_#PROGN___L19*/ meltfnum[18];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:2730:/ clear");
	      /*clear *//*_#I__L16*/ meltfnum[12] = 0;
		/*^clear */
	      /*clear *//*_#I__L17*/ meltfnum[13] = 0;
		/*^clear */
	      /*clear *//*_#SETQ___L18*/ meltfnum[17] = 0;
		/*^clear */
	      /*clear *//*_#PROGN___L19*/ meltfnum[18] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2738:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("   /*-- non paramstr --*/"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2737:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2739:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2740:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-modes.melt:2713:/ clear");
	    /*clear *//*_.CTYPCHAR__V22*/ meltfptr[18] = 0;
	  /*^clear */
	    /*clear *//*_.CTYPSTR__V23*/ meltfptr[22] = 0;
	  /*^clear */
	    /*clear *//*_#IFELSE___L15*/ meltfnum[8] = 0;
	  MELT_LOCATION ("warmelt-modes.melt:2745:/ quasiblock");


  /*_#ARGDESCRMAX__L20*/ meltfnum[12] = 0;;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2746:/ locexp");
	    /*SETARGDESCRMAX__1 */
		   /*_#ARGDESCRMAX__L20*/ meltfnum[12] = MELT_ARGDESCR_MAX;;
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2750:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#I__L21*/ meltfnum[13] =
	    (( /*_#ARGDESCRMAX__L20*/ meltfnum[12]) - (10));;
	  /*^compute */
  /*_#I__L22*/ meltfnum[17] =
	    (( /*_#LASTNUM__L7*/ meltfnum[1]) >
	     ( /*_#I__L21*/ meltfnum[13]));;
	  MELT_LOCATION ("warmelt-modes.melt:2750:/ cond");
	  /*cond */ if ( /*_#I__L22*/ meltfnum[17])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2751:/ locexp");
		  error ("MELT ERROR MSG [#%ld]::: %s", melt_dbgcounter,
			 ("too many ctypes for generated enum with MELTBPAR* w.r.t. MELT_ARGDESCR_MAX"));
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-modes.melt:2753:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#I__L23*/ meltfnum[18] =
		    (( /*_#ARGDESCRMAX__L20*/ meltfnum[12]) - (2));;
		  /*^compute */
      /*_#I__L24*/ meltfnum[8] =
		    (( /*_#LASTNUM__L7*/ meltfnum[1]) <
		     ( /*_#I__L23*/ meltfnum[18]));;
		  MELT_LOCATION ("warmelt-modes.melt:2753:/ cond");
		  /*cond */ if ( /*_#I__L24*/ meltfnum[8])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V27*/ meltfptr[26] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-modes.melt:2753:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("exhausted number of ctypes w.r.t MELT_ARGDESCR_MAX"), ("warmelt-modes.melt") ? ("warmelt-modes.melt") : __FILE__, (2753) ? (2753) : __LINE__, __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V27*/ meltfptr[26] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V26*/ meltfptr[25] =
		    /*_.IFELSE___V27*/ meltfptr[26];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:2753:/ clear");
		/*clear *//*_#I__L23*/ meltfnum[18] = 0;
		  /*^clear */
		/*clear *//*_#I__L24*/ meltfnum[8] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V27*/ meltfptr[26] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V26*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-modes.melt:2750:/ quasiblock");


		/*_.PROGN___V28*/ meltfptr[26] =
		  /*_.IFCPP___V26*/ meltfptr[25];;
		/*^compute */
		/*_.IFELSE___V25*/ meltfptr[22] =
		  /*_.PROGN___V28*/ meltfptr[26];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:2750:/ clear");
	      /*clear *//*_.IFCPP___V26*/ meltfptr[25] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V28*/ meltfptr[26] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:2755:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#I__L25*/ meltfnum[18] =
		  (( /*_#ARGDESCRMAX__L20*/ meltfnum[12]) - (20));;
		/*^compute */
    /*_#I__L26*/ meltfnum[8] =
		  (( /*_#LASTNUM__L7*/ meltfnum[1]) >
		   ( /*_#I__L25*/ meltfnum[18]));;
		MELT_LOCATION ("warmelt-modes.melt:2755:/ cond");
		/*cond */ if ( /*_#I__L26*/ meltfnum[8])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {




		      {
			MELT_LOCATION ("warmelt-modes.melt:2756:/ locexp");
			warning (0, "MELT WARNING MSG [#%ld]::: %s",
				 melt_dbgcounter,
				 ("the number of ctypes is dangerously near MELT_ARGDESCR_MAX"));
		      }
		      ;
		/*clear *//*_.IFELSE___V29*/ meltfptr[25] = 0;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-modes.melt:2755:/ cond.else");

     /*_.IFELSE___V29*/ meltfptr[25] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V25*/ meltfptr[22] =
		  /*_.IFELSE___V29*/ meltfptr[25];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:2750:/ clear");
	      /*clear *//*_#I__L25*/ meltfnum[18] = 0;
		/*^clear */
	      /*clear *//*_#I__L26*/ meltfnum[8] = 0;
		/*^clear */
	      /*clear *//*_.IFELSE___V29*/ meltfptr[25] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.LET___V24*/ meltfptr[18] = /*_.IFELSE___V25*/ meltfptr[22];;

	  MELT_LOCATION ("warmelt-modes.melt:2745:/ clear");
	    /*clear *//*_#ARGDESCRMAX__L20*/ meltfnum[12] = 0;
	  /*^clear */
	    /*clear *//*_#I__L21*/ meltfnum[13] = 0;
	  /*^clear */
	    /*clear *//*_#I__L22*/ meltfnum[17] = 0;
	  /*^clear */
	    /*clear *//*_.IFELSE___V25*/ meltfptr[22] = 0;
	  if ( /*_#TIX__L8*/ meltfnum[7] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:2703:/ clear");
	    /*clear *//*_.CURCTYP__V16*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_#TIX__L8*/ meltfnum[7] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
      /*^clear */
	    /*clear *//*_#I__L11*/ meltfnum[9] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V21*/ meltfptr[17] = 0;
      /*^clear */
	    /*clear *//*_.LET___V24*/ meltfptr[18] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2761:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   (" MELTBPAR__LAST}; /*end enum for MELT parameters*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2762:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2763:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2765:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("union meltparam_un /* generated union for MELT parameters */ {"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2766:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2767:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("void* meltbp_any;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2768:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(1), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CTYTUP__V2*/ meltfptr[1]);
      for ( /*_#TIX__L27*/ meltfnum[18] = 0;
	   ( /*_#TIX__L27*/ meltfnum[18] >= 0)
	   && ( /*_#TIX__L27*/ meltfnum[18] < meltcit2__EACHTUP_ln);
	/*_#TIX__L27*/ meltfnum[18]++)
	{
	  /*_.CURCTYP__V30*/ meltfptr[26] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1]),
			       /*_#TIX__L27*/ meltfnum[18]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2772:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L28*/ meltfnum[8] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2772:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L28*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L29*/ meltfnum[12] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:2772:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L29*/ meltfnum[12];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2772;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "runtypesupport_param curctyp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCTYP__V30*/ meltfptr[26];
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V32*/ meltfptr[22] =
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:2772:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L29*/ meltfnum[12] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V32*/ meltfptr[22] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:2772:/ quasiblock");


	    /*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[22];;
	    /*^compute */
	    /*_.IFCPP___V31*/ meltfptr[25] = /*_.PROGN___V34*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2772:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L28*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V32*/ meltfptr[22] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V31*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2773:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2774:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("/*ctype #"));
	  }
	  ;
  /*_#I__L30*/ meltfnum[13] =
	    ((1) + ( /*_#TIX__L27*/ meltfnum[18]));;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2775:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.OUTBUF__V5*/ meltfptr[4]),
				   ( /*_#I__L30*/ meltfnum[13]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2776:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 (" "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2777:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V30*/
					       meltfptr[26]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V30*/ meltfptr[26]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V35*/ meltfptr[22] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V35*/ meltfptr[22] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2777:/ locexp");
	    meltgc_add_strbuf_ccomment ((melt_ptr_t)
					( /*_.OUTBUF__V5*/ meltfptr[4]),
					melt_string_str ((melt_ptr_t)
							 ( /*_.NAMED_NAME__V35*/ meltfptr[22])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2778:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				 ("*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2779:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2780:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V30*/
					       meltfptr[26]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V30*/ meltfptr[26]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 6, "CTYPE_ARGFIELD");
    /*_.ARGTYP__V36*/ meltfptr[32] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.ARGTYP__V36*/ meltfptr[32] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2781:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V30*/
					       meltfptr[26]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V30*/ meltfptr[26]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 7, "CTYPE_RESFIELD");
    /*_.RESTYP__V37*/ meltfptr[36] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.RESTYP__V37*/ meltfptr[36] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2784:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.ARGTYP__V36*/ meltfptr[32])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:2785:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURCTYP__V30*/
						     meltfptr[26]),
						    (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[2])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURCTYP__V30*/ meltfptr[26])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 3, "CTYPE_CNAME");
      /*_.CTYPE_CNAME__V38*/ meltfptr[37] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.CTYPE_CNAME__V38*/ meltfptr[37] = NULL;;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2785:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       melt_string_str ((melt_ptr_t)
							( /*_.CTYPE_CNAME__V38*/ meltfptr[37])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2786:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       (" "));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2787:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       melt_string_str ((melt_ptr_t)
							( /*_.ARGTYP__V36*/
							 meltfptr[32])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2788:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("; /*argument param.*/"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2784:/ quasiblock");


		/*epilog */

		/*^clear */
	      /*clear *//*_.CTYPE_CNAME__V38*/ meltfptr[37] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2791:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("/* no argument */"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2790:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2793:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2796:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.RESTYP__V37*/ meltfptr[36])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2797:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.OUTBUF__V5*/ meltfptr[4]),
					    (1), 0);
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2798:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURCTYP__V30*/
						     meltfptr[26]),
						    (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[2])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURCTYP__V30*/ meltfptr[26])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 3, "CTYPE_CNAME");
      /*_.CTYPE_CNAME__V39*/ meltfptr[37] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.CTYPE_CNAME__V39*/ meltfptr[37] = NULL;;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2798:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       melt_string_str ((melt_ptr_t)
							( /*_.CTYPE_CNAME__V39*/ meltfptr[37])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2799:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       (" *"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2800:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       melt_string_str ((melt_ptr_t)
							( /*_.RESTYP__V37*/
							 meltfptr[36])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2801:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("; /*result param.*/"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2796:/ quasiblock");


		/*epilog */

		/*^clear */
	      /*clear *//*_.CTYPE_CNAME__V39*/ meltfptr[37] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2804:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.OUTBUF__V5*/ meltfptr[4]),
					    (1), 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2805:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTBUF__V5*/ meltfptr[4]),
				       ("/*no result*/"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2803:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  ;

	  MELT_LOCATION ("warmelt-modes.melt:2780:/ clear");
	    /*clear *//*_.ARGTYP__V36*/ meltfptr[32] = 0;
	  /*^clear */
	    /*clear *//*_.RESTYP__V37*/ meltfptr[36] = 0;
	  if ( /*_#TIX__L27*/ meltfnum[18] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:2769:/ clear");
	    /*clear *//*_.CURCTYP__V30*/ meltfptr[26] = 0;
      /*^clear */
	    /*clear *//*_#TIX__L27*/ meltfnum[18] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V31*/ meltfptr[25] = 0;
      /*^clear */
	    /*clear *//*_#I__L30*/ meltfnum[13] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V35*/ meltfptr[22] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2808:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2809:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("}; /* end generated union for MELT parameters */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2810:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2811:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			   ("/** end of code generated by generate_runtypesupport_param **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2813:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2814:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-modes.melt:2683:/ clear");
	   /*clear *//*_#NUMDELTA__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#LASTNUM__L7*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-modes.melt:2677:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GENERATE_RUNTYPESUPPORT_PARAM", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_52_warmelt_modes_GENERATE_RUNTYPESUPPORT_PARAM_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_52_warmelt_modes_GENERATE_RUNTYPESUPPORT_PARAM */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_modes_GENERATE_RUNTYPESUPPORT_COD2CTYPE (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un *
							     meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un *
							     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_53_warmelt_modes_GENERATE_RUNTYPESUPPORT_COD2CTYPE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_53_warmelt_modes_GENERATE_RUNTYPESUPPORT_COD2CTYPE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 27
    melt_ptr_t mcfr_varptr[27];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_53_warmelt_modes_GENERATE_RUNTYPESUPPORT_COD2CTYPE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_53_warmelt_modes_GENERATE_RUNTYPESUPPORT_COD2CTYPE_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 27; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_53_warmelt_modes_GENERATE_RUNTYPESUPPORT_COD2CTYPE nbval 27*/
  meltfram__.mcfr_nbvar = 27 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GENERATE_RUNTYPESUPPORT_COD2CTYPE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:2819:/ getarg");
 /*_.CTYTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VALDESCTUP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTNAME__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTNAME__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTBUF__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2820:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:2820:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2820:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2820;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_param start outname=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTNAME__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2820:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:2820:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2820:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2821:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2821:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2821:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctytup"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2821) ? (2821) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2821:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2822:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2]))
	 == MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2822:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2822:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check valdesctup"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2822) ? (2822) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2822:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2823:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-modes.melt:2823:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2823:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check outbuf"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2823) ? (2823) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2823:/ clear");
	     /*clear *//*_#IS_STRBUF__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2824:/ locexp");
      if (65000 > 0)
	meltgc_strbuf_reserve ((melt_ptr_t) /*_.OUTBUF__V5*/ meltfptr[4],
			       (unsigned) 65000);;
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:2825:/ quasiblock");


 /*_#NUMDELTA__L6*/ meltfnum[0] = 1;;
    /*^compute */
 /*_#LASTNUM__L7*/ meltfnum[1] = 0;;

    {
      MELT_LOCATION ("warmelt-modes.melt:2831:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2832:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2833:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2834:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		      ("/* start of code generated by generate_runtypesupport_cod2ctype */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2835:/ locexp");
      meltgc_out_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			     (0), 0);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2836:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		      ("melt_ptr_t melt_code_to_ctype (int code) {"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2837:/ locexp");
      meltgc_out_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			     (1), 0);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2838:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		      ("switch (code) {"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2839:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(2), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CTYTUP__V2*/ meltfptr[1]);
      for ( /*_#TIX__L8*/ meltfnum[7] = 0;
	   ( /*_#TIX__L8*/ meltfnum[7] >= 0)
	   && ( /*_#TIX__L8*/ meltfnum[7] < meltcit1__EACHTUP_ln);
	/*_#TIX__L8*/ meltfnum[7]++)
	{
	  /*_.CURCTYP__V16*/ meltfptr[14] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1]),
			       /*_#TIX__L8*/ meltfnum[7]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2843:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L9*/ meltfnum[8] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2843:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:2843:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2843;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "runtypesupport_cod2ctype curctyp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCTYP__V16*/ meltfptr[14];
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V18*/ meltfptr[17] =
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:2843:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V18*/ meltfptr[17] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:2843:/ quasiblock");


	    /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2843:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2844:/ quasiblock");


  /*_#TIXNEXT__L11*/ meltfnum[9] =
	    (( /*_#TIX__L8*/ meltfnum[7]) + (1));;
	  MELT_LOCATION ("warmelt-modes.melt:2845:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V16*/
					       meltfptr[14]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 4, "CTYPE_PARCHAR");
    /*_.CTYPCHAR__V22*/ meltfptr[18] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPCHAR__V22*/ meltfptr[18] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2846:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V16*/
					       meltfptr[14]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.CTYPNAME__V23*/ meltfptr[22] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CTYPNAME__V23*/ meltfptr[22] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2848:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2849:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[5];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "/* #";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#TIXNEXT__L11*/ meltfnum[9];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = ": ";
	    /*^apply.arg */
	    argtab[3].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CTYPNAME__V23*/ meltfptr[22];
	    /*^apply.arg */
	    argtab[4].meltbp_cstring = " */";
	    /*_.ADD2OUT__V24*/ meltfptr[23] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2850:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V5*/ meltfptr[4]), (2),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2851:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CTYPCHAR__V22*/ meltfptr[18])	/*then */
	    {
	      /*^cond.then */
	      /*_.IF___V25*/ meltfptr[24] = /*_.CTYPNAME__V23*/ meltfptr[22];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:2851:/ cond.else");

   /*_.IF___V25*/ meltfptr[24] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2851:/ cond");
	  /*cond */ if ( /*_.IF___V25*/ meltfptr[24])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:2852:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[5];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "    case ";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CTYPCHAR__V22*/ meltfptr[18];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = ": return MELT_PREDEF(";
		  /*^apply.arg */
		  argtab[3].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CTYPNAME__V23*/ meltfptr[22];
		  /*^apply.arg */
		  argtab[4].meltbp_cstring = ");";
		  /*_.ADD2OUT__V27*/ meltfptr[26] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[3])),
				(melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V26*/ meltfptr[25] =
		  /*_.ADD2OUT__V27*/ meltfptr[26];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:2851:/ clear");
	      /*clear *//*_.ADD2OUT__V27*/ meltfptr[26] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V26*/ meltfptr[25] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.LET___V21*/ meltfptr[17] = /*_.IF___V26*/ meltfptr[25];;

	  MELT_LOCATION ("warmelt-modes.melt:2844:/ clear");
	    /*clear *//*_#TIXNEXT__L11*/ meltfnum[9] = 0;
	  /*^clear */
	    /*clear *//*_.CTYPCHAR__V22*/ meltfptr[18] = 0;
	  /*^clear */
	    /*clear *//*_.CTYPNAME__V23*/ meltfptr[22] = 0;
	  /*^clear */
	    /*clear *//*_.ADD2OUT__V24*/ meltfptr[23] = 0;
	  /*^clear */
	    /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
	  /*^clear */
	    /*clear *//*_.IF___V26*/ meltfptr[25] = 0;
	  if ( /*_#TIX__L8*/ meltfnum[7] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:2840:/ clear");
	    /*clear *//*_.CURCTYP__V16*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_#TIX__L8*/ meltfnum[7] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
      /*^clear */
	    /*clear *//*_.LET___V21*/ meltfptr[17] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2855:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(2), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2856:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		      ("default: break;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2857:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(2), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2858:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		      ("} /*end switch code*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2859:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2860:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		      ("return NULL;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2861:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2862:/ locexp");
      meltgc_add_out ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
		      ("} /* end of generated melt_code_to_ctype */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2863:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2864:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-modes.melt:2825:/ clear");
	   /*clear *//*_#NUMDELTA__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#LASTNUM__L7*/ meltfnum[1] = 0;
    MELT_LOCATION ("warmelt-modes.melt:2819:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GENERATE_RUNTYPESUPPORT_COD2CTYPE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_53_warmelt_modes_GENERATE_RUNTYPESUPPORT_COD2CTYPE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_53_warmelt_modes_GENERATE_RUNTYPESUPPORT_COD2CTYPE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAG2STR (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   * meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_54_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAG2STR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_54_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAG2STR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 35
    melt_ptr_t mcfr_varptr[35];
#define MELTFRAM_NBVARNUM 17
    long mcfr_varnum[17];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_54_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAG2STR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_54_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAG2STR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 35; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_54_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAG2STR nbval 35*/
  meltfram__.mcfr_nbvar = 35 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GENERATE_RUNTYPESUPPORT_MAG2STR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:2869:/ getarg");
 /*_.CTYTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.VALDESCTUP__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTNAME__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTNAME__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTDECLBUF__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTDECLBUF__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTCODEBUF__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTCODEBUF__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2870:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:2870:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2870:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2870;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_mag2str start outname=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTNAME__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2870:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:2870:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2870:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2871:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2871:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2871:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctytup"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2871) ? (2871) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[7] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2871:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2872:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VALDESCTUP__V3*/ meltfptr[2]))
	 == MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2872:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2872:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check valdesctup"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2872) ? (2872) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[8] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2872:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2873:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OUTDECLBUF__V5*/ meltfptr[4]))
	 == MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-modes.melt:2873:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2873:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check outdeclbuf"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2873) ? (2873) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2873:/ clear");
	     /*clear *//*_#IS_STRBUF__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2874:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OUTCODEBUF__V6*/ meltfptr[5]))
	 == MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-modes.melt:2874:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2874:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check outcodebuf"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2874) ? (2874) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[15] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2874:/ clear");
	     /*clear *//*_#IS_STRBUF__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2875:/ locexp");
      if (35000 > 0)
	meltgc_strbuf_reserve ((melt_ptr_t) /*_.OUTDECLBUF__V5*/ meltfptr[4],
			       (unsigned) 35000);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2876:/ locexp");
      if (75000 > 0)
	meltgc_strbuf_reserve ((melt_ptr_t) /*_.OUTCODEBUF__V6*/ meltfptr[5],
			       (unsigned) 75000);;
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:2877:/ quasiblock");


 /*_#NUMDELTA__L7*/ meltfnum[1] = 1;;
    /*^compute */
 /*_#LASTNUM__L8*/ meltfnum[0] = 0;;

    {
      MELT_LOCATION ("warmelt-modes.melt:2883:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTDECLBUF__V5*/ meltfptr[4]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2884:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTDECLBUF__V5*/ meltfptr[4]),
			   ("/** declaration generated by generate_runtypesupport_mag2str **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2886:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTDECLBUF__V5*/ meltfptr[4]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2887:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTDECLBUF__V5*/ meltfptr[4]),
			   ("const char* melt_obmag_string (int i);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2889:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTDECLBUF__V5*/ meltfptr[4]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2890:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTDECLBUF__V5*/ meltfptr[4]),
			   ("#define MELT_OBMAG_STRING_generated"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2891:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTDECLBUF__V5*/ meltfptr[4]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2892:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTDECLBUF__V5*/ meltfptr[4]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2894:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTCODEBUF__V6*/ meltfptr[5]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2895:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTCODEBUF__V6*/ meltfptr[5]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2896:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
			   ("/** start of code generated by generate_runtypesupport_mag2str **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2898:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTCODEBUF__V6*/ meltfptr[5]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2899:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
			   melt_string_str ((melt_ptr_t)
					    (( /*!konst_1 */ meltfrout->
					      tabval[1]))));
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CTYTUP__V2*/ meltfptr[1]);
      for ( /*_#TIX__L9*/ meltfnum[8] = 0;
	   ( /*_#TIX__L9*/ meltfnum[8] >= 0)
	   && ( /*_#TIX__L9*/ meltfnum[8] < meltcit1__EACHTUP_ln);
	/*_#TIX__L9*/ meltfnum[8]++)
	{
	  /*_.CURCTYP__V19*/ meltfptr[17] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1]),
			       /*_#TIX__L9*/ meltfnum[8]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2909:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L10*/ meltfnum[9] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2909:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:2909:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2909;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "runtypesupport_mag2str curctyp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCTYP__V19*/ meltfptr[17];
		    /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V21*/ meltfptr[20] =
		    /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:2909:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V21*/ meltfptr[20] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:2909:/ quasiblock");


	    /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
	    /*^compute */
	    /*_.IFCPP___V20*/ meltfptr[19] = /*_.PROGN___V23*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2909:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[9] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2910:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				      (1), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2911:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				 ("/*gtyctype #"));
	  }
	  ;
  /*_#I__L12*/ meltfnum[10] =
	    ((1) + ( /*_#TIX__L9*/ meltfnum[8]));;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2912:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				   ( /*_#I__L12*/ meltfnum[10]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2913:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.OUTCODEBUF__V6*/ meltfptr[5]), (" "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2914:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V19*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V19*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V24*/ meltfptr[20] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V24*/ meltfptr[20] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2914:/ locexp");
	    meltgc_add_strbuf_ccomment ((melt_ptr_t)
					( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
					melt_string_str ((melt_ptr_t)
							 ( /*_.NAMED_NAME__V24*/ meltfptr[20])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2915:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.OUTCODEBUF__V6*/ meltfptr[5]), ("*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2916:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				      (4), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2917:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V19*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V19*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 11, "CTYPG_BOXEDMAGIC");
    /*_.BOXMAGICSTR__V25*/ meltfptr[21] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.BOXMAGICSTR__V25*/ meltfptr[21] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2919:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.BOXMAGICSTR__V25*/ meltfptr[21])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2921:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       ("case "));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2922:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       melt_string_str ((melt_ptr_t)
							( /*_.BOXMAGICSTR__V25*/ meltfptr[21])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2923:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       (": return \""));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2924:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       melt_string_str ((melt_ptr_t)
							( /*_.BOXMAGICSTR__V25*/ meltfptr[21])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2925:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       ("\";"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2920:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:2919:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2927:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       ("/*runtypesupport_mag2str no boxed magic */"));
		}
		;
		/*epilog */
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2929:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				      (1), 0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-modes.melt:2917:/ clear");
	    /*clear *//*_.BOXMAGICSTR__V25*/ meltfptr[21] = 0;
	  MELT_LOCATION ("warmelt-modes.melt:2931:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V19*/
					       meltfptr[17]),
					      (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V19*/ meltfptr[17]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 12, "CTYPG_MAPMAGIC");
    /*_.MAPMAGICSTR__V26*/ meltfptr[21] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.MAPMAGICSTR__V26*/ meltfptr[21] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2933:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.MAPMAGICSTR__V26*/ meltfptr[21])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2935:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       ("case "));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2936:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       melt_string_str ((melt_ptr_t)
							( /*_.MAPMAGICSTR__V26*/ meltfptr[21])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2937:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       (": return \""));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2938:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       melt_string_str ((melt_ptr_t)
							( /*_.MAPMAGICSTR__V26*/ meltfptr[21])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2939:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       ("\";"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2934:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:2933:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2941:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       ("/*runtypesupport_mag2str no map magic */"));
		}
		;
		/*epilog */
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2943:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				      (1), 0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-modes.melt:2931:/ clear");
	    /*clear *//*_.MAPMAGICSTR__V26*/ meltfptr[21] = 0;
	  if ( /*_#TIX__L9*/ meltfnum[8] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:2906:/ clear");
	    /*clear *//*_.CURCTYP__V19*/ meltfptr[17] = 0;
      /*^clear */
	    /*clear *//*_#TIX__L9*/ meltfnum[8] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
      /*^clear */
	    /*clear *//*_#I__L12*/ meltfnum[10] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V24*/ meltfptr[20] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2947:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTCODEBUF__V6*/ meltfptr[5]), (0), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.VALDESCTUP__V3*/ meltfptr[2]);
      for ( /*_#VIX__L13*/ meltfnum[9] = 0;
	   ( /*_#VIX__L13*/ meltfnum[9] >= 0)
	   && ( /*_#VIX__L13*/ meltfnum[9] < meltcit2__EACHTUP_ln);
	/*_#VIX__L13*/ meltfnum[9]++)
	{
	  /*_.CURVALDESC__V27*/ meltfptr[21] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.VALDESCTUP__V3*/ meltfptr[2]),
			       /*_#VIX__L13*/ meltfnum[9]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2951:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L14*/ meltfnum[13] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2951:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[13])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:2951:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2951;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "generate_runtypesupport_mag2str curvaldesc=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURVALDESC__V27*/ meltfptr[21];
		    /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V29*/ meltfptr[28] =
		    /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:2951:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V29*/ meltfptr[28] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:2951:/ quasiblock");


	    /*_.PROGN___V31*/ meltfptr[29] = /*_.IF___V29*/ meltfptr[28];;
	    /*^compute */
	    /*_.IFCPP___V28*/ meltfptr[27] = /*_.PROGN___V31*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2951:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[13] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V29*/ meltfptr[28] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V31*/ meltfptr[29] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V28*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:2952:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L16*/ meltfnum[14] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURVALDESC__V27*/ meltfptr[21]),
				   (melt_ptr_t) (( /*!CLASS_VALUE_DESCRIPTOR */ meltfrout->tabval[4])));;
	    MELT_LOCATION ("warmelt-modes.melt:2952:/ cond");
	    /*cond */ if ( /*_#IS_A__L16*/ meltfnum[14])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V33*/ meltfptr[29] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:2952:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curvaldesc"),
					("warmelt-modes.melt")
					? ("warmelt-modes.melt") : __FILE__,
					(2952) ? (2952) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V33*/ meltfptr[29] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V32*/ meltfptr[28] = /*_.IFELSE___V33*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2952:/ clear");
	      /*clear *//*_#IS_A__L16*/ meltfnum[14] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V33*/ meltfptr[29] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V32*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2953:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				      (1), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2954:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				 ("/*valdesc #"));
	  }
	  ;
  /*_#I__L17*/ meltfnum[13] =
	    ((1) + ( /*_#VIX__L13*/ meltfnum[9]));;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2955:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				   ( /*_#I__L17*/ meltfnum[13]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2956:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.OUTCODEBUF__V6*/ meltfptr[5]), (" "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2957:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURVALDESC__V27*/
					       meltfptr[21]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURVALDESC__V27*/ meltfptr[21]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V34*/ meltfptr[29] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V34*/ meltfptr[29] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2957:/ locexp");
	    meltgc_add_strbuf_ccomment ((melt_ptr_t)
					( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
					melt_string_str ((melt_ptr_t)
							 ( /*_.NAMED_NAME__V34*/ meltfptr[29])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2958:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.OUTCODEBUF__V6*/ meltfptr[5]), ("*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2959:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				      (1), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2960:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURVALDESC__V27*/
					       meltfptr[21]),
					      (melt_ptr_t) (( /*!CLASS_VALUE_DESCRIPTOR */ meltfrout->tabval[4])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURVALDESC__V27*/ meltfptr[21]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 2, "VALDESC_OBJMAGIC");
    /*_.VALMAGICSTR__V35*/ meltfptr[34] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.VALMAGICSTR__V35*/ meltfptr[34] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:2962:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.VALMAGICSTR__V35*/ meltfptr[34])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2964:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       ("case "));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2965:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       melt_string_str ((melt_ptr_t)
							( /*_.VALMAGICSTR__V35*/ meltfptr[34])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2966:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       (": return \""));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2967:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       melt_string_str ((melt_ptr_t)
							( /*_.VALMAGICSTR__V35*/ meltfptr[34])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:2968:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       ("\";"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:2963:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:2962:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:2969:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				       ("/*runtypesupport_mag2str no value magic*/"));
		}
		;
		/*epilog */
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:2970:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
				      (1), 0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-modes.melt:2960:/ clear");
	    /*clear *//*_.VALMAGICSTR__V35*/ meltfptr[34] = 0;
	  if ( /*_#VIX__L13*/ meltfnum[9] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:2948:/ clear");
	    /*clear *//*_.CURVALDESC__V27*/ meltfptr[21] = 0;
      /*^clear */
	    /*clear *//*_#VIX__L13*/ meltfnum[9] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V28*/ meltfptr[27] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V32*/ meltfptr[28] = 0;
      /*^clear */
	    /*clear *//*_#I__L17*/ meltfnum[13] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V34*/ meltfptr[29] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2974:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
			   melt_string_str ((melt_ptr_t)
					    (( /*!konst_5 */ meltfrout->
					      tabval[5]))));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2982:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTCODEBUF__V6*/ meltfptr[5]), (0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2983:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTCODEBUF__V6*/ meltfptr[5]),
			   ("/** end of code generated by generate_runtypesupport_mag2str **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2985:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTCODEBUF__V6*/ meltfptr[5]), (0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-modes.melt:2877:/ clear");
	   /*clear *//*_#NUMDELTA__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#LASTNUM__L8*/ meltfnum[0] = 0;
    MELT_LOCATION ("warmelt-modes.melt:2869:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[15] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GENERATE_RUNTYPESUPPORT_MAG2STR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_54_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAG2STR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_54_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAG2STR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_modes_GENERATE_RUNTYPESUPPORT_BOXINGFUN (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un *
							     meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un *
							     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_55_warmelt_modes_GENERATE_RUNTYPESUPPORT_BOXINGFUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_55_warmelt_modes_GENERATE_RUNTYPESUPPORT_BOXINGFUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 56
    melt_ptr_t mcfr_varptr[56];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_55_warmelt_modes_GENERATE_RUNTYPESUPPORT_BOXINGFUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_55_warmelt_modes_GENERATE_RUNTYPESUPPORT_BOXINGFUN_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 56; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_55_warmelt_modes_GENERATE_RUNTYPESUPPORT_BOXINGFUN nbval 56*/
  meltfram__.mcfr_nbvar = 56 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GENERATE_RUNTYPESUPPORT_BOXINGFUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:2993:/ getarg");
 /*_.CTYTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTARG__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTARG__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CODEBUF__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CODEBUF__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2995:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:2995:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:2995:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2995;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_boxingfun start outarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:2995:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:2995:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2995:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2996:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:2996:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2996:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctytup is tuple"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2996) ? (2996) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2996:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2997:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-modes.melt:2997:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2997:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check declbuf is buffer"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2997) ? (2997) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2997:/ clear");
	     /*clear *//*_#IS_STRBUF__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:2998:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CODEBUF__V5*/ meltfptr[4])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-modes.melt:2998:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:2998:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check codebuf is buffer"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (2998) ? (2998) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:2998:/ clear");
	     /*clear *//*_#IS_STRBUF__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:2999:/ locexp");
      if (35000 > 0)
	meltgc_strbuf_reserve ((melt_ptr_t) /*_.DECLBUF__V4*/ meltfptr[3],
			       (unsigned) 35000);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3000:/ locexp");
      if (75000 > 0)
	meltgc_strbuf_reserve ((melt_ptr_t) /*_.CODEBUF__V5*/ meltfptr[4],
			       (unsigned) 75000);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3001:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3002:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3003:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3]),
			   ("/** start of declarations generated by generate_runtypesupport_boxingfun **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3005:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3007:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.CODEBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3008:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.CODEBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3009:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.CODEBUF__V5*/ meltfptr[4]),
			   ("/** start of code generated by generate_runtypesupport_boxingfun **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3011:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.CODEBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CTYTUP__V2*/ meltfptr[1]);
      for ( /*_#TIX__L6*/ meltfnum[0] = 0;
	   ( /*_#TIX__L6*/ meltfnum[0] >= 0)
	   && ( /*_#TIX__L6*/ meltfnum[0] < meltcit1__EACHTUP_ln);
	/*_#TIX__L6*/ meltfnum[0]++)
	{
	  /*_.CURCTYP__V16*/ meltfptr[14] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1]),
			       /*_#TIX__L6*/ meltfnum[0]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:3015:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L7*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3015:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:3015:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3015;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "generate runtypesupport_mapfun curctyp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCTYP__V16*/ meltfptr[14];
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V18*/ meltfptr[17] =
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:3015:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V18*/ meltfptr[17] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3015:/ quasiblock");


	    /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3015:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[1] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:3016:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L9*/ meltfnum[7] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURCTYP__V16*/ meltfptr[14]),
				   (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */
						  meltfrout->tabval[1])));;
	    MELT_LOCATION ("warmelt-modes.melt:3016:/ cond");
	    /*cond */ if ( /*_#IS_A__L9*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V22*/ meltfptr[18] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3016:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curctyp"),
					("warmelt-modes.melt")
					? ("warmelt-modes.melt") : __FILE__,
					(3016) ? (3016) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V21*/ meltfptr[17] = /*_.IFELSE___V22*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3016:/ clear");
	      /*clear *//*_#IS_A__L9*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:3017:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^block */
	  /*anyblock */
	  {

	    /*^objgoto */
	    /*objgoto */ goto mtch1_0;
	    ;

	  /*objlabel */ mtch1_0:;
	    MELT_LOCATION ("warmelt-modes.melt:3018:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.NAMED_NAME__V23*/ meltfptr[18] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPE_CNAME__V24*/ meltfptr[23] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_BOXEDMAGIC__V25*/ meltfptr[24] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_BOXEDSTRUCT__V26*/ meltfptr[25] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_BOXDISCR__V27*/ meltfptr[26] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_BOXFUN__V28*/ meltfptr[27] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_UNBOXFUN__V29*/ meltfptr[28] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_UPDATEBOXFUN__V30*/ meltfptr[29] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*normtesterinst */
	      (melt_is_instance_of
	       ((melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]),
		(melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[1])))))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V23*/ meltfptr[18] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 3, "CTYPE_CNAME");
      /*_.CTYPE_CNAME__V24*/ meltfptr[23] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 11, "CTYPG_BOXEDMAGIC");
      /*_.CTYPG_BOXEDMAGIC__V25*/ meltfptr[24] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 13,
					   "CTYPG_BOXEDSTRUCT");
      /*_.CTYPG_BOXEDSTRUCT__V26*/ meltfptr[25] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 17, "CTYPG_BOXDISCR");
      /*_.CTYPG_BOXDISCR__V27*/ meltfptr[26] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 20, "CTYPG_BOXFUN");
      /*_.CTYPG_BOXFUN__V28*/ meltfptr[27] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 21, "CTYPG_UNBOXFUN");
      /*_.CTYPG_UNBOXFUN__V29*/ meltfptr[28] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 22,
					   "CTYPG_UPDATEBOXFUN");
      /*_.CTYPG_UPDATEBOXFUN__V30*/ meltfptr[29] = slot;
		  };
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3019:/ objgoto");
		  /*objgoto */ goto mtch1_1;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3018:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3171:/ objgoto");
		  /*objgoto */ goto mtch1_10;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_1:;
	    MELT_LOCATION ("warmelt-modes.melt:3019:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V31*/ meltfptr[30] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.NAMED_NAME__V23*/ meltfptr[18]) && melt_magic_discr ((melt_ptr_t) ( /*_.NAMED_NAME__V23*/ meltfptr[18])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V31*/ meltfptr[30] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V31*/ meltfptr[30] =
		      /*_.NAMED_NAME__V23*/ meltfptr[18];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3020:/ objgoto");
		  /*objgoto */ goto mtch1_2;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3019:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3171:/ objgoto");
		  /*objgoto */ goto mtch1_10;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_2:;
	    MELT_LOCATION ("warmelt-modes.melt:3020:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V32*/ meltfptr[18] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPE_CNAME__V24*/ meltfptr[23]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPE_CNAME__V24*/ meltfptr[23])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V32*/ meltfptr[18] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V32*/ meltfptr[18] =
		      /*_.CTYPE_CNAME__V24*/ meltfptr[23];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3021:/ objgoto");
		  /*objgoto */ goto mtch1_3;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3020:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3171:/ objgoto");
		  /*objgoto */ goto mtch1_10;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_3:;
	    MELT_LOCATION ("warmelt-modes.melt:3021:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V33*/ meltfptr[23] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_BOXEDMAGIC__V25*/ meltfptr[24]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_BOXEDMAGIC__V25*/ meltfptr[24])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V33*/ meltfptr[23] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V33*/ meltfptr[23] =
		      /*_.CTYPG_BOXEDMAGIC__V25*/ meltfptr[24];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3022:/ objgoto");
		  /*objgoto */ goto mtch1_4;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3021:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3171:/ objgoto");
		  /*objgoto */ goto mtch1_10;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_4:;
	    MELT_LOCATION ("warmelt-modes.melt:3022:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V34*/ meltfptr[24] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_BOXEDSTRUCT__V26*/ meltfptr[25]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_BOXEDSTRUCT__V26*/ meltfptr[25])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V34*/ meltfptr[24] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V34*/ meltfptr[24] =
		      /*_.CTYPG_BOXEDSTRUCT__V26*/ meltfptr[25];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3024:/ objgoto");
		  /*objgoto */ goto mtch1_5;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3022:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3171:/ objgoto");
		  /*objgoto */ goto mtch1_10;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_5:;
	    MELT_LOCATION ("warmelt-modes.melt:3024:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.NAMED_NAME__V35*/ meltfptr[25] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*normtesterinst */
	      (melt_is_instance_of
	       ((melt_ptr_t) ( /*_.CTYPG_BOXDISCR__V27*/ meltfptr[26]),
		(melt_ptr_t) (( /*!CLASS_DISCRIMINANT */ meltfrout->tabval[3])))))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CTYPG_BOXDISCR__V27*/ meltfptr[26])
		      /*=obj*/ ;
		    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V35*/ meltfptr[25] = slot;
		  };
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3026:/ objgoto");
		  /*objgoto */ goto mtch1_6;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3024:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3171:/ objgoto");
		  /*objgoto */ goto mtch1_10;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_6:;
	    MELT_LOCATION ("warmelt-modes.melt:3026:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V36*/ meltfptr[26] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_BOXFUN__V28*/ meltfptr[27]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_BOXFUN__V28*/ meltfptr[27])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V36*/ meltfptr[26] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V36*/ meltfptr[26] =
		      /*_.CTYPG_BOXFUN__V28*/ meltfptr[27];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3027:/ objgoto");
		  /*objgoto */ goto mtch1_7;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3026:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3171:/ objgoto");
		  /*objgoto */ goto mtch1_10;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_7:;
	    MELT_LOCATION ("warmelt-modes.melt:3027:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V37*/ meltfptr[27] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_UNBOXFUN__V29*/ meltfptr[28]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_UNBOXFUN__V29*/ meltfptr[28])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V37*/ meltfptr[27] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V37*/ meltfptr[27] =
		      /*_.CTYPG_UNBOXFUN__V29*/ meltfptr[28];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3028:/ objgoto");
		  /*objgoto */ goto mtch1_8;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3027:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3171:/ objgoto");
		  /*objgoto */ goto mtch1_10;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_8:;
	    MELT_LOCATION ("warmelt-modes.melt:3028:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V38*/ meltfptr[28] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_UPDATEBOXFUN__V30*/ meltfptr[29]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_UPDATEBOXFUN__V30*/ meltfptr[29])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V38*/ meltfptr[28] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V38*/ meltfptr[28] =
		      /*_.CTYPG_UPDATEBOXFUN__V30*/ meltfptr[29];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3018:/ objgoto");
		  /*objgoto */ goto mtch1_9;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3028:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3171:/ objgoto");
		  /*objgoto */ goto mtch1_10;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_9:;
	    MELT_LOCATION ("warmelt-modes.melt:3018:/ objlabel");
	    ;
	    /*^quasiblock */


	    /*_.BOXDISCR__V39*/ meltfptr[29] =
	      /*_.CTYPG_BOXDISCR__V27*/ meltfptr[26];;
	    /*^compute */
	    /*_.BOXDISCRNAME__V40*/ meltfptr[39] =
	      /*_.NAMED_NAME__V35*/ meltfptr[25];;
	    /*^compute */
	    /*_.BOXEDMAGIC__V41*/ meltfptr[40] = /*_.SV__V33*/ meltfptr[23];;
	    /*^compute */
	    /*_.BOXEDSTRUCT__V42*/ meltfptr[41] = /*_.SV__V34*/ meltfptr[24];;
	    /*^compute */
	    /*_.BOXFUN__V43*/ meltfptr[42] = /*_.SV__V36*/ meltfptr[26];;
	    /*^compute */
	    /*_.CNAME__V44*/ meltfptr[43] = /*_.SV__V32*/ meltfptr[18];;
	    /*^compute */
	    /*_.CTYPNAM__V45*/ meltfptr[44] = /*_.SV__V31*/ meltfptr[30];;
	    /*^compute */
	    /*_.UNBOXFUN__V46*/ meltfptr[45] = /*_.SV__V37*/ meltfptr[27];;
	    /*^compute */
	    /*_.UPDATEBOXFUN__V47*/ meltfptr[46] =
	      /*_.SV__V38*/ meltfptr[28];;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3030:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("/*gtyctype #"));
	    }
	    ;
   /*_#I__L10*/ meltfnum[1] =
	      ((1) + ( /*_#TIX__L6*/ meltfnum[0]));;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3031:/ locexp");
	      meltgc_add_strbuf_dec ((melt_ptr_t)
				     ( /*_.DECLBUF__V4*/ meltfptr[3]),
				     ( /*_#I__L10*/ meltfnum[1]));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3032:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]), (" "));
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3033:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						( /*_.CURCTYP__V16*/
						 meltfptr[14]),
						(melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[4])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.NAMED_NAME__V48*/ meltfptr[47] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

    /*_.NAMED_NAME__V48*/ meltfptr[47] = NULL;;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3033:/ locexp");
	      meltgc_add_strbuf_ccomment ((melt_ptr_t)
					  ( /*_.DECLBUF__V4*/ meltfptr[3]),
					  melt_string_str ((melt_ptr_t)
							   ( /*_.NAMED_NAME__V48*/ meltfptr[47])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3034:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]), ("*/"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3035:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.DECLBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3036:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("melt_ptr_t "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3037:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXFUN__V43*/
						     meltfptr[42])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3038:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("(meltobject_ptr_t discr, "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3039:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.CNAME__V44*/
						     meltfptr[43])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3040:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   (" val);"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3041:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.DECLBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3042:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("void "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3043:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.UPDATEBOXFUN__V47*/
						     meltfptr[46])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3044:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("(melt_ptr_t boxp, "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3045:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.CNAME__V44*/
						     meltfptr[43])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3046:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   (" val);"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3047:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.DECLBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3048:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.DECLBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3049:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("static inline "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3050:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.CNAME__V44*/
						     meltfptr[43])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3051:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]), (" "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3052:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.UNBOXFUN__V46*/
						     meltfptr[45])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3053:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("(melt_ptr_t box_p) {"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3054:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.DECLBUF__V4*/ meltfptr[3]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3055:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("if (melt_magic_discr(box_p) == "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3056:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXEDMAGIC__V41*/
						     meltfptr[40])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3057:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]), (")"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3058:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.DECLBUF__V4*/ meltfptr[3]), (2),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3059:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("return ((struct "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3060:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXEDSTRUCT__V42*/
						     meltfptr[41])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3061:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("*)box_p)->val;"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3062:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.DECLBUF__V4*/ meltfptr[3]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3063:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("return ("));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3064:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.CNAME__V44*/
						     meltfptr[43])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3065:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]), (")0;"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3066:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.DECLBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3067:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   ("} /* end generated "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3068:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.UNBOXFUN__V46*/
						     meltfptr[45])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3069:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.DECLBUF__V4*/ meltfptr[3]), (" */"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3070:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.DECLBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3071:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.DECLBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3073:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("/*gtyctype #"));
	    }
	    ;
   /*_#I__L11*/ meltfnum[7] =
	      ((1) + ( /*_#TIX__L6*/ meltfnum[0]));;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3074:/ locexp");
	      meltgc_add_strbuf_dec ((melt_ptr_t)
				     ( /*_.CODEBUF__V5*/ meltfptr[4]),
				     ( /*_#I__L11*/ meltfnum[7]));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3075:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]), (" "));
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3076:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						( /*_.CURCTYP__V16*/
						 meltfptr[14]),
						(melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[4])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.NAMED_NAME__V49*/ meltfptr[48] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

    /*_.NAMED_NAME__V49*/ meltfptr[48] = NULL;;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3076:/ locexp");
	      meltgc_add_strbuf_ccomment ((melt_ptr_t)
					  ( /*_.CODEBUF__V5*/ meltfptr[4]),
					  melt_string_str ((melt_ptr_t)
							   ( /*_.NAMED_NAME__V49*/ meltfptr[48])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3077:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]), ("*/"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3078:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3079:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3080:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("melt_ptr_t"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3081:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3082:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXFUN__V43*/
						     meltfptr[42])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3083:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("(meltobject_ptr_t discr_p, "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3084:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.CNAME__V44*/
						     meltfptr[43])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3085:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   (" val) { /*generated boxingfun*/"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3086:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3087:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("MELT_ENTERFRAME (2, NULL);"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3088:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3089:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("#define resv meltfram__.mcfr_varptr[0]"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3091:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3092:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("#define discrv meltfram__.mcfr_varptr[1]"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3094:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3095:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("discrv = discr_p;"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3097:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3098:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("if (!discrv) discrv = MELT_PREDEF("));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3100:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXDISCRNAME__V40*/
						     meltfptr[39])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3101:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]), (");"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3102:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3103:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("if (melt_magic_discr((melt_ptr_t)discrv) != MELTOBMAG_OBJECT) goto\
 end;"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3105:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3106:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("if (((meltobject_ptr_t)(discrv))->meltobj_magic != "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3108:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXEDMAGIC__V41*/
						     meltfptr[40])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3109:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   (") goto end;"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3110:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3111:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("resv = meltgc_allocate (sizeof (struct "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3112:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXEDSTRUCT__V42*/
						     meltfptr[41])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3113:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("), 0);"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3114:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3115:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("((struct "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3116:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXEDSTRUCT__V42*/
						     meltfptr[41])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3117:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("*) (resv))->discr = (meltobject_ptr_t)discrv;"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3118:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3119:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("((struct "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3120:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXEDSTRUCT__V42*/
						     meltfptr[41])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3121:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("*) (resv))->val = val;"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3122:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3123:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("end: MELT_EXITFRAME ();"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3124:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3125:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("return ((melt_ptr_t)(resv));"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3126:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3127:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("#undef resv"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3128:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3129:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("#undef discrv"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3130:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3131:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("} /* end generated boxingfun "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3132:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXFUN__V43*/
						     meltfptr[42])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3133:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]), (" */"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3134:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3135:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3136:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("void"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3137:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3138:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.UPDATEBOXFUN__V47*/
						     meltfptr[46])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3139:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("(melt_ptr_t box_p, "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3140:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.CNAME__V44*/
						     meltfptr[43])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3141:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   (" val) { /*generated updateboxfun */"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3142:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3143:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("MELT_ENTERFRAME (1, NULL);"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3144:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3145:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("#define boxv meltfram__.mcfr_varptr[0]"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3147:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3148:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("boxv = box_p;"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3149:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3150:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("if (melt_magic_discr((melt_ptr_t)boxv) != "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3152:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXEDMAGIC__V41*/
						     meltfptr[40])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3153:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   (") goto end;"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3154:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3155:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("((struct "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3156:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.BOXEDSTRUCT__V42*/
						     meltfptr[41])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3157:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("*) (boxv))->val = val;"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3158:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (1),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3159:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("meltgc_touch ((melt_ptr_t)boxv);"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3160:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3161:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("end: MELT_EXITFRAME ();"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3162:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3163:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("#undef boxv"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3164:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3165:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   ("} /* end generated updateboxfun "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3166:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.UPDATEBOXFUN__V47*/
						     meltfptr[46])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3167:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.CODEBUF__V5*/ meltfptr[4]), (" */"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3168:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3169:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.CODEBUF__V5*/ meltfptr[4]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3018:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;

	    /*^clear */
	     /*clear *//*_.BOXDISCR__V39*/ meltfptr[29] = 0;
	    /*^clear */
	     /*clear *//*_.BOXDISCRNAME__V40*/ meltfptr[39] = 0;
	    /*^clear */
	     /*clear *//*_.BOXEDMAGIC__V41*/ meltfptr[40] = 0;
	    /*^clear */
	     /*clear *//*_.BOXEDSTRUCT__V42*/ meltfptr[41] = 0;
	    /*^clear */
	     /*clear *//*_.BOXFUN__V43*/ meltfptr[42] = 0;
	    /*^clear */
	     /*clear *//*_.CNAME__V44*/ meltfptr[43] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPNAM__V45*/ meltfptr[44] = 0;
	    /*^clear */
	     /*clear *//*_.UNBOXFUN__V46*/ meltfptr[45] = 0;
	    /*^clear */
	     /*clear *//*_.UPDATEBOXFUN__V47*/ meltfptr[46] = 0;
	    /*^clear */
	     /*clear *//*_#I__L10*/ meltfnum[1] = 0;
	    /*^clear */
	     /*clear *//*_.NAMED_NAME__V48*/ meltfptr[47] = 0;
	    /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[7] = 0;
	    /*^clear */
	     /*clear *//*_.NAMED_NAME__V49*/ meltfptr[48] = 0;
	    /*^objgoto */
	    /*objgoto */ goto mtch1__end /*endmatch */ ;
	    ;

	  /*objlabel */ mtch1_10:;
	    MELT_LOCATION ("warmelt-modes.melt:3171:/ objlabel");
	    ;
	    /*^quasiblock */


	    MELT_LOCATION ("warmelt-modes.melt:3173:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						( /*_.CURCTYP__V16*/
						 meltfptr[14]),
						(melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[4])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.CURCTYP__V16*/ meltfptr[14]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.NAMED_NAME__V50*/ meltfptr[29] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

    /*_.NAMED_NAME__V50*/ meltfptr[29] = NULL;;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3172:/ locexp");
	      error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
		     ("invalid GTY-ed ctype for boxing"),
		     melt_string_str ((melt_ptr_t)
				      ( /*_.NAMED_NAME__V50*/ meltfptr[29])));
	    }
	    ;

#if MELT_HAVE_DEBUG
	    MELT_LOCATION ("warmelt-modes.melt:3174:/ cppif.then");
	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^cond */
	      /*cond */ if (( /*nil */ NULL))	/*then */
		{
		  /*^cond.then */
		  /*_.IFELSE___V52*/ meltfptr[40] = ( /*nil */ NULL);;
		}
	      else
		{
		  MELT_LOCATION ("warmelt-modes.melt:3174:/ cond.else");

		  /*^block */
		  /*anyblock */
		  {




		    {
		      /*^locexp */
		      melt_assert_failed (("invalid curctype"),
					  ("warmelt-modes.melt")
					  ? ("warmelt-modes.melt") : __FILE__,
					  (3174) ? (3174) : __LINE__,
					  __FUNCTION__);
		      ;
		    }
		    ;
		 /*clear *//*_.IFELSE___V52*/ meltfptr[40] = 0;
		    /*epilog */
		  }
		  ;
		}
	      ;
	      /*^compute */
	      /*_.IFCPP___V51*/ meltfptr[39] =
		/*_.IFELSE___V52*/ meltfptr[40];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-modes.melt:3174:/ clear");
	       /*clear *//*_.IFELSE___V52*/ meltfptr[40] = 0;
	    }

#else /*MELT_HAVE_DEBUG */
	    /*^cppif.else */
	    /*_.IFCPP___V51*/ meltfptr[39] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3171:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;

	    /*^clear */
	     /*clear *//*_.NAMED_NAME__V50*/ meltfptr[29] = 0;
	    /*^clear */
	     /*clear *//*_.IFCPP___V51*/ meltfptr[39] = 0;
	    /*^objgoto */
	    /*objgoto */ goto mtch1__end /*endmatch */ ;
	    ;

	  /*objlabel */ mtch1__end:;
	    MELT_LOCATION ("warmelt-modes.melt:3017:/ objlabel");
	    ;
	  }
	  ;
	  if ( /*_#TIX__L6*/ meltfnum[0] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:3012:/ clear");
	    /*clear *//*_.CURCTYP__V16*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_#TIX__L6*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3177:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3178:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3]),
			   ("/** end of declarations generated by generate_runtypesupport_boxingfun **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3180:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3181:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3182:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.CODEBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3183:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.CODEBUF__V5*/ meltfptr[4]),
			   ("/** end of code generated by generate_runtypesupport_boxingfun **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3185:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.CODEBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3186:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.CODEBUF__V5*/ meltfptr[4]),
				(0), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3187:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3187:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3187:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3187;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_boxingfun end outarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V55*/ meltfptr[43] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V54*/ meltfptr[42] =
	      /*_.MELT_DEBUG_FUN__V55*/ meltfptr[43];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3187:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V55*/ meltfptr[43] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V54*/ meltfptr[42] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3187:/ quasiblock");


      /*_.PROGN___V56*/ meltfptr[44] = /*_.IF___V54*/ meltfptr[42];;
      /*^compute */
      /*_.IFCPP___V53*/ meltfptr[41] = /*_.PROGN___V56*/ meltfptr[44];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3187:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V54*/ meltfptr[42] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V56*/ meltfptr[44] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V53*/ meltfptr[41] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:2993:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V53*/ meltfptr[41];;

    {
      MELT_LOCATION ("warmelt-modes.melt:2993:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V53*/ meltfptr[41] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GENERATE_RUNTYPESUPPORT_BOXINGFUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_55_warmelt_modes_GENERATE_RUNTYPESUPPORT_BOXINGFUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_55_warmelt_modes_GENERATE_RUNTYPESUPPORT_BOXINGFUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAPFUN (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_56_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAPFUN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_56_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAPFUN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 81
    melt_ptr_t mcfr_varptr[81];
#define MELTFRAM_NBVARNUM 21
    long mcfr_varnum[21];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_56_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAPFUN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_56_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAPFUN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 81; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_56_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAPFUN nbval 81*/
  meltfram__.mcfr_nbvar = 81 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GENERATE_RUNTYPESUPPORT_MAPFUN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:3193:/ getarg");
 /*_.CTYTUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTARG__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTARG__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3194:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3194:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3194:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3194;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_mapfun start outarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3194:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3194:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3194:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3195:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-modes.melt:3195:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:3195:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctytup is tuple"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (3195) ? (3195) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3195:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3196:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRBUF__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3])) ==
	 MELTOBMAG_STRBUF);;
      MELT_LOCATION ("warmelt-modes.melt:3196:/ cond");
      /*cond */ if ( /*_#IS_STRBUF__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:3196:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check outbuf is buffer"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (3196) ? (3196) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3196:/ clear");
	     /*clear *//*_#IS_STRBUF__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3197:/ locexp");
      if (65000 > 0)
	meltgc_strbuf_reserve ((melt_ptr_t) /*_.OUTBUF__V4*/ meltfptr[3],
			       (unsigned) 65000);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3198:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3199:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3200:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
			   ("/** start of code generated by generate_runtypesupport_mapfun **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3202:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3203:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CTYTUP__V2*/ meltfptr[1]);
      for ( /*_#TIX__L5*/ meltfnum[1] = 0;
	   ( /*_#TIX__L5*/ meltfnum[1] >= 0)
	   && ( /*_#TIX__L5*/ meltfnum[1] < meltcit1__EACHTUP_ln);
	/*_#TIX__L5*/ meltfnum[1]++)
	{
	  /*_.CURCTYP__V13*/ meltfptr[11] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.CTYTUP__V2*/ meltfptr[1]),
			       /*_#TIX__L5*/ meltfnum[1]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:3208:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3208:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:3208:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3208;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "generate runtypesupport_mapfun curctyp=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURCTYP__V13*/ meltfptr[11];
		    /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V15*/ meltfptr[14] =
		    /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:3208:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V15*/ meltfptr[14] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3208:/ quasiblock");


	    /*_.PROGN___V17*/ meltfptr[15] = /*_.IF___V15*/ meltfptr[14];;
	    /*^compute */
	    /*_.IFCPP___V14*/ meltfptr[13] = /*_.PROGN___V17*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3208:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:3209:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L8*/ meltfnum[6] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURCTYP__V13*/ meltfptr[11]),
				   (melt_ptr_t) (( /*!CLASS_CTYPE_GTY */
						  meltfrout->tabval[1])));;
	    MELT_LOCATION ("warmelt-modes.melt:3209:/ cond");
	    /*cond */ if ( /*_#IS_A__L8*/ meltfnum[6])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V19*/ meltfptr[15] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3209:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curctyp"),
					("warmelt-modes.melt")
					? ("warmelt-modes.melt") : __FILE__,
					(3209) ? (3209) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V19*/ meltfptr[15] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V18*/ meltfptr[14] = /*_.IFELSE___V19*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3209:/ clear");
	      /*clear *//*_#IS_A__L8*/ meltfnum[6] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V19*/ meltfptr[15] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V18*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:3210:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
				 ("/*gtyctype #"));
	  }
	  ;
  /*_#I__L9*/ meltfnum[0] =
	    ((1) + ( /*_#TIX__L5*/ meltfnum[1]));;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:3211:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.OUTBUF__V4*/ meltfptr[3]),
				   ( /*_#I__L9*/ meltfnum[0]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:3212:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
				 (" "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:3213:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURCTYP__V13*/
					       meltfptr[11]),
					      (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V20*/ meltfptr[15] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.NAMED_NAME__V20*/ meltfptr[15] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:3213:/ locexp");
	    meltgc_add_strbuf_ccomment ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]),
					melt_string_str ((melt_ptr_t)
							 ( /*_.NAMED_NAME__V20*/ meltfptr[15])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:3214:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
				 ("*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:3215:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V4*/ meltfptr[3]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:3216:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^block */
	  /*anyblock */
	  {

	    /*^objgoto */
	    /*objgoto */ goto mtch1_0;
	    ;

	  /*objlabel */ mtch1_0:;
	    MELT_LOCATION ("warmelt-modes.melt:3217:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.NAMED_NAME__V21*/ meltfptr[20] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPE_CNAME__V22*/ meltfptr[21] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPSTRUCT__V23*/ meltfptr[22] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPMAGIC__V24*/ meltfptr[23] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPDISCR__V25*/ meltfptr[24] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPUNIMEMB__V26*/ meltfptr[25] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_NEWMAPFUN__V27*/ meltfptr[26] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPGETFUN__V28*/ meltfptr[27] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPPUTFUN__V29*/ meltfptr[28] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPREMOVEFUN__V30*/ meltfptr[29] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPCOUNTFUN__V31*/ meltfptr[30] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPSIZEFUN__V32*/ meltfptr[31] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPNATTFUN__V33*/ meltfptr[32] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPNVALFUN__V34*/ meltfptr[33] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPAUXDATAFUN__V35*/ meltfptr[34] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPG_MAPAUXPUTFUN__V36*/ meltfptr[35] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*normtesterinst */
	      (melt_is_instance_of
	       ((melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]),
		(melt_ptr_t) (( /*!CLASS_CTYPE_GTY */ meltfrout->tabval[1])))))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V21*/ meltfptr[20] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 3, "CTYPE_CNAME");
      /*_.CTYPE_CNAME__V22*/ meltfptr[21] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 12, "CTYPG_MAPMAGIC");
      /*_.CTYPG_MAPMAGIC__V24*/ meltfptr[23] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 16, "CTYPG_MAPSTRUCT");
      /*_.CTYPG_MAPSTRUCT__V23*/ meltfptr[22] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 18, "CTYPG_MAPDISCR");
      /*_.CTYPG_MAPDISCR__V25*/ meltfptr[24] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 19, "CTYPG_MAPUNIMEMB");
      /*_.CTYPG_MAPUNIMEMB__V26*/ meltfptr[25] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 23, "CTYPG_NEWMAPFUN");
      /*_.CTYPG_NEWMAPFUN__V27*/ meltfptr[26] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 24, "CTYPG_MAPGETFUN");
      /*_.CTYPG_MAPGETFUN__V28*/ meltfptr[27] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 25, "CTYPG_MAPPUTFUN");
      /*_.CTYPG_MAPPUTFUN__V29*/ meltfptr[28] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 26,
					   "CTYPG_MAPREMOVEFUN");
      /*_.CTYPG_MAPREMOVEFUN__V30*/ meltfptr[29] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 27,
					   "CTYPG_MAPCOUNTFUN");
      /*_.CTYPG_MAPCOUNTFUN__V31*/ meltfptr[30] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 28, "CTYPG_MAPSIZEFUN");
      /*_.CTYPG_MAPSIZEFUN__V32*/ meltfptr[31] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 29, "CTYPG_MAPNATTFUN");
      /*_.CTYPG_MAPNATTFUN__V33*/ meltfptr[32] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 30, "CTYPG_MAPNVALFUN");
      /*_.CTYPG_MAPNVALFUN__V34*/ meltfptr[33] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 31,
					   "CTYPG_MAPAUXDATAFUN");
      /*_.CTYPG_MAPAUXDATAFUN__V35*/ meltfptr[34] = slot;
		  };
		  ;
		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CURCTYP__V13*/ meltfptr[11]) /*=obj*/
		      ;
		    melt_object_get_field (slot, obj, 32,
					   "CTYPG_MAPAUXPUTFUN");
      /*_.CTYPG_MAPAUXPUTFUN__V36*/ meltfptr[35] = slot;
		  };
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3218:/ objgoto");
		  /*objgoto */ goto mtch1_1;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3217:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_1:;
	    MELT_LOCATION ("warmelt-modes.melt:3218:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V37*/ meltfptr[36] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.NAMED_NAME__V21*/ meltfptr[20]) && melt_magic_discr ((melt_ptr_t) ( /*_.NAMED_NAME__V21*/ meltfptr[20])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V37*/ meltfptr[36] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V37*/ meltfptr[36] =
		      /*_.NAMED_NAME__V21*/ meltfptr[20];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3219:/ objgoto");
		  /*objgoto */ goto mtch1_2;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3218:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_2:;
	    MELT_LOCATION ("warmelt-modes.melt:3219:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V38*/ meltfptr[20] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPE_CNAME__V22*/ meltfptr[21]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPE_CNAME__V22*/ meltfptr[21])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V38*/ meltfptr[20] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V38*/ meltfptr[20] =
		      /*_.CTYPE_CNAME__V22*/ meltfptr[21];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3220:/ objgoto");
		  /*objgoto */ goto mtch1_3;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3219:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_3:;
	    MELT_LOCATION ("warmelt-modes.melt:3220:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V39*/ meltfptr[21] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPSTRUCT__V23*/ meltfptr[22]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPSTRUCT__V23*/ meltfptr[22])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V39*/ meltfptr[21] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V39*/ meltfptr[21] =
		      /*_.CTYPG_MAPSTRUCT__V23*/ meltfptr[22];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3221:/ objgoto");
		  /*objgoto */ goto mtch1_4;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3220:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_4:;
	    MELT_LOCATION ("warmelt-modes.melt:3221:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V40*/ meltfptr[22] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPMAGIC__V24*/ meltfptr[23]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPMAGIC__V24*/ meltfptr[23])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V40*/ meltfptr[22] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V40*/ meltfptr[22] =
		      /*_.CTYPG_MAPMAGIC__V24*/ meltfptr[23];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3222:/ objgoto");
		  /*objgoto */ goto mtch1_5;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3221:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_5:;
	    MELT_LOCATION ("warmelt-modes.melt:3222:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.NAMED_NAME__V41*/ meltfptr[23] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*normtesterinst */
	      (melt_is_instance_of
	       ((melt_ptr_t) ( /*_.CTYPG_MAPDISCR__V25*/ meltfptr[24]),
		(melt_ptr_t) (( /*!CLASS_DISCRIMINANT */ meltfrout->tabval[4])))))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^getslot */
		  {
		    melt_ptr_t slot = NULL, obj = NULL;
		    obj =
		      (melt_ptr_t) ( /*_.CTYPG_MAPDISCR__V25*/ meltfptr[24])
		      /*=obj*/ ;
		    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V41*/ meltfptr[23] = slot;
		  };
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3224:/ objgoto");
		  /*objgoto */ goto mtch1_6;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3222:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_6:;
	    MELT_LOCATION ("warmelt-modes.melt:3224:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V42*/ meltfptr[24] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPUNIMEMB__V26*/ meltfptr[25]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPUNIMEMB__V26*/ meltfptr[25])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V42*/ meltfptr[24] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V42*/ meltfptr[24] =
		      /*_.CTYPG_MAPUNIMEMB__V26*/ meltfptr[25];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3225:/ objgoto");
		  /*objgoto */ goto mtch1_7;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3224:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_7:;
	    MELT_LOCATION ("warmelt-modes.melt:3225:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V43*/ meltfptr[25] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_NEWMAPFUN__V27*/ meltfptr[26]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_NEWMAPFUN__V27*/ meltfptr[26])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V43*/ meltfptr[25] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V43*/ meltfptr[25] =
		      /*_.CTYPG_NEWMAPFUN__V27*/ meltfptr[26];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3226:/ objgoto");
		  /*objgoto */ goto mtch1_8;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3225:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_8:;
	    MELT_LOCATION ("warmelt-modes.melt:3226:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V44*/ meltfptr[26] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPGETFUN__V28*/ meltfptr[27]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPGETFUN__V28*/ meltfptr[27])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V44*/ meltfptr[26] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V44*/ meltfptr[26] =
		      /*_.CTYPG_MAPGETFUN__V28*/ meltfptr[27];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3227:/ objgoto");
		  /*objgoto */ goto mtch1_9;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3226:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_9:;
	    MELT_LOCATION ("warmelt-modes.melt:3227:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V45*/ meltfptr[27] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPPUTFUN__V29*/ meltfptr[28]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPPUTFUN__V29*/ meltfptr[28])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V45*/ meltfptr[27] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V45*/ meltfptr[27] =
		      /*_.CTYPG_MAPPUTFUN__V29*/ meltfptr[28];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3228:/ objgoto");
		  /*objgoto */ goto mtch1_10;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3227:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_10:;
	    MELT_LOCATION ("warmelt-modes.melt:3228:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V46*/ meltfptr[28] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPREMOVEFUN__V30*/ meltfptr[29]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPREMOVEFUN__V30*/ meltfptr[29])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V46*/ meltfptr[28] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V46*/ meltfptr[28] =
		      /*_.CTYPG_MAPREMOVEFUN__V30*/ meltfptr[29];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3229:/ objgoto");
		  /*objgoto */ goto mtch1_11;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3228:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_11:;
	    MELT_LOCATION ("warmelt-modes.melt:3229:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V47*/ meltfptr[29] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPCOUNTFUN__V31*/ meltfptr[30]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPCOUNTFUN__V31*/ meltfptr[30])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V47*/ meltfptr[29] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V47*/ meltfptr[29] =
		      /*_.CTYPG_MAPCOUNTFUN__V31*/ meltfptr[30];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3230:/ objgoto");
		  /*objgoto */ goto mtch1_12;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3229:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_12:;
	    MELT_LOCATION ("warmelt-modes.melt:3230:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V48*/ meltfptr[30] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPSIZEFUN__V32*/ meltfptr[31]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPSIZEFUN__V32*/ meltfptr[31])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V48*/ meltfptr[30] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V48*/ meltfptr[30] =
		      /*_.CTYPG_MAPSIZEFUN__V32*/ meltfptr[31];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3231:/ objgoto");
		  /*objgoto */ goto mtch1_13;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3230:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_13:;
	    MELT_LOCATION ("warmelt-modes.melt:3231:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V49*/ meltfptr[31] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPNATTFUN__V33*/ meltfptr[32]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPNATTFUN__V33*/ meltfptr[32])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V49*/ meltfptr[31] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V49*/ meltfptr[31] =
		      /*_.CTYPG_MAPNATTFUN__V33*/ meltfptr[32];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3232:/ objgoto");
		  /*objgoto */ goto mtch1_14;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3231:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_14:;
	    MELT_LOCATION ("warmelt-modes.melt:3232:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V50*/ meltfptr[32] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPNVALFUN__V34*/ meltfptr[33]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPNVALFUN__V34*/ meltfptr[33])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V50*/ meltfptr[32] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V50*/ meltfptr[32] =
		      /*_.CTYPG_MAPNVALFUN__V34*/ meltfptr[33];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3233:/ objgoto");
		  /*objgoto */ goto mtch1_15;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3232:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_15:;
	    MELT_LOCATION ("warmelt-modes.melt:3233:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V51*/ meltfptr[33] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPAUXDATAFUN__V35*/ meltfptr[34]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPAUXDATAFUN__V35*/ meltfptr[34])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V51*/ meltfptr[33] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V51*/ meltfptr[33] =
		      /*_.CTYPG_MAPAUXDATAFUN__V35*/ meltfptr[34];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3234:/ objgoto");
		  /*objgoto */ goto mtch1_16;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3233:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_16:;
	    MELT_LOCATION ("warmelt-modes.melt:3234:/ objlabel");
	    ;
	    /*^clear */
	     /*clear *//*_.SV__V52*/ meltfptr[34] = 0;
	    /*^cond */
	    /*cond */ if (
			   /*SOMESTRVAL_mtch1__1? */ (( /*_.CTYPG_MAPAUXPUTFUN__V36*/ meltfptr[35]) && melt_magic_discr ((melt_ptr_t) ( /*_.CTYPG_MAPAUXPUTFUN__V36*/ meltfptr[35])) == MELTOBMAG_STRING))	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^clear */
	       /*clear *//*_.SV__V52*/ meltfptr[34] = 0;

		  {
		    /*^locexp */
		    /*SOMESTRVAL_mtch1__1! *//*_.SV__V52*/ meltfptr[34] =
		      /*_.CTYPG_MAPAUXPUTFUN__V36*/ meltfptr[35];;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3217:/ objgoto");
		  /*objgoto */ goto mtch1_17;
		  ;
		}
		;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:3234:/ cond.else");

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
		  /*objgoto */ goto mtch1_18;
		  ;
		}
		;
	      }
	    ;

	  /*objlabel */ mtch1_17:;
	    MELT_LOCATION ("warmelt-modes.melt:3217:/ objlabel");
	    ;
	    /*^quasiblock */


	    /*_.CNAME__V53*/ meltfptr[35] = /*_.SV__V38*/ meltfptr[20];;
	    /*^compute */
	    /*_.CTYPNAM__V54*/ meltfptr[53] = /*_.SV__V37*/ meltfptr[36];;
	    /*^compute */
	    /*_.MAPAUXDATAFUN__V55*/ meltfptr[54] =
	      /*_.SV__V51*/ meltfptr[33];;
	    /*^compute */
	    /*_.MAPAUXPUTFUN__V56*/ meltfptr[55] =
	      /*_.SV__V52*/ meltfptr[34];;
	    /*^compute */
	    /*_.MAPCOUNTFUN__V57*/ meltfptr[56] = /*_.SV__V47*/ meltfptr[29];;
	    /*^compute */
	    /*_.MAPDISCR__V58*/ meltfptr[57] =
	      /*_.CTYPG_MAPDISCR__V25*/ meltfptr[24];;
	    /*^compute */
	    /*_.MAPDISCRNAME__V59*/ meltfptr[58] =
	      /*_.NAMED_NAME__V41*/ meltfptr[23];;
	    /*^compute */
	    /*_.MAPGETFUN__V60*/ meltfptr[59] = /*_.SV__V44*/ meltfptr[26];;
	    /*^compute */
	    /*_.MAPMAGIC__V61*/ meltfptr[60] = /*_.SV__V40*/ meltfptr[22];;
	    /*^compute */
	    /*_.MAPNATTFUN__V62*/ meltfptr[61] = /*_.SV__V49*/ meltfptr[31];;
	    /*^compute */
	    /*_.MAPNVALFUN__V63*/ meltfptr[62] = /*_.SV__V50*/ meltfptr[32];;
	    /*^compute */
	    /*_.MAPPUTFUN__V64*/ meltfptr[63] = /*_.SV__V45*/ meltfptr[27];;
	    /*^compute */
	    /*_.MAPREMOVEFUN__V65*/ meltfptr[64] =
	      /*_.SV__V46*/ meltfptr[28];;
	    /*^compute */
	    /*_.MAPSIZEFUN__V66*/ meltfptr[65] = /*_.SV__V48*/ meltfptr[30];;
	    /*^compute */
	    /*_.MAPSTRUCT__V67*/ meltfptr[66] = /*_.SV__V39*/ meltfptr[21];;
	    /*^compute */
	    /*_.MAPUNIMEMB__V68*/ meltfptr[67] = /*_.SV__V42*/ meltfptr[24];;
	    /*^compute */
	    /*_.NEWMAPFUN__V69*/ meltfptr[68] = /*_.SV__V43*/ meltfptr[25];;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3236:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3237:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.OUTBUF__V4*/ meltfptr[3]),
				   ("/***map support for GTY ctype "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3238:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.OUTBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.CTYPNAM__V54*/
						     meltfptr[53])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3239:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.OUTBUF__V4*/ meltfptr[3]), (" **/"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3240:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3244:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#IS_STRING__L10*/ meltfnum[6] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.NEWMAPFUN__V69*/ meltfptr[68])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-modes.melt:3244:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L10*/ meltfnum[6])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3245:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("static inline melt_ptr_t /*New map for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3246:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3247:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/ "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3248:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.NEWMAPFUN__V69*/ meltfptr[68])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3249:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" (meltobject_ptr_t discr, unsigned len) {"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3250:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3251:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  (( /*!konst_5 */
							    meltfrout->
							    tabval[5]))));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3256:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPMAGIC__V61*/ meltfptr[60])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3257:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  (( /*!konst_6 */
							    meltfrout->
							    tabval[6]))));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3261:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3262:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" */"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3263:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (0), 0);
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3244:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    else
	      {			/*^cond.else */

		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3266:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("/* no new map function */"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3265:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3269:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3273:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#IS_STRING__L11*/ meltfnum[10] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.MAPGETFUN__V60*/ meltfptr[59])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-modes.melt:3273:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L11*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3274:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("static inline melt_ptr_t /* Map getter for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3275:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3276:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/ "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3277:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPGETFUN__V60*/ meltfptr[59])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3278:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" (melt_ptr_t map_p, "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3279:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CNAME__V53*/
							   meltfptr[35])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3280:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" attr) {"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3281:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3282:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("if (!map_p || !attr "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3283:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3284:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" || melt_magic_discr ((melt_ptr_t) map_p) != "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3285:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPMAGIC__V61*/ meltfptr[60])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3286:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (")"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3287:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (4), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3288:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return NULL;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3289:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3290:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return melt_raw_get_mappointers ((void*)map_p, (void*)attr);"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3291:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (0), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3292:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("} /*end generated map getter for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3293:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3294:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3295:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (0), 0);
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3273:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    else
	      {			/*^cond.else */

		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3298:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("/*no map getter function*/"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3297:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3301:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3305:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#IS_STRING__L12*/ meltfnum[11] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.MAPPUTFUN__V64*/ meltfptr[63])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-modes.melt:3305:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L12*/ meltfnum[11])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3306:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("static inline void /* Map putter for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3307:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3308:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/ "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3309:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPPUTFUN__V64*/ meltfptr[63])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3310:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" (melt_ptr_t map_p, "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3311:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CNAME__V53*/
							   meltfptr[35])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3312:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" attr, melt_ptr_t valu_p) {"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3313:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3314:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("if (!map_p || !attr || !valu_p"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3315:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3316:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" || melt_magic_discr ((melt_ptr_t) map_p) != "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3317:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPMAGIC__V61*/ meltfptr[60])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3318:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (")"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3319:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (4), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3320:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3321:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3322:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("meltgc_raw_put_mappointers ((void*)map_p, (void*)attr, valu_p);"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3323:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (0), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3324:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("} /*end generated map putter for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3325:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3326:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3305:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    else
	      {			/*^cond.else */

		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3329:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("/*no map putter*/"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3328:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3331:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3335:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#IS_STRING__L13*/ meltfnum[12] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.MAPREMOVEFUN__V65*/ meltfptr[64])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-modes.melt:3335:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L13*/ meltfnum[12])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3336:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("static inline void /* Map remover for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3337:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3338:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/ "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3339:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPREMOVEFUN__V65*/ meltfptr[64])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3340:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" (melt_ptr_t map_p, "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3341:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CNAME__V53*/
							   meltfptr[35])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3342:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" attr) {"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3343:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3344:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("if (!map_p || !attr"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3345:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3346:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" || melt_magic_discr ((melt_ptr_t) map_p) != "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3347:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPMAGIC__V61*/ meltfptr[60])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3348:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (")"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3349:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (4), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3350:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3351:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3352:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("meltgc_raw_remove_mappointers ((void*)map_p, (void*)attr);"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3353:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (0), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3354:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("} /*end generated map remover for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3355:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3356:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3357:/ locexp");
		    /*void */ (void) 0;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3335:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    else
	      {			/*^cond.else */

		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3360:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("/*no map remover*/"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3359:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3363:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3367:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#IS_STRING__L14*/ meltfnum[13] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.MAPCOUNTFUN__V57*/ meltfptr[56])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-modes.melt:3367:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L14*/ meltfnum[13])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3368:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("static inline unsigned /* Map counter for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3369:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3370:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/ "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3371:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPCOUNTFUN__V57*/ meltfptr[56])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3372:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" (struct "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3373:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPSTRUCT__V67*/ meltfptr[66])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3374:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("* map_s) {"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3375:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3376:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("if (!map_s"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3377:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3378:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" || melt_magic_discr ((melt_ptr_t) map_s) != "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3379:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPMAGIC__V61*/ meltfptr[60])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3380:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (")"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3381:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (4), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3382:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return 0;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3383:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3384:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return map_s->count;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3385:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (0), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3386:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("} /*end generated map counter for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3387:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3388:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3389:/ locexp");
		    /*void */ (void) 0;
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3367:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    else
	      {			/*^cond.else */

		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3392:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("/*no map counter function*/"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3391:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3395:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3399:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#IS_STRING__L15*/ meltfnum[14] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.MAPSIZEFUN__V66*/ meltfptr[65])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-modes.melt:3399:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L15*/ meltfnum[14])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3400:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("static inline unsigned /* Map size for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3401:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3402:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/ "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3403:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPSIZEFUN__V66*/ meltfptr[65])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3404:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" (struct "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3405:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPSTRUCT__V67*/ meltfptr[66])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3406:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("* map_s) {"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3407:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3408:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("if (!map_s"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3409:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3410:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" || melt_magic_discr ((melt_ptr_t) map_s) != "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3411:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPMAGIC__V61*/ meltfptr[60])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3412:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (")"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3413:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (4), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3414:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return 0;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3415:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3416:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return melt_primtab[map_s->lenix];"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3417:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (0), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3418:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("} /*end generated map size for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3419:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3420:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3399:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    else
	      {			/*^cond.else */

		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3423:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("/* no map size function */"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3422:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3425:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3430:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#IS_STRING__L16*/ meltfnum[15] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.MAPNATTFUN__V62*/ meltfptr[61])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-modes.melt:3430:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L16*/ meltfnum[15])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3431:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("static inline "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3432:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CNAME__V53*/
							   meltfptr[35])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3433:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("/* Map nth attr for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3434:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3435:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/ "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3436:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPNATTFUN__V62*/ meltfptr[61])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3437:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" (struct "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3438:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPSTRUCT__V67*/ meltfptr[66])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3439:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("* map_s, int ix) {"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3440:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3441:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CNAME__V53*/
							   meltfptr[35])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3442:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" at = 0;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3443:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3444:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("if (!map_s"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3445:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3446:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" || melt_magic_discr ((melt_ptr_t) map_s) != "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3447:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPMAGIC__V61*/ meltfptr[60])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3448:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (")"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3449:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (4), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3450:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return 0;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3451:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3452:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("at = map_s->entab[ix].e_at;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3453:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3454:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("if ((void*) at == (void*) HTAB_DELETED_ENTRY) return 0;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3455:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return at;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3456:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (0), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3457:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("} /*end generated map nth attr for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3458:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3459:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3430:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    else
	      {			/*^cond.else */

		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3462:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("/*no map nth attr function*/"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3461:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3464:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3468:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#IS_STRING__L17*/ meltfnum[16] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.MAPNVALFUN__V63*/ meltfptr[62])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-modes.melt:3468:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L17*/ meltfnum[16])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3469:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("static inline melt_ptr_t "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3470:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("/* Map nth value for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3471:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3472:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/ "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3473:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPNVALFUN__V63*/ meltfptr[62])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3474:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" (struct "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3475:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPSTRUCT__V67*/ meltfptr[66])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3476:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("* map_s, int ix) {"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3477:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3478:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CNAME__V53*/
							   meltfptr[35])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3479:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" at = 0;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3480:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3481:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("if (!map_s"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3482:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3483:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (" || melt_magic_discr ((melt_ptr_t) map_s) != "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3484:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.MAPMAGIC__V61*/ meltfptr[60])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3485:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 (")"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3486:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (4), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3487:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return 0;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3488:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3489:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("at = map_s->entab[ix].e_at;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3490:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (2), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3491:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("if ((void*) at == (void*) HTAB_DELETED_ENTRY) return 0;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3492:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("return map_s->entab[ix].e_va;"));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3493:/ locexp");
		    meltgc_strbuf_add_indent ((melt_ptr_t)
					      ( /*_.OUTBUF__V4*/ meltfptr[3]),
					      (0), 0);
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3494:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("} /*end generated map nth value for "));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3495:/ locexp");
		    /*add2sbuf_string */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 melt_string_str ((melt_ptr_t)
							  ( /*_.CTYPNAM__V54*/
							   meltfptr[53])));
		  }
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:3496:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("*/"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3468:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    else
	      {			/*^cond.else */

		/*^block */
		/*anyblock */
		{


		  {
		    MELT_LOCATION ("warmelt-modes.melt:3499:/ locexp");
		    /*add2sbuf_strconst */
		      meltgc_add_strbuf ((melt_ptr_t)
					 ( /*_.OUTBUF__V4*/ meltfptr[3]),
					 ("/*no map nth value function*/"));
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3498:/ quasiblock");


		  /*epilog */
		}
		;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3501:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3505:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#IS_STRING__L18*/ meltfnum[17] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.MAPAUXDATAFUN__V55*/ meltfptr[54])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-modes.melt:3505:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L18*/ meltfnum[17])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3506:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[9];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_cstring =
		      " /* map auxiliary data access for ";
		    /*^apply.arg */
		    argtab[1].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CTYPNAM__V54*/ meltfptr[53];
		    /*^apply.arg */
		    argtab[2].meltbp_cstring =
		      " */\n\t       static inline melt_ptr_t ";
		    /*^apply.arg */
		    argtab[3].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MAPAUXDATAFUN__V55*/ meltfptr[54];
		    /*^apply.arg */
		    argtab[4].meltbp_cstring =
		      " (melt_ptr_t map_p)\n\t       {\n\t         if (melt_magic_discr (map_p\
) == ";
		    /*^apply.arg */
		    argtab[5].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MAPMAGIC__V61*/ meltfptr[60];
		    /*^apply.arg */
		    argtab[6].meltbp_cstring = ")\n\t\t   return ((struct ";
		    /*^apply.arg */
		    argtab[7].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MAPSTRUCT__V67*/ meltfptr[66];
		    /*^apply.arg */
		    argtab[8].meltbp_cstring =
		      "*)map_p)->meltmap_aux;\n\t\t return NULL;\
\n\t       }\n\t       ";
		    /*_.ADD2OUT__V71*/ meltfptr[70] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!ADD2OUT */ meltfrout->tabval[7])),
				  (melt_ptr_t) ( /*_.OUTBUF__V4*/
						meltfptr[3]),
				  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IFELSE___V70*/ meltfptr[69] =
		    /*_.ADD2OUT__V71*/ meltfptr[70];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:3505:/ clear");
	       /*clear *//*_.ADD2OUT__V71*/ meltfptr[70] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3518:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[3];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_cstring =
		      "/*no map auxiliary data access function for ";
		    /*^apply.arg */
		    argtab[1].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CTYPNAM__V54*/ meltfptr[53];
		    /*^apply.arg */
		    argtab[2].meltbp_cstring = "*/";
		    /*_.ADD2OUT__V72*/ meltfptr[70] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!ADD2OUT */ meltfrout->tabval[7])),
				  (melt_ptr_t) ( /*_.OUTBUF__V4*/
						meltfptr[3]),
				  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3517:/ quasiblock");


		  /*_.PROGN___V73*/ meltfptr[72] =
		    /*_.ADD2OUT__V72*/ meltfptr[70];;
		  /*^compute */
		  /*_.IFELSE___V70*/ meltfptr[69] =
		    /*_.PROGN___V73*/ meltfptr[72];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:3505:/ clear");
	       /*clear *//*_.ADD2OUT__V72*/ meltfptr[70] = 0;
		  /*^clear */
	       /*clear *//*_.PROGN___V73*/ meltfptr[72] = 0;
		}
		;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3520:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3524:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#IS_STRING__L19*/ meltfnum[18] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.MAPAUXPUTFUN__V56*/ meltfptr[55])) ==
	       MELTOBMAG_STRING);;
	    MELT_LOCATION ("warmelt-modes.melt:3524:/ cond");
	    /*cond */ if ( /*_#IS_STRING__L19*/ meltfnum[18])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3525:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[9];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_cstring =
		      " /* map auxiliary data put for ";
		    /*^apply.arg */
		    argtab[1].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CTYPNAM__V54*/ meltfptr[53];
		    /*^apply.arg */
		    argtab[2].meltbp_cstring =
		      " */\n\t       static inline melt_ptr_t ";
		    /*^apply.arg */
		    argtab[3].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MAPAUXPUTFUN__V56*/ meltfptr[55];
		    /*^apply.arg */
		    argtab[4].meltbp_cstring =
		      " (melt_ptr_t map_p, melt_ptr_t val_p)\
\n\t       {\n\t         if (melt_magic_discr (map_p) == ";
		    /*^apply.arg */
		    argtab[5].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MAPMAGIC__V61*/ meltfptr[60];
		    /*^apply.arg */
		    argtab[6].meltbp_cstring = ") {\n\t\t   ((struct ";
		    /*^apply.arg */
		    argtab[7].meltbp_aptr =
		      (melt_ptr_t *) & /*_.MAPSTRUCT__V67*/ meltfptr[66];
		    /*^apply.arg */
		    argtab[8].meltbp_cstring =
		      "*)map_p)->meltmap_aux = val_p;\n\t\t   meltgc_touch_dest (map_p, val_p\
);\n                 }\n\t\t return NULL;\
\n\t       }\n\t \
      ";
		    /*_.ADD2OUT__V75*/ meltfptr[72] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!ADD2OUT */ meltfrout->tabval[7])),
				  (melt_ptr_t) ( /*_.OUTBUF__V4*/
						meltfptr[3]),
				  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IFELSE___V74*/ meltfptr[70] =
		    /*_.ADD2OUT__V75*/ meltfptr[72];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:3524:/ clear");
	       /*clear *//*_.ADD2OUT__V75*/ meltfptr[72] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

		/*^block */
		/*anyblock */
		{

		  MELT_LOCATION ("warmelt-modes.melt:3539:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[3];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_cstring =
		      "/*no map auxiliary data put function for ";
		    /*^apply.arg */
		    argtab[1].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CTYPNAM__V54*/ meltfptr[53];
		    /*^apply.arg */
		    argtab[2].meltbp_cstring = "*/";
		    /*_.ADD2OUT__V76*/ meltfptr[72] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!ADD2OUT */ meltfrout->tabval[7])),
				  (melt_ptr_t) ( /*_.OUTBUF__V4*/
						meltfptr[3]),
				  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				   MELTBPARSTR_CSTRING ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:3538:/ quasiblock");


		  /*_.PROGN___V77*/ meltfptr[76] =
		    /*_.ADD2OUT__V76*/ meltfptr[72];;
		  /*^compute */
		  /*_.IFELSE___V74*/ meltfptr[70] =
		    /*_.PROGN___V77*/ meltfptr[76];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:3524:/ clear");
	       /*clear *//*_.ADD2OUT__V76*/ meltfptr[72] = 0;
		  /*^clear */
	       /*clear *//*_.PROGN___V77*/ meltfptr[76] = 0;
		}
		;
	      }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3541:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (0),
					0);
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3546:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.OUTBUF__V4*/ meltfptr[3]),
				   ("/***end of map support for GTY ctype "));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3547:/ locexp");
	      /*add2sbuf_string */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.OUTBUF__V4*/ meltfptr[3]),
				   melt_string_str ((melt_ptr_t)
						    ( /*_.CTYPNAM__V54*/
						     meltfptr[53])));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3548:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.OUTBUF__V4*/ meltfptr[3]), (" **/"));
	    }
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:3549:/ locexp");
	      meltgc_strbuf_add_indent ((melt_ptr_t)
					( /*_.OUTBUF__V4*/ meltfptr[3]), (1),
					0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3217:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;

	    /*^clear */
	     /*clear *//*_.CNAME__V53*/ meltfptr[35] = 0;
	    /*^clear */
	     /*clear *//*_.CTYPNAM__V54*/ meltfptr[53] = 0;
	    /*^clear */
	     /*clear *//*_.MAPAUXDATAFUN__V55*/ meltfptr[54] = 0;
	    /*^clear */
	     /*clear *//*_.MAPAUXPUTFUN__V56*/ meltfptr[55] = 0;
	    /*^clear */
	     /*clear *//*_.MAPCOUNTFUN__V57*/ meltfptr[56] = 0;
	    /*^clear */
	     /*clear *//*_.MAPDISCR__V58*/ meltfptr[57] = 0;
	    /*^clear */
	     /*clear *//*_.MAPDISCRNAME__V59*/ meltfptr[58] = 0;
	    /*^clear */
	     /*clear *//*_.MAPGETFUN__V60*/ meltfptr[59] = 0;
	    /*^clear */
	     /*clear *//*_.MAPMAGIC__V61*/ meltfptr[60] = 0;
	    /*^clear */
	     /*clear *//*_.MAPNATTFUN__V62*/ meltfptr[61] = 0;
	    /*^clear */
	     /*clear *//*_.MAPNVALFUN__V63*/ meltfptr[62] = 0;
	    /*^clear */
	     /*clear *//*_.MAPPUTFUN__V64*/ meltfptr[63] = 0;
	    /*^clear */
	     /*clear *//*_.MAPREMOVEFUN__V65*/ meltfptr[64] = 0;
	    /*^clear */
	     /*clear *//*_.MAPSIZEFUN__V66*/ meltfptr[65] = 0;
	    /*^clear */
	     /*clear *//*_.MAPSTRUCT__V67*/ meltfptr[66] = 0;
	    /*^clear */
	     /*clear *//*_.MAPUNIMEMB__V68*/ meltfptr[67] = 0;
	    /*^clear */
	     /*clear *//*_.NEWMAPFUN__V69*/ meltfptr[68] = 0;
	    /*^clear */
	     /*clear *//*_#IS_STRING__L10*/ meltfnum[6] = 0;
	    /*^clear */
	     /*clear *//*_#IS_STRING__L11*/ meltfnum[10] = 0;
	    /*^clear */
	     /*clear *//*_#IS_STRING__L12*/ meltfnum[11] = 0;
	    /*^clear */
	     /*clear *//*_#IS_STRING__L13*/ meltfnum[12] = 0;
	    /*^clear */
	     /*clear *//*_#IS_STRING__L14*/ meltfnum[13] = 0;
	    /*^clear */
	     /*clear *//*_#IS_STRING__L15*/ meltfnum[14] = 0;
	    /*^clear */
	     /*clear *//*_#IS_STRING__L16*/ meltfnum[15] = 0;
	    /*^clear */
	     /*clear *//*_#IS_STRING__L17*/ meltfnum[16] = 0;
	    /*^clear */
	     /*clear *//*_#IS_STRING__L18*/ meltfnum[17] = 0;
	    /*^clear */
	     /*clear *//*_.IFELSE___V70*/ meltfptr[69] = 0;
	    /*^clear */
	     /*clear *//*_#IS_STRING__L19*/ meltfnum[18] = 0;
	    /*^clear */
	     /*clear *//*_.IFELSE___V74*/ meltfptr[70] = 0;
	    /*^objgoto */
	    /*objgoto */ goto mtch1__end /*endmatch */ ;
	    ;

	  /*objlabel */ mtch1_18:;
	    MELT_LOCATION ("warmelt-modes.melt:3551:/ objlabel");
	    ;
	    /*^quasiblock */



	    {
	      MELT_LOCATION ("warmelt-modes.melt:3552:/ locexp");
	      /*add2sbuf_strconst */
		meltgc_add_strbuf ((melt_ptr_t)
				   ( /*_.OUTBUF__V4*/ meltfptr[3]),
				   ("/*incomplete gtypctype*/"));
	    }
	    ;

	    MELT_LOCATION ("warmelt-modes.melt:3551:/ objgoto");
	    /*objgoto */ goto mtch1__end /*endmatch */ ;
	    ;

	  /*objlabel */ mtch1__end:;
	    MELT_LOCATION ("warmelt-modes.melt:3216:/ objlabel");
	    ;
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:3555:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.OUTBUF__V4*/ meltfptr[3]), (1),
				      0);
	  }
	  ;
	  if ( /*_#TIX__L5*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:3205:/ clear");
	    /*clear *//*_.CURCTYP__V13*/ meltfptr[11] = 0;
      /*^clear */
	    /*clear *//*_#TIX__L5*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V18*/ meltfptr[14] = 0;
      /*^clear */
	    /*clear *//*_#I__L9*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V20*/ meltfptr[15] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3558:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
			   ("/** end of code generated by generate_runtypesupport_mapfun **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3560:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3561:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3562:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L20*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3562:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L20*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3562:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L21*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3562;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_mapfun end outarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V80*/ meltfptr[35] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V79*/ meltfptr[76] =
	      /*_.MELT_DEBUG_FUN__V80*/ meltfptr[35];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3562:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L21*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V80*/ meltfptr[35] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V79*/ meltfptr[76] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3562:/ quasiblock");


      /*_.PROGN___V81*/ meltfptr[53] = /*_.IF___V79*/ meltfptr[76];;
      /*^compute */
      /*_.IFCPP___V78*/ meltfptr[72] = /*_.PROGN___V81*/ meltfptr[53];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3562:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L20*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V79*/ meltfptr[76] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V81*/ meltfptr[53] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V78*/ meltfptr[72] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3193:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V78*/ meltfptr[72];;

    {
      MELT_LOCATION ("warmelt-modes.melt:3193:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V78*/ meltfptr[72] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GENERATE_RUNTYPESUPPORT_MAPFUN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_56_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAPFUN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_56_warmelt_modes_GENERATE_RUNTYPESUPPORT_MAPFUN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_modes_GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_57_warmelt_modes_GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_57_warmelt_modes_GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 32
    melt_ptr_t mcfr_varptr[32];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_57_warmelt_modes_GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_57_warmelt_modes_GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 32; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_57_warmelt_modes_GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS nbval 32*/
  meltfram__.mcfr_nbvar = 32 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:3570:/ getarg");
 /*_.OUTBUF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3571:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3571:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3571:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3571;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_predef_fields start outbuf=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTBUF__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3571:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3571:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3571:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3572:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OUT__L3*/ meltfnum[1] =
	(melt_is_out ((melt_ptr_t) /*_.OUTBUF__V2*/ meltfptr[1]));;
      MELT_LOCATION ("warmelt-modes.melt:3572:/ cond");
      /*cond */ if ( /*_#IS_OUT__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:3572:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check outbuf"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (3572) ? (3572) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[3] = /*_.IFELSE___V8*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3572:/ clear");
	     /*clear *//*_#IS_OUT__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3573:/ locexp");
      if (27500 > 0)
	meltgc_strbuf_reserve ((melt_ptr_t) /*_.OUTBUF__V2*/ meltfptr[1],
			       (unsigned) 27500);;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3574:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3575:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3576:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
			   ("/** start of code generated by generate_runtypesupport_predef_fields **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3578:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3579:/ quasiblock");


    /*_.CURPREDEF__V9*/ meltfptr[4] = ( /*nil */ NULL);;
    /*^compute */
 /*_#NBPREDEF__L4*/ meltfnum[0] = 0;;

    {
      MELT_LOCATION ("warmelt-modes.melt:3582:/ locexp");
		       /*GETNBPREDEFCHK__1 *//*_#NBPREDEF__L4*/ meltfnum[0] =
	MELTGLOB__LASTWIRED;;
    }
    ;
    /*citerblock FOREACH_LONG_UPTO */
    {
      /* foreach_long_upto start meltcit1__EACHLONG */
      long meltcit1__EACHLONG_min = 1;
      long meltcit1__EACHLONG_max = /*_#NBPREDEF__L4*/ meltfnum[0];
      long meltcit1__EACHLONG_cur = 0;
      for (meltcit1__EACHLONG_cur = meltcit1__EACHLONG_min;
	   meltcit1__EACHLONG_cur <= meltcit1__EACHLONG_max;
	   meltcit1__EACHLONG_cur++)
	{
	    /*_#PRIX__L5*/ meltfnum[1] = meltcit1__EACHLONG_cur;




	  {
	    MELT_LOCATION ("warmelt-modes.melt:3587:/ locexp");
	    /*GETPREDEFCHK__1 *//*_.CURPREDEF__V9*/ meltfptr[4] =
	      melt_fetch_predefined ( /*_#PRIX__L5*/ meltfnum[1]);;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:3589:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L6*/ meltfnum[5] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3589:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[5])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:3589:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3589;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "generate_runtypesupport_predef_fields prix=";
		    /*^apply.arg */
		    argtab[4].meltbp_long = /*_#PRIX__L5*/ meltfnum[1];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " curpredef=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPREDEF__V9*/ meltfptr[4];
		    /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V11*/ meltfptr[10] =
		    /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:3589:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V11*/ meltfptr[10] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3589:/ quasiblock");


	    /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
	    /*^compute */
	    /*_.IFCPP___V10*/ meltfptr[9] = /*_.PROGN___V13*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3589:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[5] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:3590:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_A__L8*/ meltfnum[6] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CURPREDEF__V9*/ meltfptr[4]),
				 (melt_ptr_t) (( /*!CLASS_CLASS */ meltfrout->
						tabval[1])));;
	  MELT_LOCATION ("warmelt-modes.melt:3590:/ cond");
	  /*cond */ if ( /*_#IS_A__L8*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:3591:/ quasiblock");


		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURPREDEF__V9*/
						     meltfptr[4]),
						    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[2])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURPREDEF__V9*/ meltfptr[4])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.CLASNAM__V16*/ meltfptr[15] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.CLASNAM__V16*/ meltfptr[15] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-modes.melt:3592:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURPREDEF__V9*/
						     meltfptr[4]),
						    (melt_ptr_t) (( /*!CLASS_CLASS */ meltfrout->tabval[1])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURPREDEF__V9*/ meltfptr[4])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 6, "CLASS_FIELDS");
      /*_.CLASFIELDS__V17*/ meltfptr[16] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.CLASFIELDS__V17*/ meltfptr[16] = NULL;;
		  }
		;
		/*^compute */
    /*_#NBFIELDS__L9*/ meltfnum[5] =
		  (melt_multiple_length
		   ((melt_ptr_t) ( /*_.CLASFIELDS__V17*/ meltfptr[16])));;

		{
		  MELT_LOCATION ("warmelt-modes.melt:3595:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.OUTBUF__V2*/ meltfptr[1]),
					    (0), 0);
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:3596:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "#define MELT_HAS_PREDEF_";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CLASNAM__V16*/ meltfptr[15];
		  /*_.ADD2OUT__V18*/ meltfptr[17] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[3])),
				(melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""),
				argtab, "", (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:3597:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.OUTBUF__V2*/ meltfptr[1]),
					    (0), 0);
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:3598:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[5];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "/* predefined class ";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CLASNAM__V16*/ meltfptr[15];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = " index ";
		  /*^apply.arg */
		  argtab[3].meltbp_long = /*_#PRIX__L5*/ meltfnum[1];
		  /*^apply.arg */
		  argtab[4].meltbp_cstring = " */ enum { ";
		  /*_.ADD2OUT__V19*/ meltfptr[18] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[3])),
				(melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*citerblock FOREACH_IN_MULTIPLE */
		{
		  /* start foreach_in_multiple meltcit2__EACHTUP */
		  long meltcit2__EACHTUP_ln =
		    melt_multiple_length ((melt_ptr_t) /*_.CLASFIELDS__V17*/
					  meltfptr[16]);
		  for ( /*_#FLDIX__L10*/ meltfnum[9] = 0;
		       ( /*_#FLDIX__L10*/ meltfnum[9] >= 0)
		       && ( /*_#FLDIX__L10*/ meltfnum[9] <
			   meltcit2__EACHTUP_ln);
	/*_#FLDIX__L10*/ meltfnum[9]++)
		    {
		      /*_.CURFIELD__V20*/ meltfptr[19] =
			melt_multiple_nth ((melt_ptr_t)
					   ( /*_.CLASFIELDS__V17*/
					    meltfptr[16]), /*_#FLDIX__L10*/
					   meltfnum[9]);




#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-modes.melt:3602:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {

			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
       /*_#IS_A__L11*/ meltfnum[10] =
			  melt_is_instance_of ((melt_ptr_t)
					       ( /*_.CURFIELD__V20*/
						meltfptr[19]),
					       (melt_ptr_t) (( /*!CLASS_FIELD */ meltfrout->tabval[4])));;
			MELT_LOCATION ("warmelt-modes.melt:3602:/ cond");
			/*cond */ if ( /*_#IS_A__L11*/ meltfnum[10])	/*then */
			  {
			    /*^cond.then */
			    /*_.IFELSE___V22*/ meltfptr[21] =
			      ( /*nil */ NULL);;
			  }
			else
			  {
			    MELT_LOCATION
			      ("warmelt-modes.melt:3602:/ cond.else");

			    /*^block */
			    /*anyblock */
			    {




			      {
				/*^locexp */
				melt_assert_failed (("check curfield"),
						    ("warmelt-modes.melt")
						    ? ("warmelt-modes.melt") :
						    __FILE__,
						    (3602) ? (3602) :
						    __LINE__, __FUNCTION__);
				;
			      }
			      ;
		   /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
			      /*epilog */
			    }
			    ;
			  }
			;
			/*^compute */
			/*_.IFCPP___V21*/ meltfptr[20] =
			  /*_.IFELSE___V22*/ meltfptr[21];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:3602:/ clear");
		 /*clear *//*_#IS_A__L11*/ meltfnum[10] = 0;
			/*^clear */
		 /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-modes.melt:3603:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^cond */
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CURFIELD__V20*/ meltfptr[19]),
							  (melt_ptr_t) (( /*!CLASS_FIELD */ meltfrout->tabval[4])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.CURFIELD__V20*/ meltfptr[19])
			      /*=obj*/ ;
			    melt_object_get_field (slot, obj, 2,
						   "FLD_OWNCLASS");
       /*_.FLD_OWNCLASS__V23*/ meltfptr[21] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

      /*_.FLD_OWNCLASS__V23*/ meltfptr[21] = NULL;;
			}
		      ;
		      /*^compute */
     /*_#__L12*/ meltfnum[10] =
			(( /*_.FLD_OWNCLASS__V23*/ meltfptr[21]) ==
			 ( /*_.CURPREDEF__V9*/ meltfptr[4]));;
		      MELT_LOCATION ("warmelt-modes.melt:3603:/ cond");
		      /*cond */ if ( /*_#__L12*/ meltfnum[10])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-modes.melt:3604:/ quasiblock");


			    /*^cond */
			    /*cond */ if (
					   /*ifisa */
					   melt_is_instance_of ((melt_ptr_t)
								( /*_.CURFIELD__V20*/ meltfptr[19]),
								(melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[2])))
			      )	/*then */
			      {
				/*^cond.then */
				/*^getslot */
				{
				  melt_ptr_t slot = NULL, obj = NULL;
				  obj =
				    (melt_ptr_t) ( /*_.CURFIELD__V20*/
						  meltfptr[19]) /*=obj*/ ;
				  melt_object_get_field (slot, obj, 1,
							 "NAMED_NAME");
	 /*_.FLDNAM__V26*/ meltfptr[25] = slot;
				};
				;
			      }
			    else
			      {	/*^cond.else */

	/*_.FLDNAM__V26*/ meltfptr[25] = NULL;;
			      }
			    ;
			    /*^compute */
       /*_#FLDIX__L13*/ meltfnum[12] =
			      (melt_get_int
			       ((melt_ptr_t)
				( /*_.CURFIELD__V20*/ meltfptr[19])));;

			    {
			      MELT_LOCATION
				("warmelt-modes.melt:3607:/ locexp");
			      meltgc_strbuf_add_indent ((melt_ptr_t)
							( /*_.OUTBUF__V2*/
							 meltfptr[1]), (1),
							0);
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-modes.melt:3608:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[7];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_cstring = " MELTFIELD_";
			      /*^apply.arg */
			      argtab[1].meltbp_aptr =
				(melt_ptr_t *) & /*_.FLDNAM__V26*/
				meltfptr[25];
			      /*^apply.arg */
			      argtab[2].meltbp_cstring = " = ";
			      /*^apply.arg */
			      argtab[3].meltbp_long =
				/*_#FLDIX__L10*/ meltfnum[9];
			      /*^apply.arg */
			      argtab[4].meltbp_cstring = " /*in ";
			      /*^apply.arg */
			      argtab[5].meltbp_aptr =
				(melt_ptr_t *) & /*_.CLASNAM__V16*/
				meltfptr[15];
			      /*^apply.arg */
			      argtab[6].meltbp_cstring = "*/,";
			      /*_.ADD2OUT__V27*/ meltfptr[26] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!ADD2OUT */ meltfrout->
					      tabval[3])),
					    (melt_ptr_t) ( /*_.OUTBUF__V2*/
							  meltfptr[1]),
					    (MELTBPARSTR_CSTRING
					     MELTBPARSTR_PTR
					     MELTBPARSTR_CSTRING
					     MELTBPARSTR_LONG
					     MELTBPARSTR_CSTRING
					     MELTBPARSTR_PTR
					     MELTBPARSTR_CSTRING ""), argtab,
					    "", (union meltparam_un *) 0);
			    }
			    ;
			    /*_.LET___V25*/ meltfptr[24] =
			      /*_.ADD2OUT__V27*/ meltfptr[26];;

			    MELT_LOCATION ("warmelt-modes.melt:3604:/ clear");
		 /*clear *//*_.FLDNAM__V26*/ meltfptr[25] = 0;
			    /*^clear */
		 /*clear *//*_#FLDIX__L13*/ meltfnum[12] = 0;
			    /*^clear */
		 /*clear *//*_.ADD2OUT__V27*/ meltfptr[26] = 0;
			    /*_.IF___V24*/ meltfptr[23] =
			      /*_.LET___V25*/ meltfptr[24];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-modes.melt:3603:/ clear");
		 /*clear *//*_.LET___V25*/ meltfptr[24] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

      /*_.IF___V24*/ meltfptr[23] = NULL;;
			}
		      ;
		      if ( /*_#FLDIX__L10*/ meltfnum[9] < 0)
			break;
		    }		/* end  foreach_in_multiple meltcit2__EACHTUP */

		  /*citerepilog */

		  MELT_LOCATION ("warmelt-modes.melt:3599:/ clear");
	       /*clear *//*_.CURFIELD__V20*/ meltfptr[19] = 0;
		  /*^clear */
	       /*clear *//*_#FLDIX__L10*/ meltfnum[9] = 0;
		  /*^clear */
	       /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
		  /*^clear */
	       /*clear *//*_.FLD_OWNCLASS__V23*/ meltfptr[21] = 0;
		  /*^clear */
	       /*clear *//*_#__L12*/ meltfnum[10] = 0;
		  /*^clear */
	       /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
		}		/*endciterblock FOREACH_IN_MULTIPLE */
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:3611:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.OUTBUF__V2*/ meltfptr[1]),
					    (1), 0);
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:3612:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[5];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "  MELTLENGTH_";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CLASNAM__V16*/ meltfptr[15];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = " = ";
		  /*^apply.arg */
		  argtab[3].meltbp_long = /*_#NBFIELDS__L9*/ meltfnum[5];
		  /*^apply.arg */
		  argtab[4].meltbp_cstring = " } ;";
		  /*_.ADD2OUT__V28*/ meltfptr[25] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[3])),
				(melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.LET___V15*/ meltfptr[11] =
		  /*_.ADD2OUT__V28*/ meltfptr[25];;

		MELT_LOCATION ("warmelt-modes.melt:3591:/ clear");
	      /*clear *//*_.CLASNAM__V16*/ meltfptr[15] = 0;
		/*^clear */
	      /*clear *//*_.CLASFIELDS__V17*/ meltfptr[16] = 0;
		/*^clear */
	      /*clear *//*_#NBFIELDS__L9*/ meltfnum[5] = 0;
		/*^clear */
	      /*clear *//*_.ADD2OUT__V18*/ meltfptr[17] = 0;
		/*^clear */
	      /*clear *//*_.ADD2OUT__V19*/ meltfptr[18] = 0;
		/*^clear */
	      /*clear *//*_.ADD2OUT__V28*/ meltfptr[25] = 0;
		/*_.IF___V14*/ meltfptr[10] = /*_.LET___V15*/ meltfptr[11];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:3590:/ clear");
	      /*clear *//*_.LET___V15*/ meltfptr[11] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V14*/ meltfptr[10] = NULL;;
	    }
	  ;
	}			/*end foreach_long_upto meltcit1__EACHLONG */

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:3584:/ clear");
	    /*clear *//*_#PRIX__L5*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
      /*^clear */
	    /*clear *//*_#IS_A__L8*/ meltfnum[6] = 0;
      /*^clear */
	    /*clear *//*_.IF___V14*/ meltfptr[10] = 0;
    }				/*endciterblock FOREACH_LONG_UPTO */
    ;

    MELT_LOCATION ("warmelt-modes.melt:3579:/ clear");
	   /*clear *//*_.CURPREDEF__V9*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_#NBPREDEF__L4*/ meltfnum[0] = 0;

    {
      MELT_LOCATION ("warmelt-modes.melt:3615:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3616:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
			   ("/** end of code generated by generate_runtypesupport_predef_fields **/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3618:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3619:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.OUTBUF__V2*/ meltfptr[1]),
				(0), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3620:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L14*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3620:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[5] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3620:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L15*/ meltfnum[5];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3620;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"generate_runtypesupport_predef_fields end outbuf=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTBUF__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V31*/ meltfptr[15] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V30*/ meltfptr[24] =
	      /*_.MELT_DEBUG_FUN__V31*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3620:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V31*/ meltfptr[15] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V30*/ meltfptr[24] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3620:/ quasiblock");


      /*_.PROGN___V32*/ meltfptr[16] = /*_.IF___V30*/ meltfptr[24];;
      /*^compute */
      /*_.IFCPP___V29*/ meltfptr[26] = /*_.PROGN___V32*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3620:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V30*/ meltfptr[24] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V32*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V29*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3570:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V29*/ meltfptr[26];;

    {
      MELT_LOCATION ("warmelt-modes.melt:3570:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V29*/ meltfptr[26] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_57_warmelt_modes_GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_57_warmelt_modes_GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_modes_RUNTYPESUPPORT_DOCMD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_58_warmelt_modes_RUNTYPESUPPORT_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_58_warmelt_modes_RUNTYPESUPPORT_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 72
    melt_ptr_t mcfr_varptr[72];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_58_warmelt_modes_RUNTYPESUPPORT_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_58_warmelt_modes_RUNTYPESUPPORT_DOCMD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 72; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_58_warmelt_modes_RUNTYPESUPPORT_DOCMD nbval 72*/
  meltfram__.mcfr_nbvar = 72 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("RUNTYPESUPPORT_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:3628:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3629:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3629:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3629:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3629;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "start runtypesupport_docmd cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3629:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3629:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3629:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3630:/ quasiblock");


 /*_?*/ meltfram__.loc_CSTRING__o0 =
      melt_argument ("output");;
    /*^compute */
 /*_.MAKE_STRINGCONST__V9*/ meltfptr[5] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[2])),
	( /*_?*/ meltfram__.loc_CSTRING__o0)));;
    MELT_LOCATION ("warmelt-modes.melt:3631:/ cond");
    /*cond */ if ( /*_.MAKE_STRINGCONST__V9*/ meltfptr[5])	/*then */
      {
	/*^cond.then */
	/*_.OUTARG__V10*/ meltfptr[9] =
	  /*_.MAKE_STRINGCONST__V9*/ meltfptr[5];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:3631:/ cond.else");

	/*_.OUTARG__V10*/ meltfptr[9] =
	  ( /*!konst_1 */ meltfrout->tabval[1]);;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3634:/ quasiblock");


 /*_.NAMBUF__V12*/ meltfptr[11] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			 (const char *) 0);;

    {
      MELT_LOCATION ("warmelt-modes.melt:3636:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V12*/ meltfptr[11]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.OUTARG__V10*/
					     meltfptr[9])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3637:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V12*/ meltfptr[11]),
			   (".h"));
    }
    ;
 /*_.STRBUF2STRING__V13*/ meltfptr[12] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[2])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V12*/ meltfptr[11]))));;
    /*^compute */
    /*_.LET___V11*/ meltfptr[10] = /*_.STRBUF2STRING__V13*/ meltfptr[12];;

    MELT_LOCATION ("warmelt-modes.melt:3634:/ clear");
	   /*clear *//*_.NAMBUF__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V13*/ meltfptr[12] = 0;
    /*_.OUTDECLNAME__V14*/ meltfptr[11] = /*_.LET___V11*/ meltfptr[10];;
    MELT_LOCATION ("warmelt-modes.melt:3640:/ quasiblock");


 /*_.NAMBUF__V16*/ meltfptr[15] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			 (const char *) 0);;

    {
      MELT_LOCATION ("warmelt-modes.melt:3642:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V16*/ meltfptr[15]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.OUTARG__V10*/
					     meltfptr[9])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3643:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V16*/ meltfptr[15]),
			   ("-inc.c"));
    }
    ;
 /*_.STRBUF2STRING__V17*/ meltfptr[16] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[2])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V16*/ meltfptr[15]))));;
    /*^compute */
    /*_.LET___V15*/ meltfptr[12] = /*_.STRBUF2STRING__V17*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-modes.melt:3640:/ clear");
	   /*clear *//*_.NAMBUF__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V17*/ meltfptr[16] = 0;
    /*_.OUTCODENAME__V18*/ meltfptr[15] = /*_.LET___V15*/ meltfptr[12];;
    MELT_LOCATION ("warmelt-modes.melt:3646:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.DICTYPGTY__V19*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!RETRIEVE_DICTIONNARY_CTYPE_GTY */ meltfrout->
		      tabval[4])), (melt_ptr_t) (NULL), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3647:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct meltlist_st rlist_0__LIST_;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inilist rlist_0__LIST_ */
   /*_.LIST___V21*/ meltfptr[20] =
	(melt_ptr_t) & meltletrec_1_ptr->rlist_0__LIST_;
      meltletrec_1_ptr->rlist_0__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*_.RAWCTYPGTYLIST__V20*/ meltfptr[19] = /*_.LIST___V21*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3647:/ clear");
	    /*clear *//*_.LIST___V21*/ meltfptr[20] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V21*/ meltfptr[20] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3648:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.DICTYP__V22*/ meltfptr[20] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!RETRIEVE_DICTIONNARY_CTYPE */ meltfrout->
		      tabval[5])), (melt_ptr_t) (NULL), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3649:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_2_st
      {
	struct meltlist_st rlist_0__LIST_;
	long meltletrec_2_endgap;
      } *meltletrec_2_ptr = 0;
      meltletrec_2_ptr =
	(struct meltletrec_2_st *)
	meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
      /*^blockmultialloc.initfill */
      /*inilist rlist_0__LIST_ */
   /*_.LIST___V24*/ meltfptr[23] =
	(melt_ptr_t) & meltletrec_2_ptr->rlist_0__LIST_;
      meltletrec_2_ptr->rlist_0__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*_.RAWCTYPLIST__V23*/ meltfptr[22] = /*_.LIST___V24*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3649:/ clear");
	    /*clear *//*_.LIST___V24*/ meltfptr[23] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V24*/ meltfptr[23] = 0;
    }				/*end multiallocblock */
    ;
 /*_.OUTDECLBUF__V25*/ meltfptr[23] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			 (const char *) 0);;
    /*^compute */
 /*_.OUTCODEBUF__V26*/ meltfptr[25] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			 (const char *) 0);;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3653:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3653:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3653:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3653;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "runtypesupport_docmd dictypgty=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DICTYPGTY__V19*/ meltfptr[16];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " outarg=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
	      /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V28*/ meltfptr[27] =
	      /*_.MELT_DEBUG_FUN__V29*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3653:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V29*/ meltfptr[28] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V28*/ meltfptr[27] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3653:/ quasiblock");


      /*_.PROGN___V30*/ meltfptr[28] = /*_.IF___V28*/ meltfptr[27];;
      /*^compute */
      /*_.IFCPP___V27*/ meltfptr[26] = /*_.PROGN___V30*/ meltfptr[28];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3653:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V28*/ meltfptr[27] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V30*/ meltfptr[28] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V27*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3655:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTDECLNAME__V14*/ meltfptr[11];
      /*_.GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT__V31*/ meltfptr[27] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT */
		      meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.OUTDECLBUF__V25*/ meltfptr[23]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3656:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTCODENAME__V18*/ meltfptr[15];
      /*_.GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT__V32*/ meltfptr[28] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT */
		      meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.OUTCODEBUF__V26*/ meltfptr[25]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MAPSTRING */
    {
      /*foreach_in_mapstring meltcit1__EACHSTRMAP : */ int
	meltcit1__EACHSTRMAP_ix = 0, meltcit1__EACHSTRMAP_siz = 0;
      for (meltcit1__EACHSTRMAP_ix = 0;
	   /* we retrieve in meltcit1__EACHSTRMAP_siz the size at each iteration since it could change. */
	   meltcit1__EACHSTRMAP_ix >= 0
	   && (meltcit1__EACHSTRMAP_siz =
	       melt_size_mapstrings ((struct meltmapstrings_st *)
				     /*_.DICTYPGTY__V19*/ meltfptr[16])) > 0
	   && meltcit1__EACHSTRMAP_ix < meltcit1__EACHSTRMAP_siz;
	   meltcit1__EACHSTRMAP_ix++)
	{
	  const char *meltcit1__EACHSTRMAP_str = NULL;
	  const char *meltcit1__EACHSTRMAP_nam = NULL;
    /*_.CURSTR__V33*/ meltfptr[32] = NULL;
    /*_.CURCTY__V34*/ meltfptr[33] = NULL;
	  meltcit1__EACHSTRMAP_str =
	    ((struct meltmapstrings_st *) /*_.DICTYPGTY__V19*/ meltfptr[16])->
	    entab[meltcit1__EACHSTRMAP_ix].e_at;
	  if (!meltcit1__EACHSTRMAP_str
	      || meltcit1__EACHSTRMAP_str == HTAB_DELETED_ENTRY)
	    continue;		/*foreach_in_mapstring meltcit1__EACHSTRMAP inside before */
	  /*_.CURCTY__V34*/ meltfptr[33] =
	    ((struct meltmapstrings_st *) /*_.DICTYPGTY__V19*/ meltfptr[16])->
	    entab[meltcit1__EACHSTRMAP_ix].e_va;
	  if (! /*_.CURCTY__V34*/ meltfptr[33])
	    continue;
	  if (melt_is_instance_of
	      ((melt_ptr_t) /*_.CURCTY__V34*/ meltfptr[33],
	       (melt_ptr_t) MELT_PREDEF (CLASS_NAMED))
	      && ( /*_.CURSTR__V33*/ meltfptr[32] =
		  melt_object_nth_field ((melt_ptr_t) /*_.CURCTY__V34*/
					 meltfptr[33],
					 MELTFIELD_NAMED_NAME)) != NULL
	      && (meltcit1__EACHSTRMAP_nam =
		  melt_string_str ((melt_ptr_t) /*_.CURSTR__V33*/
				   meltfptr[32])) != (char *) 0
	      && !strcmp (meltcit1__EACHSTRMAP_nam, meltcit1__EACHSTRMAP_str))
	    /*_.CURSTR__V33*/ meltfptr[32] = /*_.CURSTR__V33*/ meltfptr[32];
	  else
	    {
      /*_.CURSTR__V33*/ meltfptr[32] = NULL;
      /*_.CURSTR__V33*/ meltfptr[32] =
		meltgc_new_stringdup ((meltobject_ptr_t)
				      MELT_PREDEF (DISCR_STRING),
				      meltcit1__EACHSTRMAP_str);
	    }
	  meltcit1__EACHSTRMAP_str = (const char *) 0;
	  meltcit1__EACHSTRMAP_nam = (const char *) 0;




	  {
	    MELT_LOCATION ("warmelt-modes.melt:3660:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.RAWCTYPGTYLIST__V20*/ meltfptr[19]),
				(melt_ptr_t) ( /*_.CURCTY__V34*/
					      meltfptr[33]));
	  }
	  ;
	  /* end foreach_in_mapstring meltcit1__EACHSTRMAP */
    /*_.CURSTR__V33*/ meltfptr[32] = NULL;
    /*_.CURCTY__V34*/ meltfptr[33] = NULL;
	}

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:3657:/ clear");
	    /*clear *//*_.CURSTR__V33*/ meltfptr[32] = 0;
      /*^clear */
	    /*clear *//*_.CURCTY__V34*/ meltfptr[33] = 0;
    }				/*endciterblock FOREACH_IN_MAPSTRING */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3662:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3662:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3662:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3662;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"runtypesupport_docmd rawctypgtylist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RAWCTYPGTYLIST__V20*/ meltfptr[19];
	      /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V36*/ meltfptr[35] =
	      /*_.MELT_DEBUG_FUN__V37*/ meltfptr[36];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3662:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V37*/ meltfptr[36] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V36*/ meltfptr[35] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3662:/ quasiblock");


      /*_.PROGN___V38*/ meltfptr[36] = /*_.IF___V36*/ meltfptr[35];;
      /*^compute */
      /*_.IFCPP___V35*/ meltfptr[34] = /*_.PROGN___V38*/ meltfptr[36];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3662:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V36*/ meltfptr[35] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V38*/ meltfptr[36] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V35*/ meltfptr[34] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MAPSTRING */
    {
      /*foreach_in_mapstring meltcit2__EACHSTRMAP : */ int
	meltcit2__EACHSTRMAP_ix = 0, meltcit2__EACHSTRMAP_siz = 0;
      for (meltcit2__EACHSTRMAP_ix = 0;
	   /* we retrieve in meltcit2__EACHSTRMAP_siz the size at each iteration since it could change. */
	   meltcit2__EACHSTRMAP_ix >= 0
	   && (meltcit2__EACHSTRMAP_siz =
	       melt_size_mapstrings ((struct meltmapstrings_st *)
				     /*_.DICTYP__V22*/ meltfptr[20])) > 0
	   && meltcit2__EACHSTRMAP_ix < meltcit2__EACHSTRMAP_siz;
	   meltcit2__EACHSTRMAP_ix++)
	{
	  const char *meltcit2__EACHSTRMAP_str = NULL;
	  const char *meltcit2__EACHSTRMAP_nam = NULL;
    /*_.CURSTR__V39*/ meltfptr[35] = NULL;
    /*_.CURCTY__V40*/ meltfptr[36] = NULL;
	  meltcit2__EACHSTRMAP_str =
	    ((struct meltmapstrings_st *) /*_.DICTYP__V22*/ meltfptr[20])->
	    entab[meltcit2__EACHSTRMAP_ix].e_at;
	  if (!meltcit2__EACHSTRMAP_str
	      || meltcit2__EACHSTRMAP_str == HTAB_DELETED_ENTRY)
	    continue;		/*foreach_in_mapstring meltcit2__EACHSTRMAP inside before */
	  /*_.CURCTY__V40*/ meltfptr[36] =
	    ((struct meltmapstrings_st *) /*_.DICTYP__V22*/ meltfptr[20])->
	    entab[meltcit2__EACHSTRMAP_ix].e_va;
	  if (! /*_.CURCTY__V40*/ meltfptr[36])
	    continue;
	  if (melt_is_instance_of
	      ((melt_ptr_t) /*_.CURCTY__V40*/ meltfptr[36],
	       (melt_ptr_t) MELT_PREDEF (CLASS_NAMED))
	      && ( /*_.CURSTR__V39*/ meltfptr[35] =
		  melt_object_nth_field ((melt_ptr_t) /*_.CURCTY__V40*/
					 meltfptr[36],
					 MELTFIELD_NAMED_NAME)) != NULL
	      && (meltcit2__EACHSTRMAP_nam =
		  melt_string_str ((melt_ptr_t) /*_.CURSTR__V39*/
				   meltfptr[35])) != (char *) 0
	      && !strcmp (meltcit2__EACHSTRMAP_nam, meltcit2__EACHSTRMAP_str))
	    /*_.CURSTR__V39*/ meltfptr[35] = /*_.CURSTR__V39*/ meltfptr[35];
	  else
	    {
      /*_.CURSTR__V39*/ meltfptr[35] = NULL;
      /*_.CURSTR__V39*/ meltfptr[35] =
		meltgc_new_stringdup ((meltobject_ptr_t)
				      MELT_PREDEF (DISCR_STRING),
				      meltcit2__EACHSTRMAP_str);
	    }
	  meltcit2__EACHSTRMAP_str = (const char *) 0;
	  meltcit2__EACHSTRMAP_nam = (const char *) 0;




	  {
	    MELT_LOCATION ("warmelt-modes.melt:3666:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.RAWCTYPLIST__V23*/ meltfptr[22]),
				(melt_ptr_t) ( /*_.CURCTY__V40*/
					      meltfptr[36]));
	  }
	  ;
	  /* end foreach_in_mapstring meltcit2__EACHSTRMAP */
    /*_.CURSTR__V39*/ meltfptr[35] = NULL;
    /*_.CURCTY__V40*/ meltfptr[36] = NULL;
	}

      /*citerepilog */

      MELT_LOCATION ("warmelt-modes.melt:3663:/ clear");
	    /*clear *//*_.CURSTR__V39*/ meltfptr[35] = 0;
      /*^clear */
	    /*clear *//*_.CURCTY__V40*/ meltfptr[36] = 0;
    }				/*endciterblock FOREACH_IN_MAPSTRING */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3668:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3668:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3668:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3668;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "runtypesupport_docmd rawctyplist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RAWCTYPLIST__V23*/ meltfptr[22];
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V42*/ meltfptr[41] =
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3668:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V42*/ meltfptr[41] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3668:/ quasiblock");


      /*_.PROGN___V44*/ meltfptr[42] = /*_.IF___V42*/ meltfptr[41];;
      /*^compute */
      /*_.IFCPP___V41*/ meltfptr[40] = /*_.PROGN___V44*/ meltfptr[42];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3668:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V42*/ meltfptr[41] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V44*/ meltfptr[42] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V41*/ meltfptr[40] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3669:/ quasiblock");


    MELT_LOCATION ("warmelt-modes.melt:3670:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[8]);
      /*_.LIST_TO_MULTIPLE__V46*/ meltfptr[42] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[7])),
		    (melt_ptr_t) ( /*_.RAWCTYPGTYLIST__V20*/ meltfptr[19]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_.SORTEDCTYGTYTUPLE__V47*/ meltfptr[46] =
      meltgc_sort_multiple ((melt_ptr_t)
			    ( /*_.LIST_TO_MULTIPLE__V46*/ meltfptr[42]),
			    (melt_ptr_t) (( /*!COMPARE_NAMED_ALPHA */
					   meltfrout->tabval[9])),
			    (melt_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->
					   tabval[8])));;
    MELT_LOCATION ("warmelt-modes.melt:3673:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[8]);
      /*_.LIST_TO_MULTIPLE__V48*/ meltfptr[47] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[7])),
		    (melt_ptr_t) ( /*_.RAWCTYPLIST__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_.SORTEDCTYTUPLE__V49*/ meltfptr[48] =
      meltgc_sort_multiple ((melt_ptr_t)
			    ( /*_.LIST_TO_MULTIPLE__V48*/ meltfptr[47]),
			    (melt_ptr_t) (( /*!COMPARE_NAMED_ALPHA */
					   meltfrout->tabval[9])),
			    (melt_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->
					   tabval[8])));;
    MELT_LOCATION ("warmelt-modes.melt:3676:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.RETRIEVE_VALUE_DESCRIPTOR_LIST__V50*/ meltfptr[49] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!RETRIEVE_VALUE_DESCRIPTOR_LIST */ meltfrout->
		      tabval[10])), (melt_ptr_t) (NULL), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.RAWVALDESCTUPLE__V51*/ meltfptr[50] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[7])),
		    (melt_ptr_t) ( /*_.RETRIEVE_VALUE_DESCRIPTOR_LIST__V50*/
				  meltfptr[49]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
 /*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51] =
      meltgc_sort_multiple ((melt_ptr_t)
			    ( /*_.RAWVALDESCTUPLE__V51*/ meltfptr[50]),
			    (melt_ptr_t) (( /*!COMPARE_NAMED_ALPHA */
					   meltfrout->tabval[9])),
			    (melt_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->
					   tabval[8])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3681:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3681:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3681:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3681;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"runtypesupport_docmd sortedctygtytuple=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SORTEDCTYGTYTUPLE__V47*/ meltfptr[46];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " sortedvaldesctuple=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51];
	      /*_.MELT_DEBUG_FUN__V55*/ meltfptr[54] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V54*/ meltfptr[53] =
	      /*_.MELT_DEBUG_FUN__V55*/ meltfptr[54];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3681:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V55*/ meltfptr[54] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V54*/ meltfptr[53] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3681:/ quasiblock");


      /*_.PROGN___V56*/ meltfptr[54] = /*_.IF___V54*/ meltfptr[53];;
      /*^compute */
      /*_.IFCPP___V53*/ meltfptr[52] = /*_.PROGN___V56*/ meltfptr[54];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3681:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V54*/ meltfptr[53] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V56*/ meltfptr[54] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V53*/ meltfptr[52] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3685:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTDECLBUF__V25*/ meltfptr[23];
      /*_.GENERATE_RUNTYPESUPPORT_ENUM_OBJMAGIC__V57*/ meltfptr[53] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_ENUM_OBJMAGIC */ meltfrout->
		      tabval[11])),
		    (melt_ptr_t) ( /*_.SORTEDCTYGTYTUPLE__V47*/ meltfptr[46]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3689:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTDECLBUF__V25*/ meltfptr[23];
      /*_.GENERATE_RUNTYPESUPPORT_GTY__V58*/ meltfptr[54] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_GTY */ meltfrout->
		      tabval[12])),
		    (melt_ptr_t) ( /*_.SORTEDCTYGTYTUPLE__V47*/ meltfptr[46]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3693:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTDECLBUF__V25*/ meltfptr[23];
      /*_.GENERATE_RUNTYPESUPPORT_PARAM__V59*/ meltfptr[58] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_PARAM */ meltfrout->
		      tabval[13])),
		    (melt_ptr_t) ( /*_.SORTEDCTYTUPLE__V49*/ meltfptr[48]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3696:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTCODEBUF__V26*/ meltfptr[25];
      /*_.GENERATE_RUNTYPESUPPORT_COD2CTYPE__V60*/ meltfptr[59] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_COD2CTYPE */ meltfrout->
		      tabval[14])),
		    (melt_ptr_t) ( /*_.SORTEDCTYTUPLE__V49*/ meltfptr[48]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3698:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTDECLBUF__V25*/ meltfptr[23]), (0),
				0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3699:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.OUTDECLBUF__V25*/ meltfptr[23]),
			   ("melt_ptr_t melt_code_to_ctype (int);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3700:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				( /*_.OUTDECLBUF__V25*/ meltfptr[23]), (0),
				0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3702:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTDECLBUF__V25*/ meltfptr[23];
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTCODEBUF__V26*/ meltfptr[25];
      /*_.GENERATE_RUNTYPESUPPORT_MAG2STR__V61*/ meltfptr[60] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_MAG2STR */ meltfrout->
		      tabval[15])),
		    (melt_ptr_t) ( /*_.SORTEDCTYTUPLE__V49*/ meltfptr[48]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3705:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTCODEBUF__V26*/ meltfptr[25];
      /*_.GENERATE_RUNTYPESUPPORT_FORWCOPY_FUN__V62*/ meltfptr[61] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_FORWCOPY_FUN */ meltfrout->
		      tabval[16])),
		    (melt_ptr_t) ( /*_.SORTEDCTYGTYTUPLE__V47*/ meltfptr[46]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3709:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTCODEBUF__V26*/ meltfptr[25];
      /*_.GENERATE_RUNTYPESUPPORT_SCANNING__V63*/ meltfptr[62] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_SCANNING */ meltfrout->
		      tabval[17])),
		    (melt_ptr_t) ( /*_.SORTEDCTYGTYTUPLE__V47*/ meltfptr[46]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3713:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTDECLBUF__V25*/ meltfptr[23];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTCODEBUF__V26*/ meltfptr[25];
      /*_.GENERATE_RUNTYPESUPPORT_BOXINGFUN__V64*/ meltfptr[63] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_BOXINGFUN */ meltfrout->
		      tabval[18])),
		    (melt_ptr_t) ( /*_.SORTEDCTYGTYTUPLE__V47*/ meltfptr[46]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3716:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTCODEBUF__V26*/ meltfptr[25];
      /*_.GENERATE_RUNTYPESUPPORT_CLONING_FUN__V65*/ meltfptr[64] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_CLONING_FUN */ meltfrout->
		      tabval[19])),
		    (melt_ptr_t) ( /*_.SORTEDCTYGTYTUPLE__V47*/ meltfptr[46]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3720:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTDECLBUF__V25*/ meltfptr[23];
      /*_.GENERATE_RUNTYPESUPPORT_MAPFUN__V66*/ meltfptr[65] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_MAPFUN */ meltfrout->
		      tabval[20])),
		    (melt_ptr_t) ( /*_.SORTEDCTYGTYTUPLE__V47*/ meltfptr[46]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3722:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS__V67*/ meltfptr[66] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS */ meltfrout->
		      tabval[21])),
		    (melt_ptr_t) ( /*_.OUTDECLBUF__V25*/ meltfptr[23]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3729:/ locexp");
      /* runtypesupport_docmd TERMCOMMENTDECLCHK__1 */
      {
	time_t nowdecl = 0;
	char decldatebuf[48];
	memset (decldatebuf, 0, sizeof (decldatebuf));
	time (&nowdecl);
	strftime (decldatebuf, sizeof (decldatebuf) - 1,
		  "%Y %b %d", localtime (&nowdecl));
	meltgc_out_printf ((melt_ptr_t) /*_.OUTDECLBUF__V25*/ meltfptr[23],
			   "\n/*** End of declaration file %s generated on %s\n"
			   " * by GCC MELT %s . ***/\n",
			   melt_string_str ((melt_ptr_t)
					    /*_.OUTDECLNAME__V14*/
					    meltfptr[11]),
			   decldatebuf, melt_gccversionstr);
      } /* end  TERMCOMMENTDECLCHK__1 runtypesupport_docmd */ ;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3742:/ locexp");
      /*output_sbuf_strval */
	melt_output_strbuf_to_file ((melt_ptr_t)
				    ( /*_.OUTDECLBUF__V25*/ meltfptr[23]),
				    melt_string_str ((melt_ptr_t)
						     /*_.OUTDECLNAME__V14*/
						     meltfptr[11]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3743:/ locexp");
      inform (UNKNOWN_LOCATION, "MELT INFORM [#%ld]: %s - %s",
	      melt_dbgcounter, ("generated runtype support declaration file"),
	      melt_string_str ((melt_ptr_t)
			       ( /*_.OUTDECLNAME__V14*/ meltfptr[11])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3747:/ locexp");
      /* runtypesupport_docmd TERMCOMMENTCODECHK__1 */
      {
	time_t nowcode = 0;
	char codedatebuf[48];
	memset (codedatebuf, 0, sizeof (codedatebuf));
	time (&nowcode);
	strftime (codedatebuf, sizeof (codedatebuf) - 1,
		  "%Y %b %d", localtime (&nowcode));
	meltgc_out_printf ((melt_ptr_t) /*_.OUTCODEBUF__V26*/ meltfptr[25],
			   "\n/*** End of code file %s generated on %s\n"
			   " * by GCC MELT %s . ***/\n",
			   melt_string_str ((melt_ptr_t)
					    /*_.OUTCODENAME__V18*/
					    meltfptr[15]),
			   codedatebuf, melt_gccversionstr);
      } /* end  TERMCOMMENTCODECHK__1 runtypesupport_docmd */ ;
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3760:/ locexp");
      /*output_sbuf_strval */
	melt_output_strbuf_to_file ((melt_ptr_t)
				    ( /*_.OUTCODEBUF__V26*/ meltfptr[25]),
				    melt_string_str ((melt_ptr_t)
						     /*_.OUTCODENAME__V18*/
						     meltfptr[15]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:3761:/ locexp");
      inform (UNKNOWN_LOCATION, "MELT INFORM [#%ld]: %s - %s",
	      melt_dbgcounter,
	      ("generated runtype support implementation file"),
	      melt_string_str ((melt_ptr_t)
			       ( /*_.OUTCODENAME__V18*/ meltfptr[15])));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3763:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3763:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3763:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3763;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "runtypesupport_docmd done outarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V10*/ meltfptr[9];
	      /*_.MELT_DEBUG_FUN__V70*/ meltfptr[69] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V69*/ meltfptr[68] =
	      /*_.MELT_DEBUG_FUN__V70*/ meltfptr[69];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3763:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V70*/ meltfptr[69] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V69*/ meltfptr[68] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3763:/ quasiblock");


      /*_.PROGN___V71*/ meltfptr[69] = /*_.IF___V69*/ meltfptr[68];;
      /*^compute */
      /*_.IFCPP___V68*/ meltfptr[67] = /*_.PROGN___V71*/ meltfptr[69];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3763:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V69*/ meltfptr[68] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V71*/ meltfptr[69] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V68*/ meltfptr[67] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3764:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!konst_22_TRUE */ meltfrout->tabval[22]);;

    {
      MELT_LOCATION ("warmelt-modes.melt:3764:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V45*/ meltfptr[41] = /*_.RETURN___V72*/ meltfptr[68];;

    MELT_LOCATION ("warmelt-modes.melt:3669:/ clear");
	   /*clear *//*_.LIST_TO_MULTIPLE__V46*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.SORTEDCTYGTYTUPLE__V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.LIST_TO_MULTIPLE__V48*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.SORTEDCTYTUPLE__V49*/ meltfptr[48] = 0;
    /*^clear */
	   /*clear *//*_.RETRIEVE_VALUE_DESCRIPTOR_LIST__V50*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_.RAWVALDESCTUPLE__V51*/ meltfptr[50] = 0;
    /*^clear */
	   /*clear *//*_.SORTEDVALDESCTUPLE__V52*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V53*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_ENUM_OBJMAGIC__V57*/ meltfptr[53] =
      0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_GTY__V58*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_PARAM__V59*/ meltfptr[58] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_COD2CTYPE__V60*/ meltfptr[59] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_MAG2STR__V61*/ meltfptr[60] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_FORWCOPY_FUN__V62*/ meltfptr[61] =
      0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_SCANNING__V63*/ meltfptr[62] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_BOXINGFUN__V64*/ meltfptr[63] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_CLONING_FUN__V65*/ meltfptr[64] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_MAPFUN__V66*/ meltfptr[65] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_RUNTYPESUPPORT_PREDEF_FIELDS__V67*/ meltfptr[66] =
      0;
    /*^clear */
	   /*clear *//*_.IFCPP___V68*/ meltfptr[67] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V72*/ meltfptr[68] = 0;
    /*_.LET___V8*/ meltfptr[4] = /*_.LET___V45*/ meltfptr[41];;

    MELT_LOCATION ("warmelt-modes.melt:3630:/ clear");
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
    /*^clear */
	   /*clear *//*_.MAKE_STRINGCONST__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OUTARG__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OUTDECLNAME__V14*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTCODENAME__V18*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.DICTYPGTY__V19*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.RAWCTYPGTYLIST__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.DICTYP__V22*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.RAWCTYPLIST__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OUTDECLBUF__V25*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OUTCODEBUF__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT__V31*/
      meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT__V32*/
      meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.LET___V45*/ meltfptr[41] = 0;
    MELT_LOCATION ("warmelt-modes.melt:3628:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:3628:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("RUNTYPESUPPORT_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_58_warmelt_modes_RUNTYPESUPPORT_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_58_warmelt_modes_RUNTYPESUPPORT_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_modes_TRANSLATE_TO_FLAVORED_MODULE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_59_warmelt_modes_TRANSLATE_TO_FLAVORED_MODULE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_59_warmelt_modes_TRANSLATE_TO_FLAVORED_MODULE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 54
    melt_ptr_t mcfr_varptr[54];
#define MELTFRAM_NBVARNUM 19
    long mcfr_varnum[19];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_59_warmelt_modes_TRANSLATE_TO_FLAVORED_MODULE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_59_warmelt_modes_TRANSLATE_TO_FLAVORED_MODULE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 54; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_59_warmelt_modes_TRANSLATE_TO_FLAVORED_MODULE nbval 54*/
  meltfram__.mcfr_nbvar = 54 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATE_TO_FLAVORED_MODULE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:3781:/ getarg");
 /*_.INARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.OUTARG__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.OUTARG__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CARG__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CARG__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.FLAVORS__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.FLAVORS__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CURENV__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CURENV__V6*/ meltfptr[5])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3782:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3782:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3782:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[13];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3782;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_to_flavored_module inarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INARG__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " outarg=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " carg=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.CARG__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " flavors=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.FLAVORS__V5*/ meltfptr[4];
	      /*^apply.arg */
	      argtab[11].meltbp_cstring = " curenv=";
	      /*^apply.arg */
	      argtab[12].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURENV__V6*/ meltfptr[5];
	      /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] = /*_.MELT_DEBUG_FUN__V9*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3782:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V9*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3782:/ quasiblock");


      /*_.PROGN___V10*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.PROGN___V10*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3782:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V10*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3784:/ quasiblock");


    MELT_LOCATION ("warmelt-modes.melt:3787:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L3*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.OUTARG__V3*/ meltfptr[2])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-modes.melt:3787:/ cond");
    /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_STRING_WITHOUT_SUFFIX__V13*/ meltfptr[12] =
	    /*make_string_without_suffix */
	    (meltgc_new_string_without_suffix
	     ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[1])),
	      melt_string_str ((melt_ptr_t) ( /*_.OUTARG__V3*/ meltfptr[2])),
	      (".c")));;
	  /*^compute */
	  /*_.BASNAM__V12*/ meltfptr[8] =
	    /*_.MAKE_STRING_WITHOUT_SUFFIX__V13*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:3787:/ clear");
	     /*clear *//*_.MAKE_STRING_WITHOUT_SUFFIX__V13*/ meltfptr[12] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:3789:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L4*/ meltfnum[0] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.INARG__V2*/ meltfptr[1])) ==
	     MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-modes.melt:3789:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L4*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MAKE_STRING_NAKEDBASENAME__V15*/ meltfptr[14] =
		  (meltgc_new_string_nakedbasename
		   ((meltobject_ptr_t)
		    (( /*!DISCR_STRING */ meltfrout->tabval[1])),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.INARG__V2*/ meltfptr[1]))));;
		/*^compute */
		/*_.IFELSE___V14*/ meltfptr[12] =
		  /*_.MAKE_STRING_NAKEDBASENAME__V15*/ meltfptr[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:3789:/ clear");
	       /*clear *//*_.MAKE_STRING_NAKEDBASENAME__V15*/ meltfptr[14] =
		  0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:3792:/ locexp");
		  error ("MELT ERROR MSG [#%ld]::: %s", melt_dbgcounter,
			 ("invalid translate to flavored module arguments"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:3793:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-modes.melt:3793:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-modes.melt:3791:/ quasiblock");


		/*_.PROGN___V17*/ meltfptr[16] =
		  /*_.RETURN___V16*/ meltfptr[14];;
		/*^compute */
		/*_.IFELSE___V14*/ meltfptr[12] =
		  /*_.PROGN___V17*/ meltfptr[16];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:3789:/ clear");
	       /*clear *//*_.RETURN___V16*/ meltfptr[14] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V17*/ meltfptr[16] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.BASNAM__V12*/ meltfptr[8] = /*_.IFELSE___V14*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:3787:/ clear");
	     /*clear *//*_#IS_STRING__L4*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[12] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3795:/ cond");
    /*cond */ if ( /*_.OUTARG__V3*/ meltfptr[2])	/*then */
      {
	/*^cond.then */
	/*_.OUTNAM__V18*/ meltfptr[14] = /*_.OUTARG__V3*/ meltfptr[2];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:3795:/ cond.else");

	/*_.OUTNAM__V18*/ meltfptr[14] = /*_.BASNAM__V12*/ meltfptr[8];;
      }
    ;
    /*^compute */
 /*_.MAKE_STRING_WITHOUT_SUFFIX__V19*/ meltfptr[16] =
      /*make_string_without_suffix */
      (meltgc_new_string_without_suffix
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[1])),
	melt_string_str ((melt_ptr_t) ( /*_.CARG__V4*/ meltfptr[3])),
	(".c")));;
    MELT_LOCATION ("warmelt-modes.melt:3796:/ cond");
    /*cond */ if ( /*_.MAKE_STRING_WITHOUT_SUFFIX__V19*/ meltfptr[16])	/*then */
      {
	/*^cond.then */
	/*_.RAWSRCNAM__V20*/ meltfptr[12] =
	  /*_.MAKE_STRING_WITHOUT_SUFFIX__V19*/ meltfptr[16];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:3796:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_.MAKE_STRING_NAKEDBASENAME__V21*/ meltfptr[20] =
	    (meltgc_new_string_nakedbasename
	     ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[1])),
	      melt_string_str ((melt_ptr_t)
			       ( /*_.OUTNAM__V18*/ meltfptr[14]))));;
	  /*^compute */
	  /*_.RAWSRCNAM__V20*/ meltfptr[12] =
	    /*_.MAKE_STRING_NAKEDBASENAME__V21*/ meltfptr[20];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:3796:/ clear");
	     /*clear *//*_.MAKE_STRING_NAKEDBASENAME__V21*/ meltfptr[20] = 0;
	}
	;
      }
    ;
 /*_.OUTBASE__V22*/ meltfptr[20] =
      /*make_string_without_suffix */
      (meltgc_new_string_without_suffix
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[1])),
	melt_string_str ((melt_ptr_t) ( /*_.OUTNAM__V18*/ meltfptr[14])),
	(".c")));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3800:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CURENV__V6*/ meltfptr[5]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-modes.melt:3800:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:3800:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curenv"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (3800) ? (3800) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[22] = /*_.IFELSE___V24*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3800:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3801:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3801:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3801:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3801;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translate_to_flavored_module basnam=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BASNAM__V12*/ meltfptr[8];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " rawsrcnam=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.RAWSRCNAM__V20*/ meltfptr[12];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " outbase=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTBASE__V22*/ meltfptr[20];
	      /*_.MELT_DEBUG_FUN__V27*/ meltfptr[26] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V26*/ meltfptr[25] =
	      /*_.MELT_DEBUG_FUN__V27*/ meltfptr[26];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3801:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V27*/ meltfptr[26] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V26*/ meltfptr[25] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3801:/ quasiblock");


      /*_.PROGN___V28*/ meltfptr[26] = /*_.IF___V26*/ meltfptr[25];;
      /*^compute */
      /*_.IFCPP___V25*/ meltfptr[23] = /*_.PROGN___V28*/ meltfptr[26];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3801:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V26*/ meltfptr[25] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V28*/ meltfptr[26] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V25*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3803:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.OUTBASE__V22*/ meltfptr[20];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.CURENV__V6*/ meltfptr[5];
      /*_.TRANSLATE_TO_C_MODULE_MELT_SOURCES__V29*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATE_TO_C_MODULE_MELT_SOURCES */ meltfrout->
		      tabval[3])),
		    (melt_ptr_t) ( /*_.INARG__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3805:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L8*/ meltfnum[6] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FLAVORS__V5*/ meltfptr[4])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-modes.melt:3805:/ cond");
    /*cond */ if ( /*_#IS_STRING__L8*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:3806:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L9*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3806:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:3806:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[9];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-modes.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 3806;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "before generate_flavored_melt_module rawsrcnam=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.RAWSRCNAM__V20*/ meltfptr[12];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " outnam=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OUTNAM__V18*/ meltfptr[14];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "flavor=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.FLAVORS__V5*/ meltfptr[4];
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V32*/ meltfptr[31] =
		    /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:3806:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V32*/ meltfptr[31] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:3806:/ quasiblock");


	    /*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[31];;
	    /*^compute */
	    /*_.IFCPP___V31*/ meltfptr[30] = /*_.PROGN___V34*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3806:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V31*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:3808:/ locexp");
	    /*generate_flavored_melt_module */
	      melt_compile_source (melt_string_str
				   ((melt_ptr_t) /*_.RAWSRCNAM__V20*/
				    meltfptr[12]),
				   melt_string_str ((melt_ptr_t)
						    /*_.OUTNAM__V18*/
						    meltfptr[14]),
				   NULL, melt_string_str ((melt_ptr_t) /*_.FLAVORS__V5*/ meltfptr[4]));	/*generate_flavored_melt_module */
	    ;
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:3805:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.IFCPP___V31*/ meltfptr[30] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:3809:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_LIST__L11*/ meltfnum[9] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.FLAVORS__V5*/ meltfptr[4]))
	     == MELTOBMAG_LIST);;
	  MELT_LOCATION ("warmelt-modes.melt:3809:/ cond");
	  /*cond */ if ( /*_#IS_LIST__L11*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*citerblock FOREACH_IN_LIST */
		{
		  /* start foreach_in_list meltcit1__EACHLIST */
		  for ( /*_.CURPAIR__V36*/ meltfptr[32] =
		       melt_list_first ((melt_ptr_t) /*_.FLAVORS__V5*/
					meltfptr[4]);
		       melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V36*/
					 meltfptr[32]) == MELTOBMAG_PAIR;
		       /*_.CURPAIR__V36*/ meltfptr[32] =
		       melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V36*/
				       meltfptr[32]))
		    {
		      /*_.CURFLAVOR__V37*/ meltfptr[30] =
			melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V36*/
					meltfptr[32]);



#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-modes.melt:3813:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L12*/ meltfnum[0] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-modes.melt:3813:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[0])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-modes.melt:3813:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[9];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-modes.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 3813;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "before generate_flavored_melt_module rawsrcnam=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.RAWSRCNAM__V20*/
				  meltfptr[12];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " outnam=";
				/*^apply.arg */
				argtab[6].meltbp_aptr =
				  (melt_ptr_t *) & /*_.OUTNAM__V18*/
				  meltfptr[14];
				/*^apply.arg */
				argtab[7].meltbp_cstring = "flavor=";
				/*^apply.arg */
				argtab[8].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CURFLAVOR__V37*/
				  meltfptr[30];
				/*_.MELT_DEBUG_FUN__V40*/ meltfptr[39] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V39*/ meltfptr[38] =
				/*_.MELT_DEBUG_FUN__V40*/ meltfptr[39];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-modes.melt:3813:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L13*/
				meltfnum[12] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V40*/ meltfptr[39]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V39*/ meltfptr[38] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-modes.melt:3813:/ quasiblock");


			/*_.PROGN___V41*/ meltfptr[39] =
			  /*_.IF___V39*/ meltfptr[38];;
			/*^compute */
			/*_.IFCPP___V38*/ meltfptr[37] =
			  /*_.PROGN___V41*/ meltfptr[39];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:3813:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[0] = 0;
			/*^clear */
		  /*clear *//*_.IF___V39*/ meltfptr[38] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V41*/ meltfptr[39] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V38*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

		      {
			MELT_LOCATION ("warmelt-modes.melt:3815:/ locexp");
			/*generate_flavored_melt_module */
			  melt_compile_source (melt_string_str
					       ((melt_ptr_t)
						/*_.RAWSRCNAM__V20*/
						meltfptr[12]),
					       melt_string_str ((melt_ptr_t)
								/*_.OUTNAM__V18*/
								meltfptr[14]),
					       NULL, melt_string_str ((melt_ptr_t) /*_.CURFLAVOR__V37*/ meltfptr[30]));	/*generate_flavored_melt_module */
			;
		      }
		      ;
		    }		/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V36*/ meltfptr[32] = NULL;
     /*_.CURFLAVOR__V37*/ meltfptr[30] = NULL;


		  /*citerepilog */

		  MELT_LOCATION ("warmelt-modes.melt:3810:/ clear");
		/*clear *//*_.CURPAIR__V36*/ meltfptr[32] = 0;
		  /*^clear */
		/*clear *//*_.CURFLAVOR__V37*/ meltfptr[30] = 0;
		  /*^clear */
		/*clear *//*_.IFCPP___V38*/ meltfptr[37] = 0;
		}		/*endciterblock FOREACH_IN_LIST */
		;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:3809:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:3816:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_MULTIPLE__L14*/ meltfnum[12] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.FLAVORS__V5*/ meltfptr[4])) ==
		   MELTOBMAG_MULTIPLE);;
		MELT_LOCATION ("warmelt-modes.melt:3816:/ cond");
		/*cond */ if ( /*_#IS_MULTIPLE__L14*/ meltfnum[12])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*citerblock FOREACH_IN_MULTIPLE */
		      {
			/* start foreach_in_multiple meltcit2__EACHTUP */
			long meltcit2__EACHTUP_ln =
			  melt_multiple_length ((melt_ptr_t) /*_.FLAVORS__V5*/
						meltfptr[4]);
			for ( /*_#FLIX__L15*/ meltfnum[0] = 0;
			     ( /*_#FLIX__L15*/ meltfnum[0] >= 0)
			     && ( /*_#FLIX__L15*/ meltfnum[0] <
				 meltcit2__EACHTUP_ln);
	/*_#FLIX__L15*/ meltfnum[0]++)
			  {
			    /*_.CURFLAVOR__V43*/ meltfptr[39] =
			      melt_multiple_nth ((melt_ptr_t)
						 ( /*_.FLAVORS__V5*/
						  meltfptr[4]),
						 /*_#FLIX__L15*/ meltfnum[0]);




#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-modes.melt:3820:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	  /*_#MELT_NEED_DBG__L16*/ meltfnum[15] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-modes.melt:3820:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[15])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	    /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-modes.melt:3820:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[11];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L17*/
					meltfnum[16];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-modes.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 3820;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"before generate_flavored_melt_module rawsrcnam=";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.RAWSRCNAM__V20*/
					meltfptr[12];
				      /*^apply.arg */
				      argtab[5].meltbp_cstring = " outnam=";
				      /*^apply.arg */
				      argtab[6].meltbp_aptr =
					(melt_ptr_t *) & /*_.OUTNAM__V18*/
					meltfptr[14];
				      /*^apply.arg */
				      argtab[7].meltbp_cstring = "flavor=";
				      /*^apply.arg */
				      argtab[8].meltbp_aptr =
					(melt_ptr_t *) & /*_.CURFLAVOR__V43*/
					meltfptr[39];
				      /*^apply.arg */
				      argtab[9].meltbp_cstring = " flix=";
				      /*^apply.arg */
				      argtab[10].meltbp_long =
					/*_#FLIX__L15*/ meltfnum[0];
				      /*_.MELT_DEBUG_FUN__V46*/ meltfptr[45] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V45*/ meltfptr[44] =
				      /*_.MELT_DEBUG_FUN__V46*/ meltfptr[45];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-modes.melt:3820:/ clear");
		      /*clear *//*_#THE_MELTCALLCOUNT__L17*/
				      meltfnum[16] = 0;
				    /*^clear */
		      /*clear *//*_.MELT_DEBUG_FUN__V46*/
				      meltfptr[45] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	   /*_.IF___V45*/ meltfptr[44] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-modes.melt:3820:/ quasiblock");


			      /*_.PROGN___V47*/ meltfptr[45] =
				/*_.IF___V45*/ meltfptr[44];;
			      /*^compute */
			      /*_.IFCPP___V44*/ meltfptr[43] =
				/*_.PROGN___V47*/ meltfptr[45];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-modes.melt:3820:/ clear");
		    /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[15]
				= 0;
			      /*^clear */
		    /*clear *//*_.IF___V45*/ meltfptr[44] = 0;
			      /*^clear */
		    /*clear *//*_.PROGN___V47*/ meltfptr[45] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V44*/ meltfptr[43] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;

			    {
			      MELT_LOCATION
				("warmelt-modes.melt:3822:/ locexp");
			      /*generate_flavored_melt_module */
				melt_compile_source (melt_string_str
						     ((melt_ptr_t)
						      /*_.RAWSRCNAM__V20*/
						      meltfptr[12]),
						     melt_string_str ((melt_ptr_t) /*_.OUTNAM__V18*/ meltfptr[14]),
						     NULL, melt_string_str ((melt_ptr_t) /*_.CURFLAVOR__V43*/ meltfptr[39]));	/*generate_flavored_melt_module */
			      ;
			    }
			    ;
			    if ( /*_#FLIX__L15*/ meltfnum[0] < 0)
			      break;
			  }	/* end  foreach_in_multiple meltcit2__EACHTUP */

			/*citerepilog */

			MELT_LOCATION ("warmelt-modes.melt:3817:/ clear");
		  /*clear *//*_.CURFLAVOR__V43*/ meltfptr[39] = 0;
			/*^clear */
		  /*clear *//*_#FLIX__L15*/ meltfnum[0] = 0;
			/*^clear */
		  /*clear *//*_.IFCPP___V44*/ meltfptr[43] = 0;
		      }		/*endciterblock FOREACH_IN_MULTIPLE */
		      ;
		      /*epilog */
		    }
		    ;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-modes.melt:3816:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-modes.melt:3824:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L18*/ meltfnum[16] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-modes.melt:3824:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[16])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[15] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-modes.melt:3824:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[15];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-modes.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 3824;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "translate_to_flavored_module bad flavors=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.FLAVORS__V5*/
				  meltfptr[4];
				/*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V49*/ meltfptr[45] =
				/*_.MELT_DEBUG_FUN__V50*/ meltfptr[49];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-modes.melt:3824:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L19*/
				meltfnum[15] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V50*/ meltfptr[49]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V49*/ meltfptr[45] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-modes.melt:3824:/ quasiblock");


			/*_.PROGN___V51*/ meltfptr[49] =
			  /*_.IF___V49*/ meltfptr[45];;
			/*^compute */
			/*_.IFCPP___V48*/ meltfptr[44] =
			  /*_.PROGN___V51*/ meltfptr[49];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:3824:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[16] = 0;
			/*^clear */
		   /*clear *//*_.IF___V49*/ meltfptr[45] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V51*/ meltfptr[49] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V48*/ meltfptr[44] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-modes.melt:3825:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {

			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
			/*^cond */
			/*cond */ if (( /*nil */ NULL))	/*then */
			  {
			    /*^cond.then */
			    /*_.IFELSE___V53*/ meltfptr[49] =
			      ( /*nil */ NULL);;
			  }
			else
			  {
			    MELT_LOCATION
			      ("warmelt-modes.melt:3825:/ cond.else");

			    /*^block */
			    /*anyblock */
			    {




			      {
				/*^locexp */
				melt_assert_failed (("invalid flavors"),
						    ("warmelt-modes.melt")
						    ? ("warmelt-modes.melt") :
						    __FILE__,
						    (3825) ? (3825) :
						    __LINE__, __FUNCTION__);
				;
			      }
			      ;
		     /*clear *//*_.IFELSE___V53*/ meltfptr[49] = 0;
			      /*epilog */
			    }
			    ;
			  }
			;
			/*^compute */
			/*_.IFCPP___V52*/ meltfptr[45] =
			  /*_.IFELSE___V53*/ meltfptr[49];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:3825:/ clear");
		   /*clear *//*_.IFELSE___V53*/ meltfptr[49] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V52*/ meltfptr[45] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-modes.melt:3823:/ quasiblock");


		      /*_.PROGN___V54*/ meltfptr[49] =
			/*_.IFCPP___V52*/ meltfptr[45];;
		      /*^compute */
		      /*_.IFELSE___V42*/ meltfptr[38] =
			/*_.PROGN___V54*/ meltfptr[49];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-modes.melt:3816:/ clear");
		 /*clear *//*_.IFCPP___V48*/ meltfptr[44] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V52*/ meltfptr[45] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V54*/ meltfptr[49] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V35*/ meltfptr[31] =
		  /*_.IFELSE___V42*/ meltfptr[38];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:3809:/ clear");
	       /*clear *//*_#IS_MULTIPLE__L14*/ meltfnum[12] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V42*/ meltfptr[38] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V30*/ meltfptr[26] = /*_.IFELSE___V35*/ meltfptr[31];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:3805:/ clear");
	     /*clear *//*_#IS_LIST__L11*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V35*/ meltfptr[31] = 0;
	}
	;
      }
    ;
    /*_.LET___V11*/ meltfptr[7] = /*_.IFELSE___V30*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-modes.melt:3784:/ clear");
	   /*clear *//*_#IS_STRING__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BASNAM__V12*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OUTNAM__V18*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_STRING_WITHOUT_SUFFIX__V19*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.RAWSRCNAM__V20*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OUTBASE__V22*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V25*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATE_TO_C_MODULE_MELT_SOURCES__V29*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L8*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V30*/ meltfptr[26] = 0;
    MELT_LOCATION ("warmelt-modes.melt:3781:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V11*/ meltfptr[7];;

    {
      MELT_LOCATION ("warmelt-modes.melt:3781:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V11*/ meltfptr[7] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATE_TO_FLAVORED_MODULE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_59_warmelt_modes_TRANSLATE_TO_FLAVORED_MODULE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_59_warmelt_modes_TRANSLATE_TO_FLAVORED_MODULE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_modes_TRANSLATETOMODULE_DOCMD (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_60_warmelt_modes_TRANSLATETOMODULE_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_60_warmelt_modes_TRANSLATETOMODULE_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 21
    melt_ptr_t mcfr_varptr[21];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    const char *loc_CSTRING__o2;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_60_warmelt_modes_TRANSLATETOMODULE_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_60_warmelt_modes_TRANSLATETOMODULE_DOCMD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 21; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_60_warmelt_modes_TRANSLATETOMODULE_DOCMD nbval 21*/
  meltfram__.mcfr_nbvar = 21 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATETOMODULE_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:3829:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-modes.melt:3830:/ locexp");
      debugeputs (("starting translatetomodule_docmd"));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3831:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3831:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3831:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3831;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "start translatetomodule_docmd cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3831:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3831:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3831:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3832:/ quasiblock");


    /*_.PARMODENV__V9*/ meltfptr[5] = ( /*!konst_1 */ meltfrout->tabval[1]);;
    MELT_LOCATION ("warmelt-modes.melt:3834:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.MODULDATA__V3*/ meltfptr[2])	/*then */
      {
	/*^cond.then */
	/*_.CURENV__V10*/ meltfptr[9] = /*_.MODULDATA__V3*/ meltfptr[2];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:3834:/ cond.else");

	/*_.CURENV__V10*/ meltfptr[9] =
	  ( /*!INITIAL_ENVIRONMENT */ meltfrout->tabval[2]);;
      }
    ;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o0 =
      melt_argument ("arg");;
    /*^compute */
 /*_.INARG__V11*/ meltfptr[10] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o0)));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o1 =
      melt_argument ("output");;
    /*^compute */
 /*_.OUTARG__V12*/ meltfptr[11] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o1)));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o2 =
      melt_argument ("coutput");;
    /*^compute */
 /*_.COUTARG__V13*/ meltfptr[12] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o2)));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3839:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3839:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3839:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3839;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "translatetomodule_docmd inarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INARG__V11*/ meltfptr[10];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " outarg=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V12*/ meltfptr[11];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " coutarg=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.COUTARG__V13*/ meltfptr[12];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " curenv=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURENV__V10*/ meltfptr[9];
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V15*/ meltfptr[14] =
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3839:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V15*/ meltfptr[14] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3839:/ quasiblock");


      /*_.PROGN___V17*/ meltfptr[15] = /*_.IF___V15*/ meltfptr[14];;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.PROGN___V17*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3839:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3842:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (3) rtup_0__TUPLREC__x1;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x1 */
 /*_.TUPLREC___V19*/ meltfptr[15] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x1;
      meltletrec_1_ptr->rtup_0__TUPLREC__x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x1.nbval = 3;


      /*^putuple */
      /*putupl#1 */
      melt_assertmsg ("putupl [:3842] #1 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V19*/ meltfptr[15]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3842] #1 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V19*/
					      meltfptr[15]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V19*/ meltfptr[15]))->tabval[0] =
	(melt_ptr_t) (( /*!konst_5 */ meltfrout->tabval[5]));
      ;
      /*^putuple */
      /*putupl#2 */
      melt_assertmsg ("putupl [:3842] #2 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V19*/ meltfptr[15]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3842] #2 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V19*/
					      meltfptr[15]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V19*/ meltfptr[15]))->tabval[1] =
	(melt_ptr_t) (( /*!konst_6 */ meltfrout->tabval[6]));
      ;
      /*^putuple */
      /*putupl#3 */
      melt_assertmsg ("putupl [:3842] #3 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V19*/ meltfptr[15]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3842] #3 checkoff",
		      (2 >= 0
		       && 2 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V19*/
					      meltfptr[15]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V19*/ meltfptr[15]))->tabval[2] =
	(melt_ptr_t) (( /*!konst_7 */ meltfrout->tabval[7]));
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V19*/ meltfptr[15]);
      ;
      /*_.TUPLE___V18*/ meltfptr[14] = /*_.TUPLREC___V19*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3842:/ clear");
	    /*clear *//*_.TUPLREC___V19*/ meltfptr[15] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V19*/ meltfptr[15] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3841:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V12*/ meltfptr[11];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.COUTARG__V13*/ meltfptr[12];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.TUPLE___V18*/ meltfptr[14];
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.CURENV__V10*/ meltfptr[9];
      /*_.TRANSLATE_TO_FLAVORED_MODULE__V20*/ meltfptr[15] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATE_TO_FLAVORED_MODULE */ meltfrout->
		      tabval[4])),
		    (melt_ptr_t) ( /*_.INARG__V11*/ meltfptr[10]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3844:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!konst_8_TRUE */ meltfrout->tabval[8]);;

    {
      MELT_LOCATION ("warmelt-modes.melt:3844:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V8*/ meltfptr[4] = /*_.RETURN___V21*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-modes.melt:3832:/ clear");
	   /*clear *//*_.PARMODENV__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.CURENV__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
    /*^clear */
	   /*clear *//*_.INARG__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o1 = 0;
    /*^clear */
	   /*clear *//*_.OUTARG__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o2 = 0;
    /*^clear */
	   /*clear *//*_.COUTARG__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V18*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATE_TO_FLAVORED_MODULE__V20*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V21*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-modes.melt:3829:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:3829:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATETOMODULE_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_60_warmelt_modes_TRANSLATETOMODULE_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_60_warmelt_modes_TRANSLATETOMODULE_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_modes_TRANSLATEDEBUG_DOCMD (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_61_warmelt_modes_TRANSLATEDEBUG_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_61_warmelt_modes_TRANSLATEDEBUG_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    const char *loc_CSTRING__o2;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_61_warmelt_modes_TRANSLATEDEBUG_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_61_warmelt_modes_TRANSLATEDEBUG_DOCMD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_61_warmelt_modes_TRANSLATEDEBUG_DOCMD nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATEDEBUG_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:3858:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3859:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3859:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3859:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3859;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "start translatedebug_docmd cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3859:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3859:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3859:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3860:/ quasiblock");


    /*_.PARMODENV__V9*/ meltfptr[5] = ( /*!konst_1 */ meltfrout->tabval[1]);;
    MELT_LOCATION ("warmelt-modes.melt:3862:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.MODULDATA__V3*/ meltfptr[2])	/*then */
      {
	/*^cond.then */
	/*_.CURENV__V10*/ meltfptr[9] = /*_.MODULDATA__V3*/ meltfptr[2];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:3862:/ cond.else");

	/*_.CURENV__V10*/ meltfptr[9] =
	  ( /*!INITIAL_ENVIRONMENT */ meltfrout->tabval[2]);;
      }
    ;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o0 =
      melt_argument ("arg");;
    /*^compute */
 /*_.INARG__V11*/ meltfptr[10] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o0)));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o1 =
      melt_argument ("output");;
    /*^compute */
 /*_.OUTARG__V12*/ meltfptr[11] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o1)));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o2 =
      melt_argument ("coutput");;
    /*^compute */
 /*_.COUTARG__V13*/ meltfptr[12] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o2)));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3867:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3867:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3867:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3867;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "translatedebug_docmd inarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INARG__V11*/ meltfptr[10];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " outarg=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V12*/ meltfptr[11];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " coutarg=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.COUTARG__V13*/ meltfptr[12];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " curenv=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURENV__V10*/ meltfptr[9];
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V15*/ meltfptr[14] =
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3867:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V15*/ meltfptr[14] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3867:/ quasiblock");


      /*_.PROGN___V17*/ meltfptr[15] = /*_.IF___V15*/ meltfptr[14];;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.PROGN___V17*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3867:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3869:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V12*/ meltfptr[11];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.COUTARG__V13*/ meltfptr[12];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_5 */ meltfrout->tabval[5]);
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.CURENV__V10*/ meltfptr[9];
      /*_.TRANSLATE_TO_FLAVORED_MODULE__V18*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATE_TO_FLAVORED_MODULE */ meltfrout->
		      tabval[4])),
		    (melt_ptr_t) ( /*_.INARG__V11*/ meltfptr[10]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V8*/ meltfptr[4] =
      /*_.TRANSLATE_TO_FLAVORED_MODULE__V18*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-modes.melt:3860:/ clear");
	   /*clear *//*_.PARMODENV__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.CURENV__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
    /*^clear */
	   /*clear *//*_.INARG__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o1 = 0;
    /*^clear */
	   /*clear *//*_.OUTARG__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o2 = 0;
    /*^clear */
	   /*clear *//*_.COUTARG__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATE_TO_FLAVORED_MODULE__V18*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-modes.melt:3870:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!konst_6_TRUE */ meltfrout->tabval[6]);;

    {
      MELT_LOCATION ("warmelt-modes.melt:3870:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-modes.melt:3858:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETURN___V19*/ meltfptr[15];;

    {
      MELT_LOCATION ("warmelt-modes.melt:3858:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V19*/ meltfptr[15] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATEDEBUG_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_61_warmelt_modes_TRANSLATEDEBUG_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_61_warmelt_modes_TRANSLATEDEBUG_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_modes_TRANSLATEQUICKLY_DOCMD (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_62_warmelt_modes_TRANSLATEQUICKLY_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_62_warmelt_modes_TRANSLATEQUICKLY_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    const char *loc_CSTRING__o2;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_62_warmelt_modes_TRANSLATEQUICKLY_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_62_warmelt_modes_TRANSLATEQUICKLY_DOCMD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_62_warmelt_modes_TRANSLATEQUICKLY_DOCMD nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATEQUICKLY_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:3887:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3888:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3888:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3888:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3888;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "start translatequickly_docmd cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3888:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3888:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3888:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3889:/ quasiblock");


    /*_.PARMODENV__V9*/ meltfptr[5] = ( /*!konst_1 */ meltfrout->tabval[1]);;
    MELT_LOCATION ("warmelt-modes.melt:3891:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.MODULDATA__V3*/ meltfptr[2])	/*then */
      {
	/*^cond.then */
	/*_.CURENV__V10*/ meltfptr[9] = /*_.MODULDATA__V3*/ meltfptr[2];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:3891:/ cond.else");

	/*_.CURENV__V10*/ meltfptr[9] =
	  ( /*!INITIAL_ENVIRONMENT */ meltfrout->tabval[2]);;
      }
    ;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o0 =
      melt_argument ("arg");;
    /*^compute */
 /*_.INARG__V11*/ meltfptr[10] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o0)));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o1 =
      melt_argument ("output");;
    /*^compute */
 /*_.OUTARG__V12*/ meltfptr[11] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o1)));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o2 =
      melt_argument ("coutput");;
    /*^compute */
 /*_.COUTARG__V13*/ meltfptr[12] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o2)));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3896:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3896:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3896:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3896;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "translatequickly_docmd inarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INARG__V11*/ meltfptr[10];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " outarg=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V12*/ meltfptr[11];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " coutarg=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.COUTARG__V13*/ meltfptr[12];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " curenv=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURENV__V10*/ meltfptr[9];
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V15*/ meltfptr[14] =
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3896:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V15*/ meltfptr[14] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3896:/ quasiblock");


      /*_.PROGN___V17*/ meltfptr[15] = /*_.IF___V15*/ meltfptr[14];;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.PROGN___V17*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3896:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3898:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V12*/ meltfptr[11];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.COUTARG__V13*/ meltfptr[12];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_5 */ meltfrout->tabval[5]);
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.CURENV__V10*/ meltfptr[9];
      /*_.TRANSLATE_TO_FLAVORED_MODULE__V18*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATE_TO_FLAVORED_MODULE */ meltfrout->
		      tabval[4])),
		    (melt_ptr_t) ( /*_.INARG__V11*/ meltfptr[10]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3899:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!konst_6_TRUE */ meltfrout->tabval[6]);;

    {
      MELT_LOCATION ("warmelt-modes.melt:3899:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V8*/ meltfptr[4] = /*_.RETURN___V19*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-modes.melt:3889:/ clear");
	   /*clear *//*_.PARMODENV__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.CURENV__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
    /*^clear */
	   /*clear *//*_.INARG__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o1 = 0;
    /*^clear */
	   /*clear *//*_.OUTARG__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o2 = 0;
    /*^clear */
	   /*clear *//*_.COUTARG__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATE_TO_FLAVORED_MODULE__V18*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V19*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-modes.melt:3887:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:3887:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATEQUICKLY_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_62_warmelt_modes_TRANSLATEQUICKLY_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_62_warmelt_modes_TRANSLATEQUICKLY_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_modes_TRANSLATEOPTIMIZED_DOCMD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_63_warmelt_modes_TRANSLATEOPTIMIZED_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_63_warmelt_modes_TRANSLATEOPTIMIZED_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    const char *loc_CSTRING__o2;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_63_warmelt_modes_TRANSLATEOPTIMIZED_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_63_warmelt_modes_TRANSLATEOPTIMIZED_DOCMD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_63_warmelt_modes_TRANSLATEOPTIMIZED_DOCMD nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATEOPTIMIZED_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:3915:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3916:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3916:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3916:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3916;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"start translateoptimized_docmd cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3916:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3916:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3916:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3917:/ quasiblock");


    /*_.PARMODENV__V9*/ meltfptr[5] = ( /*!konst_1 */ meltfrout->tabval[1]);;
    MELT_LOCATION ("warmelt-modes.melt:3919:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.MODULDATA__V3*/ meltfptr[2])	/*then */
      {
	/*^cond.then */
	/*_.CURENV__V10*/ meltfptr[9] = /*_.MODULDATA__V3*/ meltfptr[2];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:3919:/ cond.else");

	/*_.CURENV__V10*/ meltfptr[9] =
	  ( /*!INITIAL_ENVIRONMENT */ meltfrout->tabval[2]);;
      }
    ;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o0 =
      melt_argument ("arg");;
    /*^compute */
 /*_.INARG__V11*/ meltfptr[10] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o0)));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o1 =
      melt_argument ("output");;
    /*^compute */
 /*_.OUTARG__V12*/ meltfptr[11] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o1)));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o2 =
      melt_argument ("coutput");;
    /*^compute */
 /*_.COUTARG__V13*/ meltfptr[12] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o2)));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3924:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3924:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3924:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3924;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "translateoptimized_docmd inarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INARG__V11*/ meltfptr[10];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " outarg=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V12*/ meltfptr[11];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " coutarg=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.COUTARG__V13*/ meltfptr[12];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " curenv=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURENV__V10*/ meltfptr[9];
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V15*/ meltfptr[14] =
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3924:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V15*/ meltfptr[14] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3924:/ quasiblock");


      /*_.PROGN___V17*/ meltfptr[15] = /*_.IF___V15*/ meltfptr[14];;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.PROGN___V17*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3924:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3926:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.OUTARG__V12*/ meltfptr[11];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.COUTARG__V13*/ meltfptr[12];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_5 */ meltfrout->tabval[5]);
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.CURENV__V10*/ meltfptr[9];
      /*_.TRANSLATE_TO_FLAVORED_MODULE__V18*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATE_TO_FLAVORED_MODULE */ meltfrout->
		      tabval[4])),
		    (melt_ptr_t) ( /*_.INARG__V11*/ meltfptr[10]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3927:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!konst_6_TRUE */ meltfrout->tabval[6]);;

    {
      MELT_LOCATION ("warmelt-modes.melt:3927:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V8*/ meltfptr[4] = /*_.RETURN___V19*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-modes.melt:3917:/ clear");
	   /*clear *//*_.PARMODENV__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.CURENV__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
    /*^clear */
	   /*clear *//*_.INARG__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o1 = 0;
    /*^clear */
	   /*clear *//*_.OUTARG__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o2 = 0;
    /*^clear */
	   /*clear *//*_.COUTARG__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATE_TO_FLAVORED_MODULE__V18*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V19*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-modes.melt:3915:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:3915:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATEOPTIMIZED_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_63_warmelt_modes_TRANSLATEOPTIMIZED_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_63_warmelt_modes_TRANSLATEOPTIMIZED_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_modes_TRANSLATEFILE_DOCMD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_64_warmelt_modes_TRANSLATEFILE_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_64_warmelt_modes_TRANSLATEFILE_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 35
    melt_ptr_t mcfr_varptr[35];
#define MELTFRAM_NBVARNUM 18
    long mcfr_varnum[18];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_64_warmelt_modes_TRANSLATEFILE_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_64_warmelt_modes_TRANSLATEFILE_DOCMD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 35; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_64_warmelt_modes_TRANSLATEFILE_DOCMD nbval 35*/
  meltfram__.mcfr_nbvar = 35 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATEFILE_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:3944:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-modes.melt:3945:/ locexp");
      debugeputs (("starting translatefile_docmd"));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3946:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3946:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3946:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3946;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "start translatefile_docmd cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3946:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3946:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3946:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3947:/ quasiblock");


    /*_.PARMODENV__V9*/ meltfptr[5] = ( /*!konst_1 */ meltfrout->tabval[1]);;
    MELT_LOCATION ("warmelt-modes.melt:3949:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.MODULDATA__V3*/ meltfptr[2])	/*then */
      {
	/*^cond.then */
	/*_.CURENV__V10*/ meltfptr[9] = /*_.MODULDATA__V3*/ meltfptr[2];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:3949:/ cond.else");

	/*_.CURENV__V10*/ meltfptr[9] =
	  ( /*!INITIAL_ENVIRONMENT */ meltfrout->tabval[2]);;
      }
    ;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o0 =
      melt_argument ("arg");;
    /*^compute */
 /*_.INARG__V11*/ meltfptr[10] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o0)));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o1 =
      melt_argument ("output");;
    /*^compute */
 /*_.OUTARG__V12*/ meltfptr[11] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o1)));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3953:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3953:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3953:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3953;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "translatefile_docmd inarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INARG__V11*/ meltfptr[10];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " outarg=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUTARG__V12*/ meltfptr[11];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " parmodenv=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.PARMODENV__V9*/ meltfptr[5];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = " initial_environment=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & ( /*!INITIAL_ENVIRONMENT */ meltfrout->
				  tabval[2]);
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V14*/ meltfptr[13] =
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3953:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V14*/ meltfptr[13] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3953:/ quasiblock");


      /*_.PROGN___V16*/ meltfptr[14] = /*_.IF___V14*/ meltfptr[13];;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.PROGN___V16*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3953:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V16*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3956:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CURENV__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[4])));;
      MELT_LOCATION ("warmelt-modes.melt:3956:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:3956:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curenv"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (3956) ? (3956) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[13] = /*_.IFELSE___V18*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3956:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3957:/ quasiblock");


    MELT_LOCATION ("warmelt-modes.melt:3958:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L6*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.OUTARG__V12*/ meltfptr[11])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-modes.melt:3958:/ cond");
    /*cond */ if ( /*_#IS_STRING__L6*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_STRING_WITHOUT_SUFFIX__V21*/ meltfptr[20] =
	    /*make_string_without_suffix */
	    (meltgc_new_string_without_suffix
	     ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	      melt_string_str ((melt_ptr_t)
			       ( /*_.OUTARG__V12*/ meltfptr[11])),
	      (".c")));;
	  /*^compute */
	  /*_.BASNAM__V20*/ meltfptr[19] =
	    /*_.MAKE_STRING_WITHOUT_SUFFIX__V21*/ meltfptr[20];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:3958:/ clear");
	     /*clear *//*_.MAKE_STRING_WITHOUT_SUFFIX__V21*/ meltfptr[20] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:3961:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L7*/ meltfnum[0] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.INARG__V11*/ meltfptr[10]))
	     == MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-modes.melt:3961:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L7*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:3962:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#STRING_SUFFIXED__L8*/ meltfnum[7] =
		  /*string_suffixed: */
		  (melt_string_is_ending
		   ((melt_ptr_t) /*_.INARG__V11*/ meltfptr[10], ".melt"));;
		/*^compute */
     /*_#NOT__L9*/ meltfnum[8] =
		  (!( /*_#STRING_SUFFIXED__L8*/ meltfnum[7]));;
		MELT_LOCATION ("warmelt-modes.melt:3962:/ cond");
		/*cond */ if ( /*_#NOT__L9*/ meltfnum[8])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-modes.melt:3963:/ locexp");
			warning (0, "MELT WARNING MSG [#%ld]::: %s - %s",
				 melt_dbgcounter,
				 ("MELT translated input file without .melt suffix"),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.INARG__V11*/
						   meltfptr[10])));
		      }
		      ;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;
     /*_.MAKE_STRING_NAKEDBASENAME__V23*/ meltfptr[22] =
		  (meltgc_new_string_nakedbasename
		   ((meltobject_ptr_t)
		    (( /*!DISCR_STRING */ meltfrout->tabval[3])),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.INARG__V11*/ meltfptr[10]))));;
		MELT_LOCATION ("warmelt-modes.melt:3961:/ quasiblock");


		/*_.PROGN___V24*/ meltfptr[23] =
		  /*_.MAKE_STRING_NAKEDBASENAME__V23*/ meltfptr[22];;
		/*^compute */
		/*_.IFELSE___V22*/ meltfptr[20] =
		  /*_.PROGN___V24*/ meltfptr[23];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:3961:/ clear");
	       /*clear *//*_#STRING_SUFFIXED__L8*/ meltfnum[7] = 0;
		/*^clear */
	       /*clear *//*_#NOT__L9*/ meltfnum[8] = 0;
		/*^clear */
	       /*clear *//*_.MAKE_STRING_NAKEDBASENAME__V23*/ meltfptr[22] =
		  0;
		/*^clear */
	       /*clear *//*_.PROGN___V24*/ meltfptr[23] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:3967:/ locexp");
		  error ("MELT ERROR MSG [#%ld]::: %s", melt_dbgcounter,
			 ("invalid translatefile mode"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:3968:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-modes.melt:3968:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-modes.melt:3966:/ quasiblock");


		/*_.PROGN___V26*/ meltfptr[23] =
		  /*_.RETURN___V25*/ meltfptr[22];;
		/*^compute */
		/*_.IFELSE___V22*/ meltfptr[20] =
		  /*_.PROGN___V26*/ meltfptr[23];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:3961:/ clear");
	       /*clear *//*_.RETURN___V25*/ meltfptr[22] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V26*/ meltfptr[23] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.BASNAM__V20*/ meltfptr[19] = /*_.IFELSE___V22*/ meltfptr[20];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:3958:/ clear");
	     /*clear *//*_#IS_STRING__L7*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[20] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3971:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3971:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3971:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3971;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "translatefile_mode basnam";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BASNAM__V20*/ meltfptr[19];
	      /*_.MELT_DEBUG_FUN__V29*/ meltfptr[20] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V28*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V29*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3971:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V29*/ meltfptr[20] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V28*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3971:/ quasiblock");


      /*_.PROGN___V30*/ meltfptr[20] = /*_.IF___V28*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V27*/ meltfptr[22] = /*_.PROGN___V30*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3971:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V28*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V30*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V27*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:3972:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRING_DYNLOADED_SUFFIXED__L12*/ meltfnum[0] =
      /*string_dynloaded_suffixed: */
      (melt_string_is_ending ((melt_ptr_t) /*_.BASNAM__V20*/ meltfptr[19],
			      MELT_DYNLOADED_SUFFIX));;
    MELT_LOCATION ("warmelt-modes.melt:3972:/ cond");
    /*cond */ if ( /*_#STRING_DYNLOADED_SUFFIXED__L12*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_#OR___L13*/ meltfnum[8] =
	  /*_#STRING_DYNLOADED_SUFFIXED__L12*/ meltfnum[0];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:3972:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#STRING_SUFFIXED__L14*/ meltfnum[7] =
	    /*string_suffixed: */
	    (melt_string_is_ending
	     ((melt_ptr_t) /*_.BASNAM__V20*/ meltfptr[19], ".melt"));;
	  MELT_LOCATION ("warmelt-modes.melt:3972:/ cond");
	  /*cond */ if ( /*_#STRING_SUFFIXED__L14*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*_#OR___L15*/ meltfnum[14] =
		/*_#STRING_SUFFIXED__L14*/ meltfnum[7];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:3972:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

     /*_#STRING_SUFFIXED__L16*/ meltfnum[15] =
		  /*string_suffixed: */
		  (melt_string_is_ending
		   ((melt_ptr_t) /*_.BASNAM__V20*/ meltfptr[19], ".o"));;
		MELT_LOCATION ("warmelt-modes.melt:3972:/ cond");
		/*cond */ if ( /*_#STRING_SUFFIXED__L16*/ meltfnum[15])	/*then */
		  {
		    /*^cond.then */
		    /*_#OR___L17*/ meltfnum[16] =
		      /*_#STRING_SUFFIXED__L16*/ meltfnum[15];;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-modes.melt:3972:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

       /*_#STRING_SUFFIXED__L18*/ meltfnum[17] =
			/*string_suffixed: */
			(melt_string_is_ending
			 ((melt_ptr_t) /*_.BASNAM__V20*/ meltfptr[19],
			  ".c"));;
		      /*^compute */
		      /*_#OR___L17*/ meltfnum[16] =
			/*_#STRING_SUFFIXED__L18*/ meltfnum[17];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-modes.melt:3972:/ clear");
		 /*clear *//*_#STRING_SUFFIXED__L18*/ meltfnum[17] = 0;
		    }
		    ;
		  }
		;
		/*_#OR___L15*/ meltfnum[14] = /*_#OR___L17*/ meltfnum[16];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:3972:/ clear");
	       /*clear *//*_#STRING_SUFFIXED__L16*/ meltfnum[15] = 0;
		/*^clear */
	       /*clear *//*_#OR___L17*/ meltfnum[16] = 0;
	      }
	      ;
	    }
	  ;
	  /*_#OR___L13*/ meltfnum[8] = /*_#OR___L15*/ meltfnum[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:3972:/ clear");
	     /*clear *//*_#STRING_SUFFIXED__L14*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#OR___L15*/ meltfnum[14] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L13*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-modes.melt:3977:/ locexp");
	    error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
		   ("tranlatefile mode needs a base name without suffix"),
		   melt_string_str ((melt_ptr_t)
				    ( /*_.BASNAM__V20*/ meltfptr[19])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:3979:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:3979:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-modes.melt:3976:/ quasiblock");


	  /*_.PROGN___V33*/ meltfptr[32] = /*_.RETURN___V32*/ meltfptr[20];;
	  /*^compute */
	  /*_.IF___V31*/ meltfptr[23] = /*_.PROGN___V33*/ meltfptr[32];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:3972:/ clear");
	     /*clear *//*_.RETURN___V32*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V33*/ meltfptr[32] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V31*/ meltfptr[23] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3981:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.BASNAM__V20*/ meltfptr[19];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.CURENV__V10*/ meltfptr[9];
      /*_.TRANSLATE_TO_C_MODULE_MELT_SOURCES__V34*/ meltfptr[20] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATE_TO_C_MODULE_MELT_SOURCES */ meltfrout->
		      tabval[5])),
		    (melt_ptr_t) ( /*_.INARG__V11*/ meltfptr[10]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:3982:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!konst_6_TRUE */ meltfrout->tabval[6]);;

    {
      MELT_LOCATION ("warmelt-modes.melt:3982:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V19*/ meltfptr[14] = /*_.RETURN___V35*/ meltfptr[32];;

    MELT_LOCATION ("warmelt-modes.melt:3957:/ clear");
	   /*clear *//*_#IS_STRING__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BASNAM__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V27*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#STRING_DYNLOADED_SUFFIXED__L12*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#OR___L13*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IF___V31*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATE_TO_C_MODULE_MELT_SOURCES__V34*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V35*/ meltfptr[32] = 0;
    /*_.LET___V8*/ meltfptr[4] = /*_.LET___V19*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-modes.melt:3947:/ clear");
	   /*clear *//*_.PARMODENV__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.CURENV__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
    /*^clear */
	   /*clear *//*_.INARG__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o1 = 0;
    /*^clear */
	   /*clear *//*_.OUTARG__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LET___V19*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-modes.melt:3944:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:3944:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATEFILE_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_64_warmelt_modes_TRANSLATEFILE_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_64_warmelt_modes_TRANSLATEFILE_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_modes_TRANSLATEINIT_DOCMD (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_65_warmelt_modes_TRANSLATEINIT_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_65_warmelt_modes_TRANSLATEINIT_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 43
    melt_ptr_t mcfr_varptr[43];
#define MELTFRAM_NBVARNUM 17
    long mcfr_varnum[17];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    const char *loc_CSTRING__o2;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_65_warmelt_modes_TRANSLATEINIT_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_65_warmelt_modes_TRANSLATEINIT_DOCMD_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 43; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_65_warmelt_modes_TRANSLATEINIT_DOCMD nbval 43*/
  meltfram__.mcfr_nbvar = 43 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATEINIT_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:3998:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:3999:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:3999:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:3999:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3999;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "start translateinit_mode cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " initial_environment=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & ( /*!INITIAL_ENVIRONMENT */ meltfrout->
				  tabval[1]);
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:3999:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:3999:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:3999:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4001:/ quasiblock");


 /*_.RLIST__V9*/ meltfptr[5] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o0 =
      melt_argument ("arg");;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o1 =
      melt_argument ("arglist");;
    MELT_LOCATION ("warmelt-modes.melt:4005:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o0)	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4006:/ quasiblock");


   /*_.PROGARGSTR__V12*/ meltfptr[11] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	      ( /*_?*/ meltfram__.loc_CSTRING__o0)));;
	  MELT_LOCATION ("warmelt-modes.melt:4008:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#STRING_SUFFIXED__L3*/ meltfnum[1] =
	    /*string_suffixed: */
	    (melt_string_is_ending
	     ((melt_ptr_t) /*_.PROGARGSTR__V12*/ meltfptr[11], ".melt"));;
	  /*^compute */
   /*_#NOT__L4*/ meltfnum[0] =
	    (!( /*_#STRING_SUFFIXED__L3*/ meltfnum[1]));;
	  MELT_LOCATION ("warmelt-modes.melt:4008:/ cond");
	  /*cond */ if ( /*_#NOT__L4*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:4009:/ locexp");
		  warning (0, "MELT WARNING MSG [#%ld]::: %s - %s",
			   melt_dbgcounter,
			   ("MELT translated initial file without .melt suffix"),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PROGARGSTR__V12*/
					     meltfptr[11])));
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*_.LET___V11*/ meltfptr[10] = /*_.PROGARGSTR__V12*/ meltfptr[11];;

	  MELT_LOCATION ("warmelt-modes.melt:4006:/ clear");
	     /*clear *//*_.PROGARGSTR__V12*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_#STRING_SUFFIXED__L3*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_#NOT__L4*/ meltfnum[0] = 0;
	  /*_.INARG__V10*/ meltfptr[9] = /*_.LET___V11*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4005:/ clear");
	     /*clear *//*_.LET___V11*/ meltfptr[10] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4012:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o1)	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MAKE_STRINGCONST__V14*/ meltfptr[10] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_STRING */ meltfrout->tabval[3])),
		    ( /*_?*/ meltfram__.loc_CSTRING__o1)));;
		/*^compute */
     /*_.SPLIT_STRING_COMMA__V15*/ meltfptr[14] =
		  meltgc_new_split_string (melt_string_str
					   ((melt_ptr_t)
					    /*_.MAKE_STRINGCONST__V14*/
					    meltfptr[10]), ',',
					   (melt_ptr_t) ( /*!DISCR_STRING */
							 meltfrout->
							 tabval[3]));;
		/*^compute */
		/*_.IFELSE___V13*/ meltfptr[11] =
		  /*_.SPLIT_STRING_COMMA__V15*/ meltfptr[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4012:/ clear");
	       /*clear *//*_.MAKE_STRINGCONST__V14*/ meltfptr[10] = 0;
		/*^clear */
	       /*clear *//*_.SPLIT_STRING_COMMA__V15*/ meltfptr[14] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:4016:/ locexp");
		  error ("MELT ERROR MSG [#%ld]::: %s", melt_dbgcounter,
			 ("invalid arg or arglist to translateinit mode"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:4017:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-modes.melt:4017:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-modes.melt:4015:/ quasiblock");


		/*_.PROGN___V17*/ meltfptr[14] =
		  /*_.RETURN___V16*/ meltfptr[10];;
		/*^compute */
		/*_.IFELSE___V13*/ meltfptr[11] =
		  /*_.PROGN___V17*/ meltfptr[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4012:/ clear");
	       /*clear *//*_.RETURN___V16*/ meltfptr[10] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V17*/ meltfptr[14] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.INARG__V10*/ meltfptr[9] = /*_.IFELSE___V13*/ meltfptr[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4005:/ clear");
	     /*clear *//*_.IFELSE___V13*/ meltfptr[11] = 0;
	}
	;
      }
    ;
 /*_?*/ meltfram__.loc_CSTRING__o2 =
      melt_argument ("output");;
    /*^compute */
 /*_.OUTARG__V18*/ meltfptr[10] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[3])),
	( /*_?*/ meltfram__.loc_CSTRING__o2)));;
    MELT_LOCATION ("warmelt-modes.melt:4020:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L5*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.OUTARG__V18*/ meltfptr[10])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-modes.melt:4020:/ cond");
    /*cond */ if ( /*_#IS_STRING__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*_.BASNAM__V19*/ meltfptr[14] = /*_.OUTARG__V18*/ meltfptr[10];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:4020:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4021:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L6*/ meltfnum[0] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.INARG__V10*/ meltfptr[9]))
	     == MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-modes.melt:4021:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L6*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MAKE_STRING_NAKEDBASENAME__V21*/ meltfptr[20] =
		  (meltgc_new_string_nakedbasename
		   ((meltobject_ptr_t)
		    (( /*!DISCR_STRING */ meltfrout->tabval[3])),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.INARG__V10*/ meltfptr[9]))));;
		/*^compute */
		/*_.IFELSE___V20*/ meltfptr[11] =
		  /*_.MAKE_STRING_NAKEDBASENAME__V21*/ meltfptr[20];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4021:/ clear");
	       /*clear *//*_.MAKE_STRING_NAKEDBASENAME__V21*/ meltfptr[20] =
		  0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:4023:/ locexp");
		  error ("MELT ERROR MSG [#%ld]::: %s", melt_dbgcounter,
			 ("invalid translateinit mode"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:4024:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-modes.melt:4024:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-modes.melt:4022:/ quasiblock");


		/*_.PROGN___V23*/ meltfptr[22] =
		  /*_.RETURN___V22*/ meltfptr[20];;
		/*^compute */
		/*_.IFELSE___V20*/ meltfptr[11] =
		  /*_.PROGN___V23*/ meltfptr[22];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4021:/ clear");
	       /*clear *//*_.RETURN___V22*/ meltfptr[20] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V23*/ meltfptr[22] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.BASNAM__V19*/ meltfptr[14] = /*_.IFELSE___V20*/ meltfptr[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4020:/ clear");
	     /*clear *//*_#IS_STRING__L6*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[11] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4027:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4027:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4027:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4027;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "translateinit_mode basnam=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BASNAM__V19*/ meltfptr[14];
	      /*_.MELT_DEBUG_FUN__V26*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V25*/ meltfptr[22] =
	      /*_.MELT_DEBUG_FUN__V26*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4027:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V26*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V25*/ meltfptr[22] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4027:/ quasiblock");


      /*_.PROGN___V27*/ meltfptr[11] = /*_.IF___V25*/ meltfptr[22];;
      /*^compute */
      /*_.IFCPP___V24*/ meltfptr[20] = /*_.PROGN___V27*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4027:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V25*/ meltfptr[22] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V27*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V24*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4028:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRING_DYNLOADED_SUFFIXED__L9*/ meltfnum[7] =
      /*string_dynloaded_suffixed: */
      (melt_string_is_ending ((melt_ptr_t) /*_.BASNAM__V19*/ meltfptr[14],
			      MELT_DYNLOADED_SUFFIX));;
    MELT_LOCATION ("warmelt-modes.melt:4028:/ cond");
    /*cond */ if ( /*_#STRING_DYNLOADED_SUFFIXED__L9*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*_#OR___L10*/ meltfnum[0] =
	  /*_#STRING_DYNLOADED_SUFFIXED__L9*/ meltfnum[7];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:4028:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#STRING_SUFFIXED__L11*/ meltfnum[10] =
	    /*string_suffixed: */
	    (melt_string_is_ending
	     ((melt_ptr_t) /*_.BASNAM__V19*/ meltfptr[14], ".melt"));;
	  MELT_LOCATION ("warmelt-modes.melt:4028:/ cond");
	  /*cond */ if ( /*_#STRING_SUFFIXED__L11*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*_#OR___L12*/ meltfnum[11] =
		/*_#STRING_SUFFIXED__L11*/ meltfnum[10];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:4028:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

     /*_#STRING_SUFFIXED__L13*/ meltfnum[12] =
		  /*string_suffixed: */
		  (melt_string_is_ending
		   ((melt_ptr_t) /*_.BASNAM__V19*/ meltfptr[14], ".c"));;
		/*^compute */
		/*_#OR___L12*/ meltfnum[11] =
		  /*_#STRING_SUFFIXED__L13*/ meltfnum[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4028:/ clear");
	       /*clear *//*_#STRING_SUFFIXED__L13*/ meltfnum[12] = 0;
	      }
	      ;
	    }
	  ;
	  /*_#OR___L10*/ meltfnum[0] = /*_#OR___L12*/ meltfnum[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4028:/ clear");
	     /*clear *//*_#STRING_SUFFIXED__L11*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_#OR___L12*/ meltfnum[11] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L10*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-modes.melt:4032:/ locexp");
	    error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
		   ("tranlateinit mode needs a base name without suffix"),
		   melt_string_str ((melt_ptr_t)
				    ( /*_.BASNAM__V19*/ meltfptr[14])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:4034:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:4034:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-modes.melt:4031:/ quasiblock");


	  /*_.PROGN___V30*/ meltfptr[29] = /*_.RETURN___V29*/ meltfptr[11];;
	  /*^compute */
	  /*_.IF___V28*/ meltfptr[22] = /*_.PROGN___V30*/ meltfptr[29];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4028:/ clear");
	     /*clear *//*_.RETURN___V29*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V30*/ meltfptr[29] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V28*/ meltfptr[22] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4037:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L14*/ meltfnum[12] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.INARG__V10*/ meltfptr[9])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-modes.melt:4037:/ cond");
    /*cond */ if ( /*_#IS_STRING__L14*/ meltfnum[12])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.READ_FILE__V32*/ meltfptr[29] =
	    (meltgc_read_file
	     (melt_string_str ((melt_ptr_t) ( /*_.INARG__V10*/ meltfptr[9])),
	      (char *) 0));;
	  MELT_LOCATION ("warmelt-modes.melt:4038:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.READ_FILE__V32*/ meltfptr[29];
	    /*_.LIST_APPEND2LIST__V33*/ meltfptr[32] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_APPEND2LIST */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.RLIST__V9*/ meltfptr[5]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V31*/ meltfptr[11] =
	    /*_.LIST_APPEND2LIST__V33*/ meltfptr[32];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4037:/ clear");
	     /*clear *//*_.READ_FILE__V32*/ meltfptr[29] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_APPEND2LIST__V33*/ meltfptr[32] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4039:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_LIST__L15*/ meltfnum[10] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.INARG__V10*/ meltfptr[9]))
	     == MELTOBMAG_LIST);;
	  MELT_LOCATION ("warmelt-modes.melt:4039:/ cond");
	  /*cond */ if ( /*_#IS_LIST__L15*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:4041:/ quasiblock");


		/*^newclosure */
		     /*newclosure *//*_.LAMBDA___V36*/ meltfptr[35] =
		  (melt_ptr_t)
		  meltgc_new_closure ((meltobject_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (DISCR_CLOSURE)))),
				      (meltroutine_ptr_t) (( /*!konst_8 */
							    meltfrout->
							    tabval[8])), (1));
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V36*/
						   meltfptr[35])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 0 >= 0
				&& 0 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V36*/
						    meltfptr[35])));
		((meltclosure_ptr_t) /*_.LAMBDA___V36*/ meltfptr[35])->
		  tabval[0] = (melt_ptr_t) ( /*_.RLIST__V9*/ meltfptr[5]);
		;
		/*_.LAMBDA___V35*/ meltfptr[32] =
		  /*_.LAMBDA___V36*/ meltfptr[35];;
		MELT_LOCATION ("warmelt-modes.melt:4040:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.LAMBDA___V35*/ meltfptr[32];
		  /*_.LIST_EVERY__V37*/ meltfptr[36] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!LIST_EVERY */ meltfrout->tabval[5])),
				(melt_ptr_t) ( /*_.INARG__V10*/ meltfptr[9]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V34*/ meltfptr[29] =
		  /*_.LIST_EVERY__V37*/ meltfptr[36];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4039:/ clear");
	       /*clear *//*_.LAMBDA___V35*/ meltfptr[32] = 0;
		/*^clear */
	       /*clear *//*_.LIST_EVERY__V37*/ meltfptr[36] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IFELSE___V34*/ meltfptr[29] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IFELSE___V31*/ meltfptr[11] = /*_.IFELSE___V34*/ meltfptr[29];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4037:/ clear");
	     /*clear *//*_#IS_LIST__L15*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V34*/ meltfptr[29] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4050:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L16*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4050:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4050:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L17*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4050;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"after read translateinit_mode rlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RLIST__V9*/ meltfptr[5];
	      /*_.MELT_DEBUG_FUN__V40*/ meltfptr[29] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V39*/ meltfptr[36] =
	      /*_.MELT_DEBUG_FUN__V40*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4050:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V40*/ meltfptr[29] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V39*/ meltfptr[36] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4050:/ quasiblock");


      /*_.PROGN___V41*/ meltfptr[29] = /*_.IF___V39*/ meltfptr[36];;
      /*^compute */
      /*_.IFCPP___V38*/ meltfptr[32] = /*_.PROGN___V41*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4050:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V39*/ meltfptr[36] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V41*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V38*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4056:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.BASNAM__V19*/ meltfptr[14];
      /*_.COMPILE_LIST_SEXPR__V42*/ meltfptr[36] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!COMPILE_LIST_SEXPR */ meltfrout->tabval[9])),
		    (melt_ptr_t) ( /*_.RLIST__V9*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:4059:/ locexp");
      melt_garbcoll ((10000), MELT_NEED_FULL);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4060:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!konst_10_TRUE */ meltfrout->tabval[10]);;

    {
      MELT_LOCATION ("warmelt-modes.melt:4060:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V8*/ meltfptr[4] = /*_.RETURN___V43*/ meltfptr[29];;

    MELT_LOCATION ("warmelt-modes.melt:4001:/ clear");
	   /*clear *//*_.RLIST__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o1 = 0;
    /*^clear */
	   /*clear *//*_.INARG__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o2 = 0;
    /*^clear */
	   /*clear *//*_.OUTARG__V18*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BASNAM__V19*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V24*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#STRING_DYNLOADED_SUFFIXED__L9*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#OR___L10*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V28*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L14*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V31*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V38*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.COMPILE_LIST_SEXPR__V42*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V43*/ meltfptr[29] = 0;
    MELT_LOCATION ("warmelt-modes.melt:3998:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:3998:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATEINIT_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_65_warmelt_modes_TRANSLATEINIT_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_65_warmelt_modes_TRANSLATEINIT_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_modes_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_66_warmelt_modes_LAMBDA___21___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_66_warmelt_modes_LAMBDA___21___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_66_warmelt_modes_LAMBDA___21__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_66_warmelt_modes_LAMBDA___21___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_66_warmelt_modes_LAMBDA___21__ nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4041:/ getarg");
 /*_.CURARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-modes.melt:4042:/ locexp");
      inform (UNKNOWN_LOCATION, "MELT INFORM [#%ld]: %s - %s",
	      melt_dbgcounter, ("reading from file"),
	      melt_string_str ((melt_ptr_t) ( /*_.CURARG__V2*/ meltfptr[1])));
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4043:/ quasiblock");


 /*_.CUREAD__V4*/ meltfptr[3] =
      (meltgc_read_file
       (melt_string_str ((melt_ptr_t) ( /*_.CURARG__V2*/ meltfptr[1])),
	(char *) 0));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4045:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) (( /*~RLIST */ meltfclos->tabval[0])))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-modes.melt:4045:/ cond");
      /*cond */ if ( /*_#IS_LIST__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:4045:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check rlist"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (4045) ? (4045) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4045:/ clear");
	     /*clear *//*_#IS_LIST__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4046:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST_OR_NULL__L2*/ meltfnum[0] =
	(( /*_.CUREAD__V4*/ meltfptr[3]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.CUREAD__V4*/ meltfptr[3])) == MELTOBMAG_LIST));;
      MELT_LOCATION ("warmelt-modes.melt:4046:/ cond");
      /*cond */ if ( /*_#IS_LIST_OR_NULL__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:4046:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curead"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (4046) ? (4046) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4046:/ clear");
	     /*clear *//*_#IS_LIST_OR_NULL__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4047:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4047:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4047:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4047;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "translateinit_mode curead=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CUREAD__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V10*/ meltfptr[9] =
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4047:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V10*/ meltfptr[9] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4047:/ quasiblock");


      /*_.PROGN___V12*/ meltfptr[10] = /*_.IF___V10*/ meltfptr[9];;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.PROGN___V12*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4047:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V10*/ meltfptr[9] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V12*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4048:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.CUREAD__V4*/ meltfptr[3];
      /*_.LIST_APPEND2LIST__V13*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_APPEND2LIST */ meltfrout->tabval[1])),
		    (melt_ptr_t) (( /*~RLIST */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.LIST_APPEND2LIST__V13*/ meltfptr[9];;

    MELT_LOCATION ("warmelt-modes.melt:4043:/ clear");
	   /*clear *//*_.CUREAD__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LIST_APPEND2LIST__V13*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-modes.melt:4041:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-modes.melt:4041:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_66_warmelt_modes_LAMBDA___21___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_66_warmelt_modes_LAMBDA___21__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_modes_TRANSLATEINIT_FLAVORED (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_67_warmelt_modes_TRANSLATEINIT_FLAVORED_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_67_warmelt_modes_TRANSLATEINIT_FLAVORED_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 66
    melt_ptr_t mcfr_varptr[66];
#define MELTFRAM_NBVARNUM 28
    long mcfr_varnum[28];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    const char *loc_CSTRING__o2;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_67_warmelt_modes_TRANSLATEINIT_FLAVORED is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_67_warmelt_modes_TRANSLATEINIT_FLAVORED_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 66; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_67_warmelt_modes_TRANSLATEINIT_FLAVORED nbval 66*/
  meltfram__.mcfr_nbvar = 66 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATEINIT_FLAVORED", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4077:/ getarg");
 /*_.FLAVOR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-modes.melt:4078:/ quasiblock");


 /*_.RLIST__V4*/ meltfptr[3] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[0]))));;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o0 =
      melt_argument ("arg");;
    /*^compute */
 /*_?*/ meltfram__.loc_CSTRING__o1 =
      melt_argument ("arglist");;
    MELT_LOCATION ("warmelt-modes.melt:4082:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o0)	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4083:/ quasiblock");


   /*_.PROGARGSTR__V7*/ meltfptr[6] =
	    (meltgc_new_stringdup
	     ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[1])),
	      ( /*_?*/ meltfram__.loc_CSTRING__o0)));;
	  MELT_LOCATION ("warmelt-modes.melt:4085:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#STRING_SUFFIXED__L1*/ meltfnum[0] =
	    /*string_suffixed: */
	    (melt_string_is_ending
	     ((melt_ptr_t) /*_.PROGARGSTR__V7*/ meltfptr[6], ".melt"));;
	  /*^compute */
   /*_#NOT__L2*/ meltfnum[1] =
	    (!( /*_#STRING_SUFFIXED__L1*/ meltfnum[0]));;
	  MELT_LOCATION ("warmelt-modes.melt:4085:/ cond");
	  /*cond */ if ( /*_#NOT__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:4086:/ locexp");
		  warning (0, "MELT WARNING MSG [#%ld]::: %s - %s",
			   melt_dbgcounter,
			   ("MELT translated initial file without .melt suffix"),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.PROGARGSTR__V7*/
					     meltfptr[6])));
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*_.LET___V6*/ meltfptr[5] = /*_.PROGARGSTR__V7*/ meltfptr[6];;

	  MELT_LOCATION ("warmelt-modes.melt:4083:/ clear");
	     /*clear *//*_.PROGARGSTR__V7*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_#STRING_SUFFIXED__L1*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_#NOT__L2*/ meltfnum[1] = 0;
	  /*_.INARG__V5*/ meltfptr[4] = /*_.LET___V6*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4082:/ clear");
	     /*clear *//*_.LET___V6*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4089:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o1)	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MAKE_STRINGCONST__V9*/ meltfptr[5] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t)
		    (( /*!DISCR_STRING */ meltfrout->tabval[1])),
		    ( /*_?*/ meltfram__.loc_CSTRING__o1)));;
		/*^compute */
     /*_.SPLIT_STRING_COMMA__V10*/ meltfptr[9] =
		  meltgc_new_split_string (melt_string_str
					   ((melt_ptr_t)
					    /*_.MAKE_STRINGCONST__V9*/
					    meltfptr[5]), ',',
					   (melt_ptr_t) ( /*!DISCR_STRING */
							 meltfrout->
							 tabval[1]));;
		/*^compute */
		/*_.IFELSE___V8*/ meltfptr[6] =
		  /*_.SPLIT_STRING_COMMA__V10*/ meltfptr[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4089:/ clear");
	       /*clear *//*_.MAKE_STRINGCONST__V9*/ meltfptr[5] = 0;
		/*^clear */
	       /*clear *//*_.SPLIT_STRING_COMMA__V10*/ meltfptr[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:4093:/ locexp");
		  error ("MELT ERROR MSG [#%ld]::: %s", melt_dbgcounter,
			 ("invalid arg or arglist to translateinitmodule mode"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:4094:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-modes.melt:4094:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-modes.melt:4092:/ quasiblock");


		/*_.PROGN___V12*/ meltfptr[9] =
		  /*_.RETURN___V11*/ meltfptr[5];;
		/*^compute */
		/*_.IFELSE___V8*/ meltfptr[6] =
		  /*_.PROGN___V12*/ meltfptr[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4089:/ clear");
	       /*clear *//*_.RETURN___V11*/ meltfptr[5] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V12*/ meltfptr[9] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.INARG__V5*/ meltfptr[4] = /*_.IFELSE___V8*/ meltfptr[6];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4082:/ clear");
	     /*clear *//*_.IFELSE___V8*/ meltfptr[6] = 0;
	}
	;
      }
    ;
 /*_?*/ meltfram__.loc_CSTRING__o2 =
      melt_argument ("output");;
    /*^compute */
 /*_.OUTARG__V13*/ meltfptr[5] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[1])),
	( /*_?*/ meltfram__.loc_CSTRING__o2)));;
    MELT_LOCATION ("warmelt-modes.melt:4097:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L3*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.OUTARG__V13*/ meltfptr[5])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-modes.melt:4097:/ cond");
    /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*_.BASNAM__V14*/ meltfptr[9] = /*_.OUTARG__V13*/ meltfptr[5];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:4097:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4098:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L4*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.INARG__V5*/ meltfptr[4])) ==
	     MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-modes.melt:4098:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L4*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_.MAKE_STRING_NAKEDBASENAME__V16*/ meltfptr[15] =
		  (meltgc_new_string_nakedbasename
		   ((meltobject_ptr_t)
		    (( /*!DISCR_STRING */ meltfrout->tabval[1])),
		    melt_string_str ((melt_ptr_t)
				     ( /*_.INARG__V5*/ meltfptr[4]))));;
		/*^compute */
		/*_.IFELSE___V15*/ meltfptr[6] =
		  /*_.MAKE_STRING_NAKEDBASENAME__V16*/ meltfptr[15];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4098:/ clear");
	       /*clear *//*_.MAKE_STRING_NAKEDBASENAME__V16*/ meltfptr[15] =
		  0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:4100:/ locexp");
		  error ("MELT ERROR MSG [#%ld]::: %s", melt_dbgcounter,
			 ("invalid translateinitmodule mode"));
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:4101:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-modes.melt:4101:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-modes.melt:4099:/ quasiblock");


		/*_.PROGN___V18*/ meltfptr[17] =
		  /*_.RETURN___V17*/ meltfptr[15];;
		/*^compute */
		/*_.IFELSE___V15*/ meltfptr[6] =
		  /*_.PROGN___V18*/ meltfptr[17];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4098:/ clear");
	       /*clear *//*_.RETURN___V17*/ meltfptr[15] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V18*/ meltfptr[17] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.BASNAM__V14*/ meltfptr[9] = /*_.IFELSE___V15*/ meltfptr[6];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4097:/ clear");
	     /*clear *//*_#IS_STRING__L4*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[6] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4104:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4104:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4104:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4104;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translateinitmodule_flavored basnam=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BASNAM__V14*/ meltfptr[9];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " flavor=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.FLAVOR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V20*/ meltfptr[17] =
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4104:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V20*/ meltfptr[17] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4104:/ quasiblock");


      /*_.PROGN___V22*/ meltfptr[6] = /*_.IF___V20*/ meltfptr[17];;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[15] = /*_.PROGN___V22*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4104:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V20*/ meltfptr[17] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4105:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRING_DYNLOADED_SUFFIXED__L7*/ meltfnum[5] =
      /*string_dynloaded_suffixed: */
      (melt_string_is_ending ((melt_ptr_t) /*_.BASNAM__V14*/ meltfptr[9],
			      MELT_DYNLOADED_SUFFIX));;
    MELT_LOCATION ("warmelt-modes.melt:4105:/ cond");
    /*cond */ if ( /*_#STRING_DYNLOADED_SUFFIXED__L7*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*_#OR___L8*/ meltfnum[1] =
	  /*_#STRING_DYNLOADED_SUFFIXED__L7*/ meltfnum[5];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:4105:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#STRING_SUFFIXED__L9*/ meltfnum[8] =
	    /*string_suffixed: */
	    (melt_string_is_ending
	     ((melt_ptr_t) /*_.BASNAM__V14*/ meltfptr[9], ".melt"));;
	  MELT_LOCATION ("warmelt-modes.melt:4105:/ cond");
	  /*cond */ if ( /*_#STRING_SUFFIXED__L9*/ meltfnum[8])	/*then */
	    {
	      /*^cond.then */
	      /*_#OR___L10*/ meltfnum[9] =
		/*_#STRING_SUFFIXED__L9*/ meltfnum[8];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:4105:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

     /*_#STRING_SUFFIXED__L11*/ meltfnum[10] =
		  /*string_suffixed: */
		  (melt_string_is_ending
		   ((melt_ptr_t) /*_.BASNAM__V14*/ meltfptr[9], ".c"));;
		/*^compute */
		/*_#OR___L10*/ meltfnum[9] =
		  /*_#STRING_SUFFIXED__L11*/ meltfnum[10];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4105:/ clear");
	       /*clear *//*_#STRING_SUFFIXED__L11*/ meltfnum[10] = 0;
	      }
	      ;
	    }
	  ;
	  /*_#OR___L8*/ meltfnum[1] = /*_#OR___L10*/ meltfnum[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4105:/ clear");
	     /*clear *//*_#STRING_SUFFIXED__L9*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_#OR___L10*/ meltfnum[9] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L8*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-modes.melt:4109:/ locexp");
	    error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
		   ("tranlateinit mode needs a base name without suffix"),
		   melt_string_str ((melt_ptr_t)
				    ( /*_.BASNAM__V14*/ meltfptr[9])));
	  }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:4111:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:4111:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-modes.melt:4108:/ quasiblock");


	  /*_.PROGN___V25*/ meltfptr[24] = /*_.RETURN___V24*/ meltfptr[6];;
	  /*^compute */
	  /*_.IF___V23*/ meltfptr[17] = /*_.PROGN___V25*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4105:/ clear");
	     /*clear *//*_.RETURN___V24*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V25*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V23*/ meltfptr[17] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4114:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L12*/ meltfnum[10] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.INARG__V5*/ meltfptr[4])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-modes.melt:4114:/ cond");
    /*cond */ if ( /*_#IS_STRING__L12*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.READ_FILE__V27*/ meltfptr[24] =
	    (meltgc_read_file
	     (melt_string_str ((melt_ptr_t) ( /*_.INARG__V5*/ meltfptr[4])),
	      (char *) 0));;
	  MELT_LOCATION ("warmelt-modes.melt:4115:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.READ_FILE__V27*/ meltfptr[24];
	    /*_.LIST_APPEND2LIST__V28*/ meltfptr[27] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_APPEND2LIST */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.RLIST__V4*/ meltfptr[3]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V26*/ meltfptr[6] =
	    /*_.LIST_APPEND2LIST__V28*/ meltfptr[27];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4114:/ clear");
	     /*clear *//*_.READ_FILE__V27*/ meltfptr[24] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_APPEND2LIST__V28*/ meltfptr[27] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4116:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_LIST__L13*/ meltfnum[8] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.INARG__V5*/ meltfptr[4])) ==
	     MELTOBMAG_LIST);;
	  MELT_LOCATION ("warmelt-modes.melt:4116:/ cond");
	  /*cond */ if ( /*_#IS_LIST__L13*/ meltfnum[8])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:4118:/ quasiblock");


		/*^newclosure */
		     /*newclosure *//*_.LAMBDA___V31*/ meltfptr[30] =
		  (melt_ptr_t)
		  meltgc_new_closure ((meltobject_ptr_t)
				      (((melt_ptr_t)
					(MELT_PREDEF (DISCR_CLOSURE)))),
				      (meltroutine_ptr_t) (( /*!konst_7 */
							    meltfrout->
							    tabval[7])), (1));
		;
		/*^putclosedv */
		/*putclosv */
		melt_assertmsg ("putclosv checkclo",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LAMBDA___V31*/
						   meltfptr[30])) ==
				MELTOBMAG_CLOSURE);
		melt_assertmsg ("putclosv checkoff", 0 >= 0
				&& 0 <
				melt_closure_size ((melt_ptr_t)
						   ( /*_.LAMBDA___V31*/
						    meltfptr[30])));
		((meltclosure_ptr_t) /*_.LAMBDA___V31*/ meltfptr[30])->
		  tabval[0] = (melt_ptr_t) ( /*_.RLIST__V4*/ meltfptr[3]);
		;
		/*_.LAMBDA___V30*/ meltfptr[27] =
		  /*_.LAMBDA___V31*/ meltfptr[30];;
		MELT_LOCATION ("warmelt-modes.melt:4117:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.LAMBDA___V30*/ meltfptr[27];
		  /*_.LIST_EVERY__V32*/ meltfptr[31] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!LIST_EVERY */ meltfrout->tabval[4])),
				(melt_ptr_t) ( /*_.INARG__V5*/ meltfptr[4]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V29*/ meltfptr[24] =
		  /*_.LIST_EVERY__V32*/ meltfptr[31];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4116:/ clear");
	       /*clear *//*_.LAMBDA___V30*/ meltfptr[27] = 0;
		/*^clear */
	       /*clear *//*_.LIST_EVERY__V32*/ meltfptr[31] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IFELSE___V29*/ meltfptr[24] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IFELSE___V26*/ meltfptr[6] = /*_.IFELSE___V29*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4114:/ clear");
	     /*clear *//*_#IS_LIST__L13*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V29*/ meltfptr[24] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4127:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L14*/ meltfnum[9] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4127:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4127:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L15*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4127;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"after read translateinitmodule_flavored rlist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RLIST__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V34*/ meltfptr[31] =
	      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4127:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V35*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V34*/ meltfptr[31] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4127:/ quasiblock");


      /*_.PROGN___V36*/ meltfptr[24] = /*_.IF___V34*/ meltfptr[31];;
      /*^compute */
      /*_.IFCPP___V33*/ meltfptr[27] = /*_.PROGN___V36*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4127:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IF___V34*/ meltfptr[31] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V36*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V33*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4130:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = /*nil */ (melt_ptr_t *) NULL;
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.BASNAM__V14*/ meltfptr[9];
      /*_.COMPILE_LIST_SEXPR__V37*/ meltfptr[31] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!COMPILE_LIST_SEXPR */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.RLIST__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:4133:/ locexp");
      melt_garbcoll ((10000), MELT_NEED_FULL);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4134:/ quasiblock");


    MELT_LOCATION ("warmelt-modes.melt:4135:/ cond");
    /*cond */ if ( /*_.OUTARG__V13*/ meltfptr[5])	/*then */
      {
	/*^cond.then */
	/*_.OUTBASE__V39*/ meltfptr[38] = /*_.OUTARG__V13*/ meltfptr[5];;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:4135:/ cond.else");

	/*_.OUTBASE__V39*/ meltfptr[38] = /*_.BASNAM__V14*/ meltfptr[9];;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4138:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MULTIPLE__L16*/ meltfnum[8] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.FLAVOR__V2*/ meltfptr[1])) ==
       MELTOBMAG_MULTIPLE);;
    MELT_LOCATION ("warmelt-modes.melt:4138:/ cond");
    /*cond */ if ( /*_#IS_MULTIPLE__L16*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.FLAVOR__V2*/
				    meltfptr[1]);
	    for ( /*_#FLAVIX__L17*/ meltfnum[9] = 0;
		 ( /*_#FLAVIX__L17*/ meltfnum[9] >= 0)
		 && ( /*_#FLAVIX__L17*/ meltfnum[9] < meltcit1__EACHTUP_ln);
	/*_#FLAVIX__L17*/ meltfnum[9]++)
	      {
		/*_.CURFLAVOR__V41*/ meltfptr[40] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.FLAVOR__V2*/ meltfptr[1]),
				     /*_#FLAVIX__L17*/ meltfnum[9]);




#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-modes.melt:4142:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L18*/ meltfnum[17] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:4142:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[17])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-modes.melt:4142:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[9];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-modes.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4142;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "translateinitmodule_flavored before module generation basnam=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.BASNAM__V14*/ meltfptr[9];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " outbase=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.OUTBASE__V39*/ meltfptr[38];
			  /*^apply.arg */
			  argtab[7].meltbp_cstring = " curflavor=";
			  /*^apply.arg */
			  argtab[8].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURFLAVOR__V41*/
			    meltfptr[40];
			  /*_.MELT_DEBUG_FUN__V44*/ meltfptr[43] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V43*/ meltfptr[42] =
			  /*_.MELT_DEBUG_FUN__V44*/ meltfptr[43];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4142:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V44*/ meltfptr[43] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V43*/ meltfptr[42] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4142:/ quasiblock");


		  /*_.PROGN___V45*/ meltfptr[43] =
		    /*_.IF___V43*/ meltfptr[42];;
		  /*^compute */
		  /*_.IFCPP___V42*/ meltfptr[41] =
		    /*_.PROGN___V45*/ meltfptr[43];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:4142:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[17] = 0;
		  /*^clear */
		/*clear *//*_.IF___V43*/ meltfptr[42] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V45*/ meltfptr[43] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V42*/ meltfptr[41] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:4144:/ locexp");
		  /*generate_flavored_melt_module */
		    melt_compile_source (melt_string_str
					 ((melt_ptr_t) /*_.BASNAM__V14*/
					  meltfptr[9]),
					 melt_string_str ((melt_ptr_t)
							  /*_.OUTBASE__V39*/
							  meltfptr[38]),
					 NULL, melt_string_str ((melt_ptr_t) /*_.CURFLAVOR__V41*/ meltfptr[40]));	/*generate_flavored_melt_module */
		  ;
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-modes.melt:4145:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L20*/ meltfnum[18] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:4145:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L20*/ meltfnum[18])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L21*/ meltfnum[17] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-modes.melt:4145:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[9];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[17];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-modes.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4145;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "translateinitmodule_flavored after module generation basnam=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.BASNAM__V14*/ meltfptr[9];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " outbase=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.OUTBASE__V39*/ meltfptr[38];
			  /*^apply.arg */
			  argtab[7].meltbp_cstring = " curflavor=";
			  /*^apply.arg */
			  argtab[8].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURFLAVOR__V41*/
			    meltfptr[40];
			  /*_.MELT_DEBUG_FUN__V48*/ meltfptr[47] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V47*/ meltfptr[43] =
			  /*_.MELT_DEBUG_FUN__V48*/ meltfptr[47];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4145:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L21*/ meltfnum[17] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V48*/ meltfptr[47] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V47*/ meltfptr[43] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4145:/ quasiblock");


		  /*_.PROGN___V49*/ meltfptr[47] =
		    /*_.IF___V47*/ meltfptr[43];;
		  /*^compute */
		  /*_.IFCPP___V46*/ meltfptr[42] =
		    /*_.PROGN___V49*/ meltfptr[47];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:4145:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L20*/ meltfnum[18] = 0;
		  /*^clear */
		/*clear *//*_.IF___V47*/ meltfptr[43] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V49*/ meltfptr[47] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V46*/ meltfptr[42] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
		/*_.IFELSE___V40*/ meltfptr[39] =
		  /*_.IFCPP___V46*/ meltfptr[42];;
		if ( /*_#FLAVIX__L17*/ meltfnum[9] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-modes.melt:4139:/ clear");
	      /*clear *//*_.CURFLAVOR__V41*/ meltfptr[40] = 0;
	    /*^clear */
	      /*clear *//*_#FLAVIX__L17*/ meltfnum[9] = 0;
	    /*^clear */
	      /*clear *//*_.IFCPP___V42*/ meltfptr[41] = 0;
	    /*^clear */
	      /*clear *//*_.IFCPP___V46*/ meltfptr[42] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:4138:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4149:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L22*/ meltfnum[17] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.FLAVOR__V2*/ meltfptr[1]))
	     == MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-modes.melt:4149:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L22*/ meltfnum[17])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-modes.melt:4150:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L23*/ meltfnum[18] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:4150:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L23*/ meltfnum[18])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[23] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-modes.melt:4150:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[9];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[23];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-modes.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4150;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "translateinitmodule_flavored before module generation basnam=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.BASNAM__V14*/ meltfptr[9];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " outbase=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.OUTBASE__V39*/ meltfptr[38];
			  /*^apply.arg */
			  argtab[7].meltbp_cstring = " flavor=";
			  /*^apply.arg */
			  argtab[8].meltbp_aptr =
			    (melt_ptr_t *) & /*_.FLAVOR__V2*/ meltfptr[1];
			  /*_.MELT_DEBUG_FUN__V53*/ meltfptr[52] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V52*/ meltfptr[51] =
			  /*_.MELT_DEBUG_FUN__V53*/ meltfptr[52];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4150:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L24*/ meltfnum[23] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V53*/ meltfptr[52] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V52*/ meltfptr[51] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4150:/ quasiblock");


		  /*_.PROGN___V54*/ meltfptr[52] =
		    /*_.IF___V52*/ meltfptr[51];;
		  /*^compute */
		  /*_.IFCPP___V51*/ meltfptr[47] =
		    /*_.PROGN___V54*/ meltfptr[52];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:4150:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L23*/ meltfnum[18] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V52*/ meltfptr[51] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V54*/ meltfptr[52] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V51*/ meltfptr[47] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:4152:/ locexp");
		  /*generate_flavored_melt_module */
		    melt_compile_source (melt_string_str
					 ((melt_ptr_t) /*_.BASNAM__V14*/
					  meltfptr[9]),
					 melt_string_str ((melt_ptr_t)
							  /*_.OUTBASE__V39*/
							  meltfptr[38]),
					 NULL, melt_string_str ((melt_ptr_t) /*_.FLAVOR__V2*/ meltfptr[1]));	/*generate_flavored_melt_module */
		  ;
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-modes.melt:4153:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L25*/ meltfnum[23] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:4153:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L25*/ meltfnum[23])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L26*/ meltfnum[18] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-modes.melt:4153:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[9];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L26*/ meltfnum[18];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-modes.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4153;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "translateinitmodule_flavored after module generation basnam=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.BASNAM__V14*/ meltfptr[9];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " outbase=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.OUTBASE__V39*/ meltfptr[38];
			  /*^apply.arg */
			  argtab[7].meltbp_cstring = " flavor=";
			  /*^apply.arg */
			  argtab[8].meltbp_aptr =
			    (melt_ptr_t *) & /*_.FLAVOR__V2*/ meltfptr[1];
			  /*_.MELT_DEBUG_FUN__V57*/ meltfptr[56] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V56*/ meltfptr[52] =
			  /*_.MELT_DEBUG_FUN__V57*/ meltfptr[56];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4153:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L26*/ meltfnum[18] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V57*/ meltfptr[56] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V56*/ meltfptr[52] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4153:/ quasiblock");


		  /*_.PROGN___V58*/ meltfptr[56] =
		    /*_.IF___V56*/ meltfptr[52];;
		  /*^compute */
		  /*_.IFCPP___V55*/ meltfptr[51] =
		    /*_.PROGN___V58*/ meltfptr[56];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:4153:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L25*/ meltfnum[23] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V56*/ meltfptr[52] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V58*/ meltfptr[56] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V55*/ meltfptr[51] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-modes.melt:4149:/ quasiblock");


		/*_.PROGN___V59*/ meltfptr[52] =
		  /*_.IFCPP___V55*/ meltfptr[51];;
		/*^compute */
		/*_.IFELSE___V50*/ meltfptr[43] =
		  /*_.PROGN___V59*/ meltfptr[52];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4149:/ clear");
	       /*clear *//*_.IFCPP___V51*/ meltfptr[47] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V55*/ meltfptr[51] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V59*/ meltfptr[52] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-modes.melt:4157:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L27*/ meltfnum[18] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-modes.melt:4157:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L27*/ meltfnum[18])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L28*/ meltfnum[23] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-modes.melt:4157:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L28*/ meltfnum[23];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-modes.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4157;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "translateinitmodule_flavored bad flavor=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.FLAVOR__V2*/ meltfptr[1];
			  /*_.MELT_DEBUG_FUN__V62*/ meltfptr[51] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V61*/ meltfptr[47] =
			  /*_.MELT_DEBUG_FUN__V62*/ meltfptr[51];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4157:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L28*/ meltfnum[23] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V62*/ meltfptr[51] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V61*/ meltfptr[47] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4157:/ quasiblock");


		  /*_.PROGN___V63*/ meltfptr[52] =
		    /*_.IF___V61*/ meltfptr[47];;
		  /*^compute */
		  /*_.IFCPP___V60*/ meltfptr[56] =
		    /*_.PROGN___V63*/ meltfptr[52];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:4157:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L27*/ meltfnum[18] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V61*/ meltfptr[47] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V63*/ meltfptr[52] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V60*/ meltfptr[56] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-modes.melt:4158:/ locexp");
		  error ("MELT ERROR MSG [#%ld]::: %s", melt_dbgcounter,
			 ("bad flavor for translating initial module"));
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-modes.melt:4159:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (( /*nil */ NULL))	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V65*/ meltfptr[47] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-modes.melt:4159:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("translateinitmodule_flavored bad flavor"), ("warmelt-modes.melt") ? ("warmelt-modes.melt") : __FILE__, (4159) ? (4159) : __LINE__, __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V65*/ meltfptr[47] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V64*/ meltfptr[51] =
		    /*_.IFELSE___V65*/ meltfptr[47];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:4159:/ clear");
		 /*clear *//*_.IFELSE___V65*/ meltfptr[47] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V64*/ meltfptr[51] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-modes.melt:4156:/ quasiblock");


		/*_.PROGN___V66*/ meltfptr[52] =
		  /*_.IFCPP___V64*/ meltfptr[51];;
		/*^compute */
		/*_.IFELSE___V50*/ meltfptr[43] =
		  /*_.PROGN___V66*/ meltfptr[52];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4149:/ clear");
	       /*clear *//*_.IFCPP___V60*/ meltfptr[56] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V64*/ meltfptr[51] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V66*/ meltfptr[52] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V40*/ meltfptr[39] = /*_.IFELSE___V50*/ meltfptr[43];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4138:/ clear");
	     /*clear *//*_#IS_STRING__L22*/ meltfnum[17] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V50*/ meltfptr[43] = 0;
	}
	;
      }
    ;
    /*_.LET___V38*/ meltfptr[24] = /*_.IFELSE___V40*/ meltfptr[39];;

    MELT_LOCATION ("warmelt-modes.melt:4134:/ clear");
	   /*clear *//*_.OUTBASE__V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_#IS_MULTIPLE__L16*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V40*/ meltfptr[39] = 0;
    /*_.LET___V3*/ meltfptr[2] = /*_.LET___V38*/ meltfptr[24];;

    MELT_LOCATION ("warmelt-modes.melt:4078:/ clear");
	   /*clear *//*_.RLIST__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o0 = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o1 = 0;
    /*^clear */
	   /*clear *//*_.INARG__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_?*/ meltfram__.loc_CSTRING__o2 = 0;
    /*^clear */
	   /*clear *//*_.OUTARG__V13*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L3*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.BASNAM__V14*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#STRING_DYNLOADED_SUFFIXED__L7*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#OR___L8*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V23*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L12*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V26*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V33*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.COMPILE_LIST_SEXPR__V37*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.LET___V38*/ meltfptr[24] = 0;
    MELT_LOCATION ("warmelt-modes.melt:4077:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-modes.melt:4077:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATEINIT_FLAVORED", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_67_warmelt_modes_TRANSLATEINIT_FLAVORED_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_67_warmelt_modes_TRANSLATEINIT_FLAVORED */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_modes_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_68_warmelt_modes_LAMBDA___22___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_68_warmelt_modes_LAMBDA___22___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_68_warmelt_modes_LAMBDA___22__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_68_warmelt_modes_LAMBDA___22___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_68_warmelt_modes_LAMBDA___22__ nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4118:/ getarg");
 /*_.CURARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-modes.melt:4119:/ locexp");
      inform (UNKNOWN_LOCATION, "MELT INFORM [#%ld]: %s - %s",
	      melt_dbgcounter, ("reading from file"),
	      melt_string_str ((melt_ptr_t) ( /*_.CURARG__V2*/ meltfptr[1])));
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4120:/ quasiblock");


 /*_.CUREAD__V4*/ meltfptr[3] =
      (meltgc_read_file
       (melt_string_str ((melt_ptr_t) ( /*_.CURARG__V2*/ meltfptr[1])),
	(char *) 0));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4122:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) (( /*~RLIST */ meltfclos->tabval[0])))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-modes.melt:4122:/ cond");
      /*cond */ if ( /*_#IS_LIST__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:4122:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check rlist"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (4122) ? (4122) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4122:/ clear");
	     /*clear *//*_#IS_LIST__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4123:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST_OR_NULL__L2*/ meltfnum[0] =
	(( /*_.CUREAD__V4*/ meltfptr[3]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.CUREAD__V4*/ meltfptr[3])) == MELTOBMAG_LIST));;
      MELT_LOCATION ("warmelt-modes.melt:4123:/ cond");
      /*cond */ if ( /*_#IS_LIST_OR_NULL__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:4123:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curead"),
				  ("warmelt-modes.melt")
				  ? ("warmelt-modes.melt") : __FILE__,
				  (4123) ? (4123) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4123:/ clear");
	     /*clear *//*_#IS_LIST_OR_NULL__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4124:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4124:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4124:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4124;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"translateinitmodule_flavored curead=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CUREAD__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V10*/ meltfptr[9] =
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4124:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V10*/ meltfptr[9] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4124:/ quasiblock");


      /*_.PROGN___V12*/ meltfptr[10] = /*_.IF___V10*/ meltfptr[9];;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.PROGN___V12*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4124:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V10*/ meltfptr[9] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V12*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4125:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.CUREAD__V4*/ meltfptr[3];
      /*_.LIST_APPEND2LIST__V13*/ meltfptr[9] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_APPEND2LIST */ meltfrout->tabval[1])),
		    (melt_ptr_t) (( /*~RLIST */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V3*/ meltfptr[2] = /*_.LIST_APPEND2LIST__V13*/ meltfptr[9];;

    MELT_LOCATION ("warmelt-modes.melt:4120:/ clear");
	   /*clear *//*_.CUREAD__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LIST_APPEND2LIST__V13*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-modes.melt:4118:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-modes.melt:4118:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_68_warmelt_modes_LAMBDA___22___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_68_warmelt_modes_LAMBDA___22__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_modes_TRANSLATEINITMODULE_DOCMD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_69_warmelt_modes_TRANSLATEINITMODULE_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_69_warmelt_modes_TRANSLATEINITMODULE_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_69_warmelt_modes_TRANSLATEINITMODULE_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_69_warmelt_modes_TRANSLATEINITMODULE_DOCMD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_69_warmelt_modes_TRANSLATEINITMODULE_DOCMD nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATEINITMODULE_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4166:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4167:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4167:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4167:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4167;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"start translateinitmodule_mode cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " initial_environment=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & ( /*!INITIAL_ENVIRONMENT */ meltfrout->
				  tabval[1]);
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4167:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4167:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4167:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4169:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (3) rtup_0__TUPLREC__x2;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x2 */
 /*_.TUPLREC___V9*/ meltfptr[5] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x2;
      meltletrec_1_ptr->rtup_0__TUPLREC__x2.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x2.nbval = 3;


      /*^putuple */
      /*putupl#4 */
      melt_assertmsg ("putupl [:4169] #4 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V9*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:4169] #4 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V9*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V9*/ meltfptr[5]))->tabval[0] =
	(melt_ptr_t) (( /*!konst_3 */ meltfrout->tabval[3]));
      ;
      /*^putuple */
      /*putupl#5 */
      melt_assertmsg ("putupl [:4169] #5 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V9*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:4169] #5 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V9*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V9*/ meltfptr[5]))->tabval[1] =
	(melt_ptr_t) (( /*!konst_4 */ meltfrout->tabval[4]));
      ;
      /*^putuple */
      /*putupl#6 */
      melt_assertmsg ("putupl [:4169] #6 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V9*/ meltfptr[5])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:4169] #6 checkoff",
		      (2 >= 0
		       && 2 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V9*/
					      meltfptr[5]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V9*/ meltfptr[5]))->tabval[2] =
	(melt_ptr_t) (( /*!konst_5 */ meltfrout->tabval[5]));
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V9*/ meltfptr[5]);
      ;
      /*_.TUPLE___V8*/ meltfptr[4] = /*_.TUPLREC___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4169:/ clear");
	    /*clear *//*_.TUPLREC___V9*/ meltfptr[5] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V9*/ meltfptr[5] = 0;
    }				/*end multiallocblock */
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.TRANSLATEINIT_FLAVORED__V10*/ meltfptr[5] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATEINIT_FLAVORED */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.TUPLE___V8*/ meltfptr[4]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4166:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      /*_.TRANSLATEINIT_FLAVORED__V10*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-modes.melt:4166:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATEINIT_FLAVORED__V10*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATEINITMODULE_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_69_warmelt_modes_TRANSLATEINITMODULE_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_69_warmelt_modes_TRANSLATEINITMODULE_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_modes_TRANSLATEINITMODULEQUICKLYBUILT_DOCMD
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_70_warmelt_modes_TRANSLATEINITMODULEQUICKLYBUILT_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_70_warmelt_modes_TRANSLATEINITMODULEQUICKLYBUILT_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_70_warmelt_modes_TRANSLATEINITMODULEQUICKLYBUILT_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_70_warmelt_modes_TRANSLATEINITMODULEQUICKLYBUILT_DOCMD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_70_warmelt_modes_TRANSLATEINITMODULEQUICKLYBUILT_DOCMD nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATEINITMODULEQUICKLYBUILT_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4186:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4187:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4187:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4187:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4187;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"start translateinitmodulequicklybuild_mode cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " initial_environment=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & ( /*!INITIAL_ENVIRONMENT */ meltfrout->
				  tabval[1]);
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4187:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4187:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4187:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4189:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.TRANSLATEINIT_FLAVORED__V8*/ meltfptr[4] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATEINIT_FLAVORED */ meltfrout->tabval[2])),
		    (melt_ptr_t) (( /*!konst_3 */ meltfrout->tabval[3])),
		    (""), (union meltparam_un *) 0, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4186:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      /*_.TRANSLATEINIT_FLAVORED__V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:4186:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATEINIT_FLAVORED__V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATEINITMODULEQUICKLYBUILT_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_70_warmelt_modes_TRANSLATEINITMODULEQUICKLYBUILT_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_70_warmelt_modes_TRANSLATEINITMODULEQUICKLYBUILT_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_modes_TRANSLATEINITMODULEDEBUGNOLINE_DOCMD
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_71_warmelt_modes_TRANSLATEINITMODULEDEBUGNOLINE_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_71_warmelt_modes_TRANSLATEINITMODULEDEBUGNOLINE_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_71_warmelt_modes_TRANSLATEINITMODULEDEBUGNOLINE_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_71_warmelt_modes_TRANSLATEINITMODULEDEBUGNOLINE_DOCMD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_71_warmelt_modes_TRANSLATEINITMODULEDEBUGNOLINE_DOCMD nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATEINITMODULEDEBUGNOLINE_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4204:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4205:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4205:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4205:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4205;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"start translateinitmodulequicklybuild_mode cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " initial_environment=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & ( /*!INITIAL_ENVIRONMENT */ meltfrout->
				  tabval[1]);
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4205:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4205:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4205:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4207:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.TRANSLATEINIT_FLAVORED__V8*/ meltfptr[4] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATEINIT_FLAVORED */ meltfrout->tabval[2])),
		    (melt_ptr_t) (( /*!konst_3 */ meltfrout->tabval[3])),
		    (""), (union meltparam_un *) 0, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4204:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      /*_.TRANSLATEINIT_FLAVORED__V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:4204:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATEINIT_FLAVORED__V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATEINITMODULEDEBUGNOLINE_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_71_warmelt_modes_TRANSLATEINITMODULEDEBUGNOLINE_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_71_warmelt_modes_TRANSLATEINITMODULEDEBUGNOLINE_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_modes_TRANSLATEINITMODULEOPTIMIZED_DOCMD
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_72_warmelt_modes_TRANSLATEINITMODULEOPTIMIZED_DOCMD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_72_warmelt_modes_TRANSLATEINITMODULEOPTIMIZED_DOCMD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_72_warmelt_modes_TRANSLATEINITMODULEOPTIMIZED_DOCMD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_72_warmelt_modes_TRANSLATEINITMODULEOPTIMIZED_DOCMD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_72_warmelt_modes_TRANSLATEINITMODULEOPTIMIZED_DOCMD nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("TRANSLATEINITMODULEOPTIMIZED_DOCMD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4222:/ getarg");
 /*_.CMD__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODULDATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODULDATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4223:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4223:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4223:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4223;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"start translateinitmodulequicklybuild_mode cmd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CMD__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " moduldata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODULDATA__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " initial_environment=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & ( /*!INITIAL_ENVIRONMENT */ meltfrout->
				  tabval[1]);
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4223:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4223:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4223:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4225:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.TRANSLATEINIT_FLAVORED__V8*/ meltfptr[4] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!TRANSLATEINIT_FLAVORED */ meltfrout->tabval[2])),
		    (melt_ptr_t) (( /*!konst_3 */ meltfrout->tabval[3])),
		    (""), (union meltparam_un *) 0, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4222:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      /*_.TRANSLATEINIT_FLAVORED__V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:4222:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.TRANSLATEINIT_FLAVORED__V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("TRANSLATEINITMODULEOPTIMIZED_DOCMD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_72_warmelt_modes_TRANSLATEINITMODULEOPTIMIZED_DOCMD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_72_warmelt_modes_TRANSLATEINITMODULEOPTIMIZED_DOCMD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_modes_REGISTER_INPUT_CHANNEL_HANDLER (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_73_warmelt_modes_REGISTER_INPUT_CHANNEL_HANDLER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_73_warmelt_modes_REGISTER_INPUT_CHANNEL_HANDLER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_73_warmelt_modes_REGISTER_INPUT_CHANNEL_HANDLER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_73_warmelt_modes_REGISTER_INPUT_CHANNEL_HANDLER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_73_warmelt_modes_REGISTER_INPUT_CHANNEL_HANDLER nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_INPUT_CHANNEL_HANDLER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4241:/ getarg");
 /*_.CLOS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DATA__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DATA__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#INCHFD__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4252:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4252:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4252:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4252;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"register_input_channel_handler clos=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CLOS__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " data=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DATA__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " inchfd=";
	      /*^apply.arg */
	      argtab[8].meltbp_long = /*_#INCHFD__L1*/ meltfnum[0];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4252:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4252:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4252:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4254:/ quasiblock");


 /*_#GOODINCHFD__L4*/ meltfnum[2] = 0;;
    MELT_LOCATION ("warmelt-modes.melt:4255:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L5*/ meltfnum[1] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CLOS__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-modes.melt:4255:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#I__L7*/ meltfnum[6] =
	    (( /*_#INCHFD__L1*/ meltfnum[0]) >= (0));;
	  /*^compute */
	  /*_#IF___L6*/ meltfnum[5] = /*_#I__L7*/ meltfnum[6];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4255:/ clear");
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L6*/ meltfnum[5] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4255:/ cond");
    /*cond */ if ( /*_#IF___L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-modes.melt:4259:/ locexp");
	    /*register_input_channel_handler STATINCHFDCHK__1 */
	    {
	      struct stat STATINCHFDCHK__1_stat;
	      memset (&STATINCHFDCHK__1_stat, 0, sizeof (struct stat));
	      if (fstat
		  ( /*_#INCHFD__L1*/ meltfnum[0], &STATINCHFDCHK__1_stat))
		warning (0, "MELT bad registered input channel fd#%d - %s",
			 (int) /*_#INCHFD__L1*/ meltfnum[0],
			 xstrerror (errno));
	      else
	 /*_#GOODINCHFD__L4*/ meltfnum[2] = 1;
	    } /* end STATINCHFDCHK__1 */ ;
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4269:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NOT__L8*/ meltfnum[6] =
      (!( /*_#GOODINCHFD__L4*/ meltfnum[2]));;
    MELT_LOCATION ("warmelt-modes.melt:4269:/ cond");
    /*cond */ if ( /*_#NOT__L8*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^quasiblock */


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:4269:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IF___V9*/ meltfptr[5] = /*_.RETURN___V10*/ meltfptr[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4269:/ clear");
	     /*clear *//*_.RETURN___V10*/ meltfptr[9] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V9*/ meltfptr[5] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V8*/ meltfptr[4] = /*_.IF___V9*/ meltfptr[5];;

    MELT_LOCATION ("warmelt-modes.melt:4254:/ clear");
	   /*clear *//*_#GOODINCHFD__L4*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#IF___L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L8*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IF___V9*/ meltfptr[5] = 0;
    MELT_LOCATION ("warmelt-modes.melt:4270:/ quasiblock");


 /*_.INSBUF__V11*/ meltfptr[9] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[1])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-modes.melt:4271:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_INPUT_CHANNEL_HANDLER */ meltfrout->tabval[2])), (4), "CLASS_INPUT_CHANNEL_HANDLER");
  /*_.INST__V13*/ meltfptr[12] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @INCH_SBUF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V13*/ meltfptr[12])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V13*/ meltfptr[12]), (1),
			  ( /*_.INSBUF__V11*/ meltfptr[9]), "INCH_SBUF");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @INCH_CLOS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V13*/ meltfptr[12])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V13*/ meltfptr[12]), (2),
			  ( /*_.CLOS__V2*/ meltfptr[1]), "INCH_CLOS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @INCH_DATA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V13*/ meltfptr[12])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V13*/ meltfptr[12]), (3),
			  ( /*_.DATA__V3*/ meltfptr[2]), "INCH_DATA");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V13*/ meltfptr[12],
				  "newly made instance");
    ;
    /*_.INCHDLR__V12*/ meltfptr[5] = /*_.INST__V13*/ meltfptr[12];;
    MELT_LOCATION ("warmelt-modes.melt:4275:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!INITIAL_SYSTEM_DATA */
					  meltfrout->tabval[3])),
					(melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
			   tabval[3])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 36, "SYSDATA_INCHANNEL_DATA");
   /*_.INBUCK__V14*/ meltfptr[13] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.INBUCK__V14*/ meltfptr[13] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:4277:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.INCHDLR__V12*/ meltfptr[5]),
		    ( /*_#INCHFD__L1*/ meltfnum[0]));
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4278:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_BUCKETLONG__L9*/ meltfnum[2] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.INBUCK__V14*/ meltfptr[13])) ==
       MELTOBMAG_BUCKETLONGS);;
    /*^compute */
 /*_#NOT__L10*/ meltfnum[1] =
      (!( /*_#IS_BUCKETLONG__L9*/ meltfnum[2]));;
    MELT_LOCATION ("warmelt-modes.melt:4278:/ cond");
    /*cond */ if ( /*_#NOT__L10*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4279:/ quasiblock");


   /*_.NEWINBUCK__V17*/ meltfptr[16] =
	    (meltgc_new_longsbucket
	     ((meltobject_ptr_t)
	      ( /*!DISCR_BUCKET_LONGS */ meltfrout->tabval[5]), (50)));;
	  MELT_LOCATION ("warmelt-modes.melt:4281:/ compute");
	  /*_.INBUCK__V14*/ meltfptr[13] = /*_.SETQ___V18*/ meltfptr[17] =
	    /*_.NEWINBUCK__V17*/ meltfptr[16];;
	  /*_.LET___V16*/ meltfptr[15] = /*_.SETQ___V18*/ meltfptr[17];;

	  MELT_LOCATION ("warmelt-modes.melt:4279:/ clear");
	     /*clear *//*_.NEWINBUCK__V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V18*/ meltfptr[17] = 0;
	  /*_.IF___V15*/ meltfptr[14] = /*_.LET___V16*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4278:/ clear");
	     /*clear *//*_.LET___V16*/ meltfptr[15] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V15*/ meltfptr[14] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4282:/ quasiblock");


 /*_.UPDATEDINBUCK__V19*/ meltfptr[16] =
      meltgc_longsbucket_put ((melt_ptr_t) /*_.INBUCK__V14*/ meltfptr[13],
			      ( /*_#INCHFD__L1*/ meltfnum[0]),
			      (melt_ptr_t) /*_.INCHDLR__V12*/ meltfptr[5]);;
    MELT_LOCATION ("warmelt-modes.melt:4284:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!INITIAL_SYSTEM_DATA */
					  meltfrout->tabval[3])),
					(melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @SYSDATA_INCHANNEL_DATA",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*!INITIAL_SYSTEM_DATA */
					      meltfrout->tabval[3]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				  tabval[3])), (36),
				( /*_.UPDATEDINBUCK__V19*/ meltfptr[16]),
				"SYSDATA_INCHANNEL_DATA");
	  ;
	  /*^touch */
	  meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[3]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */
					 meltfrout->tabval[3]), "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-modes.melt:4287:/ locexp");
      /* FCNTLINCHFDCHK__1 start */
      if (fcntl ( /*_#INCHFD__L1*/ meltfnum[0], F_SETOWN, getpid ()))
	melt_fatal_error
	  ("MELT failed to set ownership (F_SETOWN) fd #%d - %s",
	   (int) /*_#INCHFD__L1*/ meltfnum[0], xstrerror (errno));
      /* end  FCNTLINCHFDCHK__1 */
      ;
    }
    ;

    MELT_LOCATION ("warmelt-modes.melt:4282:/ clear");
	   /*clear *//*_.UPDATEDINBUCK__V19*/ meltfptr[16] = 0;

    MELT_LOCATION ("warmelt-modes.melt:4270:/ clear");
	   /*clear *//*_.INSBUF__V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.INCHDLR__V12*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.INBUCK__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#IS_BUCKETLONG__L9*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L10*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-modes.melt:4241:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_INPUT_CHANNEL_HANDLER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_73_warmelt_modes_REGISTER_INPUT_CHANNEL_HANDLER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_73_warmelt_modes_REGISTER_INPUT_CHANNEL_HANDLER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_modes_HANDLE_ALARM_SIGNAL (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_74_warmelt_modes_HANDLE_ALARM_SIGNAL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_74_warmelt_modes_HANDLE_ALARM_SIGNAL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 41
    melt_ptr_t mcfr_varptr[41];
#define MELTFRAM_NBVARNUM 19
    long mcfr_varnum[19];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_74_warmelt_modes_HANDLE_ALARM_SIGNAL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_74_warmelt_modes_HANDLE_ALARM_SIGNAL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 41; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_74_warmelt_modes_HANDLE_ALARM_SIGNAL nbval 41*/
  meltfram__.mcfr_nbvar = 41 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("HANDLE_ALARM_SIGNAL", meltcallcount);
/*getargs*/
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
  MELT_LOCATION ("warmelt-modes.melt:4299:/ block");
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-modes.melt:4300:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!ALARM_BUCKET_REFERENCE */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!ALARM_BUCKET_REFERENCE */ meltfrout->
			   tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.ALARMBUCK__V2*/ meltfptr[1] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ALARMBUCK__V2*/ meltfptr[1] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4301:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_REFERENCE */
					     meltfrout->tabval[1])), (1),
			      "CLASS_REFERENCE");
  /*_.INST__V4*/ meltfptr[3] =
	newobj;
    };
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V4*/ meltfptr[3],
				  "newly made instance");
    ;
    /*_.GOTALARM_CONT__V3*/ meltfptr[2] = /*_.INST__V4*/ meltfptr[3];;
    /*^compute */
 /*_#CURELTIM__L1*/ meltfnum[0] =
      melt_relative_time_millisec ();;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4304:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4304:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4304:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4304;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"handle_alarm_signal start alarmbuck=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ALARMBUCK__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " cureltim=";
	      /*^apply.arg */
	      argtab[6].meltbp_long = /*_#CURELTIM__L1*/ meltfnum[0];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4304:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4304:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4304:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4305:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ALARMBUCK__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:4305:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^quasiblock */


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:4305:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*^quasiblock */


	  /*_.PROGN___V11*/ meltfptr[10] = /*_.RETURN___V10*/ meltfptr[6];;
	  /*^compute */
	  /*_.IFELSE___V9*/ meltfptr[5] = /*_.PROGN___V11*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4305:/ clear");
	     /*clear *//*_.RETURN___V10*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[10] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4306:/ loop");
    /*loop */
    {
    labloop_HANDLELOOP_1:;	/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-modes.melt:4307:/ cppif.then");
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#IS_BUCKETLONG__L4*/ meltfnum[2] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.ALARMBUCK__V2*/ meltfptr[1])) ==
	     MELTOBMAG_BUCKETLONGS);;
	  MELT_LOCATION ("warmelt-modes.melt:4307:/ cond");
	  /*cond */ if ( /*_#IS_BUCKETLONG__L4*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:4307:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {




		{
		  /*^locexp */
		  melt_assert_failed (("check alarmbuck"),
				      ("warmelt-modes.melt")
				      ? ("warmelt-modes.melt") : __FILE__,
				      (4307) ? (4307) : __LINE__,
				      __FUNCTION__);
		  ;
		}
		;
		 /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IFCPP___V14*/ meltfptr[13] = /*_.IFELSE___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4307:/ clear");
	       /*clear *//*_#IS_BUCKETLONG__L4*/ meltfnum[2] = 0;
	  /*^clear */
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	MELT_LOCATION ("warmelt-modes.melt:4309:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-modes.melt:4311:/ quasiblock");


	/*^newclosure */
		   /*newclosure *//*_.LAMBDA___V17*/ meltfptr[16] =
	  (melt_ptr_t)
	  meltgc_new_closure ((meltobject_ptr_t)
			      (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			      (meltroutine_ptr_t) (( /*!konst_5 */ meltfrout->
						    tabval[5])), (1));
	;
	/*^putclosedv */
	/*putclosv */
	melt_assertmsg ("putclosv checkclo",
			melt_magic_discr ((melt_ptr_t)
					  ( /*_.LAMBDA___V17*/ meltfptr[16]))
			== MELTOBMAG_CLOSURE);
	melt_assertmsg ("putclosv checkoff", 0 >= 0
			&& 0 <
			melt_closure_size ((melt_ptr_t)
					   ( /*_.LAMBDA___V17*/
					    meltfptr[16])));
	((meltclosure_ptr_t) /*_.LAMBDA___V17*/ meltfptr[16])->tabval[0] =
	  (melt_ptr_t) ( /*_.GOTALARM_CONT__V3*/ meltfptr[2]);
	;
	/*_.LAMBDA___V16*/ meltfptr[14] = /*_.LAMBDA___V17*/ meltfptr[16];;
	MELT_LOCATION ("warmelt-modes.melt:4309:/ cond");
	/*cond */ if (
		       /*ifisa */
		       melt_is_instance_of ((melt_ptr_t)
					    (( /*!INITIAL_SYSTEM_DATA */
					      meltfrout->tabval[3])),
					    (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[6])))
	  )			/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @SYSDATA_ALARM_HOOK",
			      melt_magic_discr ((melt_ptr_t)
						(( /*!INITIAL_SYSTEM_DATA */
						  meltfrout->tabval[3]))) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				      tabval[3])), (37),
				    ( /*_.LAMBDA___V16*/ meltfptr[14]),
				    "SYSDATA_ALARM_HOOK");
	      ;
	      /*^touch */
	      meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
			     tabval[3]));
	      ;
	      /*^touchobj */

	      melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */
					     meltfrout->tabval[3]),
					    "put-fields");
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;
	MELT_LOCATION ("warmelt-modes.melt:4312:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#BUCKETLONG_COUNT__L5*/ meltfnum[1] =
	  melt_longsbucket_count ((melt_ptr_t) /*_.ALARMBUCK__V2*/
				  meltfptr[1]);;
	MELT_LOCATION ("warmelt-modes.melt:4312:/ cond");
	/*cond */ if ( /*_#BUCKETLONG_COUNT__L5*/ meltfnum[1])	/*then */
	  {
	    /*^cond.then */
	    /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-modes.melt:4312:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {

	      /*^quasiblock */


	      /*^compute */
     /*_.HANDLELOOP__V13*/ meltfptr[10] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_HANDLELOOP_1;
	      }
	      ;
	      /*^quasiblock */


	      /*epilog */
	    }
	    ;
	  }
	;
	MELT_LOCATION ("warmelt-modes.melt:4313:/ quasiblock");


   /*_#NEXTIM__L6*/ meltfnum[2] =
	  melt_longsbucket_nth_key ((melt_ptr_t) /*_.ALARMBUCK__V2*/
				    meltfptr[1], (int) 0);;
	/*^compute */
   /*_.NEXTALHD__V19*/ meltfptr[18] =
	  melt_longsbucket_nth_val ((melt_ptr_t) /*_.ALARMBUCK__V2*/
				    meltfptr[1], (int) 0);;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-modes.melt:4316:/ cppif.then");
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	    melt_dbgcounter++;
#endif
	    ;
	  }
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#MELT_NEED_DBG__L7*/ meltfnum[6] =
	    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	    0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	    ;;
	  MELT_LOCATION ("warmelt-modes.melt:4316:/ cond");
	  /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

       /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		  meltcallcount	/* the_meltcallcount */
#else
		  0L
#endif /* meltcallcount the_meltcallcount */
		  ;;
		MELT_LOCATION ("warmelt-modes.melt:4316:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[7];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_long =
		    /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		  /*^apply.arg */
		  argtab[1].meltbp_cstring = "warmelt-modes.melt";
		  /*^apply.arg */
		  argtab[2].meltbp_long = 4316;
		  /*^apply.arg */
		  argtab[3].meltbp_cstring = "handle_alarm_signal nextim=";
		  /*^apply.arg */
		  argtab[4].meltbp_long = /*_#NEXTIM__L6*/ meltfnum[2];
		  /*^apply.arg */
		  argtab[5].meltbp_cstring = " nextalhd=";
		  /*^apply.arg */
		  argtab[6].meltbp_aptr =
		    (melt_ptr_t *) & /*_.NEXTALHD__V19*/ meltfptr[18];
		  /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!MELT_DEBUG_FUN */ meltfrout->
				  tabval[2])),
				(melt_ptr_t) (( /*nil */ NULL)),
				(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				 MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V21*/ meltfptr[20] =
		  /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4316:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		/*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

      /*_.IF___V21*/ meltfptr[20] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:4316:/ quasiblock");


	  /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
	  /*^compute */
	  /*_.IFCPP___V20*/ meltfptr[19] = /*_.PROGN___V23*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4316:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[6] = 0;
	  /*^clear */
	       /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
	  /*^clear */
	       /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	MELT_LOCATION ("warmelt-modes.melt:4317:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*^cond */
	/*cond */ if ( /*_#NEXTIM__L6*/ meltfnum[2])	/*then */
	  {
	    /*^cond.then */
	    /*_.IFELSE___V24*/ meltfptr[20] = ( /*nil */ NULL);;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-modes.melt:4317:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {

	      /*^quasiblock */


	      /*^compute */
     /*_.HANDLELOOP__V13*/ meltfptr[10] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_HANDLELOOP_1;
	      }
	      ;
	      /*^quasiblock */


	      /*epilog */
	    }
	    ;
	  }
	;
	MELT_LOCATION ("warmelt-modes.melt:4318:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#I__L9*/ meltfnum[7] =
	  (( /*_#NEXTIM__L6*/ meltfnum[2]) >
	   ( /*_#CURELTIM__L1*/ meltfnum[0]));;
	MELT_LOCATION ("warmelt-modes.melt:4318:/ cond");
	/*cond */ if ( /*_#I__L9*/ meltfnum[7])	/*then */
	  {
	    /*^cond.then */
	    /*_.IFELSE___V25*/ meltfptr[21] = ( /*nil */ NULL);;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-modes.melt:4318:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {

	      /*^quasiblock */


	      /*^compute */
     /*_.HANDLELOOP__V13*/ meltfptr[10] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_HANDLELOOP_1;
	      }
	      ;
	      /*^quasiblock */


	      /*epilog */
	    }
	    ;
	  }
	;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-modes.melt:4319:/ cppif.then");
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#IS_A__L10*/ meltfnum[6] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.NEXTALHD__V19*/ meltfptr[18]),
				 (melt_ptr_t) (( /*!CLASS_ALARM_HANDLER */
						meltfrout->tabval[7])));;
	  MELT_LOCATION ("warmelt-modes.melt:4319:/ cond");
	  /*cond */ if ( /*_#IS_A__L10*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V27*/ meltfptr[26] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:4319:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {




		{
		  /*^locexp */
		  melt_assert_failed (("check mextalhd"),
				      ("warmelt-modes.melt")
				      ? ("warmelt-modes.melt") : __FILE__,
				      (4319) ? (4319) : __LINE__,
				      __FUNCTION__);
		  ;
		}
		;
		 /*clear *//*_.IFELSE___V27*/ meltfptr[26] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*^compute */
	  /*_.IFCPP___V26*/ meltfptr[25] = /*_.IFELSE___V27*/ meltfptr[26];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4319:/ clear");
	       /*clear *//*_#IS_A__L10*/ meltfnum[6] = 0;
	  /*^clear */
	       /*clear *//*_.IFELSE___V27*/ meltfptr[26] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V26*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	/*^compute */
   /*_.BUCKETLONG_REMOVE__V28*/ meltfptr[26] =
	  meltgc_longsbucket_remove ((melt_ptr_t) /*_.ALARMBUCK__V2*/
				     meltfptr[1],
				     ( /*_#NEXTIM__L6*/ meltfnum[2]));;
	MELT_LOCATION ("warmelt-modes.melt:4320:/ compute");
	/*_.ALARMBUCK__V2*/ meltfptr[1] = /*_.SETQ___V29*/ meltfptr[28] =
	  /*_.BUCKETLONG_REMOVE__V28*/ meltfptr[26];;
	MELT_LOCATION ("warmelt-modes.melt:4321:/ quasiblock");


	/*^cond */
	/*cond */ if (
		       /*ifisa */
		       melt_is_instance_of ((melt_ptr_t)
					    ( /*_.NEXTALHD__V19*/
					     meltfptr[18]),
					    (melt_ptr_t) (( /*!CLASS_ALARM_HANDLER */ meltfrout->tabval[7])))
	  )			/*then */
	  {
	    /*^cond.then */
	    /*^getslot */
	    {
	      melt_ptr_t slot = NULL, obj = NULL;
	      obj =
		(melt_ptr_t) ( /*_.NEXTALHD__V19*/ meltfptr[18]) /*=obj*/ ;
	      melt_object_get_field (slot, obj, 2, "ALARMH_CLOS");
     /*_.CLOS__V31*/ meltfptr[30] = slot;
	    };
	    ;
	  }
	else
	  {			/*^cond.else */

    /*_.CLOS__V31*/ meltfptr[30] = NULL;;
	  }
	;
	MELT_LOCATION ("warmelt-modes.melt:4323:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#IS_CLOSURE__L11*/ meltfnum[6] =
	  (melt_magic_discr ((melt_ptr_t) ( /*_.CLOS__V31*/ meltfptr[30])) ==
	   MELTOBMAG_CLOSURE);;
	MELT_LOCATION ("warmelt-modes.melt:4323:/ cond");
	/*cond */ if ( /*_#IS_CLOSURE__L11*/ meltfnum[6])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      MELT_LOCATION ("warmelt-modes.melt:4324:/ quasiblock");


	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		/*_.RES__V34*/ meltfptr[33] =
		  melt_apply ((meltclosure_ptr_t)
			      ( /*_.CLOS__V31*/ meltfptr[30]),
			      (melt_ptr_t) ( /*_.NEXTALHD__V19*/
					    meltfptr[18]), (""),
			      (union meltparam_un *) 0, "",
			      (union meltparam_un *) 0);
	      }
	      ;
	      MELT_LOCATION ("warmelt-modes.melt:4326:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^cond */
	      /*cond */ if ( /*_.RES__V34*/ meltfptr[33])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-modes.melt:4327:/ quasiblock");


		    /*^cond */
		    /*cond */ if (
				   /*ifisa */
				   melt_is_instance_of ((melt_ptr_t)
							( /*_.NEXTALHD__V19*/
							 meltfptr[18]),
							(melt_ptr_t) (( /*!CLASS_ALARM_HANDLER */ meltfrout->tabval[7])))
		      )		/*then */
		      {
			/*^cond.then */
			/*^getslot */
			{
			  melt_ptr_t slot = NULL, obj = NULL;
			  obj =
			    (melt_ptr_t) ( /*_.NEXTALHD__V19*/ meltfptr[18])
			    /*=obj*/ ;
			  melt_object_get_field (slot, obj, 1,
						 "ALARMH_PERIOD");
	 /*_.ALARMH_PERIOD__V37*/ meltfptr[36] = slot;
			};
			;
		      }
		    else
		      {		/*^cond.else */

	/*_.ALARMH_PERIOD__V37*/ meltfptr[36] = NULL;;
		      }
		    ;
		    /*^compute */
       /*_#NEWPERIOD__L12*/ meltfnum[11] =
		      (melt_get_int
		       ((melt_ptr_t)
			( /*_.ALARMH_PERIOD__V37*/ meltfptr[36])));;
		    MELT_LOCATION ("warmelt-modes.melt:4329:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#I__L13*/ meltfnum[12] =
		      (( /*_#NEWPERIOD__L12*/ meltfnum[11]) > (10));;
		    MELT_LOCATION ("warmelt-modes.melt:4329:/ cond");
		    /*cond */ if ( /*_#I__L13*/ meltfnum[12])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

	 /*_#I__L14*/ meltfnum[13] =
			    (( /*_#NEWPERIOD__L12*/ meltfnum[11]) +
			     ( /*_#CURELTIM__L1*/ meltfnum[0]));;
			  /*^compute */
	 /*_.BUCKETLONG_PUT__V39*/ meltfptr[38] =
			    meltgc_longsbucket_put ((melt_ptr_t)
						    /*_.ALARMBUCK__V2*/
						    meltfptr[1],
						    ( /*_#I__L14*/
						     meltfnum[13]),
						    (melt_ptr_t)
						    /*_.NEXTALHD__V19*/
						    meltfptr[18]);;
			  MELT_LOCATION ("warmelt-modes.melt:4330:/ compute");
			  /*_.ALARMBUCK__V2*/ meltfptr[1] =
			    /*_.SETQ___V40*/ meltfptr[39] =
			    /*_.BUCKETLONG_PUT__V39*/ meltfptr[38];;
			  /*_.IF___V38*/ meltfptr[37] =
			    /*_.SETQ___V40*/ meltfptr[39];;
			  /*epilog */

			  MELT_LOCATION ("warmelt-modes.melt:4329:/ clear");
		   /*clear *//*_#I__L14*/ meltfnum[13] = 0;
			  /*^clear */
		   /*clear *//*_.BUCKETLONG_PUT__V39*/ meltfptr[38] =
			    0;
			  /*^clear */
		   /*clear *//*_.SETQ___V40*/ meltfptr[39] = 0;
			}
			;
		      }
		    else
		      {		/*^cond.else */

	/*_.IF___V38*/ meltfptr[37] = NULL;;
		      }
		    ;
		    /*^compute */
		    /*_.LET___V36*/ meltfptr[35] =
		      /*_.IF___V38*/ meltfptr[37];;

		    MELT_LOCATION ("warmelt-modes.melt:4327:/ clear");
		 /*clear *//*_.ALARMH_PERIOD__V37*/ meltfptr[36] = 0;
		    /*^clear */
		 /*clear *//*_#NEWPERIOD__L12*/ meltfnum[11] = 0;
		    /*^clear */
		 /*clear *//*_#I__L13*/ meltfnum[12] = 0;
		    /*^clear */
		 /*clear *//*_.IF___V38*/ meltfptr[37] = 0;
		    /*_.IF___V35*/ meltfptr[34] =
		      /*_.LET___V36*/ meltfptr[35];;
		    /*epilog */

		    MELT_LOCATION ("warmelt-modes.melt:4326:/ clear");
		 /*clear *//*_.LET___V36*/ meltfptr[35] = 0;
		  }
		  ;
		}
	      else
		{		/*^cond.else */

      /*_.IF___V35*/ meltfptr[34] = NULL;;
		}
	      ;
	      /*^compute */
	      /*_.LET___V33*/ meltfptr[32] = /*_.IF___V35*/ meltfptr[34];;

	      MELT_LOCATION ("warmelt-modes.melt:4324:/ clear");
	       /*clear *//*_.RES__V34*/ meltfptr[33] = 0;
	      /*^clear */
	       /*clear *//*_.IF___V35*/ meltfptr[34] = 0;
	      /*_.IF___V32*/ meltfptr[31] = /*_.LET___V33*/ meltfptr[32];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-modes.melt:4323:/ clear");
	       /*clear *//*_.LET___V33*/ meltfptr[32] = 0;
	    }
	    ;
	  }
	else
	  {			/*^cond.else */

    /*_.IF___V32*/ meltfptr[31] = NULL;;
	  }
	;
	/*^compute */
	/*_.LET___V30*/ meltfptr[29] = /*_.IF___V32*/ meltfptr[31];;

	MELT_LOCATION ("warmelt-modes.melt:4321:/ clear");
	     /*clear *//*_.CLOS__V31*/ meltfptr[30] = 0;
	/*^clear */
	     /*clear *//*_#IS_CLOSURE__L11*/ meltfnum[6] = 0;
	/*^clear */
	     /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
	MELT_LOCATION ("warmelt-modes.melt:4333:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*^cond */
	/*cond */ if (
		       /*ifisa */
		       melt_is_instance_of ((melt_ptr_t)
					    ( /*_.GOTALARM_CONT__V3*/
					     meltfptr[2]),
					    (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
	  )			/*then */
	  {
	    /*^cond.then */
	    /*^getslot */
	    {
	      melt_ptr_t slot = NULL, obj = NULL;
	      obj =
		(melt_ptr_t) ( /*_.GOTALARM_CONT__V3*/ meltfptr[2]) /*=obj*/ ;
	      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V41*/ meltfptr[38] = slot;
	    };
	    ;
	  }
	else
	  {			/*^cond.else */

    /*_.REFERENCED_VALUE__V41*/ meltfptr[38] = NULL;;
	  }
	;
	MELT_LOCATION ("warmelt-modes.melt:4333:/ cond");
	/*cond */ if ( /*_.REFERENCED_VALUE__V41*/ meltfptr[38])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      MELT_LOCATION ("warmelt-modes.melt:4334:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^cond */
	      /*cond */ if (
			     /*ifisa */
			     melt_is_instance_of ((melt_ptr_t)
						  ( /*_.GOTALARM_CONT__V3*/
						   meltfptr[2]),
						  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
		)		/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    /*^putslot */
		    /*putslot */
		    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				    melt_magic_discr ((melt_ptr_t)
						      ( /*_.GOTALARM_CONT__V3*/ meltfptr[2])) == MELTOBMAG_OBJECT);
		    melt_putfield_object (( /*_.GOTALARM_CONT__V3*/
					   meltfptr[2]), (0),
					  (( /*nil */ NULL)),
					  "REFERENCED_VALUE");
		    ;
		    /*^touch */
		    meltgc_touch ( /*_.GOTALARM_CONT__V3*/ meltfptr[2]);
		    ;
		    /*^touchobj */

		    melt_dbgtrace_written_object ( /*_.GOTALARM_CONT__V3*/
						  meltfptr[2], "put-fields");
		    ;
		    /*epilog */
		  }
		  ;
		}		/*noelse */
	      ;
	      MELT_LOCATION ("warmelt-modes.melt:4335:/ again");
	      /*again */
	      {
		goto labloop_HANDLELOOP_1;
	      }
	      ;
	      MELT_LOCATION ("warmelt-modes.melt:4333:/ quasiblock");


	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;

	MELT_LOCATION ("warmelt-modes.melt:4313:/ clear");
	     /*clear *//*_#NEXTIM__L6*/ meltfnum[2] = 0;
	/*^clear */
	     /*clear *//*_.NEXTALHD__V19*/ meltfptr[18] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
	/*^clear */
	     /*clear *//*_.IFELSE___V24*/ meltfptr[20] = 0;
	/*^clear */
	     /*clear *//*_#I__L9*/ meltfnum[7] = 0;
	/*^clear */
	     /*clear *//*_.IFELSE___V25*/ meltfptr[21] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V26*/ meltfptr[25] = 0;
	/*^clear */
	     /*clear *//*_.BUCKETLONG_REMOVE__V28*/ meltfptr[26] = 0;
	/*^clear */
	     /*clear *//*_.SETQ___V29*/ meltfptr[28] = 0;
	/*^clear */
	     /*clear *//*_.LET___V30*/ meltfptr[29] = 0;
	/*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V41*/ meltfptr[38] = 0;
	MELT_LOCATION ("warmelt-modes.melt:4306:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*epilog */

	/*^clear */
	     /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
	/*^clear */
	     /*clear *//*_.LAMBDA___V16*/ meltfptr[14] = 0;
	/*^clear */
	     /*clear *//*_#BUCKETLONG_COUNT__L5*/ meltfnum[1] = 0;
	/*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
      }
      ;
      ;
      goto labloop_HANDLELOOP_1;
    labexit_HANDLELOOP_1:;	/*^loopepilog */
      /*loopepilog */
      /*_.FOREVER___V12*/ meltfptr[6] = /*_.HANDLELOOP__V13*/ meltfptr[10];;
    }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4337:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!ALARM_BUCKET_REFERENCE */
					  meltfrout->tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*!ALARM_BUCKET_REFERENCE */
					      meltfrout->tabval[0]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*!ALARM_BUCKET_REFERENCE */ meltfrout->
				  tabval[0])), (0),
				( /*_.ALARMBUCK__V2*/ meltfptr[1]),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch (( /*!ALARM_BUCKET_REFERENCE */ meltfrout->tabval[0]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*!ALARM_BUCKET_REFERENCE */
					 meltfrout->tabval[0]), "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4338:/ quasiblock");


 /*_#NOWTIM__L15*/ meltfnum[13] =
      melt_relative_time_millisec ();;
    /*^compute */
 /*_#NEXTALARM__L16*/ meltfnum[11] =
      melt_longsbucket_nth_key ((melt_ptr_t) /*_.ALARMBUCK__V2*/ meltfptr[1],
				(int) 0);;
    MELT_LOCATION ("warmelt-modes.melt:4341:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!INITIAL_SYSTEM_DATA */
					  meltfrout->tabval[3])),
					(melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[6])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @SYSDATA_ALARM_HOOK",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*!INITIAL_SYSTEM_DATA */
					      meltfrout->tabval[3]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				  tabval[3])), (37),
				(( /*!HANDLE_ALARM_SIGNAL */ meltfrout->
				  tabval[8])), "SYSDATA_ALARM_HOOK");
	  ;
	  /*^touch */
	  meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[3]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */
					 meltfrout->tabval[3]), "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4343:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L17*/ meltfnum[12] =
      (( /*_#NEXTALARM__L16*/ meltfnum[11]) >
       ( /*_#NOWTIM__L15*/ meltfnum[13]));;
    MELT_LOCATION ("warmelt-modes.melt:4343:/ cond");
    /*cond */ if ( /*_#I__L17*/ meltfnum[12])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#I__L18*/ meltfnum[6] =
	    (( /*_#NEXTALARM__L16*/ meltfnum[11]) -
	     ( /*_#NOWTIM__L15*/ meltfnum[13]));;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:4345:/ locexp");
	    melt_set_real_timer_millisec ( /*_#I__L18*/ meltfnum[6]);
	  }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4343:/ clear");
	     /*clear *//*_#I__L18*/ meltfnum[6] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4346:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L19*/ meltfnum[2] =
	    (( /*_#NEXTALARM__L16*/ meltfnum[11]) > (0));;
	  MELT_LOCATION ("warmelt-modes.melt:4346:/ cond");
	  /*cond */ if ( /*_#I__L19*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:4348:/ locexp");
		  melt_set_real_timer_millisec (30);
		}
		;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-modes.melt:4346:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-modes.melt:4351:/ locexp");
		  melt_set_real_timer_millisec (0);
		}
		;
		MELT_LOCATION ("warmelt-modes.melt:4352:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[3])),
						    (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[6])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @SYSDATA_ALARM_HOOK",
				      melt_magic_discr ((melt_ptr_t)
							(( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[3]))) == MELTOBMAG_OBJECT);
		      melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */
					      meltfrout->tabval[3])), (37),
					    (( /*nil */ NULL)),
					    "SYSDATA_ALARM_HOOK");
		      ;
		      /*^touch */
		      meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				     tabval[3]));
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[3]), "put-fields");
		      ;
		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;
		MELT_LOCATION ("warmelt-modes.melt:4349:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4343:/ clear");
	     /*clear *//*_#I__L19*/ meltfnum[2] = 0;
	}
	;
      }
    ;

    MELT_LOCATION ("warmelt-modes.melt:4338:/ clear");
	   /*clear *//*_#NOWTIM__L15*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_#NEXTALARM__L16*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_#I__L17*/ meltfnum[12] = 0;

    MELT_LOCATION ("warmelt-modes.melt:4300:/ clear");
	   /*clear *//*_.ALARMBUCK__V2*/ meltfptr[1] = 0;
    /*^clear */
	   /*clear *//*_.GOTALARM_CONT__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#CURELTIM__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V12*/ meltfptr[6] = 0;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("HANDLE_ALARM_SIGNAL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_74_warmelt_modes_HANDLE_ALARM_SIGNAL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_74_warmelt_modes_HANDLE_ALARM_SIGNAL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_modes_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_75_warmelt_modes_LAMBDA___23___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_75_warmelt_modes_LAMBDA___23___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 1
    melt_ptr_t mcfr_varptr[1];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_75_warmelt_modes_LAMBDA___23__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_75_warmelt_modes_LAMBDA___23___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 1; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_75_warmelt_modes_LAMBDA___23__ nbval 1*/
  meltfram__.mcfr_nbvar = 1 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
  MELT_LOCATION ("warmelt-modes.melt:4311:/ block");
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*~GOTALARM_CONT */ meltfclos->
					  tabval[0])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*~GOTALARM_CONT */ meltfclos->
					      tabval[0]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*~GOTALARM_CONT */ meltfclos->tabval[0])),
				(0),
				(( /*!konst_0_TRUE */ meltfrout->tabval[0])),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch (( /*~GOTALARM_CONT */ meltfclos->tabval[0]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*~GOTALARM_CONT */ meltfclos->
					 tabval[0]), "put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_75_warmelt_modes_LAMBDA___23___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_75_warmelt_modes_LAMBDA___23__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_modes_REGISTER_ALARM_TIMER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_76_warmelt_modes_REGISTER_ALARM_TIMER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_76_warmelt_modes_REGISTER_ALARM_TIMER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 21
    melt_ptr_t mcfr_varptr[21];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_76_warmelt_modes_REGISTER_ALARM_TIMER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_76_warmelt_modes_REGISTER_ALARM_TIMER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 21; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_76_warmelt_modes_REGISTER_ALARM_TIMER nbval 21*/
  meltfram__.mcfr_nbvar = 21 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("REGISTER_ALARM_TIMER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4356:/ getarg");
 /*_.CLOS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#PERIODMS__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;

  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DATA__V3*/ meltfptr[2] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DATA__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4362:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4362:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4362:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4362;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "register_alarm_timer clos=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CLOS__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " periodms=";
	      /*^apply.arg */
	      argtab[6].meltbp_long = /*_#PERIODMS__L1*/ meltfnum[0];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4362:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4362:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4362:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4363:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_CLOSURE__L4*/ meltfnum[2] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CLOS__V2*/ meltfptr[1])) ==
       MELTOBMAG_CLOSURE);;
    MELT_LOCATION ("warmelt-modes.melt:4363:/ cond");
    /*cond */ if ( /*_#IS_CLOSURE__L4*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-modes.melt:4364:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#I__L5*/ meltfnum[1] =
	    (( /*_#PERIODMS__L1*/ meltfnum[0]) >= (50));;
	  MELT_LOCATION ("warmelt-modes.melt:4364:/ cond");
	  /*cond */ if ( /*_#I__L5*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-modes.melt:4365:/ quasiblock");


		/*_.RES__V11*/ meltfptr[10] = ( /*nil */ NULL);;
		/*citerblock BLOCK_SIGNALS */
		{
		  /* block_signals meltcit1__BLKSIGNAL start */
		  long meltcit1__BLKSIGNAL_lev = melt_blocklevel_signals;
		  melt_blocklevel_signals = meltcit1__BLKSIGNAL_lev + 1;



		  MELT_LOCATION ("warmelt-modes.melt:4369:/ quasiblock");


      /*_.PERIODBOX__V13*/ meltfptr[12] =
		    (meltgc_new_int
		     ((meltobject_ptr_t)
		      (( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[1])),
		      ( /*_#PERIODMS__L1*/ meltfnum[0])));;
		  MELT_LOCATION ("warmelt-modes.melt:4370:/ cond");
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!ALARM_BUCKET_REFERENCE */ meltfrout->tabval[3])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!ALARM_BUCKET_REFERENCE */
					 meltfrout->tabval[3])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	/*_.REFERENCED_VALUE__V14*/ meltfptr[13] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.REFERENCED_VALUE__V14*/ meltfptr[13] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4370:/ cond");
		  /*cond */ if ( /*_.REFERENCED_VALUE__V14*/ meltfptr[13])	/*then */
		    {
		      /*^cond.then */
		      /*_.OLDBUCK__V15*/ meltfptr[14] =
			/*_.REFERENCED_VALUE__V14*/ meltfptr[13];;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-modes.melt:4370:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {

	/*_.MAKE_BUCKETLONG__V16*/ meltfptr[15] =
			  (meltgc_new_longsbucket
			   ((meltobject_ptr_t)
			    ( /*!DISCR_BUCKET_LONGS */ meltfrout->tabval[2]),
			    (31)));;
			/*^compute */
			/*_.OLDBUCK__V15*/ meltfptr[14] =
			  /*_.MAKE_BUCKETLONG__V16*/ meltfptr[15];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4370:/ clear");
		  /*clear *//*_.MAKE_BUCKETLONG__V16*/ meltfptr[15] = 0;
		      }
		      ;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4371:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^quasiblock */


		  /*^rawallocobj */
		  /*rawallocobj */
		  {
		    melt_ptr_t newobj = 0;
		    melt_raw_object_create (newobj,
					    (melt_ptr_t) (( /*!CLASS_ALARM_HANDLER */ meltfrout->tabval[5])), (4), "CLASS_ALARM_HANDLER");
       /*_.INST__V18*/ meltfptr[17] =
		      newobj;
		  };
		  ;
		  /*^putslot */
		  /*putslot */
		  melt_assertmsg ("putslot checkobj @ALARMH_PERIOD",
				  melt_magic_discr ((melt_ptr_t)
						    ( /*_.INST__V18*/
						     meltfptr[17])) ==
				  MELTOBMAG_OBJECT);
		  melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (1),
					( /*_.PERIODBOX__V13*/ meltfptr[12]),
					"ALARMH_PERIOD");
		  ;
		  /*^putslot */
		  /*putslot */
		  melt_assertmsg ("putslot checkobj @ALARMH_CLOS",
				  melt_magic_discr ((melt_ptr_t)
						    ( /*_.INST__V18*/
						     meltfptr[17])) ==
				  MELTOBMAG_OBJECT);
		  melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (2),
					( /*_.CLOS__V2*/ meltfptr[1]),
					"ALARMH_CLOS");
		  ;
		  /*^putslot */
		  /*putslot */
		  melt_assertmsg ("putslot checkobj @ALARMH_DATA",
				  melt_magic_discr ((melt_ptr_t)
						    ( /*_.INST__V18*/
						     meltfptr[17])) ==
				  MELTOBMAG_OBJECT);
		  melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (3),
					( /*_.DATA__V3*/ meltfptr[2]),
					"ALARMH_DATA");
		  ;
		  /*^touchobj */

		  melt_dbgtrace_written_object ( /*_.INST__V18*/ meltfptr[17],
						"newly made instance");
		  ;
		  /*_.ALHD__V17*/ meltfptr[15] =
		    /*_.INST__V18*/ meltfptr[17];;
		  /*^compute */
      /*_#CURELTIM__L6*/ meltfnum[5] =
		    melt_relative_time_millisec ();;
		  /*^compute */
      /*_#NEXTIM__L7*/ meltfnum[6] =
		    (( /*_#CURELTIM__L6*/ meltfnum[5]) +
		     ( /*_#PERIODMS__L1*/ meltfnum[0]));;
		  /*^compute */
      /*_.NEWBUCK__V19*/ meltfptr[18] =
		    meltgc_longsbucket_put ((melt_ptr_t) /*_.OLDBUCK__V15*/
					    meltfptr[14],
					    ( /*_#NEXTIM__L7*/ meltfnum[6]),
					    (melt_ptr_t) /*_.ALHD__V17*/
					    meltfptr[15]);;
		  /*^compute */
      /*_#FIRSTKEY__L8*/ meltfnum[7] =
		    melt_longsbucket_nth_key ((melt_ptr_t) /*_.NEWBUCK__V19*/
					      meltfptr[18], (int) 0);;
		  MELT_LOCATION ("warmelt-modes.melt:4379:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if ( /*_#FIRSTKEY__L8*/ meltfnum[7])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#I__L10*/ meltfnum[9] =
			  (( /*_#FIRSTKEY__L8*/ meltfnum[7]) -
			   ( /*_#CURELTIM__L6*/ meltfnum[5]));;
			/*^compute */
			/*_#FIRSTDELAY__L9*/ meltfnum[8] =
			  /*_#I__L10*/ meltfnum[9];;
			/*epilog */

			MELT_LOCATION ("warmelt-modes.melt:4379:/ clear");
		  /*clear *//*_#I__L10*/ meltfnum[9] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_#FIRSTDELAY__L9*/ meltfnum[8] = 0;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4381:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!ALARM_BUCKET_REFERENCE */ meltfrout->tabval[3])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

			/*^putslot */
			/*putslot */
			melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
					melt_magic_discr ((melt_ptr_t)
							  (( /*!ALARM_BUCKET_REFERENCE */ meltfrout->tabval[3]))) == MELTOBMAG_OBJECT);
			melt_putfield_object ((( /*!ALARM_BUCKET_REFERENCE */
						meltfrout->tabval[3])), (0),
					      ( /*_.NEWBUCK__V19*/
					       meltfptr[18]),
					      "REFERENCED_VALUE");
			;
			/*^touch */
			meltgc_touch (( /*!ALARM_BUCKET_REFERENCE */
				       meltfrout->tabval[3]));
			;
			/*^touchobj */

			melt_dbgtrace_written_object (( /*!ALARM_BUCKET_REFERENCE */ meltfrout->tabval[3]), "put-fields");
			;
			/*epilog */
		      }
		      ;
		    }		/*noelse */
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4382:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[6])),
						      (melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[8])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

			/*^putslot */
			/*putslot */
			melt_assertmsg
			  ("putslot checkobj @SYSDATA_ALARM_HOOK",
			   melt_magic_discr ((melt_ptr_t)
					     (( /*!INITIAL_SYSTEM_DATA */
					       meltfrout->tabval[6]))) ==
			   MELTOBMAG_OBJECT);
			melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */
						meltfrout->tabval[6])), (37),
					      (( /*!HANDLE_ALARM_SIGNAL */
						meltfrout->tabval[7])),
					      "SYSDATA_ALARM_HOOK");
			;
			/*^touch */
			meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				       tabval[6]));
			;
			/*^touchobj */

			melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[6]), "put-fields");
			;
			/*epilog */
		      }
		      ;
		    }		/*noelse */
		  ;

		  {
		    MELT_LOCATION ("warmelt-modes.melt:4383:/ locexp");
		    melt_set_real_timer_millisec ( /*_#FIRSTDELAY__L9*/
						  meltfnum[8]);
		  }
		  ;
		  MELT_LOCATION ("warmelt-modes.melt:4384:/ compute");
		  /*_.RES__V11*/ meltfptr[10] =
		    /*_.SETQ___V20*/ meltfptr[19] =
		    /*_.ALHD__V17*/ meltfptr[15];;
		  /*_.LET___V12*/ meltfptr[11] =
		    /*_.SETQ___V20*/ meltfptr[19];;

		  MELT_LOCATION ("warmelt-modes.melt:4369:/ clear");
		/*clear *//*_.PERIODBOX__V13*/ meltfptr[12] = 0;
		  /*^clear */
		/*clear *//*_.REFERENCED_VALUE__V14*/ meltfptr[13] = 0;
		  /*^clear */
		/*clear *//*_.OLDBUCK__V15*/ meltfptr[14] = 0;
		  /*^clear */
		/*clear *//*_.ALHD__V17*/ meltfptr[15] = 0;
		  /*^clear */
		/*clear *//*_#CURELTIM__L6*/ meltfnum[5] = 0;
		  /*^clear */
		/*clear *//*_#NEXTIM__L7*/ meltfnum[6] = 0;
		  /*^clear */
		/*clear *//*_.NEWBUCK__V19*/ meltfptr[18] = 0;
		  /*^clear */
		/*clear *//*_#FIRSTKEY__L8*/ meltfnum[7] = 0;
		  /*^clear */
		/*clear *//*_#FIRSTDELAY__L9*/ meltfnum[8] = 0;
		  /*^clear */
		/*clear *//*_.SETQ___V20*/ meltfptr[19] = 0;
		  /* block_signals meltcit1__BLKSIGNAL end */
		  melt_blocklevel_signals = meltcit1__BLKSIGNAL_lev;
		  MELT_CHECK_SIGNAL ();


		  /*citerepilog */

		  MELT_LOCATION ("warmelt-modes.melt:4367:/ clear");
		/*clear *//*_.LET___V12*/ meltfptr[11] = 0;
		}		/*endciterblock BLOCK_SIGNALS */
		;
		MELT_LOCATION ("warmelt-modes.melt:4385:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V11*/ meltfptr[10];;

		{
		  MELT_LOCATION ("warmelt-modes.melt:4385:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.LET___V10*/ meltfptr[9] =
		  /*_.RETURN___V21*/ meltfptr[12];;

		MELT_LOCATION ("warmelt-modes.melt:4365:/ clear");
	       /*clear *//*_.RES__V11*/ meltfptr[10] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V21*/ meltfptr[12] = 0;
		/*_.IF___V9*/ meltfptr[5] = /*_.LET___V10*/ meltfptr[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-modes.melt:4364:/ clear");
	       /*clear *//*_.LET___V10*/ meltfptr[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V9*/ meltfptr[5] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V8*/ meltfptr[4] = /*_.IF___V9*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4363:/ clear");
	     /*clear *//*_#I__L5*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V8*/ meltfptr[4] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4356:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-modes.melt:4356:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#IS_CLOSURE__L4*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.IF___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("REGISTER_ALARM_TIMER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_76_warmelt_modes_REGISTER_ALARM_TIMER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_76_warmelt_modes_REGISTER_ALARM_TIMER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_modes_UNREGISTER_ALARM_TIMER (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_77_warmelt_modes_UNREGISTER_ALARM_TIMER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_77_warmelt_modes_UNREGISTER_ALARM_TIMER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 26
    melt_ptr_t mcfr_varptr[26];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_77_warmelt_modes_UNREGISTER_ALARM_TIMER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_77_warmelt_modes_UNREGISTER_ALARM_TIMER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 26; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_77_warmelt_modes_UNREGISTER_ALARM_TIMER nbval 26*/
  meltfram__.mcfr_nbvar = 26 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("UNREGISTER_ALARM_TIMER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-modes.melt:4389:/ getarg");
 /*_.TIM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-modes.melt:4391:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-modes.melt:4391:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-modes.melt:4391:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-modes.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4391;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "unregister_alarm_timer tim=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.TIM__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4391:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-modes.melt:4391:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-modes.melt:4391:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4392:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.TIM__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V7*/ meltfptr[3] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:4392:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  /*^quasiblock */


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:4392:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*^quasiblock */


	  /*_.PROGN___V9*/ meltfptr[8] = /*_.RETURN___V8*/ meltfptr[4];;
	  /*^compute */
	  /*_.IFELSE___V7*/ meltfptr[3] = /*_.PROGN___V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4392:/ clear");
	     /*clear *//*_.RETURN___V8*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-modes.melt:4393:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L3*/ meltfnum[1] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.TIM__V2*/ meltfptr[1]),
			    (melt_ptr_t) (( /*!CLASS_ALARM_HANDLER */
					   meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-modes.melt:4393:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V10*/ meltfptr[4] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-modes.melt:4393:/ cond.else");

	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-modes.melt:4394:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_A__L4*/ meltfnum[0] =
	      melt_is_instance_of ((melt_ptr_t) ( /*_.TIM__V2*/ meltfptr[1]),
				   (melt_ptr_t) (( /*!CLASS_ALARM_HANDLER */
						  meltfrout->tabval[1])));;
	    MELT_LOCATION ("warmelt-modes.melt:4394:/ cond");
	    /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:4394:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check tim"),
					("warmelt-modes.melt")
					? ("warmelt-modes.melt") : __FILE__,
					(4394) ? (4394) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V11*/ meltfptr[8] = /*_.IFELSE___V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4394:/ clear");
	       /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V11*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-modes.melt:4395:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-modes.melt:4395:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-modes.melt:4393:/ quasiblock");


	  /*_.PROGN___V14*/ meltfptr[13] = /*_.RETURN___V13*/ meltfptr[11];;
	  /*^compute */
	  /*_.IFELSE___V10*/ meltfptr[4] = /*_.PROGN___V14*/ meltfptr[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-modes.melt:4393:/ clear");
	     /*clear *//*_.IFCPP___V11*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V13*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V14*/ meltfptr[13] = 0;
	}
	;
      }
    ;
    /*citerblock BLOCK_SIGNALS */
    {
      /* block_signals meltcit1__BLKSIGNAL start */
      long meltcit1__BLKSIGNAL_lev = melt_blocklevel_signals;
      melt_blocklevel_signals = meltcit1__BLKSIGNAL_lev + 1;



      MELT_LOCATION ("warmelt-modes.melt:4398:/ quasiblock");


      /*^cond */
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!ALARM_BUCKET_REFERENCE */
					    meltfrout->tabval[2])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!ALARM_BUCKET_REFERENCE */ meltfrout->
			     tabval[2])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
    /*_.OLDBUCK__V15*/ meltfptr[8] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

   /*_.OLDBUCK__V15*/ meltfptr[8] = NULL;;
	}
      ;
      /*^compute */
  /*_#OLDBUCKLEN__L5*/ meltfnum[0] =
	melt_longsbucket_count ((melt_ptr_t) /*_.OLDBUCK__V15*/ meltfptr[8]);;
      /*^compute */
  /*_.NEWBUCK__V16*/ meltfptr[11] =
	(meltgc_new_longsbucket
	 ((meltobject_ptr_t) ( /*!DISCR_BUCKET_LONGS */ meltfrout->tabval[4]),
	  ( /*_#OLDBUCKLEN__L5*/ meltfnum[0])));;
      MELT_LOCATION ("warmelt-modes.melt:4402:/ checksignal");
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if ( /*_#OLDBUCKLEN__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-modes.melt:4402:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {

	    MELT_LOCATION ("warmelt-modes.melt:4404:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^cond */
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!ALARM_BUCKET_REFERENCE */ meltfrout->tabval[2])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^putslot */
		  /*putslot */
		  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
				  melt_magic_discr ((melt_ptr_t)
						    (( /*!ALARM_BUCKET_REFERENCE */ meltfrout->tabval[2]))) == MELTOBMAG_OBJECT);
		  melt_putfield_object ((( /*!ALARM_BUCKET_REFERENCE */
					  meltfrout->tabval[2])), (0),
					(( /*nil */ NULL)),
					"REFERENCED_VALUE");
		  ;
		  /*^touch */
		  meltgc_touch (( /*!ALARM_BUCKET_REFERENCE */ meltfrout->
				 tabval[2]));
		  ;
		  /*^touchobj */

		  melt_dbgtrace_written_object (( /*!ALARM_BUCKET_REFERENCE */
						 meltfrout->tabval[2]),
						"put-fields");
		  ;
		  /*epilog */
		}
		;
	      }			/*noelse */
	    ;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:4405:/ locexp");
	      melt_set_real_timer_millisec (0);
	    }
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:4406:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^cond */
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!INITIAL_SYSTEM_DATA */
						  meltfrout->tabval[5])),
						(melt_ptr_t) (( /*!CLASS_SYSTEM_DATA */ meltfrout->tabval[6])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

		  /*^putslot */
		  /*putslot */
		  melt_assertmsg ("putslot checkobj @SYSDATA_ALARM_HOOK",
				  melt_magic_discr ((melt_ptr_t)
						    (( /*!INITIAL_SYSTEM_DATA */ meltfrout->tabval[5]))) == MELTOBMAG_OBJECT);
		  melt_putfield_object ((( /*!INITIAL_SYSTEM_DATA */
					  meltfrout->tabval[5])), (37),
					(( /*nil */ NULL)),
					"SYSDATA_ALARM_HOOK");
		  ;
		  /*^touch */
		  meltgc_touch (( /*!INITIAL_SYSTEM_DATA */ meltfrout->
				 tabval[5]));
		  ;
		  /*^touchobj */

		  melt_dbgtrace_written_object (( /*!INITIAL_SYSTEM_DATA */
						 meltfrout->tabval[5]),
						"put-fields");
		  ;
		  /*epilog */
		}
		;
	      }			/*noelse */
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:4408:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	    {
	      MELT_LOCATION ("warmelt-modes.melt:4408:/ locexp");
	      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		melt_warn_for_no_expected_secondary_results ();
	      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	      ;
	    }
	    ;
	    /*^finalreturn */
	    ;
	    /*finalret */ goto labend_rout;
	    MELT_LOCATION ("warmelt-modes.melt:4402:/ quasiblock");


	    /*_.PROGN___V19*/ meltfptr[18] = /*_.RETURN___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFELSE___V17*/ meltfptr[13] = /*_.PROGN___V19*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-modes.melt:4402:/ clear");
	      /*clear *//*_.RETURN___V18*/ meltfptr[17] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V19*/ meltfptr[18] = 0;
	  }
	  ;
	}
      ;
      /*citerblock FOREACH_IN_BUCKETLONG */
      {
	/*foreach_in_bucketlong meltcit2__EACHBUCKLONG */ unsigned
	  meltcit2__EACHBUCKLONG_ix = 0, meltcit2__EACHBUCKLONG_cnt = 0;
  /*_#OLDKEY__L6*/ meltfnum[5] = 0L;
  /*_.OLDALA__V20*/ meltfptr[17] = NULL;
	for (meltcit2__EACHBUCKLONG_ix = 0;
	     /* retrieve in meltcit2__EACHBUCKLONG iteration the count, it might change! */
	     (meltcit2__EACHBUCKLONG_cnt =
	      melt_longsbucket_count ((melt_ptr_t) /*_.OLDBUCK__V15*/
				      meltfptr[8])) > 0
	     && meltcit2__EACHBUCKLONG_ix < meltcit2__EACHBUCKLONG_cnt;
	     meltcit2__EACHBUCKLONG_ix++)
	  {
     /*_#OLDKEY__L6*/ meltfnum[5] = 0L;
     /*_.OLDALA__V20*/ meltfptr[17] = NULL;
	    {
	      struct melt_bucketlongentry_st *meltcit2__EACHBUCKLONG_buent
		=
		((struct meltbucketlongs_st *) /*_.OLDBUCK__V15*/
		 meltfptr[8])->buckl_entab + meltcit2__EACHBUCKLONG_ix;
	      if (!meltcit2__EACHBUCKLONG_buent->ebl_va)
		continue;
       /*_#OLDKEY__L6*/ meltfnum[5] =
		meltcit2__EACHBUCKLONG_buent->ebl_at;
       /*_.OLDALA__V20*/ meltfptr[17] =
		meltcit2__EACHBUCKLONG_buent->ebl_va;
	      meltcit2__EACHBUCKLONG_buent = NULL;
	    }
	    /*foreach_in_bucketlong meltcit2__EACHBUCKLONG body: */




#if MELT_HAVE_DEBUG
	    MELT_LOCATION ("warmelt-modes.melt:4412:/ cppif.then");
	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#IS_A__L7*/ meltfnum[6] =
		melt_is_instance_of ((melt_ptr_t)
				     ( /*_.OLDALA__V20*/ meltfptr[17]),
				     (melt_ptr_t) (( /*!CLASS_ALARM_HANDLER */
						    meltfrout->tabval[1])));;
	      MELT_LOCATION ("warmelt-modes.melt:4412:/ cond");
	      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[6])	/*then */
		{
		  /*^cond.then */
		  /*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
		}
	      else
		{
		  MELT_LOCATION ("warmelt-modes.melt:4412:/ cond.else");

		  /*^block */
		  /*anyblock */
		  {




		    {
		      /*^locexp */
		      melt_assert_failed (("check oldala"),
					  ("warmelt-modes.melt")
					  ? ("warmelt-modes.melt") : __FILE__,
					  (4412) ? (4412) : __LINE__,
					  __FUNCTION__);
		      ;
		    }
		    ;
		 /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
		    /*epilog */
		  }
		  ;
		}
	      ;
	      /*^compute */
	      /*_.IFCPP___V21*/ meltfptr[18] =
		/*_.IFELSE___V22*/ meltfptr[21];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-modes.melt:4412:/ clear");
	       /*clear *//*_#IS_A__L7*/ meltfnum[6] = 0;
	      /*^clear */
	       /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	    }

#else /*MELT_HAVE_DEBUG */
	    /*^cppif.else */
	    /*_.IFCPP___V21*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	    ;
	    MELT_LOCATION ("warmelt-modes.melt:4413:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
   /*_#__L8*/ meltfnum[6] =
	      (( /*_.OLDALA__V20*/ meltfptr[17]) ==
	       ( /*_.TIM__V2*/ meltfptr[1]));;
	    MELT_LOCATION ("warmelt-modes.melt:4413:/ cond");
	    /*cond */ if ( /*_#__L8*/ meltfnum[6])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V23*/ meltfptr[21] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-modes.melt:4413:/ cond.else");

		/*^block */
		/*anyblock */
		{

     /*_.BUCKETLONG_PUT__V24*/ meltfptr[23] =
		    meltgc_longsbucket_put ((melt_ptr_t) /*_.NEWBUCK__V16*/
					    meltfptr[11],
					    ( /*_#OLDKEY__L6*/ meltfnum[5]),
					    (melt_ptr_t) /*_.OLDALA__V20*/
					    meltfptr[17]);;
		  MELT_LOCATION ("warmelt-modes.melt:4414:/ compute");
		  /*_.NEWBUCK__V16*/ meltfptr[11] =
		    /*_.SETQ___V25*/ meltfptr[24] =
		    /*_.BUCKETLONG_PUT__V24*/ meltfptr[23];;
		  MELT_LOCATION ("warmelt-modes.melt:4413:/ quasiblock");


		  /*_.PROGN___V26*/ meltfptr[25] =
		    /*_.SETQ___V25*/ meltfptr[24];;
		  /*^compute */
		  /*_.IFELSE___V23*/ meltfptr[21] =
		    /*_.PROGN___V26*/ meltfptr[25];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-modes.melt:4413:/ clear");
	       /*clear *//*_.BUCKETLONG_PUT__V24*/ meltfptr[23] = 0;
		  /*^clear */
	       /*clear *//*_.SETQ___V25*/ meltfptr[24] = 0;
		  /*^clear */
	       /*clear *//*_.PROGN___V26*/ meltfptr[25] = 0;
		}
		;
	      }
	    ;
	    /* ending foreach_in_bucketlong meltcit2__EACHBUCKLONG */
     /*_#OLDKEY__L6*/ meltfnum[5] = 0L;
     /*_.OLDALA__V20*/ meltfptr[17] = NULL;
	  }			/* end foreach_in_bucketlong meltcit2__EACHBUCKLONG_ix */


	/*citerepilog */

	MELT_LOCATION ("warmelt-modes.melt:4409:/ clear");
	     /*clear *//*_#OLDKEY__L6*/ meltfnum[5] = 0;
	/*^clear */
	     /*clear *//*_.OLDALA__V20*/ meltfptr[17] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V21*/ meltfptr[18] = 0;
	/*^clear */
	     /*clear *//*_#__L8*/ meltfnum[6] = 0;
	/*^clear */
	     /*clear *//*_.IFELSE___V23*/ meltfptr[21] = 0;
      }				/*endciterblock FOREACH_IN_BUCKETLONG */
      ;
      MELT_LOCATION ("warmelt-modes.melt:4416:/ checksignal");
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!ALARM_BUCKET_REFERENCE */
					    meltfrout->tabval[2])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

	    /*^putslot */
	    /*putslot */
	    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			    melt_magic_discr ((melt_ptr_t)
					      (( /*!ALARM_BUCKET_REFERENCE */
						meltfrout->tabval[2]))) ==
			    MELTOBMAG_OBJECT);
	    melt_putfield_object ((( /*!ALARM_BUCKET_REFERENCE */ meltfrout->
				    tabval[2])), (0),
				  ( /*_.NEWBUCK__V16*/ meltfptr[11]),
				  "REFERENCED_VALUE");
	    ;
	    /*^touch */
	    meltgc_touch (( /*!ALARM_BUCKET_REFERENCE */ meltfrout->
			   tabval[2]));
	    ;
	    /*^touchobj */

	    melt_dbgtrace_written_object (( /*!ALARM_BUCKET_REFERENCE */
					   meltfrout->tabval[2]),
					  "put-fields");
	    ;
	    /*epilog */
	  }
	  ;
	}			/*noelse */
      ;

      MELT_LOCATION ("warmelt-modes.melt:4398:/ clear");
	    /*clear *//*_.OLDBUCK__V15*/ meltfptr[8] = 0;
      /*^clear */
	    /*clear *//*_#OLDBUCKLEN__L5*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.NEWBUCK__V16*/ meltfptr[11] = 0;
      /*^clear */
	    /*clear *//*_.IFELSE___V17*/ meltfptr[13] = 0;
      /* block_signals meltcit1__BLKSIGNAL end */
      melt_blocklevel_signals = meltcit1__BLKSIGNAL_lev;
      MELT_CHECK_SIGNAL ();


      /*citerepilog */
    }				/*endciterblock BLOCK_SIGNALS */
    ;
    MELT_LOCATION ("warmelt-modes.melt:4389:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V10*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("UNREGISTER_ALARM_TIMER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_77_warmelt_modes_UNREGISTER_ALARM_TIMER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_77_warmelt_modes_UNREGISTER_ALARM_TIMER */



/**** end of warmelt-modes+02.c ****/
