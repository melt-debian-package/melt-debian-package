/** GENERATED MELT DESCRIPTOR FILE meltbuild-sources/warmelt-normal+meltdesc.c 
** NEVER EDIT OR MOVE THIS, IT IS GENERATED & PARSED! **/
/* These identifiers are generated in warmelt-outobj.melt 
 & handled in melt-runtime.c carefully. */

	     #ifdef __cplusplus
	     /* explicitly declare as extern "C" our dlsym-ed symbols */
	     extern "C" const char melt_versionmeltstr[]	    ;
	     extern "C" const char melt_genversionstr[]		    ;
	     extern "C" const char melt_modulename[]		    ;
	     extern "C" const char melt_modulerealpath[]	    ;
	     extern "C" const char melt_prepromd5meltrun[]	    ;
	     extern "C" const char melt_primaryhexmd5[]		    ;
	     extern "C" const char* const melt_secondaryhexmd5tab[] ;
	     extern "C" const int melt_lastsecfileindex		    ;
	     extern "C" const char melt_cumulated_hexmd5[]	    ;

	     extern "C" {
	     #endif /*__cplusplus */
	     
/* version of the GCC compiler & MELT runtime generating this */
const char melt_genversionstr[]="4.8.0 20121010 (experimental) [melt-branch revision 192289] MELT_0\
.9.7-rc4"

	     #ifdef __cplusplus
	     " (in C++)"
	     #else
	     " (in C)"
	     #endif
					;
	     
const char melt_versionmeltstr[]="0.9.7-rc4 [melt-branch_revision_192289]";

/* source name & real path of the module */
/*MELTMODULENAME meltbuild-sources/warmelt-normal */
const char melt_modulename[]="warmelt-normal";
const char melt_modulerealpath[]="/usr/local/libexec/gcc-melt/gcc/x86_64-unknown-linux-gnu/4.8.0/melt\
-modules/0.9.7-rc4/warmelt-normal";

/* hash of preprocessed melt-run.h generating this */
const char melt_prepromd5meltrun[]="f66a30d9bc2c10de00b1532482ad91ed";
/* hexmd5checksum of primary C file */
const char melt_primaryhexmd5[]="c02d5b836790a2c753518faeb8202bbe";

/* hexmd5checksum of secondary C files */
const char* const melt_secondaryhexmd5tab[]={
 /*nosecfile*/ (const char*)0,
 /*sechexmd5checksum meltbuild-sources/warmelt-normal+01.c #1 */ "9c9e460d8b2303e6b5bc47f67cd65cb4",
 /*sechexmd5checksum meltbuild-sources/warmelt-normal+02.c #2 */ "042dac75834f6ff03087a5ba5938a4d9",
 /*sechexmd5checksum meltbuild-sources/warmelt-normal+03.c #3 */ "4930b354ceac5ed741ad4df18c356c95",
 /*sechexmd5checksum meltbuild-sources/warmelt-normal+04.c #4 */ "c5651bcb1b059b8205767925668003b4",
 /*sechexmd5checksum meltbuild-sources/warmelt-normal+05.c #5 */ "e6c949326ebee2f9c91beb8d3cad7b8f",
 /*nosecfile*/ (const char*)0,
 (const char*)0 };

/* last index of secondary files */
const int melt_lastsecfileindex=5;

/* cumulated checksum of primary & secondary files */
const char melt_cumulated_hexmd5[]="08381f22dcc1dc28539e6db3cd1d8c39" ;

/* include the timestamp file */
#define meltmod_warmelt_normal_mds__08381f22dcc1dc28539e6db3cd1d8c39 1
#include "warmelt-normal+melttime.h"
	 

		 #ifdef __cplusplus
		 }	  /* end extern C descriptor */
		 #endif /*__cplusplus */
		 
/* end of melt descriptor file */
