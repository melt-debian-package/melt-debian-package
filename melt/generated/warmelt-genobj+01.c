/* GCC MELT GENERATED FILE warmelt-genobj+01.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #1 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f1[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-genobj+01.c declarations ****/


/***************************************************
***
    Copyright 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_genobj_MAKE_OBJLOCATEDEXP (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_genobj_MAKE_OBJCOMPUTE (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_genobj_MAKE_OBJEXPANDPUREVAL (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_genobj_COMPILOBJ_CATCHALL_NREP (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_genobj_GECTYP_OBJNIL (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_genobj_VARIADIC_IDSTR (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_genobj_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_genobj_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_genobj_APPEND_COMMENT (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_genobj_APPEND_COMMENTCONST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_genobj_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_genobj_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_genobj_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_genobj_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_genobj_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_genobj_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_genobj_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_genobj_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_genobj_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_genobj_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_genobj_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_genobj_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_genobj_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_genobj_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_genobj_DISPOSE_OBJLOC (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_genobj_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_genobj_COMPILOBJ_NREP_LOCSYMOCC (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_genobj_COMPILOBJ_NREP_CLOSEDOCC (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_genobj_COMPILOBJ_NREP_CONSTOCC (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_genobj_COMPILOBJ_NREP_IMPORTEDVAL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_genobj_COMPILOBJ_NREP_LITERALVALUE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_genobj_COMPILOBJ_NREP_DEFINEDCONSTANT (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_genobj_COMPILOBJ_NREP_QUASICONSTANT (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_genobj_COMPILOBJ_NREP_QUASICONST_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_genobj_COMPILOBJ_NREP_FOREVER (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_genobj_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_genobj_COMPILOBJ_NREP_EXIT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_genobj_COMPILOBJ_NREP_AGAIN (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_genobj_COMPILOBJ_DISCRANY (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_genobj_COMPILOBJ_NREP_LET (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_genobj_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_genobj_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_genobj_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_genobj_FAIL_COMPILETRECFILL (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_genobj_COMPILETREC_LAMBDA (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_genobj_COMPILETREC_TUPLE (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_genobj_COMPILETREC_PAIR (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_genobj_COMPILETREC_LIST (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_genobj_COMPILETREC_INSTANCE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_genobj_COMPILOBJ_NREP_LETREC (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_genobj_COMPILOBJ_NREP_CITERATION (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_genobj_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_genobj_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_genobj_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_genobj_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_genobj_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_genobj_COMPILOBJ_NREP_SETQ (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_genobj_COMPILOBJ_NREP_PROGN (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_genobj_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_genobj_COMPILOBJ_NREP_MULTACC (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_genobj_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_genobj_COMPILOBJ_NREP_FIELDACC (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_genobj_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_genobj_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_genobj_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_genobj_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_genobj_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_genobj_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_genobj_LAMBDA___35__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_genobj_LAMBDA___36__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_genobj_LAMBDA___37__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_genobj_LAMBDA___38__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_genobj_LAMBDA___39__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_genobj_LAMBDA___40__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_genobj_LAMBDA___41__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_genobj_PUTOBJDEST_STRING (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_genobj_PUTOBJDEST_NULL (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_genobj_LAMBDA___42__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_genobj_LAMBDA___43__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_genobj_LAMBDA___44__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_genobj_LAMBDA___45__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_genobj_LAMBDA___46__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_genobj_LAMBDA___47__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_genobj_LAMBDA___48__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_148_warmelt_genobj_LAMBDA___49__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_149_warmelt_genobj_LAMBDA___50__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_153_warmelt_genobj_LAMBDA___51__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_155_warmelt_genobj_LAMBDA___52__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_157_warmelt_genobj_COMPILOBJ_QUASIDATA_PARENT_MODULE_ENVIRONMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_158_warmelt_genobj_COMPILOBJ_NREP_STORE_PREDEFINED (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_159_warmelt_genobj_COMPILOBJ_NREP_UPDATE_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_160_warmelt_genobj_LAMBDA___53__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_161_warmelt_genobj_COMPILOBJ_NREP_CHECK_RUNNING_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_162_warmelt_genobj_LAMBDA___54__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_163_warmelt_genobj_COMPILTST_ANYTESTER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_164_warmelt_genobj_COMPILOBJ_NREP_MATCH (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_165_warmelt_genobj_LAMBDA___55__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_166_warmelt_genobj_COMPILOBJ_NREP_ALTMATCH (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_167_warmelt_genobj_LAMBDA___56__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_168_warmelt_genobj_COMPILOBJ_NREP_MATCHLABEL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_169_warmelt_genobj_COMPILOBJ_NREP_MATCHFLAG (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_170_warmelt_genobj_COMPILOBJ_NREP_MATCHDATAINIT (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_171_warmelt_genobj_COMPILOBJ_NREP_MATCHEDATA (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_172_warmelt_genobj_COMPILOBJ_NREP_MATCHJUMP (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_173_warmelt_genobj_NORMTESTER_LABELINSTR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_174_warmelt_genobj_NORMTESTER_GOTOINSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_175_warmelt_genobj_ENDMATCH_GOTOINSTR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_176_warmelt_genobj_TESTMATCH_GOTOINSTR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_177_warmelt_genobj_NORMTESTER_FREE_OBJLOC_LIST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_178_warmelt_genobj_LAMBDA___57__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_179_warmelt_genobj_COMPILTST_NORMTESTER_ANY (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_180_warmelt_genobj_COMPILTST_NORMTESTER_MATCHER (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_181_warmelt_genobj_COMPILTST_NORMTESTER_INSTANCE (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_182_warmelt_genobj_COMPILTST_NORMTESTER_TUPLE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_183_warmelt_genobj_COMPILTST_NORMTESTER_SAME (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_184_warmelt_genobj_COMPILTST_NORMTESTER_SUCCESS (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_185_warmelt_genobj_COMPILTST_NORMTESTER_ORCLEAR (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_186_warmelt_genobj_COMPILTST_NORMTESTER_ORTRANSMIT (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_187_warmelt_genobj_COMPILTST_NORMTESTER_DISJUNCTION
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_188_warmelt_genobj_COMPILMATCHER_CMATCHER (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_189_warmelt_genobj_LAMBDA___58__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_190_warmelt_genobj_LAMBDA___59__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_191_warmelt_genobj_LAMBDA___60__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_192_warmelt_genobj_LAMBDA___61__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_193_warmelt_genobj_COMPILMATCHER_FUNMATCHER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_194_warmelt_genobj_LAMBDA___62__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_195_warmelt_genobj_LAMBDA___63__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_genobj__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_genobj__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_genobj__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_genobj__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_0 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_1 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_2 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_3 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_4 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_5 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_6 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_7 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_8 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_9 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_10 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_11 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_12 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_13 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_14 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_15 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_16 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_17 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_18 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_19 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_20 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_21 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_22 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_23 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_24 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_25 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_26 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_27 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_28 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_29 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_30 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_31 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_32 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_33 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_34 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_35 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_36 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_37 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_38 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_39 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_40 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_41 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_42 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_43 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_44 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_45 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__forward_or_mark_module_start_frame (struct
							     melt_callframe_st
							     *fp,
							     int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_genobj__forward_or_mark_module_start_frame



/**** warmelt-genobj+01.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_CATCHALL_OBJCODE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:889:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:890:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:890:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:890:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 890;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest_catchall_objcode recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " desto=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:890:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:890:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:890:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:891:/ quasiblock");


 /*_.DISCR__V9*/ meltfptr[5] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-genobj.melt:892:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DISCR__V9*/ meltfptr[5]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.DISCRNAME__V10*/ meltfptr[9] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:894:/ locexp");
      error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter, ("{Internal Error} PUT_OBJDEST sent to unexpected C-generated object\
 of class"), melt_string_str
	     ((melt_ptr_t) ( /*_.DISCRNAME__V10*/ meltfptr[9])));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:895:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:895:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@@put_objdest should be implemented in nrep-s subclasses"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (895) ? (895) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[10] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:895:/ clear");
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V8*/ meltfptr[4] = /*_.IFCPP___V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-genobj.melt:891:/ clear");
	   /*clear *//*_.DISCR__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.DISCRNAME__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:889:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:889:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_CATCHALL_OBJCODE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_CATCHALL_ANYDISCR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:900:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:901:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:901:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:901:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 901;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest_catchall_anydiscr recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " desto=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:901:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:901:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:901:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:902:/ locexp");
      melt_puts (stderr,
		 ("* putobjdest unimplemented receiver discriminant "));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:903:/ quasiblock");


 /*_.DISCR__V9*/ meltfptr[5] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-genobj.melt:904:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DISCR__V9*/ meltfptr[5]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.DISCRNAME__V10*/ meltfptr[9] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:906:/ locexp");
      error ("MELT ERROR MSG [#%ld]::: %s - %s", melt_dbgcounter,
	     ("{Internal Error} PUT_OBJDEST sent to unexpected value of "),
	     melt_string_str ((melt_ptr_t)
			      ( /*_.DISCRNAME__V10*/ meltfptr[9])));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:907:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:907:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@@ unexpected catchall putobjdest anydiscr"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (907) ? (907) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[10] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:907:/ clear");
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V8*/ meltfptr[4] = /*_.IFCPP___V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-genobj.melt:903:/ clear");
	   /*clear *//*_.DISCR__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.DISCRNAME__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:900:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:900:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_CATCHALL_ANYDISCR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GETCTYPE_OBJVALUE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:911:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:912:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJVALUE */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:912:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:912:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv objvalue"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (912) ? (912) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:912:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:913:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBV_TYPE");
  /*_.OBV_TYPE__V6*/ meltfptr[4] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:911:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OBV_TYPE__V6*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:911:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.OBV_TYPE__V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GETCTYPE_OBJVALUE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_genobj_GECTYP_OBJNIL (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_8_warmelt_genobj_GECTYP_OBJNIL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_8_warmelt_genobj_GECTYP_OBJNIL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_8_warmelt_genobj_GECTYP_OBJNIL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_8_warmelt_genobj_GECTYP_OBJNIL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_8_warmelt_genobj_GECTYP_OBJNIL nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GECTYP_OBJNIL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:921:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!CTYPE_VALUE */ meltfrout->tabval[0]);;

    {
      MELT_LOCATION ("warmelt-genobj.melt:921:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GECTYP_OBJNIL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_8_warmelt_genobj_GECTYP_OBJNIL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_8_warmelt_genobj_GECTYP_OBJNIL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GECTYP_OBJINITSTRING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:927:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:928:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      ( /*!CTYPE_VALUE */ meltfrout->tabval[0]);;

    {
      MELT_LOCATION ("warmelt-genobj.melt:928:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-genobj.melt:927:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RETURN___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:927:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.RETURN___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GECTYP_OBJINITSTRING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un *
					      meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un *
					      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DBGOUT_ROUTINEOBJ", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:933:/ getarg");
 /*_.SELF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DBGI__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DBGI__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:934:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L2*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-genobj.melt:934:/ cond");
    /*cond */ if ( /*_#I__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:935:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V5*/ meltfptr[4] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_NAMEDOBJECT_METHOD */ meltfrout->
			    tabval[0])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V4*/ meltfptr[3] =
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V5*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:934:/ clear");
	     /*clear *//*_.DBGOUT_NAMEDOBJECT_METHOD__V5*/ meltfptr[4] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:936:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[2];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DBGI__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 30;
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V6*/ meltfptr[4] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!DBGOUT_NAMEDOBJECT_METHOD */ meltfrout->
			    tabval[0])),
			  (melt_ptr_t) ( /*_.SELF__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V4*/ meltfptr[3] =
	    /*_.DBGOUT_NAMEDOBJECT_METHOD__V6*/ meltfptr[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:934:/ clear");
	     /*clear *//*_.DBGOUT_NAMEDOBJECT_METHOD__V6*/ meltfptr[4] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:933:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:933:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#I__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DBGOUT_ROUTINEOBJ", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 20
    melt_ptr_t mcfr_varptr[20];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 20; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR nbval 20*/
  meltfram__.mcfr_nbvar = 20 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_OBJDESTINSTR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:941:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:942:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJDESTINSTR */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:942:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:942:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("putobjdest_objdestinstr check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (942) ? (942) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:942:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:943:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:943:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:943:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 943;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest_objdestinstr recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " desto=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:943:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:943:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:943:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:944:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBDI_DESTLIST");
  /*_.DESTL__V11*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:945:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L4*/ meltfnum[2] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.DESTL__V11*/ meltfptr[7])) ==
       MELTOBMAG_LIST);;
    /*^compute */
 /*_#NOT__L5*/ meltfnum[0] =
      (!( /*_#IS_LIST__L4*/ meltfnum[2]));;
    MELT_LOCATION ("warmelt-genobj.melt:945:/ cond");
    /*cond */ if ( /*_#NOT__L5*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_LIST__V12*/ meltfptr[11] =
	    (meltgc_new_list
	     ((meltobject_ptr_t)
	      (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
	  MELT_LOCATION ("warmelt-genobj.melt:947:/ compute");
	  /*_.DESTL__V11*/ meltfptr[7] = /*_.SETQ___V13*/ meltfptr[12] =
	    /*_.MAKE_LIST__V12*/ meltfptr[11];;
	  MELT_LOCATION ("warmelt-genobj.melt:948:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.RECV__V2*/ meltfptr[1])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.RECV__V2*/ meltfptr[1]), (1),
				( /*_.DESTL__V11*/ meltfptr[7]),
				"OBDI_DESTLIST");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.RECV__V2*/ meltfptr[1]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.RECV__V2*/ meltfptr[1],
					"put-fields");
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:946:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:945:/ clear");
	     /*clear *//*_.MAKE_LIST__V12*/ meltfptr[11] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V13*/ meltfptr[12] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:949:/ quasiblock");


 /*_.LIST_FIRST__V15*/ meltfptr[12] =
      (melt_list_first ((melt_ptr_t) ( /*_.DESTL__V11*/ meltfptr[7])));;
    /*^compute */
 /*_.FIRSTD__V16*/ meltfptr[15] =
      (melt_pair_head ((melt_ptr_t) ( /*_.LIST_FIRST__V15*/ meltfptr[12])));;
    MELT_LOCATION ("warmelt-genobj.melt:950:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L6*/ meltfnum[5] =
      (( /*_.FIRSTD__V16*/ meltfptr[15]) == ( /*_.DESTO__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-genobj.melt:950:/ cond");
    /*cond */ if ( /*_#__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:951:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.RECV__V2*/ meltfptr[1];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:951:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IFELSE___V17*/ meltfptr[16] = /*_.RETURN___V18*/ meltfptr[17];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:950:/ clear");
	     /*clear *//*_.RETURN___V18*/ meltfptr[17] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:953:/ locexp");
	    meltgc_append_list ((melt_ptr_t) ( /*_.DESTL__V11*/ meltfptr[7]),
				(melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:954:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.RECV__V2*/ meltfptr[1];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:954:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-genobj.melt:952:/ quasiblock");


	  /*_.PROGN___V20*/ meltfptr[19] = /*_.RETURN___V19*/ meltfptr[17];;
	  /*^compute */
	  /*_.IFELSE___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[19];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:950:/ clear");
	     /*clear *//*_.RETURN___V19*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V20*/ meltfptr[19] = 0;
	}
	;
      }
    ;
    /*_.LET___V14*/ meltfptr[11] = /*_.IFELSE___V17*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-genobj.melt:949:/ clear");
	   /*clear *//*_.LIST_FIRST__V15*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.FIRSTD__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    /*_.LET___V10*/ meltfptr[6] = /*_.LET___V14*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-genobj.melt:944:/ clear");
	   /*clear *//*_.DESTL__V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L4*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L5*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:941:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V10*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:941:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_OBJDESTINSTR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 5
    melt_ptr_t mcfr_varptr[5];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 5; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN nbval 5*/
  meltfram__.mcfr_nbvar = 5 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_OBJFINALRETURN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:977:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:978:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJFINALRETURN */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:978:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:978:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (978) ? (978) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:978:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:977:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RECV__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:977:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_OBJFINALRETURN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_genobj_VARIADIC_IDSTR (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_13_warmelt_genobj_VARIADIC_IDSTR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_13_warmelt_genobj_VARIADIC_IDSTR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_13_warmelt_genobj_VARIADIC_IDSTR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_13_warmelt_genobj_VARIADIC_IDSTR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_13_warmelt_genobj_VARIADIC_IDSTR nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("VARIADIC_IDSTR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:985:/ getarg");
 /*_.VARIADSYM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[0].meltbp_cstring;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:986:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^block */
    /*anyblock */
    {

      /*^objgoto */
      /*objgoto */ goto mtch1_0;
      ;

    /*objlabel */ mtch1_0:;
      MELT_LOCATION ("warmelt-genobj.melt:987:/ objlabel");
      ;
      /*^cond */
      /*cond */ if (
		     /*ASNULL_mtch1__1 ? */ ( /*_.VARIADSYM__V2*/ meltfptr[1] == NULL))	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      ;
	    }
	    ;
	    /*^objgoto */
	    /*objgoto */ goto mtch1_1;
	    ;
	  }
	  ;
	}
      else
	{			/*^cond.else */

	  /*^block */
	  /*anyblock */
	  {

	    MELT_LOCATION ("warmelt-genobj.melt:989:/ objgoto");
	    /*objgoto */ goto mtch1_2;
	    ;
	  }
	  ;
	}
      ;

    /*objlabel */ mtch1_1:;
      MELT_LOCATION ("warmelt-genobj.melt:987:/ objlabel");
      ;
      /*^quasiblock */


      MELT_LOCATION ("warmelt-genobj.melt:988:/ checksignal");
      MELT_CHECK_SIGNAL ();
      ;
      /*^quasiblock */


      /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

      {
	MELT_LOCATION ("warmelt-genobj.melt:988:/ locexp");
	/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	  melt_warn_for_no_expected_secondary_results ();
	/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	;
      }
      ;
      /*^finalreturn */
      ;
      /*finalret */ goto labend_rout;
      /*_.MATCHRES___V3*/ meltfptr[2] = /*_.RETURN___V4*/ meltfptr[3];;

      MELT_LOCATION ("warmelt-genobj.melt:987:/ clear");
	    /*clear *//*_.RETURN___V4*/ meltfptr[3] = 0;
      /*^objgoto */
      /*objgoto */ goto mtch1__end /*endmatch */ ;
      ;

    /*objlabel */ mtch1_2:;
      MELT_LOCATION ("warmelt-genobj.melt:989:/ objlabel");
      ;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V5*/ meltfptr[3] = 0;
      /*^clear */
	    /*clear *//*_.CSYM_URANK__V6*/ meltfptr[5] = 0;
      /*^cond */
      /*cond */ if (
		     /*normtesterinst */
	(melt_is_instance_of ((melt_ptr_t) ( /*_.VARIADSYM__V2*/ meltfptr[1]),
			      (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */ meltfrout->tabval[1])))))	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

	    /*^getslot */
	    {
	      melt_ptr_t slot = NULL, obj = NULL;
	      obj = (melt_ptr_t) ( /*_.VARIADSYM__V2*/ meltfptr[1]) /*=obj*/ ;
	      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.NAMED_NAME__V5*/ meltfptr[3] = slot;
	    };
	    ;
	    /*^getslot */
	    {
	      melt_ptr_t slot = NULL, obj = NULL;
	      obj = (melt_ptr_t) ( /*_.VARIADSYM__V2*/ meltfptr[1]) /*=obj*/ ;
	      melt_object_get_field (slot, obj, 3, "CSYM_URANK");
     /*_.CSYM_URANK__V6*/ meltfptr[5] = slot;
	    };
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:991:/ objgoto");
	    /*objgoto */ goto mtch1_3;
	    ;
	  }
	  ;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:989:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {

	    MELT_LOCATION ("warmelt-genobj.melt:1002:/ objgoto");
	    /*objgoto */ goto mtch1_5;
	    ;
	  }
	  ;
	}
      ;

    /*objlabel */ mtch1_3:;
      MELT_LOCATION ("warmelt-genobj.melt:991:/ objlabel");
      ;
      /*^clear */
	    /*clear *//*_#ICT__L1*/ meltfnum[0] = 0;
      /*^cond */
      /*cond */ if (
					 /* integerbox_of IBOXOF_mtch1__1 ? *//*_.CSYM_URANK__V6*/
		     meltfptr[5]
		     && melt_magic_discr ((melt_ptr_t) /*_.CSYM_URANK__V6*/ meltfptr[5]) == MELTOBMAG_INT)	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

	    /*^clear */
	      /*clear *//*_#ICT__L1*/ meltfnum[0] = 0;

	    {
	      /*^locexp */
	      /* integerbox_of IBOXOF_mtch1__1 ! *//*_#ICT__L1*/ meltfnum[0] =
		((struct meltint_st *) /*_.CSYM_URANK__V6*/ meltfptr[5])->
		val;;
	    }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:989:/ objgoto");
	    /*objgoto */ goto mtch1_4;
	    ;
	  }
	  ;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:991:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {

	    MELT_LOCATION ("warmelt-genobj.melt:1002:/ objgoto");
	    /*objgoto */ goto mtch1_5;
	    ;
	  }
	  ;
	}
      ;

    /*objlabel */ mtch1_4:;
      MELT_LOCATION ("warmelt-genobj.melt:989:/ objlabel");
      ;
      /*^quasiblock */


      /*_.NVARNAM__V7*/ meltfptr[5] = /*_.NAMED_NAME__V5*/ meltfptr[3];;
      /*^compute */
      /*_#NVARURANK__L2*/ meltfnum[1] = /*_#ICT__L1*/ meltfnum[0];;

#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-genobj.melt:992:/ cppif.then");
      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
    /*_#IS_STRING__L3*/ meltfnum[2] =
	  (melt_magic_discr ((melt_ptr_t) ( /*_.NVARNAM__V7*/ meltfptr[5])) ==
	   MELTOBMAG_STRING);;
	MELT_LOCATION ("warmelt-genobj.melt:992:/ cond");
	/*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[2])	/*then */
	  {
	    /*^cond.then */
	    /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-genobj.melt:992:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {




	      {
		/*^locexp */
		melt_assert_failed (("check nvarnam"),
				    ("warmelt-genobj.melt")
				    ? ("warmelt-genobj.melt") : __FILE__,
				    (992) ? (992) : __LINE__, __FUNCTION__);
		;
	      }
	      ;
		/*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	      /*epilog */
	    }
	    ;
	  }
	;
	/*^compute */
	/*_.IFCPP___V8*/ meltfptr[7] = /*_.IFELSE___V9*/ meltfptr[8];;
	/*epilog */

	MELT_LOCATION ("warmelt-genobj.melt:992:/ clear");
	      /*clear *//*_#IS_STRING__L3*/ meltfnum[2] = 0;
	/*^clear */
	      /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V8*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;
      MELT_LOCATION ("warmelt-genobj.melt:993:/ quasiblock");


  /*_.SBUF__V11*/ meltfptr[10] =
	(melt_ptr_t)
	meltgc_new_strbuf ((meltobject_ptr_t)
			   (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			   (const char *) 0);;

      {
	MELT_LOCATION ("warmelt-genobj.melt:995:/ locexp");
	/*add2sbuf_strconst */
	  meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V11*/ meltfptr[10]),
			     ("variad_"));
      }
      ;

      {
	MELT_LOCATION ("warmelt-genobj.melt:996:/ locexp");
	meltgc_add_strbuf_cident ((melt_ptr_t)
				  ( /*_.SBUF__V11*/ meltfptr[10]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.NVARNAM__V7*/
						    meltfptr[5])));
      }
      ;

      {
	MELT_LOCATION ("warmelt-genobj.melt:997:/ locexp");
	/*add2sbuf_strconst */
	  meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V11*/ meltfptr[10]),
			     ("_c"));
      }
      ;

      {
	MELT_LOCATION ("warmelt-genobj.melt:998:/ locexp");
	meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.SBUF__V11*/ meltfptr[10]),
			       ( /*_#NVARURANK__L2*/ meltfnum[1]));
      }
      ;

      {
	MELT_LOCATION ("warmelt-genobj.melt:999:/ locexp");
	/*add2sbuf_strconst */
	  meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V11*/ meltfptr[10]),
			     ( /*_?*/ meltfram__.loc_CSTRING__o0));
      }
      ;
  /*_.STRBUF2STRING__V12*/ meltfptr[11] =
	(meltgc_new_stringdup
	 ((meltobject_ptr_t)
	  (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[4])),
	  melt_strbuf_str ((melt_ptr_t) ( /*_.SBUF__V11*/ meltfptr[10]))));;
      /*^compute */
      /*_.LET___V10*/ meltfptr[8] = /*_.STRBUF2STRING__V12*/ meltfptr[11];;

      MELT_LOCATION ("warmelt-genobj.melt:993:/ clear");
	    /*clear *//*_.SBUF__V11*/ meltfptr[10] = 0;
      /*^clear */
	    /*clear *//*_.STRBUF2STRING__V12*/ meltfptr[11] = 0;
      MELT_LOCATION ("warmelt-genobj.melt:989:/ checksignal");
      MELT_CHECK_SIGNAL ();
      ;
      /*_.MATCHRES___V3*/ meltfptr[2] = /*_.LET___V10*/ meltfptr[8];;

      MELT_LOCATION ("warmelt-genobj.melt:989:/ clear");
	    /*clear *//*_.NVARNAM__V7*/ meltfptr[5] = 0;
      /*^clear */
	    /*clear *//*_#NVARURANK__L2*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V8*/ meltfptr[7] = 0;
      /*^clear */
	    /*clear *//*_.LET___V10*/ meltfptr[8] = 0;
      /*^objgoto */
      /*objgoto */ goto mtch1__end /*endmatch */ ;
      ;

    /*objlabel */ mtch1_5:;
      MELT_LOCATION ("warmelt-genobj.melt:1002:/ objlabel");
      ;
      /*^clear */
	    /*clear *//*_.NAMED_NAME__V13*/ meltfptr[10] = 0;
      /*^cond */
      /*cond */ if (
		     /*normtesterinst */
	(melt_is_instance_of ((melt_ptr_t) ( /*_.VARIADSYM__V2*/ meltfptr[1]),
			      (melt_ptr_t) (( /*!CLASS_SYMBOL */ meltfrout->tabval[5])))))	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

	    /*^getslot */
	    {
	      melt_ptr_t slot = NULL, obj = NULL;
	      obj = (melt_ptr_t) ( /*_.VARIADSYM__V2*/ meltfptr[1]) /*=obj*/ ;
	      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
     /*_.NAMED_NAME__V13*/ meltfptr[10] = slot;
	    };
	    ;
	    /*^objgoto */
	    /*objgoto */ goto mtch1_6;
	    ;
	  }
	  ;
	}
      else
	{			/*^cond.else */

	  /*^block */
	  /*anyblock */
	  {

	    MELT_LOCATION ("warmelt-genobj.melt:1012:/ objgoto");
	    /*objgoto */ goto mtch1_7;
	    ;
	  }
	  ;
	}
      ;

    /*objlabel */ mtch1_6:;
      MELT_LOCATION ("warmelt-genobj.melt:1002:/ objlabel");
      ;
      /*^quasiblock */


      /*_.NVARNAM__V14*/ meltfptr[11] = /*_.NAMED_NAME__V13*/ meltfptr[10];;

#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-genobj.melt:1004:/ cppif.then");
      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
    /*_#IS_STRING__L4*/ meltfnum[2] =
	  (melt_magic_discr ((melt_ptr_t) ( /*_.NVARNAM__V14*/ meltfptr[11]))
	   == MELTOBMAG_STRING);;
	MELT_LOCATION ("warmelt-genobj.melt:1004:/ cond");
	/*cond */ if ( /*_#IS_STRING__L4*/ meltfnum[2])	/*then */
	  {
	    /*^cond.then */
	    /*_.IFELSE___V16*/ meltfptr[7] = ( /*nil */ NULL);;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1004:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {




	      {
		/*^locexp */
		melt_assert_failed (("check nvarnam"),
				    ("warmelt-genobj.melt")
				    ? ("warmelt-genobj.melt") : __FILE__,
				    (1004) ? (1004) : __LINE__, __FUNCTION__);
		;
	      }
	      ;
		/*clear *//*_.IFELSE___V16*/ meltfptr[7] = 0;
	      /*epilog */
	    }
	    ;
	  }
	;
	/*^compute */
	/*_.IFCPP___V15*/ meltfptr[5] = /*_.IFELSE___V16*/ meltfptr[7];;
	/*epilog */

	MELT_LOCATION ("warmelt-genobj.melt:1004:/ clear");
	      /*clear *//*_#IS_STRING__L4*/ meltfnum[2] = 0;
	/*^clear */
	      /*clear *//*_.IFELSE___V16*/ meltfptr[7] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V15*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1005:/ quasiblock");


  /*_.SBUF__V18*/ meltfptr[7] =
	(melt_ptr_t)
	meltgc_new_strbuf ((meltobject_ptr_t)
			   (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			   (const char *) 0);;

      {
	MELT_LOCATION ("warmelt-genobj.melt:1007:/ locexp");
	/*add2sbuf_strconst */
	  meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V18*/ meltfptr[7]),
			     ("variad_"));
      }
      ;

      {
	MELT_LOCATION ("warmelt-genobj.melt:1008:/ locexp");
	meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.SBUF__V18*/ meltfptr[7]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.NVARNAM__V14*/
						    meltfptr[11])));
      }
      ;

      {
	MELT_LOCATION ("warmelt-genobj.melt:1009:/ locexp");
	/*add2sbuf_strconst */
	  meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V18*/ meltfptr[7]),
			     ( /*_?*/ meltfram__.loc_CSTRING__o0));
      }
      ;
  /*_.STRBUF2STRING__V19*/ meltfptr[18] =
	(meltgc_new_stringdup
	 ((meltobject_ptr_t)
	  (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[4])),
	  melt_strbuf_str ((melt_ptr_t) ( /*_.SBUF__V18*/ meltfptr[7]))));;
      /*^compute */
      /*_.LET___V17*/ meltfptr[8] = /*_.STRBUF2STRING__V19*/ meltfptr[18];;

      MELT_LOCATION ("warmelt-genobj.melt:1005:/ clear");
	    /*clear *//*_.SBUF__V18*/ meltfptr[7] = 0;
      /*^clear */
	    /*clear *//*_.STRBUF2STRING__V19*/ meltfptr[18] = 0;
      MELT_LOCATION ("warmelt-genobj.melt:1002:/ checksignal");
      MELT_CHECK_SIGNAL ();
      ;
      /*_.MATCHRES___V3*/ meltfptr[2] = /*_.LET___V17*/ meltfptr[8];;

      MELT_LOCATION ("warmelt-genobj.melt:1002:/ clear");
	    /*clear *//*_.NVARNAM__V14*/ meltfptr[11] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V15*/ meltfptr[5] = 0;
      /*^clear */
	    /*clear *//*_.LET___V17*/ meltfptr[8] = 0;
      /*^objgoto */
      /*objgoto */ goto mtch1__end /*endmatch */ ;
      ;

    /*objlabel */ mtch1_7:;
      MELT_LOCATION ("warmelt-genobj.melt:1012:/ objlabel");
      ;
      /*^quasiblock */



#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-genobj.melt:1013:/ cppif.then");
      /*^block */
      /*anyblock */
      {


	{
	  /*^locexp */
	  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	  melt_dbgcounter++;
#endif
	  ;
	}
	;
	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
    /*_#MELT_NEED_DBG__L5*/ meltfnum[1] =
	  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	  0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	  ;;
	MELT_LOCATION ("warmelt-genobj.melt:1013:/ cond");
	/*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[1])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

      /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[2] =
#ifdef meltcallcount
		meltcallcount	/* the_meltcallcount */
#else
		0L
#endif /* meltcallcount the_meltcallcount */
		;;
	      MELT_LOCATION ("warmelt-genobj.melt:1013:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		union meltparam_un argtab[5];
		memset (&argtab, 0, sizeof (argtab));
		/*^apply.arg */
		argtab[0].meltbp_long =
		  /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[2];
		/*^apply.arg */
		argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		/*^apply.arg */
		argtab[2].meltbp_long = 1013;
		/*^apply.arg */
		argtab[3].meltbp_cstring = "variadic_idstr bad variadsym=";
		/*^apply.arg */
		argtab[4].meltbp_aptr =
		  (melt_ptr_t *) & /*_.VARIADSYM__V2*/ meltfptr[1];
		/*_.MELT_DEBUG_FUN__V22*/ meltfptr[11] =
		  melt_apply ((meltclosure_ptr_t)
			      (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[6])),
			      (melt_ptr_t) (( /*nil */ NULL)),
			      (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			       MELTBPARSTR_PTR ""), argtab, "",
			      (union meltparam_un *) 0);
	      }
	      ;
	      /*_.IF___V21*/ meltfptr[18] =
		/*_.MELT_DEBUG_FUN__V22*/ meltfptr[11];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-genobj.melt:1013:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[2] = 0;
	      /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[11] = 0;
	    }
	    ;
	  }
	else
	  {			/*^cond.else */

     /*_.IF___V21*/ meltfptr[18] = NULL;;
	  }
	;
	MELT_LOCATION ("warmelt-genobj.melt:1013:/ quasiblock");


	/*_.PROGN___V23*/ meltfptr[5] = /*_.IF___V21*/ meltfptr[18];;
	/*^compute */
	/*_.IFCPP___V20*/ meltfptr[7] = /*_.PROGN___V23*/ meltfptr[5];;
	/*epilog */

	MELT_LOCATION ("warmelt-genobj.melt:1013:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[1] = 0;
	/*^clear */
	      /*clear *//*_.IF___V21*/ meltfptr[18] = 0;
	/*^clear */
	      /*clear *//*_.PROGN___V23*/ meltfptr[5] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V20*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;

#if MELT_HAVE_DEBUG
      MELT_LOCATION ("warmelt-genobj.melt:1014:/ cppif.then");
      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	/*^cond */
	/*cond */ if (( /*nil */ NULL))	/*then */
	  {
	    /*^cond.then */
	    /*_.IFELSE___V25*/ meltfptr[11] = ( /*nil */ NULL);;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1014:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {




	      {
		/*^locexp */
		melt_assert_failed (("variadic_idstr bad variadsym"),
				    ("warmelt-genobj.melt")
				    ? ("warmelt-genobj.melt") : __FILE__,
				    (1014) ? (1014) : __LINE__, __FUNCTION__);
		;
	      }
	      ;
		/*clear *//*_.IFELSE___V25*/ meltfptr[11] = 0;
	      /*epilog */
	    }
	    ;
	  }
	;
	/*^compute */
	/*_.IFCPP___V24*/ meltfptr[8] = /*_.IFELSE___V25*/ meltfptr[11];;
	/*epilog */

	MELT_LOCATION ("warmelt-genobj.melt:1014:/ clear");
	      /*clear *//*_.IFELSE___V25*/ meltfptr[11] = 0;
      }

#else /*MELT_HAVE_DEBUG */
      /*^cppif.else */
      /*_.IFCPP___V24*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1012:/ checksignal");
      MELT_CHECK_SIGNAL ();
      ;
      /*_.MATCHRES___V3*/ meltfptr[2] = ( /*nil */ NULL);;

      MELT_LOCATION ("warmelt-genobj.melt:1012:/ clear");
	    /*clear *//*_.IFCPP___V20*/ meltfptr[7] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V24*/ meltfptr[8] = 0;
      /*^objgoto */
      /*objgoto */ goto mtch1__end /*endmatch */ ;
      ;

    /*objlabel */ mtch1__end:;
      MELT_LOCATION ("warmelt-genobj.melt:986:/ objlabel");
      ;
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:985:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MATCHRES___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:985:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MATCHRES___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("VARIADIC_IDSTR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_13_warmelt_genobj_VARIADIC_IDSTR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_13_warmelt_genobj_VARIADIC_IDSTR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("VARIADIC_INDEX_IDSTR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1017:/ getarg");
 /*_.VARIADSYM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:1019:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "_ix";
      /*_.VARIADIC_IDSTR__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!VARIADIC_IDSTR */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.VARIADSYM__V2*/ meltfptr[1]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1017:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.VARIADIC_IDSTR__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1017:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.VARIADIC_IDSTR__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("VARIADIC_INDEX_IDSTR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("VARIADIC_LENGTH_IDSTR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1021:/ getarg");
 /*_.VARIADSYM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:1023:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "_len";
      /*_.VARIADIC_IDSTR__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!VARIADIC_IDSTR */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.VARIADSYM__V2*/ meltfptr[1]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1021:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.VARIADIC_IDSTR__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1021:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.VARIADIC_IDSTR__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("VARIADIC_LENGTH_IDSTR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un *
						  meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un *
						  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 86
    melt_ptr_t mcfr_varptr[86];
#define MELTFRAM_NBVARNUM 35
    long mcfr_varnum[35];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 86; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE nbval 86*/
  meltfram__.mcfr_nbvar = 86 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILE2OBJ_PROCEDURE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1030:/ getarg");
 /*_.PRO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.COMPICACHE__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.COMPICACHE__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#NUM__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1031:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1031:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1031:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[8];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1031;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_procedure pro=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PRO__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " num=";
	      /*^apply.arg */
	      argtab[6].meltbp_long = /*_#NUM__L1*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring =
		"\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n";
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1031:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1031:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1031:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1033:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_ROUTPROC */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:1033:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1033:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pro"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1033) ? (1033) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1033:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1034:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MAPOBJECT__L5*/ meltfnum[1] =
	/*is_mapobject: */
	(melt_magic_discr ((melt_ptr_t) ( /*_.COMPICACHE__V4*/ meltfptr[3]))
	 == MELTOBMAG_MAPOBJECTS);;
      MELT_LOCATION ("warmelt-genobj.melt:1034:/ cond");
      /*cond */ if ( /*_#IS_MAPOBJECT__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1034:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check compicache"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1034) ? (1034) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1034:/ clear");
	     /*clear *//*_#IS_MAPOBJECT__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1035:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:1035:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1035:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1035) ? (1035) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1035:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1036:/ quasiblock");


 /*_.NAMSBUF__V16*/ meltfptr[15] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			 (const char *) 0);;
    /*^compute */
    /*_.ROUTFUNAM__V17*/ meltfptr[16] = ( /*nil */ NULL);;
    /*^compute */
    /*_.RESTNAM__V18*/ meltfptr[17] = ( /*nil */ NULL);;
    MELT_LOCATION ("warmelt-genobj.melt:1039:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 4, "MOCX_FUNCOUNT");
   /*_.MOFUNCOUNT__V19*/ meltfptr[18] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MOFUNCOUNT__V19*/ meltfptr[18] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1040:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "MOCX_FILETUPLE");
   /*_.MOFILES__V20*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MOFILES__V20*/ meltfptr[19] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L7*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.MOFUNCOUNT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_#FUNUM__L8*/ meltfnum[2] =
      ((1) + ( /*_#GET_INT__L7*/ meltfnum[1]));;
    MELT_LOCATION ("warmelt-genobj.melt:1043:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L9*/ meltfnum[8] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_NREP_DEFUNROUTPROC */
					  meltfrout->tabval[4])));;
    MELT_LOCATION ("warmelt-genobj.melt:1043:/ cond");
    /*cond */ if ( /*_#IS_A__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1044:/ locexp");
	    melt_put_int ((melt_ptr_t) ( /*_.MOFUNCOUNT__V19*/ meltfptr[18]),
			  ( /*_#FUNUM__L8*/ meltfnum[2]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1045:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMSBUF__V16*/ meltfptr[15]),
			   ("meltrout_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1046:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMSBUF__V16*/ meltfptr[15]),
			     ( /*_#NUM__L1*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1047:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMSBUF__V16*/ meltfptr[15]),
			   ("_"));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1048:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "MOCX_MODULENAME");
   /*_.MOCX_MODULENAME__V21*/ meltfptr[20] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MOCX_MODULENAME__V21*/ meltfptr[20] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1048:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t)
				( /*_.NAMSBUF__V16*/ meltfptr[15]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.MOCX_MODULENAME__V21*/
						  meltfptr[20])));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1049:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L10*/ meltfnum[9] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_NREP_ROUTPROC */
					  meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-genobj.melt:1049:/ cond");
    /*cond */ if ( /*_#IS_A__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1050:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "NRPRO_NAME");
    /*_.PRONAM__V24*/ meltfptr[23] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1051:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 9, "NRPRO_VARIADIC");
    /*_.NVARIADIC__V25*/ meltfptr[24] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1053:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L11*/ meltfnum[10] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.PRONAM__V24*/ meltfptr[23]),
				 (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
						tabval[5])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1053:/ cond");
	  /*cond */ if ( /*_#IS_A__L11*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1055:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.PRONAM__V24*/ meltfptr[23]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V26*/ meltfptr[25] = slot;
		};
		;
		/*^compute */
		/*_.ROUTFUNAM__V17*/ meltfptr[16] =
		  /*_.SETQ___V27*/ meltfptr[26] =
		  /*_.NAMED_NAME__V26*/ meltfptr[25];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:1056:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.NAMSBUF__V16*/ meltfptr[15]),
				       ("_"));
		}
		;
		MELT_LOCATION ("warmelt-genobj.melt:1057:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.PRONAM__V24*/ meltfptr[23]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V28*/ meltfptr[27] = slot;
		};
		;

		{
		  /*^locexp */
		  meltgc_add_strbuf_cident ((melt_ptr_t)
					    ( /*_.NAMSBUF__V16*/
					     meltfptr[15]),
					    melt_string_str ((melt_ptr_t)
							     ( /*_.NAMED_NAME__V28*/ meltfptr[27])));
		}
		;
		MELT_LOCATION ("warmelt-genobj.melt:1058:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L12*/ meltfnum[11] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.PRONAM__V24*/ meltfptr[23]),
				       (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */ meltfrout->tabval[6])));;
		MELT_LOCATION ("warmelt-genobj.melt:1058:/ cond");
		/*cond */ if ( /*_#IS_A__L12*/ meltfnum[11])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-genobj.melt:1060:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.NAMSBUF__V16*/
					      meltfptr[15]), ("__"));
		      }
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1061:/ cond");
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.PRONAM__V24*/
							   meltfptr[23]),
							  (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */ meltfrout->tabval[6])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.PRONAM__V24*/ meltfptr[23])
			      /*=obj*/ ;
			    melt_object_get_field (slot, obj, 3,
						   "CSYM_URANK");
	 /*_.CSYM_URANK__V29*/ meltfptr[28] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

	/*_.CSYM_URANK__V29*/ meltfptr[28] = NULL;;
			}
		      ;
		      /*^compute */
       /*_#GET_INT__L13*/ meltfnum[12] =
			(melt_get_int
			 ((melt_ptr_t)
			  ( /*_.CSYM_URANK__V29*/ meltfptr[28])));;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1061:/ locexp");
			meltgc_add_strbuf_dec ((melt_ptr_t)
					       ( /*_.NAMSBUF__V16*/
						meltfptr[15]),
					       ( /*_#GET_INT__L13*/
						meltfnum[12]));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1062:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.NAMSBUF__V16*/
					      meltfptr[15]), ("__"));
		      }
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1059:/ quasiblock");


		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:1058:/ clear");
		 /*clear *//*_.CSYM_URANK__V29*/ meltfptr[28] = 0;
		      /*^clear */
		 /*clear *//*_#GET_INT__L13*/ meltfnum[12] = 0;
		    }
		    ;
		  }		/*noelse */
		;
		MELT_LOCATION ("warmelt-genobj.melt:1054:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1053:/ clear");
	       /*clear *//*_.NAMED_NAME__V26*/ meltfptr[25] = 0;
		/*^clear */
	       /*clear *//*_.SETQ___V27*/ meltfptr[26] = 0;
		/*^clear */
	       /*clear *//*_.NAMED_NAME__V28*/ meltfptr[27] = 0;
		/*^clear */
	       /*clear *//*_#IS_A__L12*/ meltfnum[11] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1065:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.NVARIADIC__V25*/ meltfptr[24])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1066:/ compute");
		/*_.RESTNAM__V18*/ meltfptr[17] =
		  /*_.SETQ___V31*/ meltfptr[25] =
		  /*_.NVARIADIC__V25*/ meltfptr[24];;
		/*_.IF___V30*/ meltfptr[28] = /*_.SETQ___V31*/ meltfptr[25];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1065:/ clear");
	       /*clear *//*_.SETQ___V31*/ meltfptr[25] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V30*/ meltfptr[28] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.LET___V23*/ meltfptr[22] = /*_.IF___V30*/ meltfptr[28];;

	  MELT_LOCATION ("warmelt-genobj.melt:1050:/ clear");
	     /*clear *//*_.PRONAM__V24*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.NVARIADIC__V25*/ meltfptr[24] = 0;
	  /*^clear */
	     /*clear *//*_#IS_A__L11*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V30*/ meltfptr[28] = 0;
	  /*_.IF___V22*/ meltfptr[21] = /*_.LET___V23*/ meltfptr[22];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1049:/ clear");
	     /*clear *//*_.LET___V23*/ meltfptr[22] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V22*/ meltfptr[21] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1068:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:1069:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NPROC_BODY");
  /*_.NBODY__V33*/ meltfptr[27] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1070:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V34*/ meltfptr[25] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1071:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L14*/ meltfnum[12] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_NREP_ROUTPROC */
					  meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-genobj.melt:1071:/ cond");
    /*cond */ if ( /*_#IS_A__L14*/ meltfnum[12])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 3, "NRPRO_ARGB");
    /*_.NRPRO_ARGB__V36*/ meltfptr[24] = slot;
	  };
	  ;
	  /*_.NARGB__V35*/ meltfptr[23] = /*_.NRPRO_ARGB__V36*/ meltfptr[24];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1071:/ clear");
	     /*clear *//*_.NRPRO_ARGB__V36*/ meltfptr[24] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.NARGB__V35*/ meltfptr[23] = NULL;;
      }
    ;
    /*^compute */
 /*_.OBODYLIST__V37*/ meltfptr[28] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    /*^compute */
 /*_#I__L15*/ meltfnum[11] =
      (( /*_#FUNUM__L8*/ meltfnum[2]) + (20));;
    /*^compute */
 /*_#FILENUM__L16*/ meltfnum[10] =
      (( /*_#I__L15*/ meltfnum[11]) / (25));;
    /*^compute */
 /*_#NBFILES__L17*/ meltfnum[16] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.MOFILES__V20*/ meltfptr[19])));;
    MELT_LOCATION ("warmelt-genobj.melt:1079:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V38*/ meltfptr[22] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[9])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMSBUF__V16*/ meltfptr[15]))));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V39*/ meltfptr[24] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[10])),
	(0)));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V40*/ meltfptr[39] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[10])),
	(0)));;
    /*^compute */
 /*_.MAKE_LIST__V41*/ meltfptr[40] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V42*/ meltfptr[41] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[10])),
	(0)));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V43*/ meltfptr[42] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[10])),
	(0)));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V44*/ meltfptr[43] =
      (meltgc_new_int
       ((meltobject_ptr_t)
	(( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[11])),
	( /*_#FILENUM__L16*/ meltfnum[10])));;
    MELT_LOCATION ("warmelt-genobj.melt:1079:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_PROCROUTINEOBJ */
					     meltfrout->tabval[8])), (15),
			      "CLASS_PROCROUTINEOBJ");
  /*_.INST__V46*/ meltfptr[45] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NAMED_NAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (1),
			  ( /*_.STRBUF2STRING__V38*/ meltfptr[22]),
			  "NAMED_NAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_PROC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (2),
			  ( /*_.PRO__V2*/ meltfptr[1]), "OBROUT_PROC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_BODY",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (3),
			  ( /*_.OBODYLIST__V37*/ meltfptr[28]),
			  "OBROUT_BODY");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_NBVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (4),
			  ( /*_.MAKE_INTEGERBOX__V39*/ meltfptr[24]),
			  "OBROUT_NBVAL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_NBLONG",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (5),
			  ( /*_.MAKE_INTEGERBOX__V40*/ meltfptr[39]),
			  "OBROUT_NBLONG");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_OTHERS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (6),
			  ( /*_.MAKE_LIST__V41*/ meltfptr[40]),
			  "OBROUT_OTHERS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_CNTCITER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (8),
			  ( /*_.MAKE_INTEGERBOX__V42*/ meltfptr[41]),
			  "OBROUT_CNTCITER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_CNTLETREC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (9),
			  ( /*_.MAKE_INTEGERBOX__V43*/ meltfptr[42]),
			  "OBROUT_CNTLETREC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPROUT_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (11),
			  ( /*_.NLOC__V34*/ meltfptr[25]), "OPROUT_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPROUT_FUNAM",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (12),
			  ( /*_.ROUTFUNAM__V17*/ meltfptr[16]),
			  "OPROUT_FUNAM");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPROUT_FILENUM",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (14),
			  ( /*_.MAKE_INTEGERBOX__V44*/ meltfptr[43]),
			  "OPROUT_FILENUM");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPROUT_RESTNAM",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V46*/ meltfptr[45])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V46*/ meltfptr[45]), (13),
			  ( /*_.RESTNAM__V18*/ meltfptr[17]),
			  "OPROUT_RESTNAM");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V46*/ meltfptr[45],
				  "newly made instance");
    ;
    /*_.OBROUT__V45*/ meltfptr[44] = /*_.INST__V46*/ meltfptr[45];;
    /*^compute */
 /*_#MULTIPLE_LENGTH__L18*/ meltfnum[17] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.NARGB__V35*/ meltfptr[23])));;
    /*^compute */
 /*_#I__L19*/ meltfnum[18] =
      ((3) * ( /*_#MULTIPLE_LENGTH__L18*/ meltfnum[17]));;
    /*^compute */
 /*_#I__L20*/ meltfnum[19] =
      ((20) + ( /*_#I__L19*/ meltfnum[18]));;
    /*^compute */
 /*_.LOCMAP__V47*/ meltfptr[46] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[12])),
	( /*_#I__L20*/ meltfnum[19])));;
    MELT_LOCATION ("warmelt-genobj.melt:1094:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_LIST__V48*/ meltfptr[47] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    /*^compute */
 /*_.MAKE_LIST__V49*/ meltfptr[48] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[7]))));;
    /*^compute */
 /*_.MAKE_MAPOBJECT__V50*/ meltfptr[49] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[12])),
	(20)));;
    /*^compute */
 /*_.MAKE_MAPOBJECT__V51*/ meltfptr[50] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[12])),
	(40)));;
    MELT_LOCATION ("warmelt-genobj.melt:1094:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					     meltfrout->tabval[13])), (10),
			      "CLASS_C_GENERATION_CONTEXT");
  /*_.INST__V53*/ meltfptr[52] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_OBJROUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V53*/ meltfptr[52])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V53*/ meltfptr[52]), (0),
			  ( /*_.OBROUT__V45*/ meltfptr[44]), "GNCX_OBJROUT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_LOCMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V53*/ meltfptr[52])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V53*/ meltfptr[52]), (1),
			  ( /*_.LOCMAP__V47*/ meltfptr[46]), "GNCX_LOCMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_FREEPTRLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V53*/ meltfptr[52])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V53*/ meltfptr[52]), (2),
			  ( /*_.MAKE_LIST__V48*/ meltfptr[47]),
			  "GNCX_FREEPTRLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_FREELONGLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V53*/ meltfptr[52])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V53*/ meltfptr[52]), (3),
			  ( /*_.MAKE_LIST__V49*/ meltfptr[48]),
			  "GNCX_FREELONGLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_FREEOTHERMAPS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V53*/ meltfptr[52])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V53*/ meltfptr[52]), (4),
			  ( /*_.MAKE_MAPOBJECT__V50*/ meltfptr[49]),
			  "GNCX_FREEOTHERMAPS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_COMPICACHE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V53*/ meltfptr[52])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V53*/ meltfptr[52]), (6),
			  ( /*_.COMPICACHE__V4*/ meltfptr[3]),
			  "GNCX_COMPICACHE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_MODULCONTEXT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V53*/ meltfptr[52])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V53*/ meltfptr[52]), (7),
			  ( /*_.MODCTX__V3*/ meltfptr[2]),
			  "GNCX_MODULCONTEXT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_MATCHMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V53*/ meltfptr[52])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V53*/ meltfptr[52]), (8),
			  ( /*_.MAKE_MAPOBJECT__V51*/ meltfptr[50]),
			  "GNCX_MATCHMAP");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V53*/ meltfptr[52],
				  "newly made instance");
    ;
    /*_.GCX__V52*/ meltfptr[51] = /*_.INST__V53*/ meltfptr[52];;
    MELT_LOCATION ("warmelt-genobj.melt:1105:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_15_RETVAL_ */ meltfrout->tabval[15]);
      /*_.RETL__V55*/ meltfptr[54] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.GCX__V52*/ meltfptr[51]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1106:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_RETLOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.GCX__V52*/ meltfptr[51])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.GCX__V52*/ meltfptr[51]), (5),
			  ( /*_.RETL__V55*/ meltfptr[54]), "GNCX_RETLOC");
    ;
    /*^touch */
    meltgc_touch ( /*_.GCX__V52*/ meltfptr[51]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.GCX__V52*/ meltfptr[51], "put-fields");
    ;

    /*_.LET___V54*/ meltfptr[53] = /*_.RETL__V55*/ meltfptr[54];;

    MELT_LOCATION ("warmelt-genobj.melt:1105:/ clear");
	   /*clear *//*_.RETL__V55*/ meltfptr[54] = 0;
    /*_.RETLOC__V56*/ meltfptr[54] = /*_.LET___V54*/ meltfptr[53];;
    MELT_LOCATION ("warmelt-genobj.melt:1111:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V58*/ meltfptr[57] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_26 */ meltfrout->
						tabval[26])), (4));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V58*/ meltfptr[57])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V58*/ meltfptr[57])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V58*/ meltfptr[57])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V52*/ meltfptr[51]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V58*/ meltfptr[57])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V58*/ meltfptr[57])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V58*/ meltfptr[57])->tabval[1] =
      (melt_ptr_t) ( /*_.RESTNAM__V18*/ meltfptr[17]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V58*/ meltfptr[57])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V58*/ meltfptr[57])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V58*/ meltfptr[57])->tabval[2] =
      (melt_ptr_t) ( /*_.NLOC__V34*/ meltfptr[25]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V58*/ meltfptr[57])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V58*/ meltfptr[57])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V58*/ meltfptr[57])->tabval[3] =
      (melt_ptr_t) ( /*_.LOCMAP__V47*/ meltfptr[46]);
    ;
    /*_.LAMBDA___V57*/ meltfptr[56] = /*_.LAMBDA___V58*/ meltfptr[57];;
    MELT_LOCATION ("warmelt-genobj.melt:1109:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V57*/ meltfptr[56];
      /*_.GTATUP__V59*/ meltfptr[58] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[16])),
		    (melt_ptr_t) ( /*_.NARGB__V35*/ meltfptr[23]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1144:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L21*/ meltfnum[20] =
      (( /*_#FILENUM__L16*/ meltfnum[10]) >=
       ( /*_#NBFILES__L17*/ meltfnum[16]));;
    MELT_LOCATION ("warmelt-genobj.melt:1144:/ cond");
    /*cond */ if ( /*_#I__L21*/ meltfnum[20])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1145:/ quasiblock");


   /*_#IRAW__L22*/ meltfnum[21] =
	    (( /*_#FILENUM__L16*/ meltfnum[10]) / (4));;
	  /*^compute */
   /*_#I__L23*/ meltfnum[22] =
	    ((2) + ( /*_#IRAW__L22*/ meltfnum[21]));;
	  /*^compute */
   /*_#NEWNBFILES__L24*/ meltfnum[23] =
	    (( /*_#NBFILES__L17*/ meltfnum[16]) +
	     ( /*_#I__L23*/ meltfnum[22]));;
	  /*^compute */
   /*_.NEWFILETUP__V60*/ meltfptr[59] =
	    (meltgc_new_multiple
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MULTIPLE */ meltfrout->tabval[27])),
	      ( /*_#NEWNBFILES__L24*/ meltfnum[23])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1148:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.MODCTX__V3*/ meltfptr[2]),
					      (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[2])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @MOCX_FILETUPLE",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.MODCTX__V3*/
						   meltfptr[2])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.MODCTX__V3*/ meltfptr[2]), (5),
				      ( /*_.NEWFILETUP__V60*/ meltfptr[59]),
				      "MOCX_FILETUPLE");
		;
		/*^touch */
		meltgc_touch ( /*_.MODCTX__V3*/ meltfptr[2]);
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.MODCTX__V3*/ meltfptr[2],
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*citerblock FOREACH_IN_MULTIPLE */
	  {
	    /* start foreach_in_multiple meltcit1__EACHTUP */
	    long meltcit1__EACHTUP_ln =
	      melt_multiple_length ((melt_ptr_t) /*_.MOFILES__V20*/
				    meltfptr[19]);
	    for ( /*_#CURIX__L25*/ meltfnum[24] = 0;
		 ( /*_#CURIX__L25*/ meltfnum[24] >= 0)
		 && ( /*_#CURIX__L25*/ meltfnum[24] < meltcit1__EACHTUP_ln);
	/*_#CURIX__L25*/ meltfnum[24]++)
	      {
		/*_.CURFILE__V61*/ meltfptr[60] =
		  melt_multiple_nth ((melt_ptr_t)
				     ( /*_.MOFILES__V20*/ meltfptr[19]),
				     /*_#CURIX__L25*/ meltfnum[24]);




		{
		  MELT_LOCATION ("warmelt-genobj.melt:1152:/ locexp");
		  meltgc_multiple_put_nth ((melt_ptr_t)
					   ( /*_.NEWFILETUP__V60*/
					    meltfptr[59]),
					   ( /*_#CURIX__L25*/ meltfnum[24]),
					   (melt_ptr_t) ( /*_.CURFILE__V61*/
							 meltfptr[60]));
		}
		;
		if ( /*_#CURIX__L25*/ meltfnum[24] < 0)
		  break;
	      }			/* end  foreach_in_multiple meltcit1__EACHTUP */

	    /*citerepilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1149:/ clear");
	      /*clear *//*_.CURFILE__V61*/ meltfptr[60] = 0;
	    /*^clear */
	      /*clear *//*_#CURIX__L25*/ meltfnum[24] = 0;
	  }			/*endciterblock FOREACH_IN_MULTIPLE */
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:1145:/ clear");
	     /*clear *//*_#IRAW__L22*/ meltfnum[21] = 0;
	  /*^clear */
	     /*clear *//*_#I__L23*/ meltfnum[22] = 0;
	  /*^clear */
	     /*clear *//*_#NEWNBFILES__L24*/ meltfnum[23] = 0;
	  /*^clear */
	     /*clear *//*_.NEWFILETUP__V60*/ meltfptr[59] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1155:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     ( /*_.COMPICACHE__V4*/ meltfptr[3]),
			     (meltobject_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]),
			     (melt_ptr_t) ( /*_.OBROUT__V45*/ meltfptr[44]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1156:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OPROUT_GETARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.OBROUT__V45*/ meltfptr[44])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.OBROUT__V45*/ meltfptr[44]), (10),
			  ( /*_.GTATUP__V59*/ meltfptr[58]),
			  "OPROUT_GETARGS");
    ;
    /*^touch */
    meltgc_touch ( /*_.OBROUT__V45*/ meltfptr[44]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.OBROUT__V45*/ meltfptr[44],
				  "put-fields");
    ;


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1157:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L26*/ meltfnum[21] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1157:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[21])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[22] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1157:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[8];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L27*/ meltfnum[22];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1157;
	      /*^apply.arg */
	      argtab[3].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBROUT__V45*/ meltfptr[44];
	      /*^apply.arg */
	      argtab[4].meltbp_cstring = "compile2obj_procedure obrout=";
	      /*^apply.arg */
	      argtab[5].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBROUT__V45*/ meltfptr[44];
	      /*^apply.arg */
	      argtab[6].meltbp_cstring = " nbody=";
	      /*^apply.arg */
	      argtab[7].meltbp_aptr =
		(melt_ptr_t *) & /*_.NBODY__V33*/ meltfptr[27];
	      /*_.MELT_DEBUG_FUN__V64*/ meltfptr[63] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_PTR
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V63*/ meltfptr[62] =
	      /*_.MELT_DEBUG_FUN__V64*/ meltfptr[63];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1157:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[22] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V64*/ meltfptr[63] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V63*/ meltfptr[62] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1157:/ quasiblock");


      /*_.PROGN___V65*/ meltfptr[63] = /*_.IF___V63*/ meltfptr[62];;
      /*^compute */
      /*_.IFCPP___V62*/ meltfptr[59] = /*_.PROGN___V65*/ meltfptr[63];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1157:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[21] = 0;
      /*^clear */
	     /*clear *//*_.IF___V63*/ meltfptr[62] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V65*/ meltfptr[63] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V62*/ meltfptr[59] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1158:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L28*/ meltfnum[23] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NBODY__V33*/ meltfptr[27]),
			     (melt_ptr_t) (( /*!CLASS_NREP */ meltfrout->
					    tabval[28])));;
      MELT_LOCATION ("warmelt-genobj.melt:1158:/ cond");
      /*cond */ if ( /*_#IS_A__L28*/ meltfnum[23])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V67*/ meltfptr[63] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1158:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nbody"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1158) ? (1158) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V67*/ meltfptr[63] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V66*/ meltfptr[62] = /*_.IFELSE___V67*/ meltfptr[63];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1158:/ clear");
	     /*clear *//*_#IS_A__L28*/ meltfnum[23] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V67*/ meltfptr[63] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V66*/ meltfptr[62] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1159:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L29*/ meltfnum[22] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_NREP_ROUTPROC */
					  meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-genobj.melt:1159:/ cond");
    /*cond */ if ( /*_#IS_A__L29*/ meltfnum[22])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1160:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.PRO__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 8, "NRPRO_THUNKLIST");
    /*_.PTHULS__V70*/ meltfptr[69] = slot;
	  };
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1161:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L30*/ meltfnum[21] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1161:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L30*/ meltfnum[21])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[23] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1161:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[23];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1161;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_procedure pthuls=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.PTHULS__V70*/ meltfptr[69];
		    /*_.MELT_DEBUG_FUN__V73*/ meltfptr[72] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V72*/ meltfptr[71] =
		    /*_.MELT_DEBUG_FUN__V73*/ meltfptr[72];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1161:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L31*/ meltfnum[23] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V73*/ meltfptr[72] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V72*/ meltfptr[71] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1161:/ quasiblock");


	    /*_.PROGN___V74*/ meltfptr[72] = /*_.IF___V72*/ meltfptr[71];;
	    /*^compute */
	    /*_.IFCPP___V71*/ meltfptr[70] = /*_.PROGN___V74*/ meltfptr[72];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1161:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L30*/ meltfnum[21] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V72*/ meltfptr[71] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V74*/ meltfptr[72] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V71*/ meltfptr[70] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1164:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V76*/ meltfptr[72] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_31 */
						      meltfrout->tabval[31])),
				(1));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V76*/
					     meltfptr[72])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V76*/
					      meltfptr[72])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V76*/ meltfptr[72])->tabval[0] =
	    (melt_ptr_t) ( /*_.GCX__V52*/ meltfptr[51]);
	  ;
	  /*_.LAMBDA___V75*/ meltfptr[71] = /*_.LAMBDA___V76*/ meltfptr[72];;
	  MELT_LOCATION ("warmelt-genobj.melt:1162:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V75*/ meltfptr[71];
	    /*_.LIST_EVERY__V77*/ meltfptr[76] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[29])),
			  (melt_ptr_t) ( /*_.PTHULS__V70*/ meltfptr[69]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.LET___V69*/ meltfptr[68] = /*_.LIST_EVERY__V77*/ meltfptr[76];;

	  MELT_LOCATION ("warmelt-genobj.melt:1160:/ clear");
	     /*clear *//*_.PTHULS__V70*/ meltfptr[69] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V71*/ meltfptr[70] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V75*/ meltfptr[71] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V77*/ meltfptr[76] = 0;
	  /*_.IF___V68*/ meltfptr[63] = /*_.LET___V69*/ meltfptr[68];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1159:/ clear");
	     /*clear *//*_.LET___V69*/ meltfptr[68] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V68*/ meltfptr[63] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1170:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V52*/ meltfptr[51];
      /*_.OBODY__V78*/ meltfptr[69] =
	meltgc_send ((melt_ptr_t) ( /*_.NBODY__V33*/ meltfptr[27]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
				    tabval[32])), (MELTBPARSTR_PTR ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1172:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L32*/ meltfnum[23] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1172:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L32*/ meltfnum[23])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L33*/ meltfnum[21] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1172:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L33*/ meltfnum[21];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1172;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_procedure obody=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBODY__V78*/ meltfptr[69];
	      /*_.MELT_DEBUG_FUN__V81*/ meltfptr[76] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V80*/ meltfptr[71] =
	      /*_.MELT_DEBUG_FUN__V81*/ meltfptr[76];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1172:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L33*/ meltfnum[21] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V81*/ meltfptr[76] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V80*/ meltfptr[71] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1172:/ quasiblock");


      /*_.PROGN___V82*/ meltfptr[68] = /*_.IF___V80*/ meltfptr[71];;
      /*^compute */
      /*_.IFCPP___V79*/ meltfptr[70] = /*_.PROGN___V82*/ meltfptr[68];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1172:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L32*/ meltfnum[23] = 0;
      /*^clear */
	     /*clear *//*_.IF___V80*/ meltfptr[71] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V82*/ meltfptr[68] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V79*/ meltfptr[70] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1173:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OBODYLIST__V37*/ meltfptr[28]),
			  (melt_ptr_t) ( /*_.OBODY__V78*/ meltfptr[69]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:1170:/ clear");
	   /*clear *//*_.OBODY__V78*/ meltfptr[69] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V79*/ meltfptr[70] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1175:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L34*/ meltfnum[21] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1175:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L34*/ meltfnum[21])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L35*/ meltfnum[23] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1175:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L35*/ meltfnum[23];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1175;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_procedure return obrout=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBROUT__V45*/ meltfptr[44];
	      /*_.MELT_DEBUG_FUN__V85*/ meltfptr[68] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V84*/ meltfptr[71] =
	      /*_.MELT_DEBUG_FUN__V85*/ meltfptr[68];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1175:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L35*/ meltfnum[23] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V85*/ meltfptr[68] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V84*/ meltfptr[71] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1175:/ quasiblock");


      /*_.PROGN___V86*/ meltfptr[69] = /*_.IF___V84*/ meltfptr[71];;
      /*^compute */
      /*_.IFCPP___V83*/ meltfptr[76] = /*_.PROGN___V86*/ meltfptr[69];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1175:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L34*/ meltfnum[21] = 0;
      /*^clear */
	     /*clear *//*_.IF___V84*/ meltfptr[71] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V86*/ meltfptr[69] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V83*/ meltfptr[76] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V32*/ meltfptr[26] = /*_.OBROUT__V45*/ meltfptr[44];;

    MELT_LOCATION ("warmelt-genobj.melt:1068:/ clear");
	   /*clear *//*_.NBODY__V33*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V34*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L14*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.NARGB__V35*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OBODYLIST__V37*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_#I__L15*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_#FILENUM__L16*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_#NBFILES__L17*/ meltfnum[16] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V38*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V39*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V42*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V44*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.OBROUT__V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L18*/ meltfnum[17] = 0;
    /*^clear */
	   /*clear *//*_#I__L19*/ meltfnum[18] = 0;
    /*^clear */
	   /*clear *//*_#I__L20*/ meltfnum[19] = 0;
    /*^clear */
	   /*clear *//*_.LOCMAP__V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V48*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V49*/ meltfptr[48] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPOBJECT__V50*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPOBJECT__V51*/ meltfptr[50] = 0;
    /*^clear */
	   /*clear *//*_.GCX__V52*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_.LET___V54*/ meltfptr[53] = 0;
    /*^clear */
	   /*clear *//*_.RETLOC__V56*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V57*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_.GTATUP__V59*/ meltfptr[58] = 0;
    /*^clear */
	   /*clear *//*_#I__L21*/ meltfnum[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V62*/ meltfptr[59] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V66*/ meltfptr[62] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L29*/ meltfnum[22] = 0;
    /*^clear */
	   /*clear *//*_.IF___V68*/ meltfptr[63] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V83*/ meltfptr[76] = 0;
    /*_.LET___V15*/ meltfptr[13] = /*_.LET___V32*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-genobj.melt:1036:/ clear");
	   /*clear *//*_.NAMSBUF__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.ROUTFUNAM__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.RESTNAM__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.MOFUNCOUNT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.MOFILES__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L7*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#FUNUM__L8*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.MOCX_MODULENAME__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IF___V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LET___V32*/ meltfptr[26] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1030:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V15*/ meltfptr[13];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1030:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[13] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILE2OBJ_PROCEDURE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_genobj_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_17_warmelt_genobj_LAMBDA___1___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_17_warmelt_genobj_LAMBDA___1___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_17_warmelt_genobj_LAMBDA___1__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_17_warmelt_genobj_LAMBDA___1___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_17_warmelt_genobj_LAMBDA___1__ nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1111:/ getarg");
 /*_.BND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1112:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:1112:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1112:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bnd"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1112) ? (1112) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1112:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1113:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "FBIND_TYPE");
  /*_.BCTYP__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1114:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.BNAM__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1116:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L3*/ meltfnum[1] =
      (( /*_.BCTYP__V6*/ meltfptr[5]) ==
       (( /*!CTYPE_VALUE */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-genobj.melt:1116:/ cond");
    /*cond */ if ( /*_#__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1117:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BNAM__V7*/ meltfptr[6];
	    /*_.GET_FREE_OBJLOCPTR__V9*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[2])),
			  (melt_ptr_t) (( /*~GCX */ meltfclos->tabval[0])),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.OLOC__V8*/ meltfptr[7] =
	    /*_.GET_FREE_OBJLOCPTR__V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1116:/ clear");
	     /*clear *//*_.GET_FREE_OBJLOCPTR__V9*/ meltfptr[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1118:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L4*/ meltfnum[3] =
	    (( /*_.BCTYP__V6*/ meltfptr[5]) ==
	     (( /*!CTYPE_LONG */ meltfrout->tabval[3])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1118:/ cond");
	  /*cond */ if ( /*_#__L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1119:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.BNAM__V7*/ meltfptr[6];
		  /*_.GET_FREE_OBJLOCLONG__V11*/ meltfptr[10] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!GET_FREE_OBJLOCLONG */ meltfrout->
				  tabval[4])),
				(melt_ptr_t) (( /*~GCX */ meltfclos->
					       tabval[0])),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V10*/ meltfptr[8] =
		  /*_.GET_FREE_OBJLOCLONG__V11*/ meltfptr[10];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1118:/ clear");
	       /*clear *//*_.GET_FREE_OBJLOCLONG__V11*/ meltfptr[10] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1121:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[2];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.BNAM__V7*/ meltfptr[6];
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.BCTYP__V6*/ meltfptr[5];
		  /*_.GET_FREE_OBJLOCTYPED__V12*/ meltfptr[10] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!GET_FREE_OBJLOCTYPED */ meltfrout->
				  tabval[5])),
				(melt_ptr_t) (( /*~GCX */ meltfclos->
					       tabval[0])),
				(MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab,
				"", (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-genobj.melt:1120:/ quasiblock");


		/*_.PROGN___V13*/ meltfptr[12] =
		  /*_.GET_FREE_OBJLOCTYPED__V12*/ meltfptr[10];;
		/*^compute */
		/*_.IFELSE___V10*/ meltfptr[8] =
		  /*_.PROGN___V13*/ meltfptr[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1118:/ clear");
	       /*clear *//*_.GET_FREE_OBJLOCTYPED__V12*/ meltfptr[10] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V13*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.OLOC__V8*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1116:/ clear");
	     /*clear *//*_#__L4*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1124:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (( /*~RESTNAM */ meltfclos->tabval[1]))	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1125:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJGETARGREST */
						   meltfrout->tabval[6])),
				    (4), "CLASS_OBJGETARGREST");
    /*_.INST__V16*/ meltfptr[8] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBI_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V16*/ meltfptr[8])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V16*/ meltfptr[8]), (0),
				(( /*~NLOC */ meltfclos->tabval[2])),
				"OBI_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBARG_OBLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V16*/ meltfptr[8])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V16*/ meltfptr[8]), (1),
				( /*_.OLOC__V8*/ meltfptr[7]), "OBARG_OBLOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBARG_BIND",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V16*/ meltfptr[8])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V16*/ meltfptr[8]), (2),
				( /*_.BND__V2*/ meltfptr[1]), "OBARG_BIND");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBARG_REST",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V16*/ meltfptr[8])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V16*/ meltfptr[8]), (3),
				(( /*~RESTNAM */ meltfclos->tabval[1])),
				"OBARG_REST");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V16*/ meltfptr[8],
					"newly made instance");
	  ;
	  /*_.INST___V15*/ meltfptr[12] = /*_.INST__V16*/ meltfptr[8];;
	  /*^compute */
	  /*_.OGARG__V14*/ meltfptr[10] = /*_.INST___V15*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1124:/ clear");
	     /*clear *//*_.INST___V15*/ meltfptr[12] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1130:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJGETARG */
						   meltfrout->tabval[7])),
				    (3), "CLASS_OBJGETARG");
    /*_.INST__V18*/ meltfptr[17] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBI_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V18*/ meltfptr[17]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (0),
				(( /*~NLOC */ meltfclos->tabval[2])),
				"OBI_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBARG_OBLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V18*/ meltfptr[17]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (1),
				( /*_.OLOC__V8*/ meltfptr[7]), "OBARG_OBLOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBARG_BIND",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V18*/ meltfptr[17]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (2),
				( /*_.BND__V2*/ meltfptr[1]), "OBARG_BIND");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V18*/ meltfptr[17],
					"newly made instance");
	  ;
	  /*_.INST___V17*/ meltfptr[12] = /*_.INST__V18*/ meltfptr[17];;
	  /*^compute */
	  /*_.OGARG__V14*/ meltfptr[10] = /*_.INST___V17*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1124:/ clear");
	     /*clear *//*_.INST___V17*/ meltfptr[12] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1135:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L5*/ meltfnum[3] =
      (( /*_#IX__L1*/ meltfnum[0]) <= (0));;
    MELT_LOCATION ("warmelt-genobj.melt:1135:/ cond");
    /*cond */ if ( /*_#I__L5*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#__L7*/ meltfnum[6] =
	    (( /*_.BCTYP__V6*/ meltfptr[5]) !=
	     (( /*!CTYPE_VALUE */ meltfrout->tabval[1])));;
	  /*^compute */
	  /*_#IF___L6*/ meltfnum[5] = /*_#__L7*/ meltfnum[6];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1135:/ clear");
	     /*clear *//*_#__L7*/ meltfnum[6] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L6*/ meltfnum[5] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1135:/ cond");
    /*cond */ if ( /*_#IF___L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1136:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t)
			      (( /*~NLOC */ meltfclos->tabval[2])),
			      ("first argument of function should be a value"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1137:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1137:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1137:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1137;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_procedure formal bnd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BND__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " oloc=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OLOC__V8*/ meltfptr[7];
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[8])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V20*/ meltfptr[19] =
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1137:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V20*/ meltfptr[19] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1137:/ quasiblock");


      /*_.PROGN___V22*/ meltfptr[20] = /*_.IF___V20*/ meltfptr[19];;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[12] = /*_.PROGN___V22*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1137:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V20*/ meltfptr[19] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1138:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     (( /*~LOCMAP */ meltfclos->tabval[3])),
			     (meltobject_ptr_t) ( /*_.BND__V2*/ meltfptr[1]),
			     (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]));
    }
    ;
    /*_.LET___V5*/ meltfptr[3] = /*_.OGARG__V14*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-genobj.melt:1113:/ clear");
	   /*clear *//*_.BCTYP__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.BNAM__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OGARG__V14*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#IF___L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[12] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1111:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1111:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_17_warmelt_genobj_LAMBDA___1___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_17_warmelt_genobj_LAMBDA___1__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_genobj_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_18_warmelt_genobj_LAMBDA___2___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_18_warmelt_genobj_LAMBDA___2___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_18_warmelt_genobj_LAMBDA___2__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_18_warmelt_genobj_LAMBDA___2___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_18_warmelt_genobj_LAMBDA___2__ nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1164:/ getarg");
 /*_.PTHU__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1165:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1165:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1165:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1165;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_procedure pthu=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PTHU__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1165:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1165:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1165:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1166:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_CLOSURE__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.PTHU__V2*/ meltfptr[1])) ==
	 MELTOBMAG_CLOSURE);;
      MELT_LOCATION ("warmelt-genobj.melt:1166:/ cond");
      /*cond */ if ( /*_#IS_CLOSURE__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1166:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compile2obj_procedure check pthu"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1166) ? (1166) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[3] = /*_.IFELSE___V8*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1166:/ clear");
	     /*clear *//*_#IS_CLOSURE__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1167:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.PTHU__V9*/ meltfptr[4] =
	melt_apply ((meltclosure_ptr_t) ( /*_.PTHU__V2*/ meltfptr[1]),
		    (melt_ptr_t) (( /*~GCX */ meltfclos->tabval[0])), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1164:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.PTHU__V9*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1164:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.PTHU__V9*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_18_warmelt_genobj_LAMBDA___2___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_18_warmelt_genobj_LAMBDA___2__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_genobj_APPEND_COMMENT (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_19_warmelt_genobj_APPEND_COMMENT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_19_warmelt_genobj_APPEND_COMMENT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_19_warmelt_genobj_APPEND_COMMENT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_19_warmelt_genobj_APPEND_COMMENT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_19_warmelt_genobj_APPEND_COMMENT nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("APPEND_COMMENT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1188:/ getarg");
 /*_.ILIST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.COMSTR__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.COMSTR__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ILOC__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ILOC__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:1189:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMMENTINSTR */
					     meltfrout->tabval[0])), (2),
			      "CLASS_OBJCOMMENTINSTR");
  /*_.INST__V6*/ meltfptr[5] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (0),
			  ( /*_.ILOC__V4*/ meltfptr[3]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCI_COMMENT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (1),
			  ( /*_.COMSTR__V3*/ meltfptr[2]), "OBCI_COMMENT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V6*/ meltfptr[5],
				  "newly made instance");
    ;
    /*_.INST___V5*/ meltfptr[4] = /*_.INST__V6*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1189:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.ILIST__V2*/ meltfptr[1]),
			  (melt_ptr_t) ( /*_.INST___V5*/ meltfptr[4]));
    }
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-genobj.melt:1188:/ clear");
	   /*clear *//*_.INST___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("APPEND_COMMENT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_19_warmelt_genobj_APPEND_COMMENT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_19_warmelt_genobj_APPEND_COMMENT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_genobj_APPEND_COMMENTCONST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_20_warmelt_genobj_APPEND_COMMENTCONST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_20_warmelt_genobj_APPEND_COMMENTCONST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_20_warmelt_genobj_APPEND_COMMENTCONST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_20_warmelt_genobj_APPEND_COMMENTCONST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_20_warmelt_genobj_APPEND_COMMENTCONST nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("APPEND_COMMENTCONST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1190:/ getarg");
 /*_.ILIST__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[0].meltbp_cstring;

  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ILOC__V3*/ meltfptr[2] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ILOC__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:1191:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_STRINGCONST__V4*/ meltfptr[3] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[1])),
	( /*_?*/ meltfram__.loc_CSTRING__o0)));;
    MELT_LOCATION ("warmelt-genobj.melt:1191:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMMENTINSTR */
					     meltfrout->tabval[0])), (2),
			      "CLASS_OBJCOMMENTINSTR");
  /*_.INST__V6*/ meltfptr[5] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (0),
			  ( /*_.ILOC__V3*/ meltfptr[2]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCI_COMMENT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V6*/ meltfptr[5])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V6*/ meltfptr[5]), (1),
			  ( /*_.MAKE_STRINGCONST__V4*/ meltfptr[3]),
			  "OBCI_COMMENT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V6*/ meltfptr[5],
				  "newly made instance");
    ;
    /*_.INST___V5*/ meltfptr[4] = /*_.INST__V6*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1191:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.ILIST__V2*/ meltfptr[1]),
			  (melt_ptr_t) ( /*_.INST___V5*/ meltfptr[4]));
    }
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-genobj.melt:1190:/ clear");
	   /*clear *//*_.MAKE_STRINGCONST__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.INST___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("APPEND_COMMENTCONST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_20_warmelt_genobj_APPEND_COMMENTCONST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_20_warmelt_genobj_APPEND_COMMENTCONST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 171
    melt_ptr_t mcfr_varptr[171];
#define MELTFRAM_NBVARNUM 25
    long mcfr_varnum[25];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 171; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC nbval 171*/
  meltfram__.mcfr_nbvar = 171 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILE2OBJ_INITPROC", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1197:/ getarg");
 /*_.IPRO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IDATA__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IDATA__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.COMPICACHE__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.COMPICACHE__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PROCURMODENVLIST__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr
	      ((melt_ptr_t) ( /*_.PROCURMODENVLIST__V6*/ meltfptr[5])) !=
	      NULL);


  /*getarg#5 */
  /*^getarg */
  if (meltxargdescr_[4] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPORTVALUES__V7*/ meltfptr[6] =
    (meltxargtab_[4].meltbp_aptr) ? (*(meltxargtab_[4].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPORTVALUES__V7*/ meltfptr[6]))
	      != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1198:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1198:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1198:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[14];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1198;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_initproc ipro=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.IPRO__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* modctx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n* compicache=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.COMPICACHE__V5*/ meltfptr[4];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n* procurmodenvlist=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.PROCURMODENVLIST__V6*/ meltfptr[5];
	      /*^apply.arg */
	      argtab[11].meltbp_cstring = "\n* importvalues=";
	      /*^apply.arg */
	      argtab[12].meltbp_aptr =
		(melt_ptr_t *) & /*_.IMPORTVALUES__V7*/ meltfptr[6];
	      /*^apply.arg */
	      argtab[13].meltbp_cstring =
		"\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n";
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1198:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1198:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[7] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1198:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1204:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.IPRO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_INITPROC */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:1204:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1204:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ipro"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1204) ? (1204) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[8] = /*_.IFELSE___V13*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1204:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1205:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.IDATA__V4*/ meltfptr[3])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-genobj.melt:1205:/ cond");
      /*cond */ if ( /*_#IS_LIST__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1205:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check idata"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1205) ? (1205) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[9] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1205:/ clear");
	     /*clear *//*_#IS_LIST__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1206:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:1206:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1206:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1206) ? (1206) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1206:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1207:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MAPOBJECT__L6*/ meltfnum[0] =
	/*is_mapobject: */
	(melt_magic_discr ((melt_ptr_t) ( /*_.COMPICACHE__V5*/ meltfptr[4]))
	 == MELTOBMAG_MAPOBJECTS);;
      MELT_LOCATION ("warmelt-genobj.melt:1207:/ cond");
      /*cond */ if ( /*_#IS_MAPOBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1207:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check compicache"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1207) ? (1207) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[16] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1207:/ clear");
	     /*clear *//*_#IS_MAPOBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1208:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L7*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.IMPORTVALUES__V7*/ meltfptr[6]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-genobj.melt:1208:/ cond");
      /*cond */ if ( /*_#IS_LIST__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V21*/ meltfptr[20] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1208:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check importvalues"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1208) ? (1208) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[18] = /*_.IFELSE___V21*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1208:/ clear");
	     /*clear *//*_#IS_LIST__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1209:/ quasiblock");


 /*_.LOCMAP__V23*/ meltfptr[22] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[3])),
	(50)));;
    /*^compute */
 /*_.OINIPROLOG__V24*/ meltfptr[23] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    /*^compute */
 /*_.OINIBODY__V25*/ meltfptr[24] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    MELT_LOCATION ("warmelt-genobj.melt:1213:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_INTEGERBOX__V26*/ meltfptr[25] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[7])),
	(0)));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V27*/ meltfptr[26] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[7])),
	(0)));;
    /*^compute */
 /*_.MAKE_LIST__V28*/ meltfptr[27] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V29*/ meltfptr[28] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[7])),
	(0)));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V30*/ meltfptr[29] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[7])),
	(0)));;
    /*^compute */
 /*_.MAKE_LIST__V31*/ meltfptr[30] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    MELT_LOCATION ("warmelt-genobj.melt:1229:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "MOCX_MODULENAME");
   /*_.MOCX_MODULENAME__V32*/ meltfptr[31] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MOCX_MODULENAME__V32*/ meltfptr[31] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1213:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_INITIAL_MODULE_ROUTINEOBJ */ meltfrout->tabval[5])), (14), "CLASS_INITIAL_MODULE_ROUTINEOBJ");
  /*_.INST__V34*/ meltfptr[33] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NAMED_NAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (1),
			  (( /*!konst_6 */ meltfrout->tabval[6])),
			  "NAMED_NAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_PROC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (2),
			  ( /*_.IPRO__V2*/ meltfptr[1]), "OBROUT_PROC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_BODY",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (3),
			  ( /*_.OINIBODY__V25*/ meltfptr[24]), "OBROUT_BODY");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_NBVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (4),
			  ( /*_.MAKE_INTEGERBOX__V26*/ meltfptr[25]),
			  "OBROUT_NBVAL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_NBLONG",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (5),
			  ( /*_.MAKE_INTEGERBOX__V27*/ meltfptr[26]),
			  "OBROUT_NBLONG");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_OTHERS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (6),
			  ( /*_.MAKE_LIST__V28*/ meltfptr[27]),
			  "OBROUT_OTHERS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_CNTCITER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (8),
			  ( /*_.MAKE_INTEGERBOX__V29*/ meltfptr[28]),
			  "OBROUT_CNTCITER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_CNTLETREC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (9),
			  ( /*_.MAKE_INTEGERBOX__V30*/ meltfptr[29]),
			  "OBROUT_CNTLETREC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIROUT_FILL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (12),
			  ( /*_.MAKE_LIST__V31*/ meltfptr[30]),
			  "OIROUT_FILL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIROUT_PROLOG",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (11),
			  ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  "OIROUT_PROLOG");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIROUT_MODULENAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V34*/ meltfptr[33])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V34*/ meltfptr[33]), (13),
			  ( /*_.MOCX_MODULENAME__V32*/ meltfptr[31]),
			  "OIROUT_MODULENAME");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V34*/ meltfptr[33],
				  "newly made instance");
    ;
    /*_.OINITROUT__V33*/ meltfptr[32] = /*_.INST__V34*/ meltfptr[33];;
    /*^compute */
 /*_.IMPORTMAP__V35*/ meltfptr[34] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[3])),
	(50)));;
    MELT_LOCATION ("warmelt-genobj.melt:1232:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_LIST__V36*/ meltfptr[35] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    /*^compute */
 /*_.MAKE_LIST__V37*/ meltfptr[36] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    /*^compute */
 /*_.MAKE_MAPOBJECT__V38*/ meltfptr[37] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[3])),
	(20)));;
    /*^compute */
 /*_.MAKE_MAPOBJECT__V39*/ meltfptr[38] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[3])),
	(60)));;
    MELT_LOCATION ("warmelt-genobj.melt:1245:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V41*/ meltfptr[40] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_11 */ meltfrout->
						tabval[11])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V41*/ meltfptr[40])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V41*/ meltfptr[40])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V41*/ meltfptr[40])->tabval[0] =
      (melt_ptr_t) ( /*_.COMPICACHE__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V40*/ meltfptr[39] = /*_.LAMBDA___V41*/ meltfptr[40];;
    MELT_LOCATION ("warmelt-genobj.melt:1243:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V40*/ meltfptr[39];
      /*_.LIST_MAP__V42*/ meltfptr[41] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_MAP */ meltfrout->tabval[9])),
		    (melt_ptr_t) ( /*_.PROCURMODENVLIST__V6*/ meltfptr[5]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1232:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_INITIAL_GENERATION_CONTEXT */ meltfrout->tabval[8])), (14), "CLASS_INITIAL_GENERATION_CONTEXT");
  /*_.INST__V44*/ meltfptr[43] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_OBJROUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (0),
			  ( /*_.OINITROUT__V33*/ meltfptr[32]),
			  "GNCX_OBJROUT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_LOCMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (1),
			  ( /*_.LOCMAP__V23*/ meltfptr[22]), "GNCX_LOCMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_FREEPTRLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (2),
			  ( /*_.MAKE_LIST__V36*/ meltfptr[35]),
			  "GNCX_FREEPTRLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_FREELONGLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (3),
			  ( /*_.MAKE_LIST__V37*/ meltfptr[36]),
			  "GNCX_FREELONGLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_FREEOTHERMAPS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (4),
			  ( /*_.MAKE_MAPOBJECT__V38*/ meltfptr[37]),
			  "GNCX_FREEOTHERMAPS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_COMPICACHE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (6),
			  ( /*_.COMPICACHE__V5*/ meltfptr[4]),
			  "GNCX_COMPICACHE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_MODULCONTEXT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (7),
			  ( /*_.MODCTX__V3*/ meltfptr[2]),
			  "GNCX_MODULCONTEXT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_MATCHMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (8),
			  ( /*_.MAKE_MAPOBJECT__V39*/ meltfptr[38]),
			  "GNCX_MATCHMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @IGNCX_PROCURMODENVLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (12),
			  ( /*_.LIST_MAP__V42*/ meltfptr[41]),
			  "IGNCX_PROCURMODENVLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @IGNCX_IMPORTMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (13),
			  ( /*_.IMPORTMAP__V35*/ meltfptr[34]),
			  "IGNCX_IMPORTMAP");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V44*/ meltfptr[43],
				  "newly made instance");
    ;
    /*_.GCX__V43*/ meltfptr[42] = /*_.INST__V44*/ meltfptr[43];;
    MELT_LOCATION ("warmelt-genobj.melt:1255:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_13_RETINIT_ */ meltfrout->tabval[13]);
      /*_.RETI__V46*/ meltfptr[45] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1256:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_RETLOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.GCX__V43*/ meltfptr[42])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.GCX__V43*/ meltfptr[42]), (5),
			  ( /*_.RETI__V46*/ meltfptr[45]), "GNCX_RETLOC");
    ;
    /*^touch */
    meltgc_touch ( /*_.GCX__V43*/ meltfptr[42]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.GCX__V43*/ meltfptr[42], "put-fields");
    ;

    MELT_LOCATION ("warmelt-genobj.melt:1257:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_RETVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.OINITROUT__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.OINITROUT__V33*/ meltfptr[32]), (7),
			  ( /*_.RETI__V46*/ meltfptr[45]), "OBROUT_RETVAL");
    ;
    /*^touch */
    meltgc_touch ( /*_.OINITROUT__V33*/ meltfptr[32]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.OINITROUT__V33*/ meltfptr[32],
				  "put-fields");
    ;

    /*_.LET___V45*/ meltfptr[44] = /*_.RETI__V46*/ meltfptr[45];;

    MELT_LOCATION ("warmelt-genobj.melt:1255:/ clear");
	   /*clear *//*_.RETI__V46*/ meltfptr[45] = 0;
    /*_.RETINIT__V47*/ meltfptr[45] = /*_.LET___V45*/ meltfptr[44];;
    MELT_LOCATION ("warmelt-genobj.melt:1260:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_14_CONTENV_ */ meltfrout->tabval[14]);
      /*_.BOXL__V49*/ meltfptr[48] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1261:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GCX__V43*/ meltfptr[42]),
					(melt_ptr_t) (( /*!CLASS_INITIAL_GENERATION_CONTEXT */ meltfrout->tabval[8])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @IGNCX_CONTENVLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.GCX__V43*/ meltfptr[42])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.GCX__V43*/ meltfptr[42]), (11),
				( /*_.BOXL__V49*/ meltfptr[48]),
				"IGNCX_CONTENVLOC");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.GCX__V43*/ meltfptr[42]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.GCX__V43*/ meltfptr[42],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V48*/ meltfptr[47] = /*_.BOXL__V49*/ meltfptr[48];;

    MELT_LOCATION ("warmelt-genobj.melt:1260:/ clear");
	   /*clear *//*_.BOXL__V49*/ meltfptr[48] = 0;
    /*_.BOXLOC__V50*/ meltfptr[48] = /*_.LET___V48*/ meltfptr[47];;
    MELT_LOCATION ("warmelt-genobj.melt:1263:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_15_FRESHENV_ */ meltfrout->tabval[15]);
      /*_.OFRESHENV__V51*/ meltfptr[50] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1264:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_16_PREVENV_ */ meltfrout->tabval[16]);
      /*_.PREVE__V53*/ meltfptr[52] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1266:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GCX__V43*/ meltfptr[42]),
					(melt_ptr_t) (( /*!CLASS_INITIAL_GENERATION_CONTEXT */ meltfrout->tabval[8])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @IGNCX_PREVENVLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.GCX__V43*/ meltfptr[42])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.GCX__V43*/ meltfptr[42]), (10),
				( /*_.PREVE__V53*/ meltfptr[52]),
				"IGNCX_PREVENVLOC");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.GCX__V43*/ meltfptr[42]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.GCX__V43*/ meltfptr[42],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V52*/ meltfptr[51] = /*_.PREVE__V53*/ meltfptr[52];;

    MELT_LOCATION ("warmelt-genobj.melt:1264:/ clear");
	   /*clear *//*_.PREVE__V53*/ meltfptr[52] = 0;
    /*_.OPREVENV__V54*/ meltfptr[52] = /*_.LET___V52*/ meltfptr[51];;
    MELT_LOCATION ("warmelt-genobj.melt:1268:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_17_VALEXPORT_ */ meltfrout->tabval[17]);
      /*_.OVALUEEXPORTER__V55*/ meltfptr[54] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1269:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_18_VALIMPORT_ */ meltfrout->tabval[18]);
      /*_.OVALUEIMPORTER__V56*/ meltfptr[55] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1270:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.IPRO__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_INITPROC */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.IPRO__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "NINIT_DEFBINDS");
   /*_.INIDEFBINDS__V57*/ meltfptr[56] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.INIDEFBINDS__V57*/ meltfptr[56] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1272:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "start of oinibody";
      /*_.APPEND_COMMENTCONST__V58*/ meltfptr[57] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1273:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1273:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1273:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[8];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1273;
	      /*^apply.arg */
	      argtab[3].meltbp_aptr =
		(melt_ptr_t *) & /*_.BOXLOC__V50*/ meltfptr[48];
	      /*^apply.arg */
	      argtab[4].meltbp_cstring = "compile2obj_initproc boxloc=";
	      /*^apply.arg */
	      argtab[5].meltbp_aptr =
		(melt_ptr_t *) & /*_.BOXLOC__V50*/ meltfptr[48];
	      /*^apply.arg */
	      argtab[6].meltbp_cstring = " inidefbinds=";
	      /*^apply.arg */
	      argtab[7].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIDEFBINDS__V57*/ meltfptr[56];
	      /*_.MELT_DEBUG_FUN__V61*/ meltfptr[60] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_PTR
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V60*/ meltfptr[59] =
	      /*_.MELT_DEBUG_FUN__V61*/ meltfptr[60];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1273:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V61*/ meltfptr[60] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V60*/ meltfptr[59] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1273:/ quasiblock");


      /*_.PROGN___V62*/ meltfptr[60] = /*_.IF___V60*/ meltfptr[59];;
      /*^compute */
      /*_.IFCPP___V59*/ meltfptr[58] = /*_.PROGN___V62*/ meltfptr[60];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1273:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V60*/ meltfptr[59] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V62*/ meltfptr[60] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V59*/ meltfptr[58] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.CURPAIR__V63*/ meltfptr[59] =
	   melt_list_first ((melt_ptr_t) /*_.INIDEFBINDS__V57*/ meltfptr[56]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V63*/ meltfptr[59]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V63*/ meltfptr[59] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V63*/ meltfptr[59]))
	{
	  /*_.CURDEFBIND__V64*/ meltfptr[60] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V63*/ meltfptr[59]);



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1278:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L10*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1278:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[0] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  /*^compute */
      /*_.DISCRIM__V67*/ meltfptr[66] =
		    ((melt_ptr_t)
		     (melt_discr
		      ((melt_ptr_t) ( /*_.CURDEFBIND__V64*/ meltfptr[60]))));;
		  MELT_LOCATION ("warmelt-genobj.melt:1278:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1278;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_initproc curdefbind=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURDEFBIND__V64*/ meltfptr[60];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " of discrim=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DISCRIM__V67*/ meltfptr[66];
		    /*_.MELT_DEBUG_FUN__V68*/ meltfptr[67] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V66*/ meltfptr[65] =
		    /*_.MELT_DEBUG_FUN__V68*/ meltfptr[67];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1278:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[0] = 0;
		  /*^clear */
		/*clear *//*_.DISCRIM__V67*/ meltfptr[66] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V68*/ meltfptr[67] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V66*/ meltfptr[65] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1278:/ quasiblock");


	    /*_.PROGN___V69*/ meltfptr[66] = /*_.IF___V66*/ meltfptr[65];;
	    /*^compute */
	    /*_.IFCPP___V65*/ meltfptr[64] = /*_.PROGN___V69*/ meltfptr[66];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1278:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[1] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V66*/ meltfptr[65] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V69*/ meltfptr[66] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V65*/ meltfptr[64] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1280:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURDEFBIND__V64*/
					       meltfptr[60]),
					      (melt_ptr_t) (( /*!CLASS_ANY_BINDING */ meltfrout->tabval[20])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURDEFBIND__V64*/ meltfptr[60]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 0, "BINDER");
    /*_.INIDEFSYMB__V70*/ meltfptr[67] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.INIDEFSYMB__V70*/ meltfptr[67] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1281:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURDEFBIND__V64*/
					       meltfptr[60]),
					      (melt_ptr_t) (( /*!CLASS_FIXED_BINDING */ meltfrout->tabval[21])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURDEFBIND__V64*/ meltfptr[60]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 1, "FIXBIND_DATA");
    /*_.OINISYM__V71*/ meltfptr[65] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.OINISYM__V71*/ meltfptr[65] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1282:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.INIDEFSYMB__V70*/ meltfptr[67];
	    /*_.OINIDEFLOC__V72*/ meltfptr[66] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[12])),
			  (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1284:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L12*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1284:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[1] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  /*^compute */
      /*_.DISCRIM__V75*/ meltfptr[74] =
		    ((melt_ptr_t)
		     (melt_discr
		      ((melt_ptr_t) ( /*_.OINISYM__V71*/ meltfptr[65]))));;
		  MELT_LOCATION ("warmelt-genobj.melt:1284:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[11];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[1];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1284;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_initproc oinidefloc=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OINIDEFLOC__V72*/ meltfptr[66];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " oinisym=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OINISYM__V71*/ meltfptr[65];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = " of discrim:";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DISCRIM__V75*/ meltfptr[74];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = " curdefbind=";
		    /*^apply.arg */
		    argtab[10].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURDEFBIND__V64*/ meltfptr[60];
		    /*_.MELT_DEBUG_FUN__V76*/ meltfptr[75] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V74*/ meltfptr[73] =
		    /*_.MELT_DEBUG_FUN__V76*/ meltfptr[75];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1284:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[1] = 0;
		  /*^clear */
		/*clear *//*_.DISCRIM__V75*/ meltfptr[74] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V76*/ meltfptr[75] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V74*/ meltfptr[73] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1284:/ quasiblock");


	    /*_.PROGN___V77*/ meltfptr[74] = /*_.IF___V74*/ meltfptr[73];;
	    /*^compute */
	    /*_.IFCPP___V73*/ meltfptr[72] = /*_.PROGN___V77*/ meltfptr[74];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1284:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[0] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V74*/ meltfptr[73] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V77*/ meltfptr[74] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V73*/ meltfptr[72] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1289:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L14*/ meltfnum[1] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.OINISYM__V71*/ meltfptr[65]),
				   (melt_ptr_t) (( /*!CLASS_NREP_SIMPLE */
						  meltfrout->tabval[22])));;
	    MELT_LOCATION ("warmelt-genobj.melt:1289:/ cond");
	    /*cond */ if ( /*_#IS_A__L14*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V79*/ meltfptr[73] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-genobj.melt:1289:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check oinisym"),
					("warmelt-genobj.melt")
					? ("warmelt-genobj.melt") : __FILE__,
					(1289) ? (1289) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V79*/ meltfptr[73] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V78*/ meltfptr[75] = /*_.IFELSE___V79*/ meltfptr[73];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1289:/ clear");
	      /*clear *//*_#IS_A__L14*/ meltfnum[1] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V79*/ meltfptr[73] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V78*/ meltfptr[75] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1290:/ locexp");
	    meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				   ( /*_.LOCMAP__V23*/ meltfptr[22]),
				   (meltobject_ptr_t) ( /*_.CURDEFBIND__V64*/
						       meltfptr[60]),
				   (melt_ptr_t) ( /*_.OINIDEFLOC__V72*/
						 meltfptr[66]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:1280:/ clear");
	    /*clear *//*_.INIDEFSYMB__V70*/ meltfptr[67] = 0;
	  /*^clear */
	    /*clear *//*_.OINISYM__V71*/ meltfptr[65] = 0;
	  /*^clear */
	    /*clear *//*_.OINIDEFLOC__V72*/ meltfptr[66] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V73*/ meltfptr[72] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V78*/ meltfptr[75] = 0;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V63*/ meltfptr[59] = NULL;
     /*_.CURDEFBIND__V64*/ meltfptr[60] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-genobj.melt:1275:/ clear");
	    /*clear *//*_.CURPAIR__V63*/ meltfptr[59] = 0;
      /*^clear */
	    /*clear *//*_.CURDEFBIND__V64*/ meltfptr[60] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V65*/ meltfptr[64] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1293:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:1296:/ quasiblock");


 /*_.BODFEL__V82*/ meltfptr[67] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    MELT_LOCATION ("warmelt-genobj.melt:1298:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "start computing boxloc";
      /*_.APPEND_COMMENTCONST__V83*/ meltfptr[65] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.BODFEL__V82*/ meltfptr[67]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1301:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1302:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct meltpair_st rpair_0__OFRESHENV_x1;
	struct meltlist_st rlist_1__LIST_;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inipair rpair_0__OFRESHENV_x1 */
   /*_.OFRESHENV__V85*/ meltfptr[72] =
	(melt_ptr_t) & meltletrec_1_ptr->rpair_0__OFRESHENV_x1;
      meltletrec_1_ptr->rpair_0__OFRESHENV_x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

      /*inilist rlist_1__LIST_ */
   /*_.LIST___V86*/ meltfptr[75] =
	(melt_ptr_t) & meltletrec_1_ptr->rlist_1__LIST_;
      meltletrec_1_ptr->rlist_1__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*^putpairhead */
      /*putpairhead */
      melt_assertmsg ("putpairhead /1 checkpair",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.OFRESHENV__V85*/ meltfptr[72]))
		      == MELTOBMAG_PAIR);
      ((meltpair_ptr_t) ( /*_.OFRESHENV__V85*/ meltfptr[72]))->hd =
	(melt_ptr_t) ( /*_.OFRESHENV__V51*/ meltfptr[50]);
      ;
      /*^touch */
      meltgc_touch ( /*_.OFRESHENV__V85*/ meltfptr[72]);
      ;
      /*^putlist */
      /*putlist */
      melt_assertmsg ("putlist checklist",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.LIST___V86*/ meltfptr[75])) ==
		      MELTOBMAG_LIST);
      ((meltlist_ptr_t) ( /*_.LIST___V86*/ meltfptr[75]))->first =
	(meltpair_ptr_t) ( /*_.OFRESHENV__V85*/ meltfptr[72]);
      ((meltlist_ptr_t) ( /*_.LIST___V86*/ meltfptr[75]))->last =
	(meltpair_ptr_t) ( /*_.OFRESHENV__V85*/ meltfptr[72]);
      ;
      /*^touch */
      meltgc_touch ( /*_.LIST___V86*/ meltfptr[75]);
      ;
      /*_.LIST___V84*/ meltfptr[66] = /*_.LIST___V86*/ meltfptr[75];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1302:/ clear");
	    /*clear *//*_.OFRESHENV__V85*/ meltfptr[72] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V86*/ meltfptr[75] = 0;
      /*^clear */
	    /*clear *//*_.OFRESHENV__V85*/ meltfptr[72] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V86*/ meltfptr[75] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1301:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJGETSLOT */
					     meltfrout->tabval[23])), (4),
			      "CLASS_OBJGETSLOT");
  /*_.INST__V88*/ meltfptr[75] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V88*/ meltfptr[75])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V88*/ meltfptr[75]), (1),
			  ( /*_.LIST___V84*/ meltfptr[66]), "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OGETSL_OBJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V88*/ meltfptr[75])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V88*/ meltfptr[75]), (2),
			  (( /*!INITIALSYSTEMDATA_OBJPREDEF */ meltfrout->
			    tabval[24])), "OGETSL_OBJ");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OGETSL_FIELD",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V88*/ meltfptr[75])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V88*/ meltfptr[75]), (3),
			  (( /*!SYSDATA_CONT_FRESH_ENV */ meltfrout->
			    tabval[25])), "OGETSL_FIELD");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V88*/ meltfptr[75],
				  "newly made instance");
    ;
    /*_.INST___V87*/ meltfptr[72] = /*_.INST__V88*/ meltfptr[75];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1300:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.BODFEL__V82*/ meltfptr[67]),
			  (melt_ptr_t) ( /*_.INST___V87*/ meltfptr[72]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1308:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[8];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!CTYPE_VOID */ meltfrout->tabval[27]);
      /*^apply.arg */
      argtab[1].meltbp_cstring = "/*checkfreshenv*/ if ((";
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OPREVENV__V54*/ meltfptr[52];
      /*^apply.arg */
      argtab[3].meltbp_cstring =
	")\n\t\t\t\t\t&& melt_magic_discr((melt_ptr_t)(";
      /*^apply.arg */
      argtab[4].meltbp_aptr =
	(melt_ptr_t *) & /*_.OFRESHENV__V51*/ meltfptr[50];
      /*^apply.arg */
      argtab[5].meltbp_cstring =
	")) != MELTOBMAG_CLOSURE)\n  \t           warning(0, \"MELT corruption\
: bad FRESH_ENV @%p in system data <%s:%d>\",\
\n\t\t\t      (void*) (";
      /*^apply.arg */
      argtab[6].meltbp_aptr =
	(melt_ptr_t *) & /*_.OFRESHENV__V51*/ meltfptr[50];
      /*^apply.arg */
      argtab[7].meltbp_cstring = "), __FILE__, __LINE__);";
      /*_.MAKE_OBJCOMPUTE__V89*/ meltfptr[88] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MAKE_OBJCOMPUTE */ meltfrout->tabval[26])),
		    (melt_ptr_t) (( /*nil */ NULL)),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1306:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.BODFEL__V82*/ meltfptr[67]),
			  (melt_ptr_t) ( /*_.MAKE_OBJCOMPUTE__V89*/
					meltfptr[88]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1320:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1321:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_2_st
      {
	struct meltpair_st rpair_0__BOXLOC_x1;
	struct meltlist_st rlist_1__LIST_;
	long meltletrec_2_endgap;
      } *meltletrec_2_ptr = 0;
      meltletrec_2_ptr =
	(struct meltletrec_2_st *)
	meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
      /*^blockmultialloc.initfill */
      /*inipair rpair_0__BOXLOC_x1 */
   /*_.BOXLOC__V91*/ meltfptr[90] =
	(melt_ptr_t) & meltletrec_2_ptr->rpair_0__BOXLOC_x1;
      meltletrec_2_ptr->rpair_0__BOXLOC_x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

      /*inilist rlist_1__LIST_ */
   /*_.LIST___V92*/ meltfptr[91] =
	(melt_ptr_t) & meltletrec_2_ptr->rlist_1__LIST_;
      meltletrec_2_ptr->rlist_1__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*^putpairhead */
      /*putpairhead */
      melt_assertmsg ("putpairhead /2 checkpair",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.BOXLOC__V91*/ meltfptr[90])) ==
		      MELTOBMAG_PAIR);
      ((meltpair_ptr_t) ( /*_.BOXLOC__V91*/ meltfptr[90]))->hd =
	(melt_ptr_t) ( /*_.BOXLOC__V50*/ meltfptr[48]);
      ;
      /*^touch */
      meltgc_touch ( /*_.BOXLOC__V91*/ meltfptr[90]);
      ;
      /*^putlist */
      /*putlist */
      melt_assertmsg ("putlist checklist",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.LIST___V92*/ meltfptr[91])) ==
		      MELTOBMAG_LIST);
      ((meltlist_ptr_t) ( /*_.LIST___V92*/ meltfptr[91]))->first =
	(meltpair_ptr_t) ( /*_.BOXLOC__V91*/ meltfptr[90]);
      ((meltlist_ptr_t) ( /*_.LIST___V92*/ meltfptr[91]))->last =
	(meltpair_ptr_t) ( /*_.BOXLOC__V91*/ meltfptr[90]);
      ;
      /*^touch */
      meltgc_touch ( /*_.LIST___V92*/ meltfptr[91]);
      ;
      /*_.LIST___V90*/ meltfptr[89] = /*_.LIST___V92*/ meltfptr[91];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1321:/ clear");
	    /*clear *//*_.BOXLOC__V91*/ meltfptr[90] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V92*/ meltfptr[91] = 0;
      /*^clear */
	    /*clear *//*_.BOXLOC__V91*/ meltfptr[90] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V92*/ meltfptr[91] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1323:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_3_st
      {
	struct MELT_MULTIPLE_STRUCT (1) rtup_0__TUPLREC__x1;
	long meltletrec_3_endgap;
      } *meltletrec_3_ptr = 0;
      meltletrec_3_ptr =
	(struct meltletrec_3_st *)
	meltgc_allocate (sizeof (struct meltletrec_3_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x1 */
 /*_.TUPLREC___V94*/ meltfptr[91] =
	(melt_ptr_t) & meltletrec_3_ptr->rtup_0__TUPLREC__x1;
      meltletrec_3_ptr->rtup_0__TUPLREC__x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_3_ptr->rtup_0__TUPLREC__x1.nbval = 1;


      /*^putuple */
      /*putupl#1 */
      melt_assertmsg ("putupl [:1323] #1 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V94*/ meltfptr[91]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1323] #1 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V94*/
					      meltfptr[91]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V94*/ meltfptr[91]))->tabval[0] =
	(melt_ptr_t) ( /*_.OPREVENV__V54*/ meltfptr[52]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V94*/ meltfptr[91]);
      ;
      /*_.TUPLE___V93*/ meltfptr[90] = /*_.TUPLREC___V94*/ meltfptr[91];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1323:/ clear");
	    /*clear *//*_.TUPLREC___V94*/ meltfptr[91] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V94*/ meltfptr[91] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1320:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJAPPLY */ meltfrout->
					     tabval[28])), (4),
			      "CLASS_OBJAPPLY");
  /*_.INST__V96*/ meltfptr[95] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V96*/ meltfptr[95])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V96*/ meltfptr[95]), (1),
			  ( /*_.LIST___V90*/ meltfptr[89]), "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBAPP_CLOS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V96*/ meltfptr[95])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V96*/ meltfptr[95]), (2),
			  ( /*_.OFRESHENV__V51*/ meltfptr[50]), "OBAPP_CLOS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBAPP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V96*/ meltfptr[95])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V96*/ meltfptr[95]), (3),
			  ( /*_.TUPLE___V93*/ meltfptr[90]), "OBAPP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V96*/ meltfptr[95],
				  "newly made instance");
    ;
    /*_.INST___V95*/ meltfptr[91] = /*_.INST__V96*/ meltfptr[95];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1318:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.BODFEL__V82*/ meltfptr[67]),
			  (melt_ptr_t) ( /*_.INST___V95*/ meltfptr[91]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1325:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_STRINGCONST__V97*/ meltfptr[96] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[30])),
	("compute fresh module environment")));;
    MELT_LOCATION ("warmelt-genobj.melt:1325:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMMENTEDBLOCK */
					     meltfrout->tabval[29])), (4),
			      "CLASS_OBJCOMMENTEDBLOCK");
  /*_.INST__V99*/ meltfptr[98] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_BODYL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V99*/ meltfptr[98])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V99*/ meltfptr[98]), (1),
			  ( /*_.BODFEL__V82*/ meltfptr[67]), "OBLO_BODYL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_EPIL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V99*/ meltfptr[98])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V99*/ meltfptr[98]), (2),
			  (( /*nil */ NULL)), "OBLO_EPIL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OCOMBLO_COMMENT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V99*/ meltfptr[98])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V99*/ meltfptr[98]), (3),
			  ( /*_.MAKE_STRINGCONST__V97*/ meltfptr[96]),
			  "OCOMBLO_COMMENT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V99*/ meltfptr[98],
				  "newly made instance");
    ;
    /*_.INST___V98*/ meltfptr[97] = /*_.INST__V99*/ meltfptr[98];;
    /*^compute */
    /*_.LET___V81*/ meltfptr[73] = /*_.INST___V98*/ meltfptr[97];;

    MELT_LOCATION ("warmelt-genobj.melt:1296:/ clear");
	   /*clear *//*_.BODFEL__V82*/ meltfptr[67] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V83*/ meltfptr[65] = 0;
    /*^clear */
	   /*clear *//*_.LIST___V84*/ meltfptr[66] = 0;
    /*^clear */
	   /*clear *//*_.INST___V87*/ meltfptr[72] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_OBJCOMPUTE__V89*/ meltfptr[88] = 0;
    /*^clear */
	   /*clear *//*_.LIST___V90*/ meltfptr[89] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V93*/ meltfptr[90] = 0;
    /*^clear */
	   /*clear *//*_.INST___V95*/ meltfptr[91] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_STRINGCONST__V97*/ meltfptr[96] = 0;
    /*^clear */
	   /*clear *//*_.INST___V98*/ meltfptr[97] = 0;
    /*_.OCOMPUTBOXLOC__V100*/ meltfptr[67] = /*_.LET___V81*/ meltfptr[73];;
    MELT_LOCATION ("warmelt-genobj.melt:1332:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1334:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_STRINGCONST__V101*/ meltfptr[65] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[33])),
	(" || melt_object_length((melt_ptr_t) MELT_PREDEF (INITIAL_SYSTEM_DATA\
))<MELTFIELD_SYSDATA_CONT_FRESH_ENV")));;
    MELT_LOCATION ("warmelt-genobj.melt:1336:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_4_st
      {
	struct MELT_MULTIPLE_STRUCT (2) rtup_0__TUPLREC__x2;
	long meltletrec_4_endgap;
      } *meltletrec_4_ptr = 0;
      meltletrec_4_ptr =
	(struct meltletrec_4_st *)
	meltgc_allocate (sizeof (struct meltletrec_4_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x2 */
 /*_.TUPLREC___V103*/ meltfptr[72] =
	(melt_ptr_t) & meltletrec_4_ptr->rtup_0__TUPLREC__x2;
      meltletrec_4_ptr->rtup_0__TUPLREC__x2.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_4_ptr->rtup_0__TUPLREC__x2.nbval = 2;


      /*^putuple */
      /*putupl#2 */
      melt_assertmsg ("putupl [:1336] #2 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V103*/ meltfptr[72]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1336] #2 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V103*/
					      meltfptr[72]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V103*/ meltfptr[72]))->tabval[0] =
	(melt_ptr_t) ( /*_.BOXLOC__V50*/ meltfptr[48]);
      ;
      /*^putuple */
      /*putupl#3 */
      melt_assertmsg ("putupl [:1336] #3 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V103*/ meltfptr[72]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1336] #3 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V103*/
					      meltfptr[72]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V103*/ meltfptr[72]))->tabval[1] =
	(melt_ptr_t) ( /*_.MAKE_STRINGCONST__V101*/ meltfptr[65]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V103*/ meltfptr[72]);
      ;
      /*_.TUPLE___V102*/ meltfptr[66] = /*_.TUPLREC___V103*/ meltfptr[72];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1336:/ clear");
	    /*clear *//*_.TUPLREC___V103*/ meltfptr[72] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V103*/ meltfptr[72] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1334:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJEXPV */ meltfrout->
					     tabval[32])), (2),
			      "CLASS_OBJEXPV");
  /*_.INST__V105*/ meltfptr[89] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBX_CONT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V105*/ meltfptr[89])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V105*/ meltfptr[89]), (1),
			  ( /*_.TUPLE___V102*/ meltfptr[66]), "OBX_CONT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V105*/ meltfptr[89],
				  "newly made instance");
    ;
    /*_.INST___V104*/ meltfptr[88] = /*_.INST__V105*/ meltfptr[89];;
    MELT_LOCATION ("warmelt-genobj.melt:1332:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOND */ meltfrout->
					     tabval[31])), (4),
			      "CLASS_OBJCOND");
  /*_.INST__V107*/ meltfptr[91] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_TEST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V107*/ meltfptr[91])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V107*/ meltfptr[91]), (1),
			  ( /*_.INST___V104*/ meltfptr[88]), "OBCOND_TEST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V107*/ meltfptr[91])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V107*/ meltfptr[91]), (2),
			  (( /*nil */ NULL)), "OBCOND_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCOND_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V107*/ meltfptr[91])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V107*/ meltfptr[91]), (3),
			  ( /*_.OCOMPUTBOXLOC__V100*/ meltfptr[67]),
			  "OBCOND_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V107*/ meltfptr[91],
				  "newly made instance");
    ;
    /*_.OTESTCOMPUTBOXLOC__V106*/ meltfptr[90] =
      /*_.INST__V107*/ meltfptr[91];;
    MELT_LOCATION ("warmelt-genobj.melt:1348:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V109*/ meltfptr[97] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_37 */ meltfrout->
						tabval[37])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V109*/ meltfptr[97])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V109*/ meltfptr[97])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V109*/ meltfptr[97])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V109*/ meltfptr[97])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V109*/ meltfptr[97])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V109*/ meltfptr[97])->tabval[1] =
      (melt_ptr_t) ( /*_.IMPORTMAP__V35*/ meltfptr[34]);
    ;
    /*_.LAMBDA___V108*/ meltfptr[96] = /*_.LAMBDA___V109*/ meltfptr[97];;
    MELT_LOCATION ("warmelt-genobj.melt:1346:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V108*/ meltfptr[96];
      /*_.LIMPLOCV__V110*/ meltfptr[72] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_MAP */ meltfrout->tabval[9])),
		    (melt_ptr_t) ( /*_.IMPORTVALUES__V7*/ meltfptr[6]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1359:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L15*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1359:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1359:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L16*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1359;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initproc otestcomputboxloc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OTESTCOMPUTBOXLOC__V106*/ meltfptr[90];
	      /*_.MELT_DEBUG_FUN__V113*/ meltfptr[112] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V112*/ meltfptr[111] =
	      /*_.MELT_DEBUG_FUN__V113*/ meltfptr[112];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1359:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V113*/ meltfptr[112] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V112*/ meltfptr[111] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1359:/ quasiblock");


      /*_.PROGN___V114*/ meltfptr[112] = /*_.IF___V112*/ meltfptr[111];;
      /*^compute */
      /*_.IFCPP___V111*/ meltfptr[110] = /*_.PROGN___V114*/ meltfptr[112];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1359:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V112*/ meltfptr[111] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V114*/ meltfptr[112] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V111*/ meltfptr[110] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1362:/ quasiblock");


 /*_.LISDEST__V115*/ meltfptr[111] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1363:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.LISDEST__V115*/ meltfptr[111]),
			  (melt_ptr_t) ( /*_.OPREVENV__V54*/ meltfptr[52]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1364:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "get previous environment";
      /*_.APPEND_COMMENTCONST__V116*/ meltfptr[112] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1366:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_STRINGCONST__V117*/ meltfptr[116] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[33])),
	("/*getprevenv*/ (melt_ptr_t) modargp_")));;
    MELT_LOCATION ("warmelt-genobj.melt:1366:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMPUTE */
					     meltfrout->tabval[38])), (4),
			      "CLASS_OBJCOMPUTE");
  /*_.INST__V119*/ meltfptr[118] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V119*/ meltfptr[118])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V119*/ meltfptr[118]), (1),
			  ( /*_.LISDEST__V115*/ meltfptr[111]),
			  "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V119*/ meltfptr[118])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V119*/ meltfptr[118]), (3),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[39])),
			  "OBCPT_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V119*/ meltfptr[118])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V119*/ meltfptr[118]), (2),
			  ( /*_.MAKE_STRINGCONST__V117*/ meltfptr[116]),
			  "OBCPT_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V119*/ meltfptr[118],
				  "newly made instance");
    ;
    /*_.INST___V118*/ meltfptr[117] = /*_.INST__V119*/ meltfptr[118];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1365:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (melt_ptr_t) ( /*_.INST___V118*/ meltfptr[117]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:1362:/ clear");
	   /*clear *//*_.LISDEST__V115*/ meltfptr[111] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V116*/ meltfptr[112] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_STRINGCONST__V117*/ meltfptr[116] = 0;
    /*^clear */
	   /*clear *//*_.INST___V118*/ meltfptr[117] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1372:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "compute boxloc";
      /*_.APPEND_COMMENTCONST__V120*/ meltfptr[111] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1374:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (melt_ptr_t) ( /*_.OTESTCOMPUTBOXLOC__V106*/
					meltfptr[90]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1376:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:1380:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V123*/ meltfptr[117] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_44 */ meltfrout->
						tabval[44])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V123*/ meltfptr[117])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V123*/ meltfptr[117])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V123*/ meltfptr[117])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]);
    ;
    /*_.LAMBDA___V122*/ meltfptr[116] = /*_.LAMBDA___V123*/ meltfptr[117];;
    MELT_LOCATION ("warmelt-genobj.melt:1378:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[41]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V122*/ meltfptr[116];
      /*_.ODATATUP__V124*/ meltfptr[123] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[40])),
		    (melt_ptr_t) ( /*_.IDATA__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1384:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.IPRO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NINIT_TOPL");
  /*_.TOPLIS__V125*/ meltfptr[124] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1386:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIROUT_DATA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.OINITROUT__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.OINITROUT__V33*/ meltfptr[32]), (10),
			  ( /*_.ODATATUP__V124*/ meltfptr[123]),
			  "OIROUT_DATA");
    ;
    /*^touch */
    meltgc_touch ( /*_.OINITROUT__V33*/ meltfptr[32]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.OINITROUT__V33*/ meltfptr[32],
				  "put-fields");
    ;


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1388:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST_OR_NULL__L17*/ meltfnum[1] =
	(( /*_.TOPLIS__V125*/ meltfptr[124]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.TOPLIS__V125*/ meltfptr[124])) ==
	  MELTOBMAG_LIST));;
      MELT_LOCATION ("warmelt-genobj.melt:1388:/ cond");
      /*cond */ if ( /*_#IS_LIST_OR_NULL__L17*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V127*/ meltfptr[126] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1388:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check toplis"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1388) ? (1388) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V127*/ meltfptr[126] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V126*/ meltfptr[125] = /*_.IFELSE___V127*/ meltfptr[126];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1388:/ clear");
	     /*clear *//*_#IS_LIST_OR_NULL__L17*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V127*/ meltfptr[126] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V126*/ meltfptr[125] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1390:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:1393:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V130*/ meltfptr[129] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_47 */ meltfrout->
						tabval[47])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V130*/ meltfptr[129])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V130*/ meltfptr[129])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V130*/ meltfptr[129])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]);
    ;
    /*_.LAMBDA___V129*/ meltfptr[128] = /*_.LAMBDA___V130*/ meltfptr[129];;
    MELT_LOCATION ("warmelt-genobj.melt:1391:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V129*/ meltfptr[128];
      /*_.OBJTOPLIS__V131*/ meltfptr[130] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_MAP */ meltfrout->tabval[9])),
		    (melt_ptr_t) ( /*_.TOPLIS__V125*/ meltfptr[124]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1399:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "get symbols & keywords";
      /*_.APPEND_COMMENTCONST__V132*/ meltfptr[131] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1402:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V134*/ meltfptr[133] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_55 */ meltfrout->
						tabval[55])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V134*/ meltfptr[133])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V134*/ meltfptr[133])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V134*/ meltfptr[133])->tabval[0] =
      (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]);
    ;
    /*_.LAMBDA___V133*/ meltfptr[132] = /*_.LAMBDA___V134*/ meltfptr[133];;
    MELT_LOCATION ("warmelt-genobj.melt:1400:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V133*/ meltfptr[132];
      /*_.MULTIPLE_EVERY__V135*/ meltfptr[134] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[48])),
		    (melt_ptr_t) ( /*_.ODATATUP__V124*/ meltfptr[123]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1425:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MAPOBJECT_COUNT__L18*/ meltfnum[0] =
      (melt_count_mapobjects
       ((meltmapobjects_ptr_t) ( /*_.IMPORTMAP__V35*/ meltfptr[34])));;
    /*^compute */
 /*_#I__L19*/ meltfnum[1] =
      (( /*_#MAPOBJECT_COUNT__L18*/ meltfnum[0]) > (0));;
    MELT_LOCATION ("warmelt-genobj.melt:1425:/ cond");
    /*cond */ if ( /*_#I__L19*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1427:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "get the value importer";
	    /*_.APPEND_COMMENTCONST__V136*/ meltfptr[135] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!APPEND_COMMENTCONST */ meltfrout->
			    tabval[19])),
			  (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1429:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1430:/ blockmultialloc");
	  /*multiallocblock */
	  {
	    struct meltletrec_5_st
	    {
	      struct meltpair_st rpair_0__OVALUEIMPORTER_x1;
	      struct meltlist_st rlist_1__LIST_;
	      long meltletrec_5_endgap;
	    } *meltletrec_5_ptr = 0;
	    meltletrec_5_ptr =
	      (struct meltletrec_5_st *)
	      meltgc_allocate (sizeof (struct meltletrec_5_st), 0);
	    /*^blockmultialloc.initfill */
	    /*inipair rpair_0__OVALUEIMPORTER_x1 */
     /*_.OVALUEIMPORTER__V138*/ meltfptr[137] =
	      (melt_ptr_t) & meltletrec_5_ptr->rpair_0__OVALUEIMPORTER_x1;
	    meltletrec_5_ptr->rpair_0__OVALUEIMPORTER_x1.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

	    /*inilist rlist_1__LIST_ */
     /*_.LIST___V139*/ meltfptr[138] =
	      (melt_ptr_t) & meltletrec_5_ptr->rlist_1__LIST_;
	    meltletrec_5_ptr->rlist_1__LIST_.discr =
	      (meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



	    /*^putpairhead */
	    /*putpairhead */
	    melt_assertmsg ("putpairhead /3 checkpair",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.OVALUEIMPORTER__V138*/
					       meltfptr[137])) ==
			    MELTOBMAG_PAIR);
	    ((meltpair_ptr_t) ( /*_.OVALUEIMPORTER__V138*/ meltfptr[137]))->
	      hd = (melt_ptr_t) ( /*_.OVALUEIMPORTER__V56*/ meltfptr[55]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.OVALUEIMPORTER__V138*/ meltfptr[137]);
	    ;
	    /*^putlist */
	    /*putlist */
	    melt_assertmsg ("putlist checklist",
			    melt_magic_discr ((melt_ptr_t)
					      ( /*_.LIST___V139*/
					       meltfptr[138])) ==
			    MELTOBMAG_LIST);
	    ((meltlist_ptr_t) ( /*_.LIST___V139*/ meltfptr[138]))->first =
	      (meltpair_ptr_t) ( /*_.OVALUEIMPORTER__V138*/ meltfptr[137]);
	    ((meltlist_ptr_t) ( /*_.LIST___V139*/ meltfptr[138]))->last =
	      (meltpair_ptr_t) ( /*_.OVALUEIMPORTER__V138*/ meltfptr[137]);
	    ;
	    /*^touch */
	    meltgc_touch ( /*_.LIST___V139*/ meltfptr[138]);
	    ;
	    /*_.LIST___V137*/ meltfptr[136] =
	      /*_.LIST___V139*/ meltfptr[138];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1430:/ clear");
	      /*clear *//*_.OVALUEIMPORTER__V138*/ meltfptr[137] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V139*/ meltfptr[138] = 0;
	    /*^clear */
	      /*clear *//*_.OVALUEIMPORTER__V138*/ meltfptr[137] = 0;
	    /*^clear */
	      /*clear *//*_.LIST___V139*/ meltfptr[138] = 0;
	  }			/*end multiallocblock */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1429:/ quasiblock");


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJGETSLOT */
						   meltfrout->tabval[23])),
				    (4), "CLASS_OBJGETSLOT");
    /*_.INST__V141*/ meltfptr[138] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V141*/ meltfptr[138]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V141*/ meltfptr[138]), (1),
				( /*_.LIST___V137*/ meltfptr[136]),
				"OBDI_DESTLIST");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OGETSL_OBJ",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V141*/ meltfptr[138]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V141*/ meltfptr[138]), (2),
				(( /*!INITIALSYSTEMDATA_OBJPREDEF */
				  meltfrout->tabval[24])), "OGETSL_OBJ");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OGETSL_FIELD",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V141*/ meltfptr[138]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V141*/ meltfptr[138]), (3),
				(( /*!SYSDATA_VALUE_IMPORTER */ meltfrout->
				  tabval[56])), "OGETSL_FIELD");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V141*/ meltfptr[138],
					"newly made instance");
	  ;
	  /*_.INST___V140*/ meltfptr[137] = /*_.INST__V141*/ meltfptr[138];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1428:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.OINIPROLOG__V24*/ meltfptr[23]),
				(melt_ptr_t) ( /*_.INST___V140*/
					      meltfptr[137]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1426:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1425:/ clear");
	     /*clear *//*_.APPEND_COMMENTCONST__V136*/ meltfptr[135] = 0;
	  /*^clear */
	     /*clear *//*_.LIST___V137*/ meltfptr[136] = 0;
	  /*^clear */
	     /*clear *//*_.INST___V140*/ meltfptr[137] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1434:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#LIST_LENGTH__L20*/ meltfnum[19] =
      (melt_list_length
       ((melt_ptr_t) ( /*_.IMPORTVALUES__V7*/ meltfptr[6])));;
    /*^compute */
 /*_#I__L21*/ meltfnum[20] =
      (( /*_#LIST_LENGTH__L20*/ meltfnum[19]) > (0));;
    MELT_LOCATION ("warmelt-genobj.melt:1434:/ cond");
    /*cond */ if ( /*_#I__L21*/ meltfnum[20])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1436:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "before getting imported values";
	    /*_.APPEND_COMMENTCONST__V143*/ meltfptr[136] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!APPEND_COMMENTCONST */ meltfrout->
			    tabval[19])),
			  (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1440:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V145*/ meltfptr[144] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_64 */
						      meltfrout->tabval[64])),
				(6));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V145*/
					     meltfptr[144])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V145*/
					      meltfptr[144])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V145*/ meltfptr[144])->tabval[0] =
	    (melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V145*/
					     meltfptr[144])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V145*/
					      meltfptr[144])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V145*/ meltfptr[144])->tabval[1] =
	    (melt_ptr_t) ( /*_.GCX__V43*/ meltfptr[42]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V145*/
					     meltfptr[144])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V145*/
					      meltfptr[144])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V145*/ meltfptr[144])->tabval[2] =
	    (melt_ptr_t) ( /*_.IMPORTMAP__V35*/ meltfptr[34]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V145*/
					     meltfptr[144])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 3 >= 0
			  && 3 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V145*/
					      meltfptr[144])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V145*/ meltfptr[144])->tabval[3] =
	    (melt_ptr_t) ( /*_.OVALUEIMPORTER__V56*/ meltfptr[55]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V145*/
					     meltfptr[144])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 4 >= 0
			  && 4 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V145*/
					      meltfptr[144])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V145*/ meltfptr[144])->tabval[4] =
	    (melt_ptr_t) ( /*_.OPREVENV__V54*/ meltfptr[52]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V145*/
					     meltfptr[144])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 5 >= 0
			  && 5 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V145*/
					      meltfptr[144])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V145*/ meltfptr[144])->tabval[5] =
	    (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]);
	  ;
	  /*_.LAMBDA___V144*/ meltfptr[137] =
	    /*_.LAMBDA___V145*/ meltfptr[144];;
	  MELT_LOCATION ("warmelt-genobj.melt:1438:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V144*/ meltfptr[137];
	    /*_.LIST_EVERY__V146*/ meltfptr[145] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[57])),
			  (melt_ptr_t) ( /*_.IMPORTVALUES__V7*/ meltfptr[6]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1457:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "after getting imported values";
	    /*_.APPEND_COMMENTCONST__V147*/ meltfptr[146] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!APPEND_COMMENTCONST */ meltfrout->
			    tabval[19])),
			  (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1435:/ quasiblock");


	  /*_.PROGN___V148*/ meltfptr[147] =
	    /*_.APPEND_COMMENTCONST__V147*/ meltfptr[146];;
	  /*^compute */
	  /*_.IF___V142*/ meltfptr[135] = /*_.PROGN___V148*/ meltfptr[147];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1434:/ clear");
	     /*clear *//*_.APPEND_COMMENTCONST__V143*/ meltfptr[136] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V144*/ meltfptr[137] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V146*/ meltfptr[145] = 0;
	  /*^clear */
	     /*clear *//*_.APPEND_COMMENTCONST__V147*/ meltfptr[146] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V148*/ meltfptr[147] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V142*/ meltfptr[135] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1460:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "before toplevel body";
      /*_.APPEND_COMMENTCONST__V149*/ meltfptr[136] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1463:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V151*/ meltfptr[145] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_65 */ meltfrout->
						tabval[65])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V151*/ meltfptr[145])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V151*/ meltfptr[145])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V151*/ meltfptr[145])->tabval[0] =
      (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]);
    ;
    /*_.LAMBDA___V150*/ meltfptr[137] = /*_.LAMBDA___V151*/ meltfptr[145];;
    MELT_LOCATION ("warmelt-genobj.melt:1461:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V150*/ meltfptr[137];
      /*_.LIST_EVERY__V152*/ meltfptr[146] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[57])),
		    (melt_ptr_t) ( /*_.OBJTOPLIS__V131*/ meltfptr[130]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1465:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "after toplevel body";
      /*_.APPEND_COMMENTCONST__V153*/ meltfptr[147] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1467:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "compute boxloc again";
      /*_.APPEND_COMMENTCONST__V154*/ meltfptr[153] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1468:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
			  (melt_ptr_t) ( /*_.OTESTCOMPUTBOXLOC__V106*/
					meltfptr[90]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1469:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "intern symbols";
      /*_.APPEND_COMMENTCONST__V155*/ meltfptr[154] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1473:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V157*/ meltfptr[156] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_72 */ meltfrout->
						tabval[72])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V157*/ meltfptr[156])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V157*/ meltfptr[156])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V157*/ meltfptr[156])->tabval[0] =
      (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]);
    ;
    /*_.LAMBDA___V156*/ meltfptr[155] = /*_.LAMBDA___V157*/ meltfptr[156];;
    MELT_LOCATION ("warmelt-genobj.melt:1471:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V156*/ meltfptr[155];
      /*_.MULTIPLE_EVERY__V158*/ meltfptr[157] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[48])),
		    (melt_ptr_t) ( /*_.ODATATUP__V124*/ meltfptr[123]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V128*/ meltfptr[126] =
      /*_.MULTIPLE_EVERY__V158*/ meltfptr[157];;

    MELT_LOCATION ("warmelt-genobj.melt:1390:/ clear");
	   /*clear *//*_.LAMBDA___V129*/ meltfptr[128] = 0;
    /*^clear */
	   /*clear *//*_.OBJTOPLIS__V131*/ meltfptr[130] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V132*/ meltfptr[131] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V133*/ meltfptr[132] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V135*/ meltfptr[134] = 0;
    /*^clear */
	   /*clear *//*_#MAPOBJECT_COUNT__L18*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#I__L19*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#LIST_LENGTH__L20*/ meltfnum[19] = 0;
    /*^clear */
	   /*clear *//*_#I__L21*/ meltfnum[20] = 0;
    /*^clear */
	   /*clear *//*_.IF___V142*/ meltfptr[135] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V149*/ meltfptr[136] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V150*/ meltfptr[137] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V152*/ meltfptr[146] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V153*/ meltfptr[147] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V154*/ meltfptr[153] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V155*/ meltfptr[154] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V156*/ meltfptr[155] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V158*/ meltfptr[157] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1496:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "set retinit from boxloc";
      /*_.APPEND_COMMENTCONST__V159*/ meltfptr[128] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1498:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:1499:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!CTYPE_VALUE */ meltfrout->tabval[39]);
      /*^apply.arg */
      argtab[1].meltbp_cstring =
	"/* finalsetretinit */ melt_reference_value((melt_ptr_t)(";
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.BOXLOC__V50*/ meltfptr[48];
      /*^apply.arg */
      argtab[3].meltbp_cstring = "))";
      /*_.OSETRETINIT__V160*/ meltfptr[130] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MAKE_OBJCOMPUTE */ meltfrout->tabval[26])),
		    (melt_ptr_t) (( /*nil */ NULL)),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1502:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.RETINIT__V47*/ meltfptr[45];
      /*_.PUT_OBJDEST__V161*/ meltfptr[131] =
	meltgc_send ((melt_ptr_t) ( /*_.OSETRETINIT__V160*/ meltfptr[130]),
		     (melt_ptr_t) (( /*!PUT_OBJDEST */ meltfrout->
				    tabval[73])), (MELTBPARSTR_PTR ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1503:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L22*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1503:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1503:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L23*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1503;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initproc adding osetretinit=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OSETRETINIT__V160*/ meltfptr[130];
	      /*_.MELT_DEBUG_FUN__V164*/ meltfptr[135] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V163*/ meltfptr[134] =
	      /*_.MELT_DEBUG_FUN__V164*/ meltfptr[135];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1503:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L23*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V164*/ meltfptr[135] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V163*/ meltfptr[134] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1503:/ quasiblock");


      /*_.PROGN___V165*/ meltfptr[136] = /*_.IF___V163*/ meltfptr[134];;
      /*^compute */
      /*_.IFCPP___V162*/ meltfptr[132] = /*_.PROGN___V165*/ meltfptr[136];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1503:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V163*/ meltfptr[134] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V165*/ meltfptr[136] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V162*/ meltfptr[132] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1504:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
			  (melt_ptr_t) ( /*_.OSETRETINIT__V160*/
					meltfptr[130]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:1498:/ clear");
	   /*clear *//*_.OSETRETINIT__V160*/ meltfptr[130] = 0;
    /*^clear */
	   /*clear *//*_.PUT_OBJDEST__V161*/ meltfptr[131] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V162*/ meltfptr[132] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1506:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "end the initproc";
      /*_.APPEND_COMMENTCONST__V166*/ meltfptr[137] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[19])),
		    (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1507:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L24*/ meltfnum[19] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1507:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[19])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[20] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1507:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L25*/ meltfnum[20];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1507;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initproc final oinibody=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OINIBODY__V25*/ meltfptr[24];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " gcx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.GCX__V43*/ meltfptr[42];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " oinitrout=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.OINITROUT__V33*/ meltfptr[32];
	      /*_.MELT_DEBUG_FUN__V169*/ meltfptr[153] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V168*/ meltfptr[147] =
	      /*_.MELT_DEBUG_FUN__V169*/ meltfptr[153];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1507:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L25*/ meltfnum[20] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V169*/ meltfptr[153] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V168*/ meltfptr[147] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1507:/ quasiblock");


      /*_.PROGN___V170*/ meltfptr[154] = /*_.IF___V168*/ meltfptr[147];;
      /*^compute */
      /*_.IFCPP___V167*/ meltfptr[146] = /*_.PROGN___V170*/ meltfptr[154];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1507:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[19] = 0;
      /*^clear */
	     /*clear *//*_.IF___V168*/ meltfptr[147] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V170*/ meltfptr[154] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V167*/ meltfptr[146] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1508:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OINITROUT__V33*/ meltfptr[32];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1508:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V121*/ meltfptr[112] = /*_.RETURN___V171*/ meltfptr[155];;

    MELT_LOCATION ("warmelt-genobj.melt:1376:/ clear");
	   /*clear *//*_.LAMBDA___V122*/ meltfptr[116] = 0;
    /*^clear */
	   /*clear *//*_.ODATATUP__V124*/ meltfptr[123] = 0;
    /*^clear */
	   /*clear *//*_.TOPLIS__V125*/ meltfptr[124] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V126*/ meltfptr[125] = 0;
    /*^clear */
	   /*clear *//*_.LET___V128*/ meltfptr[126] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V159*/ meltfptr[128] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V166*/ meltfptr[137] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V167*/ meltfptr[146] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V171*/ meltfptr[155] = 0;
    /*_.LET___V80*/ meltfptr[74] = /*_.LET___V121*/ meltfptr[112];;

    MELT_LOCATION ("warmelt-genobj.melt:1293:/ clear");
	   /*clear *//*_.LET___V81*/ meltfptr[73] = 0;
    /*^clear */
	   /*clear *//*_.OCOMPUTBOXLOC__V100*/ meltfptr[67] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_STRINGCONST__V101*/ meltfptr[65] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V102*/ meltfptr[66] = 0;
    /*^clear */
	   /*clear *//*_.INST___V104*/ meltfptr[88] = 0;
    /*^clear */
	   /*clear *//*_.OTESTCOMPUTBOXLOC__V106*/ meltfptr[90] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V108*/ meltfptr[96] = 0;
    /*^clear */
	   /*clear *//*_.LIMPLOCV__V110*/ meltfptr[72] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V111*/ meltfptr[110] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V120*/ meltfptr[111] = 0;
    /*^clear */
	   /*clear *//*_.LET___V121*/ meltfptr[112] = 0;
    /*_.LET___V22*/ meltfptr[20] = /*_.LET___V80*/ meltfptr[74];;

    MELT_LOCATION ("warmelt-genobj.melt:1209:/ clear");
	   /*clear *//*_.LOCMAP__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OINIPROLOG__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OINIBODY__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.MOCX_MODULENAME__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.OINITROUT__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IMPORTMAP__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPOBJECT__V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPOBJECT__V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.LIST_MAP__V42*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.GCX__V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.LET___V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.RETINIT__V47*/ meltfptr[45] = 0;
    /*^clear */
	   /*clear *//*_.LET___V48*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.BOXLOC__V50*/ meltfptr[48] = 0;
    /*^clear */
	   /*clear *//*_.OFRESHENV__V51*/ meltfptr[50] = 0;
    /*^clear */
	   /*clear *//*_.LET___V52*/ meltfptr[51] = 0;
    /*^clear */
	   /*clear *//*_.OPREVENV__V54*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.OVALUEEXPORTER__V55*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_.OVALUEIMPORTER__V56*/ meltfptr[55] = 0;
    /*^clear */
	   /*clear *//*_.INIDEFBINDS__V57*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V58*/ meltfptr[57] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V59*/ meltfptr[58] = 0;
    /*^clear */
	   /*clear *//*_.LET___V80*/ meltfptr[74] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1197:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V22*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1197:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LET___V22*/ meltfptr[20] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILE2OBJ_INITPROC", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_genobj_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_22_warmelt_genobj_LAMBDA___3___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_22_warmelt_genobj_LAMBDA___3___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_22_warmelt_genobj_LAMBDA___3__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_22_warmelt_genobj_LAMBDA___3___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_22_warmelt_genobj_LAMBDA___3__ nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1245:/ getarg");
 /*_.CURPRO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1246:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1246:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1246:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1246;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initproc procurmodenvlist curpro=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURPRO__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1246:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1246:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1246:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1247:/ quasiblock");


 /*_.CUROU__V8*/ meltfptr[4] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   (( /*~COMPICACHE */ meltfclos->tabval[0])),
			   (meltobject_ptr_t) ( /*_.CURPRO__V2*/
					       meltfptr[1]));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1248:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1248:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1248:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1248;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initproc procurmodenvlist curou=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CUROU__V8*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V10*/ meltfptr[9] =
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1248:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V10*/ meltfptr[9] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1248:/ quasiblock");


      /*_.PROGN___V12*/ meltfptr[10] = /*_.IF___V10*/ meltfptr[9];;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.PROGN___V12*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1248:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V10*/ meltfptr[9] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V12*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1249:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CUROU__V8*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-genobj.melt:1249:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1249:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curou"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1249) ? (1249) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[9] = /*_.IFELSE___V14*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1249:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[3] = /*_.CUROU__V8*/ meltfptr[4];;

    MELT_LOCATION ("warmelt-genobj.melt:1247:/ clear");
	   /*clear *//*_.CUROU__V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1245:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1245:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_22_warmelt_genobj_LAMBDA___3___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_22_warmelt_genobj_LAMBDA___3__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_genobj_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_23_warmelt_genobj_LAMBDA___4___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_23_warmelt_genobj_LAMBDA___4___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_23_warmelt_genobj_LAMBDA___4__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_23_warmelt_genobj_LAMBDA___4___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_23_warmelt_genobj_LAMBDA___4__ nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1348:/ getarg");
 /*_.IVAL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1349:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1349:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1349:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1349;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initproc imported ival=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.IVAL__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1349:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1349:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1349:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1350:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.IVAL__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_IMPORTEDVAL */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:1350:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1350:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ival"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1350) ? (1350) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[3] = /*_.IFELSE___V8*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1350:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1351:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.IVAL__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NIMPORT_SYMB");
  /*_.ISYM__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1352:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ISYM__V10*/ meltfptr[9];
      /*_.ILOCV__V11*/ meltfptr[10] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[2])),
		    (melt_ptr_t) (( /*~GCX */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1354:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1354:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1354:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1354;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initproc imported ilocv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ILOCV__V11*/ meltfptr[10];
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V13*/ meltfptr[12] =
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1354:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V13*/ meltfptr[12] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1354:/ quasiblock");


      /*_.PROGN___V15*/ meltfptr[13] = /*_.IF___V13*/ meltfptr[12];;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.PROGN___V15*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1354:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V15*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1355:/ locexp");
      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
			     (( /*~IMPORTMAP */ meltfclos->tabval[1])),
			     (meltobject_ptr_t) ( /*_.ISYM__V10*/
						 meltfptr[9]),
			     (melt_ptr_t) ( /*_.ILOCV__V11*/ meltfptr[10]));
    }
    ;
    /*_.LET___V9*/ meltfptr[4] = /*_.ILOCV__V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-genobj.melt:1351:/ clear");
	   /*clear *//*_.ISYM__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.ILOCV__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1348:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V9*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1348:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V9*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_23_warmelt_genobj_LAMBDA___4___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_23_warmelt_genobj_LAMBDA___4__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_genobj_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_24_warmelt_genobj_LAMBDA___5___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_24_warmelt_genobj_LAMBDA___5___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_24_warmelt_genobj_LAMBDA___5__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_24_warmelt_genobj_LAMBDA___5___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_24_warmelt_genobj_LAMBDA___5__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1380:/ getarg");
 /*_.CURDAT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1381:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CURDAT__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_BOUND_DATA */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:1381:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1381:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curdat"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1381) ? (1381) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1381:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1382:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.CUROBD__V6*/ meltfptr[5] =
	meltgc_send ((melt_ptr_t) ( /*_.CURDAT__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[1])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V5*/ meltfptr[3] = /*_.CUROBD__V6*/ meltfptr[5];;

    MELT_LOCATION ("warmelt-genobj.melt:1382:/ clear");
	   /*clear *//*_.CUROBD__V6*/ meltfptr[5] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1380:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1380:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_24_warmelt_genobj_LAMBDA___5___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_24_warmelt_genobj_LAMBDA___5__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_genobj_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_25_warmelt_genobj_LAMBDA___6___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_25_warmelt_genobj_LAMBDA___6___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_25_warmelt_genobj_LAMBDA___6__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_25_warmelt_genobj_LAMBDA___6___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_25_warmelt_genobj_LAMBDA___6__ nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1393:/ getarg");
 /*_.CURTOP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:1394:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.OTOP__V4*/ meltfptr[3] =
	meltgc_send ((melt_ptr_t) ( /*_.CURTOP__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[0])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1395:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1395:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1395:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1395;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_initproc otop=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OTOP__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1395:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1395:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1395:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V3*/ meltfptr[2] = /*_.OTOP__V4*/ meltfptr[3];;

    MELT_LOCATION ("warmelt-genobj.melt:1394:/ clear");
	   /*clear *//*_.OTOP__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1393:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1393:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_25_warmelt_genobj_LAMBDA___6___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_25_warmelt_genobj_LAMBDA___6__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_genobj_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_26_warmelt_genobj_LAMBDA___7___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_26_warmelt_genobj_LAMBDA___7___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_26_warmelt_genobj_LAMBDA___7__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_26_warmelt_genobj_LAMBDA___7___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_26_warmelt_genobj_LAMBDA___7__ nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1402:/ getarg");
 /*_.CURPDAT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#CURK__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:1403:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L2*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CURPDAT__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
					  meltfrout->tabval[0])));;
    MELT_LOCATION ("warmelt-genobj.melt:1403:/ cond");
    /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1404:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURPDAT__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "OIE_DATA");
    /*_.ODAT__V5*/ meltfptr[4] = slot;
	  };
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1406:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L3*/ meltfnum[2] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1406:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[2])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1406:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1406;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_initproc getting curpdat=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPDAT__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V7*/ meltfptr[6] =
		    /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1406:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V7*/ meltfptr[6] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1406:/ quasiblock");


	    /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
	    /*^compute */
	    /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1406:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1409:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L5*/ meltfnum[3] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.ODAT__V5*/ meltfptr[4]),
				 (melt_ptr_t) (( /*!CLASS_NREP_DATAKEYWORD */
						meltfrout->tabval[2])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1409:/ cond");
	  /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1410:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_OBJGETNAMEDKEYWORD */ meltfrout->tabval[3])), (2), "CLASS_OBJGETNAMEDKEYWORD");
      /*_.INST__V13*/ meltfptr[12] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBGNAMED_IOBJ",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V13*/
						   meltfptr[12])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V13*/ meltfptr[12]), (1),
				      ( /*_.CURPDAT__V2*/ meltfptr[1]),
				      "OBGNAMED_IOBJ");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V13*/ meltfptr[12],
					      "newly made instance");
		;
		/*_.OGKW__V12*/ meltfptr[11] = /*_.INST__V13*/ meltfptr[12];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:1413:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      (( /*~OINIPROLOG */ meltfclos->
					tabval[0])),
				      (melt_ptr_t) ( /*_.OGKW__V12*/
						    meltfptr[11]));
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:1414:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L6*/ meltfnum[2] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1414:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[2])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1414:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-genobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 1414;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "compile2obj_initproc added keyword getting ogkw=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.OGKW__V12*/ meltfptr[11];
			  /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[1])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V15*/ meltfptr[14] =
			  /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1414:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V15*/ meltfptr[14] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-genobj.melt:1414:/ quasiblock");


		  /*_.PROGN___V17*/ meltfptr[15] =
		    /*_.IF___V15*/ meltfptr[14];;
		  /*^compute */
		  /*_.IFCPP___V14*/ meltfptr[13] =
		    /*_.PROGN___V17*/ meltfptr[15];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1414:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[2] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
		/*_.LET___V11*/ meltfptr[7] = /*_.IFCPP___V14*/ meltfptr[13];;

		MELT_LOCATION ("warmelt-genobj.melt:1410:/ clear");
	       /*clear *//*_.OGKW__V12*/ meltfptr[11] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
		/*_.IFELSE___V10*/ meltfptr[6] = /*_.LET___V11*/ meltfptr[7];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1409:/ clear");
	       /*clear *//*_.LET___V11*/ meltfptr[7] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1416:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L8*/ meltfnum[6] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.ODAT__V5*/ meltfptr[4]),
				       (melt_ptr_t) (( /*!CLASS_NREP_DATASYMBOL */ meltfrout->tabval[4])));;
		MELT_LOCATION ("warmelt-genobj.melt:1416:/ cond");
		/*cond */ if ( /*_#IS_A__L8*/ meltfnum[6])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-genobj.melt:1417:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_OBJGETNAMEDSYMBOL */ meltfrout->tabval[5])), (2), "CLASS_OBJGETNAMEDSYMBOL");
	/*_.INST__V21*/ meltfptr[13] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBGNAMED_IOBJ",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V21*/
							 meltfptr[13])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V21*/ meltfptr[13]),
					    (1),
					    ( /*_.CURPDAT__V2*/ meltfptr[1]),
					    "OBGNAMED_IOBJ");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V21*/
						    meltfptr[13],
						    "newly made instance");
		      ;
		      /*_.OGSY__V20*/ meltfptr[11] =
			/*_.INST__V21*/ meltfptr[13];;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1420:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    (( /*~OINIPROLOG */ meltfclos->
					      tabval[0])),
					    (melt_ptr_t) ( /*_.OGSY__V20*/
							  meltfptr[11]));
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-genobj.melt:1421:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L9*/ meltfnum[2] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-genobj.melt:1421:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[2])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-genobj.melt:1421:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-genobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1421;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "compile2obj_initproc added symbol getting ogsy=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.OGSY__V20*/
				  meltfptr[11];
				/*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[1])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V23*/ meltfptr[22] =
				/*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-genobj.melt:1421:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L10*/
				meltfnum[9] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V23*/ meltfptr[22] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1421:/ quasiblock");


			/*_.PROGN___V25*/ meltfptr[23] =
			  /*_.IF___V23*/ meltfptr[22];;
			/*^compute */
			/*_.IFCPP___V22*/ meltfptr[7] =
			  /*_.PROGN___V25*/ meltfptr[23];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1421:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[2] = 0;
			/*^clear */
		   /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V22*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      /*^compute */
		      /*_.LET___V19*/ meltfptr[15] =
			/*_.IFCPP___V22*/ meltfptr[7];;

		      MELT_LOCATION ("warmelt-genobj.melt:1417:/ clear");
		 /*clear *//*_.OGSY__V20*/ meltfptr[11] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V22*/ meltfptr[7] = 0;
		      /*_.IFELSE___V18*/ meltfptr[14] =
			/*_.LET___V19*/ meltfptr[15];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:1416:/ clear");
		 /*clear *//*_.LET___V19*/ meltfptr[15] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IFELSE___V18*/ meltfptr[14] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V10*/ meltfptr[6] =
		  /*_.IFELSE___V18*/ meltfptr[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1409:/ clear");
	       /*clear *//*_#IS_A__L8*/ meltfnum[6] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V18*/ meltfptr[14] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.LET___V4*/ meltfptr[3] = /*_.IFELSE___V10*/ meltfptr[6];;

	  MELT_LOCATION ("warmelt-genobj.melt:1404:/ clear");
	     /*clear *//*_.ODAT__V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
	  /*^clear */
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	  /*_.IF___V3*/ meltfptr[2] = /*_.LET___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1403:/ clear");
	     /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1402:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1402:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_26_warmelt_genobj_LAMBDA___7___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_26_warmelt_genobj_LAMBDA___7__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_genobj_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_27_warmelt_genobj_LAMBDA___8___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_27_warmelt_genobj_LAMBDA___8___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 21
    melt_ptr_t mcfr_varptr[21];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_27_warmelt_genobj_LAMBDA___8__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_27_warmelt_genobj_LAMBDA___8___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 21; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_27_warmelt_genobj_LAMBDA___8__ nbval 21*/
  meltfram__.mcfr_nbvar = 21 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1440:/ getarg");
 /*_.CURIMPORT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1441:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1441:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1441:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1441;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_initproc curimport=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURIMPORT__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1441:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1441:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1441:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1442:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CURIMPORT__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_IMPORTEDVAL */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:1442:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1442:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curimport"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1442) ? (1442) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[3] = /*_.IFELSE___V8*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1442:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1444:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CURIMPORT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NIMPORT_SYMB");
  /*_.IMPSYM__V9*/ meltfptr[4] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1445:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CURIMPORT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NIMPORT_SYDATA");
  /*_.IMPSYDAT__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1446:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.IMPSYM__V9*/ meltfptr[4]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.IMPSYM__V9*/ meltfptr[4]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.IMPSYMSTR__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.IMPSYMSTR__V11*/ meltfptr[10] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1447:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*~MODCTX */ meltfclos->
					  tabval[0])),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) (( /*~MODCTX */ meltfclos->tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "MOCX_MODULENAME");
   /*_.MODULSTR__V12*/ meltfptr[11] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MODULSTR__V12*/ meltfptr[11] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1448:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[1]);
      /*_.OSYMDAT__V13*/ meltfptr[12] =
	meltgc_send ((melt_ptr_t) ( /*_.IMPSYDAT__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
 /*_.OIMPLOCV__V14*/ meltfptr[13] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   (( /*~IMPORTMAP */ meltfclos->tabval[2])),
			   (meltobject_ptr_t) ( /*_.IMPSYM__V9*/
					       meltfptr[4]));;
    MELT_LOCATION ("warmelt-genobj.melt:1450:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1451:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct meltpair_st rpair_0__OIMPLOCV_x1;
	struct meltlist_st rlist_1__LIST_;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inipair rpair_0__OIMPLOCV_x1 */
   /*_.OIMPLOCV__V16*/ meltfptr[15] =
	(melt_ptr_t) & meltletrec_1_ptr->rpair_0__OIMPLOCV_x1;
      meltletrec_1_ptr->rpair_0__OIMPLOCV_x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

      /*inilist rlist_1__LIST_ */
   /*_.LIST___V17*/ meltfptr[16] =
	(melt_ptr_t) & meltletrec_1_ptr->rlist_1__LIST_;
      meltletrec_1_ptr->rlist_1__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*^putpairhead */
      /*putpairhead */
      melt_assertmsg ("putpairhead /4 checkpair",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.OIMPLOCV__V16*/ meltfptr[15]))
		      == MELTOBMAG_PAIR);
      ((meltpair_ptr_t) ( /*_.OIMPLOCV__V16*/ meltfptr[15]))->hd =
	(melt_ptr_t) ( /*_.OIMPLOCV__V14*/ meltfptr[13]);
      ;
      /*^touch */
      meltgc_touch ( /*_.OIMPLOCV__V16*/ meltfptr[15]);
      ;
      /*^putlist */
      /*putlist */
      melt_assertmsg ("putlist checklist",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.LIST___V17*/ meltfptr[16])) ==
		      MELTOBMAG_LIST);
      ((meltlist_ptr_t) ( /*_.LIST___V17*/ meltfptr[16]))->first =
	(meltpair_ptr_t) ( /*_.OIMPLOCV__V16*/ meltfptr[15]);
      ((meltlist_ptr_t) ( /*_.LIST___V17*/ meltfptr[16]))->last =
	(meltpair_ptr_t) ( /*_.OIMPLOCV__V16*/ meltfptr[15]);
      ;
      /*^touch */
      meltgc_touch ( /*_.LIST___V17*/ meltfptr[16]);
      ;
      /*_.LIST___V15*/ meltfptr[14] = /*_.LIST___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1451:/ clear");
	    /*clear *//*_.OIMPLOCV__V16*/ meltfptr[15] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V17*/ meltfptr[16] = 0;
      /*^clear */
	    /*clear *//*_.OIMPLOCV__V16*/ meltfptr[15] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V17*/ meltfptr[16] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1453:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_2_st
      {
	struct MELT_MULTIPLE_STRUCT (4) rtup_0__TUPLREC__x3;
	long meltletrec_2_endgap;
      } *meltletrec_2_ptr = 0;
      meltletrec_2_ptr =
	(struct meltletrec_2_st *)
	meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x3 */
 /*_.TUPLREC___V19*/ meltfptr[16] =
	(melt_ptr_t) & meltletrec_2_ptr->rtup_0__TUPLREC__x3;
      meltletrec_2_ptr->rtup_0__TUPLREC__x3.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_2_ptr->rtup_0__TUPLREC__x3.nbval = 4;


      /*^putuple */
      /*putupl#4 */
      melt_assertmsg ("putupl [:1453] #4 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V19*/ meltfptr[16]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1453] #4 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V19*/
					      meltfptr[16]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V19*/ meltfptr[16]))->tabval[0] =
	(melt_ptr_t) ( /*_.OSYMDAT__V13*/ meltfptr[12]);
      ;
      /*^putuple */
      /*putupl#5 */
      melt_assertmsg ("putupl [:1453] #5 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V19*/ meltfptr[16]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1453] #5 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V19*/
					      meltfptr[16]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V19*/ meltfptr[16]))->tabval[1] =
	(melt_ptr_t) (( /*~OPREVENV */ meltfclos->tabval[4]));
      ;
      /*^putuple */
      /*putupl#6 */
      melt_assertmsg ("putupl [:1453] #6 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V19*/ meltfptr[16]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1453] #6 checkoff",
		      (2 >= 0
		       && 2 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V19*/
					      meltfptr[16]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V19*/ meltfptr[16]))->tabval[2] =
	(melt_ptr_t) ( /*_.IMPSYMSTR__V11*/ meltfptr[10]);
      ;
      /*^putuple */
      /*putupl#7 */
      melt_assertmsg ("putupl [:1453] #7 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V19*/ meltfptr[16]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:1453] #7 checkoff",
		      (3 >= 0
		       && 3 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V19*/
					      meltfptr[16]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V19*/ meltfptr[16]))->tabval[3] =
	(melt_ptr_t) ( /*_.MODULSTR__V12*/ meltfptr[11]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V19*/ meltfptr[16]);
      ;
      /*_.TUPLE___V18*/ meltfptr[15] = /*_.TUPLREC___V19*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1453:/ clear");
	    /*clear *//*_.TUPLREC___V19*/ meltfptr[16] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V19*/ meltfptr[16] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1450:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJAPPLY */ meltfrout->
					     tabval[5])), (4),
			      "CLASS_OBJAPPLY");
  /*_.INST__V21*/ meltfptr[20] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V21*/ meltfptr[20])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V21*/ meltfptr[20]), (1),
			  ( /*_.LIST___V15*/ meltfptr[14]), "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBAPP_CLOS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V21*/ meltfptr[20])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V21*/ meltfptr[20]), (2),
			  (( /*~OVALUEIMPORTER */ meltfclos->tabval[3])),
			  "OBAPP_CLOS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBAPP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V21*/ meltfptr[20])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V21*/ meltfptr[20]), (3),
			  ( /*_.TUPLE___V18*/ meltfptr[15]), "OBAPP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V21*/ meltfptr[20],
				  "newly made instance");
    ;
    /*_.OIMPAPPL__V20*/ meltfptr[16] = /*_.INST__V21*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1455:/ locexp");
      meltgc_append_list ((melt_ptr_t)
			  (( /*~OINIPROLOG */ meltfclos->tabval[5])),
			  (melt_ptr_t) ( /*_.OIMPAPPL__V20*/ meltfptr[16]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:1444:/ clear");
	   /*clear *//*_.IMPSYM__V9*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IMPSYDAT__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IMPSYMSTR__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.MODULSTR__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OSYMDAT__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OIMPLOCV__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LIST___V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V18*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OIMPAPPL__V20*/ meltfptr[16] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1440:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_27_warmelt_genobj_LAMBDA___8___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_27_warmelt_genobj_LAMBDA___8__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_genobj_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_28_warmelt_genobj_LAMBDA___9___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_28_warmelt_genobj_LAMBDA___9___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_28_warmelt_genobj_LAMBDA___9__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_28_warmelt_genobj_LAMBDA___9___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_28_warmelt_genobj_LAMBDA___9__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1463:/ getarg");
 /*_.CUROBJT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-genobj.melt:1464:/ locexp");
      meltgc_append_list ((melt_ptr_t)
			  (( /*~OINIBODY */ meltfclos->tabval[0])),
			  (melt_ptr_t) ( /*_.CUROBJT__V2*/ meltfptr[1]));
    }
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_28_warmelt_genobj_LAMBDA___9___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_28_warmelt_genobj_LAMBDA___9__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_genobj_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_29_warmelt_genobj_LAMBDA___10___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_29_warmelt_genobj_LAMBDA___10___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_29_warmelt_genobj_LAMBDA___10__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_29_warmelt_genobj_LAMBDA___10___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_29_warmelt_genobj_LAMBDA___10__ nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1473:/ getarg");
 /*_.CURPDAT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#CURK__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:1474:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L2*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CURPDAT__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
					  meltfrout->tabval[0])));;
    MELT_LOCATION ("warmelt-genobj.melt:1474:/ cond");
    /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1475:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURPDAT__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "OIE_DATA");
    /*_.ODAT__V5*/ meltfptr[4] = slot;
	  };
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1477:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L3*/ meltfnum[2] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1477:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[2])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1477:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1477;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_initproc interning curpdat=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPDAT__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V7*/ meltfptr[6] =
		    /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1477:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V7*/ meltfptr[6] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1477:/ quasiblock");


	    /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
	    /*^compute */
	    /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1477:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1480:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L5*/ meltfnum[3] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.ODAT__V5*/ meltfptr[4]),
				 (melt_ptr_t) (( /*!CLASS_NREP_DATAKEYWORD */
						meltfrout->tabval[2])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1480:/ cond");
	  /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1481:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_OBJINTERNKEYWORD */ meltfrout->tabval[3])), (2), "CLASS_OBJINTERNKEYWORD");
      /*_.INST__V13*/ meltfptr[12] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBINTERN_IOBJ",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V13*/
						   meltfptr[12])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V13*/ meltfptr[12]), (1),
				      ( /*_.CURPDAT__V2*/ meltfptr[1]),
				      "OBINTERN_IOBJ");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V13*/ meltfptr[12],
					      "newly made instance");
		;
		/*_.OIKW__V12*/ meltfptr[11] = /*_.INST__V13*/ meltfptr[12];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:1484:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      (( /*~OINIBODY */ meltfclos->
					tabval[0])),
				      (melt_ptr_t) ( /*_.OIKW__V12*/
						    meltfptr[11]));
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:1485:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L6*/ meltfnum[2] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1485:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[2])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1485:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-genobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 1485;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "compile2obj_initproc added keyword interning oikw=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.OIKW__V12*/ meltfptr[11];
			  /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[1])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V15*/ meltfptr[14] =
			  /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1485:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V15*/ meltfptr[14] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-genobj.melt:1485:/ quasiblock");


		  /*_.PROGN___V17*/ meltfptr[15] =
		    /*_.IF___V15*/ meltfptr[14];;
		  /*^compute */
		  /*_.IFCPP___V14*/ meltfptr[13] =
		    /*_.PROGN___V17*/ meltfptr[15];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1485:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[2] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
		/*_.LET___V11*/ meltfptr[7] = /*_.IFCPP___V14*/ meltfptr[13];;

		MELT_LOCATION ("warmelt-genobj.melt:1481:/ clear");
	       /*clear *//*_.OIKW__V12*/ meltfptr[11] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
		/*_.IFELSE___V10*/ meltfptr[6] = /*_.LET___V11*/ meltfptr[7];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1480:/ clear");
	       /*clear *//*_.LET___V11*/ meltfptr[7] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1487:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L8*/ meltfnum[6] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.ODAT__V5*/ meltfptr[4]),
				       (melt_ptr_t) (( /*!CLASS_NREP_DATASYMBOL */ meltfrout->tabval[4])));;
		MELT_LOCATION ("warmelt-genobj.melt:1487:/ cond");
		/*cond */ if ( /*_#IS_A__L8*/ meltfnum[6])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-genobj.melt:1488:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_OBJINTERNSYMBOL */ meltfrout->tabval[5])), (2), "CLASS_OBJINTERNSYMBOL");
	/*_.INST__V21*/ meltfptr[13] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBINTERN_IOBJ",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V21*/
							 meltfptr[13])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V21*/ meltfptr[13]),
					    (1),
					    ( /*_.CURPDAT__V2*/ meltfptr[1]),
					    "OBINTERN_IOBJ");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V21*/
						    meltfptr[13],
						    "newly made instance");
		      ;
		      /*_.OISY__V20*/ meltfptr[11] =
			/*_.INST__V21*/ meltfptr[13];;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1491:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    (( /*~OINIBODY */ meltfclos->
					      tabval[0])),
					    (melt_ptr_t) ( /*_.OISY__V20*/
							  meltfptr[11]));
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-genobj.melt:1492:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L9*/ meltfnum[2] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-genobj.melt:1492:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[2])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-genobj.melt:1492:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-genobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1492;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "compile2obj_initproc added symbol interning oisy=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.OISY__V20*/
				  meltfptr[11];
				/*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[1])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V23*/ meltfptr[22] =
				/*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-genobj.melt:1492:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L10*/
				meltfnum[9] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V23*/ meltfptr[22] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1492:/ quasiblock");


			/*_.PROGN___V25*/ meltfptr[23] =
			  /*_.IF___V23*/ meltfptr[22];;
			/*^compute */
			/*_.IFCPP___V22*/ meltfptr[7] =
			  /*_.PROGN___V25*/ meltfptr[23];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1492:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[2] = 0;
			/*^clear */
		   /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V22*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      /*^compute */
		      /*_.LET___V19*/ meltfptr[15] =
			/*_.IFCPP___V22*/ meltfptr[7];;

		      MELT_LOCATION ("warmelt-genobj.melt:1488:/ clear");
		 /*clear *//*_.OISY__V20*/ meltfptr[11] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V22*/ meltfptr[7] = 0;
		      /*_.IFELSE___V18*/ meltfptr[14] =
			/*_.LET___V19*/ meltfptr[15];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:1487:/ clear");
		 /*clear *//*_.LET___V19*/ meltfptr[15] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IFELSE___V18*/ meltfptr[14] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V10*/ meltfptr[6] =
		  /*_.IFELSE___V18*/ meltfptr[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1480:/ clear");
	       /*clear *//*_#IS_A__L8*/ meltfnum[6] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V18*/ meltfptr[14] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.LET___V4*/ meltfptr[3] = /*_.IFELSE___V10*/ meltfptr[6];;

	  MELT_LOCATION ("warmelt-genobj.melt:1475:/ clear");
	     /*clear *//*_.ODAT__V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
	  /*^clear */
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	  /*_.IF___V3*/ meltfptr[2] = /*_.LET___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1474:/ clear");
	     /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1473:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1473:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_29_warmelt_genobj_LAMBDA___10___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_29_warmelt_genobj_LAMBDA___10__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 201
    melt_ptr_t mcfr_varptr[201];
#define MELTFRAM_NBVARNUM 54
    long mcfr_varnum[54];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 201; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC nbval 201*/
  meltfram__.mcfr_nbvar = 201 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILE2OBJ_INITEXTENDPROC", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1517:/ getarg");
 /*_.IPRO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IDATA__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IDATA__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.COMPICACHE__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.COMPICACHE__V5*/ meltfptr[4])) !=
	      NULL);


  /*getarg#4 */
  /*^getarg */
  if (meltxargdescr_[3] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PROCURMODENVLIST__V6*/ meltfptr[5] =
    (meltxargtab_[3].meltbp_aptr) ? (*(meltxargtab_[3].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr
	      ((melt_ptr_t) ( /*_.PROCURMODENVLIST__V6*/ meltfptr[5])) !=
	      NULL);


  /*getarg#5 */
  /*^getarg */
  if (meltxargdescr_[4] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPORTVALUES__V7*/ meltfptr[6] =
    (meltxargtab_[4].meltbp_aptr) ? (*(meltxargtab_[4].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPORTVALUES__V7*/ meltfptr[6]))
	      != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1518:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1518:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1518:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[16];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1518;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_initextendproc ipro=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.IPRO__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* modctx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODCTX__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n* idata=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.IDATA__V4*/ meltfptr[3];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n* compicache=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & /*_.COMPICACHE__V5*/ meltfptr[4];
	      /*^apply.arg */
	      argtab[11].meltbp_cstring = "\n* procurmodenvlist=";
	      /*^apply.arg */
	      argtab[12].meltbp_aptr =
		(melt_ptr_t *) & /*_.PROCURMODENVLIST__V6*/ meltfptr[5];
	      /*^apply.arg */
	      argtab[13].meltbp_cstring = "\n* importvalues=";
	      /*^apply.arg */
	      argtab[14].meltbp_aptr =
		(melt_ptr_t *) & /*_.IMPORTVALUES__V7*/ meltfptr[6];
	      /*^apply.arg */
	      argtab[15].meltbp_cstring =
		"\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n";
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1518:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1518:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[7] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1518:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1524:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.IPRO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_INITEXTENDPROC */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:1524:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1524:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ipro"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1524) ? (1524) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[8] = /*_.IFELSE___V13*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1524:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1525:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L4*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.IDATA__V4*/ meltfptr[3])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-genobj.melt:1525:/ cond");
      /*cond */ if ( /*_#IS_LIST__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1525:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check idata"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1525) ? (1525) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[9] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1525:/ clear");
	     /*clear *//*_#IS_LIST__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1526:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_RUNNING_EXTENSION_MODULE_CONTEXT */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:1526:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1526:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1526) ? (1526) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1526:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1527:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MAPOBJECT__L6*/ meltfnum[0] =
	/*is_mapobject: */
	(melt_magic_discr ((melt_ptr_t) ( /*_.COMPICACHE__V5*/ meltfptr[4]))
	 == MELTOBMAG_MAPOBJECTS);;
      MELT_LOCATION ("warmelt-genobj.melt:1527:/ cond");
      /*cond */ if ( /*_#IS_MAPOBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1527:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check compicache"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1527) ? (1527) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[16] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1527:/ clear");
	     /*clear *//*_#IS_MAPOBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1528:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L7*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.IMPORTVALUES__V7*/ meltfptr[6]))
	 == MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-genobj.melt:1528:/ cond");
      /*cond */ if ( /*_#IS_LIST__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V21*/ meltfptr[20] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1528:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check importvalues"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1528) ? (1528) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[18] = /*_.IFELSE___V21*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1528:/ clear");
	     /*clear *//*_#IS_LIST__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1529:/ quasiblock");


 /*_.LOCMAP__V23*/ meltfptr[22] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[3])),
	(50)));;
    /*^compute */
 /*_.OINIPROLOG__V24*/ meltfptr[23] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    /*^compute */
 /*_.OINIBODY__V25*/ meltfptr[24] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    MELT_LOCATION ("warmelt-genobj.melt:1532:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.IPRO__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_INITEXTENDPROC */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.IPRO__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 4, "NINITEXTEND_MODENV");
   /*_.MODENV__V26*/ meltfptr[25] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MODENV__V26*/ meltfptr[25] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1533:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.IPRO__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_ANYPROC */ meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.IPRO__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NPROC_BODY");
   /*_.NBODY__V27*/ meltfptr[26] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NBODY__V27*/ meltfptr[26] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1534:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[6])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "MOCX_MODULENAME");
   /*_.MODNAME__V28*/ meltfptr[27] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MODNAME__V28*/ meltfptr[27] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1535:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_RUNNING_EXTENSION_MODULE_CONTEXT */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 12, "MORCX_LITERVALIST");
   /*_.LITVALIST__V29*/ meltfptr[28] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LITVALIST__V29*/ meltfptr[28] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1537:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_INTEGERBOX__V30*/ meltfptr[29] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[9])),
	(0)));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V31*/ meltfptr[30] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[9])),
	(0)));;
    /*^compute */
 /*_.MAKE_LIST__V32*/ meltfptr[31] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V33*/ meltfptr[32] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[9])),
	(0)));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V34*/ meltfptr[33] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[9])),
	(0)));;
    /*^compute */
 /*_.MAKE_LIST__V35*/ meltfptr[34] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    MELT_LOCATION ("warmelt-genobj.melt:1551:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.MODCTX__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[6])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "MOCX_MODULENAME");
   /*_.MOCX_MODULENAME__V36*/ meltfptr[35] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MOCX_MODULENAME__V36*/ meltfptr[35] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1537:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_INITIAL_EXTENSION_ROUTINEOBJ */ meltfrout->tabval[7])), (14), "CLASS_INITIAL_EXTENSION_ROUTINEOBJ");
  /*_.INST__V38*/ meltfptr[37] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @NAMED_NAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (1),
			  (( /*!konst_8 */ meltfrout->tabval[8])),
			  "NAMED_NAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_PROC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (2),
			  ( /*_.IPRO__V2*/ meltfptr[1]), "OBROUT_PROC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_BODY",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (3),
			  ( /*_.OINIBODY__V25*/ meltfptr[24]), "OBROUT_BODY");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_NBVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (4),
			  ( /*_.MAKE_INTEGERBOX__V30*/ meltfptr[29]),
			  "OBROUT_NBVAL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_NBLONG",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (5),
			  ( /*_.MAKE_INTEGERBOX__V31*/ meltfptr[30]),
			  "OBROUT_NBLONG");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_OTHERS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (6),
			  ( /*_.MAKE_LIST__V32*/ meltfptr[31]),
			  "OBROUT_OTHERS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_CNTCITER",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (8),
			  ( /*_.MAKE_INTEGERBOX__V33*/ meltfptr[32]),
			  "OBROUT_CNTCITER");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBROUT_CNTLETREC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (9),
			  ( /*_.MAKE_INTEGERBOX__V34*/ meltfptr[33]),
			  "OBROUT_CNTLETREC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIROUT_FILL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (12),
			  ( /*_.MAKE_LIST__V35*/ meltfptr[34]),
			  "OIROUT_FILL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIROUT_PROLOG",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (11),
			  ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  "OIROUT_PROLOG");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIROUTX_EXTENDNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (13),
			  ( /*_.MOCX_MODULENAME__V36*/ meltfptr[35]),
			  "OIROUTX_EXTENDNAME");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V38*/ meltfptr[37],
				  "newly made instance");
    ;
    /*_.OINITXROUT__V37*/ meltfptr[36] = /*_.INST__V38*/ meltfptr[37];;
    /*^compute */
 /*_.IMPORTMAP__V39*/ meltfptr[38] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[3])),
	(10)));;
    /*^compute */
 /*_.VALOCLIST__V40*/ meltfptr[39] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    MELT_LOCATION ("warmelt-genobj.melt:1556:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_LIST__V41*/ meltfptr[40] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    /*^compute */
 /*_.MAKE_LIST__V42*/ meltfptr[41] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[4]))));;
    /*^compute */
 /*_.MAKE_MAPOBJECT__V43*/ meltfptr[42] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[3])),
	(10)));;
    /*^compute */
 /*_.MAKE_MAPOBJECT__V44*/ meltfptr[43] =
      (meltgc_new_mapobjects
       ((meltobject_ptr_t) (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[3])),
	(20)));;
    MELT_LOCATION ("warmelt-genobj.melt:1569:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V46*/ meltfptr[45] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_13 */ meltfrout->
						tabval[13])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V46*/ meltfptr[45])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V46*/ meltfptr[45])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V46*/ meltfptr[45])->tabval[0] =
      (melt_ptr_t) ( /*_.COMPICACHE__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V45*/ meltfptr[44] = /*_.LAMBDA___V46*/ meltfptr[45];;
    MELT_LOCATION ("warmelt-genobj.melt:1567:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V45*/ meltfptr[44];
      /*_.LIST_MAP__V47*/ meltfptr[46] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_MAP */ meltfrout->tabval[11])),
		    (melt_ptr_t) ( /*_.PROCURMODENVLIST__V6*/ meltfptr[5]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1556:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_EXTENSION_GENERATION_CONTEXT */ meltfrout->tabval[10])), (16), "CLASS_EXTENSION_GENERATION_CONTEXT");
  /*_.INST__V49*/ meltfptr[48] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_OBJROUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (0),
			  ( /*_.OINITXROUT__V37*/ meltfptr[36]),
			  "GNCX_OBJROUT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_LOCMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (1),
			  ( /*_.LOCMAP__V23*/ meltfptr[22]), "GNCX_LOCMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_FREEPTRLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (2),
			  ( /*_.MAKE_LIST__V41*/ meltfptr[40]),
			  "GNCX_FREEPTRLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_FREELONGLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (3),
			  ( /*_.MAKE_LIST__V42*/ meltfptr[41]),
			  "GNCX_FREELONGLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_FREEOTHERMAPS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (4),
			  ( /*_.MAKE_MAPOBJECT__V43*/ meltfptr[42]),
			  "GNCX_FREEOTHERMAPS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_COMPICACHE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (6),
			  ( /*_.COMPICACHE__V5*/ meltfptr[4]),
			  "GNCX_COMPICACHE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_MODULCONTEXT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (7),
			  ( /*_.MODCTX__V3*/ meltfptr[2]),
			  "GNCX_MODULCONTEXT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @GNCX_MATCHMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (8),
			  ( /*_.MAKE_MAPOBJECT__V44*/ meltfptr[43]),
			  "GNCX_MATCHMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @IGNCX_PROCURMODENVLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (12),
			  ( /*_.LIST_MAP__V47*/ meltfptr[46]),
			  "IGNCX_PROCURMODENVLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @IGNCX_IMPORTMAP",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (13),
			  ( /*_.IMPORTMAP__V39*/ meltfptr[38]),
			  "IGNCX_IMPORTMAP");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @EGNCX_VALOCLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V49*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V49*/ meltfptr[48]), (14),
			  ( /*_.VALOCLIST__V40*/ meltfptr[39]),
			  "EGNCX_VALOCLIST");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V49*/ meltfptr[48],
				  "newly made instance");
    ;
    /*_.GCX__V48*/ meltfptr[47] = /*_.INST__V49*/ meltfptr[48];;
    MELT_LOCATION ("warmelt-genobj.melt:1581:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_15_RETRUNINIT_ */ meltfrout->tabval[15]);
      /*_.RETI__V51*/ meltfptr[50] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.GCX__V48*/ meltfptr[47]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1582:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GCX__V48*/ meltfptr[47]),
					(melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */ meltfrout->tabval[16])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @GNCX_RETLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.GCX__V48*/ meltfptr[47])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.GCX__V48*/ meltfptr[47]), (5),
				( /*_.RETI__V51*/ meltfptr[50]),
				"GNCX_RETLOC");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.GCX__V48*/ meltfptr[47]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.GCX__V48*/ meltfptr[47],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1583:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OINITXROUT__V37*/ meltfptr[36]),
					(melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
						       meltfrout->
						       tabval[17])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBROUT_RETVAL",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.OINITXROUT__V37*/
					     meltfptr[36])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.OINITXROUT__V37*/ meltfptr[36]), (7),
				( /*_.RETI__V51*/ meltfptr[50]),
				"OBROUT_RETVAL");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.OINITXROUT__V37*/ meltfptr[36]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.OINITXROUT__V37*/ meltfptr[36],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V50*/ meltfptr[49] = /*_.RETI__V51*/ meltfptr[50];;

    MELT_LOCATION ("warmelt-genobj.melt:1581:/ clear");
	   /*clear *//*_.RETI__V51*/ meltfptr[50] = 0;
    /*_.RETINIT__V52*/ meltfptr[50] = /*_.LET___V50*/ meltfptr[49];;
    MELT_LOCATION ("warmelt-genobj.melt:1587:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_18_OCURENVBOXLOC_ */ meltfrout->
			  tabval[18]);
      /*_.BOXL__V54*/ meltfptr[53] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.GCX__V48*/ meltfptr[47]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1588:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GCX__V48*/ meltfptr[47]),
					(melt_ptr_t) (( /*!CLASS_INITIAL_GENERATION_CONTEXT */ meltfrout->tabval[19])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @IGNCX_CONTENVLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.GCX__V48*/ meltfptr[47])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.GCX__V48*/ meltfptr[47]), (11),
				( /*_.BOXL__V54*/ meltfptr[53]),
				"IGNCX_CONTENVLOC");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.GCX__V48*/ meltfptr[47]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.GCX__V48*/ meltfptr[47],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V53*/ meltfptr[52] = /*_.BOXL__V54*/ meltfptr[53];;

    MELT_LOCATION ("warmelt-genobj.melt:1587:/ clear");
	   /*clear *//*_.BOXL__V54*/ meltfptr[53] = 0;
    /*_.OCURENVBOXLOC__V55*/ meltfptr[53] = /*_.LET___V53*/ meltfptr[52];;
    MELT_LOCATION ("warmelt-genobj.melt:1592:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_20_OCURENVLOC_ */ meltfrout->tabval[20]);
      /*_.LOCL__V57*/ meltfptr[56] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.GCX__V48*/ meltfptr[47]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V56*/ meltfptr[55] = /*_.LOCL__V57*/ meltfptr[56];;

    MELT_LOCATION ("warmelt-genobj.melt:1592:/ clear");
	   /*clear *//*_.LOCL__V57*/ meltfptr[56] = 0;
    /*_.OCURENVLOC__V58*/ meltfptr[56] = /*_.LET___V56*/ meltfptr[55];;
    MELT_LOCATION ("warmelt-genobj.melt:1596:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_21_PREVENVLOC_ */ meltfrout->tabval[21]);
      /*_.BOXL__V60*/ meltfptr[59] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.GCX__V48*/ meltfptr[47]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1597:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GCX__V48*/ meltfptr[47]),
					(melt_ptr_t) (( /*!CLASS_INITIAL_GENERATION_CONTEXT */ meltfrout->tabval[19])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @IGNCX_PREVENVLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.GCX__V48*/ meltfptr[47])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.GCX__V48*/ meltfptr[47]), (10),
				( /*_.BOXL__V60*/ meltfptr[59]),
				"IGNCX_PREVENVLOC");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.GCX__V48*/ meltfptr[47]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.GCX__V48*/ meltfptr[47],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V59*/ meltfptr[58] = /*_.BOXL__V60*/ meltfptr[59];;

    MELT_LOCATION ("warmelt-genobj.melt:1596:/ clear");
	   /*clear *//*_.BOXL__V60*/ meltfptr[59] = 0;
    /*_.OPREVENVLOC__V61*/ meltfptr[59] = /*_.LET___V59*/ meltfptr[58];;
    MELT_LOCATION ("warmelt-genobj.melt:1601:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_22_OLITVALUETUPLOC_ */ meltfrout->
			  tabval[22]);
      /*_.BOXL__V63*/ meltfptr[62] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.GCX__V48*/ meltfptr[47]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1602:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GCX__V48*/ meltfptr[47]),
					(melt_ptr_t) (( /*!CLASS_EXTENSION_GENERATION_CONTEXT */ meltfrout->tabval[10])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @EGNCX_LITVALTUPLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.GCX__V48*/ meltfptr[47])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.GCX__V48*/ meltfptr[47]), (15),
				( /*_.BOXL__V63*/ meltfptr[62]),
				"EGNCX_LITVALTUPLOC");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.GCX__V48*/ meltfptr[47]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.GCX__V48*/ meltfptr[47],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V62*/ meltfptr[61] = /*_.BOXL__V63*/ meltfptr[62];;

    MELT_LOCATION ("warmelt-genobj.melt:1601:/ clear");
	   /*clear *//*_.BOXL__V63*/ meltfptr[62] = 0;
    /*_.OLITVALUETUPLOC__V64*/ meltfptr[62] = /*_.LET___V62*/ meltfptr[61];;
    MELT_LOCATION ("warmelt-genobj.melt:1612:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.IPRO__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_INITPROC */ meltfrout->tabval[23])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.IPRO__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "NINIT_DEFBINDS");
   /*_.INIDEFBINDS__V65*/ meltfptr[64] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.INIDEFBINDS__V65*/ meltfptr[64] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1614:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "start of oinibody";
      /*_.APPEND_COMMENTCONST__V66*/ meltfptr[65] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[24])),
		    (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1615:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1615:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1615:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1615;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc ocurenvboxloc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OCURENVBOXLOC__V55*/ meltfptr[53];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n inidefbinds=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIDEFBINDS__V65*/ meltfptr[64];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n modenv=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.MODENV__V26*/ meltfptr[25];
	      /*_.MELT_DEBUG_FUN__V69*/ meltfptr[68] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V68*/ meltfptr[67] =
	      /*_.MELT_DEBUG_FUN__V69*/ meltfptr[68];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1615:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V69*/ meltfptr[68] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V68*/ meltfptr[67] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1615:/ quasiblock");


      /*_.PROGN___V70*/ meltfptr[68] = /*_.IF___V68*/ meltfptr[67];;
      /*^compute */
      /*_.IFCPP___V67*/ meltfptr[66] = /*_.PROGN___V70*/ meltfptr[68];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1615:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V68*/ meltfptr[67] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V70*/ meltfptr[68] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V67*/ meltfptr[66] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.CURPAIR__V71*/ meltfptr[67] =
	   melt_list_first ((melt_ptr_t) /*_.INIDEFBINDS__V65*/ meltfptr[64]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V71*/ meltfptr[67]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V71*/ meltfptr[67] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V71*/ meltfptr[67]))
	{
	  /*_.CURDEFBIND__V72*/ meltfptr[68] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V71*/ meltfptr[67]);



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1621:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L10*/ meltfnum[1] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1621:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[0] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  /*^compute */
      /*_.DISCRIM__V75*/ meltfptr[74] =
		    ((melt_ptr_t)
		     (melt_discr
		      ((melt_ptr_t) ( /*_.CURDEFBIND__V72*/ meltfptr[68]))));;
		  MELT_LOCATION ("warmelt-genobj.melt:1621:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[0];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1621;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_initextendproc curdefbind=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURDEFBIND__V72*/ meltfptr[68];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " of discrim=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DISCRIM__V75*/ meltfptr[74];
		    /*_.MELT_DEBUG_FUN__V76*/ meltfptr[75] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V74*/ meltfptr[73] =
		    /*_.MELT_DEBUG_FUN__V76*/ meltfptr[75];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1621:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[0] = 0;
		  /*^clear */
		/*clear *//*_.DISCRIM__V75*/ meltfptr[74] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V76*/ meltfptr[75] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V74*/ meltfptr[73] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1621:/ quasiblock");


	    /*_.PROGN___V77*/ meltfptr[74] = /*_.IF___V74*/ meltfptr[73];;
	    /*^compute */
	    /*_.IFCPP___V73*/ meltfptr[72] = /*_.PROGN___V77*/ meltfptr[74];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1621:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[1] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V74*/ meltfptr[73] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V77*/ meltfptr[74] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V73*/ meltfptr[72] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1623:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURDEFBIND__V72*/
					       meltfptr[68]),
					      (melt_ptr_t) (( /*!CLASS_ANY_BINDING */ meltfrout->tabval[25])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURDEFBIND__V72*/ meltfptr[68]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 0, "BINDER");
    /*_.INIDEFSYMB__V78*/ meltfptr[75] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.INIDEFSYMB__V78*/ meltfptr[75] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1624:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURDEFBIND__V72*/
					       meltfptr[68]),
					      (melt_ptr_t) (( /*!CLASS_FIXED_BINDING */ meltfrout->tabval[26])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURDEFBIND__V72*/ meltfptr[68]) /*=obj*/
		  ;
		melt_object_get_field (slot, obj, 1, "FIXBIND_DATA");
    /*_.OINISYM__V79*/ meltfptr[73] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.OINISYM__V79*/ meltfptr[73] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1625:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.INIDEFSYMB__V78*/ meltfptr[75];
	    /*_.OINIDEFLOC__V80*/ meltfptr[74] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[14])),
			  (melt_ptr_t) ( /*_.GCX__V48*/ meltfptr[47]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1627:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L12*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1627:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[1] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  /*^compute */
      /*_.DISCRIM__V83*/ meltfptr[82] =
		    ((melt_ptr_t)
		     (melt_discr
		      ((melt_ptr_t) ( /*_.OINISYM__V79*/ meltfptr[73]))));;
		  MELT_LOCATION ("warmelt-genobj.melt:1627:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[11];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[1];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1627;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_initextendproc oinidefloc=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OINIDEFLOC__V80*/ meltfptr[74];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " oinisym=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OINISYM__V79*/ meltfptr[73];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = " of discrim:";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DISCRIM__V83*/ meltfptr[82];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = " curdefbind=";
		    /*^apply.arg */
		    argtab[10].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURDEFBIND__V72*/ meltfptr[68];
		    /*_.MELT_DEBUG_FUN__V84*/ meltfptr[83] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V82*/ meltfptr[81] =
		    /*_.MELT_DEBUG_FUN__V84*/ meltfptr[83];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1627:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[1] = 0;
		  /*^clear */
		/*clear *//*_.DISCRIM__V83*/ meltfptr[82] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V84*/ meltfptr[83] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V82*/ meltfptr[81] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1627:/ quasiblock");


	    /*_.PROGN___V85*/ meltfptr[82] = /*_.IF___V82*/ meltfptr[81];;
	    /*^compute */
	    /*_.IFCPP___V81*/ meltfptr[80] = /*_.PROGN___V85*/ meltfptr[82];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1627:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[0] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V82*/ meltfptr[81] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V85*/ meltfptr[82] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V81*/ meltfptr[80] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1632:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L14*/ meltfnum[1] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.OINISYM__V79*/ meltfptr[73]),
				   (melt_ptr_t) (( /*!CLASS_NREP_SIMPLE */
						  meltfrout->tabval[27])));;
	    MELT_LOCATION ("warmelt-genobj.melt:1632:/ cond");
	    /*cond */ if ( /*_#IS_A__L14*/ meltfnum[1])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V87*/ meltfptr[81] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-genobj.melt:1632:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check oinisym"),
					("warmelt-genobj.melt")
					? ("warmelt-genobj.melt") : __FILE__,
					(1632) ? (1632) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V87*/ meltfptr[81] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V86*/ meltfptr[83] = /*_.IFELSE___V87*/ meltfptr[81];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1632:/ clear");
	      /*clear *//*_#IS_A__L14*/ meltfnum[1] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V87*/ meltfptr[81] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V86*/ meltfptr[83] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1633:/ locexp");
	    meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				   ( /*_.LOCMAP__V23*/ meltfptr[22]),
				   (meltobject_ptr_t) ( /*_.CURDEFBIND__V72*/
						       meltfptr[68]),
				   (melt_ptr_t) ( /*_.OINIDEFLOC__V80*/
						 meltfptr[74]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:1623:/ clear");
	    /*clear *//*_.INIDEFSYMB__V78*/ meltfptr[75] = 0;
	  /*^clear */
	    /*clear *//*_.OINISYM__V79*/ meltfptr[73] = 0;
	  /*^clear */
	    /*clear *//*_.OINIDEFLOC__V80*/ meltfptr[74] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V81*/ meltfptr[80] = 0;
	  /*^clear */
	    /*clear *//*_.IFCPP___V86*/ meltfptr[83] = 0;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V71*/ meltfptr[67] = NULL;
     /*_.CURDEFBIND__V72*/ meltfptr[68] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-genobj.melt:1618:/ clear");
	    /*clear *//*_.CURPAIR__V71*/ meltfptr[67] = 0;
      /*^clear */
	    /*clear *//*_.CURDEFBIND__V72*/ meltfptr[68] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V73*/ meltfptr[72] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1635:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L15*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1635:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1635:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L16*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1635;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc updated locmap=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.LOCMAP__V23*/ meltfptr[22];
	      /*_.MELT_DEBUG_FUN__V90*/ meltfptr[75] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V89*/ meltfptr[81] =
	      /*_.MELT_DEBUG_FUN__V90*/ meltfptr[75];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1635:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V90*/ meltfptr[75] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V89*/ meltfptr[81] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1635:/ quasiblock");


      /*_.PROGN___V91*/ meltfptr[73] = /*_.IF___V89*/ meltfptr[81];;
      /*^compute */
      /*_.IFCPP___V88*/ meltfptr[82] = /*_.PROGN___V91*/ meltfptr[73];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1635:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V89*/ meltfptr[81] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V91*/ meltfptr[73] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V88*/ meltfptr[82] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1636:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:1640:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V93*/ meltfptr[80] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_33 */ meltfrout->
						tabval[33])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V93*/ meltfptr[80])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V93*/ meltfptr[80])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V93*/ meltfptr[80])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V48*/ meltfptr[47]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V93*/ meltfptr[80])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V93*/ meltfptr[80])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V93*/ meltfptr[80])->tabval[1] =
      (melt_ptr_t) ( /*_.IMPORTMAP__V39*/ meltfptr[38]);
    ;
    /*_.LAMBDA___V92*/ meltfptr[74] = /*_.LAMBDA___V93*/ meltfptr[80];;
    MELT_LOCATION ("warmelt-genobj.melt:1638:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V92*/ meltfptr[74];
      /*_.LIMPLOCV__V94*/ meltfptr[83] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_MAP */ meltfrout->tabval[11])),
		    (melt_ptr_t) ( /*_.IMPORTVALUES__V7*/ meltfptr[6]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1657:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L17*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1657:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1657:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L18*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1657;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc limplocv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.LIMPLOCV__V94*/ meltfptr[83];
	      /*_.MELT_DEBUG_FUN__V97*/ meltfptr[73] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V96*/ meltfptr[81] =
	      /*_.MELT_DEBUG_FUN__V97*/ meltfptr[73];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1657:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L18*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V97*/ meltfptr[73] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V96*/ meltfptr[81] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1657:/ quasiblock");


      /*_.PROGN___V98*/ meltfptr[73] = /*_.IF___V96*/ meltfptr[81];;
      /*^compute */
      /*_.IFCPP___V95*/ meltfptr[75] = /*_.PROGN___V98*/ meltfptr[73];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1657:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V96*/ meltfptr[81] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V98*/ meltfptr[73] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V95*/ meltfptr[75] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1658:/ locexp");
      /*void */ (void) 0;
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:1636:/ clear");
	   /*clear *//*_.LAMBDA___V92*/ meltfptr[74] = 0;
    /*^clear */
	   /*clear *//*_.LIMPLOCV__V94*/ meltfptr[83] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V95*/ meltfptr[75] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1664:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!CTYPE_VOID */ meltfrout->tabval[35]);
      /*^apply.arg */
      argtab[1].meltbp_cstring = "/*initextendproc get boxcurenv*/ ";
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OCURENVBOXLOC__V55*/ meltfptr[53];
      /*^apply.arg */
      argtab[3].meltbp_cstring = "\n\t\t  = meltarg_curenvbox_p ;";
      /*_.MAKE_OBJLOCATEDEXP__V99*/ meltfptr[81] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MAKE_OBJLOCATEDEXP */ meltfrout->tabval[34])),
		    (melt_ptr_t) (( /*nil */ NULL)),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1663:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (melt_ptr_t) ( /*_.MAKE_OBJLOCATEDEXP__V99*/
					meltfptr[81]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1669:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L19*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1669:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1669:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L20*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1669;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = " litvalist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.LITVALIST__V29*/ meltfptr[28];
	      /*_.MELT_DEBUG_FUN__V102*/ meltfptr[83] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V101*/ meltfptr[74] =
	      /*_.MELT_DEBUG_FUN__V102*/ meltfptr[83];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1669:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V102*/ meltfptr[83] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V101*/ meltfptr[74] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1669:/ quasiblock");


      /*_.PROGN___V103*/ meltfptr[75] = /*_.IF___V101*/ meltfptr[74];;
      /*^compute */
      /*_.IFCPP___V100*/ meltfptr[73] = /*_.PROGN___V103*/ meltfptr[75];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1669:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V101*/ meltfptr[74] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V103*/ meltfptr[75] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V100*/ meltfptr[73] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1670:/ quasiblock");


 /*_#NBLITVAL__L21*/ meltfnum[1] =
      (melt_list_length ((melt_ptr_t) ( /*_.LITVALIST__V29*/ meltfptr[28])));;
    MELT_LOCATION ("warmelt-genobj.melt:1673:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[14];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!CTYPE_VOID */ meltfrout->tabval[35]);
      /*^apply.arg */
      argtab[1].meltbp_cstring = "/*initextendproc get litvaluetup*/ ";
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.OLITVALUETUPLOC__V64*/ meltfptr[62];
      /*^apply.arg */
      argtab[3].meltbp_cstring = " \n\t\t    = meltarg_tuplitval_p ;\
\n\t\t    if (melt_multiple_length((melt_ptr_t) ";
      /*^apply.arg */
      argtab[4].meltbp_aptr =
	(melt_ptr_t *) & /*_.OLITVALUETUPLOC__V64*/ meltfptr[62];
      /*^apply.arg */
      argtab[5].meltbp_cstring = ") !=\n\t\t\t\t\t    ";
      /*^apply.arg */
      argtab[6].meltbp_long = /*_#NBLITVAL__L21*/ meltfnum[1];
      /*^apply.arg */
      argtab[7].meltbp_cstring =
	")\n\t\t    melt_fatal_error (\"bad runtime extension literal value\
 tuple@%p in \" \n\t\t\t\t      ";
      /*^apply.arg */
      argtab[8].meltbp_aptr =
	(melt_ptr_t *) & /*_.MODNAME__V28*/ meltfptr[27];
      /*^apply.arg */
      argtab[9].meltbp_cstring = " \", wants ";
      /*^apply.arg */
      argtab[10].meltbp_long = /*_#NBLITVAL__L21*/ meltfnum[1];
      /*^apply.arg */
      argtab[11].meltbp_cstring = " components\", \n\t\t\t\t      (void*) ";
      /*^apply.arg */
      argtab[12].meltbp_aptr =
	(melt_ptr_t *) & /*_.OLITVALUETUPLOC__V64*/ meltfptr[62];
      /*^apply.arg */
      argtab[13].meltbp_cstring = ")\t;\n\t\t    ";
      /*_.MAKE_OBJLOCATEDEXP__V105*/ meltfptr[74] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MAKE_OBJLOCATEDEXP */ meltfrout->tabval[34])),
		    (melt_ptr_t) (( /*nil */ NULL)),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1672:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (melt_ptr_t) ( /*_.MAKE_OBJLOCATEDEXP__V105*/
					meltfptr[74]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1685:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1686:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct meltpair_st rpair_0__OCURENVLOC_x1;
	struct meltlist_st rlist_1__LIST_;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inipair rpair_0__OCURENVLOC_x1 */
   /*_.OCURENVLOC__V107*/ meltfptr[106] =
	(melt_ptr_t) & meltletrec_1_ptr->rpair_0__OCURENVLOC_x1;
      meltletrec_1_ptr->rpair_0__OCURENVLOC_x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

      /*inilist rlist_1__LIST_ */
   /*_.LIST___V108*/ meltfptr[107] =
	(melt_ptr_t) & meltletrec_1_ptr->rlist_1__LIST_;
      meltletrec_1_ptr->rlist_1__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*^putpairhead */
      /*putpairhead */
      melt_assertmsg ("putpairhead /5 checkpair",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.OCURENVLOC__V107*/
					 meltfptr[106])) == MELTOBMAG_PAIR);
      ((meltpair_ptr_t) ( /*_.OCURENVLOC__V107*/ meltfptr[106]))->hd =
	(melt_ptr_t) ( /*_.OCURENVLOC__V58*/ meltfptr[56]);
      ;
      /*^touch */
      meltgc_touch ( /*_.OCURENVLOC__V107*/ meltfptr[106]);
      ;
      /*^putlist */
      /*putlist */
      melt_assertmsg ("putlist checklist",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.LIST___V108*/ meltfptr[107])) ==
		      MELTOBMAG_LIST);
      ((meltlist_ptr_t) ( /*_.LIST___V108*/ meltfptr[107]))->first =
	(meltpair_ptr_t) ( /*_.OCURENVLOC__V107*/ meltfptr[106]);
      ((meltlist_ptr_t) ( /*_.LIST___V108*/ meltfptr[107]))->last =
	(meltpair_ptr_t) ( /*_.OCURENVLOC__V107*/ meltfptr[106]);
      ;
      /*^touch */
      meltgc_touch ( /*_.LIST___V108*/ meltfptr[107]);
      ;
      /*_.LIST___V106*/ meltfptr[75] = /*_.LIST___V108*/ meltfptr[107];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1686:/ clear");
	    /*clear *//*_.OCURENVLOC__V107*/ meltfptr[106] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V108*/ meltfptr[107] = 0;
      /*^clear */
	    /*clear *//*_.OCURENVLOC__V107*/ meltfptr[106] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V108*/ meltfptr[107] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1685:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJGETSLOT */
					     meltfrout->tabval[36])), (4),
			      "CLASS_OBJGETSLOT");
  /*_.INST__V110*/ meltfptr[107] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V110*/ meltfptr[107])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V110*/ meltfptr[107]), (1),
			  ( /*_.LIST___V106*/ meltfptr[75]), "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OGETSL_OBJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V110*/ meltfptr[107])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V110*/ meltfptr[107]), (2),
			  ( /*_.OCURENVBOXLOC__V55*/ meltfptr[53]),
			  "OGETSL_OBJ");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OGETSL_FIELD",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V110*/ meltfptr[107])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V110*/ meltfptr[107]), (3),
			  (( /*!REFERENCED_VALUE */ meltfrout->tabval[37])),
			  "OGETSL_FIELD");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V110*/ meltfptr[107],
				  "newly made instance");
    ;
    /*_.INST___V109*/ meltfptr[106] = /*_.INST__V110*/ meltfptr[107];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1684:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (melt_ptr_t) ( /*_.INST___V109*/ meltfptr[106]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1691:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1692:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_2_st
      {
	struct meltpair_st rpair_0__OPREVENVLOC_x1;
	struct meltlist_st rlist_1__LIST_;
	long meltletrec_2_endgap;
      } *meltletrec_2_ptr = 0;
      meltletrec_2_ptr =
	(struct meltletrec_2_st *)
	meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
      /*^blockmultialloc.initfill */
      /*inipair rpair_0__OPREVENVLOC_x1 */
   /*_.OPREVENVLOC__V112*/ meltfptr[111] =
	(melt_ptr_t) & meltletrec_2_ptr->rpair_0__OPREVENVLOC_x1;
      meltletrec_2_ptr->rpair_0__OPREVENVLOC_x1.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_PAIR))));

      /*inilist rlist_1__LIST_ */
   /*_.LIST___V113*/ meltfptr[112] =
	(melt_ptr_t) & meltletrec_2_ptr->rlist_1__LIST_;
      meltletrec_2_ptr->rlist_1__LIST_.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_LIST))));



      /*^putpairhead */
      /*putpairhead */
      melt_assertmsg ("putpairhead /6 checkpair",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.OPREVENVLOC__V112*/
					 meltfptr[111])) == MELTOBMAG_PAIR);
      ((meltpair_ptr_t) ( /*_.OPREVENVLOC__V112*/ meltfptr[111]))->hd =
	(melt_ptr_t) ( /*_.OPREVENVLOC__V61*/ meltfptr[59]);
      ;
      /*^touch */
      meltgc_touch ( /*_.OPREVENVLOC__V112*/ meltfptr[111]);
      ;
      /*^putlist */
      /*putlist */
      melt_assertmsg ("putlist checklist",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.LIST___V113*/ meltfptr[112])) ==
		      MELTOBMAG_LIST);
      ((meltlist_ptr_t) ( /*_.LIST___V113*/ meltfptr[112]))->first =
	(meltpair_ptr_t) ( /*_.OPREVENVLOC__V112*/ meltfptr[111]);
      ((meltlist_ptr_t) ( /*_.LIST___V113*/ meltfptr[112]))->last =
	(meltpair_ptr_t) ( /*_.OPREVENVLOC__V112*/ meltfptr[111]);
      ;
      /*^touch */
      meltgc_touch ( /*_.LIST___V113*/ meltfptr[112]);
      ;
      /*_.LIST___V111*/ meltfptr[110] = /*_.LIST___V113*/ meltfptr[112];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1692:/ clear");
	    /*clear *//*_.OPREVENVLOC__V112*/ meltfptr[111] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V113*/ meltfptr[112] = 0;
      /*^clear */
	    /*clear *//*_.OPREVENVLOC__V112*/ meltfptr[111] = 0;
      /*^clear */
	    /*clear *//*_.LIST___V113*/ meltfptr[112] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1691:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJGETSLOT */
					     meltfrout->tabval[36])), (4),
			      "CLASS_OBJGETSLOT");
  /*_.INST__V115*/ meltfptr[112] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V115*/ meltfptr[112])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V115*/ meltfptr[112]), (1),
			  ( /*_.LIST___V111*/ meltfptr[110]),
			  "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OGETSL_OBJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V115*/ meltfptr[112])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V115*/ meltfptr[112]), (2),
			  ( /*_.OCURENVLOC__V58*/ meltfptr[56]),
			  "OGETSL_OBJ");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OGETSL_FIELD",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V115*/ meltfptr[112])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V115*/ meltfptr[112]), (3),
			  (( /*!ENV_PREV */ meltfrout->tabval[38])),
			  "OGETSL_FIELD");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V115*/ meltfptr[112],
				  "newly made instance");
    ;
    /*_.INST___V114*/ meltfptr[111] = /*_.INST__V115*/ meltfptr[112];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1690:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (melt_ptr_t) ( /*_.INST___V114*/ meltfptr[111]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1696:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L22*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1696:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1696:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1696;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc before body oiniprolog=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OINIPROLOG__V24*/ meltfptr[23];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n idata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.IDATA__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V118*/ meltfptr[117] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V117*/ meltfptr[116] =
	      /*_.MELT_DEBUG_FUN__V118*/ meltfptr[117];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1696:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V118*/ meltfptr[117] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V117*/ meltfptr[116] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1696:/ quasiblock");


      /*_.PROGN___V119*/ meltfptr[117] = /*_.IF___V117*/ meltfptr[116];;
      /*^compute */
      /*_.IFCPP___V116*/ meltfptr[115] = /*_.PROGN___V119*/ meltfptr[117];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1696:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V117*/ meltfptr[116] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V119*/ meltfptr[117] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V116*/ meltfptr[115] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1700:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:1704:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V122*/ meltfptr[121] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_43 */ meltfrout->
						tabval[43])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V122*/ meltfptr[121])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V122*/ meltfptr[121])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V122*/ meltfptr[121])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V48*/ meltfptr[47]);
    ;
    /*_.LAMBDA___V121*/ meltfptr[117] = /*_.LAMBDA___V122*/ meltfptr[121];;
    MELT_LOCATION ("warmelt-genobj.melt:1702:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[40]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V121*/ meltfptr[117];
      /*_.ODATATUP__V123*/ meltfptr[122] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[39])),
		    (melt_ptr_t) ( /*_.IDATA__V4*/ meltfptr[3]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1708:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.IPRO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NINIT_TOPL");
  /*_.TOPLIS__V124*/ meltfptr[123] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1710:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OINITXROUT__V37*/ meltfptr[36]),
					(melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */ meltfrout->tabval[44])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OIROUT_DATA",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.OINITXROUT__V37*/
					     meltfptr[36])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.OINITXROUT__V37*/ meltfptr[36]), (10),
				( /*_.ODATATUP__V123*/ meltfptr[122]),
				"OIROUT_DATA");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.OINITXROUT__V37*/ meltfptr[36]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.OINITXROUT__V37*/ meltfptr[36],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1712:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L24*/ meltfnum[22] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1712:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L24*/ meltfnum[22])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L25*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1712:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L25*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1712;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_initextendproc toplis=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.TOPLIS__V124*/ meltfptr[123];
	      /*_.MELT_DEBUG_FUN__V127*/ meltfptr[126] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V126*/ meltfptr[125] =
	      /*_.MELT_DEBUG_FUN__V127*/ meltfptr[126];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1712:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L25*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V127*/ meltfptr[126] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V126*/ meltfptr[125] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1712:/ quasiblock");


      /*_.PROGN___V128*/ meltfptr[126] = /*_.IF___V126*/ meltfptr[125];;
      /*^compute */
      /*_.IFCPP___V125*/ meltfptr[124] = /*_.PROGN___V128*/ meltfptr[126];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1712:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L24*/ meltfnum[22] = 0;
      /*^clear */
	     /*clear *//*_.IF___V126*/ meltfptr[125] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V128*/ meltfptr[126] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V125*/ meltfptr[124] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1713:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST_OR_NULL__L26*/ meltfnum[0] =
	(( /*_.TOPLIS__V124*/ meltfptr[123]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.TOPLIS__V124*/ meltfptr[123])) ==
	  MELTOBMAG_LIST));;
      MELT_LOCATION ("warmelt-genobj.melt:1713:/ cond");
      /*cond */ if ( /*_#IS_LIST_OR_NULL__L26*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V130*/ meltfptr[126] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1713:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check toplis"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1713) ? (1713) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V130*/ meltfptr[126] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V129*/ meltfptr[125] = /*_.IFELSE___V130*/ meltfptr[126];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1713:/ clear");
	     /*clear *//*_#IS_LIST_OR_NULL__L26*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V130*/ meltfptr[126] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V129*/ meltfptr[125] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1715:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:1718:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V133*/ meltfptr[132] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_47 */ meltfrout->
						tabval[47])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V133*/ meltfptr[132])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V133*/ meltfptr[132])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V133*/ meltfptr[132])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V48*/ meltfptr[47]);
    ;
    /*_.LAMBDA___V132*/ meltfptr[131] = /*_.LAMBDA___V133*/ meltfptr[132];;
    MELT_LOCATION ("warmelt-genobj.melt:1716:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V132*/ meltfptr[131];
      /*_.OBJTOPLIS__V134*/ meltfptr[133] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_MAP */ meltfrout->tabval[11])),
		    (melt_ptr_t) ( /*_.TOPLIS__V124*/ meltfptr[123]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1724:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L27*/ meltfnum[22] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1724:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L27*/ meltfnum[22])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L28*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1724:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L28*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1724;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc objtoplis=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBJTOPLIS__V134*/ meltfptr[133];
	      /*_.MELT_DEBUG_FUN__V137*/ meltfptr[136] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V136*/ meltfptr[135] =
	      /*_.MELT_DEBUG_FUN__V137*/ meltfptr[136];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1724:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L28*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V137*/ meltfptr[136] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V136*/ meltfptr[135] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1724:/ quasiblock");


      /*_.PROGN___V138*/ meltfptr[136] = /*_.IF___V136*/ meltfptr[135];;
      /*^compute */
      /*_.IFCPP___V135*/ meltfptr[134] = /*_.PROGN___V138*/ meltfptr[136];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1724:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L27*/ meltfnum[22] = 0;
      /*^clear */
	     /*clear *//*_.IF___V136*/ meltfptr[135] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V138*/ meltfptr[136] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V135*/ meltfptr[134] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1726:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "get symbols & keywords";
      /*_.APPEND_COMMENTCONST__V139*/ meltfptr[135] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[24])),
		    (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1729:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V141*/ meltfptr[140] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_55 */ meltfrout->
						tabval[55])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V141*/ meltfptr[140])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V141*/ meltfptr[140])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V141*/ meltfptr[140])->tabval[0] =
      (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]);
    ;
    /*_.LAMBDA___V140*/ meltfptr[136] = /*_.LAMBDA___V141*/ meltfptr[140];;
    MELT_LOCATION ("warmelt-genobj.melt:1727:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V140*/ meltfptr[136];
      /*_.MULTIPLE_EVERY__V142*/ meltfptr[141] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[48])),
		    (melt_ptr_t) ( /*_.ODATATUP__V123*/ meltfptr[122]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1751:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L29*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1751:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L29*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L30*/ meltfnum[22] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1751:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L30*/ meltfnum[22];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1751;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc before imports oiniprolog=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OINIPROLOG__V24*/ meltfptr[23];
	      /*_.MELT_DEBUG_FUN__V145*/ meltfptr[144] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V144*/ meltfptr[143] =
	      /*_.MELT_DEBUG_FUN__V145*/ meltfptr[144];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1751:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L30*/ meltfnum[22] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V145*/ meltfptr[144] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V144*/ meltfptr[143] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1751:/ quasiblock");


      /*_.PROGN___V146*/ meltfptr[144] = /*_.IF___V144*/ meltfptr[143];;
      /*^compute */
      /*_.IFCPP___V143*/ meltfptr[142] = /*_.PROGN___V146*/ meltfptr[144];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1751:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L29*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V144*/ meltfptr[143] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V146*/ meltfptr[144] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V143*/ meltfptr[142] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1753:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#LIST_LENGTH__L31*/ meltfnum[22] =
      (melt_list_length
       ((melt_ptr_t) ( /*_.IMPORTVALUES__V7*/ meltfptr[6])));;
    /*^compute */
 /*_#I__L32*/ meltfnum[0] =
      (( /*_#LIST_LENGTH__L31*/ meltfnum[22]) > (0));;
    MELT_LOCATION ("warmelt-genobj.melt:1753:/ cond");
    /*cond */ if ( /*_#I__L32*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1755:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "before getting imported values";
	    /*_.APPEND_COMMENTCONST__V148*/ meltfptr[144] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!APPEND_COMMENTCONST */ meltfrout->
			    tabval[24])),
			  (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1759:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V150*/ meltfptr[149] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_65 */
						      meltfrout->tabval[65])),
				(4));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V150*/
					     meltfptr[149])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V150*/
					      meltfptr[149])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V150*/ meltfptr[149])->tabval[0] =
	    (melt_ptr_t) ( /*_.MODCTX__V3*/ meltfptr[2]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V150*/
					     meltfptr[149])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 1 >= 0
			  && 1 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V150*/
					      meltfptr[149])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V150*/ meltfptr[149])->tabval[1] =
	    (melt_ptr_t) ( /*_.IMPORTMAP__V39*/ meltfptr[38]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V150*/
					     meltfptr[149])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 2 >= 0
			  && 2 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V150*/
					      meltfptr[149])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V150*/ meltfptr[149])->tabval[2] =
	    (melt_ptr_t) ( /*_.OLITVALUETUPLOC__V64*/ meltfptr[62]);
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V150*/
					     meltfptr[149])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 3 >= 0
			  && 3 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V150*/
					      meltfptr[149])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V150*/ meltfptr[149])->tabval[3] =
	    (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]);
	  ;
	  /*_.LAMBDA___V149*/ meltfptr[148] =
	    /*_.LAMBDA___V150*/ meltfptr[149];;
	  MELT_LOCATION ("warmelt-genobj.melt:1757:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V149*/ meltfptr[148];
	    /*_.LIST_EVERY__V151*/ meltfptr[150] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[56])),
			  (melt_ptr_t) ( /*_.IMPORTVALUES__V7*/ meltfptr[6]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1785:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "after getting imported values";
	    /*_.APPEND_COMMENTCONST__V152*/ meltfptr[151] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!APPEND_COMMENTCONST */ meltfrout->
			    tabval[24])),
			  (melt_ptr_t) ( /*_.OINIPROLOG__V24*/ meltfptr[23]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1754:/ quasiblock");


	  /*_.PROGN___V153*/ meltfptr[152] =
	    /*_.APPEND_COMMENTCONST__V152*/ meltfptr[151];;
	  /*^compute */
	  /*_.IF___V147*/ meltfptr[143] = /*_.PROGN___V153*/ meltfptr[152];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1753:/ clear");
	     /*clear *//*_.APPEND_COMMENTCONST__V148*/ meltfptr[144] = 0;
	  /*^clear */
	     /*clear *//*_.LAMBDA___V149*/ meltfptr[148] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V151*/ meltfptr[150] = 0;
	  /*^clear */
	     /*clear *//*_.APPEND_COMMENTCONST__V152*/ meltfptr[151] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V153*/ meltfptr[152] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V147*/ meltfptr[143] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1788:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "before toplevel body";
      /*_.APPEND_COMMENTCONST__V154*/ meltfptr[144] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[24])),
		    (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1789:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L33*/ meltfnum[32] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1789:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L33*/ meltfnum[32])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L34*/ meltfnum[33] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1789:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L34*/ meltfnum[33];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1789;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc objtoplis=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBJTOPLIS__V134*/ meltfptr[133];
	      /*_.MELT_DEBUG_FUN__V157*/ meltfptr[151] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V156*/ meltfptr[150] =
	      /*_.MELT_DEBUG_FUN__V157*/ meltfptr[151];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1789:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L34*/ meltfnum[33] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V157*/ meltfptr[151] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V156*/ meltfptr[150] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1789:/ quasiblock");


      /*_.PROGN___V158*/ meltfptr[152] = /*_.IF___V156*/ meltfptr[150];;
      /*^compute */
      /*_.IFCPP___V155*/ meltfptr[148] = /*_.PROGN___V158*/ meltfptr[152];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1789:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L33*/ meltfnum[32] = 0;
      /*^clear */
	     /*clear *//*_.IF___V156*/ meltfptr[150] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V158*/ meltfptr[152] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V155*/ meltfptr[148] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit2__EACHLIST */
      for ( /*_.CURPAIR__V159*/ meltfptr[151] =
	   melt_list_first ((melt_ptr_t) /*_.OBJTOPLIS__V134*/ meltfptr[133]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V159*/ meltfptr[151])
	   == MELTOBMAG_PAIR;
	   /*_.CURPAIR__V159*/ meltfptr[151] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V159*/ meltfptr[151]))
	{
	  /*_.CUROBJT__V160*/ meltfptr[150] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V159*/ meltfptr[151]);



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1793:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L35*/ meltfnum[33] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1793:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L35*/ meltfnum[33])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L36*/ meltfnum[32] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1793:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L36*/ meltfnum[32];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1793;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_initextendproc appendinf curobjt=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CUROBJT__V160*/ meltfptr[150];
		    /*_.MELT_DEBUG_FUN__V163*/ meltfptr[162] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V162*/ meltfptr[161] =
		    /*_.MELT_DEBUG_FUN__V163*/ meltfptr[162];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1793:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L36*/ meltfnum[32] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V163*/ meltfptr[162] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V162*/ meltfptr[161] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1793:/ quasiblock");


	    /*_.PROGN___V164*/ meltfptr[162] = /*_.IF___V162*/ meltfptr[161];;
	    /*^compute */
	    /*_.IFCPP___V161*/ meltfptr[152] =
	      /*_.PROGN___V164*/ meltfptr[162];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1793:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L35*/ meltfnum[33] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V162*/ meltfptr[161] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V164*/ meltfptr[162] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V161*/ meltfptr[152] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1794:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.OINIBODY__V25*/ meltfptr[24]),
				(melt_ptr_t) ( /*_.CUROBJT__V160*/
					      meltfptr[150]));
	  }
	  ;
	}			/* end foreach_in_list meltcit2__EACHLIST */
     /*_.CURPAIR__V159*/ meltfptr[151] = NULL;
     /*_.CUROBJT__V160*/ meltfptr[150] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-genobj.melt:1790:/ clear");
	    /*clear *//*_.CURPAIR__V159*/ meltfptr[151] = 0;
      /*^clear */
	    /*clear *//*_.CUROBJT__V160*/ meltfptr[150] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V161*/ meltfptr[152] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1795:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L37*/ meltfnum[32] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1795:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L37*/ meltfnum[32])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L38*/ meltfnum[33] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1795:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L38*/ meltfnum[33];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1795;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc updated oinibody=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OINIBODY__V25*/ meltfptr[24];
	      /*_.MELT_DEBUG_FUN__V167*/ meltfptr[166] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V166*/ meltfptr[162] =
	      /*_.MELT_DEBUG_FUN__V167*/ meltfptr[166];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1795:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L38*/ meltfnum[33] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V167*/ meltfptr[166] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V166*/ meltfptr[162] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1795:/ quasiblock");


      /*_.PROGN___V168*/ meltfptr[166] = /*_.IF___V166*/ meltfptr[162];;
      /*^compute */
      /*_.IFCPP___V165*/ meltfptr[161] = /*_.PROGN___V168*/ meltfptr[166];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1795:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L37*/ meltfnum[32] = 0;
      /*^clear */
	     /*clear *//*_.IF___V166*/ meltfptr[162] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V168*/ meltfptr[166] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V165*/ meltfptr[161] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1796:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L39*/ meltfnum[33] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1796:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L39*/ meltfnum[33])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L40*/ meltfnum[32] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1796:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L40*/ meltfnum[32];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1796;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_initextendproc nbody=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NBODY__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V171*/ meltfptr[170] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V170*/ meltfptr[166] =
	      /*_.MELT_DEBUG_FUN__V171*/ meltfptr[170];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1796:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L40*/ meltfnum[32] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V171*/ meltfptr[170] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V170*/ meltfptr[166] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1796:/ quasiblock");


      /*_.PROGN___V172*/ meltfptr[170] = /*_.IF___V170*/ meltfptr[166];;
      /*^compute */
      /*_.IFCPP___V169*/ meltfptr[162] = /*_.PROGN___V172*/ meltfptr[170];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1796:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L39*/ meltfnum[33] = 0;
      /*^clear */
	     /*clear *//*_.IF___V170*/ meltfptr[166] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V172*/ meltfptr[170] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V169*/ meltfptr[162] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit3__EACHLIST */
      for ( /*_.CURNPAIR__V173*/ meltfptr[166] =
	   melt_list_first ((melt_ptr_t) /*_.NBODY__V27*/ meltfptr[26]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURNPAIR__V173*/ meltfptr[166])
	   == MELTOBMAG_PAIR;
	   /*_.CURNPAIR__V173*/ meltfptr[166] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURNPAIR__V173*/ meltfptr[166]))
	{
	  /*_.CURNINST__V174*/ meltfptr[170] =
	    melt_pair_head ((melt_ptr_t) /*_.CURNPAIR__V173*/ meltfptr[166]);



#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1800:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L41*/ meltfnum[32] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1800:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L41*/ meltfnum[32])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L42*/ meltfnum[33] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1800:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L42*/ meltfnum[33];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1800;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_initextendproc curninst=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURNINST__V174*/ meltfptr[170];
		    /*_.MELT_DEBUG_FUN__V177*/ meltfptr[176] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V176*/ meltfptr[175] =
		    /*_.MELT_DEBUG_FUN__V177*/ meltfptr[176];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1800:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L42*/ meltfnum[33] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V177*/ meltfptr[176] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V176*/ meltfptr[175] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1800:/ quasiblock");


	    /*_.PROGN___V178*/ meltfptr[176] = /*_.IF___V176*/ meltfptr[175];;
	    /*^compute */
	    /*_.IFCPP___V175*/ meltfptr[174] =
	      /*_.PROGN___V178*/ meltfptr[176];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1800:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L41*/ meltfnum[32] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V176*/ meltfptr[175] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V178*/ meltfptr[176] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V175*/ meltfptr[174] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1801:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_NOT_A__L43*/ meltfnum[33] =
	    !melt_is_instance_of ((melt_ptr_t)
				  ( /*_.CURNINST__V174*/ meltfptr[170]),
				  (melt_ptr_t) (( /*!CLASS_NREP_ROUTPROC */
						 meltfrout->tabval[66])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1801:/ cond");
	  /*cond */ if ( /*_#IS_NOT_A__L43*/ meltfnum[33])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1802:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.GCX__V48*/ meltfptr[47];
		  /*_.CUROBJB__V179*/ meltfptr[175] =
		    meltgc_send ((melt_ptr_t)
				 ( /*_.CURNINST__V174*/ meltfptr[170]),
				 (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
						tabval[67])),
				 (MELTBPARSTR_PTR ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:1804:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L44*/ meltfnum[32] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1804:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L44*/ meltfnum[32])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L45*/ meltfnum[44] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1804:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L45*/ meltfnum[44];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-genobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 1804;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "compile2obj_initextendproc curobjb=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CUROBJB__V179*/
			    meltfptr[175];
			  /*_.MELT_DEBUG_FUN__V182*/ meltfptr[181] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V181*/ meltfptr[180] =
			  /*_.MELT_DEBUG_FUN__V182*/ meltfptr[181];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1804:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L45*/ meltfnum[44] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V182*/ meltfptr[181] =
			  0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V181*/ meltfptr[180] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-genobj.melt:1804:/ quasiblock");


		  /*_.PROGN___V183*/ meltfptr[181] =
		    /*_.IF___V181*/ meltfptr[180];;
		  /*^compute */
		  /*_.IFCPP___V180*/ meltfptr[176] =
		    /*_.PROGN___V183*/ meltfptr[181];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1804:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L44*/ meltfnum[32] = 0;
		  /*^clear */
		/*clear *//*_.IF___V181*/ meltfptr[180] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V183*/ meltfptr[181] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V180*/ meltfptr[176] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-genobj.melt:1805:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
    /*_#IS_A__L46*/ meltfnum[44] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.CUROBJB__V179*/ meltfptr[175]),
				       (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */ meltfrout->tabval[68])));;
		MELT_LOCATION ("warmelt-genobj.melt:1805:/ cond");
		/*cond */ if ( /*_#IS_A__L46*/ meltfnum[44])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-genobj.melt:1806:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L47*/ meltfnum[32] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-genobj.melt:1806:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L47*/ meltfnum[32])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L48*/ meltfnum[47] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-genobj.melt:1806:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L48*/ meltfnum[47];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-genobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1806;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "compile2obj_initextendproc skipping pure curobjb=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CUROBJB__V179*/
				  meltfptr[175];
				/*_.MELT_DEBUG_FUN__V186*/ meltfptr[185] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V185*/ meltfptr[181] =
				/*_.MELT_DEBUG_FUN__V186*/ meltfptr[185];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-genobj.melt:1806:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L48*/
				meltfnum[47] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V186*/
				meltfptr[185] = 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V185*/ meltfptr[181] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1806:/ quasiblock");


			/*_.PROGN___V187*/ meltfptr[185] =
			  /*_.IF___V185*/ meltfptr[181];;
			/*^compute */
			/*_.IFCPP___V184*/ meltfptr[180] =
			  /*_.PROGN___V187*/ meltfptr[185];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1806:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L47*/ meltfnum[32] = 0;
			/*^clear */
		  /*clear *//*_.IF___V185*/ meltfptr[181] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V187*/ meltfptr[185] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V184*/ meltfptr[180] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1807:/ locexp");
			/*void */ (void) 0;
		      }
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1805:/ quasiblock");


		      /*epilog */

		      /*^clear */
		/*clear *//*_.IFCPP___V184*/ meltfptr[180] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-genobj.melt:1810:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	/*_#MELT_NEED_DBG__L49*/ meltfnum[47] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-genobj.melt:1810:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L49*/ meltfnum[47])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	  /*_#THE_MELTCALLCOUNT__L50*/ meltfnum[32] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-genobj.melt:1810:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L50*/ meltfnum[32];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-genobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1810;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "compile2obj_initextendproc appending curobjb=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CUROBJB__V179*/
				  meltfptr[175];
				/*_.MELT_DEBUG_FUN__V190*/ meltfptr[180] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V189*/ meltfptr[185] =
				/*_.MELT_DEBUG_FUN__V190*/ meltfptr[180];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-genobj.melt:1810:/ clear");
		    /*clear *//*_#THE_MELTCALLCOUNT__L50*/
				meltfnum[32] = 0;
			      /*^clear */
		    /*clear *//*_.MELT_DEBUG_FUN__V190*/
				meltfptr[180] = 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	 /*_.IF___V189*/ meltfptr[185] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1810:/ quasiblock");


			/*_.PROGN___V191*/ meltfptr[180] =
			  /*_.IF___V189*/ meltfptr[185];;
			/*^compute */
			/*_.IFCPP___V188*/ meltfptr[181] =
			  /*_.PROGN___V191*/ meltfptr[180];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1810:/ clear");
		  /*clear *//*_#MELT_NEED_DBG__L49*/ meltfnum[47] = 0;
			/*^clear */
		  /*clear *//*_.IF___V189*/ meltfptr[185] = 0;
			/*^clear */
		  /*clear *//*_.PROGN___V191*/ meltfptr[180] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V188*/ meltfptr[181] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1811:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.OINIBODY__V25*/
					     meltfptr[24]),
					    (melt_ptr_t) ( /*_.CUROBJB__V179*/
							  meltfptr[175]));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1812:/ locexp");
			/*void */ (void) 0;
		      }
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1809:/ quasiblock");


		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:1805:/ clear");
		/*clear *//*_.IFCPP___V188*/ meltfptr[181] = 0;
		    }
		    ;
		  }
		;

		MELT_LOCATION ("warmelt-genobj.melt:1802:/ clear");
	      /*clear *//*_.CUROBJB__V179*/ meltfptr[175] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V180*/ meltfptr[176] = 0;
		/*^clear */
	      /*clear *//*_#IS_A__L46*/ meltfnum[44] = 0;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	}			/* end foreach_in_list meltcit3__EACHLIST */
     /*_.CURNPAIR__V173*/ meltfptr[166] = NULL;
     /*_.CURNINST__V174*/ meltfptr[170] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-genobj.melt:1797:/ clear");
	    /*clear *//*_.CURNPAIR__V173*/ meltfptr[166] = 0;
      /*^clear */
	    /*clear *//*_.CURNINST__V174*/ meltfptr[170] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V175*/ meltfptr[174] = 0;
      /*^clear */
	    /*clear *//*_#IS_NOT_A__L43*/ meltfnum[33] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1815:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "after toplevel body";
      /*_.APPEND_COMMENTCONST__V192*/ meltfptr[185] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!APPEND_COMMENTCONST */ meltfrout->tabval[24])),
		    (melt_ptr_t) ( /*_.OINIBODY__V25*/ meltfptr[24]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1816:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L51*/ meltfnum[32] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1816:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L51*/ meltfnum[32])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L52*/ meltfnum[47] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1816:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L52*/ meltfnum[47];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1816;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc final oinibody=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OINIBODY__V25*/ meltfptr[24];
	      /*_.MELT_DEBUG_FUN__V195*/ meltfptr[175] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V194*/ meltfptr[181] =
	      /*_.MELT_DEBUG_FUN__V195*/ meltfptr[175];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1816:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L52*/ meltfnum[47] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V195*/ meltfptr[175] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V194*/ meltfptr[181] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1816:/ quasiblock");


      /*_.PROGN___V196*/ meltfptr[176] = /*_.IF___V194*/ meltfptr[181];;
      /*^compute */
      /*_.IFCPP___V193*/ meltfptr[180] = /*_.PROGN___V196*/ meltfptr[176];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1816:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L51*/ meltfnum[32] = 0;
      /*^clear */
	     /*clear *//*_.IF___V194*/ meltfptr[181] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V196*/ meltfptr[176] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V193*/ meltfptr[180] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1818:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L53*/ meltfnum[44] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1818:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L53*/ meltfnum[44])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L54*/ meltfnum[47] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1818:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L54*/ meltfnum[47];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1818;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc gives oinitxrout=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OINITXROUT__V37*/ meltfptr[36];
	      /*_.MELT_DEBUG_FUN__V199*/ meltfptr[176] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V198*/ meltfptr[181] =
	      /*_.MELT_DEBUG_FUN__V199*/ meltfptr[176];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1818:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L54*/ meltfnum[47] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V199*/ meltfptr[176] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V198*/ meltfptr[181] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1818:/ quasiblock");


      /*_.PROGN___V200*/ meltfptr[176] = /*_.IF___V198*/ meltfptr[181];;
      /*^compute */
      /*_.IFCPP___V197*/ meltfptr[175] = /*_.PROGN___V200*/ meltfptr[176];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1818:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L53*/ meltfnum[44] = 0;
      /*^clear */
	     /*clear *//*_.IF___V198*/ meltfptr[181] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V200*/ meltfptr[176] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V197*/ meltfptr[175] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1819:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OINITXROUT__V37*/ meltfptr[36];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1819:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V131*/ meltfptr[126] = /*_.RETURN___V201*/ meltfptr[181];;

    MELT_LOCATION ("warmelt-genobj.melt:1715:/ clear");
	   /*clear *//*_.LAMBDA___V132*/ meltfptr[131] = 0;
    /*^clear */
	   /*clear *//*_.OBJTOPLIS__V134*/ meltfptr[133] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V135*/ meltfptr[134] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V139*/ meltfptr[135] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V140*/ meltfptr[136] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V142*/ meltfptr[141] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V143*/ meltfptr[142] = 0;
    /*^clear */
	   /*clear *//*_#LIST_LENGTH__L31*/ meltfnum[22] = 0;
    /*^clear */
	   /*clear *//*_#I__L32*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V147*/ meltfptr[143] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V154*/ meltfptr[144] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V155*/ meltfptr[148] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V165*/ meltfptr[161] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V169*/ meltfptr[162] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V192*/ meltfptr[185] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V193*/ meltfptr[180] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V197*/ meltfptr[175] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V201*/ meltfptr[181] = 0;
    /*_.LET___V120*/ meltfptr[116] = /*_.LET___V131*/ meltfptr[126];;

    MELT_LOCATION ("warmelt-genobj.melt:1700:/ clear");
	   /*clear *//*_.LAMBDA___V121*/ meltfptr[117] = 0;
    /*^clear */
	   /*clear *//*_.ODATATUP__V123*/ meltfptr[122] = 0;
    /*^clear */
	   /*clear *//*_.TOPLIS__V124*/ meltfptr[123] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V125*/ meltfptr[124] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V129*/ meltfptr[125] = 0;
    /*^clear */
	   /*clear *//*_.LET___V131*/ meltfptr[126] = 0;
    /*_.LET___V104*/ meltfptr[83] = /*_.LET___V120*/ meltfptr[116];;

    MELT_LOCATION ("warmelt-genobj.melt:1670:/ clear");
	   /*clear *//*_#NBLITVAL__L21*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_OBJLOCATEDEXP__V105*/ meltfptr[74] = 0;
    /*^clear */
	   /*clear *//*_.LIST___V106*/ meltfptr[75] = 0;
    /*^clear */
	   /*clear *//*_.INST___V109*/ meltfptr[106] = 0;
    /*^clear */
	   /*clear *//*_.LIST___V111*/ meltfptr[110] = 0;
    /*^clear */
	   /*clear *//*_.INST___V114*/ meltfptr[111] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V116*/ meltfptr[115] = 0;
    /*^clear */
	   /*clear *//*_.LET___V120*/ meltfptr[116] = 0;
    /*_.LET___V22*/ meltfptr[20] = /*_.LET___V104*/ meltfptr[83];;

    MELT_LOCATION ("warmelt-genobj.melt:1529:/ clear");
	   /*clear *//*_.LOCMAP__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OINIPROLOG__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OINIBODY__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.MODENV__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.NBODY__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.MODNAME__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.LITVALIST__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.MOCX_MODULENAME__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.OINITXROUT__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.IMPORTMAP__V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.VALOCLIST__V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V42*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPOBJECT__V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_MAPOBJECT__V44*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.LIST_MAP__V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.GCX__V48*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.LET___V50*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_.RETINIT__V52*/ meltfptr[50] = 0;
    /*^clear */
	   /*clear *//*_.LET___V53*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.OCURENVBOXLOC__V55*/ meltfptr[53] = 0;
    /*^clear */
	   /*clear *//*_.LET___V56*/ meltfptr[55] = 0;
    /*^clear */
	   /*clear *//*_.OCURENVLOC__V58*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_.LET___V59*/ meltfptr[58] = 0;
    /*^clear */
	   /*clear *//*_.OPREVENVLOC__V61*/ meltfptr[59] = 0;
    /*^clear */
	   /*clear *//*_.LET___V62*/ meltfptr[61] = 0;
    /*^clear */
	   /*clear *//*_.OLITVALUETUPLOC__V64*/ meltfptr[62] = 0;
    /*^clear */
	   /*clear *//*_.INIDEFBINDS__V65*/ meltfptr[64] = 0;
    /*^clear */
	   /*clear *//*_.APPEND_COMMENTCONST__V66*/ meltfptr[65] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V67*/ meltfptr[66] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V88*/ meltfptr[82] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_OBJLOCATEDEXP__V99*/ meltfptr[81] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V100*/ meltfptr[73] = 0;
    /*^clear */
	   /*clear *//*_.LET___V104*/ meltfptr[83] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1517:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V22*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1517:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LET___V22*/ meltfptr[20] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILE2OBJ_INITEXTENDPROC", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_genobj_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_31_warmelt_genobj_LAMBDA___11___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_31_warmelt_genobj_LAMBDA___11___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_31_warmelt_genobj_LAMBDA___11__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_31_warmelt_genobj_LAMBDA___11___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_31_warmelt_genobj_LAMBDA___11__ nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1569:/ getarg");
 /*_.CURPRO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1570:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1570:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1570:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1570;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc procurmodenvlist curpro=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURPRO__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1570:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1570:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1570:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1571:/ quasiblock");


 /*_.CUROU__V8*/ meltfptr[4] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   (( /*~COMPICACHE */ meltfclos->tabval[0])),
			   (meltobject_ptr_t) ( /*_.CURPRO__V2*/
					       meltfptr[1]));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1572:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1572:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1572:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1572;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc procurmodenvlist curou=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CUROU__V8*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V10*/ meltfptr[9] =
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1572:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V10*/ meltfptr[9] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1572:/ quasiblock");


      /*_.PROGN___V12*/ meltfptr[10] = /*_.IF___V10*/ meltfptr[9];;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.PROGN___V12*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1572:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V10*/ meltfptr[9] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V12*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1573:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.CUROU__V8*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-genobj.melt:1573:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1573:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curou"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1573) ? (1573) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[9] = /*_.IFELSE___V14*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1573:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[3] = /*_.CUROU__V8*/ meltfptr[4];;

    MELT_LOCATION ("warmelt-genobj.melt:1571:/ clear");
	   /*clear *//*_.CUROU__V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1569:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1569:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_31_warmelt_genobj_LAMBDA___11___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_31_warmelt_genobj_LAMBDA___11__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_genobj_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_32_warmelt_genobj_LAMBDA___12___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_32_warmelt_genobj_LAMBDA___12___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_32_warmelt_genobj_LAMBDA___12__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_32_warmelt_genobj_LAMBDA___12___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_32_warmelt_genobj_LAMBDA___12__ nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1640:/ getarg");
 /*_.IVAL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1641:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1641:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1641:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1641;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc imported ival=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.IVAL__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1641:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1641:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1641:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1642:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.IVAL__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_LITERALNAMEDVALUE */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:1642:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1642:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ival"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1642) ? (1642) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[3] = /*_.IFELSE___V8*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1642:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1643:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.IVAL__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_LITERALNAMEDVALUE */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.IVAL__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "NLITVAL_SYMBOL");
   /*_.ISYM__V10*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ISYM__V10*/ meltfptr[9] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1644:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.IVAL__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_LITERALVALUE */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.IVAL__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NLITVAL_REGVAL");
   /*_.REGVAL__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REGVAL__V11*/ meltfptr[10] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1645:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.REGVAL__V11*/ meltfptr[10]),
					(melt_ptr_t) (( /*!CLASS_LITERAL_VALUE */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.REGVAL__V11*/ meltfptr[10]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "LITV_LOC");
   /*_.LITLOC__V12*/ meltfptr[11] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LITLOC__V12*/ meltfptr[11] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1646:/ cond");
    /*cond */ if ( /*_.LITLOC__V12*/ meltfptr[11])	/*then */
      {
	/*^cond.then */
	/*_.ILOCV__V13*/ meltfptr[12] = /*_.LITLOC__V12*/ meltfptr[11];;
      }
    else
      {
	MELT_LOCATION ("warmelt-genobj.melt:1646:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1647:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ISYM__V10*/ meltfptr[9];
	    /*_.NLOC__V15*/ meltfptr[14] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!GET_FREE_OBJLOCPTR */ meltfrout->tabval[4])),
			  (melt_ptr_t) (( /*~GCX */ meltfclos->tabval[0])),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1649:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1649:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1649:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[7];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1649;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_initextendproc nloc=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.NLOC__V15*/ meltfptr[14];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = " for ival=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.IVAL__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V17*/ meltfptr[16] =
		    /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1649:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V17*/ meltfptr[16] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1649:/ quasiblock");


	    /*_.PROGN___V19*/ meltfptr[17] = /*_.IF___V17*/ meltfptr[16];;
	    /*^compute */
	    /*_.IFCPP___V16*/ meltfptr[15] = /*_.PROGN___V19*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1649:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V17*/ meltfptr[16] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V19*/ meltfptr[17] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1650:/ locexp");
	    meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				   (( /*~IMPORTMAP */ meltfclos->tabval[1])),
				   (meltobject_ptr_t) ( /*_.ISYM__V10*/
						       meltfptr[9]),
				   (melt_ptr_t) ( /*_.NLOC__V15*/
						 meltfptr[14]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1651:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.LITLOC__V12*/
					       meltfptr[11]),
					      (melt_ptr_t) (( /*!CLASS_LITERAL_VALUE */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @LITV_LOC",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.LITLOC__V12*/
						   meltfptr[11])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.LITLOC__V12*/ meltfptr[11]), (3),
				      ( /*_.NLOC__V15*/ meltfptr[14]),
				      "LITV_LOC");
		;
		/*^touch */
		meltgc_touch ( /*_.LITLOC__V12*/ meltfptr[11]);
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.LITLOC__V12*/ meltfptr[11],
					      "put-fields");
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  /*_.LET___V14*/ meltfptr[13] = /*_.NLOC__V15*/ meltfptr[14];;

	  MELT_LOCATION ("warmelt-genobj.melt:1647:/ clear");
	     /*clear *//*_.NLOC__V15*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
	  /*_.ILOCV__V13*/ meltfptr[12] = /*_.LET___V14*/ meltfptr[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1646:/ clear");
	     /*clear *//*_.LET___V14*/ meltfptr[13] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1654:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1654:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1654:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1654;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc imported ilocv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ILOCV__V13*/ meltfptr[12];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " for isym=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.ISYM__V10*/ meltfptr[9];
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[14] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V21*/ meltfptr[17] =
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1654:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[14] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V21*/ meltfptr[17] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1654:/ quasiblock");


      /*_.PROGN___V23*/ meltfptr[15] = /*_.IF___V21*/ meltfptr[17];;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[16] = /*_.PROGN___V23*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1654:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V21*/ meltfptr[17] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V9*/ meltfptr[4] = /*_.ILOCV__V13*/ meltfptr[12];;

    MELT_LOCATION ("warmelt-genobj.melt:1643:/ clear");
	   /*clear *//*_.ISYM__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.REGVAL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LITLOC__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.ILOCV__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[16] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1640:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V9*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1640:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V9*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_32_warmelt_genobj_LAMBDA___12___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_32_warmelt_genobj_LAMBDA___12__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_genobj_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_33_warmelt_genobj_LAMBDA___13___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_33_warmelt_genobj_LAMBDA___13___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_33_warmelt_genobj_LAMBDA___13__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_33_warmelt_genobj_LAMBDA___13___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_33_warmelt_genobj_LAMBDA___13__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1704:/ getarg");
 /*_.CURDAT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1705:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CURDAT__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_BOUND_DATA */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:1705:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1705:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curdat"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1705) ? (1705) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1705:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1706:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.CUROBD__V6*/ meltfptr[5] =
	meltgc_send ((melt_ptr_t) ( /*_.CURDAT__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[1])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V5*/ meltfptr[3] = /*_.CUROBD__V6*/ meltfptr[5];;

    MELT_LOCATION ("warmelt-genobj.melt:1706:/ clear");
	   /*clear *//*_.CUROBD__V6*/ meltfptr[5] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1704:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1704:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_33_warmelt_genobj_LAMBDA___13___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_33_warmelt_genobj_LAMBDA___13__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_genobj_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_34_warmelt_genobj_LAMBDA___14___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_34_warmelt_genobj_LAMBDA___14___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_34_warmelt_genobj_LAMBDA___14__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_34_warmelt_genobj_LAMBDA___14___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_34_warmelt_genobj_LAMBDA___14__ nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1718:/ getarg");
 /*_.CURTOP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1719:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1719:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1719:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1719;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_initextendproc curtop=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURTOP__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1719:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1719:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1719:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1720:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.OTOP__V8*/ meltfptr[4] =
	meltgc_send ((melt_ptr_t) ( /*_.CURTOP__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[1])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1721:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1721:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1721:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1721;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_initextendproc otop=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OTOP__V8*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V10*/ meltfptr[9] =
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1721:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V10*/ meltfptr[9] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1721:/ quasiblock");


      /*_.PROGN___V12*/ meltfptr[10] = /*_.IF___V10*/ meltfptr[9];;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.PROGN___V12*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1721:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V10*/ meltfptr[9] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V12*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[3] = /*_.OTOP__V8*/ meltfptr[4];;

    MELT_LOCATION ("warmelt-genobj.melt:1720:/ clear");
	   /*clear *//*_.OTOP__V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1718:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1718:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_34_warmelt_genobj_LAMBDA___14___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_34_warmelt_genobj_LAMBDA___14__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_genobj_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_35_warmelt_genobj_LAMBDA___15___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_35_warmelt_genobj_LAMBDA___15___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_35_warmelt_genobj_LAMBDA___15__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_35_warmelt_genobj_LAMBDA___15___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_35_warmelt_genobj_LAMBDA___15__ nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1729:/ getarg");
 /*_.CURPDAT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#CURK__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:1730:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L2*/ meltfnum[1] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CURPDAT__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
					  meltfrout->tabval[0])));;
    MELT_LOCATION ("warmelt-genobj.melt:1730:/ cond");
    /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1731:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CURPDAT__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "OIE_DATA");
    /*_.ODAT__V5*/ meltfptr[4] = slot;
	  };
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1733:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L3*/ meltfnum[2] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1733:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[2])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1733:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1733;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "compile2obj_initextendproc getting curpdat=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURPDAT__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V7*/ meltfptr[6] =
		    /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1733:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V7*/ meltfptr[6] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1733:/ quasiblock");


	    /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
	    /*^compute */
	    /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1733:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:1736:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L5*/ meltfnum[3] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.ODAT__V5*/ meltfptr[4]),
				 (melt_ptr_t) (( /*!CLASS_NREP_DATAKEYWORD */
						meltfrout->tabval[2])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1736:/ cond");
	  /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1737:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_OBJGETNAMEDKEYWORD */ meltfrout->tabval[3])), (2), "CLASS_OBJGETNAMEDKEYWORD");
      /*_.INST__V13*/ meltfptr[12] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBGNAMED_IOBJ",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V13*/
						   meltfptr[12])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V13*/ meltfptr[12]), (1),
				      ( /*_.CURPDAT__V2*/ meltfptr[1]),
				      "OBGNAMED_IOBJ");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V13*/ meltfptr[12],
					      "newly made instance");
		;
		/*_.OGKW__V12*/ meltfptr[11] = /*_.INST__V13*/ meltfptr[12];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:1740:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      (( /*~OINIPROLOG */ meltfclos->
					tabval[0])),
				      (melt_ptr_t) ( /*_.OGKW__V12*/
						    meltfptr[11]));
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:1741:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L6*/ meltfnum[2] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1741:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[2])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1741:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-genobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 1741;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "compile2obj_initextendproc added keyword getting ogkw=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.OGKW__V12*/ meltfptr[11];
			  /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[1])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V15*/ meltfptr[14] =
			  /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1741:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V15*/ meltfptr[14] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-genobj.melt:1741:/ quasiblock");


		  /*_.PROGN___V17*/ meltfptr[15] =
		    /*_.IF___V15*/ meltfptr[14];;
		  /*^compute */
		  /*_.IFCPP___V14*/ meltfptr[13] =
		    /*_.PROGN___V17*/ meltfptr[15];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1741:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[2] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		/*^compute */
		/*_.LET___V11*/ meltfptr[7] = /*_.IFCPP___V14*/ meltfptr[13];;

		MELT_LOCATION ("warmelt-genobj.melt:1737:/ clear");
	       /*clear *//*_.OGKW__V12*/ meltfptr[11] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
		/*_.IFELSE___V10*/ meltfptr[6] = /*_.LET___V11*/ meltfptr[7];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1736:/ clear");
	       /*clear *//*_.LET___V11*/ meltfptr[7] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1743:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_A__L8*/ meltfnum[6] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.ODAT__V5*/ meltfptr[4]),
				       (melt_ptr_t) (( /*!CLASS_NREP_DATASYMBOL */ meltfrout->tabval[4])));;
		MELT_LOCATION ("warmelt-genobj.melt:1743:/ cond");
		/*cond */ if ( /*_#IS_A__L8*/ meltfnum[6])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-genobj.melt:1744:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_OBJGETNAMEDSYMBOL */ meltfrout->tabval[5])), (2), "CLASS_OBJGETNAMEDSYMBOL");
	/*_.INST__V21*/ meltfptr[13] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBGNAMED_IOBJ",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V21*/
							 meltfptr[13])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V21*/ meltfptr[13]),
					    (1),
					    ( /*_.CURPDAT__V2*/ meltfptr[1]),
					    "OBGNAMED_IOBJ");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V21*/
						    meltfptr[13],
						    "newly made instance");
		      ;
		      /*_.OGSY__V20*/ meltfptr[11] =
			/*_.INST__V21*/ meltfptr[13];;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1747:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    (( /*~OINIPROLOG */ meltfclos->
					      tabval[0])),
					    (melt_ptr_t) ( /*_.OGSY__V20*/
							  meltfptr[11]));
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-genobj.melt:1748:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L9*/ meltfnum[2] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-genobj.melt:1748:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[2])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-genobj.melt:1748:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-genobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1748;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "compile2obj_initextendproc added symbol getting ogsy=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.OGSY__V20*/
				  meltfptr[11];
				/*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[1])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V23*/ meltfptr[22] =
				/*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-genobj.melt:1748:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L10*/
				meltfnum[9] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V23*/ meltfptr[22] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1748:/ quasiblock");


			/*_.PROGN___V25*/ meltfptr[23] =
			  /*_.IF___V23*/ meltfptr[22];;
			/*^compute */
			/*_.IFCPP___V22*/ meltfptr[7] =
			  /*_.PROGN___V25*/ meltfptr[23];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1748:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[2] = 0;
			/*^clear */
		   /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V22*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      /*^compute */
		      /*_.LET___V19*/ meltfptr[15] =
			/*_.IFCPP___V22*/ meltfptr[7];;

		      MELT_LOCATION ("warmelt-genobj.melt:1744:/ clear");
		 /*clear *//*_.OGSY__V20*/ meltfptr[11] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V22*/ meltfptr[7] = 0;
		      /*_.IFELSE___V18*/ meltfptr[14] =
			/*_.LET___V19*/ meltfptr[15];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:1743:/ clear");
		 /*clear *//*_.LET___V19*/ meltfptr[15] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IFELSE___V18*/ meltfptr[14] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V10*/ meltfptr[6] =
		  /*_.IFELSE___V18*/ meltfptr[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1736:/ clear");
	       /*clear *//*_#IS_A__L8*/ meltfnum[6] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V18*/ meltfptr[14] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.LET___V4*/ meltfptr[3] = /*_.IFELSE___V10*/ meltfptr[6];;

	  MELT_LOCATION ("warmelt-genobj.melt:1731:/ clear");
	     /*clear *//*_.ODAT__V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
	  /*^clear */
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	  /*_.IF___V3*/ meltfptr[2] = /*_.LET___V4*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1730:/ clear");
	     /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V3*/ meltfptr[2] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1729:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1729:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_35_warmelt_genobj_LAMBDA___15___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_35_warmelt_genobj_LAMBDA___15__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_genobj_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_36_warmelt_genobj_LAMBDA___16___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_36_warmelt_genobj_LAMBDA___16___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 24
    melt_ptr_t mcfr_varptr[24];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_36_warmelt_genobj_LAMBDA___16__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_36_warmelt_genobj_LAMBDA___16___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 24; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_36_warmelt_genobj_LAMBDA___16__ nbval 24*/
  meltfram__.mcfr_nbvar = 24 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1759:/ getarg");
 /*_.CURIMPORT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1760:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1760:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1760:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1760;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc curimport=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURIMPORT__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1760:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1760:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1760:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1761:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CURIMPORT__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_LITERALNAMEDVALUE */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:1761:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1761:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check curimport"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1761) ? (1761) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[3] = /*_.IFELSE___V8*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1761:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1763:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CURIMPORT__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_LITERALNAMEDVALUE */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CURIMPORT__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "NLITVAL_SYMBOL");
   /*_.IMPSYM__V9*/ meltfptr[4] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.IMPSYM__V9*/ meltfptr[4] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1764:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CURIMPORT__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NREP_LITERALVALUE */ meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CURIMPORT__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NLITVAL_REGVAL");
   /*_.REGVAL__V10*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REGVAL__V10*/ meltfptr[9] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1765:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.REGVAL__V10*/ meltfptr[9]),
					(melt_ptr_t) (( /*!CLASS_LITERAL_VALUE */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.REGVAL__V10*/ meltfptr[9]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "LITV_VALUE");
   /*_.VAL__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.VAL__V11*/ meltfptr[10] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1766:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*~MODCTX */ meltfclos->
					  tabval[0])),
					(melt_ptr_t) (( /*!CLASS_MODULE_CONTEXT */ meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) (( /*~MODCTX */ meltfclos->tabval[0])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "MOCX_MODULENAME");
   /*_.MODULSTR__V12*/ meltfptr[11] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.MODULSTR__V12*/ meltfptr[11] = NULL;;
      }
    ;
    /*^compute */
 /*_.OIMPLOCV__V13*/ meltfptr[12] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   (( /*~IMPORTMAP */ meltfclos->tabval[1])),
			   (meltobject_ptr_t) ( /*_.IMPSYM__V9*/
					       meltfptr[4]));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1769:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1769:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1769:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1769;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compile2obj_initextendproc impsym=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.IMPSYM__V9*/ meltfptr[4];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n regval=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.REGVAL__V10*/ meltfptr[9];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n oimplocv=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.OIMPLOCV__V13*/ meltfptr[12];
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V15*/ meltfptr[14] =
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1769:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V15*/ meltfptr[14] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1769:/ quasiblock");


      /*_.PROGN___V17*/ meltfptr[15] = /*_.IF___V15*/ meltfptr[14];;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.PROGN___V17*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1769:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1770:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:1771:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.REGVAL__V10*/ meltfptr[9]),
					(melt_ptr_t) (( /*!CLASS_LITERAL_VALUE */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.REGVAL__V10*/ meltfptr[9]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "LITV_RANK");
   /*_.LITV_RANK__V18*/ meltfptr[14] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LITV_RANK__V18*/ meltfptr[14] = NULL;;
      }
    ;
    /*^compute */
 /*_#LITRANK__L6*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.LITV_RANK__V18*/ meltfptr[14])));;
    MELT_LOCATION ("warmelt-genobj.melt:1772:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[8];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!CTYPE_VALUE */ meltfrout->tabval[6]);
      /*^apply.arg */
      argtab[1].meltbp_cstring = "/* retrieve runtime literal value #";
      /*^apply.arg */
      argtab[2].meltbp_long = /*_#LITRANK__L6*/ meltfnum[1];
      /*^apply.arg */
      argtab[3].meltbp_cstring =
	" */\n\t\t\t\t     ((melt_ptr_t) (((meltmultiple_ptr_t)";
      /*^apply.arg */
      argtab[4].meltbp_aptr =
	(melt_ptr_t *) & ( /*~OLITVALUETUPLOC */ meltfclos->tabval[2]);
      /*^apply.arg */
      argtab[5].meltbp_cstring = ")->tabval[";
      /*^apply.arg */
      argtab[6].meltbp_long = /*_#LITRANK__L6*/ meltfnum[1];
      /*^apply.arg */
      argtab[7].meltbp_cstring = "]))\n\t\t\t\t     ";
      /*_.OGETLIT__V19*/ meltfptr[15] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MAKE_OBJCOMPUTE */ meltfrout->tabval[5])),
		    (melt_ptr_t) (( /*nil */ NULL)),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_LONG
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1779:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.OIMPLOCV__V13*/ meltfptr[12];
      /*_.PUT_OBJDEST__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.OGETLIT__V19*/ meltfptr[15]),
		     (melt_ptr_t) (( /*!PUT_OBJDEST */ meltfrout->tabval[7])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1780:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1780:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1780:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1780;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compile2obj_initextendproc/import val=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.VAL__V11*/ meltfptr[10];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " regval=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.REGVAL__V10*/ meltfptr[9];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " ogetlit=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.OGETLIT__V19*/ meltfptr[15];
	      /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V22*/ meltfptr[21] =
	      /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1780:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V22*/ meltfptr[21] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1780:/ quasiblock");


      /*_.PROGN___V24*/ meltfptr[22] = /*_.IF___V22*/ meltfptr[21];;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[20] = /*_.PROGN___V24*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1780:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V22*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1783:/ locexp");
      meltgc_append_list ((melt_ptr_t)
			  (( /*~OINIPROLOG */ meltfclos->tabval[3])),
			  (melt_ptr_t) ( /*_.OGETLIT__V19*/ meltfptr[15]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:1770:/ clear");
	   /*clear *//*_.LITV_RANK__V18*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#LITRANK__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.OGETLIT__V19*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.PUT_OBJDEST__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;

    MELT_LOCATION ("warmelt-genobj.melt:1763:/ clear");
	   /*clear *//*_.IMPSYM__V9*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.REGVAL__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.VAL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.MODULSTR__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OIMPLOCV__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1759:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_36_warmelt_genobj_LAMBDA___16___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_36_warmelt_genobj_LAMBDA___16__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 49
    melt_ptr_t mcfr_varptr[49];
#define MELTFRAM_NBVARNUM 30
    long mcfr_varnum[30];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 49; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR nbval 49*/
  meltfram__.mcfr_nbvar = 49 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GET_FREE_OBJLOCPTR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1837:/ getarg");
 /*_.GCX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NAM__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NAM__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1838:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:1838:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1838:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1838) ? (1838) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1838:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1839:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1839:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1839:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1839;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "get_free_objlocptr start nam=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NAM__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1839:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1839:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1839:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1840:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.OROUT__V11*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1841:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "GNCX_FREEPTRLIST");
  /*_.FREELI__V12*/ meltfptr[11] = slot;
    };
    ;
 /*_.PFREE__V13*/ meltfptr[12] =
      (meltgc_popfirst_list
       ((melt_ptr_t) ( /*_.FREELI__V12*/ meltfptr[11])));;
    /*^compute */
 /*_#OFF__L4*/ meltfnum[2] = -1;;
    MELT_LOCATION ("warmelt-genobj.melt:1845:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L5*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.OROUT__V11*/ meltfptr[7]),
			   (melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */ meltfrout->
					  tabval[2])));;
    /*^compute */
 /*_#NOT__L6*/ meltfnum[5] =
      (!( /*_#IS_A__L5*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-genobj.melt:1845:/ cond");
    /*cond */ if ( /*_#NOT__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1846:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L7*/ meltfnum[6] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1846:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[6])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1846:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1846;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "get_free_objlocptr bad orout=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.OROUT__V11*/ meltfptr[7];
		    /*_.MELT_DEBUG_FUN__V17*/ meltfptr[16] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V16*/ meltfptr[15] =
		    /*_.MELT_DEBUG_FUN__V17*/ meltfptr[16];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1846:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V17*/ meltfptr[16] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V16*/ meltfptr[15] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1846:/ quasiblock");


	    /*_.PROGN___V18*/ meltfptr[16] = /*_.IF___V16*/ meltfptr[15];;
	    /*^compute */
	    /*_.IFCPP___V15*/ meltfptr[14] = /*_.PROGN___V18*/ meltfptr[16];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1846:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V16*/ meltfptr[15] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V18*/ meltfptr[16] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
	  /*_.IF___V14*/ meltfptr[13] = /*_.IFCPP___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1845:/ clear");
	     /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[13] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1847:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L9*/ meltfnum[7] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OROUT__V11*/ meltfptr[7]),
			     (melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:1847:/ cond");
      /*cond */ if ( /*_#IS_A__L9*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1847:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check orout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1847) ? (1847) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[15] = /*_.IFELSE___V20*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1847:/ clear");
	     /*clear *//*_#IS_A__L9*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1848:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L10*/ meltfnum[6] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.PFREE__V13*/ meltfptr[12]),
			   (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					  tabval[3])));;
    MELT_LOCATION ("warmelt-genobj.melt:1848:/ cond");
    /*cond */ if ( /*_#IS_A__L10*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1849:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.PFREE__V13*/ meltfptr[12]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "OBL_OFF");
    /*_.OFFPFREE__V21*/ meltfptr[14] = slot;
	  };
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1850:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L13*/ meltfnum[12] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1850:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[12])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1850:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1850;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "get_free_objlocptr pfree=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.PFREE__V13*/ meltfptr[12];
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V23*/ meltfptr[22] =
		    /*_.MELT_DEBUG_FUN__V24*/ meltfptr[23];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1850:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[13] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V24*/ meltfptr[23] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V23*/ meltfptr[22] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1850:/ quasiblock");


	    /*_.PROGN___V25*/ meltfptr[23] = /*_.IF___V23*/ meltfptr[22];;
	    /*^compute */
	    /*_.IFCPP___V22*/ meltfptr[16] = /*_.PROGN___V25*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1850:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V23*/ meltfptr[22] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V25*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V22*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1851:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_INTEGERBOX__L15*/ meltfnum[13] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.OFFPFREE__V21*/ meltfptr[14])) ==
	       MELTOBMAG_INT);;
	    MELT_LOCATION ("warmelt-genobj.melt:1851:/ cond");
	    /*cond */ if ( /*_#IS_INTEGERBOX__L15*/ meltfnum[13])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V27*/ meltfptr[23] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-genobj.melt:1851:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("get_free_objlocptr check offpfree"),
					("warmelt-genobj.melt")
					? ("warmelt-genobj.melt") : __FILE__,
					(1851) ? (1851) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V27*/ meltfptr[23] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V26*/ meltfptr[22] = /*_.IFELSE___V27*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1851:/ clear");
	       /*clear *//*_#IS_INTEGERBOX__L15*/ meltfnum[13] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V27*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V26*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1853:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#GET_INT__L16*/ meltfnum[12] =
	      (melt_get_int ((melt_ptr_t) ( /*_.PFREE__V13*/ meltfptr[12])));;
	    /*^compute */
     /*_#NOT__L17*/ meltfnum[13] =
	      (!( /*_#GET_INT__L16*/ meltfnum[12]));;
	    MELT_LOCATION ("warmelt-genobj.melt:1853:/ cond");
	    /*cond */ if ( /*_#NOT__L17*/ meltfnum[13])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V29*/ meltfptr[28] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-genobj.melt:1853:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check pfree not used"),
					("warmelt-genobj.melt")
					? ("warmelt-genobj.melt") : __FILE__,
					(1853) ? (1853) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V28*/ meltfptr[23] = /*_.IFELSE___V29*/ meltfptr[28];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1853:/ clear");
	       /*clear *//*_#GET_INT__L16*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_#NOT__L17*/ meltfnum[13] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V28*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
   /*_#GET_INT__L18*/ meltfnum[12] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.OFFPFREE__V21*/ meltfptr[14])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1854:/ compute");
	  /*_#OFF__L4*/ meltfnum[2] = /*_#SETQ___L19*/ meltfnum[13] =
	    /*_#GET_INT__L18*/ meltfnum[12];;
	  /*_#LET___L12*/ meltfnum[11] = /*_#SETQ___L19*/ meltfnum[13];;

	  MELT_LOCATION ("warmelt-genobj.melt:1849:/ clear");
	     /*clear *//*_.OFFPFREE__V21*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V22*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V26*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V28*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L18*/ meltfnum[12] = 0;
	  /*^clear */
	     /*clear *//*_#SETQ___L19*/ meltfnum[13] = 0;
	  /*_#IFELSE___L11*/ meltfnum[7] = /*_#LET___L12*/ meltfnum[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1848:/ clear");
	     /*clear *//*_#LET___L12*/ meltfnum[11] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1855:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OROUT__V11*/ meltfptr[7]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 4, "OBROUT_NBVAL");
    /*_.NBVALOROUT__V30*/ meltfptr[28] = slot;
	  };
	  ;
   /*_#GET_INT__L21*/ meltfnum[13] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.NBVALOROUT__V30*/ meltfptr[28])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1856:/ compute");
	  /*_#OFF__L4*/ meltfnum[2] = /*_#SETQ___L22*/ meltfnum[11] =
	    /*_#GET_INT__L21*/ meltfnum[13];;
	  /*_#LET___L20*/ meltfnum[12] = /*_#SETQ___L22*/ meltfnum[11];;

	  MELT_LOCATION ("warmelt-genobj.melt:1855:/ clear");
	     /*clear *//*_.NBVALOROUT__V30*/ meltfptr[28] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L21*/ meltfnum[13] = 0;
	  /*^clear */
	     /*clear *//*_#SETQ___L22*/ meltfnum[11] = 0;
	  /*_#IFELSE___L11*/ meltfnum[7] = /*_#LET___L20*/ meltfnum[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1848:/ clear");
	     /*clear *//*_#LET___L20*/ meltfnum[12] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1858:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#I__L23*/ meltfnum[13] =
	(( /*_#OFF__L4*/ meltfnum[2]) >= (0));;
      MELT_LOCATION ("warmelt-genobj.melt:1858:/ cond");
      /*cond */ if ( /*_#I__L23*/ meltfnum[13])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V32*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1858:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check off"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1858) ? (1858) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V32*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[14] = /*_.IFELSE___V32*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1858:/ clear");
	     /*clear *//*_#I__L23*/ meltfnum[13] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V32*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1859:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OROUT__V11*/ meltfptr[7]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBROUT_NBVAL");
  /*_.NBVALBOX__V34*/ meltfptr[23] = slot;
    };
    ;
 /*_#NBVAL__L24*/ meltfnum[11] =
      (melt_get_int ((melt_ptr_t) ( /*_.NBVALBOX__V34*/ meltfptr[23])));;
    /*^compute */
 /*_.NAMBUF__V35*/ meltfptr[28] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[4])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-genobj.melt:1863:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L25*/ meltfnum[12] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.NAM__V3*/ meltfptr[2]),
			   (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
					  tabval[5])));;
    MELT_LOCATION ("warmelt-genobj.melt:1863:/ cond");
    /*cond */ if ( /*_#IS_A__L25*/ meltfnum[12])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1864:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NAM__V3*/ meltfptr[2]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMSTR__V37*/ meltfptr[36] = slot;
	  };
	  ;



	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1865:/ locexp");
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.NAMBUF__V35*/ meltfptr[28]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMSTR__V37*/
							meltfptr[36])));
	  }
	  ;
	     /*clear *//*_.IFELSE___V36*/ meltfptr[16] = 0;

	  MELT_LOCATION ("warmelt-genobj.melt:1864:/ clear");
	     /*clear *//*_.NAMSTR__V37*/ meltfptr[36] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-genobj.melt:1863:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1866:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L26*/ meltfnum[13] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.NAM__V3*/ meltfptr[2])) ==
	     MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-genobj.melt:1866:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L26*/ meltfnum[13])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {




		{
		  MELT_LOCATION ("warmelt-genobj.melt:1867:/ locexp");
		  meltgc_add_strbuf_cident ((melt_ptr_t)
					    ( /*_.NAMBUF__V35*/ meltfptr[28]),
					    melt_string_str ((melt_ptr_t)
							     ( /*_.NAM__V3*/
							      meltfptr[2])));
		}
		;
	       /*clear *//*_.IFELSE___V38*/ meltfptr[36] = 0;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-genobj.melt:1866:/ cond.else");

    /*_.IFELSE___V38*/ meltfptr[36] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IFELSE___V36*/ meltfptr[16] = /*_.IFELSE___V38*/ meltfptr[36];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1863:/ clear");
	     /*clear *//*_#IS_STRING__L26*/ meltfnum[13] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V38*/ meltfptr[36] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1868:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V35*/ meltfptr[28]),
			   ("__V"));
    }
    ;
 /*_#I__L27*/ meltfnum[13] =
      (( /*_#NBVAL__L24*/ meltfnum[11]) + (1));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1869:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V35*/ meltfptr[28]),
			     ( /*_#I__L27*/ meltfnum[13]));
    }
    ;
 /*_#I__L28*/ meltfnum[27] =
      (( /*_#NBVAL__L24*/ meltfnum[11]) + (1));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1870:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.NBVALBOX__V34*/ meltfptr[23]),
		    ( /*_#I__L28*/ meltfnum[27]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1871:/ quasiblock");


 /*_.NAMSTR__V40*/ meltfptr[39] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[6])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V35*/ meltfptr[28]))));;
    /*^compute */
 /*_.OLDNBVALBOX__V41*/ meltfptr[40] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[7])),
	( /*_#NBVAL__L24*/ meltfnum[11])));;
    MELT_LOCATION ("warmelt-genobj.melt:1873:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_INTEGERBOX__V42*/ meltfptr[41] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[7])),
	( /*_#OFF__L4*/ meltfnum[2])));;
    MELT_LOCATION ("warmelt-genobj.melt:1873:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					     tabval[3])), (4),
			      "CLASS_OBJLOCV");
  /*_.INST__V44*/ meltfptr[43] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[8])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBL_OFF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (1),
			  ( /*_.MAKE_INTEGERBOX__V42*/ meltfptr[41]),
			  "OBL_OFF");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBL_PROC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (2),
			  ( /*_.OROUT__V11*/ meltfptr[7]), "OBL_PROC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBL_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V44*/ meltfptr[43])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V44*/ meltfptr[43]), (3),
			  ( /*_.NAMSTR__V40*/ meltfptr[39]), "OBL_CNAME");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V44*/ meltfptr[43],
				  "newly made instance");
    ;
    /*_.NLOC__V43*/ meltfptr[42] = /*_.INST__V44*/ meltfptr[43];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1879:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L29*/ meltfnum[28] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1879:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L29*/ meltfnum[28])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L30*/ meltfnum[29] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1879:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L30*/ meltfnum[29];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1879;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "get_free_objlocptr new nloc";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NLOC__V43*/ meltfptr[42];
	      /*_.MELT_DEBUG_FUN__V47*/ meltfptr[46] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V46*/ meltfptr[45] =
	      /*_.MELT_DEBUG_FUN__V47*/ meltfptr[46];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1879:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L30*/ meltfnum[29] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V47*/ meltfptr[46] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V46*/ meltfptr[45] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1879:/ quasiblock");


      /*_.PROGN___V48*/ meltfptr[46] = /*_.IF___V46*/ meltfptr[45];;
      /*^compute */
      /*_.IFCPP___V45*/ meltfptr[44] = /*_.PROGN___V48*/ meltfptr[46];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1879:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L29*/ meltfnum[28] = 0;
      /*^clear */
	     /*clear *//*_.IF___V46*/ meltfptr[45] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V48*/ meltfptr[46] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V45*/ meltfptr[44] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1880:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NLOC__V43*/ meltfptr[42];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1880:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V39*/ meltfptr[36] = /*_.RETURN___V49*/ meltfptr[45];;

    MELT_LOCATION ("warmelt-genobj.melt:1871:/ clear");
	   /*clear *//*_.NAMSTR__V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.OLDNBVALBOX__V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V42*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V49*/ meltfptr[45] = 0;
    /*_.LET___V33*/ meltfptr[22] = /*_.LET___V39*/ meltfptr[36];;

    MELT_LOCATION ("warmelt-genobj.melt:1859:/ clear");
	   /*clear *//*_.NBVALBOX__V34*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#NBVAL__L24*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_.NAMBUF__V35*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L25*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V36*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#I__L27*/ meltfnum[13] = 0;
    /*^clear */
	   /*clear *//*_#I__L28*/ meltfnum[27] = 0;
    /*^clear */
	   /*clear *//*_.LET___V39*/ meltfptr[36] = 0;
    /*_.LET___V10*/ meltfptr[6] = /*_.LET___V33*/ meltfptr[22];;

    MELT_LOCATION ("warmelt-genobj.melt:1840:/ clear");
	   /*clear *//*_.OROUT__V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.FREELI__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.PFREE__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#OFF__L4*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L5*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L10*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#IFELSE___L11*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V33*/ meltfptr[22] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1837:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V10*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1837:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GET_FREE_OBJLOCPTR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 45
    melt_ptr_t mcfr_varptr[45];
#define MELTFRAM_NBVARNUM 27
    long mcfr_varnum[27];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 45; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG nbval 45*/
  meltfram__.mcfr_nbvar = 45 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GET_FREE_OBJLOCLONG", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1886:/ getarg");
 /*_.GCX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NAM__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NAM__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1887:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:1887:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1887:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1887) ? (1887) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1887:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1888:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1888:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1888:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1888;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "get_free_objloclong start nam=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NAM__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1888:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1888:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1888:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1890:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
  /*_.OROUT__V11*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1891:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "GNCX_FREELONGLIST");
  /*_.FREELI__V12*/ meltfptr[11] = slot;
    };
    ;
 /*_.PFREE__V13*/ meltfptr[12] =
      (meltgc_popfirst_list
       ((melt_ptr_t) ( /*_.FREELI__V12*/ meltfptr[11])));;
    /*^compute */
 /*_#OFF__L4*/ meltfnum[2] = -1;;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1895:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OROUT__V11*/ meltfptr[7]),
			     (melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:1895:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1895:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check orout"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1895) ? (1895) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1895:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1896:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L6*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.PFREE__V13*/ meltfptr[12]),
			   (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					  tabval[3])));;
    MELT_LOCATION ("warmelt-genobj.melt:1896:/ cond");
    /*cond */ if ( /*_#IS_A__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1897:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.PFREE__V13*/ meltfptr[12]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "OBL_OFF");
    /*_.OFFPFREE__V16*/ meltfptr[14] = slot;
	  };
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1898:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L9*/ meltfnum[8] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1898:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:1898:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1898;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "get_free_objloclong pfree";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.PFREE__V13*/ meltfptr[12];
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V18*/ meltfptr[17] =
		    /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:1898:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V18*/ meltfptr[17] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:1898:/ quasiblock");


	    /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
	    /*^compute */
	    /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1898:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1899:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_INTEGERBOX__L11*/ meltfnum[9] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.OFFPFREE__V16*/ meltfptr[14])) ==
	       MELTOBMAG_INT);;
	    MELT_LOCATION ("warmelt-genobj.melt:1899:/ cond");
	    /*cond */ if ( /*_#IS_INTEGERBOX__L11*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V22*/ meltfptr[18] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-genobj.melt:1899:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check offpfree"),
					("warmelt-genobj.melt")
					? ("warmelt-genobj.melt") : __FILE__,
					(1899) ? (1899) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V21*/ meltfptr[17] = /*_.IFELSE___V22*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1899:/ clear");
	       /*clear *//*_#IS_INTEGERBOX__L11*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1901:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#GET_INT__L12*/ meltfnum[8] =
	      (melt_get_int ((melt_ptr_t) ( /*_.PFREE__V13*/ meltfptr[12])));;
	    /*^compute */
     /*_#NOT__L13*/ meltfnum[9] =
	      (!( /*_#GET_INT__L12*/ meltfnum[8]));;
	    MELT_LOCATION ("warmelt-genobj.melt:1901:/ cond");
	    /*cond */ if ( /*_#NOT__L13*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-genobj.melt:1901:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check pfree not used"),
					("warmelt-genobj.melt")
					? ("warmelt-genobj.melt") : __FILE__,
					(1901) ? (1901) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V23*/ meltfptr[18] = /*_.IFELSE___V24*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1901:/ clear");
	       /*clear *//*_#GET_INT__L12*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_#NOT__L13*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V23*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
   /*_#GET_INT__L14*/ meltfnum[8] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.OFFPFREE__V16*/ meltfptr[14])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1902:/ compute");
	  /*_#OFF__L4*/ meltfnum[2] = /*_#SETQ___L15*/ meltfnum[9] =
	    /*_#GET_INT__L14*/ meltfnum[8];;
	  /*_#LET___L8*/ meltfnum[7] = /*_#SETQ___L15*/ meltfnum[9];;

	  MELT_LOCATION ("warmelt-genobj.melt:1897:/ clear");
	     /*clear *//*_.OFFPFREE__V16*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V23*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L14*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_#SETQ___L15*/ meltfnum[9] = 0;
	  /*_#IFELSE___L7*/ meltfnum[6] = /*_#LET___L8*/ meltfnum[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1896:/ clear");
	     /*clear *//*_#LET___L8*/ meltfnum[7] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1903:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.OROUT__V11*/ meltfptr[7]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 5, "OBROUT_NBLONG");
    /*_.NBLONGOROUT__V25*/ meltfptr[23] = slot;
	  };
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:1904:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#IS_INTEGERBOX__L17*/ meltfnum[9] =
	      (melt_magic_discr
	       ((melt_ptr_t) ( /*_.NBLONGOROUT__V25*/ meltfptr[23])) ==
	       MELTOBMAG_INT);;
	    MELT_LOCATION ("warmelt-genobj.melt:1904:/ cond");
	    /*cond */ if ( /*_#IS_INTEGERBOX__L17*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V27*/ meltfptr[16] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-genobj.melt:1904:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check nblongorout"),
					("warmelt-genobj.melt")
					? ("warmelt-genobj.melt") : __FILE__,
					(1904) ? (1904) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V27*/ meltfptr[16] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V26*/ meltfptr[14] = /*_.IFELSE___V27*/ meltfptr[16];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1904:/ clear");
	       /*clear *//*_#IS_INTEGERBOX__L17*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V27*/ meltfptr[16] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V26*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
   /*_#GET_INT__L18*/ meltfnum[7] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.NBLONGOROUT__V25*/ meltfptr[23])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1905:/ compute");
	  /*_#OFF__L4*/ meltfnum[2] = /*_#SETQ___L19*/ meltfnum[9] =
	    /*_#GET_INT__L18*/ meltfnum[7];;
	  /*_#LET___L16*/ meltfnum[8] = /*_#SETQ___L19*/ meltfnum[9];;

	  MELT_LOCATION ("warmelt-genobj.melt:1903:/ clear");
	     /*clear *//*_.NBLONGOROUT__V25*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V26*/ meltfptr[14] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L18*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#SETQ___L19*/ meltfnum[9] = 0;
	  /*_#IFELSE___L7*/ meltfnum[6] = /*_#LET___L16*/ meltfnum[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1896:/ clear");
	     /*clear *//*_#LET___L16*/ meltfnum[8] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1906:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#I__L20*/ meltfnum[7] =
	(( /*_#OFF__L4*/ meltfnum[2]) >= (0));;
      MELT_LOCATION ("warmelt-genobj.melt:1906:/ cond");
      /*cond */ if ( /*_#I__L20*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V29*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1906:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check off"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1906) ? (1906) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V29*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[17] = /*_.IFELSE___V29*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1906:/ clear");
	     /*clear *//*_#I__L20*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V29*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1907:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OROUT__V11*/ meltfptr[7]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "OBROUT_NBLONG");
  /*_.NBLONGBOX__V31*/ meltfptr[23] = slot;
    };
    ;
 /*_#NBLONG__L21*/ meltfnum[9] =
      (melt_get_int ((melt_ptr_t) ( /*_.NBLONGBOX__V31*/ meltfptr[23])));;
    /*^compute */
 /*_.NAMBUF__V32*/ meltfptr[14] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[4])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-genobj.melt:1911:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L22*/ meltfnum[8] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.NAM__V3*/ meltfptr[2]),
			   (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
					  tabval[5])));;
    MELT_LOCATION ("warmelt-genobj.melt:1911:/ cond");
    /*cond */ if ( /*_#IS_A__L22*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1912:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NAM__V3*/ meltfptr[2]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V34*/ meltfptr[33] = slot;
	  };
	  ;



	  {
	    /*^locexp */
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.NAMBUF__V32*/ meltfptr[14]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.NAMED_NAME__V34*/
							meltfptr[33])));
	  }
	  ;
	     /*clear *//*_.IFELSE___V33*/ meltfptr[18] = 0;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1911:/ clear");
	     /*clear *//*_.NAMED_NAME__V34*/ meltfptr[33] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1913:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L23*/ meltfnum[7] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.NAM__V3*/ meltfptr[2])) ==
	     MELTOBMAG_STRING);;
	  MELT_LOCATION ("warmelt-genobj.melt:1913:/ cond");
	  /*cond */ if ( /*_#IS_STRING__L23*/ meltfnum[7])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {




		{
		  MELT_LOCATION ("warmelt-genobj.melt:1914:/ locexp");
		  meltgc_add_strbuf_cident ((melt_ptr_t)
					    ( /*_.NAMBUF__V32*/ meltfptr[14]),
					    melt_string_str ((melt_ptr_t)
							     ( /*_.NAM__V3*/
							      meltfptr[2])));
		}
		;
	       /*clear *//*_.IFELSE___V35*/ meltfptr[33] = 0;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-genobj.melt:1913:/ cond.else");

    /*_.IFELSE___V35*/ meltfptr[33] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IFELSE___V33*/ meltfptr[18] = /*_.IFELSE___V35*/ meltfptr[33];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1911:/ clear");
	     /*clear *//*_#IS_STRING__L23*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V35*/ meltfptr[33] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1916:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V32*/ meltfptr[14]),
			   ("__L"));
    }
    ;
 /*_#I__L24*/ meltfnum[7] =
      (( /*_#NBLONG__L21*/ meltfnum[9]) + (1));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1917:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V32*/ meltfptr[14]),
			     ( /*_#I__L24*/ meltfnum[7]));
    }
    ;
 /*_#I__L25*/ meltfnum[24] =
      (( /*_#NBLONG__L21*/ meltfnum[9]) + (1));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1918:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.NBLONGBOX__V31*/ meltfptr[23]),
		    ( /*_#I__L25*/ meltfnum[24]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1919:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_INTEGERBOX__V37*/ meltfptr[36] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[7])),
	( /*_#OFF__L4*/ meltfnum[2])));;
    /*^compute */
 /*_.STRBUF2STRING__V38*/ meltfptr[37] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[8])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V32*/ meltfptr[14]))));;
    MELT_LOCATION ("warmelt-genobj.melt:1919:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					     tabval[3])), (4),
			      "CLASS_OBJLOCV");
  /*_.INST__V40*/ meltfptr[39] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[39])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (0),
			  (( /*!CTYPE_LONG */ meltfrout->tabval[6])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBL_OFF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[39])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (1),
			  ( /*_.MAKE_INTEGERBOX__V37*/ meltfptr[36]),
			  "OBL_OFF");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBL_PROC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[39])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (2),
			  ( /*_.OROUT__V11*/ meltfptr[7]), "OBL_PROC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBL_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V40*/ meltfptr[39])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (3),
			  ( /*_.STRBUF2STRING__V38*/ meltfptr[37]),
			  "OBL_CNAME");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V40*/ meltfptr[39],
				  "newly made instance");
    ;
    /*_.NLOC__V39*/ meltfptr[38] = /*_.INST__V40*/ meltfptr[39];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1924:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L26*/ meltfnum[25] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1924:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[25])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1924:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1924;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "get_free_objloclong new nloc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NLOC__V39*/ meltfptr[38];
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V42*/ meltfptr[41] =
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1924:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V42*/ meltfptr[41] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1924:/ quasiblock");


      /*_.PROGN___V44*/ meltfptr[42] = /*_.IF___V42*/ meltfptr[41];;
      /*^compute */
      /*_.IFCPP___V41*/ meltfptr[40] = /*_.PROGN___V44*/ meltfptr[42];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1924:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[25] = 0;
      /*^clear */
	     /*clear *//*_.IF___V42*/ meltfptr[41] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V44*/ meltfptr[42] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V41*/ meltfptr[40] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1925:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NLOC__V39*/ meltfptr[38];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1925:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V36*/ meltfptr[33] = /*_.RETURN___V45*/ meltfptr[41];;

    MELT_LOCATION ("warmelt-genobj.melt:1919:/ clear");
	   /*clear *//*_.MAKE_INTEGERBOX__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V45*/ meltfptr[41] = 0;
    /*_.LET___V30*/ meltfptr[16] = /*_.LET___V36*/ meltfptr[33];;

    MELT_LOCATION ("warmelt-genobj.melt:1907:/ clear");
	   /*clear *//*_.NBLONGBOX__V31*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#NBLONG__L21*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.NAMBUF__V32*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L22*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V33*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#I__L24*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_#I__L25*/ meltfnum[24] = 0;
    /*^clear */
	   /*clear *//*_.LET___V36*/ meltfptr[33] = 0;
    /*_.LET___V10*/ meltfptr[6] = /*_.LET___V30*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-genobj.melt:1890:/ clear");
	   /*clear *//*_.OROUT__V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.FREELI__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.PFREE__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#OFF__L4*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IFELSE___L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LET___V30*/ meltfptr[16] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1886:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V10*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1886:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GET_FREE_OBJLOCLONG", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 63
    melt_ptr_t mcfr_varptr[63];
#define MELTFRAM_NBVARNUM 28
    long mcfr_varnum[28];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 63; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED nbval 63*/
  meltfram__.mcfr_nbvar = 63 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GET_FREE_OBJLOCTYPED", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1929:/ getarg");
 /*_.GCX__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.NAM__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.NAM__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.CTYP__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.CTYP__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1930:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1930:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1930:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1930;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "get_free_objloctyped nam=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NAM__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ctyp=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.CTYP__V4*/ meltfptr[3];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1930:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1930:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1930:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1931:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:1931:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1931:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1931) ? (1931) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[5] = /*_.IFELSE___V10*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1931:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1932:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CTYP__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:1932:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1932:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctyp"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1932) ? (1932) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[6] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1932:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1933:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L5*/ meltfnum[1] =
      (( /*_.CTYP__V4*/ meltfptr[3]) ==
       (( /*!CTYPE_LONG */ meltfrout->tabval[3])));;
    MELT_LOCATION ("warmelt-genobj.melt:1933:/ cond");
    /*cond */ if ( /*_#__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1934:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.NAM__V3*/ meltfptr[2];
	    /*_.GET_FREE_OBJLOCLONG__V14*/ meltfptr[13] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!GET_FREE_OBJLOCLONG */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] =
	    /*_.GET_FREE_OBJLOCLONG__V14*/ meltfptr[13];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:1934:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IFELSE___V13*/ meltfptr[11] = /*_.RETURN___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1933:/ clear");
	     /*clear *//*_.GET_FREE_OBJLOCLONG__V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:1935:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L6*/ meltfnum[0] =
	    (( /*_.CTYP__V4*/ meltfptr[3]) ==
	     (( /*!CTYPE_VALUE */ meltfrout->tabval[5])));;
	  MELT_LOCATION ("warmelt-genobj.melt:1935:/ cond");
	  /*cond */ if ( /*_#__L6*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1936:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[1];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.NAM__V3*/ meltfptr[2];
		  /*_.GET_FREE_OBJLOCPTR__V17*/ meltfptr[14] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!GET_FREE_OBJLOCPTR */ meltfrout->
				  tabval[6])),
				(melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]),
				(MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] =
		  /*_.GET_FREE_OBJLOCPTR__V17*/ meltfptr[14];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:1936:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.IFELSE___V16*/ meltfptr[13] =
		  /*_.RETURN___V18*/ meltfptr[17];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1935:/ clear");
	       /*clear *//*_.GET_FREE_OBJLOCPTR__V17*/ meltfptr[14] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V18*/ meltfptr[17] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:1938:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#__L7*/ meltfnum[6] =
		  (( /*_.CTYP__V4*/ meltfptr[3]) ==
		   (( /*!CTYPE_VOID */ meltfrout->tabval[7])));;
		MELT_LOCATION ("warmelt-genobj.melt:1938:/ cond");
		/*cond */ if ( /*_#__L7*/ meltfnum[6])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-genobj.melt:1939:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1939:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.IFELSE___V19*/ meltfptr[14] =
			/*_.RETURN___V20*/ meltfptr[17];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:1938:/ clear");
		 /*clear *//*_.RETURN___V20*/ meltfptr[17] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-genobj.melt:1941:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L8*/ meltfnum[7] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-genobj.melt:1941:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-genobj.melt:1941:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[7];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-genobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1941;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "get_free_objloctyped otherctyp nam=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.NAM__V3*/ meltfptr[2];
				/*^apply.arg */
				argtab[5].meltbp_cstring = " ctyp=";
				/*^apply.arg */
				argtab[6].meltbp_aptr =
				  (melt_ptr_t *) & /*_.CTYP__V4*/ meltfptr[3];
				/*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V22*/ meltfptr[21] =
				/*_.MELT_DEBUG_FUN__V23*/ meltfptr[22];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-genobj.melt:1941:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L9*/
				meltfnum[8] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V23*/ meltfptr[22]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V22*/ meltfptr[21] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1941:/ quasiblock");


			/*_.PROGN___V24*/ meltfptr[22] =
			  /*_.IF___V22*/ meltfptr[21];;
			/*^compute */
			/*_.IFCPP___V21*/ meltfptr[17] =
			  /*_.PROGN___V24*/ meltfptr[22];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1941:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[7] = 0;
			/*^clear */
		   /*clear *//*_.IF___V22*/ meltfptr[21] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V24*/ meltfptr[22] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1943:/ quasiblock");


		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]) /*=obj*/ ;
			melt_object_get_field (slot, obj, 4,
					       "GNCX_FREEOTHERMAPS");
	/*_.FREEMAP__V26*/ meltfptr[22] = slot;
		      };
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1944:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.GCX__V2*/ meltfptr[1]) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
	/*_.OROUT__V27*/ meltfptr[26] = slot;
		      };
		      ;
       /*_#OFF__L10*/ meltfnum[8] = -1;;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-genobj.melt:1947:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {

			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#IS_MAPOBJECT__L11*/ meltfnum[7] =
			  /*is_mapobject: */
			  (melt_magic_discr
			   ((melt_ptr_t) ( /*_.FREEMAP__V26*/ meltfptr[22]))
			   == MELTOBMAG_MAPOBJECTS);;
			MELT_LOCATION ("warmelt-genobj.melt:1947:/ cond");
			/*cond */ if ( /*_#IS_MAPOBJECT__L11*/ meltfnum[7])	/*then */
			  {
			    /*^cond.then */
			    /*_.IFELSE___V29*/ meltfptr[28] =
			      ( /*nil */ NULL);;
			  }
			else
			  {
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1947:/ cond.else");

			    /*^block */
			    /*anyblock */
			    {




			      {
				/*^locexp */
				melt_assert_failed (("check freemap"),
						    ("warmelt-genobj.melt")
						    ? ("warmelt-genobj.melt")
						    : __FILE__,
						    (1947) ? (1947) :
						    __LINE__, __FUNCTION__);
				;
			      }
			      ;
		     /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
			      /*epilog */
			    }
			    ;
			  }
			;
			/*^compute */
			/*_.IFCPP___V28*/ meltfptr[27] =
			  /*_.IFELSE___V29*/ meltfptr[28];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1947:/ clear");
		   /*clear *//*_#IS_MAPOBJECT__L11*/ meltfnum[7] = 0;
			/*^clear */
		   /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V28*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-genobj.melt:1948:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {

			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#IS_A__L12*/ meltfnum[7] =
			  melt_is_instance_of ((melt_ptr_t)
					       ( /*_.OROUT__V27*/
						meltfptr[26]),
					       (melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */ meltfrout->tabval[8])));;
			MELT_LOCATION ("warmelt-genobj.melt:1948:/ cond");
			/*cond */ if ( /*_#IS_A__L12*/ meltfnum[7])	/*then */
			  {
			    /*^cond.then */
			    /*_.IFELSE___V31*/ meltfptr[30] =
			      ( /*nil */ NULL);;
			  }
			else
			  {
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1948:/ cond.else");

			    /*^block */
			    /*anyblock */
			    {




			      {
				/*^locexp */
				melt_assert_failed (("check orout"),
						    ("warmelt-genobj.melt")
						    ? ("warmelt-genobj.melt")
						    : __FILE__,
						    (1948) ? (1948) :
						    __LINE__, __FUNCTION__);
				;
			      }
			      ;
		     /*clear *//*_.IFELSE___V31*/ meltfptr[30] = 0;
			      /*epilog */
			    }
			    ;
			  }
			;
			/*^compute */
			/*_.IFCPP___V30*/ meltfptr[28] =
			  /*_.IFELSE___V31*/ meltfptr[30];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1948:/ clear");
		   /*clear *//*_#IS_A__L12*/ meltfnum[7] = 0;
			/*^clear */
		   /*clear *//*_.IFELSE___V31*/ meltfptr[30] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V30*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1949:/ quasiblock");


       /*_.FREELI__V33*/ meltfptr[32] =
			/*mapobject_get */
			melt_get_mapobjects ((meltmapobjects_ptr_t)
					     ( /*_.FREEMAP__V26*/
					      meltfptr[22]),
					     (meltobject_ptr_t) ( /*_.CTYP__V4*/ meltfptr[3]));;
		      MELT_LOCATION
			("warmelt-genobj.melt:1950:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#NULL__L13*/ meltfnum[7] =
			(( /*_.FREELI__V33*/ meltfptr[32]) == NULL);;
		      MELT_LOCATION ("warmelt-genobj.melt:1950:/ cond");
		      /*cond */ if ( /*_#NULL__L13*/ meltfnum[7])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

	 /*_.MAKE_LIST__V34*/ meltfptr[33] =
			      (meltgc_new_list
			       ((meltobject_ptr_t)
				(( /*!DISCR_LIST */ meltfrout->tabval[9]))));;
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1952:/ compute");
			    /*_.FREELI__V33*/ meltfptr[32] =
			      /*_.SETQ___V35*/ meltfptr[34] =
			      /*_.MAKE_LIST__V34*/ meltfptr[33];;

			    {
			      MELT_LOCATION
				("warmelt-genobj.melt:1953:/ locexp");
			      meltgc_put_mapobjects ((meltmapobjects_ptr_t)
						     ( /*_.FREEMAP__V26*/
						      meltfptr[22]),
						     (meltobject_ptr_t) ( /*_.CTYP__V4*/ meltfptr[3]),
						     (melt_ptr_t) ( /*_.FREELI__V33*/ meltfptr[32]));
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1951:/ quasiblock");


			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-genobj.melt:1950:/ clear");
		   /*clear *//*_.MAKE_LIST__V34*/ meltfptr[33] = 0;
			    /*^clear */
		   /*clear *//*_.SETQ___V35*/ meltfptr[34] = 0;
			  }
			  ;
			}	/*noelse */
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1954:/ quasiblock");


       /*_.PFREE__V37*/ meltfptr[34] =
			(meltgc_popfirst_list
			 ((melt_ptr_t) ( /*_.FREELI__V33*/ meltfptr[32])));;
		      MELT_LOCATION
			("warmelt-genobj.melt:1955:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_A__L14*/ meltfnum[13] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.PFREE__V37*/ meltfptr[34]),
					     (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->tabval[10])));;
		      MELT_LOCATION ("warmelt-genobj.melt:1955:/ cond");
		      /*cond */ if ( /*_#IS_A__L14*/ meltfnum[13])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {


#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1957:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {


			      {
				/*^locexp */
				/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				melt_dbgcounter++;
#endif
				;
			      }
			      ;
			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#MELT_NEED_DBG__L16*/ meltfnum[15] =
				/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				( /*melt_need_dbg */
				 melt_need_debug ((int) 0))
#else
				0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				;;
			      MELT_LOCATION
				("warmelt-genobj.melt:1957:/ cond");
			      /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[15])	/*then */
				{
				  /*^cond.then */
				  /*^block */
				  /*anyblock */
				  {

	     /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16]
				      =
#ifdef meltcallcount
				      meltcallcount	/* the_meltcallcount */
#else
				      0L
#endif /* meltcallcount the_meltcallcount */
				      ;;
				    MELT_LOCATION
				      ("warmelt-genobj.melt:1957:/ checksignal");
				    MELT_CHECK_SIGNAL ();
				    ;
				    /*^apply */
				    /*apply */
				    {
				      union meltparam_un argtab[5];
				      memset (&argtab, 0, sizeof (argtab));
				      /*^apply.arg */
				      argtab[0].meltbp_long =
					/*_#THE_MELTCALLCOUNT__L17*/
					meltfnum[16];
				      /*^apply.arg */
				      argtab[1].meltbp_cstring =
					"warmelt-genobj.melt";
				      /*^apply.arg */
				      argtab[2].meltbp_long = 1957;
				      /*^apply.arg */
				      argtab[3].meltbp_cstring =
					"get_free_objloctyped pfree=";
				      /*^apply.arg */
				      argtab[4].meltbp_aptr =
					(melt_ptr_t *) & /*_.PFREE__V37*/
					meltfptr[34];
				      /*_.MELT_DEBUG_FUN__V40*/ meltfptr[39] =
					melt_apply ((meltclosure_ptr_t)
						    (( /*!MELT_DEBUG_FUN */
						      meltfrout->tabval[0])),
						    (melt_ptr_t) (( /*nil */
								   NULL)),
						    (MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_LONG
						     MELTBPARSTR_CSTRING
						     MELTBPARSTR_PTR ""),
						    argtab, "",
						    (union meltparam_un *) 0);
				    }
				    ;
				    /*_.IF___V39*/ meltfptr[38] =
				      /*_.MELT_DEBUG_FUN__V40*/ meltfptr[39];;
				    /*epilog */

				    MELT_LOCATION
				      ("warmelt-genobj.melt:1957:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L17*/
				      meltfnum[16] = 0;
				    /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V40*/
				      meltfptr[39] = 0;
				  }
				  ;
				}
			      else
				{	/*^cond.else */

	    /*_.IF___V39*/ meltfptr[38] = NULL;;
				}
			      ;
			      MELT_LOCATION
				("warmelt-genobj.melt:1957:/ quasiblock");


			      /*_.PROGN___V41*/ meltfptr[39] =
				/*_.IF___V39*/ meltfptr[38];;
			      /*^compute */
			      /*_.IFCPP___V38*/ meltfptr[37] =
				/*_.PROGN___V41*/ meltfptr[39];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-genobj.melt:1957:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[15]
				= 0;
			      /*^clear */
		     /*clear *//*_.IF___V39*/ meltfptr[38] = 0;
			      /*^clear */
		     /*clear *//*_.PROGN___V41*/ meltfptr[39] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V38*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;

#if MELT_HAVE_DEBUG
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1958:/ cppif.then");
			    /*^block */
			    /*anyblock */
			    {

			      /*^checksignal */
			      MELT_CHECK_SIGNAL ();
			      ;
	   /*_#GET_INT__L18*/ meltfnum[16] =
				(melt_get_int
				 ((melt_ptr_t)
				  ( /*_.PFREE__V37*/ meltfptr[34])));;
			      /*^compute */
	   /*_#NOT__L19*/ meltfnum[15] =
				(!( /*_#GET_INT__L18*/ meltfnum[16]));;
			      MELT_LOCATION
				("warmelt-genobj.melt:1958:/ cond");
			      /*cond */ if ( /*_#NOT__L19*/ meltfnum[15])	/*then */
				{
				  /*^cond.then */
				  /*_.IFELSE___V43*/ meltfptr[39] =
				    ( /*nil */ NULL);;
				}
			      else
				{
				  MELT_LOCATION
				    ("warmelt-genobj.melt:1958:/ cond.else");

				  /*^block */
				  /*anyblock */
				  {




				    {
				      /*^locexp */
				      melt_assert_failed (("check pfree was unused"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (1958) ? (1958) : __LINE__, __FUNCTION__);
				      ;
				    }
				    ;
		       /*clear *//*_.IFELSE___V43*/ meltfptr[39]
				      = 0;
				    /*epilog */
				  }
				  ;
				}
			      ;
			      /*^compute */
			      /*_.IFCPP___V42*/ meltfptr[38] =
				/*_.IFELSE___V43*/ meltfptr[39];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-genobj.melt:1958:/ clear");
		     /*clear *//*_#GET_INT__L18*/ meltfnum[16] = 0;
			      /*^clear */
		     /*clear *//*_#NOT__L19*/ meltfnum[15] = 0;
			      /*^clear */
		     /*clear *//*_.IFELSE___V43*/ meltfptr[39] = 0;
			    }

#else /*MELT_HAVE_DEBUG */
			    /*^cppif.else */
			    /*_.IFCPP___V42*/ meltfptr[38] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			    ;
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1959:/ getslot");
			    {
			      melt_ptr_t slot = NULL, obj = NULL;
			      obj =
				(melt_ptr_t) ( /*_.PFREE__V37*/ meltfptr[34])
				/*=obj*/ ;
			      melt_object_get_field (slot, obj, 1, "OBL_OFF");
	  /*_.OBL_OFF__V44*/ meltfptr[39] = slot;
			    };
			    ;
	 /*_#GET_INT__L20*/ meltfnum[16] =
			      (melt_get_int
			       ((melt_ptr_t)
				( /*_.OBL_OFF__V44*/ meltfptr[39])));;
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1959:/ compute");
			    /*_#OFF__L10*/ meltfnum[8] =
			      /*_#SETQ___L21*/ meltfnum[15] =
			      /*_#GET_INT__L20*/ meltfnum[16];;
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1956:/ quasiblock");


			    /*_#PROGN___L22*/ meltfnum[21] =
			      /*_#SETQ___L21*/ meltfnum[15];;
			    /*^compute */
			    /*_#IFELSE___L15*/ meltfnum[14] =
			      /*_#PROGN___L22*/ meltfnum[21];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-genobj.melt:1955:/ clear");
		   /*clear *//*_.IFCPP___V38*/ meltfptr[37] = 0;
			    /*^clear */
		   /*clear *//*_.IFCPP___V42*/ meltfptr[38] = 0;
			    /*^clear */
		   /*clear *//*_.OBL_OFF__V44*/ meltfptr[39] = 0;
			    /*^clear */
		   /*clear *//*_#GET_INT__L20*/ meltfnum[16] = 0;
			    /*^clear */
		   /*clear *//*_#SETQ___L21*/ meltfnum[15] = 0;
			    /*^clear */
		   /*clear *//*_#PROGN___L22*/ meltfnum[21] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-genobj.melt:1961:/ getslot");
			    {
			      melt_ptr_t slot = NULL, obj = NULL;
			      obj =
				(melt_ptr_t) ( /*_.OROUT__V27*/ meltfptr[26])
				/*=obj*/ ;
			      melt_object_get_field (slot, obj, 6,
						     "OBROUT_OTHERS");
	  /*_.OBROUT_OTHERS__V45*/ meltfptr[37] = slot;
			    };
			    ;
	 /*_#LIST_LENGTH__L23*/ meltfnum[16] =
			      (melt_list_length
			       ((melt_ptr_t)
				( /*_.OBROUT_OTHERS__V45*/ meltfptr[37])));;
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1961:/ compute");
			    /*_#OFF__L10*/ meltfnum[8] =
			      /*_#SETQ___L24*/ meltfnum[15] =
			      /*_#LIST_LENGTH__L23*/ meltfnum[16];;
			    /*_#IFELSE___L15*/ meltfnum[14] =
			      /*_#SETQ___L24*/ meltfnum[15];;
			    /*epilog */

			    MELT_LOCATION
			      ("warmelt-genobj.melt:1955:/ clear");
		   /*clear *//*_.OBROUT_OTHERS__V45*/ meltfptr[37] =
			      0;
			    /*^clear */
		   /*clear *//*_#LIST_LENGTH__L23*/ meltfnum[16] = 0;
			    /*^clear */
		   /*clear *//*_#SETQ___L24*/ meltfnum[15] = 0;
			  }
			  ;
			}
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1962:/ quasiblock");


       /*_.NAMBUF__V47*/ meltfptr[39] =
			(melt_ptr_t)
			meltgc_new_strbuf ((meltobject_ptr_t)
					   (( /*!DISCR_STRBUF */ meltfrout->
					     tabval[11])), (const char *) 0);;
		      MELT_LOCATION ("warmelt-genobj.melt:1964:/ getslot");
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) ( /*_.OROUT__V27*/ meltfptr[26])
			  /*=obj*/ ;
			melt_object_get_field (slot, obj, 6, "OBROUT_OTHERS");
	/*_.OTHERS__V48*/ meltfptr[37] = slot;
		      };
		      ;
       /*_#NBOTHERS__L25*/ meltfnum[21] =
			(melt_list_length
			 ((melt_ptr_t) ( /*_.OTHERS__V48*/ meltfptr[37])));;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-genobj.melt:1967:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {

			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#IS_LIST__L26*/ meltfnum[16] =
			  (melt_magic_discr
			   ((melt_ptr_t) ( /*_.OTHERS__V48*/ meltfptr[37])) ==
			   MELTOBMAG_LIST);;
			MELT_LOCATION ("warmelt-genobj.melt:1967:/ cond");
			/*cond */ if ( /*_#IS_LIST__L26*/ meltfnum[16])	/*then */
			  {
			    /*^cond.then */
			    /*_.IFELSE___V50*/ meltfptr[49] =
			      ( /*nil */ NULL);;
			  }
			else
			  {
			    MELT_LOCATION
			      ("warmelt-genobj.melt:1967:/ cond.else");

			    /*^block */
			    /*anyblock */
			    {




			      {
				/*^locexp */
				melt_assert_failed (("check others"),
						    ("warmelt-genobj.melt")
						    ? ("warmelt-genobj.melt")
						    : __FILE__,
						    (1967) ? (1967) :
						    __LINE__, __FUNCTION__);
				;
			      }
			      ;
		     /*clear *//*_.IFELSE___V50*/ meltfptr[49] = 0;
			      /*epilog */
			    }
			    ;
			  }
			;
			/*^compute */
			/*_.IFCPP___V49*/ meltfptr[48] =
			  /*_.IFELSE___V50*/ meltfptr[49];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1967:/ clear");
		   /*clear *//*_#IS_LIST__L26*/ meltfnum[16] = 0;
			/*^clear */
		   /*clear *//*_.IFELSE___V50*/ meltfptr[49] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V49*/ meltfptr[48] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1968:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.NAMBUF__V47*/
					      meltfptr[39]), ("loc_"));
		      }
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1971:/ cond");
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CTYP__V4*/
							   meltfptr[3]),
							  (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[2])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.CTYP__V4*/ meltfptr[3])
			      /*=obj*/ ;
			    melt_object_get_field (slot, obj, 2,
						   "CTYPE_KEYWORD");
	 /*_.CTYPE_KEYWORD__V51*/ meltfptr[49] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

	/*_.CTYPE_KEYWORD__V51*/ meltfptr[49] = NULL;;
			}
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1970:/ cond");
		      /*cond */ if (
				     /*ifisa */
				     melt_is_instance_of ((melt_ptr_t)
							  ( /*_.CTYPE_KEYWORD__V51*/ meltfptr[49]),
							  (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[12])))
			)	/*then */
			{
			  /*^cond.then */
			  /*^getslot */
			  {
			    melt_ptr_t slot = NULL, obj = NULL;
			    obj =
			      (melt_ptr_t) ( /*_.CTYPE_KEYWORD__V51*/
					    meltfptr[49]) /*=obj*/ ;
			    melt_object_get_field (slot, obj, 1,
						   "NAMED_NAME");
	 /*_.NAMED_NAME__V52*/ meltfptr[51] = slot;
			  };
			  ;
			}
		      else
			{	/*^cond.else */

	/*_.NAMED_NAME__V52*/ meltfptr[51] = NULL;;
			}
		      ;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1969:/ locexp");
			meltgc_add_strbuf_cident ((melt_ptr_t)
						  ( /*_.NAMBUF__V47*/
						   meltfptr[39]),
						  melt_string_str ((melt_ptr_t) ( /*_.NAMED_NAME__V52*/ meltfptr[51])));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1972:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.NAMBUF__V47*/
					      meltfptr[39]), ("__o"));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1973:/ locexp");
			meltgc_add_strbuf_dec ((melt_ptr_t)
					       ( /*_.NAMBUF__V47*/
						meltfptr[39]),
					       ( /*_#NBOTHERS__L25*/
						meltfnum[21]));
		      }
		      ;
		      MELT_LOCATION ("warmelt-genobj.melt:1974:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_.MAKE_INTEGERBOX__V54*/ meltfptr[53] =
			(meltgc_new_int
			 ((meltobject_ptr_t)
			  (( /*!DISCR_INTEGER */ meltfrout->tabval[13])),
			  ( /*_#OFF__L10*/ meltfnum[8])));;
		      /*^compute */
       /*_.STRBUF2STRING__V55*/ meltfptr[54] =
			(meltgc_new_stringdup
			 ((meltobject_ptr_t)
			  (( /*!DISCR_STRING */ meltfrout->tabval[14])),
			  melt_strbuf_str ((melt_ptr_t)
					   ( /*_.NAMBUF__V47*/
					    meltfptr[39]))));;
		      MELT_LOCATION ("warmelt-genobj.melt:1974:/ quasiblock");


		      /*^rawallocobj */
		      /*rawallocobj */
		      {
			melt_ptr_t newobj = 0;
			melt_raw_object_create (newobj,
						(melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->tabval[10])), (4), "CLASS_OBJLOCV");
	/*_.INST__V57*/ meltfptr[56] =
			  newobj;
		      };
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBV_TYPE",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V57*/
							 meltfptr[56])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V57*/ meltfptr[56]),
					    (0),
					    ( /*_.CTYP__V4*/ meltfptr[3]),
					    "OBV_TYPE");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBL_PROC",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V57*/
							 meltfptr[56])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V57*/ meltfptr[56]),
					    (2),
					    ( /*_.OROUT__V27*/ meltfptr[26]),
					    "OBL_PROC");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBL_OFF",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V57*/
							 meltfptr[56])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V57*/ meltfptr[56]),
					    (1),
					    ( /*_.MAKE_INTEGERBOX__V54*/
					     meltfptr[53]), "OBL_OFF");
		      ;
		      /*^putslot */
		      /*putslot */
		      melt_assertmsg ("putslot checkobj @OBL_CNAME",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.INST__V57*/
							 meltfptr[56])) ==
				      MELTOBMAG_OBJECT);
		      melt_putfield_object (( /*_.INST__V57*/ meltfptr[56]),
					    (3),
					    ( /*_.STRBUF2STRING__V55*/
					     meltfptr[54]), "OBL_CNAME");
		      ;
		      /*^touchobj */

		      melt_dbgtrace_written_object ( /*_.INST__V57*/
						    meltfptr[56],
						    "newly made instance");
		      ;
		      /*_.NLOC__V56*/ meltfptr[55] =
			/*_.INST__V57*/ meltfptr[56];;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-genobj.melt:1979:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L27*/ meltfnum[15] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-genobj.melt:1979:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L27*/ meltfnum[15])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L28*/ meltfnum[16] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-genobj.melt:1979:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L28*/ meltfnum[16];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-genobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1979;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "get_free_objloctyped new nloc";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.NLOC__V56*/
				  meltfptr[55];
				/*_.MELT_DEBUG_FUN__V60*/ meltfptr[59] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V59*/ meltfptr[58] =
				/*_.MELT_DEBUG_FUN__V60*/ meltfptr[59];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-genobj.melt:1979:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L28*/
				meltfnum[16] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V60*/ meltfptr[59]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V59*/ meltfptr[58] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-genobj.melt:1979:/ quasiblock");


			/*_.PROGN___V61*/ meltfptr[59] =
			  /*_.IF___V59*/ meltfptr[58];;
			/*^compute */
			/*_.IFCPP___V58*/ meltfptr[57] =
			  /*_.PROGN___V61*/ meltfptr[59];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:1979:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L27*/ meltfnum[15] = 0;
			/*^clear */
		   /*clear *//*_.IF___V59*/ meltfptr[58] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V61*/ meltfptr[59] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V58*/ meltfptr[57] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1980:/ locexp");
			meltgc_append_list ((melt_ptr_t)
					    ( /*_.OTHERS__V48*/ meltfptr[37]),
					    (melt_ptr_t) ( /*_.NLOC__V56*/
							  meltfptr[55]));
		      }
		      ;
		      MELT_LOCATION
			("warmelt-genobj.melt:1981:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			/*_.NLOC__V56*/ meltfptr[55];;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:1981:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.LET___V53*/ meltfptr[52] =
			/*_.RETURN___V62*/ meltfptr[58];;

		      MELT_LOCATION ("warmelt-genobj.melt:1974:/ clear");
		 /*clear *//*_.MAKE_INTEGERBOX__V54*/ meltfptr[53] = 0;
		      /*^clear */
		 /*clear *//*_.STRBUF2STRING__V55*/ meltfptr[54] = 0;
		      /*^clear */
		 /*clear *//*_.NLOC__V56*/ meltfptr[55] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V58*/ meltfptr[57] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V62*/ meltfptr[58] = 0;
		      /*_.LET___V46*/ meltfptr[38] =
			/*_.LET___V53*/ meltfptr[52];;

		      MELT_LOCATION ("warmelt-genobj.melt:1962:/ clear");
		 /*clear *//*_.NAMBUF__V47*/ meltfptr[39] = 0;
		      /*^clear */
		 /*clear *//*_.OTHERS__V48*/ meltfptr[37] = 0;
		      /*^clear */
		 /*clear *//*_#NBOTHERS__L25*/ meltfnum[21] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V49*/ meltfptr[48] = 0;
		      /*^clear */
		 /*clear *//*_.CTYPE_KEYWORD__V51*/ meltfptr[49] = 0;
		      /*^clear */
		 /*clear *//*_.NAMED_NAME__V52*/ meltfptr[51] = 0;
		      /*^clear */
		 /*clear *//*_.LET___V53*/ meltfptr[52] = 0;
		      /*_.LET___V36*/ meltfptr[33] =
			/*_.LET___V46*/ meltfptr[38];;

		      MELT_LOCATION ("warmelt-genobj.melt:1954:/ clear");
		 /*clear *//*_.PFREE__V37*/ meltfptr[34] = 0;
		      /*^clear */
		 /*clear *//*_#IS_A__L14*/ meltfnum[13] = 0;
		      /*^clear */
		 /*clear *//*_#IFELSE___L15*/ meltfnum[14] = 0;
		      /*^clear */
		 /*clear *//*_.LET___V46*/ meltfptr[38] = 0;
		      /*_.LET___V32*/ meltfptr[30] =
			/*_.LET___V36*/ meltfptr[33];;

		      MELT_LOCATION ("warmelt-genobj.melt:1949:/ clear");
		 /*clear *//*_.FREELI__V33*/ meltfptr[32] = 0;
		      /*^clear */
		 /*clear *//*_#NULL__L13*/ meltfnum[7] = 0;
		      /*^clear */
		 /*clear *//*_.LET___V36*/ meltfptr[33] = 0;
		      /*_.LET___V25*/ meltfptr[21] =
			/*_.LET___V32*/ meltfptr[30];;

		      MELT_LOCATION ("warmelt-genobj.melt:1943:/ clear");
		 /*clear *//*_.FREEMAP__V26*/ meltfptr[22] = 0;
		      /*^clear */
		 /*clear *//*_.OROUT__V27*/ meltfptr[26] = 0;
		      /*^clear */
		 /*clear *//*_#OFF__L10*/ meltfnum[8] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V28*/ meltfptr[27] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V30*/ meltfptr[28] = 0;
		      /*^clear */
		 /*clear *//*_.LET___V32*/ meltfptr[30] = 0;
		      MELT_LOCATION ("warmelt-genobj.melt:1940:/ quasiblock");


		      /*_.PROGN___V63*/ meltfptr[59] =
			/*_.LET___V25*/ meltfptr[21];;
		      /*^compute */
		      /*_.IFELSE___V19*/ meltfptr[14] =
			/*_.PROGN___V63*/ meltfptr[59];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:1938:/ clear");
		 /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
		      /*^clear */
		 /*clear *//*_.LET___V25*/ meltfptr[21] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V63*/ meltfptr[59] = 0;
		    }
		    ;
		  }
		;
		/*_.IFELSE___V16*/ meltfptr[13] =
		  /*_.IFELSE___V19*/ meltfptr[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:1935:/ clear");
	       /*clear *//*_#__L7*/ meltfnum[6] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V19*/ meltfptr[14] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V13*/ meltfptr[11] = /*_.IFELSE___V16*/ meltfptr[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:1933:/ clear");
	     /*clear *//*_#__L6*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[13] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1929:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V13*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1929:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V13*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GET_FREE_OBJLOCTYPED", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_genobj_DISPOSE_OBJLOC (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_40_warmelt_genobj_DISPOSE_OBJLOC_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_40_warmelt_genobj_DISPOSE_OBJLOC_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 34
    melt_ptr_t mcfr_varptr[34];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_40_warmelt_genobj_DISPOSE_OBJLOC is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_40_warmelt_genobj_DISPOSE_OBJLOC_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 34; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_40_warmelt_genobj_DISPOSE_OBJLOC nbval 34*/
  meltfram__.mcfr_nbvar = 34 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DISPOSE_OBJLOC", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:1989:/ getarg");
 /*_.OLDLOC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1990:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:1990:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:1990:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1990;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "dispose_objloc freeeing oldloc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OLDLOC__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:1990:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:1990:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1990:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:1991:/ locexp");

#if MELT_HAVE_DEBUG
      if (melt_need_debug (0))
	melt_dbgshortbacktrace (("dispose_objloc"), (7));
#endif
      ;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1992:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OLDLOC__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:1992:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1992:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oldloc"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1992) ? (1992) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1992:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1993:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:1993:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1993:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1993) ? (1993) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1993:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1995:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^getslot */
      {
	melt_ptr_t slot = NULL, obj = NULL;
	obj = (melt_ptr_t) ( /*_.OLDLOC__V2*/ meltfptr[1]) /*=obj*/ ;
	melt_object_get_field (slot, obj, 2, "OBL_PROC");
    /*_.OBL_PROC__V13*/ meltfptr[12] = slot;
      };
      ;
      /*^cond */
      /*cond */ if ( /*_.OBL_PROC__V13*/ meltfptr[12])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1995:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check used oldloc"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1995) ? (1995) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[10] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1995:/ clear");
	     /*clear *//*_.OBL_PROC__V13*/ meltfptr[12] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1996:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OLDLOC__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBV_TYPE");
  /*_.OLDCTY__V15*/ meltfptr[12] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1997:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#__L5*/ meltfnum[1] =
	(( /*_.OLDCTY__V15*/ meltfptr[12]) !=
	 (( /*!CTYPE_VOID */ meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-genobj.melt:1997:/ cond");
      /*cond */ if ( /*_#__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1997:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oldcty not void"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1997) ? (1997) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[13] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1997:/ clear");
	     /*clear *//*_#__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:1998:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OLDCTY__V15*/ meltfptr[12]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[4])));;
      MELT_LOCATION ("warmelt-genobj.melt:1998:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:1998:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oldcty is ctype"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (1998) ? (1998) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[16] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:1998:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:1999:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OLDLOC__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJLOCV */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBL_PROC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.OLDLOC__V2*/ meltfptr[1]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.OLDLOC__V2*/ meltfptr[1]), (2),
				(( /*nil */ NULL)), "OBL_PROC");
	  ;
	  /*^touch */
	  meltgc_touch ( /*_.OLDLOC__V2*/ meltfptr[1]);
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.OLDLOC__V2*/ meltfptr[1],
					"put-fields");
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2000:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:2000:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:2000:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2000;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = " freed oldloc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OLDLOC__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V21*/ meltfptr[20] =
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:2000:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V21*/ meltfptr[20] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:2000:/ quasiblock");


      /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[18] = /*_.PROGN___V23*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2000:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2001:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L9*/ meltfnum[0] =
      (( /*_.OLDCTY__V15*/ meltfptr[12]) ==
       (( /*!CTYPE_VALUE */ meltfrout->tabval[5])));;
    MELT_LOCATION ("warmelt-genobj.melt:2001:/ cond");
    /*cond */ if ( /*_#__L9*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:2002:/ quasiblock");


	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 2, "GNCX_FREEPTRLIST");
    /*_.FREEPL__V24*/ meltfptr[20] = slot;
	  };
	  ;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:2003:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.FREEPL__V24*/ meltfptr[20]),
				(melt_ptr_t) ( /*_.OLDLOC__V2*/ meltfptr[1]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:2002:/ clear");
	     /*clear *//*_.FREEPL__V24*/ meltfptr[20] = 0;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-genobj.melt:2001:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:2004:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L10*/ meltfnum[1] =
	    (( /*_.OLDCTY__V15*/ meltfptr[12]) ==
	     (( /*!CTYPE_LONG */ meltfrout->tabval[6])));;
	  MELT_LOCATION ("warmelt-genobj.melt:2004:/ cond");
	  /*cond */ if ( /*_#__L10*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:2005:/ quasiblock");


		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 3, "GNCX_FREELONGLIST");
      /*_.FREENL__V25*/ meltfptr[21] = slot;
		};
		;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:2006:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.FREENL__V25*/ meltfptr[21]),
				      (melt_ptr_t) ( /*_.OLDLOC__V2*/
						    meltfptr[1]));
		}
		;

		MELT_LOCATION ("warmelt-genobj.melt:2005:/ clear");
	       /*clear *//*_.FREENL__V25*/ meltfptr[21] = 0;
		/*epilog */
	      }
	      ;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-genobj.melt:2004:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:2008:/ quasiblock");


		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 4, "GNCX_FREEOTHERMAPS");
      /*_.FREEMAP__V26*/ meltfptr[20] = slot;
		};
		;
		MELT_LOCATION ("warmelt-genobj.melt:2009:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "GNCX_OBJROUT");
      /*_.OROUT__V27*/ meltfptr[21] = slot;
		};
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:2011:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#IS_MAPOBJECT__L11*/ meltfnum[10] =
		    /*is_mapobject: */
		    (melt_magic_discr
		     ((melt_ptr_t) ( /*_.FREEMAP__V26*/ meltfptr[20])) ==
		     MELTOBMAG_MAPOBJECTS);;
		  MELT_LOCATION ("warmelt-genobj.melt:2011:/ cond");
		  /*cond */ if ( /*_#IS_MAPOBJECT__L11*/ meltfnum[10])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V29*/ meltfptr[28] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-genobj.melt:2011:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check freemap"),
					      ("warmelt-genobj.melt")
					      ? ("warmelt-genobj.melt") :
					      __FILE__,
					      (2011) ? (2011) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V28*/ meltfptr[27] =
		    /*_.IFELSE___V29*/ meltfptr[28];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:2011:/ clear");
		 /*clear *//*_#IS_MAPOBJECT__L11*/ meltfnum[10] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V29*/ meltfptr[28] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V28*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:2012:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#IS_A__L12*/ meltfnum[10] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.OROUT__V27*/ meltfptr[21]),
					 (melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */ meltfrout->tabval[7])));;
		  MELT_LOCATION ("warmelt-genobj.melt:2012:/ cond");
		  /*cond */ if ( /*_#IS_A__L12*/ meltfnum[10])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V31*/ meltfptr[30] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-genobj.melt:2012:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check orout"),
					      ("warmelt-genobj.melt")
					      ? ("warmelt-genobj.melt") :
					      __FILE__,
					      (2012) ? (2012) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V31*/ meltfptr[30] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V30*/ meltfptr[28] =
		    /*_.IFELSE___V31*/ meltfptr[30];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:2012:/ clear");
		 /*clear *//*_#IS_A__L12*/ meltfnum[10] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V31*/ meltfptr[30] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V30*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-genobj.melt:2013:/ quasiblock");


     /*_.FREELI__V32*/ meltfptr[30] =
		  /*mapobject_get */
		  melt_get_mapobjects ((meltmapobjects_ptr_t)
				       ( /*_.FREEMAP__V26*/ meltfptr[20]),
				       (meltobject_ptr_t) ( /*_.OLDCTY__V15*/
							   meltfptr[12]));;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:2015:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#IS_LIST__L13*/ meltfnum[10] =
		    (melt_magic_discr
		     ((melt_ptr_t) ( /*_.FREELI__V32*/ meltfptr[30])) ==
		     MELTOBMAG_LIST);;
		  MELT_LOCATION ("warmelt-genobj.melt:2015:/ cond");
		  /*cond */ if ( /*_#IS_LIST__L13*/ meltfnum[10])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V34*/ meltfptr[33] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-genobj.melt:2015:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check freeli"),
					      ("warmelt-genobj.melt")
					      ? ("warmelt-genobj.melt") :
					      __FILE__,
					      (2015) ? (2015) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V34*/ meltfptr[33] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V33*/ meltfptr[32] =
		    /*_.IFELSE___V34*/ meltfptr[33];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:2015:/ clear");
		 /*clear *//*_#IS_LIST__L13*/ meltfnum[10] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V34*/ meltfptr[33] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V33*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:2016:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.FREELI__V32*/ meltfptr[30]),
				      (melt_ptr_t) ( /*_.OLDLOC__V2*/
						    meltfptr[1]));
		}
		;

		MELT_LOCATION ("warmelt-genobj.melt:2013:/ clear");
	       /*clear *//*_.FREELI__V32*/ meltfptr[30] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V33*/ meltfptr[32] = 0;

		MELT_LOCATION ("warmelt-genobj.melt:2008:/ clear");
	       /*clear *//*_.FREEMAP__V26*/ meltfptr[20] = 0;
		/*^clear */
	       /*clear *//*_.OROUT__V27*/ meltfptr[21] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V28*/ meltfptr[27] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V30*/ meltfptr[28] = 0;
		MELT_LOCATION ("warmelt-genobj.melt:2007:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:2001:/ clear");
	     /*clear *//*_#__L10*/ meltfnum[1] = 0;
	}
	;
      }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:1996:/ clear");
	   /*clear *//*_.OLDCTY__V15*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#__L9*/ meltfnum[0] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:1989:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DISPOSE_OBJLOC", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_40_warmelt_genobj_DISPOSE_OBJLOC_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_40_warmelt_genobj_DISPOSE_OBJLOC */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 31
    melt_ptr_t mcfr_varptr[31];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 31; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ nbval 31*/
  meltfram__.mcfr_nbvar = 31 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("DISPOSE_BND_OBJ", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:2020:/ getarg");
 /*_.BND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2021:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ANY_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:2021:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2021:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bnd"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2021) ? (2021) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2021:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2022:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:2022:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2022:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2022) ? (2022) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2022:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2023:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:2023:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:2023:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2023;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "dispose_bnd_obj start bnd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:2023:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:2023:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2023:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2024:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "GNCX_LOCMAP");
  /*_.LOCMAP__V13*/ meltfptr[9] = slot;
    };
    ;
 /*_.OLDLOC__V14*/ meltfptr[13] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.LOCMAP__V13*/ meltfptr[9]),
			   (meltobject_ptr_t) ( /*_.BND__V2*/ meltfptr[1]));;
    MELT_LOCATION ("warmelt-genobj.melt:2026:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L5*/ meltfnum[3] =
      (( /*_.OLDLOC__V14*/ meltfptr[13]) == NULL);;
    MELT_LOCATION ("warmelt-genobj.melt:2026:/ cond");
    /*cond */ if ( /*_#NULL__L5*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:2029:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L6*/ meltfnum[0] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.BND__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_LET_BINDING */
						meltfrout->tabval[3])));;
	  MELT_LOCATION ("warmelt-genobj.melt:2029:/ cond");
	  /*cond */ if ( /*_#IS_A__L6*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		MELT_LOCATION ("warmelt-genobj.melt:2030:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.BND__V2*/ meltfptr[1]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "LETBIND_TYPE");
      /*_.LETBIND_TYPE__V17*/ meltfptr[16] = slot;
		};
		;
     /*_#__L7*/ meltfnum[6] =
		  (( /*_.LETBIND_TYPE__V17*/ meltfptr[16]) ==
		   (( /*!CTYPE_VOID */ meltfrout->tabval[4])));;
		MELT_LOCATION ("warmelt-genobj.melt:2029:/ cond");
		/*cond */ if ( /*_#__L7*/ meltfnum[6])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-genobj.melt:2031:/ quasiblock");


       /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		      {
			MELT_LOCATION ("warmelt-genobj.melt:2031:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.IF___V18*/ meltfptr[17] =
			/*_.RETURN___V19*/ meltfptr[18];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:2029:/ clear");
		 /*clear *//*_.RETURN___V19*/ meltfptr[18] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IF___V18*/ meltfptr[17] = NULL;;
		  }
		;
		/*^compute */
		/*_.IF___V16*/ meltfptr[15] = /*_.IF___V18*/ meltfptr[17];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:2029:/ clear");
	       /*clear *//*_.LETBIND_TYPE__V17*/ meltfptr[16] = 0;
		/*^clear */
	       /*clear *//*_#__L7*/ meltfnum[6] = 0;
		/*^clear */
	       /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V16*/ meltfptr[15] = NULL;;
	    }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-genobj.melt:2032:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L8*/ meltfnum[6] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:2032:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[6])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:2032:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-genobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2032;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "dispose_bnd_obj nulloldloc bnd=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.BND__V2*/ meltfptr[1];
		    /*_.MELT_DEBUG_FUN__V22*/ meltfptr[17] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[2])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V21*/ meltfptr[16] =
		    /*_.MELT_DEBUG_FUN__V22*/ meltfptr[17];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:2032:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[17] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V21*/ meltfptr[16] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-genobj.melt:2032:/ quasiblock");


	    /*_.PROGN___V23*/ meltfptr[17] = /*_.IF___V21*/ meltfptr[16];;
	    /*^compute */
	    /*_.IFCPP___V20*/ meltfptr[18] = /*_.PROGN___V23*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:2032:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V21*/ meltfptr[16] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V23*/ meltfptr[17] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V20*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:2027:/ quasiblock");


	  /*_.PROGN___V24*/ meltfptr[16] = /*_.IFCPP___V20*/ meltfptr[18];;
	  /*^compute */
	  /*_.IF___V15*/ meltfptr[14] = /*_.PROGN___V24*/ meltfptr[16];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:2026:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[0] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V20*/ meltfptr[18] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[16] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V15*/ meltfptr[14] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2034:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L10*/ meltfnum[8] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OLDLOC__V14*/ meltfptr[13]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					    tabval[5])));;
      MELT_LOCATION ("warmelt-genobj.melt:2034:/ cond");
      /*cond */ if ( /*_#IS_A__L10*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V26*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2034:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oldloc"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2034) ? (2034) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V26*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V25*/ meltfptr[17] = /*_.IFELSE___V26*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2034:/ clear");
	     /*clear *//*_#IS_A__L10*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V26*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V25*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2035:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.DISPOSE_OBJLOC__V27*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!DISPOSE_OBJLOC */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.OLDLOC__V14*/ meltfptr[13]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2036:/ locexp");
      meltgc_remove_mapobjects ((meltmapobjects_ptr_t)
				( /*_.LOCMAP__V13*/ meltfptr[9]),
				(meltobject_ptr_t) ( /*_.BND__V2*/
						    meltfptr[1]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2037:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:2037:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:2037:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2037;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "dispose_bnd_obj end bnd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V29*/ meltfptr[15] =
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:2037:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V29*/ meltfptr[15] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:2037:/ quasiblock");


      /*_.PROGN___V31*/ meltfptr[29] = /*_.IF___V29*/ meltfptr[15];;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[16] = /*_.PROGN___V31*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2037:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V29*/ meltfptr[15] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V31*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V12*/ meltfptr[8] = /*_.IFCPP___V28*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-genobj.melt:2024:/ clear");
	   /*clear *//*_.LOCMAP__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OLDLOC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V25*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.DISPOSE_OBJLOC__V27*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[16] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:2020:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2020:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("DISPOSE_BND_OBJ", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 37
    melt_ptr_t mcfr_varptr[37];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 37; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK nbval 37*/
  meltfram__.mcfr_nbvar = 37 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_CHUNK", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:2041:/ getarg");
 /*_.NCHK__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2042:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCHK__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_CHUNK */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:2042:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2042:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nchk"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2042) ? (2042) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2042:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2043:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:2043:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2043:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2043) ? (2043) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2043:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2044:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:2044:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:2044:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2044;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj nrepchunk nchk=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCHK__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:2044:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:2044:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2044:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2045:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCHK__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2046:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCHK__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NCHUNK_EXPANSION");
  /*_.NEXP__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2047:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCHK__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NCHUNK_OPER");
  /*_.NOPER__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2048:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCHK__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NEXPR_CTYP");
  /*_.NTYP__V16*/ meltfptr[15] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2050:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NOPER__V15*/ meltfptr[14]),
			     (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
					    tabval[3])));;
      MELT_LOCATION ("warmelt-genobj.melt:2050:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2050:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check noper"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2050) ? (2050) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[16] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2050:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2051:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.NEXP__V14*/ meltfptr[13])) ==
	 MELTOBMAG_MULTIPLE);;
      MELT_LOCATION ("warmelt-genobj.melt:2051:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2051:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nexp"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2051) ? (2051) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[17] = /*_.IFELSE___V20*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2051:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2052:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NTYP__V16*/ meltfptr[15]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[4])));;
      MELT_LOCATION ("warmelt-genobj.melt:2052:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2052:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ntyp"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2052) ? (2052) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[19] = /*_.IFELSE___V22*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2052:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2053:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:2055:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V25*/ meltfptr[24] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_9 */ meltfrout->
						tabval[9])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V25*/ meltfptr[24])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V25*/ meltfptr[24])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V25*/ meltfptr[24])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V24*/ meltfptr[23] = /*_.LAMBDA___V25*/ meltfptr[24];;
    MELT_LOCATION ("warmelt-genobj.melt:2053:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V24*/ meltfptr[23];
      /*_.OTUP__V26*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.NEXP__V14*/ meltfptr[13]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2061:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.LOC__V13*/ meltfptr[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:2062:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJLOCATEDEXPV */
						   meltfrout->tabval[10])),
				    (3), "CLASS_OBJLOCATEDEXPV");
    /*_.INST__V29*/ meltfptr[28] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBV_TYPE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V29*/ meltfptr[28]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (0),
				( /*_.NTYP__V16*/ meltfptr[15]), "OBV_TYPE");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBX_CONT",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V29*/ meltfptr[28]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (1),
				( /*_.OTUP__V26*/ meltfptr[25]), "OBX_CONT");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBCX_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V29*/ meltfptr[28]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (2),
				( /*_.LOC__V13*/ meltfptr[9]), "OBCX_LOC");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V29*/ meltfptr[28],
					"newly made instance");
	  ;
	  /*_.INST___V28*/ meltfptr[27] = /*_.INST__V29*/ meltfptr[28];;
	  /*^compute */
	  /*_.OEXP__V27*/ meltfptr[26] = /*_.INST___V28*/ meltfptr[27];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:2061:/ clear");
	     /*clear *//*_.INST___V28*/ meltfptr[27] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:2067:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJEXPV */
						   meltfrout->tabval[11])),
				    (2), "CLASS_OBJEXPV");
    /*_.INST__V31*/ meltfptr[30] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBV_TYPE",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V31*/ meltfptr[30]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (0),
				( /*_.NTYP__V16*/ meltfptr[15]), "OBV_TYPE");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBX_CONT",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V31*/ meltfptr[30]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (1),
				( /*_.OTUP__V26*/ meltfptr[25]), "OBX_CONT");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V31*/ meltfptr[30],
					"newly made instance");
	  ;
	  /*_.INST___V30*/ meltfptr[27] = /*_.INST__V31*/ meltfptr[30];;
	  /*^compute */
	  /*_.OEXP__V27*/ meltfptr[26] = /*_.INST___V30*/ meltfptr[27];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:2061:/ clear");
	     /*clear *//*_.INST___V30*/ meltfptr[27] = 0;
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2072:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L8*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NTYP__V16*/ meltfptr[15]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[4])));;
      MELT_LOCATION ("warmelt-genobj.melt:2072:/ cond");
      /*cond */ if ( /*_#IS_A__L8*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V33*/ meltfptr[32] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2072:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ntyp"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2072) ? (2072) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V33*/ meltfptr[32] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V32*/ meltfptr[27] = /*_.IFELSE___V33*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2072:/ clear");
	     /*clear *//*_#IS_A__L8*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V33*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V32*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2073:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:2073:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:2073:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2073;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj nrepchunk oexp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OEXP__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V36*/ meltfptr[35] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V35*/ meltfptr[34] =
	      /*_.MELT_DEBUG_FUN__V36*/ meltfptr[35];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:2073:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V36*/ meltfptr[35] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V35*/ meltfptr[34] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:2073:/ quasiblock");


      /*_.PROGN___V37*/ meltfptr[35] = /*_.IF___V35*/ meltfptr[34];;
      /*^compute */
      /*_.IFCPP___V34*/ meltfptr[32] = /*_.PROGN___V37*/ meltfptr[35];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2073:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V35*/ meltfptr[34] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V37*/ meltfptr[35] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V34*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V23*/ meltfptr[21] = /*_.OEXP__V27*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-genobj.melt:2053:/ clear");
	   /*clear *//*_.LAMBDA___V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OTUP__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.OEXP__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V32*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V34*/ meltfptr[32] = 0;
    /*_.LET___V12*/ meltfptr[8] = /*_.LET___V23*/ meltfptr[21];;

    MELT_LOCATION ("warmelt-genobj.melt:2045:/ clear");
	   /*clear *//*_.LOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NEXP__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NOPER__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NTYP__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LET___V23*/ meltfptr[21] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:2041:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2041:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_CHUNK", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_genobj_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_43_warmelt_genobj_LAMBDA___17___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_43_warmelt_genobj_LAMBDA___17___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 9
    melt_ptr_t mcfr_varptr[9];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_43_warmelt_genobj_LAMBDA___17__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_43_warmelt_genobj_LAMBDA___17___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 9; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_43_warmelt_genobj_LAMBDA___17__ nbval 9*/
  meltfram__.mcfr_nbvar = 9 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:2055:/ getarg");
 /*_.COMP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2056:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:2056:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:2056:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2056;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compobj nrepchunk comp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.COMP__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:2056:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:2056:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2056:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2057:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.DISCRIM__V7*/ meltfptr[3] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.COMP__V2*/ meltfptr[1]))));;
    /*^compute */
 /*_#__L4*/ meltfnum[2] =
      (( /*_.DISCRIM__V7*/ meltfptr[3]) ==
       (( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-genobj.melt:2057:/ cond");
    /*cond */ if ( /*_#__L4*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V8*/ meltfptr[4] = /*_.COMP__V2*/ meltfptr[1];;
      }
    else
      {
	MELT_LOCATION ("warmelt-genobj.melt:2057:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:2059:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
	    /*_.COMPILE_OBJ__V9*/ meltfptr[8] =
	      meltgc_send ((melt_ptr_t) ( /*_.COMP__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
					  tabval[2])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V8*/ meltfptr[4] = /*_.COMPILE_OBJ__V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:2057:/ clear");
	     /*clear *//*_.COMPILE_OBJ__V9*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2055:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V8*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2055:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.DISCRIM__V7*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#__L4*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V8*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_43_warmelt_genobj_LAMBDA___17___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_43_warmelt_genobj_LAMBDA___17__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("GETCTYPE_TYPEXPR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:2080:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2081:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_TYPED_EXPRESSION */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:2081:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2081:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2081) ? (2081) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2081:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2082:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NEXPR_CTYP");
  /*_.NEXPR_CTYP__V6*/ meltfptr[4] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2080:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.NEXPR_CTYP__V6*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2080:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.NEXPR_CTYP__V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("GETCTYPE_TYPEXPR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 11
    melt_ptr_t mcfr_varptr[11];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 11; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL nbval 11*/
  meltfram__.mcfr_nbvar = 11 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_NIL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:2086:/ getarg");
 /*_.NILO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2087:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NILO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_NIL */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:2087:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2087:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nilo"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2087) ? (2087) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2087:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2088:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:2088:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2088:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2088) ? (2088) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2088:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2089:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJNIL */ meltfrout->
					     tabval[2])), (1),
			      "CLASS_OBJNIL");
  /*_.INST__V10*/ meltfptr[9] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V10*/ meltfptr[9])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V10*/ meltfptr[9]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[3])),
			  "OBV_TYPE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V10*/ meltfptr[9],
				  "newly made instance");
    ;
    /*_.OBNIL__V9*/ meltfptr[8] = /*_.INST__V10*/ meltfptr[9];;
    MELT_LOCATION ("warmelt-genobj.melt:2091:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OBNIL__V9*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2091:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V8*/ meltfptr[6] = /*_.RETURN___V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-genobj.melt:2089:/ clear");
	   /*clear *//*_.OBNIL__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:2086:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2086:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_NIL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 24
    melt_ptr_t mcfr_varptr[24];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 24; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT nbval 24*/
  meltfram__.mcfr_nbvar = 24 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_COMMENT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:2097:/ getarg");
 /*_.NCOMM__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2098:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:2098:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:2098:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2098;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_comment start ncomm=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCOMM__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:2098:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:2098:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2098:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2099:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCOMM__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_COMMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:2099:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2099:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ncomm"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2099) ? (2099) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2099:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2100:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:2100:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:2100:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (2100) ? (2100) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2100:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2101:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCOMM__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NCOMM_STRING");
  /*_.COMS__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2102:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCOMM__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V14*/ meltfptr[13] = slot;
    };
    ;
 /*_.SBUF__V15*/ meltfptr[14] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			 (const char *) 0);;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2104:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V15*/ meltfptr[14]),
			   ("/**!* "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2105:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t)
				  ( /*_.SBUF__V15*/ meltfptr[14]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.COMS__V13*/
						    meltfptr[12])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2106:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.SBUF__V15*/ meltfptr[14]),
			   (" *!**/"));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2107:/ quasiblock");


 /*_.OSTR__V17*/ meltfptr[16] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[4])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.SBUF__V15*/ meltfptr[14]))));;
    MELT_LOCATION ("warmelt-genobj.melt:2108:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMPUTE */
					     meltfrout->tabval[5])), (4),
			      "CLASS_OBJCOMPUTE");
  /*_.INST__V19*/ meltfptr[18] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (0),
			  ( /*_.NLOC__V14*/ meltfptr[13]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (1),
			  (( /*nil */ NULL)), "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (3),
			  (( /*!CTYPE_VOID */ meltfrout->tabval[6])),
			  "OBCPT_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (2),
			  ( /*_.OSTR__V17*/ meltfptr[16]), "OBCPT_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V19*/ meltfptr[18],
				  "newly made instance");
    ;
    /*_.RES__V18*/ meltfptr[17] = /*_.INST__V19*/ meltfptr[18];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:2114:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:2114:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:2114:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2114;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_comment ostr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OSTR__V17*/ meltfptr[16];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " res=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V18*/ meltfptr[17];
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V21*/ meltfptr[20] =
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:2114:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V21*/ meltfptr[20] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:2114:/ quasiblock");


      /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[19] = /*_.PROGN___V23*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:2114:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:2115:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V18*/ meltfptr[17];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2115:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V16*/ meltfptr[15] = /*_.RETURN___V24*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-genobj.melt:2107:/ clear");
	   /*clear *//*_.OSTR__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.RES__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V24*/ meltfptr[20] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-genobj.melt:2101:/ clear");
	   /*clear *//*_.COMS__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.SBUF__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:2097:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:2097:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_COMMENT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT */



/**** end of warmelt-genobj+01.c ****/
