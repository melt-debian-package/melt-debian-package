/* GCC MELT GENERATED FILE warmelt-outobj+02.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #2 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f2[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-outobj+02.c declarations ****/


/***************************************************
***
    Copyright (C) 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_outobj_GET_CODE_BUFFER_LIMIT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_outobj_PUT_CODE_BUFFER_LIMIT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_outobj_CODE_BUFFER_LIMIT_OPTSET (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_outobj_OUTDECLINIT_ROOT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_outobj_OUTPUCOD_OBJINIELEM (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_outobj_OUTCINITFILL_ROOT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_outobj_OUTCINITPREDEF_ROOT (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_outobj_OUTPUT_PREDEF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_outobj_OUTPUCOD_PREDEF (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_outobj_OUTPUCOD_NIL (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_outobj_OUTPUCOD_OBJEXPANDPUREVAL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_outobj_OUTDECLINIT_OBJINITOBJECT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_outobj_OUTCINITFILL_OBJINITOBJECT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_outobj_OUTCINITPREDEF_OBJINITOBJECT (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_outobj_OUTDECLINIT_OBJINITMULTIPLE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_outobj_OUTCINITFILL_OBJINITMULTIPLE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_outobj_OUTDECLINIT_OBJINITCLOSURE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_outobj_OUTCINITFILL_OBJINITCLOSURE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_outobj_OUTDECLINIT_OBJINITROUTINE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_outobj_OUTCINITFILL_OBJINITROUTINE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_outobj_OUTDECLINIT_OBJINITSTRING (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_outobj_OUTCINITFILL_OBJINITSTRING (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_outobj_OUTDECLINIT_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_outobj_OUTCINITFILL_OBJINITBOXEDINTEGER (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_outobj_OUTDECLINIT_OBJINITPAIR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_outobj_OUTCINITFILL_OBJINITPAIR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_outobj_OUTDECLINIT_OBJINITLIST (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_outobj_OUTCINITFILL_OBJINITLIST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_outobj_OUTPUCOD_ANYDISCR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_outobj_OUTPUCOD_NULL (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_outobj_OUTPUT_LOCATION (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_outobj_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_outobj_OUTPUCOD_MARKER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_outobj_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_outobj_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_outobj_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_outobj_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_outobj_OUTPUCOD_GETARG (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_outobj_OUTPUCOD_OBJLABELINSTR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_outobj_OUTPUCOD_OBJGOTOINSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_outobj_ADD2SBUF_CLONSYM (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_outobj_OUTPUCOD_OBJLOOP (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_outobj_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_outobj_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_outobj_OUTPUCOD_OBJEXIT (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_outobj_OUTPUCOD_OBJAGAIN (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_outobj_OUTPUCOD_OBJCOMPUTE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_outobj_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_outobj_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_outobj_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_outobj_OUTPUCOD_OBJCOND (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_outobj_OUTPUCOD_OBJCPPIF (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_outobj_OUTPUCOD_OBJINTERNSYMBOL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_outobj_OUTPUCOD_OBJINTERNKEYWORD (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_outobj_OUTPUCOD_OBJGETNAMEDSYMBOL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_outobj_OUTPUCOD_OBJGETNAMEDKEYWORD (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_outobj_OUTPUCOD_OBJAPPLY (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_outobj_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_outobj_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_outobj_OUTPUCOD_OBJMSEND (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_outobj_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_outobj_OUTPUCOD_OBJMULTIAPPLY (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_outobj_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_outobj_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_outobj_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_outobj_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_outobj_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_outobj_OUTPUCOD_OBJMULTIMSEND (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_outobj_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_outobj_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_outobj_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_outobj_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_outobj_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_outobj_OUTPUCOD_OBJCLEAR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_outobj_OUTPUCOD_OBJRAWALLOCOBJ (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_outobj_OUTPUCOD_OBJNEWCLOSURE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_outobj_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_outobj_OUTPUCOD_OBJTOUCH (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_outobj_OUTPUCOD_DBGTRACEWRITEOBJ (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_outobj_OUTPUCOD_OBJPUTUPLE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_outobj_OUTPUCOD_OBJPUTPAIRHEAD (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_outobj_OUTPUCOD_OBJPUTPAIRTAIL (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_outobj_OUTPUCOD_OBJPUTLIST (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_outobj_OUTPUCOD_OBJGETSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_outobj_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_outobj_OUTPUCOD_OBJPUTSLOT (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_outobj_OUTPUCOD_OBJPUTCLOSUROUT (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDV (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_outobj_OUTPUCOD_OBJPUTCLOSEDNOTNULLV (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_outobj_OUTPUCOD_OBJPUTROUTCONSTNOTNULL (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_outobj_OUTPUCOD_OBJPUTXTRARESULT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_outobj_OUTPUCOD_OBJEXPV (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_outobj_OUTPUCOD_OBJLOCATEDEXPV (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_outobj_OUTPUCOD_VERBATIMSTRING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_outobj_OUTPUCOD_STRING (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_outobj_OUTPUCOD_INTEGER (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_outobj_OUTPUCOD_FINALRETURN (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_outobj_SORTED_NAMED_DICT_TUPLE (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_outobj_OUTPUT_EXPORTED_OFFSETS (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_outobj_NTH_SECUNDARY_FILE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_outobj_OUTPUT_MELT_DESCRIPTOR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_outobj_SYNTESTGEN_ANY (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_outobj_SUBSTITUTE_FORMALS_FOR_SYNTEST (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_outobj_EXPAND_TUPLE_FOR_SYNTEST (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_outobj_SYNTESTGEN_PRIMITIVE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_outobj_SYNTESTGEN_CITERATOR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_outobj_SYNTESTGEN_CMATCHER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_outobj_EMIT_SYNTAX_TESTING_ROUTINE (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_outobj_NORMADECLB_MACROEXPANDED_LIST (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_outobj_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_outobj_TRANSLATE_MACROEXPANDED_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_outobj_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_outobj_FATAL_COMPILE_ERROR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_outobj_COMPILE_LIST_SEXPR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_outobj_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_outobj_AUTOBOX_NORMAL_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_outobj_TRANSLATE_RUN_MELT_EXPRESSIONS (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_outobj_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_outobj_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_outobj_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_outobj_READ_MELT_EXPRESSIONS (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_outobj_TRANSLATE_TO_C_MODULE_MELT_SOURCES
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_outobj_GENERATE_GPLV3PLUS_COPYRIGHT_NOTICE_C_COMMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_outobj__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_outobj__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_outobj__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_outobj__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_0 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_1 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_2 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_3 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_4 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_5 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_6 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_7 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_8 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_9 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_10 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_11 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_12 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_13 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_14 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_15 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_16 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__initialmeltchunk_17 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_outobj__forward_or_mark_module_start_frame (struct
							     melt_callframe_st
							     *fp,
							     int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_outobj__forward_or_mark_module_start_frame



/**** warmelt-outobj+02.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_outobj_OUTPUCOD_NULL (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_30_warmelt_outobj_OUTPUCOD_NULL_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_30_warmelt_outobj_OUTPUCOD_NULL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_30_warmelt_outobj_OUTPUCOD_NULL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_30_warmelt_outobj_OUTPUCOD_NULL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_30_warmelt_outobj_OUTPUCOD_NULL nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_NULL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1119:/ getarg");
 /*_.NUL__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:1120:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("NULL"));
    }
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_NULL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_30_warmelt_outobj_OUTPUCOD_NULL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_30_warmelt_outobj_OUTPUCOD_NULL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_CATCHALL_ROOT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1125:/ getarg");
 /*_.ANYR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1126:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:1126:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1126:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[6];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1126;
	      /*^apply.arg */
	      argtab[3].meltbp_aptr =
		(melt_ptr_t *) & /*_.ANYR__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[4].meltbp_cstring = "outpucod_catchall_root anyr=";
	      /*^apply.arg */
	      argtab[5].meltbp_aptr =
		(melt_ptr_t *) & /*_.ANYR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_PTR
			     MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab,
			    "", (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1126:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:1126:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V8*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1126:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1127:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "outpucod_catchall_root anyr";
      /*_.DISPLAY_DEBUG_MESSAGE__V9*/ meltfptr[5] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!DISPLAY_DEBUG_MESSAGE */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.ANYR__V2*/ meltfptr[1]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1128:/ locexp");
      melt_puts (stderr, ("* output_c_code unimplemented receiver class "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1129:/ quasiblock");


 /*_.DISCR__V10*/ meltfptr[6] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.ANYR__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-outobj.melt:1129:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DISCR__V10*/ meltfptr[6]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V11*/ meltfptr[10] = slot;
    };
    ;

    {
      /*^locexp */
      melt_putstr (stderr,
		   (melt_ptr_t) ( /*_.NAMED_NAME__V11*/ meltfptr[10]));
    }
    ;

    /*^clear */
	   /*clear *//*_.DISCR__V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V11*/ meltfptr[10] = 0;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1130:/ locexp");
      melt_newlineflush (stderr);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1131:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1131:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@@ outpucod_catchall_root not able to output"), ("warmelt-outobj.melt") ? ("warmelt-outobj.melt") : __FILE__, (1131) ? (1131) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[6] = /*_.IFELSE___V13*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1131:/ clear");
	     /*clear *//*_.IFELSE___V13*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1125:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V12*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1125:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.DISPLAY_DEBUG_MESSAGE__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_CATCHALL_ROOT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_31_warmelt_outobj_OUTPUCOD_CATCHALL_ROOT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 18
    melt_ptr_t mcfr_varptr[18];
#define MELTFRAM_NBVARNUM 14
    long mcfr_varnum[14];
/*others*/
    const char *loc_CSTRING__o0;
    const char *loc_CSTRING__o1;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 18; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION nbval 18*/
  meltfram__.mcfr_nbvar = 18 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUT_RAW_LOCATION", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1138:/ getarg");
 /*_.LOC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;

  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[2].meltbp_cstring;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:1139:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.LOC__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1141:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V3*/ meltfptr[2]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1142:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				 ("#ifndef MELTGCC_NOLINENUMBERING"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1143:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V3*/ meltfptr[2]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1144:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_MIXINT__L2*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])) ==
	     MELTOBMAG_MIXINT);;
	  MELT_LOCATION ("warmelt-outobj.melt:1144:/ cond");
	  /*cond */ if ( /*_#IS_MIXINT__L2*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:1148:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       ("#"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1149:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       ("line "));
		}
		;
     /*_#GET_INT__L3*/ meltfnum[2] =
		  (melt_get_int ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1150:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					 ( /*_#GET_INT__L3*/ meltfnum[2]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1151:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       (" \""));
		}
		;
     /*_.MIXINT_VAL__V6*/ meltfptr[5] =
		  (melt_val_mixint
		   ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1152:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       melt_string_str ((melt_ptr_t)
							( /*_.MIXINT_VAL__V6*/
							 meltfptr[5])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1153:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       ("\""));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1144:/ quasiblock");


		/*epilog */

		/*^clear */
	       /*clear *//*_#GET_INT__L3*/ meltfnum[2] = 0;
		/*^clear */
	       /*clear *//*_.MIXINT_VAL__V6*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:1154:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_MIXLOC__L4*/ meltfnum[2] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])) ==
		   MELTOBMAG_MIXLOC);;
		MELT_LOCATION ("warmelt-outobj.melt:1154:/ cond");
		/*cond */ if ( /*_#IS_MIXLOC__L4*/ meltfnum[2])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-outobj.melt:1155:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     ("#"));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:1156:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     ("line "));
		      }
		      ;
       /*_#MIXLOC_LOCLINE__L5*/ meltfnum[4] =
			(LOCATION_LINE
			 (melt_location_mixloc
			  ((melt_ptr_t) /*_.LOC__V2*/ meltfptr[1])));;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:1157:/ locexp");
			meltgc_add_strbuf_dec ((melt_ptr_t)
					       ( /*_.IMPLBUF__V3*/
						meltfptr[2]),
					       ( /*_#MIXLOC_LOCLINE__L5*/
						meltfnum[4]));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:1158:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     ("   \""));
		      }
		      ;
       /*_?*/ meltfram__.loc_CSTRING__o1 =
			(lbasename
			 (LOCATION_FILE
			  (melt_location_mixloc
			   ((melt_ptr_t) /*_.LOC__V2*/ meltfptr[1]))));;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:1159:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     ( /*_?*/ meltfram__.
					      loc_CSTRING__o1));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:1160:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     ("\""));
		      }
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:1154:/ quasiblock");


		      /*epilog */

		      /*^clear */
		 /*clear *//*_#MIXLOC_LOCLINE__L5*/ meltfnum[4] = 0;
		      /*^clear */
		 /*clear *//*_?*/ meltfram__.loc_CSTRING__o1 = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IFELSE___V7*/ meltfptr[5] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V5*/ meltfptr[4] =
		  /*_.IFELSE___V7*/ meltfptr[5];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1144:/ clear");
	       /*clear *//*_#IS_MIXLOC__L4*/ meltfnum[2] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V7*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1162:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o0)	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:1164:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       (" /**::"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1165:/ locexp");
		  /*add2sbuf_ccomconst */
		    meltgc_add_strbuf_ccomment ((melt_ptr_t)
						( /*_.IMPLBUF__V3*/
						 meltfptr[2]), /*_?*/
						meltfram__.loc_CSTRING__o0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1166:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       ("::**/"));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1163:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1168:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V3*/ meltfptr[2]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1169:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				 ("#endif /*MELTGCC_NOLINENUMBERING*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1170:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1172:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#STRBUF_USEDLENGTH__L6*/ meltfnum[4] =
	    melt_strbuf_usedlength ((melt_ptr_t)
				    ( /*_.IMPLBUF__V3*/ meltfptr[2]));;
	  MELT_LOCATION ("warmelt-outobj.melt:1172:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!BUFFER_LIMIT_CONT */
						meltfrout->tabval[0])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				 tabval[0])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V8*/ meltfptr[5] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.REFERENCED_VALUE__V8*/ meltfptr[5] = NULL;;
	    }
	  ;
	  /*^compute */
   /*_#GET_INT__L7*/ meltfnum[2] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V8*/ meltfptr[5])));;
	  /*^compute */
   /*_#IRAW__L8*/ meltfnum[7] =
	    (( /*_#GET_INT__L7*/ meltfnum[2]) / (2));;
	  /*^compute */
   /*_#I__L9*/ meltfnum[8] =
	    (( /*_#STRBUF_USEDLENGTH__L6*/ meltfnum[4]) >
	     ( /*_#IRAW__L8*/ meltfnum[7]));;
	  MELT_LOCATION ("warmelt-outobj.melt:1172:/ cond");
	  /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:1173:/ locexp");

#if MELT_HAVE_DEBUG
		  if (melt_need_debug (0))
		    melt_dbgshortbacktrace (("output_raw_location huge implbuf"), (12));
#endif
		  ;
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:1174:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L10*/ meltfnum[9] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:1174:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[9])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:1174:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 1174;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "output_raw_location huge implbuf=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.IMPLBUF__V3*/ meltfptr[2];
			  /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V11*/ meltfptr[10] =
			  /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:1174:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V11*/ meltfptr[10] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:1174:/ quasiblock");


		  /*_.PROGN___V13*/ meltfptr[11] =
		    /*_.IF___V11*/ meltfptr[10];;
		  /*^compute */
		  /*_.IFCPP___V10*/ meltfptr[9] =
		    /*_.PROGN___V13*/ meltfptr[11];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1174:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:1175:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#STRBUF_USEDLENGTH__L12*/ meltfnum[10] =
		    melt_strbuf_usedlength ((melt_ptr_t)
					    ( /*_.IMPLBUF__V3*/
					     meltfptr[2]));;
		  MELT_LOCATION ("warmelt-outobj.melt:1176:/ cond");
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[0])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[1])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
					 tabval[0])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	 /*_.REFERENCED_VALUE__V15*/ meltfptr[11] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.REFERENCED_VALUE__V15*/ meltfptr[11] = NULL;;
		    }
		  ;
		  /*^compute */
       /*_#GET_INT__L13*/ meltfnum[9] =
		    (melt_get_int
		     ((melt_ptr_t)
		      ( /*_.REFERENCED_VALUE__V15*/ meltfptr[11])));;
		  /*^compute */
       /*_#I__L14*/ meltfnum[13] =
		    (( /*_#STRBUF_USEDLENGTH__L12*/ meltfnum[10]) <
		     ( /*_#GET_INT__L13*/ meltfnum[9]));;
		  MELT_LOCATION ("warmelt-outobj.melt:1175:/ cond");
		  /*cond */ if ( /*_#I__L14*/ meltfnum[13])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:1175:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check limited implbuf"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (1175) ? (1175) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V14*/ meltfptr[10] =
		    /*_.IFELSE___V16*/ meltfptr[15];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1175:/ clear");
		 /*clear *//*_#STRBUF_USEDLENGTH__L12*/ meltfnum[10] = 0;
		  /*^clear */
		 /*clear *//*_.REFERENCED_VALUE__V15*/ meltfptr[11] = 0;
		  /*^clear */
		 /*clear *//*_#GET_INT__L13*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_#I__L14*/ meltfnum[13] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V14*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:1172:/ quasiblock");


		/*_.PROGN___V17*/ meltfptr[11] =
		  /*_.IFCPP___V14*/ meltfptr[10];;
		/*^compute */
		/*_.IFELSE___V9*/ meltfptr[8] =
		  /*_.PROGN___V17*/ meltfptr[11];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1172:/ clear");
	       /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V14*/ meltfptr[10] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V17*/ meltfptr[11] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IFELSE___V9*/ meltfptr[8] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1140:/ quasiblock");


	  /*_.PROGN___V18*/ meltfptr[15] = /*_.IFELSE___V9*/ meltfptr[8];;
	  /*^compute */
	  /*_.IF___V4*/ meltfptr[3] = /*_.PROGN___V18*/ meltfptr[15];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1139:/ clear");
	     /*clear *//*_#IS_MIXINT__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_#STRBUF_USEDLENGTH__L6*/ meltfnum[4] = 0;
	  /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V8*/ meltfptr[5] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L7*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_#IRAW__L8*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#I__L9*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V18*/ meltfptr[15] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V4*/ meltfptr[3] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1138:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IF___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1138:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUT_RAW_LOCATION", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_32_warmelt_outobj_OUTPUT_RAW_LOCATION */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LINE_AND_FILE_OF_LOCATION", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1186:/ getarg");
 /*_.LOC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:1188:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MIXINT__L1*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])) ==
       MELTOBMAG_MIXINT);;
    MELT_LOCATION ("warmelt-outobj.melt:1188:/ cond");
    /*cond */ if ( /*_#IS_MIXINT__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#GET_INT__L2*/ meltfnum[1] =
	    (melt_get_int ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;
	  /*^compute */
   /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[0])),
	      ( /*_#GET_INT__L2*/ meltfnum[1])));;
	  /*^compute */
   /*_.MIXINT_VAL__V5*/ meltfptr[4] =
	    (melt_val_mixint ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;
	  MELT_LOCATION ("warmelt-outobj.melt:1189:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] =
	    /*_.MAKE_INTEGERBOX__V4*/ meltfptr[3];;
	  MELT_LOCATION ("warmelt-outobj.melt:1189:/ putxtraresult");
	  if (!meltxrestab_ || !meltxresdescr_)
	    goto labend_rout;
	  if (meltxresdescr_[0] != MELTBPAR_PTR)
	    goto labend_rout;
	  if (meltxrestab_[0].meltbp_aptr)
	    *(meltxrestab_[0].meltbp_aptr) =
	      (melt_ptr_t) ( /*_.MIXINT_VAL__V5*/ meltfptr[4]);
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.IFELSE___V3*/ meltfptr[2] = /*_.RETURN___V6*/ meltfptr[5];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1188:/ clear");
	     /*clear *//*_#GET_INT__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_INTEGERBOX__V4*/ meltfptr[3] = 0;
	  /*^clear */
	     /*clear *//*_.MIXINT_VAL__V5*/ meltfptr[4] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V6*/ meltfptr[5] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1192:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_MIXLOC__L3*/ meltfnum[1] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])) ==
	     MELTOBMAG_MIXLOC);;
	  MELT_LOCATION ("warmelt-outobj.melt:1192:/ cond");
	  /*cond */ if ( /*_#IS_MIXLOC__L3*/ meltfnum[1])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

     /*_#MIXLOC_LOCLINE__L4*/ meltfnum[3] =
		  (LOCATION_LINE
		   (melt_location_mixloc
		    ((melt_ptr_t) /*_.LOC__V2*/ meltfptr[1])));;
		/*^compute */
     /*_.MAKE_INTEGERBOX__V8*/ meltfptr[4] =
		  (meltgc_new_int
		   ((meltobject_ptr_t)
		    (( /*!DISCR_INTEGER */ meltfrout->tabval[0])),
		    ( /*_#MIXLOC_LOCLINE__L4*/ meltfnum[3])));;
		/*^compute */
     /*_.MAKE_STRING_MIXLOC_FILE__V9*/ meltfptr[5] =
		  (meltgc_new_stringdup
		   ((meltobject_ptr_t) ( /*_.LOC__V2*/ meltfptr[1]),
		    (LOCATION_FILE
		     (melt_location_mixloc
		      ((melt_ptr_t)
		       ( /*!DISCR_STRING */ meltfrout->tabval[1]))))));;
		MELT_LOCATION ("warmelt-outobj.melt:1193:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] =
		  /*_.MAKE_INTEGERBOX__V8*/ meltfptr[4];;
		MELT_LOCATION ("warmelt-outobj.melt:1193:/ putxtraresult");
		if (!meltxrestab_ || !meltxresdescr_)
		  goto labend_rout;
		if (meltxresdescr_[0] != MELTBPAR_PTR)
		  goto labend_rout;
		if (meltxrestab_[0].meltbp_aptr)
		  *(meltxrestab_[0].meltbp_aptr) =
		    (melt_ptr_t) ( /*_.MAKE_STRING_MIXLOC_FILE__V9*/
				  meltfptr[5]);
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.IFELSE___V7*/ meltfptr[3] =
		  /*_.RETURN___V10*/ meltfptr[9];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1192:/ clear");
	       /*clear *//*_#MIXLOC_LOCLINE__L4*/ meltfnum[3] = 0;
		/*^clear */
	       /*clear *//*_.MAKE_INTEGERBOX__V8*/ meltfptr[4] = 0;
		/*^clear */
	       /*clear *//*_.MAKE_STRING_MIXLOC_FILE__V9*/ meltfptr[5] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V10*/ meltfptr[9] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:1196:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;
		MELT_LOCATION ("warmelt-outobj.melt:1196:/ putxtraresult");
		if (!meltxrestab_ || !meltxresdescr_)
		  goto labend_rout;
		if (meltxresdescr_[0] != MELTBPAR_PTR)
		  goto labend_rout;
		if (meltxrestab_[0].meltbp_aptr)
		  *(meltxrestab_[0].meltbp_aptr) =
		    (melt_ptr_t) (( /*nil */ NULL));
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-outobj.melt:1195:/ quasiblock");


		/*_.PROGN___V12*/ meltfptr[5] =
		  /*_.RETURN___V11*/ meltfptr[4];;
		/*^compute */
		/*_.IFELSE___V7*/ meltfptr[3] =
		  /*_.PROGN___V12*/ meltfptr[5];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1192:/ clear");
	       /*clear *//*_.RETURN___V11*/ meltfptr[4] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V12*/ meltfptr[5] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V3*/ meltfptr[2] = /*_.IFELSE___V7*/ meltfptr[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1188:/ clear");
	     /*clear *//*_#IS_MIXLOC__L3*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[3] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1186:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1186:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_MIXINT__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LINE_AND_FILE_OF_LOCATION", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_33_warmelt_outobj_LINE_AND_FILE_OF_LOCATION */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_outobj_OUTPUT_LOCATION (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_34_warmelt_outobj_OUTPUT_LOCATION_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_34_warmelt_outobj_OUTPUT_LOCATION_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 29
    melt_ptr_t mcfr_varptr[29];
#define MELTFRAM_NBVARNUM 26
    long mcfr_varnum[26];
/*others*/
    const char *loc_CSTRING__o0;
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_34_warmelt_outobj_OUTPUT_LOCATION is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_34_warmelt_outobj_OUTPUT_LOCATION_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 29; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_34_warmelt_outobj_OUTPUT_LOCATION nbval 29*/
  meltfram__.mcfr_nbvar = 29 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUT_LOCATION", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1201:/ getarg");
 /*_.LOC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[1].meltbp_long;

  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_CSTRING)
    goto lab_endgetargs;
 /*_?*/ meltfram__.loc_CSTRING__o0 = meltxargtab_[2].meltbp_cstring;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:1202:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!PREVLOC_CONTAINER */ meltfrout->
		       tabval[0])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.PREVLOC__V5*/ meltfptr[4] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1203:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj =
	(melt_ptr_t) (( /*!PREVIMPLBUF_CONTAINER */ meltfrout->
		       tabval[1])) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
  /*_.PREVBUF__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1205:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L2*/ meltfnum[1] =
      (( /*_.PREVBUF__V6*/ meltfptr[5]) == ( /*_.IMPLBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:1205:/ cond");
    /*cond */ if ( /*_#__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1206:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L3*/ meltfnum[2] =
	    (( /*_.PREVLOC__V5*/ meltfptr[4]) ==
	     ( /*_.LOC__V2*/ meltfptr[1]));;
	  MELT_LOCATION ("warmelt-outobj.melt:1206:/ cond");
	  /*cond */ if ( /*_#__L3*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*_#OR___L4*/ meltfnum[3] = /*_#__L3*/ meltfnum[2];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-outobj.melt:1206:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:1207:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#GET_INT__L5*/ meltfnum[4] =
		  (melt_get_int ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;
		/*^compute */
     /*_#GET_INT__L6*/ meltfnum[5] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.PREVLOC__V5*/ meltfptr[4])));;
		/*^compute */
     /*_#I__L7*/ meltfnum[6] =
		  (( /*_#GET_INT__L5*/ meltfnum[4]) ==
		   ( /*_#GET_INT__L6*/ meltfnum[5]));;
		MELT_LOCATION ("warmelt-outobj.melt:1207:/ cond");
		/*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

       /*_.MIXLOC_VAL__V8*/ meltfptr[7] =
			(melt_val_mixloc
			 ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;
		      /*^compute */
       /*_.MIXLOC_VAL__V9*/ meltfptr[8] =
			(melt_val_mixloc
			 ((melt_ptr_t) ( /*_.PREVLOC__V5*/ meltfptr[4])));;
		      /*^compute */
       /*_#__L9*/ meltfnum[8] =
			(( /*_.MIXLOC_VAL__V8*/ meltfptr[7]) ==
			 ( /*_.MIXLOC_VAL__V9*/ meltfptr[8]));;
		      /*^compute */
		      /*_#IF___L8*/ meltfnum[7] = /*_#__L9*/ meltfnum[8];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:1207:/ clear");
		 /*clear *//*_.MIXLOC_VAL__V8*/ meltfptr[7] = 0;
		      /*^clear */
		 /*clear *//*_.MIXLOC_VAL__V9*/ meltfptr[8] = 0;
		      /*^clear */
		 /*clear *//*_#__L9*/ meltfnum[8] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_#IF___L8*/ meltfnum[7] = 0;;
		  }
		;
		/*^compute */
		/*_#OR___L4*/ meltfnum[3] = /*_#IF___L8*/ meltfnum[7];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1206:/ clear");
	       /*clear *//*_#GET_INT__L5*/ meltfnum[4] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
		/*^clear */
	       /*clear *//*_#I__L7*/ meltfnum[6] = 0;
		/*^clear */
	       /*clear *//*_#IF___L8*/ meltfnum[7] = 0;
	      }
	      ;
	    }
	  ;
	  /*^cond */
	  /*cond */ if ( /*_#OR___L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:1210:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o0)	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-outobj.melt:1211:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     ("/*^"));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:1212:/ locexp");
			/*add2sbuf_ccomconst */
			  meltgc_add_strbuf_ccomment ((melt_ptr_t)
						      ( /*_.IMPLBUF__V3*/
						       meltfptr[2]), /*_?*/
						      meltfram__.
						      loc_CSTRING__o0);
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:1213:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     ("*/"));
		      }
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:1210:/ quasiblock");


		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1215:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					    (0), 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1216:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       ("#ifndef MELTGCC_NOLINENUMBERING"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1217:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					    (0), 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1218:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#IS_MIXINT__L10*/ meltfnum[8] =
		  (melt_magic_discr
		   ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])) ==
		   MELTOBMAG_MIXINT);;
		MELT_LOCATION ("warmelt-outobj.melt:1218:/ cond");
		/*cond */ if ( /*_#IS_MIXINT__L10*/ meltfnum[8])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-outobj.melt:1222:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     ("#"));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:1223:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     ("line "));
		      }
		      ;
       /*_#GET_INT__L11*/ meltfnum[4] =
			(melt_get_int
			 ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:1224:/ locexp");
			meltgc_add_strbuf_dec ((melt_ptr_t)
					       ( /*_.IMPLBUF__V3*/
						meltfptr[2]),
					       ( /*_#GET_INT__L11*/
						meltfnum[4]));
		      }
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:1218:/ quasiblock");


		      /*epilog */

		      /*^clear */
		 /*clear *//*_#GET_INT__L11*/ meltfnum[4] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-outobj.melt:1226:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
       /*_#IS_MIXLOC__L12*/ meltfnum[5] =
			(melt_magic_discr
			 ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])) ==
			 MELTOBMAG_MIXLOC);;
		      MELT_LOCATION ("warmelt-outobj.melt:1226:/ cond");
		      /*cond */ if ( /*_#IS_MIXLOC__L12*/ meltfnum[5])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {


			    {
			      MELT_LOCATION
				("warmelt-outobj.melt:1227:/ locexp");
			      /*add2sbuf_strconst */
				meltgc_add_strbuf ((melt_ptr_t)
						   ( /*_.IMPLBUF__V3*/
						    meltfptr[2]), ("#"));
			    }
			    ;

			    {
			      MELT_LOCATION
				("warmelt-outobj.melt:1228:/ locexp");
			      /*add2sbuf_strconst */
				meltgc_add_strbuf ((melt_ptr_t)
						   ( /*_.IMPLBUF__V3*/
						    meltfptr[2]), ("line "));
			    }
			    ;
	 /*_#MIXLOC_LOCLINE__L13*/ meltfnum[6] =
			      (LOCATION_LINE
			       (melt_location_mixloc
				((melt_ptr_t) /*_.LOC__V2*/ meltfptr[1])));;

			    {
			      MELT_LOCATION
				("warmelt-outobj.melt:1229:/ locexp");
			      meltgc_add_strbuf_dec ((melt_ptr_t)
						     ( /*_.IMPLBUF__V3*/
						      meltfptr[2]),
						     ( /*_#MIXLOC_LOCLINE__L13*/ meltfnum[6]));
			    }
			    ;
			    MELT_LOCATION
			      ("warmelt-outobj.melt:1226:/ quasiblock");


			    /*epilog */

			    /*^clear */
		   /*clear *//*_#MIXLOC_LOCLINE__L13*/ meltfnum[6] =
			      0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

	/*_.IFELSE___V12*/ meltfptr[11] = NULL;;
			}
		      ;
		      /*^compute */
		      /*_.IFELSE___V11*/ meltfptr[8] =
			/*_.IFELSE___V12*/ meltfptr[11];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:1218:/ clear");
		 /*clear *//*_#IS_MIXLOC__L12*/ meltfnum[5] = 0;
		      /*^clear */
		 /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
		    }
		    ;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1232:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					    (0), 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1233:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       ("#endif"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1234:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					    ( /*_#DEPTH__L1*/ meltfnum[0]),
					    0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1236:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
     /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[7] =
		  melt_strbuf_usedlength ((melt_ptr_t)
					  ( /*_.IMPLBUF__V3*/ meltfptr[2]));;
		MELT_LOCATION ("warmelt-outobj.melt:1236:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    (( /*!BUFFER_LIMIT_CONT */
						      meltfrout->tabval[2])),
						    (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				       tabval[2])) /*=obj*/ ;
		      melt_object_get_field (slot, obj, 0,
					     "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V13*/ meltfptr[11] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.REFERENCED_VALUE__V13*/ meltfptr[11] = NULL;;
		  }
		;
		/*^compute */
     /*_#GET_INT__L15*/ meltfnum[4] =
		  (melt_get_int
		   ((melt_ptr_t)
		    ( /*_.REFERENCED_VALUE__V13*/ meltfptr[11])));;
		/*^compute */
     /*_#IRAW__L16*/ meltfnum[6] =
		  (( /*_#GET_INT__L15*/ meltfnum[4]) / (2));;
		/*^compute */
     /*_#I__L17*/ meltfnum[5] =
		  (( /*_#STRBUF_USEDLENGTH__L14*/ meltfnum[7]) >
		   ( /*_#IRAW__L16*/ meltfnum[6]));;
		MELT_LOCATION ("warmelt-outobj.melt:1236:/ cond");
		/*cond */ if ( /*_#I__L17*/ meltfnum[5])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-outobj.melt:1237:/ locexp");

#if MELT_HAVE_DEBUG
			if (melt_need_debug (0))
			  melt_dbgshortbacktrace (("output_location huge implbuf"), (12));
#endif
			;
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-outobj.melt:1238:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L18*/ meltfnum[17] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-outobj.melt:1238:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[17])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-outobj.melt:1238:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-outobj.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 1238;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "output_location huge implbuf=";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.IMPLBUF__V3*/
				  meltfptr[2];
				/*_.MELT_DEBUG_FUN__V17*/ meltfptr[16] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[4])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V16*/ meltfptr[15] =
				/*_.MELT_DEBUG_FUN__V17*/ meltfptr[16];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-outobj.melt:1238:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L19*/
				meltfnum[18] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V17*/ meltfptr[16]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V16*/ meltfptr[15] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-outobj.melt:1238:/ quasiblock");


			/*_.PROGN___V18*/ meltfptr[16] =
			  /*_.IF___V16*/ meltfptr[15];;
			/*^compute */
			/*_.IFCPP___V15*/ meltfptr[14] =
			  /*_.PROGN___V18*/ meltfptr[16];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:1238:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[17] = 0;
			/*^clear */
		   /*clear *//*_.IF___V16*/ meltfptr[15] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V18*/ meltfptr[16] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-outobj.melt:1239:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {

			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#STRBUF_USEDLENGTH__L20*/ meltfnum[18] =
			  melt_strbuf_usedlength ((melt_ptr_t)
						  ( /*_.IMPLBUF__V3*/
						   meltfptr[2]));;
			MELT_LOCATION ("warmelt-outobj.melt:1240:/ cond");
			/*cond */ if (
				       /*ifisa */
				       melt_is_instance_of ((melt_ptr_t)
							    (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[2])),
							    (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
			  )	/*then */
			  {
			    /*^cond.then */
			    /*^getslot */
			    {
			      melt_ptr_t slot = NULL, obj = NULL;
			      obj =
				(melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */
					       meltfrout->
					       tabval[2])) /*=obj*/ ;
			      melt_object_get_field (slot, obj, 0,
						     "REFERENCED_VALUE");
	   /*_.REFERENCED_VALUE__V20*/ meltfptr[16] = slot;
			    };
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.REFERENCED_VALUE__V20*/ meltfptr[16] = NULL;;
			  }
			;
			/*^compute */
	 /*_#GET_INT__L21*/ meltfnum[17] =
			  (melt_get_int
			   ((melt_ptr_t)
			    ( /*_.REFERENCED_VALUE__V20*/ meltfptr[16])));;
			/*^compute */
	 /*_#I__L22*/ meltfnum[21] =
			  (( /*_#STRBUF_USEDLENGTH__L20*/ meltfnum[18]) <
			   ( /*_#GET_INT__L21*/ meltfnum[17]));;
			MELT_LOCATION ("warmelt-outobj.melt:1239:/ cond");
			/*cond */ if ( /*_#I__L22*/ meltfnum[21])	/*then */
			  {
			    /*^cond.then */
			    /*_.IFELSE___V21*/ meltfptr[20] =
			      ( /*nil */ NULL);;
			  }
			else
			  {
			    MELT_LOCATION
			      ("warmelt-outobj.melt:1239:/ cond.else");

			    /*^block */
			    /*anyblock */
			    {




			      {
				/*^locexp */
				melt_assert_failed (("check limited implbuf"),
						    ("warmelt-outobj.melt")
						    ? ("warmelt-outobj.melt")
						    : __FILE__,
						    (1239) ? (1239) :
						    __LINE__, __FUNCTION__);
				;
			      }
			      ;
		     /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
			      /*epilog */
			    }
			    ;
			  }
			;
			/*^compute */
			/*_.IFCPP___V19*/ meltfptr[15] =
			  /*_.IFELSE___V21*/ meltfptr[20];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:1239:/ clear");
		   /*clear *//*_#STRBUF_USEDLENGTH__L20*/ meltfnum[18] =
			  0;
			/*^clear */
		   /*clear *//*_.REFERENCED_VALUE__V20*/ meltfptr[16] =
			  0;
			/*^clear */
		   /*clear *//*_#GET_INT__L21*/ meltfnum[17] = 0;
			/*^clear */
		   /*clear *//*_#I__L22*/ meltfnum[21] = 0;
			/*^clear */
		   /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V19*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:1236:/ quasiblock");


		      /*_.PROGN___V22*/ meltfptr[16] =
			/*_.IFCPP___V19*/ meltfptr[15];;
		      /*^compute */
		      /*_.IFELSE___V14*/ meltfptr[13] =
			/*_.PROGN___V22*/ meltfptr[16];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:1236:/ clear");
		 /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V19*/ meltfptr[15] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V22*/ meltfptr[16] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.IFELSE___V14*/ meltfptr[13] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:1241:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1241:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-outobj.melt:1209:/ quasiblock");


		/*_.PROGN___V24*/ meltfptr[14] =
		  /*_.RETURN___V23*/ meltfptr[20];;
		/*^compute */
		/*_.IF___V10*/ meltfptr[7] = /*_.PROGN___V24*/ meltfptr[14];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1206:/ clear");
	       /*clear *//*_#IS_MIXINT__L10*/ meltfnum[8] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V11*/ meltfptr[8] = 0;
		/*^clear */
	       /*clear *//*_#STRBUF_USEDLENGTH__L14*/ meltfnum[7] = 0;
		/*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V13*/ meltfptr[11] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L15*/ meltfnum[4] = 0;
		/*^clear */
	       /*clear *//*_#IRAW__L16*/ meltfnum[6] = 0;
		/*^clear */
	       /*clear *//*_#I__L17*/ meltfnum[5] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V23*/ meltfptr[20] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V24*/ meltfptr[14] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IF___V10*/ meltfptr[7] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IF___V7*/ meltfptr[6] = /*_.IF___V10*/ meltfptr[7];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1205:/ clear");
	     /*clear *//*_#__L3*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_#OR___L4*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IF___V10*/ meltfptr[7] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V7*/ meltfptr[6] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1245:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      (( /*!PREVLOC_CONTAINER */ meltfrout->
					tabval[0]))) == MELTOBMAG_OBJECT);
    melt_putfield_object ((( /*!PREVLOC_CONTAINER */ meltfrout->tabval[0])),
			  (0), ( /*_.LOC__V2*/ meltfptr[1]),
			  "REFERENCED_VALUE");
    ;
    /*^touch */
    meltgc_touch (( /*!PREVLOC_CONTAINER */ meltfrout->tabval[0]));
    ;
    /*^touchobj */

    melt_dbgtrace_written_object (( /*!PREVLOC_CONTAINER */ meltfrout->
				   tabval[0]), "put-fields");
    ;

    MELT_LOCATION ("warmelt-outobj.melt:1246:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      (( /*!PREVIMPLBUF_CONTAINER */
					meltfrout->tabval[1]))) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object ((( /*!PREVIMPLBUF_CONTAINER */ meltfrout->
			    tabval[1])), (0),
			  ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			  "REFERENCED_VALUE");
    ;
    /*^touch */
    meltgc_touch (( /*!PREVIMPLBUF_CONTAINER */ meltfrout->tabval[1]));
    ;
    /*^touchobj */

    melt_dbgtrace_written_object (( /*!PREVIMPLBUF_CONTAINER */ meltfrout->
				   tabval[1]), "put-fields");
    ;

    MELT_LOCATION ("warmelt-outobj.melt:1249:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MIXINT__L23*/ meltfnum[18] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])) ==
       MELTOBMAG_MIXINT);;
    MELT_LOCATION ("warmelt-outobj.melt:1249:/ cond");
    /*cond */ if ( /*_#IS_MIXINT__L23*/ meltfnum[18])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1250:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				 ("MELT_LOCATION(\""));
	  }
	  ;
   /*_.MIXINT_VAL__V26*/ meltfptr[16] =
	    (melt_val_mixint ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1251:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.MIXINT_VAL__V26*/
						   meltfptr[16])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1252:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V3*/ meltfptr[2]), (":"));
	  }
	  ;
   /*_#GET_INT__L24*/ meltfnum[17] =
	    (melt_get_int ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1253:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				   ( /*_#GET_INT__L24*/ meltfnum[17]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1254:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o0)	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:1255:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       (":/ "));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1256:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       ( /*_?*/ meltfram__.loc_CSTRING__o0));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1254:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1258:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V3*/ meltfptr[2]), ("\");"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1249:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.MIXINT_VAL__V26*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L24*/ meltfnum[17] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1260:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_MIXLOC__L25*/ meltfnum[21] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])) ==
	     MELTOBMAG_MIXLOC);;
	  MELT_LOCATION ("warmelt-outobj.melt:1260:/ cond");
	  /*cond */ if ( /*_#IS_MIXLOC__L25*/ meltfnum[21])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:1261:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       ("MELT_LOCATION(\""));
		}
		;
     /*_.MIXLOC_VAL__V28*/ meltfptr[11] =
		  (melt_val_mixloc
		   ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1262:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       melt_string_str ((melt_ptr_t)
							( /*_.MIXLOC_VAL__V28*/ meltfptr[11])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1263:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       (":"));
		}
		;
     /*_#GET_INT__L26*/ meltfnum[8] =
		  (melt_get_int ((melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1264:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					 ( /*_#GET_INT__L26*/ meltfnum[8]));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1265:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_?*/ meltfram__.loc_CSTRING__o0)	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-outobj.melt:1266:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     (":/ "));
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:1267:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V3*/ meltfptr[2]),
					     ( /*_?*/ meltfram__.
					      loc_CSTRING__o0));
		      }
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:1265:/ quasiblock");


		      /*epilog */
		    }
		    ;
		  }		/*noelse */
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1269:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				       ("\");"));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1260:/ quasiblock");


		/*epilog */

		/*^clear */
	       /*clear *//*_.MIXLOC_VAL__V28*/ meltfptr[11] = 0;
		/*^clear */
	       /*clear *//*_#GET_INT__L26*/ meltfnum[8] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IFELSE___V27*/ meltfptr[8] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IFELSE___V25*/ meltfptr[15] = /*_.IFELSE___V27*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1249:/ clear");
	     /*clear *//*_#IS_MIXLOC__L25*/ meltfnum[21] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V27*/ meltfptr[8] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1272:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = /*_?*/ meltfram__.loc_CSTRING__o0;
      /*_.OUTPUT_RAW_LOCATION__V29*/ meltfptr[13] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_RAW_LOCATION */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.LOC__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V4*/ meltfptr[3] = /*_.OUTPUT_RAW_LOCATION__V29*/ meltfptr[13];;

    MELT_LOCATION ("warmelt-outobj.melt:1202:/ clear");
	   /*clear *//*_.PREVLOC__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.PREVBUF__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#IS_MIXINT__L23*/ meltfnum[18] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V25*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_RAW_LOCATION__V29*/ meltfptr[13] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:1201:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1201:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUT_LOCATION", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_34_warmelt_outobj_OUTPUT_LOCATION_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_34_warmelt_outobj_OUTPUT_LOCATION */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUT_CURFRAME_DECLSTRUCT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1277:/ getarg");
 /*_.ROU__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DSBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1278:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:1278:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1278:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1278;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "output_curframe_declstruct rou=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ROU__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1278:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:1278:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1278:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1279:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:1280:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBROUT_BODY");
  /*_.OBODY__V8*/ meltfptr[4] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1281:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBROUT_NBVAL");
  /*_.ONBVAL__V9*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1282:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "OBROUT_NBLONG");
  /*_.ONBLONG__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_#NBVAL__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.ONBVAL__V9*/ meltfptr[5])));;
    /*^compute */
 /*_#NBLONG__L4*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.ONBLONG__V10*/ meltfptr[9])));;
    /*^compute */
 /*_#IS_A__L5*/ meltfnum[4] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					  meltfrout->tabval[2])));;
    MELT_LOCATION ("warmelt-outobj.melt:1286:/ cond");
    /*cond */ if ( /*_#IS_A__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*_#ISINITIAL__L6*/ meltfnum[5] = /*_#IS_A__L5*/ meltfnum[4];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:1286:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_A__L7*/ meltfnum[6] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_INITIAL_EXTENSION_ROUTINEOBJ */ meltfrout->tabval[1])));;
	  /*^compute */
	  /*_#ISINITIAL__L6*/ meltfnum[5] = /*_#IS_A__L7*/ meltfnum[6];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1286:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[6] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1289:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "OBROUT_OTHERS");
  /*_.OTHERS__V11*/ meltfptr[10] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1292:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1293:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   (" struct "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1294:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L8*/ meltfnum[6] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
					  tabval[3])));;
    MELT_LOCATION ("warmelt-outobj.melt:1294:/ cond");
    /*cond */ if ( /*_#IS_A__L8*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1296:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("frame_"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1297:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "NAMED_NAME");
    /*_.NAMED_NAME__V12*/ meltfptr[11] = slot;
	  };
	  ;

	  {
	    /*^locexp */
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.NAMED_NAME__V12*/
						   meltfptr[11])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1298:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("_st "));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1295:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1294:/ clear");
	     /*clear *//*_.NAMED_NAME__V12*/ meltfptr[11] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1299:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("{"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1300:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1301:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("  int mcfr_nbvar;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1302:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1303:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("#if MELT_HAVE_DEBUG"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1304:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1305:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("  const char* mcfr_flocs;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1306:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1307:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("#else /*!MELT_HAVE_DEBUG*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1308:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1309:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("  const char* mcfr_unusedflocs;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1310:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1311:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("#endif /*MELT_HAVE_DEBUG*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1312:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1315:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#ISINITIAL__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1316:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 (" void (*mcfr_initforwmarkrout) ( struct melt_callframe_st*, int);"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:1315:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1318:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("  struct meltclosure_st *mcfr_clos;"));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1319:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1320:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("  struct excepth_melt_st *mcfr_exh;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1321:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1322:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("  struct melt_callframe_st *mcfr_prev;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1323:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1324:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("#undef MELTFRAM_NBVARPTR"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1325:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1326:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("#undef MELTFRAM_NBVARNUM"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1327:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1328:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L9*/ meltfnum[8] =
      (( /*_#NBVAL__L3*/ meltfnum[1]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:1328:/ cond");
    /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1330:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("#define MELTFRAM_NBVARPTR "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1331:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.DSBUF__V3*/ meltfptr[2]),
				   ( /*_#NBVAL__L3*/ meltfnum[1]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1332:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DSBUF__V3*/ meltfptr[2]), (0), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1333:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 (" melt_ptr_t mcfr_varptr["));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1334:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.DSBUF__V3*/ meltfptr[2]),
				   ( /*_#NBVAL__L3*/ meltfnum[1]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1335:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("];"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1336:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DSBUF__V3*/ meltfptr[2]), (0), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1329:/ quasiblock");


	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:1328:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1338:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("/*no varptr*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1339:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DSBUF__V3*/ meltfptr[2]), (0), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1340:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("#define MELTFRAM_NBVARPTR /*none*/0"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1341:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DSBUF__V3*/ meltfptr[2]), (0), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1337:/ quasiblock");


	  /*epilog */
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1342:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L10*/ meltfnum[9] =
      (( /*_#NBLONG__L4*/ meltfnum[0]) > (0));;
    MELT_LOCATION ("warmelt-outobj.melt:1342:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1344:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("#define MELTFRAM_NBVARNUM "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1345:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.DSBUF__V3*/ meltfptr[2]),
				   ( /*_#NBLONG__L4*/ meltfnum[0]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1346:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DSBUF__V3*/ meltfptr[2]), (0), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1347:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("  long mcfr_varnum["));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1348:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.DSBUF__V3*/ meltfptr[2]),
				   ( /*_#NBLONG__L4*/ meltfnum[0]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1349:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("];"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1350:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DSBUF__V3*/ meltfptr[2]), (0), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1343:/ quasiblock");


	  /*epilog */
	}
	;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:1342:/ cond.else");

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1352:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("/*no varnum*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1353:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DSBUF__V3*/ meltfptr[2]), (0), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1354:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("#define MELTFRAM_NBVARNUM /*none*/0"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1355:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DSBUF__V3*/ meltfptr[2]), (0), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1351:/ quasiblock");


	  /*epilog */
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1356:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OTHERS__V11*/ meltfptr[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1358:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("/*others*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1359:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DSBUF__V3*/ meltfptr[2]), (0), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1362:/ quasiblock");


	  /*^newclosure */
		   /*newclosure *//*_.LAMBDA___V15*/ meltfptr[14] =
	    (melt_ptr_t)
	    meltgc_new_closure ((meltobject_ptr_t)
				(((melt_ptr_t)
				  (MELT_PREDEF (DISCR_CLOSURE)))),
				(meltroutine_ptr_t) (( /*!konst_10 */
						      meltfrout->tabval[10])),
				(1));
	  ;
	  /*^putclosedv */
	  /*putclosv */
	  melt_assertmsg ("putclosv checkclo",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.LAMBDA___V15*/
					     meltfptr[14])) ==
			  MELTOBMAG_CLOSURE);
	  melt_assertmsg ("putclosv checkoff", 0 >= 0
			  && 0 <
			  melt_closure_size ((melt_ptr_t)
					     ( /*_.LAMBDA___V15*/
					      meltfptr[14])));
	  ((meltclosure_ptr_t) /*_.LAMBDA___V15*/ meltfptr[14])->tabval[0] =
	    (melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]);
	  ;
	  /*_.LAMBDA___V14*/ meltfptr[13] = /*_.LAMBDA___V15*/ meltfptr[14];;
	  MELT_LOCATION ("warmelt-outobj.melt:1360:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.LAMBDA___V14*/ meltfptr[13];
	    /*_.LIST_EVERY__V16*/ meltfptr[15] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_EVERY */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.OTHERS__V11*/ meltfptr[10]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1357:/ quasiblock");


	  /*_.PROGN___V17*/ meltfptr[16] =
	    /*_.LIST_EVERY__V16*/ meltfptr[15];;
	  /*^compute */
	  /*_.IFELSE___V13*/ meltfptr[11] = /*_.PROGN___V17*/ meltfptr[16];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1356:/ clear");
	     /*clear *//*_.LAMBDA___V14*/ meltfptr[13] = 0;
	  /*^clear */
	     /*clear *//*_.LIST_EVERY__V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[16] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1382:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				 ("/*no others*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1383:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DSBUF__V3*/ meltfptr[2]), (0), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1381:/ quasiblock");


	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1385:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("  long _spare_; }"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1386:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:1279:/ clear");
	   /*clear *//*_.OBODY__V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.ONBVAL__V9*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.ONBLONG__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#NBVAL__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#NBLONG__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#ISINITIAL__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.OTHERS__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L8*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#I__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V13*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:1277:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUT_CURFRAME_DECLSTRUCT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_35_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_outobj_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_36_warmelt_outobj_LAMBDA___1___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_36_warmelt_outobj_LAMBDA___1___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_36_warmelt_outobj_LAMBDA___1__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_36_warmelt_outobj_LAMBDA___1___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_36_warmelt_outobj_LAMBDA___1__ nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1362:/ getarg");
 /*_.OLOC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1363:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OLOC__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:1363:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1363:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check other oloc"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1363) ? (1363) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1363:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1364:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OLOC__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBV_TYPE");
  /*_.OCTYP__V5*/ meltfptr[3] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1365:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OLOC__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBL_CNAME");
  /*_.ONAME__V6*/ meltfptr[5] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1367:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OCTYP__V5*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:1367:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1367:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check octyp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1367) ? (1367) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[6] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1367:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1368:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCTYP__V5*/ meltfptr[3]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "CTYPE_CNAME");
  /*_.CTYPE_CNAME__V9*/ meltfptr[7] = slot;
    };
    ;

    {
      /*^locexp */
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) (( /*~DSBUF */ meltfclos->tabval[0])),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CTYPE_CNAME__V9*/
					     meltfptr[7])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1369:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) (( /*~DSBUF */ meltfclos->tabval[0])),
			   (" "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1370:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) (( /*~DSBUF */ meltfclos->tabval[0])),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V6*/ meltfptr[5])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1371:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) (( /*~DSBUF */ meltfclos->tabval[0])),
			   (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1372:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t)
				(( /*~DSBUF */ meltfclos->tabval[0])), (0),
				0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:1364:/ clear");
	   /*clear *//*_.OCTYP__V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.ONAME__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.CTYPE_CNAME__V9*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:1374:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[0] =
      melt_strbuf_usedlength ((melt_ptr_t)
			      (( /*~DSBUF */ meltfclos->tabval[0])));;
    MELT_LOCATION ("warmelt-outobj.melt:1374:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[2])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V10*/ meltfptr[3] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V10*/ meltfptr[3] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L4*/ meltfnum[3] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V10*/ meltfptr[3])));;
    /*^compute */
 /*_#IRAW__L5*/ meltfnum[4] =
      (( /*_#GET_INT__L4*/ meltfnum[3]) / (2));;
    /*^compute */
 /*_#I__L6*/ meltfnum[5] =
      (( /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[0]) >
       ( /*_#IRAW__L5*/ meltfnum[4]));;
    MELT_LOCATION ("warmelt-outobj.melt:1374:/ cond");
    /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1375:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("output_curframe_declstruct huge dsbuf"), (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1376:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L7*/ meltfnum[6] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1376:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[6])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:1376:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1376;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "output_curframe_declstruct huge dsbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & ( /*~DSBUF */ meltfclos->tabval[0]);
		    /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[4])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V13*/ meltfptr[7] =
		    /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1376:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V13*/ meltfptr[7] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:1376:/ quasiblock");


	    /*_.PROGN___V15*/ meltfptr[13] = /*_.IF___V13*/ meltfptr[7];;
	    /*^compute */
	    /*_.IFCPP___V12*/ meltfptr[6] = /*_.PROGN___V15*/ meltfptr[13];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1376:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V13*/ meltfptr[7] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V15*/ meltfptr[13] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V12*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1377:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L9*/ meltfnum[7] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      (( /*~DSBUF */ meltfclos->tabval[0])));;
	    MELT_LOCATION ("warmelt-outobj.melt:1378:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[2])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[2])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V17*/ meltfptr[13] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V17*/ meltfptr[13] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L10*/ meltfnum[6] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V17*/ meltfptr[13])));;
	    /*^compute */
     /*_#I__L11*/ meltfnum[10] =
	      (( /*_#STRBUF_USEDLENGTH__L9*/ meltfnum[7]) <
	       ( /*_#GET_INT__L10*/ meltfnum[6]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1377:/ cond");
	    /*cond */ if ( /*_#I__L11*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:1377:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited dsbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(1377) ? (1377) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V16*/ meltfptr[7] = /*_.IFELSE___V18*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1377:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L9*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V17*/ meltfptr[13] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L10*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V16*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1374:/ quasiblock");


	  /*_.PROGN___V19*/ meltfptr[13] = /*_.IFCPP___V16*/ meltfptr[7];;
	  /*^compute */
	  /*_.IFELSE___V11*/ meltfptr[5] = /*_.PROGN___V19*/ meltfptr[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1374:/ clear");
	     /*clear *//*_.IFCPP___V12*/ meltfptr[6] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V16*/ meltfptr[7] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V19*/ meltfptr[13] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V11*/ meltfptr[5] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1362:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V11*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1362:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L3*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V10*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V11*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_36_warmelt_outobj_LAMBDA___1___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_36_warmelt_outobj_LAMBDA___1__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_outobj_OUTPUCOD_MARKER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_37_warmelt_outobj_OUTPUCOD_MARKER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_37_warmelt_outobj_OUTPUCOD_MARKER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_37_warmelt_outobj_OUTPUCOD_MARKER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_37_warmelt_outobj_OUTPUCOD_MARKER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_37_warmelt_outobj_OUTPUCOD_MARKER nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_MARKER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1392:/ getarg");
 /*_.ROU__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1393:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:1393:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1393:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check rou"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1393) ? (1393) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1393:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1394:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.ROU__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 6, "OBROUT_OTHERS");
   /*_.OTHERS__V7*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OTHERS__V7*/ meltfptr[6] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1395:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.ROU__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 4, "OBROUT_NBVAL");
   /*_.OBROUT_NBVAL__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OBROUT_NBVAL__V8*/ meltfptr[7] = NULL;;
      }
    ;
    /*^compute */
 /*_#NBVAL__L2*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.OBROUT_NBVAL__V8*/ meltfptr[7])));;
    MELT_LOCATION ("warmelt-outobj.melt:1397:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L3*/ meltfnum[2] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.ROU__V2*/ meltfptr[1]),
			    (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					   meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:1397:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L3*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1399:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!CTYPE_VALUE */ meltfrout->
						tabval[2])),
					      (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->tabval[3])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!CTYPE_VALUE */ meltfrout->
				 tabval[2])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 8, "CTYPE_MARKER");
     /*_.CTYPE_MARKER__V9*/ meltfptr[8] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.CTYPE_MARKER__V9*/ meltfptr[8] = NULL;;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1399:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.CTYPE_MARKER__V9*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1400:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				 (" (meltframptr_->mcfr_clos);"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1401:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V3*/ meltfptr[2]), (3),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1398:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1397:/ clear");
	     /*clear *//*_.CTYPE_MARKER__V9*/ meltfptr[8] = 0;
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1402:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   ("for(ix=0; ix<"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1403:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			     ( /*_#NBVAL__L2*/ meltfnum[0]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1404:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   ("; ix++)"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1405:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(4), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1406:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   ("if (meltframptr_->mcfr_varptr[ix])"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1407:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(5), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1408:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!CTYPE_VALUE */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*!CLASS_CTYPE */
						       meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!CTYPE_VALUE */ meltfrout->
			   tabval[2])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 8, "CTYPE_MARKER");
   /*_.CTYPE_MARKER__V10*/ meltfptr[8] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CTYPE_MARKER__V10*/ meltfptr[8] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1408:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.CTYPE_MARKER__V10*/
					     meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1409:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   (" (meltframptr_->mcfr_varptr[ix]);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1410:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(3), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1413:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V12*/ meltfptr[11] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_11 */ meltfrout->
						tabval[11])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V12*/ meltfptr[11])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V12*/ meltfptr[11])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V12*/ meltfptr[11])->tabval[0] =
      (melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V11*/ meltfptr[10] = /*_.LAMBDA___V12*/ meltfptr[11];;
    MELT_LOCATION ("warmelt-outobj.melt:1411:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V11*/ meltfptr[10];
      /*_.LIST_EVERY__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.OTHERS__V7*/ meltfptr[6]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.LET___V6*/ meltfptr[4] = /*_.LIST_EVERY__V13*/ meltfptr[12];;

    MELT_LOCATION ("warmelt-outobj.melt:1394:/ clear");
	   /*clear *//*_.OTHERS__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OBROUT_NBVAL__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NBVAL__L2*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.CTYPE_MARKER__V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V13*/ meltfptr[12] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:1392:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V6*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1392:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_MARKER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_37_warmelt_outobj_OUTPUCOD_MARKER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_37_warmelt_outobj_OUTPUCOD_MARKER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_outobj_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_38_warmelt_outobj_LAMBDA___2___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_38_warmelt_outobj_LAMBDA___2___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_38_warmelt_outobj_LAMBDA___2__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_38_warmelt_outobj_LAMBDA___2___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_38_warmelt_outobj_LAMBDA___2__ nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1413:/ getarg");
 /*_.OLOC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1414:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OLOC__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:1414:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1414:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check other oloc"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1414) ? (1414) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1414:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1415:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OLOC__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJVALUE */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OLOC__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "OBV_TYPE");
   /*_.OCTYP__V6*/ meltfptr[5] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OCTYP__V6*/ meltfptr[5] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1416:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OLOC__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJLOCV */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OLOC__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "OBL_CNAME");
   /*_.ONAME__V7*/ meltfptr[6] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ONAME__V7*/ meltfptr[6] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1417:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.OCTYP__V6*/ meltfptr[5]),
					(melt_ptr_t) (( /*!CLASS_CTYPE */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.OCTYP__V6*/ meltfptr[5]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 8, "CTYPE_MARKER");
   /*_.OMARKER__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OMARKER__V8*/ meltfptr[7] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1419:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OCTYP__V6*/ meltfptr[5]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:1419:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1419:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check octyp"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1419) ? (1419) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1419:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1421:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L3*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.OMARKER__V8*/ meltfptr[7])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-outobj.melt:1421:/ cond");
    /*cond */ if ( /*_#IS_STRING__L3*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1422:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~IMPLBUF */ meltfclos->tabval[0])),
				 ("if (meltframptr_->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1423:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~IMPLBUF */ meltfclos->tabval[0])),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.ONAME__V7*/
						   meltfptr[6])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1424:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~IMPLBUF */ meltfclos->tabval[0])),
				 (") "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1425:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~IMPLBUF */ meltfclos->tabval[0])),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.OMARKER__V8*/
						   meltfptr[7])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1426:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~IMPLBUF */ meltfclos->tabval[0])),
				 (" (meltframptr_->"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1427:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~IMPLBUF */ meltfclos->tabval[0])),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.ONAME__V7*/
						   meltfptr[6])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1428:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 (( /*~IMPLBUF */ meltfclos->tabval[0])),
				 (");"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1429:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      (( /*~IMPLBUF */ meltfclos->tabval[0])),
				      (3), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1421:/ quasiblock");


	  /*epilog */
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1431:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L4*/ meltfnum[3] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.OMARKER__V8*/ meltfptr[7]),
				 (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->
						tabval[3])));;
	  MELT_LOCATION ("warmelt-outobj.melt:1431:/ cond");
	  /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:1432:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[0])), ("if (meltframptr_->"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1433:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[0])),
				       melt_string_str ((melt_ptr_t)
							( /*_.ONAME__V7*/
							 meltfptr[6])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1434:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[0])), (") "));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1435:/ cond");
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.OMARKER__V8*/
						     meltfptr[7]),
						    (melt_ptr_t) (( /*!CLASS_NAMED */ meltfrout->tabval[3])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.OMARKER__V8*/ meltfptr[7]) /*=obj*/
			;
		      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
       /*_.NAMED_NAME__V13*/ meltfptr[12] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

      /*_.NAMED_NAME__V13*/ meltfptr[12] = NULL;;
		  }
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1435:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[0])),
				       melt_string_str ((melt_ptr_t)
							( /*_.NAMED_NAME__V13*/ meltfptr[12])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1436:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[0])), (" (meltframptr_->"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1437:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[0])),
				       melt_string_str ((melt_ptr_t)
							( /*_.ONAME__V7*/
							 meltfptr[6])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1438:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       (( /*~IMPLBUF */ meltfclos->
					 tabval[0])), (");"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1439:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    (( /*~IMPLBUF */ meltfclos->
					      tabval[0])), (3), 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1431:/ quasiblock");


		/*epilog */

		/*^clear */
	       /*clear *//*_.NAMED_NAME__V13*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.IFELSE___V12*/ meltfptr[11] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.IFELSE___V11*/ meltfptr[9] = /*_.IFELSE___V12*/ meltfptr[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1421:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	}
	;
      }
    ;
    /*_.LET___V5*/ meltfptr[3] = /*_.IFELSE___V11*/ meltfptr[9];;

    MELT_LOCATION ("warmelt-outobj.melt:1415:/ clear");
	   /*clear *//*_.OCTYP__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.ONAME__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OMARKER__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L3*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V11*/ meltfptr[9] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1442:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[3] =
	melt_strbuf_usedlength ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:1443:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V15*/ meltfptr[11] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V15*/ meltfptr[11] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L6*/ meltfnum[0] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V15*/ meltfptr[11])));;
      /*^compute */
   /*_#I__L7*/ meltfnum[6] =
	(( /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[3]) <
	 ( /*_#GET_INT__L6*/ meltfnum[0]));;
      MELT_LOCATION ("warmelt-outobj.melt:1442:/ cond");
      /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1442:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1442) ? (1442) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V16*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1442:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V15*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1413:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V14*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1413:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_38_warmelt_outobj_LAMBDA___2___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_38_warmelt_outobj_LAMBDA___2__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 34
    melt_ptr_t mcfr_varptr[34];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 34; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT nbval 34*/
  meltfram__.mcfr_nbvar = 34 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUT_CURFRAME_DECLSTRUCT_INIT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1449:/ getarg");
 /*_.DECLSTRUCT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ROU__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:1450:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:1451:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBROUT_BODY");
  /*_.OBODY__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1452:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBROUT_NBVAL");
  /*_.ONBVAL__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1453:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "OBROUT_NBLONG");
  /*_.ONBLONG__V8*/ meltfptr[7] = slot;
    };
    ;
 /*_#NBVAL__L1*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.ONBVAL__V7*/ meltfptr[6])));;
    /*^compute */
 /*_#NBLONG__L2*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.ONBLONG__V8*/ meltfptr[7])));;
    /*^compute */
 /*_#IS_A__L3*/ meltfnum[2] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2]),
			   (melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */
					  meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:1457:/ cond");
    /*cond */ if ( /*_#IS_A__L3*/ meltfnum[2])	/*then */
      {
	/*^cond.then */
	/*_#ISINITIAL__L4*/ meltfnum[3] = /*_#IS_A__L3*/ meltfnum[2];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:1457:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_A__L5*/ meltfnum[4] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2]),
				 (melt_ptr_t) (( /*!CLASS_INITIAL_EXTENSION_ROUTINEOBJ */ meltfrout->tabval[0])));;
	  /*^compute */
	  /*_#ISINITIAL__L4*/ meltfnum[3] = /*_#IS_A__L5*/ meltfnum[4];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1457:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[4] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1461:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#ISINITIAL__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#OBJ_HASH__L7*/ meltfnum[6] =
	    (melt_obj_hash ((melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2])));;
	  /*^compute */
   /*_#IRAW__L8*/ meltfnum[7] =
	    (( /*_#OBJ_HASH__L7*/ meltfnum[6]) % (4096));;
	  /*^compute */
   /*_#I__L9*/ meltfnum[8] =
	    ((1) + ( /*_#IRAW__L8*/ meltfnum[7]));;
	  /*^compute */
	  /*_#MINIHASH__L6*/ meltfnum[4] = /*_#I__L9*/ meltfnum[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1461:/ clear");
	     /*clear *//*_#OBJ_HASH__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_#IRAW__L8*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#I__L9*/ meltfnum[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#MINIHASH__L6*/ meltfnum[4] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1462:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "OBROUT_OTHERS");
  /*_.OTHERS__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1463:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.ROU__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.ROUNAME__V10*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ROUNAME__V10*/ meltfptr[9] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1465:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:1465:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1465:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1465;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"output_curframe_declstruct_init declstruct= ";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.DECLSTRUCT__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n declstructinit rou";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.ROU__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " isinitial=";
	      /*^apply.arg */
	      argtab[8].meltbp_long = /*_#ISINITIAL__L4*/ meltfnum[3];
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V12*/ meltfptr[11] =
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1465:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V12*/ meltfptr[11] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:1465:/ quasiblock");


      /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[10] = /*_.PROGN___V14*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1465:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1468:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1469:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#if MELT_HAVE_DEBUG"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1470:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1471:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" static long call_counter__;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1472:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1473:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" long thiscallcounter__ ATTRIBUTE_UNUSED = ++ call_counter__;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1474:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1475:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef meltcallcount"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1476:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1477:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#define meltcallcount thiscallcounter__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1478:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1479:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#else"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1480:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1481:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef meltcallcount"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1482:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1483:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#define meltcallcount 0L"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1484:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1485:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#endif"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1486:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1487:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.DECLSTRUCT__V15*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t) ( /*_.DECLSTRUCT__V2*/ meltfptr[1]),
		    (melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1488:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NOT__L12*/ meltfnum[8] =
      (!( /*_#ISINITIAL__L4*/ meltfnum[3]));;
    MELT_LOCATION ("warmelt-outobj.melt:1488:/ cond");
    /*cond */ if ( /*_#NOT__L12*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1489:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("    *meltframptr_=0,"));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1490:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("    meltfram__; /*declfrastruct*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1491:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1492:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#define meltframe meltfram__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1493:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1494:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#ISINITIAL__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1496:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1497:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "/*";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ROUNAME__V10*/ meltfptr[9];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " is initial declstructinit*/";
	    /*_.ADD2OUT__V17*/ meltfptr[16] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1495:/ quasiblock");


	  /*_.PROGN___V18*/ meltfptr[17] = /*_.ADD2OUT__V17*/ meltfptr[16];;
	  /*^compute */
	  /*_.IFELSE___V16*/ meltfptr[12] = /*_.PROGN___V18*/ meltfptr[17];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1494:/ clear");
	     /*clear *//*_.ADD2OUT__V17*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V18*/ meltfptr[17] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1500:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "/*";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ROUNAME__V10*/ meltfptr[9];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " is not initial declstructinit*/";
	    /*_.ADD2OUT__V19*/ meltfptr[16] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1501:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1502:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("if (MELT_UNLIKELY(meltxargdescr_ == MELTPAR_MARKGGC)) { /*mark for\
 ggc*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1503:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (3),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1504:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("int ix=0;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1505:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (3),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1506:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.ROUNAME__V10*/ meltfptr[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:1507:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = "meltframptr_ = (struct frame_";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.ROUNAME__V10*/ meltfptr[9];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = "_st*) meltfirstargp_;";
		  /*_.ADD2OUT__V21*/ meltfptr[20] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[4])),
				(melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IFELSE___V20*/ meltfptr[17] =
		  /*_.ADD2OUT__V21*/ meltfptr[20];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1506:/ clear");
	       /*clear *//*_.ADD2OUT__V21*/ meltfptr[20] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {




		{
		  MELT_LOCATION ("warmelt-outobj.melt:1508:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("meltframptr_ = (void*)meltfirstargp_;"));
		}
		;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[17] = 0;
		/*epilog */
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1509:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (3),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1510:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring =
	      "  /* use arguments output_curframe_declstruct_init */\
\n\t\t (void) meltclosp_\t ;\n\t\t (void) meltfirstargp_\t ;\
\n\t\t (void) meltxargdescr_\t ;\n\t\t (void) meltxargtab_\t ;\
\n\t\t (void) meltxresdescr_\t ;\n\t\t (void) meltx\
restab_\t ;\n\t\t ";
	    /*_.ADD2OUT__V22*/ meltfptr[20] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1518:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*_.OUTPUCOD_MARKER__V23*/ meltfptr[22] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUCOD_MARKER */ meltfrout->tabval[5])),
			  (melt_ptr_t) ( /*_.ROU__V3*/ meltfptr[2]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1519:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("return NULL;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1520:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (2),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1521:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("}/*end markggc*/;"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1499:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1494:/ clear");
	     /*clear *//*_.ADD2OUT__V19*/ meltfptr[16] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V22*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUCOD_MARKER__V23*/ meltfptr[22] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1524:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1525:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("  memset(&meltfram__, 0, sizeof(meltfram__));"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1526:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1528:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_#ISINITIAL__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1529:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[5];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "/* declstructinit initial routine ";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ROUNAME__V10*/ meltfptr[9];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " minihash ";
	    /*^apply.arg */
	    argtab[3].meltbp_long = /*_#MINIHASH__L6*/ meltfnum[4];
	    /*^apply.arg */
	    argtab[4].meltbp_cstring = "*/";
	    /*_.ADD2OUT__V25*/ meltfptr[17] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1530:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1531:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = " meltfram__.mcfr_nbvar = -";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#MINIHASH__L6*/ meltfnum[4];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "/*minihash*/;";
	    /*_.ADD2OUT__V26*/ meltfptr[20] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1532:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1533:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring =
	      " ((struct melt_callframe_st*)&meltfram__)->mcfr_forwmarkrout = meltmarking_";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ROUNAME__V10*/ meltfptr[9];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = ";";
	    /*_.ADD2OUT__V27*/ meltfptr[22] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1534:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1528:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_.ADD2OUT__V25*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V26*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V27*/ meltfptr[22] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1537:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[5];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = "/* declstructinit plain routine ";
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ROUNAME__V10*/ meltfptr[9];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = " nbval ";
	    /*^apply.arg */
	    argtab[3].meltbp_long = /*_#NBVAL__L1*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[4].meltbp_cstring = "*/";
	    /*_.ADD2OUT__V28*/ meltfptr[17] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_PTR
			   MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1538:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1539:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = " meltfram__.mcfr_nbvar = ";
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#NBVAL__L1*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "/*nbval*/;";
	    /*_.ADD2OUT__V29*/ meltfptr[20] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1540:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1541:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_cstring = " meltfram__.mcfr_clos = meltclosp_;";
	    /*_.ADD2OUT__V30*/ meltfptr[22] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!ADD2OUT */ meltfrout->tabval[4])),
			  (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			  (MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1536:/ quasiblock");


	  /*_.PROGN___V31*/ meltfptr[30] = /*_.ADD2OUT__V30*/ meltfptr[22];;
	  /*^compute */
	  /*_.IFELSE___V24*/ meltfptr[16] = /*_.PROGN___V31*/ meltfptr[30];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1528:/ clear");
	     /*clear *//*_.ADD2OUT__V28*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V29*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.ADD2OUT__V30*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V31*/ meltfptr[30] = 0;
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1543:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1545:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1546:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1547:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" melt_topframe = (struct melt_callframe_st *) &meltfram__;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1548:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1549:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[7] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:1550:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[6])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[7])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[6])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V33*/ meltfptr[20] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V33*/ meltfptr[20] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L14*/ meltfnum[6] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V33*/ meltfptr[20])));;
      /*^compute */
   /*_#I__L15*/ meltfnum[14] =
	(( /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[7]) <
	 ( /*_#GET_INT__L14*/ meltfnum[6]));;
      MELT_LOCATION ("warmelt-outobj.melt:1549:/ cond");
      /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V34*/ meltfptr[22] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1549:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1549) ? (1549) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V34*/ meltfptr[22] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V32*/ meltfptr[17] = /*_.IFELSE___V34*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1549:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L13*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V33*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L14*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_#I__L15*/ meltfnum[14] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V34*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V32*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V5*/ meltfptr[4] = /*_.IFCPP___V32*/ meltfptr[17];;

    MELT_LOCATION ("warmelt-outobj.melt:1450:/ clear");
	   /*clear *//*_.OBODY__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.ONBVAL__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.ONBLONG__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NBVAL__L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NBLONG__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#ISINITIAL__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#MINIHASH__L6*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.OTHERS__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.ROUNAME__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.DECLSTRUCT__V15*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L12*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V16*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V24*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V32*/ meltfptr[17] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:1449:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1449:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUT_CURFRAME_DECLSTRUCT_INIT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_39_warmelt_outobj_OUTPUT_CURFRAME_DECLSTRUCT_INIT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 68
    melt_ptr_t mcfr_varptr[68];
#define MELTFRAM_NBVARNUM 39
    long mcfr_varnum[39];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 68; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE nbval 68*/
  meltfram__.mcfr_nbvar = 68 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_PROCROUTINE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1554:/ getarg");
 /*_.PROU__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1555:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_PROCROUTINEOBJ */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:1555:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1555:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check prou"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1555) ? (1555) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1555:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1556:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.ONAM__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1557:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBROUT_BODY");
  /*_.OBODY__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1558:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBROUT_NBVAL");
  /*_.ONBVAL__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1559:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "OBROUT_NBLONG");
  /*_.ONBLONG__V11*/ meltfptr[10] = slot;
    };
    ;
 /*_#NBVAL__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.ONBVAL__V10*/ meltfptr[9])));;
    /*^compute */
 /*_#NBLONG__L4*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.ONBLONG__V11*/ meltfptr[10])));;
    MELT_LOCATION ("warmelt-outobj.melt:1562:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "OBROUT_OTHERS");
  /*_.OTHERS__V12*/ meltfptr[11] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1563:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 10, "OPROUT_GETARGS");
  /*_.OGARGS__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1564:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 7, "OBROUT_RETVAL");
  /*_.ORETVAL__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1565:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 11, "OPROUT_LOC");
  /*_.ORLOC__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1566:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 12, "OPROUT_FUNAM");
  /*_.OFUNAM__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1567:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PROU__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 13, "OPROUT_RESTNAM");
  /*_.ORESTNAM__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1569:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L5*/ meltfnum[4] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.OFUNAM__V16*/ meltfptr[15])) ==
       MELTOBMAG_STRING);;
    /*^compute */
 /*_#NOT__L6*/ meltfnum[5] =
      (!( /*_#IS_STRING__L5*/ meltfnum[4]));;
    MELT_LOCATION ("warmelt-outobj.melt:1569:/ cond");
    /*cond */ if ( /*_#NOT__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1570:/ compute");
	  /*_.OFUNAM__V16*/ meltfptr[15] = /*_.SETQ___V19*/ meltfptr[18] =
	    ( /*!konst_1 */ meltfrout->tabval[1]);;
	  /*_.IF___V18*/ meltfptr[17] = /*_.SETQ___V19*/ meltfptr[18];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1569:/ clear");
	     /*clear *//*_.SETQ___V19*/ meltfptr[18] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V18*/ meltfptr[17] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1572:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1573:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1574:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = 0;
      /*^apply.arg */
      argtab[2].meltbp_cstring = "proc";
      /*_.OUTPUT_RAW_LOCATION__V20*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_RAW_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.ORLOC__V15*/ meltfptr[14]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1575:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1576:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_MIXINT__L7*/ meltfnum[6] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.ORLOC__V15*/ meltfptr[14])) ==
       MELTOBMAG_MIXINT);;
    MELT_LOCATION ("warmelt-outobj.melt:1576:/ cond");
    /*cond */ if ( /*_#IS_MIXINT__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*_#OR___L8*/ meltfnum[7] = /*_#IS_MIXINT__L7*/ meltfnum[6];;
      }
    else
      {
	MELT_LOCATION ("warmelt-outobj.melt:1576:/ cond.else");

	/*^block */
	/*anyblock */
	{

   /*_#IS_MIXLOC__L9*/ meltfnum[8] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.ORLOC__V15*/ meltfptr[14]))
	     == MELTOBMAG_MIXLOC);;
	  /*^compute */
	  /*_#OR___L8*/ meltfnum[7] = /*_#IS_MIXLOC__L9*/ meltfnum[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1576:/ clear");
	     /*clear *//*_#IS_MIXLOC__L9*/ meltfnum[8] = 0;
	}
	;
      }
    ;
    /*^cond */
    /*cond */ if ( /*_#OR___L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1577:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_long = 0;
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "procdecl";
	    /*_.OUTPUT_RAW_LOCATION__V22*/ meltfptr[21] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_RAW_LOCATION */ meltfrout->tabval[2])),
			  (melt_ptr_t) ( /*_.ORLOC__V15*/ meltfptr[14]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V21*/ meltfptr[20] =
	    /*_.OUTPUT_RAW_LOCATION__V22*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1576:/ clear");
	     /*clear *//*_.OUTPUT_RAW_LOCATION__V22*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V21*/ meltfptr[20] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1579:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("melt_ptr_t MELT_MODULE_VISIBILITY "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1580:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAM__V8*/ meltfptr[7])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1581:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("(meltclosure_ptr_t meltclosp_,"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1582:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   (" melt_ptr_t meltfirstargp_,"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1583:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   (" const melt_argdescr_cell_t meltxargdescr_[],"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1584:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   (" union meltparam_un *meltxargtab_,"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1585:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   (" const melt_argdescr_cell_t meltxresdescr_[],"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1586:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   (" union meltparam_un *meltxrestab_);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1587:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1589:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1590:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1591:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1592:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_ptr_t MELT_MODULE_VISIBILITY "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1593:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1594:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAM__V8*/ meltfptr[7])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1595:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("(meltclosure_ptr_t meltclosp_,"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1596:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" melt_ptr_t meltfirstargp_,"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1597:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" const melt_argdescr_cell_t meltxargdescr_[],"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1598:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" union meltparam_un *meltxargtab_,"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1599:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(5), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1600:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" const melt_argdescr_cell_t meltxresdescr_[],"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1601:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" union meltparam_un *meltxrestab_)"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1602:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1603:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("{"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1604:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ORESTNAM__V17*/ meltfptr[16])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1605:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.OVARIADICINDEX__V23*/ meltfptr[21] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!VARIADIC_INDEX_IDSTR */ meltfrout->
			    tabval[3])),
			  (melt_ptr_t) ( /*_.ORESTNAM__V17*/ meltfptr[16]),
			  (""), (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1606:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.OVARIADICLENGTH__V24*/ meltfptr[23] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!VARIADIC_LENGTH_IDSTR */ meltfrout->
			    tabval[4])),
			  (melt_ptr_t) ( /*_.ORESTNAM__V17*/ meltfptr[16]),
			  (""), (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1608:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (1),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1609:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("/*variadic*/ int "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1610:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.OVARIADICINDEX__V23*/
						   meltfptr[21])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1611:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 (" = 0, "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1612:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.OVARIADICLENGTH__V24*/
						   meltfptr[23])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1613:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 (" = melt_argdescr_length (meltxargdescr_);"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1614:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1621:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("#define melt_variadic_length  (0+"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1622:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.OVARIADICLENGTH__V24*/
						   meltfptr[23])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1623:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (")"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1624:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1625:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("#define melt_variadic_index "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1626:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.OVARIADICINDEX__V23*/
						   meltfptr[21])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1627:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:1605:/ clear");
	     /*clear *//*_.OVARIADICINDEX__V23*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.OVARIADICLENGTH__V24*/ meltfptr[23] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1630:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(2), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1631:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "long current_blocklevel_signals_";
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.ONAM__V8*/ meltfptr[7];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "_melt =  melt_blocklevel_signals;";
      /*_.ADD2OUT__V25*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1632:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1633:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PROU__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.OUTPUT_CURFRAME_DECLSTRUCT_INIT__V26*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_CURFRAME_DECLSTRUCT_INIT */ meltfrout->
		      tabval[6])),
		    (melt_ptr_t) (( /*!OUTPUT_CURFRAME_DECLSTRUCT */
				   meltfrout->tabval[7])),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1634:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_trace_start(\""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1635:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.OFUNAM__V16*/
					     meltfptr[15])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1636:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\", meltcallcount);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1637:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1639:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*getargs*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1640:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1641:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L10*/ meltfnum[8] =
	(( /*_.OGARGS__V13*/ meltfptr[12]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.OGARGS__V13*/ meltfptr[12])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-outobj.melt:1641:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L10*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V28*/ meltfptr[27] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1641:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ogargs"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1641) ? (1641) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V28*/ meltfptr[27] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V27*/ meltfptr[26] = /*_.IFELSE___V28*/ meltfptr[27];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1641:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L10*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V28*/ meltfptr[27] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V27*/ meltfptr[26] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.OGARGS__V13*/ meltfptr[12]);
      for ( /*_#CURANK__L11*/ meltfnum[8] = 0;
	   ( /*_#CURANK__L11*/ meltfnum[8] >= 0)
	   && ( /*_#CURANK__L11*/ meltfnum[8] < meltcit1__EACHTUP_ln);
	/*_#CURANK__L11*/ meltfnum[8]++)
	{
	  /*_.CURGET__V29*/ meltfptr[27] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.OGARGS__V13*/ meltfptr[12]),
			       /*_#CURANK__L11*/ meltfnum[8]);




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1645:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (1),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1646:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("/*getarg#"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1647:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#CURANK__L11*/ meltfnum[8]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1648:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1649:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1650:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 1;
	    /*_.OUTPUT_C_CODE__V30*/ meltfptr[29] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURGET__V29*/ meltfptr[27]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[8])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1652:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#STRBUF_USEDLENGTH__L12*/ meltfnum[11] =
	    melt_strbuf_usedlength ((melt_ptr_t)
				    ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	  MELT_LOCATION ("warmelt-outobj.melt:1652:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!BUFFER_LIMIT_CONT */
						meltfrout->tabval[9])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[10])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				 tabval[9])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
    /*_.REFERENCED_VALUE__V31*/ meltfptr[30] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.REFERENCED_VALUE__V31*/ meltfptr[30] = NULL;;
	    }
	  ;
	  /*^compute */
  /*_#GET_INT__L13*/ meltfnum[12] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V31*/ meltfptr[30])));;
	  /*^compute */
  /*_#IRAW__L14*/ meltfnum[13] =
	    (( /*_#GET_INT__L13*/ meltfnum[12]) / (2));;
	  /*^compute */
  /*_#I__L15*/ meltfnum[14] =
	    (( /*_#STRBUF_USEDLENGTH__L12*/ meltfnum[11]) >
	     ( /*_#IRAW__L14*/ meltfnum[13]));;
	  MELT_LOCATION ("warmelt-outobj.melt:1652:/ cond");
	  /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:1653:/ locexp");

#if MELT_HAVE_DEBUG
		  if (melt_need_debug (0))
		    melt_dbgshortbacktrace (("outpucod_procroutine huge implbuf"), (10));
#endif
		  ;
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:1654:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L16*/ meltfnum[15] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:1654:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[15])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:1654:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 1654;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "outpucod_procroutine huge implbuf=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
			  /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[11])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V34*/ meltfptr[33] =
			  /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:1654:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[16] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V34*/ meltfptr[33] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:1654:/ quasiblock");


		  /*_.PROGN___V36*/ meltfptr[34] =
		    /*_.IF___V34*/ meltfptr[33];;
		  /*^compute */
		  /*_.IFCPP___V33*/ meltfptr[32] =
		    /*_.PROGN___V36*/ meltfptr[34];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1654:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[15] = 0;
		  /*^clear */
		/*clear *//*_.IF___V34*/ meltfptr[33] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V36*/ meltfptr[34] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V33*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:1655:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#STRBUF_USEDLENGTH__L18*/ meltfnum[16] =
		    melt_strbuf_usedlength ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/
					     meltfptr[3]));;
		  MELT_LOCATION ("warmelt-outobj.melt:1656:/ cond");
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[9])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[10])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
					 tabval[9])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	/*_.REFERENCED_VALUE__V38*/ meltfptr[34] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.REFERENCED_VALUE__V38*/ meltfptr[34] = NULL;;
		    }
		  ;
		  /*^compute */
      /*_#GET_INT__L19*/ meltfnum[15] =
		    (melt_get_int
		     ((melt_ptr_t)
		      ( /*_.REFERENCED_VALUE__V38*/ meltfptr[34])));;
		  /*^compute */
      /*_#I__L20*/ meltfnum[19] =
		    (( /*_#STRBUF_USEDLENGTH__L18*/ meltfnum[16]) <
		     ( /*_#GET_INT__L19*/ meltfnum[15]));;
		  MELT_LOCATION ("warmelt-outobj.melt:1655:/ cond");
		  /*cond */ if ( /*_#I__L20*/ meltfnum[19])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V39*/ meltfptr[38] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:1655:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check limited implbuf"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (1655) ? (1655) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V39*/ meltfptr[38] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V37*/ meltfptr[33] =
		    /*_.IFELSE___V39*/ meltfptr[38];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1655:/ clear");
		/*clear *//*_#STRBUF_USEDLENGTH__L18*/ meltfnum[16] = 0;
		  /*^clear */
		/*clear *//*_.REFERENCED_VALUE__V38*/ meltfptr[34] = 0;
		  /*^clear */
		/*clear *//*_#GET_INT__L19*/ meltfnum[15] = 0;
		  /*^clear */
		/*clear *//*_#I__L20*/ meltfnum[19] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V39*/ meltfptr[38] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V37*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:1652:/ quasiblock");


		/*_.PROGN___V40*/ meltfptr[34] =
		  /*_.IFCPP___V37*/ meltfptr[33];;
		/*^compute */
		/*_.IFELSE___V32*/ meltfptr[31] =
		  /*_.PROGN___V40*/ meltfptr[34];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1652:/ clear");
	      /*clear *//*_.IFCPP___V33*/ meltfptr[32] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V37*/ meltfptr[33] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V40*/ meltfptr[34] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IFELSE___V32*/ meltfptr[31] = NULL;;
	    }
	  ;
	  if ( /*_#CURANK__L11*/ meltfnum[8] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:1642:/ clear");
	    /*clear *//*_.CURGET__V29*/ meltfptr[27] = 0;
      /*^clear */
	    /*clear *//*_#CURANK__L11*/ meltfnum[8] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_CODE__V30*/ meltfptr[29] = 0;
      /*^clear */
	    /*clear *//*_#STRBUF_USEDLENGTH__L12*/ meltfnum[11] = 0;
      /*^clear */
	    /*clear *//*_.REFERENCED_VALUE__V31*/ meltfptr[30] = 0;
      /*^clear */
	    /*clear *//*_#GET_INT__L13*/ meltfnum[12] = 0;
      /*^clear */
	    /*clear *//*_#IRAW__L14*/ meltfnum[13] = 0;
      /*^clear */
	    /*clear *//*_#I__L15*/ meltfnum[14] = 0;
      /*^clear */
	    /*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1658:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1659:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1660:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" goto lab_endgetargs;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1661:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1662:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("lab_endgetargs:;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1663:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1665:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L21*/ meltfnum[16] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OBODY__V9*/ meltfptr[8])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-outobj.melt:1665:/ cond");
      /*cond */ if ( /*_#IS_LIST__L21*/ meltfnum[16])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V42*/ meltfptr[32] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1665:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obody"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1665) ? (1665) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V42*/ meltfptr[32] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V41*/ meltfptr[38] = /*_.IFELSE___V42*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1665:/ clear");
	     /*clear *//*_#IS_LIST__L21*/ meltfnum[16] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V42*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V41*/ meltfptr[38] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1666:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*body*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1667:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1670:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V44*/ meltfptr[34] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_17 */ meltfrout->
						tabval[17])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V44*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V44*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V44*/ meltfptr[34])->tabval[0] =
      (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V44*/ meltfptr[34])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V44*/ meltfptr[34])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V44*/ meltfptr[34])->tabval[1] =
      (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]);
    ;
    /*_.LAMBDA___V43*/ meltfptr[33] = /*_.LAMBDA___V44*/ meltfptr[34];;
    MELT_LOCATION ("warmelt-outobj.melt:1668:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V43*/ meltfptr[33];
      /*_.LIST_EVERY__V45*/ meltfptr[32] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_EVERY */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.OBODY__V9*/ meltfptr[8]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1679:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1680:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1681:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" goto labend_rout;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1682:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1683:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("labend_rout:"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1684:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1685:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("melt_trace_end(\""));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1686:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.OFUNAM__V16*/
					     meltfptr[15])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1687:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("\", meltcallcount);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1688:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1689:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("MELT_TRACE_EXIT_LOCATION ();"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1690:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1691:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	" melt_blocklevel_signals = current_blocklevel_signals_";
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.ONAM__V8*/ meltfptr[7];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "_melt;";
      /*_.ADD2OUT__V46*/ meltfptr[45] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1692:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1693:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" melt_topframe = (struct melt_callframe_st*) meltfram__.mcfr_prev;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1694:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1695:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" return (melt_ptr_t)("));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1696:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ORETVAL__V14*/ meltfptr[13])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1697:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 1;
	    /*_.OUTPUT_C_CODE__V48*/ meltfptr[47] =
	      meltgc_send ((melt_ptr_t) ( /*_.ORETVAL__V14*/ meltfptr[13]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[8])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V47*/ meltfptr[46] =
	    /*_.OUTPUT_C_CODE__V48*/ meltfptr[47];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1696:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V48*/ meltfptr[47] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1698:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("/*noretval*/ NULL"));
	  }
	  ;
	     /*clear *//*_.IFELSE___V47*/ meltfptr[46] = 0;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1699:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1700:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ORESTNAM__V17*/ meltfptr[16])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1702:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1703:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("#undef melt_variadic_length"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1704:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1705:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("#undef melt_variadic_index"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1706:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1701:/ quasiblock");


	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1707:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1708:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef meltcallcount"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1709:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1710:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef meltfram__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1711:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1712:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef MELTFRAM_NBVARNUM"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1713:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1714:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef MELTFRAM_NBVARPTR"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1715:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1716:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("} /*end "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1717:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAM__V8*/ meltfptr[7])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1718:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1719:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1720:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1722:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L22*/ meltfnum[15] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:1722:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[9])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->
						       tabval[10])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[9])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V49*/ meltfptr[47] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V49*/ meltfptr[47] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L23*/ meltfnum[19] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V49*/ meltfptr[47])));;
    /*^compute */
 /*_#IRAW__L24*/ meltfnum[16] =
      (( /*_#GET_INT__L23*/ meltfnum[19]) / (2));;
    /*^compute */
 /*_#I__L25*/ meltfnum[24] =
      (( /*_#STRBUF_USEDLENGTH__L22*/ meltfnum[15]) >
       ( /*_#IRAW__L24*/ meltfnum[16]));;
    MELT_LOCATION ("warmelt-outobj.melt:1722:/ cond");
    /*cond */ if ( /*_#I__L25*/ meltfnum[24])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1723:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outpucod_procroutine huge declbuf"),
				      (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1724:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L26*/ meltfnum[25] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1724:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[25])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:1724:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1724;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outpucod_procroutine huge declbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V53*/ meltfptr[52] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[11])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V52*/ meltfptr[51] =
		    /*_.MELT_DEBUG_FUN__V53*/ meltfptr[52];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1724:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V53*/ meltfptr[52] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V52*/ meltfptr[51] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:1724:/ quasiblock");


	    /*_.PROGN___V54*/ meltfptr[52] = /*_.IF___V52*/ meltfptr[51];;
	    /*^compute */
	    /*_.IFCPP___V51*/ meltfptr[50] = /*_.PROGN___V54*/ meltfptr[52];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1724:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[25] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V52*/ meltfptr[51] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V54*/ meltfptr[52] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V51*/ meltfptr[50] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1725:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L28*/ meltfnum[26] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.DECLBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1726:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[9])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[10])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[9])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V56*/ meltfptr[52] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V56*/ meltfptr[52] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L29*/ meltfnum[25] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V56*/ meltfptr[52])));;
	    /*^compute */
     /*_#I__L30*/ meltfnum[29] =
	      (( /*_#STRBUF_USEDLENGTH__L28*/ meltfnum[26]) <
	       ( /*_#GET_INT__L29*/ meltfnum[25]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1725:/ cond");
	    /*cond */ if ( /*_#I__L30*/ meltfnum[29])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V57*/ meltfptr[56] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:1725:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited declbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(1725) ? (1725) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V57*/ meltfptr[56] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V55*/ meltfptr[51] = /*_.IFELSE___V57*/ meltfptr[56];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1725:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L28*/ meltfnum[26] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V56*/ meltfptr[52] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L29*/ meltfnum[25] = 0;
	    /*^clear */
	       /*clear *//*_#I__L30*/ meltfnum[29] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V57*/ meltfptr[56] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V55*/ meltfptr[51] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1722:/ quasiblock");


	  /*_.PROGN___V58*/ meltfptr[52] = /*_.IFCPP___V55*/ meltfptr[51];;
	  /*^compute */
	  /*_.IFELSE___V50*/ meltfptr[49] = /*_.PROGN___V58*/ meltfptr[52];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1722:/ clear");
	     /*clear *//*_.IFCPP___V51*/ meltfptr[50] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V55*/ meltfptr[51] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V58*/ meltfptr[52] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V50*/ meltfptr[49] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1728:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L31*/ meltfnum[26] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
    MELT_LOCATION ("warmelt-outobj.melt:1728:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[9])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->
						       tabval[10])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[9])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V59*/ meltfptr[56] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V59*/ meltfptr[56] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L32*/ meltfnum[25] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V59*/ meltfptr[56])));;
    /*^compute */
 /*_#IRAW__L33*/ meltfnum[29] =
      (( /*_#GET_INT__L32*/ meltfnum[25]) / (2));;
    /*^compute */
 /*_#I__L34*/ meltfnum[33] =
      (( /*_#STRBUF_USEDLENGTH__L31*/ meltfnum[26]) >
       ( /*_#IRAW__L33*/ meltfnum[29]));;
    MELT_LOCATION ("warmelt-outobj.melt:1728:/ cond");
    /*cond */ if ( /*_#I__L34*/ meltfnum[33])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1729:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("outpucod_procroutine huge implbuf"),
				      (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1730:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L35*/ meltfnum[34] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1730:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L35*/ meltfnum[34])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L36*/ meltfnum[35] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:1730:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L36*/ meltfnum[35];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1730;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outpucod_procroutine huge implbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		    /*_.MELT_DEBUG_FUN__V63*/ meltfptr[62] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[11])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V62*/ meltfptr[52] =
		    /*_.MELT_DEBUG_FUN__V63*/ meltfptr[62];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1730:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L36*/ meltfnum[35] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V63*/ meltfptr[62] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V62*/ meltfptr[52] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:1730:/ quasiblock");


	    /*_.PROGN___V64*/ meltfptr[62] = /*_.IF___V62*/ meltfptr[52];;
	    /*^compute */
	    /*_.IFCPP___V61*/ meltfptr[51] = /*_.PROGN___V64*/ meltfptr[62];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1730:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L35*/ meltfnum[34] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V62*/ meltfptr[52] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V64*/ meltfptr[62] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V61*/ meltfptr[51] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1731:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L37*/ meltfnum[35] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1732:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[9])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[10])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[9])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V66*/ meltfptr[62] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V66*/ meltfptr[62] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L38*/ meltfnum[34] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V66*/ meltfptr[62])));;
	    /*^compute */
     /*_#I__L39*/ meltfnum[38] =
	      (( /*_#STRBUF_USEDLENGTH__L37*/ meltfnum[35]) <
	       ( /*_#GET_INT__L38*/ meltfnum[34]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1731:/ cond");
	    /*cond */ if ( /*_#I__L39*/ meltfnum[38])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V67*/ meltfptr[66] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:1731:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(1731) ? (1731) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V67*/ meltfptr[66] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V65*/ meltfptr[52] = /*_.IFELSE___V67*/ meltfptr[66];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1731:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L37*/ meltfnum[35] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V66*/ meltfptr[62] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L38*/ meltfnum[34] = 0;
	    /*^clear */
	       /*clear *//*_#I__L39*/ meltfnum[38] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V67*/ meltfptr[66] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V65*/ meltfptr[52] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1728:/ quasiblock");


	  /*_.PROGN___V68*/ meltfptr[62] = /*_.IFCPP___V65*/ meltfptr[52];;
	  /*^compute */
	  /*_.IFELSE___V60*/ meltfptr[50] = /*_.PROGN___V68*/ meltfptr[62];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1728:/ clear");
	     /*clear *//*_.IFCPP___V61*/ meltfptr[51] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V65*/ meltfptr[52] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V68*/ meltfptr[62] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V60*/ meltfptr[50] = NULL;;
      }
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFELSE___V60*/ meltfptr[50];;

    MELT_LOCATION ("warmelt-outobj.melt:1556:/ clear");
	   /*clear *//*_.ONAM__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.OBODY__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.ONBVAL__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.ONBLONG__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#NBVAL__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#NBLONG__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.OTHERS__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OGARGS__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.ORETVAL__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.ORLOC__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OFUNAM__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.ORESTNAM__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_RAW_LOCATION__V20*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#IS_MIXINT__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#OR___L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V25*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_CURFRAME_DECLSTRUCT_INIT__V26*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V41*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V43*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.LIST_EVERY__V45*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V46*/ meltfptr[45] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L22*/ meltfnum[15] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V49*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L23*/ meltfnum[19] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L24*/ meltfnum[16] = 0;
    /*^clear */
	   /*clear *//*_#I__L25*/ meltfnum[24] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V50*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L31*/ meltfnum[26] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V59*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L32*/ meltfnum[25] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L33*/ meltfnum[29] = 0;
    /*^clear */
	   /*clear *//*_#I__L34*/ meltfnum[33] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V60*/ meltfptr[50] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:1554:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1554:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_PROCROUTINE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_40_warmelt_outobj_OUTPUCOD_PROCROUTINE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_outobj_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_41_warmelt_outobj_LAMBDA___3___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_41_warmelt_outobj_LAMBDA___3___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_41_warmelt_outobj_LAMBDA___3__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_41_warmelt_outobj_LAMBDA___3___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_41_warmelt_outobj_LAMBDA___3__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1670:/ getarg");
 /*_.CURBODY__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-outobj.melt:1671:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.CURBODY__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#IS_A__L2*/ meltfnum[1] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.CURBODY__V2*/ meltfptr[1]),
				 (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
						meltfrout->tabval[0])));;
	  /*^compute */
   /*_#NOT__L3*/ meltfnum[2] =
	    (!( /*_#IS_A__L2*/ meltfnum[1]));;
	  /*^compute */
	  /*_#IF___L1*/ meltfnum[0] = /*_#NOT__L3*/ meltfnum[2];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1671:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
	  /*^clear */
	     /*clear *//*_#NOT__L3*/ meltfnum[2] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L1*/ meltfnum[0] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1671:/ cond");
    /*cond */ if ( /*_#IF___L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:1673:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~DECLBUF */ meltfclos->tabval[0]);
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & ( /*~IMPLBUF */ meltfclos->tabval[1]);
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 0;
	    /*_.OUTPUT_C_CODE__V3*/ meltfptr[2] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURBODY__V2*/ meltfptr[1]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[1])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1674:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      (( /*~IMPLBUF */ meltfclos->tabval[1])),
				      (0), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1672:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1671:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V3*/ meltfptr[2] = 0;
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1675:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				(( /*~IMPLBUF */ meltfclos->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:1676:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[2])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[2])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V5*/ meltfptr[4] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L5*/ meltfnum[2] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V5*/ meltfptr[4])));;
      /*^compute */
   /*_#I__L6*/ meltfnum[5] =
	(( /*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1]) <
	 ( /*_#GET_INT__L5*/ meltfnum[2]));;
      MELT_LOCATION ("warmelt-outobj.melt:1675:/ cond");
      /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1675:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1675) ? (1675) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[2] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1675:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L4*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L5*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_#I__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1670:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V4*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1670:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IF___L1*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_41_warmelt_outobj_LAMBDA___3___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_41_warmelt_outobj_LAMBDA___3__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUT_CURFRAME_CDAT_STRUCT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1740:/ getarg");
 /*_.IDATUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:1741:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1742:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   ("struct cdata_st {"));
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.IDATUP__V2*/ meltfptr[1]);
      for ( /*_#CURK__L1*/ meltfnum[0] = 0;
	   ( /*_#CURK__L1*/ meltfnum[0] >= 0)
	   && ( /*_#CURK__L1*/ meltfnum[0] < meltcit1__EACHTUP_ln);
	/*_#CURK__L1*/ meltfnum[0]++)
	{
	  /*_.CURDAT__V4*/ meltfptr[3] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.IDATUP__V2*/ meltfptr[1]),
			       /*_#CURK__L1*/ meltfnum[0]);




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1746:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V3*/ meltfptr[2]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1747:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V3*/ meltfptr[2];
	    /*_.OUTPUT_C_DECLINIT__V5*/ meltfptr[4] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURDAT__V4*/ meltfptr[3]),
			   (melt_ptr_t) (( /*!OUTPUT_C_DECLINIT */ meltfrout->
					  tabval[0])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  if ( /*_#CURK__L1*/ meltfnum[0] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:1743:/ clear");
	    /*clear *//*_.CURDAT__V4*/ meltfptr[3] = 0;
      /*^clear */
	    /*clear *//*_#CURK__L1*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_DECLINIT__V5*/ meltfptr[4] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1748:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1749:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   (" long spare_;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1750:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1751:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   ("}"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1752:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:1752:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[1])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[1])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V6*/ meltfptr[5] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V6*/ meltfptr[5] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L3*/ meltfnum[2] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V6*/ meltfptr[5])));;
    /*^compute */
 /*_#IRAW__L4*/ meltfnum[3] =
      (( /*_#GET_INT__L3*/ meltfnum[2]) / (2));;
    /*^compute */
 /*_#I__L5*/ meltfnum[4] =
      (( /*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1]) >
       ( /*_#IRAW__L4*/ meltfnum[3]));;
    MELT_LOCATION ("warmelt-outobj.melt:1752:/ cond");
    /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1753:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("output_curframe_cdat_struct huge implbuf"), (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1754:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L6*/ meltfnum[5] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1754:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[5])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:1754:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1754;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "output_curframe_cdat_struct huge implbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.IMPLBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V9*/ meltfptr[8] =
		    /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1754:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V9*/ meltfptr[8] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:1754:/ quasiblock");


	    /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
	    /*^compute */
	    /*_.IFCPP___V8*/ meltfptr[7] = /*_.PROGN___V11*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1754:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V8*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1755:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L8*/ meltfnum[6] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1756:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[1])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[2])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[1])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V13*/ meltfptr[9] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V13*/ meltfptr[9] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L9*/ meltfnum[5] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V13*/ meltfptr[9])));;
	    /*^compute */
     /*_#I__L10*/ meltfnum[9] =
	      (( /*_#STRBUF_USEDLENGTH__L8*/ meltfnum[6]) <
	       ( /*_#GET_INT__L9*/ meltfnum[5]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1755:/ cond");
	    /*cond */ if ( /*_#I__L10*/ meltfnum[9])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:1755:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(1755) ? (1755) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V12*/ meltfptr[8] = /*_.IFELSE___V14*/ meltfptr[13];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1755:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L8*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V13*/ meltfptr[9] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L9*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_#I__L10*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V12*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1752:/ quasiblock");


	  /*_.PROGN___V15*/ meltfptr[9] = /*_.IFCPP___V12*/ meltfptr[8];;
	  /*^compute */
	  /*_.IFELSE___V7*/ meltfptr[6] = /*_.PROGN___V15*/ meltfptr[9];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1752:/ clear");
	     /*clear *//*_.IFCPP___V8*/ meltfptr[7] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V12*/ meltfptr[8] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V15*/ meltfptr[9] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V7*/ meltfptr[6] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1740:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V7*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1740:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L2*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#I__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUT_CURFRAME_CDAT_STRUCT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_42_warmelt_outobj_OUTPUT_CURFRAME_CDAT_STRUCT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUT_CURFRAME_CDAT_FILL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1762:/ getarg");
 /*_.IDATUP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:1764:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   (" cdat = (struct cdata_st*) meltgc_allocate(sizeof(*cdat),0);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1765:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1766:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   (" melt_prohibit_garbcoll = TRUE;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1767:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1770:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   ("/*initial routine predef*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1771:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.IDATUP__V2*/ meltfptr[1]);
      for ( /*_#CURK__L1*/ meltfnum[0] = 0;
	   ( /*_#CURK__L1*/ meltfnum[0] >= 0)
	   && ( /*_#CURK__L1*/ meltfnum[0] < meltcit1__EACHTUP_ln);
	/*_#CURK__L1*/ meltfnum[0]++)
	{
	  /*_.CURPDAT__V4*/ meltfptr[3] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.IDATUP__V2*/ meltfptr[1]),
			       /*_#CURK__L1*/ meltfnum[0]);



	  MELT_LOCATION ("warmelt-outobj.melt:1775:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & ( /*!konst_1 */ meltfrout->tabval[1]);
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 1;
	    /*_.OUTPUT_C_INITIAL_PREDEF__V5*/ meltfptr[4] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURPDAT__V4*/ meltfptr[3]),
			   (melt_ptr_t) (( /*!OUTPUT_C_INITIAL_PREDEF */
					  meltfrout->tabval[0])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  if ( /*_#CURK__L1*/ meltfnum[0] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:1772:/ clear");
	    /*clear *//*_.CURPDAT__V4*/ meltfptr[3] = 0;
      /*^clear */
	    /*clear *//*_#CURK__L1*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_INITIAL_PREDEF__V5*/ meltfptr[4] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1778:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   ("/*initial routine fill*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1779:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.IDATUP__V2*/ meltfptr[1]);
      for ( /*_#CURK__L2*/ meltfnum[1] = 0;
	   ( /*_#CURK__L2*/ meltfnum[1] >= 0)
	   && ( /*_#CURK__L2*/ meltfnum[1] < meltcit2__EACHTUP_ln);
	/*_#CURK__L2*/ meltfnum[1]++)
	{
	  /*_.CURFIL__V6*/ meltfptr[5] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.IDATUP__V2*/ meltfptr[1]),
			       /*_#CURK__L2*/ meltfnum[1]);




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1783:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V3*/ meltfptr[2]), (1),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1784:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & ( /*!konst_3 */ meltfrout->tabval[3]);
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 0;
	    /*_.OUTPUT_C_INITIAL_FILL__V7*/ meltfptr[6] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURFIL__V6*/ meltfptr[5]),
			   (melt_ptr_t) (( /*!OUTPUT_C_INITIAL_FILL */
					  meltfrout->tabval[2])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  if ( /*_#CURK__L2*/ meltfnum[1] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:1780:/ clear");
	    /*clear *//*_.CURFIL__V6*/ meltfptr[5] = 0;
      /*^clear */
	    /*clear *//*_#CURK__L2*/ meltfnum[1] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_INITIAL_FILL__V7*/ meltfptr[6] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1789:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   (" cdat = NULL;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1790:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1791:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
			   (" melt_prohibit_garbcoll = FALSE;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1792:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1794:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[2] =
      melt_strbuf_usedlength ((melt_ptr_t) ( /*_.IMPLBUF__V3*/ meltfptr[2]));;
    MELT_LOCATION ("warmelt-outobj.melt:1794:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					(( /*!BUFFER_LIMIT_CONT */ meltfrout->
					  tabval[4])),
					(melt_ptr_t) (( /*!CLASS_REFERENCE */
						       meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj =
	    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			   tabval[4])) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
   /*_.REFERENCED_VALUE__V8*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.REFERENCED_VALUE__V8*/ meltfptr[7] = NULL;;
      }
    ;
    /*^compute */
 /*_#GET_INT__L4*/ meltfnum[3] =
      (melt_get_int
       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V8*/ meltfptr[7])));;
    /*^compute */
 /*_#IRAW__L5*/ meltfnum[4] =
      (( /*_#GET_INT__L4*/ meltfnum[3]) / (2));;
    /*^compute */
 /*_#I__L6*/ meltfnum[5] =
      (( /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[2]) >
       ( /*_#IRAW__L5*/ meltfnum[4]));;
    MELT_LOCATION ("warmelt-outobj.melt:1794:/ cond");
    /*cond */ if ( /*_#I__L6*/ meltfnum[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:1795:/ locexp");

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("output_curframe_cdat_fill implbuf"),
				      (10));
#endif
	    ;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1796:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L7*/ meltfnum[6] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:1796:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[6])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:1796:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 1796;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "output_curframe_cdat_fill huge implbuf=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.IMPLBUF__V3*/ meltfptr[2];
		    /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[6])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V11*/ meltfptr[10] =
		    /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:1796:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V11*/ meltfptr[10] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:1796:/ quasiblock");


	    /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
	    /*^compute */
	    /*_.IFCPP___V10*/ meltfptr[9] = /*_.PROGN___V13*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1796:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:1797:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#STRBUF_USEDLENGTH__L9*/ meltfnum[7] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1798:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[4])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[4])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
       /*_.REFERENCED_VALUE__V15*/ meltfptr[11] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.REFERENCED_VALUE__V15*/ meltfptr[11] = NULL;;
	      }
	    ;
	    /*^compute */
     /*_#GET_INT__L10*/ meltfnum[6] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V15*/ meltfptr[11])));;
	    /*^compute */
     /*_#I__L11*/ meltfnum[10] =
	      (( /*_#STRBUF_USEDLENGTH__L9*/ meltfnum[7]) <
	       ( /*_#GET_INT__L10*/ meltfnum[6]));;
	    MELT_LOCATION ("warmelt-outobj.melt:1797:/ cond");
	    /*cond */ if ( /*_#I__L11*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:1797:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(1797) ? (1797) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V14*/ meltfptr[10] = /*_.IFELSE___V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1797:/ clear");
	       /*clear *//*_#STRBUF_USEDLENGTH__L9*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.REFERENCED_VALUE__V15*/ meltfptr[11] = 0;
	    /*^clear */
	       /*clear *//*_#GET_INT__L10*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V14*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1794:/ quasiblock");


	  /*_.PROGN___V17*/ meltfptr[11] = /*_.IFCPP___V14*/ meltfptr[10];;
	  /*^compute */
	  /*_.IFELSE___V9*/ meltfptr[8] = /*_.PROGN___V17*/ meltfptr[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:1794:/ clear");
	     /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V14*/ meltfptr[10] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[11] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IFELSE___V9*/ meltfptr[8] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1762:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFELSE___V9*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1762:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#STRBUF_USEDLENGTH__L3*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_.REFERENCED_VALUE__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUT_CURFRAME_CDAT_FILL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_43_warmelt_outobj_OUTPUT_CURFRAME_CDAT_FILL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 79
    melt_ptr_t mcfr_varptr[79];
#define MELTFRAM_NBVARNUM 46
    long mcfr_varnum[46];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 79; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE nbval 79*/
  meltfram__.mcfr_nbvar = 79 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_INITIALMODULEROUTINE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1805:/ getarg");
 /*_.PINI__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1806:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:1806:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    /*^compute */
     /*_.DISCRIM__V7*/ meltfptr[6] =
	      ((melt_ptr_t)
	       (melt_discr ((melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]))));;
	    MELT_LOCATION ("warmelt-outobj.melt:1806:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 1806;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"outpucod_initialmoduleroutine start pini=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PINI__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n of discrim=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DISCRIM__V7*/ meltfptr[6];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V6*/ meltfptr[5] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:1806:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.DISCRIM__V7*/ meltfptr[6] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V6*/ meltfptr[5] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:1806:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[6] = /*_.IF___V6*/ meltfptr[5];;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.PROGN___V9*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1806:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V6*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:1808:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[2] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_INITIAL_MODULE_ROUTINEOBJ */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:1808:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:1808:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pini"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (1808) ? (1808) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[7] = /*_.IFELSE___V11*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:1808:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1809:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:1810:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 10, "OIROUT_DATA");
  /*_.IDATUP__V13*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1811:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 12, "OIROUT_FILL");
  /*_.IRFILL__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1812:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 11, "OIROUT_PROLOG");
  /*_.IPROLOG__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1813:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 7, "OBROUT_RETVAL");
  /*_.ORETVAL__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1814:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 13, "OIROUT_MODULENAME");
  /*_.OMODNAM__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1815:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.PINI__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 4, "OBROUT_NBVAL");
   /*_.ONBVAL__V18*/ meltfptr[17] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ONBVAL__V18*/ meltfptr[17] = NULL;;
      }
    ;
    /*^compute */
 /*_#NBVAL__L5*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.ONBVAL__V18*/ meltfptr[17])));;
    /*^compute */
 /*_#OBJ_HASH__L6*/ meltfnum[2] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1])));;
    /*^compute */
 /*_#IRAW__L7*/ meltfnum[6] =
      (( /*_#OBJ_HASH__L6*/ meltfnum[2]) % (4096));;
    /*^compute */
 /*_#MINIHASH__L8*/ meltfnum[7] =
      ((1) + ( /*_#IRAW__L7*/ meltfnum[6]));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1819:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1820:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1821:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"\nMELT_EXTERN  void* melt_start_this_module (void*);\n ";
      /*_.ADD2OUT__V19*/ meltfptr[18] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1824:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1825:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1826:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1827:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("typedef "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1828:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.OUTPUT_CURFRAME_DECLSTRUCT__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_CURFRAME_DECLSTRUCT */ meltfrout->
		      tabval[4])), (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1829:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" initial_frame_st;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1830:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1831:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1832:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1833:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("static void initialize_module_meltdata_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1834:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1835:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" (initial_frame_st *iniframp__, char meltpredefinited[])"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1836:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1837:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("{"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1838:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1839:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#define meltfram__  (*iniframp__)"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1840:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1841:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.OUTPUT_CURFRAME_CDAT_STRUCT__V21*/ meltfptr[20] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_CURFRAME_CDAT_STRUCT */ meltfrout->
		      tabval[5])),
		    (melt_ptr_t) ( /*_.IDATUP__V13*/ meltfptr[5]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1842:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" *cdat = NULL;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1843:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1844:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" dbgprintf (\"start initialize_module_meltdata_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1845:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1846:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" iniframp__=%p\", (void*) iniframp__);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1847:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1848:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("(void) meltpredefinited; /* avoid warning if non-used. */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1849:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1850:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" melt_assertmsg (\"check module initial frame\", iniframp__->mcfr_nbvar\
 == /*minihash*/ -"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1851:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#MINIHASH__L8*/ meltfnum[7]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1852:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1853:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1855:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.OUTPUT_CURFRAME_CDAT_FILL__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_CURFRAME_CDAT_FILL */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.IDATUP__V13*/ meltfptr[5]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1856:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1857:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef meltfram__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1858:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1859:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("} /*end initialize_module_meltdata_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1860:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1861:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1862:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1863:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1866:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1867:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("/* define different names when debugging or not */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1869:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1870:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("#if MELT_HAVE_DEBUG"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1871:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1872:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("MELT_EXTERN "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1873:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("const char meltmodule_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1874:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1875:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("__melt_have_debug_enabled[];"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1876:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1877:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("#define melt_have_debug_string meltmodule_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1878:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1879:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("__melt_have_debug_enabled"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1880:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1881:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("#else /*!MELT_HAVE_DEBUG*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1882:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1883:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("MELT_EXTERN const char meltmodule_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1884:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1885:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("__melt_have_debug_disabled[];"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1886:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1887:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("#define melt_have_debug_string meltmodule_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1888:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1889:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("__melt_have_debug_disabled"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1890:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1891:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("#endif /*!MELT_HAVE_DEBUG*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1892:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1893:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1895:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1896:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#if MELT_HAVE_DEBUG"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1897:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1898:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("const char meltmodule_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1899:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1900:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("__melt_have_debug_enabled[] = \"MELT module "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1901:/ locexp");
      meltgc_add_strbuf_cstr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			      melt_string_str ((melt_ptr_t)
					       ( /*_.OMODNAM__V17*/
						meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1902:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" have debug enabled\";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1903:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1904:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#else /*!MELT_HAVE_DEBUG*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1905:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1906:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("const char meltmodule_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1907:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1908:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("__melt_have_debug_disabled[] = \"MELT module "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1909:/ locexp");
      meltgc_add_strbuf_cstr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			      melt_string_str ((melt_ptr_t)
					       ( /*_.OMODNAM__V17*/
						meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1910:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" have debug disabled\";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1911:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1912:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#endif /*MELT_HAVE_DEBUG*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1913:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1865:/ quasiblock");



    {
      MELT_LOCATION ("warmelt-outobj.melt:1916:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1917:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1918:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("void* melt_start_this_module (void* modargp_) {"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1919:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1921:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1922:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("char meltpredefinited[MELTGLOB__LASTGLOB+8];"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1923:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1926:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V24*/ meltfptr[23] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_8 */ meltfrout->
						tabval[8])), (0));
    ;
    /*_.LAMBDA___V23*/ meltfptr[22] = /*_.LAMBDA___V24*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-outobj.melt:1925:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PINI__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.OUTPUT_CURFRAME_DECLSTRUCT_INIT__V25*/ meltfptr[24] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_CURFRAME_DECLSTRUCT_INIT */ meltfrout->
		      tabval[7])),
		    (melt_ptr_t) ( /*_.LAMBDA___V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1933:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/**initial routine prologue**/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1934:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1935:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/* set initial frame marking */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1936:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1937:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((struct melt_callframe_st*)&meltfram__)->mcfr_nbvar = /*minihash*/ -"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1938:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#MINIHASH__L8*/ meltfnum[7]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1939:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1940:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1941:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("((struct melt_callframe_st*)&meltfram__)->mcfr_forwmarkrout = meltmod__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1942:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1943:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("__forward_or_mark_module_start_frame;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1944:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.CURPAIR__V26*/ meltfptr[25] =
	   melt_list_first ((melt_ptr_t) /*_.IPROLOG__V15*/ meltfptr[14]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V26*/ meltfptr[25]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V26*/ meltfptr[25] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V26*/ meltfptr[25]))
	{
	  /*_.CURPROL__V27*/ meltfptr[26] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V26*/ meltfptr[25]);


	  MELT_LOCATION ("warmelt-outobj.melt:1948:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CURPROL__V27*/ meltfptr[26])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

    /*_#IS_A__L10*/ meltfnum[9] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.CURPROL__V27*/ meltfptr[26]),
				       (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */ meltfrout->tabval[9])));;
		/*^compute */
    /*_#NOT__L11*/ meltfnum[10] =
		  (!( /*_#IS_A__L10*/ meltfnum[9]));;
		/*^compute */
		/*_#IF___L9*/ meltfnum[8] = /*_#NOT__L11*/ meltfnum[10];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1948:/ clear");
	      /*clear *//*_#IS_A__L10*/ meltfnum[9] = 0;
		/*^clear */
	      /*clear *//*_#NOT__L11*/ meltfnum[10] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_#IF___L9*/ meltfnum[8] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1948:/ cond");
	  /*cond */ if ( /*_#IF___L9*/ meltfnum[8])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:1950:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = 1;
		  /*_.OUTPUT_C_CODE__V28*/ meltfptr[27] =
		    meltgc_send ((melt_ptr_t)
				 ( /*_.CURPROL__V27*/ meltfptr[26]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[10])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1951:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    (1), 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1949:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1948:/ clear");
	      /*clear *//*_.OUTPUT_C_CODE__V28*/ meltfptr[27] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V26*/ meltfptr[25] = NULL;
     /*_.CURPROL__V27*/ meltfptr[26] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:1945:/ clear");
	    /*clear *//*_.CURPAIR__V26*/ meltfptr[25] = 0;
      /*^clear */
	    /*clear *//*_.CURPROL__V27*/ meltfptr[26] = 0;
      /*^clear */
	    /*clear *//*_#IF___L9*/ meltfnum[8] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1953:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/**initial routine cdata initializer**/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1954:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1956:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1957:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("memset(meltpredefinited, 0, sizeof(meltpredefinited));"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1958:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1959:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("initialize_module_meltdata_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1960:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1961:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" (&meltfram__, meltpredefinited);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1962:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1965:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/**initial routine body**/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1966:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1968:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBROUT_BODY");
  /*_.RAWBODY__V29*/ meltfptr[27] = slot;
    };
    ;
 /*_.BODYLIST__V30*/ meltfptr[29] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[11]))));;
    /*^compute */
 /*_.CHUNKBUFLIST__V31*/ meltfptr[30] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[11]))));;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit2__EACHLIST */
      for ( /*_.CURPAIR__V32*/ meltfptr[31] =
	   melt_list_first ((melt_ptr_t) /*_.RAWBODY__V29*/ meltfptr[27]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V32*/ meltfptr[31]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V32*/ meltfptr[31] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V32*/ meltfptr[31]))
	{
	  /*_.CURBODY__V33*/ meltfptr[32] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V32*/ meltfptr[31]);


	  MELT_LOCATION ("warmelt-outobj.melt:1975:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CURBODY__V33*/ meltfptr[32])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

    /*_#IS_A__L13*/ meltfnum[10] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.CURBODY__V33*/ meltfptr[32]),
				       (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */ meltfrout->tabval[9])));;
		/*^compute */
    /*_#NOT__L14*/ meltfnum[13] =
		  (!( /*_#IS_A__L13*/ meltfnum[10]));;
		/*^compute */
		/*_#IF___L12*/ meltfnum[9] = /*_#NOT__L14*/ meltfnum[13];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1975:/ clear");
	      /*clear *//*_#IS_A__L13*/ meltfnum[10] = 0;
		/*^clear */
	      /*clear *//*_#NOT__L14*/ meltfnum[13] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_#IF___L12*/ meltfnum[9] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1975:/ cond");
	  /*cond */ if ( /*_#IF___L12*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:1976:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.BODYLIST__V30*/ meltfptr[29]),
				      (melt_ptr_t) ( /*_.CURBODY__V33*/
						    meltfptr[32]));
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	}			/* end foreach_in_list meltcit2__EACHLIST */
     /*_.CURPAIR__V32*/ meltfptr[31] = NULL;
     /*_.CURBODY__V33*/ meltfptr[32] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:1972:/ clear");
	    /*clear *//*_.CURPAIR__V32*/ meltfptr[31] = 0;
      /*^clear */
	    /*clear *//*_.CURBODY__V33*/ meltfptr[32] = 0;
      /*^clear */
	    /*clear *//*_#IF___L12*/ meltfnum[9] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1977:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[13]);
      /*_.BODTUP__V34*/ meltfptr[33] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.BODYLIST__V30*/ meltfptr[29]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*_.CHUNKBUF__V35*/ meltfptr[34] = ( /*nil */ NULL);;
    /*^compute */
 /*_#NBBODY__L15*/ meltfnum[10] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.BODTUP__V34*/ meltfptr[33])));;
    /*^compute */
 /*_#CHUNKCOUNT__L16*/ meltfnum[13] = 0;;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit3__EACHTUP */
      long meltcit3__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.BODTUP__V34*/ meltfptr[33]);
      for ( /*_#BODIX__L17*/ meltfnum[16] = 0;
	   ( /*_#BODIX__L17*/ meltfnum[16] >= 0)
	   && ( /*_#BODIX__L17*/ meltfnum[16] < meltcit3__EACHTUP_ln);
	/*_#BODIX__L17*/ meltfnum[16]++)
	{
	  /*_.CURBODY__V36*/ meltfptr[35] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.BODTUP__V34*/ meltfptr[33]),
			       /*_#BODIX__L17*/ meltfnum[16]);



	  MELT_LOCATION ("warmelt-outobj.melt:1986:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IRAW__L18*/ meltfnum[17] =
	    (( /*_#BODIX__L17*/ meltfnum[16]) % (128));;
	  /*^compute */
  /*_#I__L19*/ meltfnum[18] =
	    (( /*_#IRAW__L18*/ meltfnum[17]) == (0));;
	  MELT_LOCATION ("warmelt-outobj.melt:1986:/ cond");
	  /*cond */ if ( /*_#I__L19*/ meltfnum[18])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:1987:/ quasiblock");


    /*_#CHUNKIX__L20*/ meltfnum[19] =
		  (( /*_#CHUNKCOUNT__L16*/ meltfnum[13]) + (1));;
		/*^compute */
    /*_.NEWCHUNKBUF__V39*/ meltfptr[38] =
		  (melt_ptr_t)
		  meltgc_new_strbuf ((meltobject_ptr_t)
				     (( /*!DISCR_STRBUF */ meltfrout->
				       tabval[14])), (const char *) 0);;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1991:/ locexp");
		   /*increment *//*_#CHUNKCOUNT__L16*/ meltfnum[13] += 1;
		  ;
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:1992:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.CHUNKBUFLIST__V31*/ meltfptr[30]),
				      (melt_ptr_t) ( /*_.NEWCHUNKBUF__V39*/
						    meltfptr[38]));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:1993:/ compute");
		/*_.CHUNKBUF__V35*/ meltfptr[34] =
		  /*_.SETQ___V40*/ meltfptr[39] =
		  /*_.NEWCHUNKBUF__V39*/ meltfptr[38];;
		/*_.LET___V38*/ meltfptr[37] = /*_.SETQ___V40*/ meltfptr[39];;

		MELT_LOCATION ("warmelt-outobj.melt:1987:/ clear");
	      /*clear *//*_#CHUNKIX__L20*/ meltfnum[19] = 0;
		/*^clear */
	      /*clear *//*_.NEWCHUNKBUF__V39*/ meltfptr[38] = 0;
		/*^clear */
	      /*clear *//*_.SETQ___V40*/ meltfptr[39] = 0;
		/*_.IF___V37*/ meltfptr[36] = /*_.LET___V38*/ meltfptr[37];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1986:/ clear");
	      /*clear *//*_.LET___V38*/ meltfptr[37] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V37*/ meltfptr[36] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1995:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*!PREVLOC_CONTAINER */
					      meltfrout->tabval[15]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*!PREVLOC_CONTAINER */ meltfrout->
				  tabval[15])), (0), (( /*nil */ NULL)),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch (( /*!PREVLOC_CONTAINER */ meltfrout->tabval[15]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*!PREVLOC_CONTAINER */ meltfrout->
					 tabval[15]), "put-fields");
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:1996:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
			  melt_magic_discr ((melt_ptr_t)
					    (( /*!PREVIMPLBUF_CONTAINER */
					      meltfrout->tabval[16]))) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object ((( /*!PREVIMPLBUF_CONTAINER */ meltfrout->
				  tabval[16])), (0), (( /*nil */ NULL)),
				"REFERENCED_VALUE");
	  ;
	  /*^touch */
	  meltgc_touch (( /*!PREVIMPLBUF_CONTAINER */ meltfrout->tabval[16]));
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object (( /*!PREVIMPLBUF_CONTAINER */
					 meltfrout->tabval[16]),
					"put-fields");
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:1997:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURBODY__V36*/
					       meltfptr[35]),
					      (melt_ptr_t) (( /*!CLASS_OBJINSTR */ meltfrout->tabval[17])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURBODY__V36*/ meltfptr[35]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "OBI_LOC");
    /*_.CURLOC__V42*/ meltfptr[39] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.CURLOC__V42*/ meltfptr[39] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:1999:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CURLOC__V42*/ meltfptr[39])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:2000:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CHUNKBUF__V35*/ meltfptr[34];
		  /*^apply.arg */
		  argtab[1].meltbp_long = 1;
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = "initchunk";
		  /*_.OUTPUT_LOCATION__V44*/ meltfptr[43] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!OUTPUT_LOCATION */ meltfrout->
				  tabval[18])),
				(melt_ptr_t) ( /*_.CURLOC__V42*/
					      meltfptr[39]),
				(MELTBPARSTR_PTR MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V43*/ meltfptr[37] =
		  /*_.OUTPUT_LOCATION__V44*/ meltfptr[43];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:1999:/ clear");
	      /*clear *//*_.OUTPUT_LOCATION__V44*/ meltfptr[43] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V43*/ meltfptr[37] = NULL;;
	    }
	  ;
	  /*^compute */
	  /*_.LET___V41*/ meltfptr[38] = /*_.IF___V43*/ meltfptr[37];;

	  MELT_LOCATION ("warmelt-outobj.melt:1997:/ clear");
	    /*clear *//*_.CURLOC__V42*/ meltfptr[39] = 0;
	  /*^clear */
	    /*clear *//*_.IF___V43*/ meltfptr[37] = 0;
	  MELT_LOCATION ("warmelt-outobj.melt:2002:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.CHUNKBUF__V35*/ meltfptr[34];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 1;
	    /*_.OUTPUT_C_CODE__V45*/ meltfptr[43] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURBODY__V36*/ meltfptr[35]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[10])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2003:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.CHUNKBUF__V35*/ meltfptr[34]),
				      (1), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2004:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#STRBUF_USEDLENGTH__L21*/ meltfnum[19] =
	    melt_strbuf_usedlength ((melt_ptr_t)
				    ( /*_.CHUNKBUF__V35*/ meltfptr[34]));;
	  MELT_LOCATION ("warmelt-outobj.melt:2004:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!BUFFER_LIMIT_CONT */
						meltfrout->tabval[19])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[20])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				 tabval[19])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
    /*_.REFERENCED_VALUE__V46*/ meltfptr[39] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.REFERENCED_VALUE__V46*/ meltfptr[39] = NULL;;
	    }
	  ;
	  /*^compute */
  /*_#GET_INT__L22*/ meltfnum[21] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V46*/ meltfptr[39])));;
	  /*^compute */
  /*_#IRAW__L23*/ meltfnum[22] =
	    (( /*_#GET_INT__L22*/ meltfnum[21]) / (2));;
	  /*^compute */
  /*_#I__L24*/ meltfnum[23] =
	    (( /*_#STRBUF_USEDLENGTH__L21*/ meltfnum[19]) >
	     ( /*_#IRAW__L23*/ meltfnum[22]));;
	  MELT_LOCATION ("warmelt-outobj.melt:2004:/ cond");
	  /*cond */ if ( /*_#I__L24*/ meltfnum[23])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:2005:/ locexp");

#if MELT_HAVE_DEBUG
		  if (melt_need_debug (0))
		    melt_dbgshortbacktrace (("outpucod_initialroutine huge chunkbuf"), (10));
#endif
		  ;
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:2006:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L25*/ meltfnum[24] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:2006:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L25*/ meltfnum[24])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L26*/ meltfnum[25] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:2006:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L26*/ meltfnum[25];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2006;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "outpucod_initialroutine huge chunkbuf=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CHUNKBUF__V35*/ meltfptr[34];
			  /*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V49*/ meltfptr[48] =
			  /*_.MELT_DEBUG_FUN__V50*/ meltfptr[49];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:2006:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L26*/ meltfnum[25] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V49*/ meltfptr[48] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:2006:/ quasiblock");


		  /*_.PROGN___V51*/ meltfptr[49] =
		    /*_.IF___V49*/ meltfptr[48];;
		  /*^compute */
		  /*_.IFCPP___V48*/ meltfptr[47] =
		    /*_.PROGN___V51*/ meltfptr[49];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:2006:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L25*/ meltfnum[24] = 0;
		  /*^clear */
		/*clear *//*_.IF___V49*/ meltfptr[48] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V51*/ meltfptr[49] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V48*/ meltfptr[47] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:2007:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#STRBUF_USEDLENGTH__L27*/ meltfnum[25] =
		    melt_strbuf_usedlength ((melt_ptr_t)
					    ( /*_.CHUNKBUF__V35*/
					     meltfptr[34]));;
		  MELT_LOCATION ("warmelt-outobj.melt:2008:/ cond");
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[19])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[20])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
					 tabval[19])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	/*_.REFERENCED_VALUE__V53*/ meltfptr[49] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.REFERENCED_VALUE__V53*/ meltfptr[49] = NULL;;
		    }
		  ;
		  /*^compute */
      /*_#GET_INT__L28*/ meltfnum[24] =
		    (melt_get_int
		     ((melt_ptr_t)
		      ( /*_.REFERENCED_VALUE__V53*/ meltfptr[49])));;
		  /*^compute */
      /*_#I__L29*/ meltfnum[28] =
		    (( /*_#STRBUF_USEDLENGTH__L27*/ meltfnum[25]) <
		     ( /*_#GET_INT__L28*/ meltfnum[24]));;
		  MELT_LOCATION ("warmelt-outobj.melt:2007:/ cond");
		  /*cond */ if ( /*_#I__L29*/ meltfnum[28])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V54*/ meltfptr[53] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:2007:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check limited chunkbuf"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (2007) ? (2007) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V54*/ meltfptr[53] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V52*/ meltfptr[48] =
		    /*_.IFELSE___V54*/ meltfptr[53];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:2007:/ clear");
		/*clear *//*_#STRBUF_USEDLENGTH__L27*/ meltfnum[25] = 0;
		  /*^clear */
		/*clear *//*_.REFERENCED_VALUE__V53*/ meltfptr[49] = 0;
		  /*^clear */
		/*clear *//*_#GET_INT__L28*/ meltfnum[24] = 0;
		  /*^clear */
		/*clear *//*_#I__L29*/ meltfnum[28] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V54*/ meltfptr[53] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V52*/ meltfptr[48] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:2004:/ quasiblock");


		/*_.PROGN___V55*/ meltfptr[49] =
		  /*_.IFCPP___V52*/ meltfptr[48];;
		/*^compute */
		/*_.IFELSE___V47*/ meltfptr[37] =
		  /*_.PROGN___V55*/ meltfptr[49];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2004:/ clear");
	      /*clear *//*_.IFCPP___V48*/ meltfptr[47] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V52*/ meltfptr[48] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V55*/ meltfptr[49] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IFELSE___V47*/ meltfptr[37] = NULL;;
	    }
	  ;
	  if ( /*_#BODIX__L17*/ meltfnum[16] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit3__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:1982:/ clear");
	    /*clear *//*_.CURBODY__V36*/ meltfptr[35] = 0;
      /*^clear */
	    /*clear *//*_#BODIX__L17*/ meltfnum[16] = 0;
      /*^clear */
	    /*clear *//*_#IRAW__L18*/ meltfnum[17] = 0;
      /*^clear */
	    /*clear *//*_#I__L19*/ meltfnum[18] = 0;
      /*^clear */
	    /*clear *//*_.IF___V37*/ meltfptr[36] = 0;
      /*^clear */
	    /*clear *//*_.LET___V41*/ meltfptr[38] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_CODE__V45*/ meltfptr[43] = 0;
      /*^clear */
	    /*clear *//*_#STRBUF_USEDLENGTH__L21*/ meltfnum[19] = 0;
      /*^clear */
	    /*clear *//*_.REFERENCED_VALUE__V46*/ meltfptr[39] = 0;
      /*^clear */
	    /*clear *//*_#GET_INT__L22*/ meltfnum[21] = 0;
      /*^clear */
	    /*clear *//*_#IRAW__L23*/ meltfnum[22] = 0;
      /*^clear */
	    /*clear *//*_#I__L24*/ meltfnum[23] = 0;
      /*^clear */
	    /*clear *//*_.IFELSE___V47*/ meltfptr[37] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-outobj.melt:1977:/ clear");
	   /*clear *//*_.BODTUP__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.CHUNKBUF__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_#NBBODY__L15*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_#CHUNKCOUNT__L16*/ meltfnum[13] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2012:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      (( /*!PREVLOC_CONTAINER */ meltfrout->
					tabval[15]))) == MELTOBMAG_OBJECT);
    melt_putfield_object ((( /*!PREVLOC_CONTAINER */ meltfrout->tabval[15])),
			  (0), (( /*nil */ NULL)), "REFERENCED_VALUE");
    ;
    /*^touch */
    meltgc_touch (( /*!PREVLOC_CONTAINER */ meltfrout->tabval[15]));
    ;
    /*^touchobj */

    melt_dbgtrace_written_object (( /*!PREVLOC_CONTAINER */ meltfrout->
				   tabval[15]), "put-fields");
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2013:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @REFERENCED_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      (( /*!PREVIMPLBUF_CONTAINER */
					meltfrout->tabval[16]))) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object ((( /*!PREVIMPLBUF_CONTAINER */ meltfrout->
			    tabval[16])), (0), (( /*nil */ NULL)),
			  "REFERENCED_VALUE");
    ;
    /*^touch */
    meltgc_touch (( /*!PREVIMPLBUF_CONTAINER */ meltfrout->tabval[16]));
    ;
    /*^touchobj */

    melt_dbgtrace_written_object (( /*!PREVIMPLBUF_CONTAINER */ meltfrout->
				   tabval[16]), "put-fields");
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2014:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[13]);
      /*_.CHUNKTUP__V56*/ meltfptr[53] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[12])),
		    (melt_ptr_t) ( /*_.CHUNKBUFLIST__V31*/ meltfptr[30]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2020:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2021:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("struct frame_melt_start_this_module_st;"));
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit4__EACHTUP */
      long meltcit4__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CHUNKTUP__V56*/ meltfptr[53]);
      for ( /*_#CHUNKIX__L30*/ meltfnum[25] = 0;
	   ( /*_#CHUNKIX__L30*/ meltfnum[25] >= 0)
	   && ( /*_#CHUNKIX__L30*/ meltfnum[25] < meltcit4__EACHTUP_ln);
	/*_#CHUNKIX__L30*/ meltfnum[25]++)
	{
	  /*_.CURCHUNK__V57*/ meltfptr[47] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.CHUNKTUP__V56*/ meltfptr[53]),
			       /*_#CHUNKIX__L30*/ meltfnum[25]);




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2025:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.DECLBUF__V3*/ meltfptr[2]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2026:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.DECLBUF__V3*/ meltfptr[2]),
				 ("void MELT_MODULE_VISIBILITY meltmod__"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2027:/ locexp");
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.DECLBUF__V3*/ meltfptr[2]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.OMODNAM__V17*/
							meltfptr[16])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2028:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.DECLBUF__V3*/ meltfptr[2]),
				 ("__initialmeltchunk_"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2029:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.DECLBUF__V3*/ meltfptr[2]),
				   ( /*_#CHUNKIX__L30*/ meltfnum[25]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2030:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.DECLBUF__V3*/ meltfptr[2]),
				 (" (struct frame_melt_start_this_module_st*, char*);"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2031:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (1),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2032:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("meltmod__"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2033:/ locexp");
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.OMODNAM__V17*/
							meltfptr[16])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2034:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("__initialmeltchunk_"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2035:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#CHUNKIX__L30*/ meltfnum[25]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2036:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 (" (&meltfram__, meltpredefinited);"));
	  }
	  ;
	  if ( /*_#CHUNKIX__L30*/ meltfnum[25] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit4__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:2022:/ clear");
	    /*clear *//*_.CURCHUNK__V57*/ meltfptr[47] = 0;
      /*^clear */
	    /*clear *//*_#CHUNKIX__L30*/ meltfnum[25] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2038:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2041:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2042:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2043:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" goto labend_rout;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2044:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2045:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("labend_rout:;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2046:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2047:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2048:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2049:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/* popped initial frame */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2050:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2051:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("{ /* clear initial frame & return */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2052:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" melt_ptr_t retval = "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2053:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ORETVAL__V16*/ meltfptr[15])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2054:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = 1;
	    /*_.OUTPUT_C_CODE__V59*/ meltfptr[49] =
	      meltgc_send ((melt_ptr_t) ( /*_.ORETVAL__V16*/ meltfptr[15]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[10])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IFELSE___V58*/ meltfptr[48] =
	    /*_.OUTPUT_C_CODE__V59*/ meltfptr[49];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2053:/ clear");
	     /*clear *//*_.OUTPUT_C_CODE__V59*/ meltfptr[49] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2055:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("/*noretval*/ NULL"));
	  }
	  ;
	     /*clear *//*_.IFELSE___V58*/ meltfptr[48] = 0;
	  /*epilog */
	}
	;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2056:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (";"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2057:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2058:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" memset((void*) &meltfram__, 0, sizeof(meltfram__));"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2059:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2060:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" return retval;}"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2061:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2062:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef meltcallcount"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2063:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2064:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef meltfram__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2065:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2066:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef MELTFRAM_NBVARNUM"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2067:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2068:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("#undef MELTFRAM_NBVARPTR"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2069:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2070:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2071:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("} /* end  */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2072:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit5__EACHTUP */
      long meltcit5__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.CHUNKTUP__V56*/ meltfptr[53]);
      for ( /*_#CHUNKIX__L31*/ meltfnum[24] = 0;
	   ( /*_#CHUNKIX__L31*/ meltfnum[24] >= 0)
	   && ( /*_#CHUNKIX__L31*/ meltfnum[24] < meltcit5__EACHTUP_ln);
	/*_#CHUNKIX__L31*/ meltfnum[24]++)
	{
	  /*_.CURCHUNK__V60*/ meltfptr[33] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.CHUNKTUP__V56*/ meltfptr[53]),
			       /*_#CHUNKIX__L31*/ meltfnum[24]);




	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2077:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2078:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("void meltmod__"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2079:/ locexp");
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.OMODNAM__V17*/
							meltfptr[16])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2080:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("__initialmeltchunk_"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2081:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#CHUNKIX__L31*/ meltfnum[24]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2082:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 (" (struct frame_melt_start_this_module_st* meltmeltframptr__, char meltpredefi\
nited[]) {"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2083:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2084:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("#define meltfram__ (*meltmeltframptr__)"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2085:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2086:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("#undef meltcallcount"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2087:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2088:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("#define meltcallcount 0L"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2089:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2090:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("(void) meltpredefinited;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2091:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2092:/ locexp");
	    /*add2sbuf_sbuf */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_strbuf_str ((melt_ptr_t)
						  /*_.CURCHUNK__V60*/
						  meltfptr[33]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2093:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2094:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("#undef meltfram__"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2095:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2096:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("} /*end of meltmod__"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2097:/ locexp");
	    meltgc_add_strbuf_cident ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      melt_string_str ((melt_ptr_t)
						       ( /*_.OMODNAM__V17*/
							meltfptr[16])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2098:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("__initialmeltchunk_"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2099:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#CHUNKIX__L31*/ meltfnum[24]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2100:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("*/"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2101:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]), (0),
				      0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2103:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#STRBUF_USEDLENGTH__L32*/ meltfnum[28] =
	    melt_strbuf_usedlength ((melt_ptr_t)
				    ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	  MELT_LOCATION ("warmelt-outobj.melt:2103:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      (( /*!BUFFER_LIMIT_CONT */
						meltfrout->tabval[19])),
					      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[20])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				 tabval[19])) /*=obj*/ ;
		melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
    /*_.REFERENCED_VALUE__V61*/ meltfptr[34] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.REFERENCED_VALUE__V61*/ meltfptr[34] = NULL;;
	    }
	  ;
	  /*^compute */
  /*_#GET_INT__L33*/ meltfnum[10] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V61*/ meltfptr[34])));;
	  /*^compute */
  /*_#IRAW__L34*/ meltfnum[13] =
	    (( /*_#GET_INT__L33*/ meltfnum[10]) / (2));;
	  /*^compute */
  /*_#I__L35*/ meltfnum[34] =
	    (( /*_#STRBUF_USEDLENGTH__L32*/ meltfnum[28]) >
	     ( /*_#IRAW__L34*/ meltfnum[13]));;
	  MELT_LOCATION ("warmelt-outobj.melt:2103:/ cond");
	  /*cond */ if ( /*_#I__L35*/ meltfnum[34])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:2104:/ locexp");

#if MELT_HAVE_DEBUG
		  if (melt_need_debug (0))
		    melt_dbgshortbacktrace (("outpucod_initialroutine huge implbuf"), (10));
#endif
		  ;
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:2105:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L36*/ meltfnum[35] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:2105:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L36*/ meltfnum[35])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L37*/ meltfnum[36] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:2105:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L37*/ meltfnum[36];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2105;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "outpucod_initialroutine huge implbuf=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
			  /*_.MELT_DEBUG_FUN__V65*/ meltfptr[64] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V64*/ meltfptr[63] =
			  /*_.MELT_DEBUG_FUN__V65*/ meltfptr[64];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:2105:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L37*/ meltfnum[36] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V65*/ meltfptr[64] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V64*/ meltfptr[63] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:2105:/ quasiblock");


		  /*_.PROGN___V66*/ meltfptr[64] =
		    /*_.IF___V64*/ meltfptr[63];;
		  /*^compute */
		  /*_.IFCPP___V63*/ meltfptr[62] =
		    /*_.PROGN___V66*/ meltfptr[64];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:2105:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L36*/ meltfnum[35] = 0;
		  /*^clear */
		/*clear *//*_.IF___V64*/ meltfptr[63] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V66*/ meltfptr[64] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V63*/ meltfptr[62] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:2106:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#STRBUF_USEDLENGTH__L38*/ meltfnum[36] =
		    melt_strbuf_usedlength ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/
					     meltfptr[3]));;
		  MELT_LOCATION ("warmelt-outobj.melt:2107:/ cond");
		  /*cond */ if (
				 /*ifisa */
				 melt_is_instance_of ((melt_ptr_t)
						      (( /*!BUFFER_LIMIT_CONT */ meltfrout->tabval[19])),
						      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[20])))
		    )		/*then */
		    {
		      /*^cond.then */
		      /*^getslot */
		      {
			melt_ptr_t slot = NULL, obj = NULL;
			obj =
			  (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
					 tabval[19])) /*=obj*/ ;
			melt_object_get_field (slot, obj, 0,
					       "REFERENCED_VALUE");
	/*_.REFERENCED_VALUE__V68*/ meltfptr[64] = slot;
		      };
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.REFERENCED_VALUE__V68*/ meltfptr[64] = NULL;;
		    }
		  ;
		  /*^compute */
      /*_#GET_INT__L39*/ meltfnum[35] =
		    (melt_get_int
		     ((melt_ptr_t)
		      ( /*_.REFERENCED_VALUE__V68*/ meltfptr[64])));;
		  /*^compute */
      /*_#I__L40*/ meltfnum[39] =
		    (( /*_#STRBUF_USEDLENGTH__L38*/ meltfnum[36]) <
		     ( /*_#GET_INT__L39*/ meltfnum[35]));;
		  MELT_LOCATION ("warmelt-outobj.melt:2106:/ cond");
		  /*cond */ if ( /*_#I__L40*/ meltfnum[39])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V69*/ meltfptr[68] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:2106:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check limited implbuf"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (2106) ? (2106) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		  /*clear *//*_.IFELSE___V69*/ meltfptr[68] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V67*/ meltfptr[63] =
		    /*_.IFELSE___V69*/ meltfptr[68];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:2106:/ clear");
		/*clear *//*_#STRBUF_USEDLENGTH__L38*/ meltfnum[36] = 0;
		  /*^clear */
		/*clear *//*_.REFERENCED_VALUE__V68*/ meltfptr[64] = 0;
		  /*^clear */
		/*clear *//*_#GET_INT__L39*/ meltfnum[35] = 0;
		  /*^clear */
		/*clear *//*_#I__L40*/ meltfnum[39] = 0;
		  /*^clear */
		/*clear *//*_.IFELSE___V69*/ meltfptr[68] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V67*/ meltfptr[63] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:2103:/ quasiblock");


		/*_.PROGN___V70*/ meltfptr[64] =
		  /*_.IFCPP___V67*/ meltfptr[63];;
		/*^compute */
		/*_.IFELSE___V62*/ meltfptr[49] =
		  /*_.PROGN___V70*/ meltfptr[64];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2103:/ clear");
	      /*clear *//*_.IFCPP___V63*/ meltfptr[62] = 0;
		/*^clear */
	      /*clear *//*_.IFCPP___V67*/ meltfptr[63] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V70*/ meltfptr[64] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IFELSE___V62*/ meltfptr[49] = NULL;;
	    }
	  ;
	  if ( /*_#CHUNKIX__L31*/ meltfnum[24] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit5__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:2074:/ clear");
	    /*clear *//*_.CURCHUNK__V60*/ meltfptr[33] = 0;
      /*^clear */
	    /*clear *//*_#CHUNKIX__L31*/ meltfnum[24] = 0;
      /*^clear */
	    /*clear *//*_#STRBUF_USEDLENGTH__L32*/ meltfnum[28] = 0;
      /*^clear */
	    /*clear *//*_.REFERENCED_VALUE__V61*/ meltfptr[34] = 0;
      /*^clear */
	    /*clear *//*_#GET_INT__L33*/ meltfnum[10] = 0;
      /*^clear */
	    /*clear *//*_#IRAW__L34*/ meltfnum[13] = 0;
      /*^clear */
	    /*clear *//*_#I__L35*/ meltfnum[34] = 0;
      /*^clear */
	    /*clear *//*_.IFELSE___V62*/ meltfptr[49] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2014:/ clear");
	   /*clear *//*_.CHUNKTUP__V56*/ meltfptr[53] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V58*/ meltfptr[48] = 0;

    MELT_LOCATION ("warmelt-outobj.melt:1968:/ clear");
	   /*clear *//*_.RAWBODY__V29*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.BODYLIST__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.CHUNKBUFLIST__V31*/ meltfptr[30] = 0;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2112:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2113:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("void MELT_MODULE_VISIBILITY meltmod__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2114:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2115:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
			   ("__forward_or_mark_module_start_frame (struct melt_callframe_st* fp\
, int marking);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2116:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2117:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"#define meltmarking_melt_start_this_module  meltmod__";
      /*_.ADD2OUT__V71*/ meltfptr[68] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2118:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2119:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "__forward_or_mark_module_start_frame";
      /*_.ADD2OUT__V72*/ meltfptr[62] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2120:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2121:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2122:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("void meltmod__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2123:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2124:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("__forward_or_mark_module_start_frame (struct melt_callframe_st* fp\
, int marking)"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2125:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2126:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("{"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2127:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2128:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("int ix=0;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2129:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2130:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("initial_frame_st* meltframptr_= (initial_frame_st*)fp;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2131:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2132:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" melt_assertmsg (\"check module frame\", meltframptr_->mcfr_nbvar \
== /*minihash*/ -"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2133:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#MINIHASH__L8*/ meltfnum[7]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2134:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (");"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2135:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2137:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("if (!marking && melt_is_forwarding) {"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2138:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2139:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("dbgprintf (\"forward_or_mark_module_start_frame_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2140:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2141:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" forwarding %d pointers in frame %p\", "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2142:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#NBVAL__L5*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2143:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (", (void*) meltframptr_);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2144:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2145:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("for (ix = 0;  ix < "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2146:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#NBVAL__L5*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2147:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("; ix++)  MELT_FORWARDED(meltframptr_->mcfr_varptr[ix]);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2148:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2149:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" return;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2150:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2151:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("} /*end forwarding*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2152:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2154:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("dbgprintf (\"forward_or_mark_module_start_frame_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2155:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2156:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" marking in frame %p\", (void*) meltframptr_);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2157:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2158:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.OUTPUCOD_MARKER__V73*/ meltfptr[63] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUCOD_MARKER */ meltfrout->tabval[21])),
		    (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2159:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2160:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("} /* end meltmod__"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2161:/ locexp");
      meltgc_add_strbuf_cident ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.OMODNAM__V17*/
						  meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2162:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("__forward_or_mark_module_start_frame */"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2163:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2164:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2165:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L41*/ meltfnum[36] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2166:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[19])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[20])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[19])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V75*/ meltfptr[53] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V75*/ meltfptr[53] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L42*/ meltfnum[35] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V75*/ meltfptr[53])));;
      /*^compute */
   /*_#I__L43*/ meltfnum[39] =
	(( /*_#STRBUF_USEDLENGTH__L41*/ meltfnum[36]) <
	 ( /*_#GET_INT__L42*/ meltfnum[35]));;
      MELT_LOCATION ("warmelt-outobj.melt:2165:/ cond");
      /*cond */ if ( /*_#I__L43*/ meltfnum[39])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V76*/ meltfptr[48] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2165:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2165) ? (2165) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V76*/ meltfptr[48] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V74*/ meltfptr[64] = /*_.IFELSE___V76*/ meltfptr[48];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2165:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L41*/ meltfnum[36] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V75*/ meltfptr[53] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L42*/ meltfnum[35] = 0;
      /*^clear */
	     /*clear *//*_#I__L43*/ meltfnum[39] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V76*/ meltfptr[48] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V74*/ meltfptr[64] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2167:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L44*/ meltfnum[36] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.DECLBUF__V3*/ meltfptr[2]));;
      MELT_LOCATION ("warmelt-outobj.melt:2168:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[19])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[20])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[19])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V78*/ meltfptr[29] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V78*/ meltfptr[29] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L45*/ meltfnum[35] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V78*/ meltfptr[29])));;
      /*^compute */
   /*_#I__L46*/ meltfnum[39] =
	(( /*_#STRBUF_USEDLENGTH__L44*/ meltfnum[36]) <
	 ( /*_#GET_INT__L45*/ meltfnum[35]));;
      MELT_LOCATION ("warmelt-outobj.melt:2167:/ cond");
      /*cond */ if ( /*_#I__L46*/ meltfnum[39])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V79*/ meltfptr[30] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2167:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited declbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2167) ? (2167) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V79*/ meltfptr[30] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V77*/ meltfptr[27] = /*_.IFELSE___V79*/ meltfptr[30];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2167:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L44*/ meltfnum[36] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V78*/ meltfptr[29] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L45*/ meltfnum[35] = 0;
      /*^clear */
	     /*clear *//*_#I__L46*/ meltfnum[39] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V79*/ meltfptr[30] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V77*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V12*/ meltfptr[6] = /*_.IFCPP___V77*/ meltfptr[27];;

    MELT_LOCATION ("warmelt-outobj.melt:1809:/ clear");
	   /*clear *//*_.IDATUP__V13*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IRFILL__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IPROLOG__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.ORETVAL__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OMODNAM__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.ONBVAL__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#NBVAL__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L6*/ meltfnum[2] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#MINIHASH__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_CURFRAME_DECLSTRUCT__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_CURFRAME_CDAT_STRUCT__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_CURFRAME_CDAT_FILL__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_CURFRAME_DECLSTRUCT_INIT__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V71*/ meltfptr[68] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V72*/ meltfptr[62] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUCOD_MARKER__V73*/ meltfptr[63] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V74*/ meltfptr[64] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V77*/ meltfptr[27] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:1805:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1805:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_INITIALMODULEROUTINE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_44_warmelt_outobj_OUTPUCOD_INITIALMODULEROUTINE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_outobj_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_45_warmelt_outobj_LAMBDA___4___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_45_warmelt_outobj_LAMBDA___4___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_45_warmelt_outobj_LAMBDA___4__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_45_warmelt_outobj_LAMBDA___4___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_45_warmelt_outobj_LAMBDA___4__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:1926:/ getarg");
 /*_.ROU__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DSBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:1927:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:1928:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
			   ("initial_frame_st "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:1926:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_45_warmelt_outobj_LAMBDA___4___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_45_warmelt_outobj_LAMBDA___4__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un *
							     meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un *
							     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 85
    melt_ptr_t mcfr_varptr[85];
#define MELTFRAM_NBVARNUM 27
    long mcfr_varnum[27];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 85; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE nbval 85*/
  meltfram__.mcfr_nbvar = 85 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_INITIALEXTENSIONROUTINE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2175:/ getarg");
 /*_.PINI__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2176:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_INITIAL_EXTENSION_ROUTINEOBJ */ meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2176:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2176:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pini"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2176) ? (2176) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2176:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2177:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:2177:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    /*^compute */
     /*_.DISCRIM__V9*/ meltfptr[8] =
	      ((melt_ptr_t)
	       (melt_discr ((melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]))));;
	    MELT_LOCATION ("warmelt-outobj.melt:2177:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2177;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"outpucod_initialextensionroutine start pini=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.PINI__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n of discrim ";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DISCRIM__V9*/ meltfptr[8];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V8*/ meltfptr[7] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2177:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.DISCRIM__V9*/ meltfptr[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V8*/ meltfptr[7] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:2177:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[8] = /*_.IF___V8*/ meltfptr[7];;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.PROGN___V11*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2177:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2179:/ locexp");

#if MELT_HAVE_DEBUG
      if (melt_need_debug (0))
	melt_dbgshortbacktrace (("outpucod_initialextensionroutine"), (20));
#endif
      ;
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2180:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:2181:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.PINI__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NAMED */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
   /*_.ONAME__V12*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ONAME__V12*/ meltfptr[9] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2182:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.PINI__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 10, "OIROUT_DATA");
   /*_.ODATUP__V13*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ODATUP__V13*/ meltfptr[7] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2183:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.PINI__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 12, "OIROUT_FILL");
   /*_.ORFILL__V14*/ meltfptr[8] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ORFILL__V14*/ meltfptr[8] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2184:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.PINI__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_INITIALROUTINEOBJ */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 11, "OIROUT_PROLOG");
   /*_.OPROLOG__V15*/ meltfptr[14] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OPROLOG__V15*/ meltfptr[14] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2185:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 13, "OIROUT_MODULENAME");
  /*_.OMODNAM__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2186:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.PINI__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "OBROUT_BODY");
   /*_.OBODY__V17*/ meltfptr[16] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OBODY__V17*/ meltfptr[16] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2187:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.PINI__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 4, "OBROUT_NBVAL");
   /*_.ONBVAL__V18*/ meltfptr[17] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ONBVAL__V18*/ meltfptr[17] = NULL;;
      }
    ;
    /*^compute */
 /*_#NBVAL__L5*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.ONBVAL__V18*/ meltfptr[17])));;
    /*^compute */
 /*_#OBJ_HASH__L6*/ meltfnum[1] =
      (melt_obj_hash ((melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1])));;
    /*^compute */
 /*_#IRAW__L7*/ meltfnum[6] =
      (( /*_#OBJ_HASH__L6*/ meltfnum[1]) % (4096));;
    /*^compute */
 /*_#MINIHASH__L8*/ meltfnum[7] =
      ((1) + ( /*_#IRAW__L7*/ meltfnum[6]));;
    MELT_LOCATION ("warmelt-outobj.melt:2190:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.PINI__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_ROUTINEOBJ */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 7, "OBROUT_RETVAL");
   /*_.ORETVAL__V19*/ meltfptr[18] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.ORETVAL__V19*/ meltfptr[18] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2192:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2193:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2194:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[15];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	" /** declare initial extension running routine ";
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.ONAME__V12*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " \n* of ";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[4].meltbp_cstring = " [outpucod_initialextensionroutine] **/\
\nMELT_EXTERN melt_ptr_t ";
      /*^apply.arg */
      argtab[5].meltbp_aptr = (melt_ptr_t *) & /*_.ONAME__V12*/ meltfptr[9];
      /*^apply.arg */
      argtab[6].meltbp_cstring =
	" (melt_ptr_t meltarg_curenvbox_p, melt_ptr_t meltarg_tuplitval_p) \
;\n\n#ifdef MELT_HAVE_DEBUG\nMELT_EXTERN const char meltextend_";
      /*^apply.arg */
      argtab[7].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[8].meltbp_cstring =
	"_have_debug_enabled[] ;\n#define melt_have_debug_string meltextend_";
      /*^apply.arg */
      argtab[9].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[10].meltbp_cstring =
	"_have_debug_enabled\n#else\nMELT_EXTERN const char meltextend_";
      /*^apply.arg */
      argtab[11].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[12].meltbp_cstring =
	"_have_debug_disabled[] ;\n#define melt_have_debug_string meltextend_";
      /*^apply.arg */
      argtab[13].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[14].meltbp_cstring =
	"_have_debug_disabled\n#endif /* MELT_HAVE_DEBUG */\
\n\n     ";
      /*_.ADD2OUT__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2209:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2210:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[17];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " /* implement extension running routine ";
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.ONAME__V12*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " \nof ";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[4].meltbp_cstring = " [outpucod_initialextensionroutine] **/\
\n#ifdef MELT_HAVE_DEBUG\nconst char meltextend_";
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[6].meltbp_cstring =
	"_have_debug_enabled[]\n= \"MELT running extension ";
      /*^apply.arg */
      argtab[7].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[8].meltbp_cstring =
	" has debug enabled\" ;\n#else\nconst char meltextend_";
      /*^apply.arg */
      argtab[9].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[10].meltbp_cstring =
	"_have_debug_disabled[]\n= \"MELT running extension ";
      /*^apply.arg */
      argtab[11].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[12].meltbp_cstring =
	" has debug disabled\" ;\n#endif /* MELT_HAVE_DEBUG */\
\n\n\n/******* starting initial extend run routine ";
      /*^apply.arg */
      argtab[13].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[14].meltbp_cstring = " ******/\nmelt_ptr_t \n";
      /*^apply.arg */
      argtab[15].meltbp_aptr = (melt_ptr_t *) & /*_.ONAME__V12*/ meltfptr[9];
      /*^apply.arg */
      argtab[16].meltbp_cstring =
	" (melt_ptr_t meltarg_curenvbox_p, melt_ptr_t meltarg_tuplitval_p) \
\n{\n";
      /*_.ADD2OUT__V21*/ meltfptr[20] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2232:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2233:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/* extension routine ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " initial frame decl */ typedef ";
      /*_.ADD2OUT__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2234:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*_.OUTPUT_CURFRAME_DECLSTRUCT__V23*/ meltfptr[22] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_CURFRAME_DECLSTRUCT */ meltfrout->
		      tabval[6])), (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2235:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " meltrun_";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "_initialext_frame_t /*declaring runextend ";
      /*^apply.arg */
      argtab[3].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[4].meltbp_cstring = " frame type*/;";
      /*_.ADD2OUT__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2237:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2241:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2242:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/* extension routine ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " initial frame marking routine decl */\
\n     static void meltmarking_";
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.ONAME__V12*/ meltfptr[9];
      /*^apply.arg */
      argtab[4].meltbp_cstring = " (struct melt_callframe_st*, int);\n";
      /*_.ADD2OUT__V25*/ meltfptr[24] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2249:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2250:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/* extension routine ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " initial frame init */";
      /*_.ADD2OUT__V26*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2252:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V28*/ meltfptr[27] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_9 */ meltfrout->
						tabval[9])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V28*/ meltfptr[27])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V28*/ meltfptr[27])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V28*/ meltfptr[27])->tabval[0] =
      (melt_ptr_t) ( /*_.OMODNAM__V16*/ meltfptr[15]);
    ;
    /*_.LAMBDA___V27*/ meltfptr[26] = /*_.LAMBDA___V28*/ meltfptr[27];;
    MELT_LOCATION ("warmelt-outobj.melt:2251:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.PINI__V2*/ meltfptr[1];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.OUTPUT_CURFRAME_DECLSTRUCT_INIT__V29*/ meltfptr[28] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_CURFRAME_DECLSTRUCT_INIT */ meltfrout->
		      tabval[7])),
		    (melt_ptr_t) ( /*_.LAMBDA___V27*/ meltfptr[26]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2256:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2257:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " /* extension ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " initialized initial frame */";
      /*_.ADD2OUT__V30*/ meltfptr[29] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2259:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2260:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " /* extension ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " cdata struct */";
      /*_.ADD2OUT__V31*/ meltfptr[30] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2261:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.OUTPUT_CURFRAME_CDAT_STRUCT__V32*/ meltfptr[31] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_CURFRAME_CDAT_STRUCT */ meltfrout->
		      tabval[10])),
		    (melt_ptr_t) ( /*_.ODATUP__V13*/ meltfptr[7]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2262:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" *cdat = NULL;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2263:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2265:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[8] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:2265:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:2265:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2265;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"outpucod_initialextensionroutine oprolog=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OPROLOG__V15*/ meltfptr[14];
	      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V34*/ meltfptr[33] =
	      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2265:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V34*/ meltfptr[33] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:2265:/ quasiblock");


      /*_.PROGN___V36*/ meltfptr[34] = /*_.IF___V34*/ meltfptr[33];;
      /*^compute */
      /*_.IFCPP___V33*/ meltfptr[32] = /*_.PROGN___V36*/ meltfptr[34];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2265:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IF___V34*/ meltfptr[33] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V36*/ meltfptr[34] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V33*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2266:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2267:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " /* extension ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " prologue */";
      /*_.ADD2OUT__V37*/ meltfptr[33] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.CURPAIR__V38*/ meltfptr[34] =
	   melt_list_first ((melt_ptr_t) /*_.OPROLOG__V15*/ meltfptr[14]);
	   melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V38*/ meltfptr[34]) ==
	   MELTOBMAG_PAIR;
	   /*_.CURPAIR__V38*/ meltfptr[34] =
	   melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V38*/ meltfptr[34]))
	{
	  /*_.CURPROL__V39*/ meltfptr[38] =
	    melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V38*/ meltfptr[34]);


	  MELT_LOCATION ("warmelt-outobj.melt:2271:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CURPROL__V39*/ meltfptr[38])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

    /*_#IS_A__L12*/ meltfnum[8] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.CURPROL__V39*/ meltfptr[38]),
				       (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */ meltfrout->tabval[11])));;
		/*^compute */
    /*_#NOT__L13*/ meltfnum[12] =
		  (!( /*_#IS_A__L12*/ meltfnum[8]));;
		/*^compute */
		/*_#IF___L11*/ meltfnum[9] = /*_#NOT__L13*/ meltfnum[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2271:/ clear");
	      /*clear *//*_#IS_A__L12*/ meltfnum[8] = 0;
		/*^clear */
	      /*clear *//*_#NOT__L13*/ meltfnum[12] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_#IF___L11*/ meltfnum[9] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2271:/ cond");
	  /*cond */ if ( /*_#IF___L11*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:2272:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    (1), 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2273:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = 1;
		  /*_.OUTPUT_C_CODE__V41*/ meltfptr[40] =
		    meltgc_send ((melt_ptr_t)
				 ( /*_.CURPROL__V39*/ meltfptr[38]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[12])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2271:/ quasiblock");


		/*_.PROGN___V42*/ meltfptr[41] =
		  /*_.OUTPUT_C_CODE__V41*/ meltfptr[40];;
		/*^compute */
		/*_.IF___V40*/ meltfptr[39] = /*_.PROGN___V42*/ meltfptr[41];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2271:/ clear");
	      /*clear *//*_.OUTPUT_C_CODE__V41*/ meltfptr[40] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V42*/ meltfptr[41] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V40*/ meltfptr[39] = NULL;;
	    }
	  ;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V38*/ meltfptr[34] = NULL;
     /*_.CURPROL__V39*/ meltfptr[38] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:2268:/ clear");
	    /*clear *//*_.CURPAIR__V38*/ meltfptr[34] = 0;
      /*^clear */
	    /*clear *//*_.CURPROL__V39*/ meltfptr[38] = 0;
      /*^clear */
	    /*clear *//*_#IF___L11*/ meltfnum[9] = 0;
      /*^clear */
	    /*clear *//*_.IF___V40*/ meltfptr[39] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2275:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2276:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " /* extension ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " cdata fill */";
      /*_.ADD2OUT__V43*/ meltfptr[40] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2277:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.OUTPUT_CURFRAME_CDAT_FILL__V44*/ meltfptr[41] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_CURFRAME_CDAT_FILL */ meltfrout->
		      tabval[13])),
		    (melt_ptr_t) ( /*_.ODATUP__V13*/ meltfptr[7]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2278:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2279:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " /* extension ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " cdata done fill */";
      /*_.ADD2OUT__V45*/ meltfptr[44] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2280:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2282:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L14*/ meltfnum[8] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:2282:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:2282:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L15*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2282;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"outpucod_initialextensionroutine obody=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBODY__V17*/ meltfptr[16];
	      /*_.MELT_DEBUG_FUN__V48*/ meltfptr[47] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V47*/ meltfptr[46] =
	      /*_.MELT_DEBUG_FUN__V48*/ meltfptr[47];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2282:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V48*/ meltfptr[47] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V47*/ meltfptr[46] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:2282:/ quasiblock");


      /*_.PROGN___V49*/ meltfptr[47] = /*_.IF___V47*/ meltfptr[46];;
      /*^compute */
      /*_.IFCPP___V46*/ meltfptr[45] = /*_.PROGN___V49*/ meltfptr[47];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2282:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IF___V47*/ meltfptr[46] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V49*/ meltfptr[47] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V46*/ meltfptr[45] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2283:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2284:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:2286:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L16*/ meltfnum[12] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.OBODY__V17*/ meltfptr[16])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2286:/ cond");
    /*cond */ if ( /*_#IS_LIST__L16*/ meltfnum[12])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[15]);
	    /*_.LIST_TO_MULTIPLE__V51*/ meltfptr[47] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[14])),
			  (melt_ptr_t) ( /*_.OBODY__V17*/ meltfptr[16]),
			  (MELTBPARSTR_PTR ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.BODTUP__V50*/ meltfptr[46] =
	    /*_.LIST_TO_MULTIPLE__V51*/ meltfptr[47];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2286:/ clear");
	     /*clear *//*_.LIST_TO_MULTIPLE__V51*/ meltfptr[47] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2287:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_MULTIPLE__L17*/ meltfnum[8] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.OBODY__V17*/ meltfptr[16]))
	     == MELTOBMAG_MULTIPLE);;
	  MELT_LOCATION ("warmelt-outobj.melt:2287:/ cond");
	  /*cond */ if ( /*_#IS_MULTIPLE__L17*/ meltfnum[8])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V52*/ meltfptr[47] =
		/*_.OBODY__V17*/ meltfptr[16];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-outobj.melt:2287:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:2289:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (( /*nil */ NULL))	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V54*/ meltfptr[53] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-outobj.melt:2289:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("unexpected body"),
					      ("warmelt-outobj.melt")
					      ? ("warmelt-outobj.melt") :
					      __FILE__,
					      (2289) ? (2289) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V54*/ meltfptr[53] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V53*/ meltfptr[52] =
		    /*_.IFELSE___V54*/ meltfptr[53];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:2289:/ clear");
		 /*clear *//*_.IFELSE___V54*/ meltfptr[53] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V53*/ meltfptr[52] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-outobj.melt:2288:/ quasiblock");


		/*_.PROGN___V55*/ meltfptr[53] =
		  /*_.IFCPP___V53*/ meltfptr[52];;
		/*^compute */
		/*_.IFELSE___V52*/ meltfptr[47] =
		  /*_.PROGN___V55*/ meltfptr[53];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2287:/ clear");
	       /*clear *//*_.IFCPP___V53*/ meltfptr[52] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V55*/ meltfptr[53] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.BODTUP__V50*/ meltfptr[46] = /*_.IFELSE___V52*/ meltfptr[47];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2286:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L17*/ meltfnum[8] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V52*/ meltfptr[47] = 0;
	}
	;
      }
    ;
 /*_#NBBODY__L18*/ meltfnum[8] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.BODTUP__V50*/ meltfptr[46])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2292:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L19*/ meltfnum[18] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-outobj.melt:2292:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[18])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:2292:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-outobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 2292;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"outpucod_initialextensionroutine bodtup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BODTUP__V50*/ meltfptr[46];
	      /*_.MELT_DEBUG_FUN__V58*/ meltfptr[47] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V57*/ meltfptr[53] =
	      /*_.MELT_DEBUG_FUN__V58*/ meltfptr[47];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2292:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[19] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V58*/ meltfptr[47] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V57*/ meltfptr[53] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-outobj.melt:2292:/ quasiblock");


      /*_.PROGN___V59*/ meltfptr[47] = /*_.IF___V57*/ meltfptr[53];;
      /*^compute */
      /*_.IFCPP___V56*/ meltfptr[52] = /*_.PROGN___V59*/ meltfptr[47];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2292:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[18] = 0;
      /*^clear */
	     /*clear *//*_.IF___V57*/ meltfptr[53] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V59*/ meltfptr[47] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V56*/ meltfptr[52] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2293:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " /* extension ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " body of ";
      /*^apply.arg */
      argtab[3].meltbp_long = /*_#NBBODY__L18*/ meltfnum[8];
      /*^apply.arg */
      argtab[4].meltbp_cstring = " instructions */";
      /*_.ADD2OUT__V60*/ meltfptr[53] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.BODTUP__V50*/ meltfptr[46]);
      for ( /*_#BODIX__L21*/ meltfnum[19] = 0;
	   ( /*_#BODIX__L21*/ meltfnum[19] >= 0)
	   && ( /*_#BODIX__L21*/ meltfnum[19] < meltcit2__EACHTUP_ln);
	/*_#BODIX__L21*/ meltfnum[19]++)
	{
	  /*_.CURBODY__V61*/ meltfptr[47] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.BODTUP__V50*/ meltfptr[46]),
			       /*_#BODIX__L21*/ meltfnum[19]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:2297:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#MELT_NEED_DBG__L22*/ meltfnum[18] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-outobj.melt:2297:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L22*/ meltfnum[18])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

      /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  /*^compute */
      /*_.DISCRIM__V64*/ meltfptr[63] =
		    ((melt_ptr_t)
		     (melt_discr
		      ((melt_ptr_t) ( /*_.CURBODY__V61*/ meltfptr[47]))));;
		  MELT_LOCATION ("warmelt-outobj.melt:2297:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[10];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-outobj.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 2297;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring =
		      "outpucod_initialextensionroutine bodix=";
		    /*^apply.arg */
		    argtab[4].meltbp_long = /*_#BODIX__L21*/ meltfnum[19];
		    /*^apply.arg */
		    argtab[5].meltbp_cstring = "\n curbody=";
		    /*^apply.arg */
		    argtab[6].meltbp_aptr =
		      (melt_ptr_t *) & /*_.CURBODY__V61*/ meltfptr[47];
		    /*^apply.arg */
		    argtab[7].meltbp_cstring = "\n of discrim=";
		    /*^apply.arg */
		    argtab[8].meltbp_aptr =
		      (melt_ptr_t *) & /*_.DISCRIM__V64*/ meltfptr[63];
		    /*^apply.arg */
		    argtab[9].meltbp_cstring = "\n";
		    /*_.MELT_DEBUG_FUN__V65*/ meltfptr[64] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[1])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""),
				  argtab, "", (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V63*/ meltfptr[62] =
		    /*_.MELT_DEBUG_FUN__V65*/ meltfptr[64];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:2297:/ clear");
		/*clear *//*_#THE_MELTCALLCOUNT__L23*/ meltfnum[22] = 0;
		  /*^clear */
		/*clear *//*_.DISCRIM__V64*/ meltfptr[63] = 0;
		  /*^clear */
		/*clear *//*_.MELT_DEBUG_FUN__V65*/ meltfptr[64] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.IF___V63*/ meltfptr[62] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-outobj.melt:2297:/ quasiblock");


	    /*_.PROGN___V66*/ meltfptr[63] = /*_.IF___V63*/ meltfptr[62];;
	    /*^compute */
	    /*_.IFCPP___V62*/ meltfptr[61] = /*_.PROGN___V66*/ meltfptr[63];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2297:/ clear");
	      /*clear *//*_#MELT_NEED_DBG__L22*/ meltfnum[18] = 0;
	    /*^clear */
	      /*clear *//*_.IF___V63*/ meltfptr[62] = 0;
	    /*^clear */
	      /*clear *//*_.PROGN___V66*/ meltfptr[63] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V62*/ meltfptr[61] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2300:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.CURBODY__V61*/ meltfptr[47])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

    /*_#IS_NOT_A__L25*/ meltfnum[18] =
		  !melt_is_instance_of ((melt_ptr_t)
					( /*_.CURBODY__V61*/ meltfptr[47]),
					(melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */ meltfrout->tabval[11])));;
		/*^compute */
		/*_#IF___L24*/ meltfnum[22] =
		  /*_#IS_NOT_A__L25*/ meltfnum[18];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2300:/ clear");
	      /*clear *//*_#IS_NOT_A__L25*/ meltfnum[18] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_#IF___L24*/ meltfnum[22] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2300:/ cond");
	  /*cond */ if ( /*_#IF___L24*/ meltfnum[22])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-outobj.melt:2301:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
      /*_#MELT_NEED_DBG__L26*/ meltfnum[18] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-outobj.melt:2301:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[18])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	/*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-outobj.melt:2301:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[7];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-outobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 2301;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "outpucod_initialextensionroutine good bodix=";
			  /*^apply.arg */
			  argtab[4].meltbp_long =
			    /*_#BODIX__L21*/ meltfnum[19];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = "\n real curbody=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.CURBODY__V61*/ meltfptr[47];
			  /*_.MELT_DEBUG_FUN__V70*/ meltfptr[69] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[1])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V69*/ meltfptr[63] =
			  /*_.MELT_DEBUG_FUN__V70*/ meltfptr[69];;
			/*epilog */

			MELT_LOCATION ("warmelt-outobj.melt:2301:/ clear");
		  /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[26] =
			  0;
			/*^clear */
		  /*clear *//*_.MELT_DEBUG_FUN__V70*/ meltfptr[69] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

       /*_.IF___V69*/ meltfptr[63] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-outobj.melt:2301:/ quasiblock");


		  /*_.PROGN___V71*/ meltfptr[69] =
		    /*_.IF___V69*/ meltfptr[63];;
		  /*^compute */
		  /*_.IFCPP___V68*/ meltfptr[62] =
		    /*_.PROGN___V71*/ meltfptr[69];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-outobj.melt:2301:/ clear");
		/*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[18] = 0;
		  /*^clear */
		/*clear *//*_.IF___V69*/ meltfptr[63] = 0;
		  /*^clear */
		/*clear *//*_.PROGN___V71*/ meltfptr[69] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V68*/ meltfptr[62] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2302:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    (1), 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2303:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[5];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_cstring = " /*sideffecting ";
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
		  /*^apply.arg */
		  argtab[2].meltbp_cstring = " extension body #";
		  /*^apply.arg */
		  argtab[3].meltbp_long = /*_#BODIX__L21*/ meltfnum[19];
		  /*^apply.arg */
		  argtab[4].meltbp_cstring = " */";
		  /*_.ADD2OUT__V72*/ meltfptr[63] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!ADD2OUT */ meltfrout->tabval[5])),
				(melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(MELTBPARSTR_CSTRING MELTBPARSTR_PTR
				 MELTBPARSTR_CSTRING MELTBPARSTR_LONG
				 MELTBPARSTR_CSTRING ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2304:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    (1), 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2305:/ quasiblock");


		/*^cond */
		/*cond */ if (
			       /*ifisa */
			       melt_is_instance_of ((melt_ptr_t)
						    ( /*_.CURBODY__V61*/
						     meltfptr[47]),
						    (melt_ptr_t) (( /*!CLASS_OBJINSTR */ meltfrout->tabval[16])))
		  )		/*then */
		  {
		    /*^cond.then */
		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURBODY__V61*/ meltfptr[47])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 0, "OBI_LOC");
      /*_.CURLOC__V74*/ meltfptr[73] = slot;
		    };
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.CURLOC__V74*/ meltfptr[73] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:2307:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.CURLOC__V74*/ meltfptr[73])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION
			("warmelt-outobj.melt:2308:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[3];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
			/*^apply.arg */
			argtab[1].meltbp_long = 1;
			/*^apply.arg */
			argtab[2].meltbp_cstring = "curbody";
			/*_.OUTPUT_LOCATION__V76*/ meltfptr[75] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!OUTPUT_LOCATION */ meltfrout->
					tabval[17])),
				      (melt_ptr_t) ( /*_.CURLOC__V74*/
						    meltfptr[73]),
				      (MELTBPARSTR_PTR MELTBPARSTR_LONG
				       MELTBPARSTR_CSTRING ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IF___V75*/ meltfptr[74] =
			/*_.OUTPUT_LOCATION__V76*/ meltfptr[75];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:2307:/ clear");
		/*clear *//*_.OUTPUT_LOCATION__V76*/ meltfptr[75] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.IF___V75*/ meltfptr[74] = NULL;;
		  }
		;
		/*^compute */
		/*_.LET___V73*/ meltfptr[69] = /*_.IF___V75*/ meltfptr[74];;

		MELT_LOCATION ("warmelt-outobj.melt:2305:/ clear");
	      /*clear *//*_.CURLOC__V74*/ meltfptr[73] = 0;
		/*^clear */
	      /*clear *//*_.IF___V75*/ meltfptr[74] = 0;
		MELT_LOCATION ("warmelt-outobj.melt:2309:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = 1;
		  /*_.OUTPUT_C_CODE__V77*/ meltfptr[75] =
		    meltgc_send ((melt_ptr_t)
				 ( /*_.CURBODY__V61*/ meltfptr[47]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[12])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2300:/ quasiblock");


		/*_.PROGN___V78*/ meltfptr[73] =
		  /*_.OUTPUT_C_CODE__V77*/ meltfptr[75];;
		/*^compute */
		/*_.IF___V67*/ meltfptr[64] = /*_.PROGN___V78*/ meltfptr[73];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2300:/ clear");
	      /*clear *//*_.IFCPP___V68*/ meltfptr[62] = 0;
		/*^clear */
	      /*clear *//*_.ADD2OUT__V72*/ meltfptr[63] = 0;
		/*^clear */
	      /*clear *//*_.LET___V73*/ meltfptr[69] = 0;
		/*^clear */
	      /*clear *//*_.OUTPUT_C_CODE__V77*/ meltfptr[75] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V78*/ meltfptr[73] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_.IF___V67*/ meltfptr[64] = NULL;;
	    }
	  ;
	  if ( /*_#BODIX__L21*/ meltfnum[19] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:2294:/ clear");
	    /*clear *//*_.CURBODY__V61*/ meltfptr[47] = 0;
      /*^clear */
	    /*clear *//*_#BODIX__L21*/ meltfnum[19] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V62*/ meltfptr[61] = 0;
      /*^clear */
	    /*clear *//*_#IF___L24*/ meltfnum[22] = 0;
      /*^clear */
	    /*clear *//*_.IF___V67*/ meltfptr[64] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2312:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(1), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2313:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " /*-- ending run extension ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "  --*/";
      /*_.ADD2OUT__V79*/ meltfptr[74] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2314:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring =
	"\n goto labend_rout;\nlabend_rout: melt_topframe = meltfram__.mcfr_prev\
;\n  return (melt_ptr_t)";
      /*_.ADD2OUT__V80*/ meltfptr[62] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2318:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
      /*^ojbmsend.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^ojbmsend.arg */
      argtab[2].meltbp_long = 1;
      /*_.OUTPUT_C_CODE__V81*/ meltfptr[63] =
	meltgc_send ((melt_ptr_t) ( /*_.ORETVAL__V19*/ meltfptr[18]),
		     (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
				    tabval[12])),
		     (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2319:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "; /*return value of extension ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " */\n} /* end  ";
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.ONAME__V12*/ meltfptr[9];
      /*^apply.arg */
      argtab[4].meltbp_cstring = " */";
      /*_.ADD2OUT__V82*/ meltfptr[69] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2321:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2322:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2323:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2324:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2328:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2329:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[25];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "/* extension routine ";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " initial frame marking routine implem */\
\nstatic void \nmeltmarking_";
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.ONAME__V12*/ meltfptr[9];
      /*^apply.arg */
      argtab[4].meltbp_cstring =
	" (struct melt_callframe_st*fp, int marking) {\
\n     int ix=0;\n     meltrun_";
      /*^apply.arg */
      argtab[5].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[6].meltbp_cstring = "_initialext_frame_t* meltframptr_ \
\n      = ( meltrun_";
      /*^apply.arg */
      argtab[7].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[8].meltbp_cstring =
	"_initialext_frame_t*)fp;\n     melt_assertmsg(\"check extension ";
      /*^apply.arg */
      argtab[9].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[10].meltbp_cstring =
	" frame\", \n\t\t    meltframptr_->mcfr_nbvar == /*minihash*/ -";
      /*^apply.arg */
      argtab[11].meltbp_long = /*_#MINIHASH__L8*/ meltfnum[7];
      /*^apply.arg */
      argtab[12].meltbp_cstring =
	");\n     if (!marking && melt_is_forwarding) {\
\n       dbgprintf (\"";
      /*^apply.arg */
      argtab[13].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[14].meltbp_cstring = " extension forwarding ";
      /*^apply.arg */
      argtab[15].meltbp_long = /*_#NBVAL__L5*/ meltfnum[3];
      /*^apply.arg */
      argtab[16].meltbp_cstring =
	" variables in frame @%p\",\n\t\t  (void*)meltframptr_);\
\n       for (ix=0; \n\t    ix< ";
      /*^apply.arg */
      argtab[17].meltbp_long = /*_#NBVAL__L5*/ meltfnum[3];
      /*^apply.arg */
      argtab[18].meltbp_cstring =
	";\n\t    ix++) \n         MELT_FORWARDED (meltframptr_->mcfr_varptr\
[ix]);\n       return;\n     } /*end forwarking extension ";
      /*^apply.arg */
      argtab[19].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[20].meltbp_cstring = "*/\n     dbgprintf (\"";
      /*^apply.arg */
      argtab[21].meltbp_aptr =
	(melt_ptr_t *) & /*_.OMODNAM__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[22].meltbp_cstring = " extension marking frame@%p with ";
      /*^apply.arg */
      argtab[23].meltbp_long = /*_#NBVAL__L5*/ meltfnum[3];
      /*^apply.arg */
      argtab[24].meltbp_cstring = " values\",\n\t\t(void*)meltframptr_);\n";
      /*_.ADD2OUT__V83*/ meltfptr[75] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_LONG
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_LONG
		     MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2352:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2353:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*_.OUTPUCOD_MARKER__V84*/ meltfptr[73] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUCOD_MARKER */ meltfrout->tabval[18])),
		    (melt_ptr_t) ( /*_.PINI__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2354:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "} /* end extension meltmarking_";
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.ONAME__V12*/ meltfptr[9];
      /*^apply.arg */
      argtab[2].meltbp_cstring = " */";
      /*_.ADD2OUT__V85*/ meltfptr[84] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2357:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2361:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2362:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				(0), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2284:/ clear");
	   /*clear *//*_#IS_LIST__L16*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.BODTUP__V50*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_#NBBODY__L18*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V56*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V60*/ meltfptr[53] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V79*/ meltfptr[74] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V80*/ meltfptr[62] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_C_CODE__V81*/ meltfptr[63] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V82*/ meltfptr[69] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V83*/ meltfptr[75] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUCOD_MARKER__V84*/ meltfptr[73] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V85*/ meltfptr[84] = 0;

    MELT_LOCATION ("warmelt-outobj.melt:2180:/ clear");
	   /*clear *//*_.ONAME__V12*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.ODATUP__V13*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ORFILL__V14*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OPROLOG__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OMODNAM__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OBODY__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.ONBVAL__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#NBVAL__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#OBJ_HASH__L6*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#IRAW__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_#MINIHASH__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.ORETVAL__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_CURFRAME_DECLSTRUCT__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_CURFRAME_DECLSTRUCT_INIT__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_CURFRAME_CDAT_STRUCT__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V37*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V43*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_CURFRAME_CDAT_FILL__V44*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V46*/ meltfptr[45] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2175:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_INITIALEXTENSIONROUTINE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_46_warmelt_outobj_OUTPUCOD_INITIALEXTENSIONROUTINE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_outobj_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_47_warmelt_outobj_LAMBDA___5___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_47_warmelt_outobj_LAMBDA___5___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_47_warmelt_outobj_LAMBDA___5__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_47_warmelt_outobj_LAMBDA___5___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_47_warmelt_outobj_LAMBDA___5__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2252:/ getarg");
 /*_.ROU__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DSBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


    {
      MELT_LOCATION ("warmelt-outobj.melt:2253:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
				(0), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2254:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " meltrun_";
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~OMODNAM */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_cstring = "_initialext_frame_t";
      /*_.ADD2OUT__V4*/ meltfptr[3] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.DSBUF__V3*/ meltfptr[2]),
		    (MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2252:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.ADD2OUT__V4*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2252:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.ADD2OUT__V4*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_47_warmelt_outobj_LAMBDA___5___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_47_warmelt_outobj_LAMBDA___5__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_outobj_OUTPUCOD_GETARG (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_48_warmelt_outobj_OUTPUCOD_GETARG_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_48_warmelt_outobj_OUTPUCOD_GETARG_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 30
    melt_ptr_t mcfr_varptr[30];
#define MELTFRAM_NBVARNUM 18
    long mcfr_varnum[18];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_48_warmelt_outobj_OUTPUCOD_GETARG is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_48_warmelt_outobj_OUTPUCOD_GETARG_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 30; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_48_warmelt_outobj_OUTPUCOD_GETARG nbval 30*/
  meltfram__.mcfr_nbvar = 30 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_GETARG", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2373:/ getarg");
 /*_.GARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2374:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GARG__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJGETARG */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2374:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2374:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check garg"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2374) ? (2374) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2374:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2375:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GARG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBARG_OBLOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2376:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GARG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.NLOC__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2377:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GARG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBARG_BIND");
  /*_.OBIND__V10*/ meltfptr[9] = slot;
    };
    ;
 /*_#RKBIND__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.OBIND__V10*/ meltfptr[9])));;
    MELT_LOCATION ("warmelt-outobj.melt:2379:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBIND__V10*/ meltfptr[9]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "FBIND_TYPE");
  /*_.CTYBIND__V11*/ meltfptr[10] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2381:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBIND__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:2381:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2381:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obind"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2381) ? (2381) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2381:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2382:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "getarg";
      /*_.OUTPUT_LOCATION__V14*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.NLOC__V9*/ meltfptr[8]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2383:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					    tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:2383:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2383:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oloc"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2383) ? (2383) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[14] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2383:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2384:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CTYBIND__V11*/ meltfptr[10]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[4])));;
      MELT_LOCATION ("warmelt-outobj.melt:2384:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2384:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctybind"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2384) ? (2384) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[15] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2384:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2385:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L7*/ meltfnum[3] =
      (( /*_#RKBIND__L3*/ meltfnum[1]) == (0));;
    MELT_LOCATION ("warmelt-outobj.melt:2385:/ cond");
    /*cond */ if ( /*_#I__L7*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:2387:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#__L8*/ meltfnum[7] =
	      (( /*_.CTYBIND__V11*/ meltfptr[10]) ==
	       (( /*!CTYPE_VALUE */ meltfrout->tabval[5])));;
	    MELT_LOCATION ("warmelt-outobj.melt:2387:/ cond");
	    /*cond */ if ( /*_#__L8*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:2387:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check ctybind first"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(2387) ? (2387) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V19*/ meltfptr[17] = /*_.IFELSE___V20*/ meltfptr[19];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2387:/ clear");
	       /*clear *//*_#__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V19*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2388:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V21*/ meltfptr[19] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[6])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2389:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 (" = (melt_ptr_t) meltfirstargp_;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2390:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2386:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2385:/ clear");
	     /*clear *//*_.IFCPP___V19*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V21*/ meltfptr[19] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2392:/ quasiblock");


	  MELT_LOCATION ("warmelt-outobj.melt:2394:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CTYBIND__V11*/ meltfptr[10]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 4, "CTYPE_PARCHAR");
    /*_.PARC__V22*/ meltfptr[17] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2395:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CTYBIND__V11*/ meltfptr[10]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 6, "CTYPE_ARGFIELD");
    /*_.ARGF__V23*/ meltfptr[19] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2397:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L9*/ meltfnum[7] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.PARC__V22*/ meltfptr[17]))
	     == MELTOBMAG_STRING);;
	  /*^compute */
   /*_#NOT__L10*/ meltfnum[9] =
	    (!( /*_#IS_STRING__L9*/ meltfnum[7]));;
	  MELT_LOCATION ("warmelt-outobj.melt:2397:/ cond");
	  /*cond */ if ( /*_#NOT__L10*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:2399:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.CTYBIND__V11*/ meltfptr[10]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V24*/ meltfptr[23] = slot;
		};
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2398:/ locexp");
		  melt_error_str ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				  ("impossible argument ctype"),
				  (melt_ptr_t) ( /*_.NAMED_NAME__V24*/
						meltfptr[23]));
		}
		;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2397:/ clear");
	       /*clear *//*_.NAMED_NAME__V24*/ meltfptr[23] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2400:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("if (meltxargdescr_["));
	  }
	  ;
   /*_#I__L11*/ meltfnum[10] =
	    (( /*_#RKBIND__L3*/ meltfnum[1]) - (1));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2401:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#I__L11*/ meltfnum[10]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2402:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("] != "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2403:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PARC__V22*/
						   meltfptr[17])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2404:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 (") goto lab_endgetargs;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2405:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2406:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L12*/ meltfnum[11] =
	    (( /*_.CTYBIND__V11*/ meltfptr[10]) ==
	     (( /*!CTYPE_VALUE */ meltfrout->tabval[5])));;
	  MELT_LOCATION ("warmelt-outobj.melt:2406:/ cond");
	  /*cond */ if ( /*_#__L12*/ meltfnum[11])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:2408:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*_.OUTPUT_C_CODE__V25*/ meltfptr[23] =
		    meltgc_send ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[6])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2409:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (" = (meltxargtab_["));
		}
		;
     /*_#I__L13*/ meltfnum[12] =
		  (( /*_#RKBIND__L3*/ meltfnum[1]) - (1));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2410:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					 ( /*_#I__L13*/ meltfnum[12]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2411:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("].meltbp_aptr) ? (*(meltxargtab_["));
		}
		;
     /*_#I__L14*/ meltfnum[13] =
		  (( /*_#RKBIND__L3*/ meltfnum[1]) - (1));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2412:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					 ( /*_#I__L14*/ meltfnum[13]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2413:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("].meltbp_aptr)) : NULL;"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2414:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    ( /*_#DEPTH__L1*/ meltfnum[0]),
					    0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2415:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("gcc_assert(melt_discr((melt_ptr_t)("));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2416:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*_.OUTPUT_C_CODE__V26*/ meltfptr[25] =
		    meltgc_send ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[6])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2417:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (")) != NULL);"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2418:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    ( /*_#DEPTH__L1*/ meltfnum[0]),
					    0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2407:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2406:/ clear");
	       /*clear *//*_.OUTPUT_C_CODE__V25*/ meltfptr[23] = 0;
		/*^clear */
	       /*clear *//*_#I__L13*/ meltfnum[12] = 0;
		/*^clear */
	       /*clear *//*_#I__L14*/ meltfnum[13] = 0;
		/*^clear */
	       /*clear *//*_.OUTPUT_C_CODE__V26*/ meltfptr[25] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:2421:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*_.OUTPUT_C_CODE__V27*/ meltfptr[23] =
		    meltgc_send ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[6])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2422:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (" = meltxargtab_["));
		}
		;
     /*_#I__L15*/ meltfnum[12] =
		  (( /*_#RKBIND__L3*/ meltfnum[1]) - (1));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2423:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					 ( /*_#I__L15*/ meltfnum[12]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2424:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("]."));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2425:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       melt_string_str ((melt_ptr_t)
							( /*_.ARGF__V23*/
							 meltfptr[19])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2426:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (";"));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2420:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2406:/ clear");
	       /*clear *//*_.OUTPUT_C_CODE__V27*/ meltfptr[23] = 0;
		/*^clear */
	       /*clear *//*_#I__L15*/ meltfnum[12] = 0;
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2429:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:2392:/ clear");
	     /*clear *//*_.PARC__V22*/ meltfptr[17] = 0;
	  /*^clear */
	     /*clear *//*_.ARGF__V23*/ meltfptr[19] = 0;
	  /*^clear */
	     /*clear *//*_#IS_STRING__L9*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#NOT__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_#__L12*/ meltfnum[11] = 0;
	  /*epilog */
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2431:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L16*/ meltfnum[13] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2432:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[7])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[8])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[7])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V29*/ meltfptr[23] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V29*/ meltfptr[23] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L17*/ meltfnum[12] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V29*/ meltfptr[23])));;
      /*^compute */
   /*_#I__L18*/ meltfnum[7] =
	(( /*_#STRBUF_USEDLENGTH__L16*/ meltfnum[13]) <
	 ( /*_#GET_INT__L17*/ meltfnum[12]));;
      MELT_LOCATION ("warmelt-outobj.melt:2431:/ cond");
      /*cond */ if ( /*_#I__L18*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V30*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2431:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2431) ? (2431) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V30*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[25] = /*_.IFELSE___V30*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2431:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L16*/ meltfnum[13] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V29*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L17*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_#I__L18*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V30*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V28*/ meltfptr[25];;

    MELT_LOCATION ("warmelt-outobj.melt:2375:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OBIND__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#RKBIND__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.CTYBIND__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[25] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2373:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2373:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_GETARG", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_48_warmelt_outobj_OUTPUCOD_GETARG_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_48_warmelt_outobj_OUTPUCOD_GETARG */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 33
    melt_ptr_t mcfr_varptr[33];
#define MELTFRAM_NBVARNUM 20
    long mcfr_varnum[20];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 33; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST nbval 33*/
  meltfram__.mcfr_nbvar = 33 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_GETARGREST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2438:/ getarg");
 /*_.GARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2439:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GARG__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJGETARGREST */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2439:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2439:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check garg"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2439) ? (2439) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2439:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2440:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GARG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBARG_OBLOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2441:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GARG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.NLOC__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2442:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GARG__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBARG_BIND");
  /*_.OBIND__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2443:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GARG__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_OBJGETARGREST */ meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GARG__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 3, "OBARG_REST");
   /*_.OREST__V11*/ meltfptr[10] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.OREST__V11*/ meltfptr[10] = NULL;;
      }
    ;
    /*^compute */
 /*_#RKBIND__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.OBIND__V10*/ meltfptr[9])));;
    MELT_LOCATION ("warmelt-outobj.melt:2445:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBIND__V10*/ meltfptr[9]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "FBIND_TYPE");
  /*_.CTYBIND__V12*/ meltfptr[11] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2446:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.OVARIADICINDEX__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!VARIADIC_INDEX_IDSTR */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OREST__V11*/ meltfptr[10]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2447:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      /*_.OVARIADICLENGTH__V14*/ meltfptr[13] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!VARIADIC_LENGTH_IDSTR */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.OREST__V11*/ meltfptr[10]), (""),
		    (union meltparam_un *) 0, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2449:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBIND__V10*/ meltfptr[9]),
			     (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */
					    meltfrout->tabval[3])));;
      MELT_LOCATION ("warmelt-outobj.melt:2449:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2449:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obind"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2449) ? (2449) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[14] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2449:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2450:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "getarg";
      /*_.OUTPUT_LOCATION__V17*/ meltfptr[15] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.NLOC__V9*/ meltfptr[8]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2451:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					    tabval[5])));;
      MELT_LOCATION ("warmelt-outobj.melt:2451:/ cond");
      /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2451:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oloc"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2451) ? (2451) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[17] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2451:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2452:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L6*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CTYBIND__V12*/ meltfptr[11]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[6])));;
      MELT_LOCATION ("warmelt-outobj.melt:2452:/ cond");
      /*cond */ if ( /*_#IS_A__L6*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V21*/ meltfptr[20] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2452:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ctybind"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2452) ? (2452) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[18] = /*_.IFELSE___V21*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2452:/ clear");
	     /*clear *//*_#IS_A__L6*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2453:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L7*/ meltfnum[3] =
      (( /*_#RKBIND__L3*/ meltfnum[1]) == (0));;
    MELT_LOCATION ("warmelt-outobj.melt:2453:/ cond");
    /*cond */ if ( /*_#I__L7*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:2455:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#__L8*/ meltfnum[7] =
	      (( /*_.CTYBIND__V12*/ meltfptr[11]) ==
	       (( /*!CTYPE_VALUE */ meltfrout->tabval[7])));;
	    MELT_LOCATION ("warmelt-outobj.melt:2455:/ cond");
	    /*cond */ if ( /*_#__L8*/ meltfnum[7])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V23*/ meltfptr[22] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:2455:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check ctybind first"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(2455) ? (2455) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		 /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V22*/ meltfptr[20] = /*_.IFELSE___V23*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2455:/ clear");
	       /*clear *//*_#__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V22*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2456:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*_.OUTPUT_C_CODE__V24*/ meltfptr[22] =
	      meltgc_send ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[8])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2457:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 (" = (melt_ptr_t) meltfirstargp_;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2458:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2454:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2453:/ clear");
	     /*clear *//*_.IFCPP___V22*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_C_CODE__V24*/ meltfptr[22] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2460:/ quasiblock");


	  MELT_LOCATION ("warmelt-outobj.melt:2462:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CTYBIND__V12*/ meltfptr[11]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 4, "CTYPE_PARCHAR");
    /*_.PARC__V25*/ meltfptr[20] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2463:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.CTYBIND__V12*/ meltfptr[11]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 6, "CTYPE_ARGFIELD");
    /*_.ARGF__V26*/ meltfptr[22] = slot;
	  };
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2465:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_STRING__L9*/ meltfnum[7] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.PARC__V25*/ meltfptr[20]))
	     == MELTOBMAG_STRING);;
	  /*^compute */
   /*_#NOT__L10*/ meltfnum[9] =
	    (!( /*_#IS_STRING__L9*/ meltfnum[7]));;
	  MELT_LOCATION ("warmelt-outobj.melt:2465:/ cond");
	  /*cond */ if ( /*_#NOT__L10*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:2467:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.CTYBIND__V12*/ meltfptr[11]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V27*/ meltfptr[26] = slot;
		};
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2466:/ locexp");
		  melt_error_str ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				  ("impossible argument ctype"),
				  (melt_ptr_t) ( /*_.NAMED_NAME__V27*/
						meltfptr[26]));
		}
		;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2465:/ clear");
	       /*clear *//*_.NAMED_NAME__V27*/ meltfptr[26] = 0;
	      }
	      ;
	    }			/*noelse */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2468:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("if (meltxargdescr_["));
	  }
	  ;
   /*_#I__L11*/ meltfnum[10] =
	    (( /*_#RKBIND__L3*/ meltfnum[1]) - (1));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2469:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#I__L11*/ meltfnum[10]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2470:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("] != "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2471:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.PARC__V25*/
						   meltfptr[20])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2472:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 (") { /*getargrest*/"));
	  }
	  ;
   /*_#I__L12*/ meltfnum[11] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2473:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L12*/ meltfnum[11]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2474:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.OVARIADICINDEX__V13*/
						   meltfptr[12])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2475:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (" = "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2476:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.OVARIADICLENGTH__V14*/
						   meltfptr[13])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2477:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (";"));
	  }
	  ;
   /*_#I__L13*/ meltfnum[12] =
	    ((1) + ( /*_#DEPTH__L1*/ meltfnum[0]));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2478:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#I__L13*/ meltfnum[12]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2479:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("goto lab_endgetargs;"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2480:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2481:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("}"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2482:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2483:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.OVARIADICINDEX__V13*/
						   meltfptr[12])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2484:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (" = "));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2485:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#RKBIND__L3*/ meltfnum[1]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2486:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), (";"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2487:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2488:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L14*/ meltfnum[13] =
	    (( /*_.CTYBIND__V12*/ meltfptr[11]) ==
	     (( /*!CTYPE_VALUE */ meltfrout->tabval[7])));;
	  MELT_LOCATION ("warmelt-outobj.melt:2488:/ cond");
	  /*cond */ if ( /*_#__L14*/ meltfnum[13])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:2490:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*_.OUTPUT_C_CODE__V28*/ meltfptr[26] =
		    meltgc_send ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[8])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2491:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (" = (meltxargtab_["));
		}
		;
     /*_#I__L15*/ meltfnum[14] =
		  (( /*_#RKBIND__L3*/ meltfnum[1]) - (1));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2492:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					 ( /*_#I__L15*/ meltfnum[14]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2493:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("].meltbp_aptr) ? (*(meltxargtab_["));
		}
		;
     /*_#I__L16*/ meltfnum[15] =
		  (( /*_#RKBIND__L3*/ meltfnum[1]) - (1));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2494:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					 ( /*_#I__L16*/ meltfnum[15]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2495:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("].meltbp_aptr)) : NULL;"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2496:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    ( /*_#DEPTH__L1*/ meltfnum[0]),
					    0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2497:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("gcc_assert(melt_discr((melt_ptr_t)("));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2498:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*_.OUTPUT_C_CODE__V29*/ meltfptr[28] =
		    meltgc_send ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[8])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2499:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (")) != NULL);"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2500:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    ( /*_#DEPTH__L1*/ meltfnum[0]),
					    0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2489:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2488:/ clear");
	       /*clear *//*_.OUTPUT_C_CODE__V28*/ meltfptr[26] = 0;
		/*^clear */
	       /*clear *//*_#I__L15*/ meltfnum[14] = 0;
		/*^clear */
	       /*clear *//*_#I__L16*/ meltfnum[15] = 0;
		/*^clear */
	       /*clear *//*_.OUTPUT_C_CODE__V29*/ meltfptr[28] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:2503:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^msend */
		/*msend */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^ojbmsend.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
		  /*^ojbmsend.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
		  /*^ojbmsend.arg */
		  argtab[2].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
		  /*_.OUTPUT_C_CODE__V30*/ meltfptr[26] =
		    meltgc_send ((melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
				 (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						meltfrout->tabval[8])),
				 (MELTBPARSTR_PTR MELTBPARSTR_PTR
				  MELTBPARSTR_LONG ""), argtab, "",
				 (union meltparam_un *) 0);
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2504:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (" = meltxargtab_["));
		}
		;
     /*_#I__L17*/ meltfnum[14] =
		  (( /*_#RKBIND__L3*/ meltfnum[1]) - (1));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2505:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					 ( /*_#I__L17*/ meltfnum[14]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2506:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("]."));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2507:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       melt_string_str ((melt_ptr_t)
							( /*_.ARGF__V26*/
							 meltfptr[22])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2508:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       (";"));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2502:/ quasiblock");


		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2488:/ clear");
	       /*clear *//*_.OUTPUT_C_CODE__V30*/ meltfptr[26] = 0;
		/*^clear */
	       /*clear *//*_#I__L17*/ meltfnum[14] = 0;
	      }
	      ;
	    }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2511:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTH__L1*/ meltfnum[0]), 0);
	  }
	  ;

	  MELT_LOCATION ("warmelt-outobj.melt:2460:/ clear");
	     /*clear *//*_.PARC__V25*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.ARGF__V26*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_#IS_STRING__L9*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_#NOT__L10*/ meltfnum[9] = 0;
	  /*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_#I__L12*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_#I__L13*/ meltfnum[12] = 0;
	  /*^clear */
	     /*clear *//*_#__L14*/ meltfnum[13] = 0;
	  /*epilog */
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2513:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L18*/ meltfnum[15] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2514:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[9])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[10])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[9])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V32*/ meltfptr[26] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V32*/ meltfptr[26] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L19*/ meltfnum[14] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V32*/ meltfptr[26])));;
      /*^compute */
   /*_#I__L20*/ meltfnum[7] =
	(( /*_#STRBUF_USEDLENGTH__L18*/ meltfnum[15]) <
	 ( /*_#GET_INT__L19*/ meltfnum[14]));;
      MELT_LOCATION ("warmelt-outobj.melt:2513:/ cond");
      /*cond */ if ( /*_#I__L20*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V33*/ meltfptr[20] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2513:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2513) ? (2513) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V33*/ meltfptr[20] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[28] = /*_.IFELSE___V33*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2513:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L18*/ meltfnum[15] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V32*/ meltfptr[26] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L19*/ meltfnum[14] = 0;
      /*^clear */
	     /*clear *//*_#I__L20*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V33*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V31*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-outobj.melt:2440:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OBIND__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OREST__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#RKBIND__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.CTYBIND__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OVARIADICINDEX__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.OVARIADICLENGTH__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V17*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[28] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2438:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2438:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_GETARGREST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_49_warmelt_outobj_OUTPUCOD_GETARGREST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 12
    melt_ptr_t mcfr_varptr[12];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 12; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV nbval 12*/
  meltfram__.mcfr_nbvar = 12 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJLOCV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2521:/ getarg");
 /*_.LOCV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2522:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.LOCV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2522:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2522:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check locv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2522) ? (2522) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2522:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2523:/ quasiblock");


    MELT_LOCATION ("warmelt-outobj.melt:2524:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.LOCV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBV_TYPE");
  /*_.LTYP__V7*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2525:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.LOCV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBL_OFF");
  /*_.LOFF__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2526:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.LOCV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBL_CNAME");
  /*_.LCNAM__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2529:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L3*/ meltfnum[1] =
      (( /*_.LTYP__V7*/ meltfptr[5]) ==
       (( /*!CTYPE_VALUE */ meltfrout->tabval[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:2529:/ cond");
    /*cond */ if ( /*_#__L3*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2530:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("/*_."));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2531:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.LCNAM__V9*/
						   meltfptr[8])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2532:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("*/ meltfptr["));
	  }
	  ;
   /*_#GET_INT__L4*/ meltfnum[3] =
	    (melt_get_int ((melt_ptr_t) ( /*_.LOFF__V8*/ meltfptr[7])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2533:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				   ( /*_#GET_INT__L4*/ meltfnum[3]));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2534:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("]"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2529:/ quasiblock");


	  /*epilog */

	  /*^clear */
	     /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2535:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L5*/ meltfnum[3] =
	    (( /*_.LTYP__V7*/ meltfptr[5]) ==
	     (( /*!CTYPE_LONG */ meltfrout->tabval[2])));;
	  MELT_LOCATION ("warmelt-outobj.melt:2535:/ cond");
	  /*cond */ if ( /*_#__L5*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:2536:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("/*_#"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2537:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       melt_string_str ((melt_ptr_t)
							( /*_.LCNAM__V9*/
							 meltfptr[8])));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2538:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("*/ meltfnum["));
		}
		;
     /*_#GET_INT__L6*/ meltfnum[5] =
		  (melt_get_int
		   ((melt_ptr_t) ( /*_.LOFF__V8*/ meltfptr[7])));;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2539:/ locexp");
		  meltgc_add_strbuf_dec ((melt_ptr_t)
					 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					 ( /*_#GET_INT__L6*/ meltfnum[5]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2540:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("]"));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2535:/ quasiblock");


		/*epilog */

		/*^clear */
	       /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:2542:/ locexp");
		  /*add2sbuf_strconst */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       ("/*_?*/ meltfram__."));
		}
		;

		{
		  MELT_LOCATION ("warmelt-outobj.melt:2543:/ locexp");
		  /*add2sbuf_string */
		    meltgc_add_strbuf ((melt_ptr_t)
				       ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				       melt_string_str ((melt_ptr_t)
							( /*_.LCNAM__V9*/
							 meltfptr[8])));
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2541:/ quasiblock");


		/*epilog */
	      }
	      ;
	    }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2529:/ clear");
	     /*clear *//*_#__L5*/ meltfnum[3] = 0;
	}
	;
      }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2523:/ clear");
	   /*clear *//*_.LTYP__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LOFF__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LCNAM__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#__L3*/ meltfnum[1] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2545:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2546:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[3])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[3])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V11*/ meltfptr[7] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V11*/ meltfptr[7] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L8*/ meltfnum[3] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V11*/ meltfptr[7])));;
      /*^compute */
   /*_#I__L9*/ meltfnum[1] =
	(( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5]) <
	 ( /*_#GET_INT__L8*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2545:/ cond");
      /*cond */ if ( /*_#I__L9*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2545:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2545) ? (2545) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V12*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2545:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V11*/ meltfptr[7] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L8*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#I__L9*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2521:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V10*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2521:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJLOCV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_50_warmelt_outobj_OUTPUCOD_OBJLOCV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJCLOCCV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2552:/ getarg");
 /*_.OCCV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2553:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OCCV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCLOCCV */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2553:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2553:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check occv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2553) ? (2553) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2553:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2554:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCCV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBC_OFF");
  /*_.OOFF__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2555:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCCV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBC_NAME");
  /*_.ONAM__V9*/ meltfptr[8] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2556:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      MELT_LOCATION ("warmelt-outobj.melt:2557:/ getslot");
      {
	melt_ptr_t slot = NULL, obj = NULL;
	obj = (melt_ptr_t) ( /*_.OCCV__V2*/ meltfptr[1]) /*=obj*/ ;
	melt_object_get_field (slot, obj, 0, "OBV_TYPE");
    /*_.OBV_TYPE__V11*/ meltfptr[10] = slot;
      };
      ;
   /*_#__L3*/ meltfnum[1] =
	(( /*_.OBV_TYPE__V11*/ meltfptr[10]) ==
	 (( /*!CTYPE_VALUE */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:2556:/ cond");
      /*cond */ if ( /*_#__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2556:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check valueness of closed occurrence"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2556) ? (2556) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[9] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2556:/ clear");
	     /*clear *//*_.OBV_TYPE__V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_#__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2558:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("(/*~"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2559:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAM__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2560:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/ meltfclos->tabval["));
    }
    ;
 /*_#GET_INT__L4*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.OOFF__V8*/ meltfptr[7])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2561:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#GET_INT__L4*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2562:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("])"));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2563:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[4] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2564:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[2])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[2])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V14*/ meltfptr[11] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V14*/ meltfptr[11] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L6*/ meltfnum[5] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V14*/ meltfptr[11])));;
      /*^compute */
   /*_#I__L7*/ meltfnum[6] =
	(( /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[4]) <
	 ( /*_#GET_INT__L6*/ meltfnum[5]));;
      MELT_LOCATION ("warmelt-outobj.melt:2563:/ cond");
      /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2563:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2563) ? (2563) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[10] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2563:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V14*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V13*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-outobj.melt:2554:/ clear");
	   /*clear *//*_.OOFF__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ONAM__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2552:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2552:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJCLOCCV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_51_warmelt_outobj_OUTPUCOD_OBJCLOCCV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJCONSTV", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2571:/ getarg");
 /*_.OCNSTV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2572:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OCNSTV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCONSTV */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2572:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2572:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ocnstv"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2572) ? (2572) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2572:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2573:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCNSTV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBC_OFF");
  /*_.OOFF__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2574:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OCNSTV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBC_NAME");
  /*_.ONAM__V9*/ meltfptr[8] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2575:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      MELT_LOCATION ("warmelt-outobj.melt:2576:/ getslot");
      {
	melt_ptr_t slot = NULL, obj = NULL;
	obj = (melt_ptr_t) ( /*_.OCNSTV__V2*/ meltfptr[1]) /*=obj*/ ;
	melt_object_get_field (slot, obj, 0, "OBV_TYPE");
    /*_.OBV_TYPE__V11*/ meltfptr[10] = slot;
      };
      ;
   /*_#__L3*/ meltfnum[1] =
	(( /*_.OBV_TYPE__V11*/ meltfptr[10]) ==
	 (( /*!CTYPE_VALUE */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-outobj.melt:2575:/ cond");
      /*cond */ if ( /*_#__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2575:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check valueness of const occurrence"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2575) ? (2575) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[9] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2575:/ clear");
	     /*clear *//*_.OBV_TYPE__V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_#__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2577:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("(/*!"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2578:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAM__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2580:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/ meltfrout->tabval["));
    }
    ;
 /*_#GET_INT__L4*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.OOFF__V8*/ meltfptr[7])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2581:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			     ( /*_#GET_INT__L4*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2582:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("])"));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2583:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[4] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2584:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[2])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[3])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[2])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V14*/ meltfptr[11] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V14*/ meltfptr[11] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L6*/ meltfnum[5] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V14*/ meltfptr[11])));;
      /*^compute */
   /*_#I__L7*/ meltfnum[6] =
	(( /*_#STRBUF_USEDLENGTH__L5*/ meltfnum[4]) <
	 ( /*_#GET_INT__L6*/ meltfnum[5]));;
      MELT_LOCATION ("warmelt-outobj.melt:2583:/ cond");
      /*cond */ if ( /*_#I__L7*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2583:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2583) ? (2583) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[10] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2583:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V14*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V13*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-outobj.melt:2573:/ clear");
	   /*clear *//*_.OOFF__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ONAM__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#GET_INT__L4*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2571:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2571:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJCONSTV", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_52_warmelt_outobj_OUTPUCOD_OBJCONSTV */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 31
    melt_ptr_t mcfr_varptr[31];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 31; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST nbval 31*/
  meltfram__.mcfr_nbvar = 31 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUT_CODE_INSTRUCTIONS_LIST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2590:/ getarg");
 /*_.LIS__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.BOXEDDEPTH__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.BOXEDDEPTH__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2591:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST_OR_NULL__L1*/ meltfnum[0] =
	(( /*_.LIS__V2*/ meltfptr[1]) == NULL
	 ||
	 (melt_unsafe_magic_discr ((melt_ptr_t) ( /*_.LIS__V2*/ meltfptr[1]))
	  == MELTOBMAG_LIST));;
      MELT_LOCATION ("warmelt-outobj.melt:2591:/ cond");
      /*cond */ if ( /*_#IS_LIST_OR_NULL__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2591:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check lis"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2591) ? (2591) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2591:/ clear");
	     /*clear *//*_#IS_LIST_OR_NULL__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2592:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_INTEGERBOX__L2*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.BOXEDDEPTH__V5*/ meltfptr[4]))
	 == MELTOBMAG_INT);;
      MELT_LOCATION ("warmelt-outobj.melt:2592:/ cond");
      /*cond */ if ( /*_#IS_INTEGERBOX__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2592:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check boxeddepth"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2592) ? (2592) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2592:/ clear");
	     /*clear *//*_#IS_INTEGERBOX__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2593:/ quasiblock");


 /*_#DEPTH__L3*/ meltfnum[0] =
      (melt_get_int ((melt_ptr_t) ( /*_.BOXEDDEPTH__V5*/ meltfptr[4])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2594:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L3*/ meltfnum[0]), 0);
    }
    ;
    /*citerblock FOREACH_IN_LIST */
    {
      /* start foreach_in_list meltcit1__EACHLIST */
      for ( /*_.PAIR__V10*/ meltfptr[8] =
	   melt_list_first ((melt_ptr_t) /*_.LIS__V2*/ meltfptr[1]);
	   melt_magic_discr ((melt_ptr_t) /*_.PAIR__V10*/ meltfptr[8]) ==
	   MELTOBMAG_PAIR;
	   /*_.PAIR__V10*/ meltfptr[8] =
	   melt_pair_tail ((melt_ptr_t) /*_.PAIR__V10*/ meltfptr[8]))
	{
	  /*_.CUR__V11*/ meltfptr[10] =
	    melt_pair_head ((melt_ptr_t) /*_.PAIR__V10*/ meltfptr[8]);


	  MELT_LOCATION ("warmelt-outobj.melt:2599:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
  /*_#IS_A__L4*/ meltfnum[3] =
	    melt_is_instance_of ((melt_ptr_t) ( /*_.CUR__V11*/ meltfptr[10]),
				 (melt_ptr_t) (( /*!CLASS_OBJPLAINBLOCK */
						meltfrout->tabval[0])));;
	  MELT_LOCATION ("warmelt-outobj.melt:2599:/ cond");
	  /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-outobj.melt:2600:/ locexp");
		  meltgc_strbuf_add_indent ((melt_ptr_t)
					    ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					    ( /*_#DEPTH__L3*/ meltfnum[0]),
					    0);
		}
		;
		MELT_LOCATION ("warmelt-outobj.melt:2601:/ quasiblock");


		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.CUR__V11*/ meltfptr[10]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "OBI_LOC");
     /*_.BLOC__V14*/ meltfptr[13] = slot;
		};
		;
		MELT_LOCATION ("warmelt-outobj.melt:2602:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.CUR__V11*/ meltfptr[10]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "OBLO_BODYL");
     /*_.BODYL__V15*/ meltfptr[14] = slot;
		};
		;
		MELT_LOCATION ("warmelt-outobj.melt:2603:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj = (melt_ptr_t) ( /*_.CUR__V11*/ meltfptr[10]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 2, "OBLO_EPIL");
     /*_.EPIL__V16*/ meltfptr[15] = slot;
		};
		;
		MELT_LOCATION ("warmelt-outobj.melt:2605:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.BLOC__V14*/ meltfptr[13])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[3];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
			/*^apply.arg */
			argtab[1].meltbp_long = /*_#DEPTH__L3*/ meltfnum[0];
			/*^apply.arg */
			argtab[2].meltbp_cstring = "quasiblock";
			/*_.OUTPUT_LOCATION__V18*/ meltfptr[17] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!OUTPUT_LOCATION */ meltfrout->
					tabval[1])),
				      (melt_ptr_t) ( /*_.BLOC__V14*/
						    meltfptr[13]),
				      (MELTBPARSTR_PTR MELTBPARSTR_LONG
				       MELTBPARSTR_CSTRING ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IF___V17*/ meltfptr[16] =
			/*_.OUTPUT_LOCATION__V18*/ meltfptr[17];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:2605:/ clear");
		/*clear *//*_.OUTPUT_LOCATION__V18*/ meltfptr[17] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.IF___V17*/ meltfptr[16] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:2606:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.BODYL__V15*/ meltfptr[14])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[3];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
			/*^apply.arg */
			argtab[1].meltbp_aptr =
			  (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
			/*^apply.arg */
			argtab[2].meltbp_aptr =
			  (melt_ptr_t *) & /*_.BOXEDDEPTH__V5*/ meltfptr[4];
			/*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V20*/ meltfptr[19]
			  =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!OUTPUT_CODE_INSTRUCTIONS_LIST */
					meltfrout->tabval[2])),
				      (melt_ptr_t) ( /*_.BODYL__V15*/
						    meltfptr[14]),
				      (MELTBPARSTR_PTR MELTBPARSTR_PTR
				       MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IF___V19*/ meltfptr[17] =
			/*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V20*/
			meltfptr[19];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:2606:/ clear");
		/*clear *//*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V20*/
			meltfptr[19] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.IF___V19*/ meltfptr[17] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:2607:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.EPIL__V16*/ meltfptr[15])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[3];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
			/*^apply.arg */
			argtab[1].meltbp_aptr =
			  (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
			/*^apply.arg */
			argtab[2].meltbp_aptr =
			  (melt_ptr_t *) & /*_.BOXEDDEPTH__V5*/ meltfptr[4];
			/*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V22*/ meltfptr[21]
			  =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!OUTPUT_CODE_INSTRUCTIONS_LIST */
					meltfrout->tabval[2])),
				      (melt_ptr_t) ( /*_.EPIL__V16*/
						    meltfptr[15]),
				      (MELTBPARSTR_PTR MELTBPARSTR_PTR
				       MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IF___V21*/ meltfptr[19] =
			/*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V22*/
			meltfptr[21];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:2607:/ clear");
		/*clear *//*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V22*/
			meltfptr[21] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.IF___V21*/ meltfptr[19] = NULL;;
		  }
		;
		/*^compute */
		/*_.LET___V13*/ meltfptr[12] = /*_.IF___V21*/ meltfptr[19];;

		MELT_LOCATION ("warmelt-outobj.melt:2601:/ clear");
	      /*clear *//*_.BLOC__V14*/ meltfptr[13] = 0;
		/*^clear */
	      /*clear *//*_.BODYL__V15*/ meltfptr[14] = 0;
		/*^clear */
	      /*clear *//*_.EPIL__V16*/ meltfptr[15] = 0;
		/*^clear */
	      /*clear *//*_.IF___V17*/ meltfptr[16] = 0;
		/*^clear */
	      /*clear *//*_.IF___V19*/ meltfptr[17] = 0;
		/*^clear */
	      /*clear *//*_.IF___V21*/ meltfptr[19] = 0;
		MELT_LOCATION ("warmelt-outobj.melt:2599:/ quasiblock");


		/*_.PROGN___V23*/ meltfptr[21] =
		  /*_.LET___V13*/ meltfptr[12];;
		/*^compute */
		/*_.IFELSE___V12*/ meltfptr[11] =
		  /*_.PROGN___V23*/ meltfptr[21];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2599:/ clear");
	      /*clear *//*_.LET___V13*/ meltfptr[12] = 0;
		/*^clear */
	      /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-outobj.melt:2610:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.CUR__V11*/ meltfptr[10])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

      /*_#IS_NOT_A__L6*/ meltfnum[5] =
			!melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CUR__V11*/ meltfptr[10]),
					      (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */ meltfrout->tabval[3])));;
		      /*^compute */
		      /*_#IF___L5*/ meltfnum[4] =
			/*_#IS_NOT_A__L6*/ meltfnum[5];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-outobj.melt:2610:/ clear");
		/*clear *//*_#IS_NOT_A__L6*/ meltfnum[5] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_#IF___L5*/ meltfnum[4] = 0;;
		  }
		;
		MELT_LOCATION ("warmelt-outobj.melt:2610:/ cond");
		/*cond */ if ( /*_#IF___L5*/ meltfnum[4])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {


		      {
			MELT_LOCATION ("warmelt-outobj.melt:2611:/ locexp");
			meltgc_strbuf_add_indent ((melt_ptr_t)
						  ( /*_.IMPLBUF__V4*/
						   meltfptr[3]),
						  ( /*_#DEPTH__L3*/
						   meltfnum[0]), 0);
		      }
		      ;
		      MELT_LOCATION
			("warmelt-outobj.melt:2612:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^msend */
		      /*msend */
		      {
			union meltparam_un argtab[3];
			memset (&argtab, 0, sizeof (argtab));
			/*^ojbmsend.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
			/*^ojbmsend.arg */
			argtab[1].meltbp_aptr =
			  (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
			/*^ojbmsend.arg */
			argtab[2].meltbp_long = /*_#DEPTH__L3*/ meltfnum[0];
			/*_.OUTPUT_C_CODE__V25*/ meltfptr[14] =
			  meltgc_send ((melt_ptr_t)
				       ( /*_.CUR__V11*/ meltfptr[10]),
				       (melt_ptr_t) (( /*!OUTPUT_C_CODE */
						      meltfrout->tabval[4])),
				       (MELTBPARSTR_PTR MELTBPARSTR_PTR
					MELTBPARSTR_LONG ""), argtab, "",
				       (union meltparam_un *) 0);
		      }
		      ;

		      {
			MELT_LOCATION ("warmelt-outobj.melt:2613:/ locexp");
			/*add2sbuf_strconst */
			  meltgc_add_strbuf ((melt_ptr_t)
					     ( /*_.IMPLBUF__V4*/ meltfptr[3]),
					     (";"));
		      }
		      ;
		      MELT_LOCATION ("warmelt-outobj.melt:2610:/ quasiblock");


		      /*epilog */

		      /*^clear */
		/*clear *//*_.OUTPUT_C_CODE__V25*/ meltfptr[14] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

     /*_.IFELSE___V24*/ meltfptr[13] = NULL;;
		  }
		;
		/*^compute */
		/*_.IFELSE___V12*/ meltfptr[11] =
		  /*_.IFELSE___V24*/ meltfptr[13];;
		/*epilog */

		MELT_LOCATION ("warmelt-outobj.melt:2599:/ clear");
	      /*clear *//*_#IF___L5*/ meltfnum[4] = 0;
		/*^clear */
	      /*clear *//*_.IFELSE___V24*/ meltfptr[13] = 0;
	      }
	      ;
	    }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:2615:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2616:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[5])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[5])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V27*/ meltfptr[16] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V27*/ meltfptr[16] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L8*/ meltfnum[4] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V27*/ meltfptr[16])));;
	    /*^compute */
    /*_#I__L9*/ meltfnum[8] =
	      (( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5]) <
	       ( /*_#GET_INT__L8*/ meltfnum[4]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2615:/ cond");
	    /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V28*/ meltfptr[17] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:2615:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(2615) ? (2615) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V28*/ meltfptr[17] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V26*/ meltfptr[15] = /*_.IFELSE___V28*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2615:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V27*/ meltfptr[16] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L8*/ meltfnum[4] = 0;
	    /*^clear */
	      /*clear *//*_#I__L9*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V28*/ meltfptr[17] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V26*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:2617:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L10*/ meltfnum[5] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.DECLBUF__V3*/ meltfptr[2]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2618:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[5])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[5])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V30*/ meltfptr[12] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V30*/ meltfptr[12] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L11*/ meltfnum[4] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V30*/ meltfptr[12])));;
	    /*^compute */
    /*_#I__L12*/ meltfnum[8] =
	      (( /*_#STRBUF_USEDLENGTH__L10*/ meltfnum[5]) <
	       ( /*_#GET_INT__L11*/ meltfnum[4]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2617:/ cond");
	    /*cond */ if ( /*_#I__L12*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V31*/ meltfptr[21] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:2617:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited declbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(2617) ? (2617) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V31*/ meltfptr[21] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V29*/ meltfptr[19] = /*_.IFELSE___V31*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2617:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L10*/ meltfnum[5] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V30*/ meltfptr[12] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L11*/ meltfnum[4] = 0;
	    /*^clear */
	      /*clear *//*_#I__L12*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V31*/ meltfptr[21] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V29*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	}			/* end foreach_in_list meltcit1__EACHLIST */
     /*_.PAIR__V10*/ meltfptr[8] = NULL;
     /*_.CUR__V11*/ meltfptr[10] = NULL;


      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:2595:/ clear");
	    /*clear *//*_.PAIR__V10*/ meltfptr[8] = 0;
      /*^clear */
	    /*clear *//*_.CUR__V11*/ meltfptr[10] = 0;
      /*^clear */
	    /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
      /*^clear */
	    /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V26*/ meltfptr[15] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V29*/ meltfptr[19] = 0;
    }				/*endciterblock FOREACH_IN_LIST */
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2593:/ clear");
	   /*clear *//*_#DEPTH__L3*/ meltfnum[0] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2590:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUT_CODE_INSTRUCTIONS_LIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_53_warmelt_outobj_OUTPUT_CODE_INSTRUCTIONS_LIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJCHECKSIGNAL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2624:/ getarg");
 /*_.OBCHI__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2625:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBCHI__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCHECKSIGNAL */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2625:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2625:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ochi"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2625) ? (2625) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2625:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2626:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCHI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V7*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2628:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OLOC__V7*/ meltfptr[5])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
	    /*^apply.arg */
	    argtab[2].meltbp_cstring = "checksignal";
	    /*_.OUTPUT_LOCATION__V9*/ meltfptr[8] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
			  (melt_ptr_t) ( /*_.OLOC__V7*/ meltfptr[5]),
			  (MELTBPARSTR_PTR MELTBPARSTR_LONG
			   MELTBPARSTR_CSTRING ""), argtab, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V8*/ meltfptr[7] = /*_.OUTPUT_LOCATION__V9*/ meltfptr[8];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2628:/ clear");
	     /*clear *//*_.OUTPUT_LOCATION__V9*/ meltfptr[8] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V8*/ meltfptr[7] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2629:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = " MELT_CHECK_SIGNAL();";
      /*_.ADD2OUT__V10*/ meltfptr[8] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!ADD2OUT */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2630:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2626:/ clear");
	   /*clear *//*_.OLOC__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IF___V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ADD2OUT__V10*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2624:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJCHECKSIGNAL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_54_warmelt_outobj_OUTPUCOD_OBJCHECKSIGNAL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 20
    melt_ptr_t mcfr_varptr[20];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 20; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK nbval 20*/
  meltfram__.mcfr_nbvar = 20 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJANYBLOCK", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2635:/ getarg");
 /*_.OBLO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2636:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJANYBLOCK */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2636:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2636:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oblo"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2636) ? (2636) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2636:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2637:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OBI_LOC__V7*/ meltfptr[5] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "block";
      /*_.OUTPUT_LOCATION__V8*/ meltfptr[7] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OBI_LOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2638:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBLO_BODYL");
  /*_.BODYL__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2639:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBLO_EPIL");
  /*_.EPIL__V11*/ meltfptr[10] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V12*/ meltfptr[11] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[2])),
	( /*_#I__L3*/ meltfnum[1])));;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2642:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*anyblock*/{"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2643:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L4*/ meltfnum[3] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.BODYL__V10*/ meltfptr[9])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2643:/ cond");
    /*cond */ if ( /*_#IS_LIST__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2644:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXDEPTHP1__V12*/ meltfptr[11];
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V14*/ meltfptr[13] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_CODE_INSTRUCTIONS_LIST */ meltfrout->
			    tabval[3])),
			  (melt_ptr_t) ( /*_.BODYL__V10*/ meltfptr[9]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V13*/ meltfptr[12] =
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V14*/ meltfptr[13];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2643:/ clear");
	     /*clear *//*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V14*/ meltfptr[13] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V13*/ meltfptr[12] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2645:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L5*/ meltfnum[4] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.EPIL__V11*/ meltfptr[10])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2645:/ cond");
    /*cond */ if ( /*_#IS_LIST__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#GET_INT__L6*/ meltfnum[5] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V12*/ meltfptr[11])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2647:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#GET_INT__L6*/ meltfnum[5]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2648:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("/*epilog*/"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2649:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXDEPTHP1__V12*/ meltfptr[11];
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V16*/ meltfptr[15] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_CODE_INSTRUCTIONS_LIST */ meltfrout->
			    tabval[3])),
			  (melt_ptr_t) ( /*_.EPIL__V11*/ meltfptr[10]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2646:/ quasiblock");


	  /*_.PROGN___V17*/ meltfptr[16] =
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V16*/ meltfptr[15];;
	  /*^compute */
	  /*_.IF___V15*/ meltfptr[13] = /*_.PROGN___V17*/ meltfptr[16];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2645:/ clear");
	     /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V16*/ meltfptr[15] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[16] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V15*/ meltfptr[13] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2650:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("}"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2651:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2652:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2653:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V19*/ meltfptr[16] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V19*/ meltfptr[16] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L8*/ meltfnum[7] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V19*/ meltfptr[16])));;
      /*^compute */
   /*_#I__L9*/ meltfnum[8] =
	(( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5]) <
	 ( /*_#GET_INT__L8*/ meltfnum[7]));;
      MELT_LOCATION ("warmelt-outobj.melt:2652:/ cond");
      /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2652:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2652) ? (2652) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[15] = /*_.IFELSE___V20*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2652:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V19*/ meltfptr[16] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_#I__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V9*/ meltfptr[8] = /*_.IFCPP___V18*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-outobj.melt:2638:/ clear");
	   /*clear *//*_.BODYL__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.EPIL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.IF___V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2635:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V9*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2635:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OBI_LOC__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V9*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJANYBLOCK", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_55_warmelt_outobj_OUTPUCOD_OBJANYBLOCK */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 40
    melt_ptr_t mcfr_varptr[40];
#define MELTFRAM_NBVARNUM 19
    long mcfr_varnum[19];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 40; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK nbval 40*/
  meltfram__.mcfr_nbvar = 40 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJMULTIALLOCBLOCK", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2659:/ getarg");
 /*_.OBLO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2660:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJMULTIALLOCBLOCK */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2660:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2660:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oblo"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2660) ? (2660) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2660:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2661:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V7*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2662:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OMALBLO_ALLSTRUCT");
  /*_.OALLSTRUCT__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2663:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OMALBLO_NAME");
  /*_.ONAME__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2664:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBLO_EPIL");
  /*_.EPIL__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2665:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBLO_BODYL");
  /*_.BODYL__V11*/ meltfptr[10] = slot;
    };
    ;
 /*_#DEPTHP1__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V12*/ meltfptr[11] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#DEPTHP1__L3*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:2668:/ quasiblock");


 /*_.PTRBUF__V14*/ meltfptr[13] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[2])),
			 (const char *) 0);;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2669:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.PTRBUF__V14*/ meltfptr[13]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2670:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.PTRBUF__V14*/ meltfptr[13]),
			   ("_ptr"));
    }
    ;
 /*_.STRBUF2STRING__V15*/ meltfptr[14] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[3])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.PTRBUF__V14*/ meltfptr[13]))));;
    /*^compute */
    /*_.LET___V13*/ meltfptr[12] = /*_.STRBUF2STRING__V15*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-outobj.melt:2668:/ clear");
	   /*clear *//*_.PTRBUF__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V15*/ meltfptr[14] = 0;
    /*_.ONAMEPTR__V16*/ meltfptr[13] = /*_.LET___V13*/ meltfptr[12];;
    MELT_LOCATION ("warmelt-outobj.melt:2673:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "blockmultialloc";
      /*_.OUTPUT_LOCATION__V17*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.OLOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2674:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L4*/ meltfnum[3] =
	(( /*_.OALLSTRUCT__V8*/ meltfptr[7]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.OALLSTRUCT__V8*/ meltfptr[7])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-outobj.melt:2674:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2674:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oallstruct"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2674) ? (2674) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[17] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2674:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2675:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*multiallocblock*/{"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2676:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTHP1__L3*/ meltfnum[1]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2677:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("struct "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2678:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2679:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("_st {"));
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.OALLSTRUCT__V8*/ meltfptr[7]);
      for ( /*_#STRIX__L5*/ meltfnum[3] = 0;
	   ( /*_#STRIX__L5*/ meltfnum[3] >= 0)
	   && ( /*_#STRIX__L5*/ meltfnum[3] < meltcit1__EACHTUP_ln);
	/*_#STRIX__L5*/ meltfnum[3]++)
	{
	  /*_.CURSTRU__V20*/ meltfptr[18] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.OALLSTRUCT__V8*/ meltfptr[7]),
			       /*_#STRIX__L5*/ meltfnum[3]);




#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:2683:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#IS_A__L6*/ meltfnum[5] =
	      melt_is_instance_of ((melt_ptr_t)
				   ( /*_.CURSTRU__V20*/ meltfptr[18]),
				   (melt_ptr_t) (( /*!CLASS_OBJINITELEM */
						  meltfrout->tabval[5])));;
	    MELT_LOCATION ("warmelt-outobj.melt:2683:/ cond");
	    /*cond */ if ( /*_#IS_A__L6*/ meltfnum[5])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V22*/ meltfptr[21] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:2683:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check curstru"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(2683) ? (2683) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V21*/ meltfptr[20] = /*_.IFELSE___V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2683:/ clear");
	      /*clear *//*_#IS_A__L6*/ meltfnum[5] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V22*/ meltfptr[21] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2684:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTHP1__L3*/ meltfnum[1]), 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2685:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*_.OUTPUT_C_DECLINIT__V23*/ meltfptr[21] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURSTRU__V20*/ meltfptr[18]),
			   (melt_ptr_t) (( /*!OUTPUT_C_DECLINIT */ meltfrout->
					  tabval[6])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:2686:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2687:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[7])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[8])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[7])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V25*/ meltfptr[24] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V25*/ meltfptr[24] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L8*/ meltfnum[7] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V25*/ meltfptr[24])));;
	    /*^compute */
    /*_#I__L9*/ meltfnum[8] =
	      (( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5]) <
	       ( /*_#GET_INT__L8*/ meltfnum[7]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2686:/ cond");
	    /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V26*/ meltfptr[25] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:2686:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(2686) ? (2686) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V24*/ meltfptr[23] = /*_.IFELSE___V26*/ meltfptr[25];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2686:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V25*/ meltfptr[24] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_#I__L9*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V24*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  if ( /*_#STRIX__L5*/ meltfnum[3] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:2680:/ clear");
	    /*clear *//*_.CURSTRU__V20*/ meltfptr[18] = 0;
      /*^clear */
	    /*clear *//*_#STRIX__L5*/ meltfnum[3] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_DECLINIT__V23*/ meltfptr[21] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V24*/ meltfptr[23] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2689:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" long "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2690:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2691:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("_endgap; } *"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2692:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2693:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("_ptr = 0;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2694:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTHP1__L3*/ meltfnum[1]), 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2695:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2696:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("_ptr = (struct "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2697:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2698:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("_st *) meltgc_allocate (sizeof (struct "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2699:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.ONAME__V9*/ meltfptr[8])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2700:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("_st), 0);"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2701:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTHP1__L3*/ meltfnum[1]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2703:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "blockmultialloc.initfill";
      /*_.OUTPUT_LOCATION__V27*/ meltfptr[24] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.OLOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.OALLSTRUCT__V8*/ meltfptr[7]);
      for ( /*_#STRIX__L10*/ meltfnum[5] = 0;
	   ( /*_#STRIX__L10*/ meltfnum[5] >= 0)
	   && ( /*_#STRIX__L10*/ meltfnum[5] < meltcit2__EACHTUP_ln);
	/*_#STRIX__L10*/ meltfnum[5]++)
	{
	  /*_.CURSTRU__V28*/ meltfptr[25] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.OALLSTRUCT__V8*/ meltfptr[7]),
			       /*_#STRIX__L10*/ meltfnum[5]);



	  MELT_LOCATION ("warmelt-outobj.melt:2707:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ONAMEPTR__V16*/ meltfptr[13];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#DEPTHP1__L3*/ meltfnum[1];
	    /*_.OUTPUT_C_INITIAL_FILL__V29*/ meltfptr[28] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURSTRU__V28*/ meltfptr[25]),
			   (melt_ptr_t) (( /*!OUTPUT_C_INITIAL_FILL */
					  meltfrout->tabval[9])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:2708:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L11*/ meltfnum[7] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2709:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[7])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[8])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[7])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V31*/ meltfptr[30] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V31*/ meltfptr[30] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L12*/ meltfnum[8] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V31*/ meltfptr[30])));;
	    /*^compute */
    /*_#I__L13*/ meltfnum[12] =
	      (( /*_#STRBUF_USEDLENGTH__L11*/ meltfnum[7]) <
	       ( /*_#GET_INT__L12*/ meltfnum[8]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2708:/ cond");
	    /*cond */ if ( /*_#I__L13*/ meltfnum[12])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V32*/ meltfptr[31] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:2708:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(2708) ? (2708) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V30*/ meltfptr[29] = /*_.IFELSE___V32*/ meltfptr[31];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2708:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L11*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V31*/ meltfptr[30] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L12*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_#I__L13*/ meltfnum[12] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V30*/ meltfptr[29] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2710:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#DEPTHP1__L3*/ meltfnum[1]), 0);
	  }
	  ;
	  if ( /*_#STRIX__L10*/ meltfnum[5] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:2704:/ clear");
	    /*clear *//*_.CURSTRU__V28*/ meltfptr[25] = 0;
      /*^clear */
	    /*clear *//*_#STRIX__L10*/ meltfnum[5] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_INITIAL_FILL__V29*/ meltfptr[28] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V30*/ meltfptr[29] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2713:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L14*/ meltfnum[7] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.BODYL__V11*/ meltfptr[10])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2713:/ cond");
    /*cond */ if ( /*_#IS_LIST__L14*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2714:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXDEPTHP1__V12*/ meltfptr[11];
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V34*/ meltfptr[31] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_CODE_INSTRUCTIONS_LIST */ meltfrout->
			    tabval[10])),
			  (melt_ptr_t) ( /*_.BODYL__V11*/ meltfptr[10]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V33*/ meltfptr[30] =
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V34*/ meltfptr[31];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2713:/ clear");
	     /*clear *//*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V34*/ meltfptr[31] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V33*/ meltfptr[30] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2716:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L15*/ meltfnum[8] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.EPIL__V10*/ meltfptr[9])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2716:/ cond");
    /*cond */ if ( /*_#IS_LIST__L15*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#GET_INT__L16*/ meltfnum[12] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V12*/ meltfptr[11])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2718:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#GET_INT__L16*/ meltfnum[12]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2719:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("/*epilog*/"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2720:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXDEPTHP1__V12*/ meltfptr[11];
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V36*/ meltfptr[35] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_CODE_INSTRUCTIONS_LIST */ meltfrout->
			    tabval[10])),
			  (melt_ptr_t) ( /*_.EPIL__V10*/ meltfptr[9]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2717:/ quasiblock");


	  /*_.PROGN___V37*/ meltfptr[36] =
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V36*/ meltfptr[35];;
	  /*^compute */
	  /*_.IF___V35*/ meltfptr[31] = /*_.PROGN___V37*/ meltfptr[36];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2716:/ clear");
	     /*clear *//*_#GET_INT__L16*/ meltfnum[12] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V36*/ meltfptr[35] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V37*/ meltfptr[36] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V35*/ meltfptr[31] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2722:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("} /*end multiallocblock*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2723:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2661:/ clear");
	   /*clear *//*_.OLOC__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OALLSTRUCT__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.ONAME__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.EPIL__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.BODYL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#DEPTHP1__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.ONAMEPTR__V16*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V17*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V27*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L14*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IF___V33*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L15*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IF___V35*/ meltfptr[31] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2725:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L17*/ meltfnum[12] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2726:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[7])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[8])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[7])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V39*/ meltfptr[36] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V39*/ meltfptr[36] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L18*/ meltfnum[1] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V39*/ meltfptr[36])));;
      /*^compute */
   /*_#I__L19*/ meltfnum[7] =
	(( /*_#STRBUF_USEDLENGTH__L17*/ meltfnum[12]) <
	 ( /*_#GET_INT__L18*/ meltfnum[1]));;
      MELT_LOCATION ("warmelt-outobj.melt:2725:/ cond");
      /*cond */ if ( /*_#I__L19*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V40*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2725:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2725) ? (2725) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V40*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V38*/ meltfptr[35] = /*_.IFELSE___V40*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2725:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L17*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V39*/ meltfptr[36] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L18*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_#I__L19*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V40*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V38*/ meltfptr[35] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2659:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V38*/ meltfptr[35];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2659:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V38*/ meltfptr[35] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJMULTIALLOCBLOCK", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_56_warmelt_outobj_OUTPUCOD_OBJMULTIALLOCBLOCK */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 40
    melt_ptr_t mcfr_varptr[40];
#define MELTFRAM_NBVARNUM 20
    long mcfr_varnum[20];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 40; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK nbval 40*/
  meltfram__.mcfr_nbvar = 40 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJCITERBLOCK", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2731:/ getarg");
 /*_.OBCIT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2732:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBCIT__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCITERBLOCK */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2732:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2732:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obcit"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2732) ? (2732) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2732:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2733:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCIT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V7*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2734:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCIT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBLO_BODYL");
  /*_.BODYL__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2735:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCIT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBLO_EPIL");
  /*_.EPIL__V9*/ meltfptr[8] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V10*/ meltfptr[9] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[1])),
	( /*_#I__L3*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:2737:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCIT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBCITER_BEFORE");
  /*_.OBEFORE__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2738:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCIT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "OBCITER_AFTER");
  /*_.OAFTER__V12*/ meltfptr[11] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2739:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCIT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OBCITER_CITER");
  /*_.CITER__V13*/ meltfptr[12] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2741:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CITER__V13*/ meltfptr[12]),
			     (melt_ptr_t) (( /*!CLASS_CITERATOR */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-outobj.melt:2741:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2741:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check citer"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2741) ? (2741) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2741:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2742:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "citerblock";
      /*_.OUTPUT_LOCATION__V16*/ meltfptr[14] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2743:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*citerblock "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2744:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CITER__V13*/ meltfptr[12]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V17*/ meltfptr[16] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_strbuf_ccomment ((melt_ptr_t)
				  ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.NAMED_NAME__V17*/
						    meltfptr[16])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2745:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/ {"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2746:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2747:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "citerbefore";
      /*_.OUTPUT_LOCATION__V18*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.OBEFORE__V11*/ meltfptr[10]);
      for ( /*_#IX__L5*/ meltfnum[3] = 0;
	   ( /*_#IX__L5*/ meltfnum[3] >= 0)
	   && ( /*_#IX__L5*/ meltfnum[3] < meltcit1__EACHTUP_ln);
	/*_#IX__L5*/ meltfnum[3]++)
	{
	  /*_.OBEF__V19*/ meltfptr[18] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.OBEFORE__V11*/ meltfptr[10]),
			       /*_#IX__L5*/ meltfnum[3]);



  /*_#GET_INT__L6*/ meltfnum[5] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V10*/ meltfptr[9])));;
	  MELT_LOCATION ("warmelt-outobj.melt:2751:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#GET_INT__L6*/ meltfnum[5];
	    /*_.OUTPUT_C_CODE__V20*/ meltfptr[19] =
	      meltgc_send ((melt_ptr_t) ( /*_.OBEF__V19*/ meltfptr[18]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:2752:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[6] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2753:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[5])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[5])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V22*/ meltfptr[21] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L8*/ meltfnum[7] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V22*/ meltfptr[21])));;
	    /*^compute */
    /*_#I__L9*/ meltfnum[8] =
	      (( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[6]) <
	       ( /*_#GET_INT__L8*/ meltfnum[7]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2752:/ cond");
	    /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V23*/ meltfptr[22] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:2752:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(2752) ? (2752) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V21*/ meltfptr[20] = /*_.IFELSE___V23*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2752:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[6] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V22*/ meltfptr[21] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	      /*clear *//*_#I__L9*/ meltfnum[8] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V23*/ meltfptr[22] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V21*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  if ( /*_#IX__L5*/ meltfnum[3] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:2748:/ clear");
	    /*clear *//*_.OBEF__V19*/ meltfptr[18] = 0;
      /*^clear */
	    /*clear *//*_#IX__L5*/ meltfnum[3] = 0;
      /*^clear */
	    /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_CODE__V20*/ meltfptr[19] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V21*/ meltfptr[20] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2755:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2756:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "citerbody";
      /*_.OUTPUT_LOCATION__V24*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2757:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L10*/ meltfnum[6] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.BODYL__V8*/ meltfptr[7])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2757:/ cond");
    /*cond */ if ( /*_#IS_LIST__L10*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2758:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXDEPTHP1__V10*/ meltfptr[9];
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V26*/ meltfptr[25] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_CODE_INSTRUCTIONS_LIST */ meltfrout->
			    tabval[7])),
			  (melt_ptr_t) ( /*_.BODYL__V8*/ meltfptr[7]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V25*/ meltfptr[22] =
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V26*/ meltfptr[25];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2757:/ clear");
	     /*clear *//*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V26*/ meltfptr[25] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V25*/ meltfptr[22] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2759:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2760:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "citerafter";
      /*_.OUTPUT_LOCATION__V27*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit2__EACHTUP */
      long meltcit2__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.OAFTER__V12*/ meltfptr[11]);
      for ( /*_#IX__L11*/ meltfnum[7] = 0;
	   ( /*_#IX__L11*/ meltfnum[7] >= 0)
	   && ( /*_#IX__L11*/ meltfnum[7] < meltcit2__EACHTUP_ln);
	/*_#IX__L11*/ meltfnum[7]++)
	{
	  /*_.OAFT__V28*/ meltfptr[27] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.OAFTER__V12*/ meltfptr[11]),
			       /*_#IX__L11*/ meltfnum[7]);



  /*_#GET_INT__L12*/ meltfnum[8] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V10*/ meltfptr[9])));;
	  MELT_LOCATION ("warmelt-outobj.melt:2764:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^ojbmsend.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^ojbmsend.arg */
	    argtab[2].meltbp_long = /*_#GET_INT__L12*/ meltfnum[8];
	    /*_.OUTPUT_C_CODE__V29*/ meltfptr[28] =
	      meltgc_send ((melt_ptr_t) ( /*_.OAFT__V28*/ meltfptr[27]),
			   (melt_ptr_t) (( /*!OUTPUT_C_CODE */ meltfrout->
					  tabval[4])),
			   (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_LONG
			    ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-outobj.melt:2765:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {

	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
    /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[12] =
	      melt_strbuf_usedlength ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2766:/ cond");
	    /*cond */ if (
			   /*ifisa */
			   melt_is_instance_of ((melt_ptr_t)
						(( /*!BUFFER_LIMIT_CONT */
						  meltfrout->tabval[5])),
						(melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	      )			/*then */
	      {
		/*^cond.then */
		/*^getslot */
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
				   tabval[5])) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
      /*_.REFERENCED_VALUE__V31*/ meltfptr[30] = slot;
		};
		;
	      }
	    else
	      {			/*^cond.else */

     /*_.REFERENCED_VALUE__V31*/ meltfptr[30] = NULL;;
	      }
	    ;
	    /*^compute */
    /*_#GET_INT__L14*/ meltfnum[13] =
	      (melt_get_int
	       ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V31*/ meltfptr[30])));;
	    /*^compute */
    /*_#I__L15*/ meltfnum[14] =
	      (( /*_#STRBUF_USEDLENGTH__L13*/ meltfnum[12]) <
	       ( /*_#GET_INT__L14*/ meltfnum[13]));;
	    MELT_LOCATION ("warmelt-outobj.melt:2765:/ cond");
	    /*cond */ if ( /*_#I__L15*/ meltfnum[14])	/*then */
	      {
		/*^cond.then */
		/*_.IFELSE___V32*/ meltfptr[31] = ( /*nil */ NULL);;
	      }
	    else
	      {
		MELT_LOCATION ("warmelt-outobj.melt:2765:/ cond.else");

		/*^block */
		/*anyblock */
		{




		  {
		    /*^locexp */
		    melt_assert_failed (("check limited implbuf"),
					("warmelt-outobj.melt")
					? ("warmelt-outobj.melt") : __FILE__,
					(2765) ? (2765) : __LINE__,
					__FUNCTION__);
		    ;
		  }
		  ;
		/*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
		  /*epilog */
		}
		;
	      }
	    ;
	    /*^compute */
	    /*_.IFCPP___V30*/ meltfptr[29] = /*_.IFELSE___V32*/ meltfptr[31];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-outobj.melt:2765:/ clear");
	      /*clear *//*_#STRBUF_USEDLENGTH__L13*/ meltfnum[12] = 0;
	    /*^clear */
	      /*clear *//*_.REFERENCED_VALUE__V31*/ meltfptr[30] = 0;
	    /*^clear */
	      /*clear *//*_#GET_INT__L14*/ meltfnum[13] = 0;
	    /*^clear */
	      /*clear *//*_#I__L15*/ meltfnum[14] = 0;
	    /*^clear */
	      /*clear *//*_.IFELSE___V32*/ meltfptr[31] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V30*/ meltfptr[29] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  if ( /*_#IX__L11*/ meltfnum[7] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit2__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-outobj.melt:2761:/ clear");
	    /*clear *//*_.OAFT__V28*/ meltfptr[27] = 0;
      /*^clear */
	    /*clear *//*_#IX__L11*/ meltfnum[7] = 0;
      /*^clear */
	    /*clear *//*_#GET_INT__L12*/ meltfnum[8] = 0;
      /*^clear */
	    /*clear *//*_.OUTPUT_C_CODE__V29*/ meltfptr[28] = 0;
      /*^clear */
	    /*clear *//*_.IFCPP___V30*/ meltfptr[29] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2768:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2769:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_cstring = "citerepil";
      /*_.OUTPUT_LOCATION__V33*/ meltfptr[30] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_CSTRING ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2770:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L16*/ meltfnum[12] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.EPIL__V9*/ meltfptr[8])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2770:/ cond");
    /*cond */ if ( /*_#IS_LIST__L16*/ meltfnum[12])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#GET_INT__L17*/ meltfnum[13] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V10*/ meltfptr[9])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2772:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#GET_INT__L17*/ meltfnum[13]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2773:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("/*citerepilog*/"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2774:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXDEPTHP1__V10*/ meltfptr[9];
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V35*/ meltfptr[34] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_CODE_INSTRUCTIONS_LIST */ meltfrout->
			    tabval[7])),
			  (melt_ptr_t) ( /*_.EPIL__V9*/ meltfptr[8]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2771:/ quasiblock");


	  /*_.PROGN___V36*/ meltfptr[35] =
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V35*/ meltfptr[34];;
	  /*^compute */
	  /*_.IF___V34*/ meltfptr[31] = /*_.PROGN___V36*/ meltfptr[35];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2770:/ clear");
	     /*clear *//*_#GET_INT__L17*/ meltfnum[13] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V35*/ meltfptr[34] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V36*/ meltfptr[35] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V34*/ meltfptr[31] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2775:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("} /*endciterblock "));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2776:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.CITER__V13*/ meltfptr[12]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V37*/ meltfptr[34] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_strbuf_ccomment ((melt_ptr_t)
				  ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.NAMED_NAME__V37*/
						    meltfptr[34])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2777:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2778:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

    MELT_LOCATION ("warmelt-outobj.melt:2733:/ clear");
	   /*clear *//*_.OLOC__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.BODYL__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.EPIL__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OBEFORE__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OAFTER__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.CITER__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V24*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L10*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.IF___V25*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V27*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V33*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L16*/ meltfnum[12] = 0;
    /*^clear */
	   /*clear *//*_.IF___V34*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V37*/ meltfptr[34] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2780:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L18*/ meltfnum[14] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2781:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[5])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[6])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[5])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V39*/ meltfptr[5] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V39*/ meltfptr[5] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L19*/ meltfnum[13] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V39*/ meltfptr[5])));;
      /*^compute */
   /*_#I__L20*/ meltfnum[1] =
	(( /*_#STRBUF_USEDLENGTH__L18*/ meltfnum[14]) <
	 ( /*_#GET_INT__L19*/ meltfnum[13]));;
      MELT_LOCATION ("warmelt-outobj.melt:2780:/ cond");
      /*cond */ if ( /*_#I__L20*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V40*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2780:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2780) ? (2780) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V40*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V38*/ meltfptr[35] = /*_.IFELSE___V40*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2780:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L18*/ meltfnum[14] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V39*/ meltfptr[5] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L19*/ meltfnum[13] = 0;
      /*^clear */
	     /*clear *//*_#I__L20*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V40*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V38*/ meltfptr[35] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2731:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V38*/ meltfptr[35];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2731:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V38*/ meltfptr[35] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJCITERBLOCK", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_57_warmelt_outobj_OUTPUCOD_OBJCITERBLOCK */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJCOMMENTINSTR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2786:/ getarg");
 /*_.OBCI__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2787:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBCI__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCOMMENTINSTR */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2787:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2787:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obci"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2787) ? (2787) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2787:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2788:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OLOC__V8*/ meltfptr[7] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2789:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBCI__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBCI_COMMENT");
  /*_.COMS__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2790:/ quasiblock");


 /*_.SBU__V11*/ meltfptr[10] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[1])),
			 (const char *) 0);;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2791:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t) ( /*_.SBU__V11*/ meltfptr[10]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.COMS__V9*/
						    meltfptr[8])));
    }
    ;
 /*_.STRBUF2STRING__V12*/ meltfptr[11] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[2])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.SBU__V11*/ meltfptr[10]))));;
    /*^compute */
    /*_.LET___V10*/ meltfptr[9] = /*_.STRBUF2STRING__V12*/ meltfptr[11];;

    MELT_LOCATION ("warmelt-outobj.melt:2790:/ clear");
	   /*clear *//*_.SBU__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V12*/ meltfptr[11] = 0;
    /*_.COMSTR__V13*/ meltfptr[10] = /*_.LET___V10*/ meltfptr[9];;
    MELT_LOCATION ("warmelt-outobj.melt:2795:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "comment";
      /*_.OUTPUT_LOCATION__V14*/ meltfptr[11] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.OLOC__V8*/ meltfptr[7]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2796:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/**COMMENT: "));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2797:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.COMSTR__V13*/
					     meltfptr[10])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2798:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   (" **/;"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2799:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2800:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2801:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V16*/ meltfptr[15] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V16*/ meltfptr[15] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L4*/ meltfnum[3] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V16*/ meltfptr[15])));;
      /*^compute */
   /*_#I__L5*/ meltfnum[4] =
	(( /*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1]) <
	 ( /*_#GET_INT__L4*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2800:/ cond");
      /*cond */ if ( /*_#I__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2800:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2800) ? (2800) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2800:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V16*/ meltfptr[15] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#I__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[5] = /*_.IFCPP___V15*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-outobj.melt:2788:/ clear");
	   /*clear *//*_.OLOC__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.COMS__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.COMSTR__V13*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V14*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2786:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2786:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJCOMMENTINSTR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_58_warmelt_outobj_OUTPUCOD_OBJCOMMENTINSTR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 25
    melt_ptr_t mcfr_varptr[25];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 25; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK nbval 25*/
  meltfram__.mcfr_nbvar = 25 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("OUTPUCOD_OBJCOMMENTEDBLOCK", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-outobj.melt:2807:/ getarg");
 /*_.OBLO__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DECLBUF__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DECLBUF__V3*/ meltfptr[2])) !=
	      NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.IMPLBUF__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#DEPTH__L1*/ meltfnum[0] = meltxargtab_[2].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2808:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJCOMMENTEDBLOCK */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-outobj.melt:2808:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2808:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oblo"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2808) ? (2808) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2808:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2809:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBI_LOC");
  /*_.OBI_LOC__V7*/ meltfptr[5] = slot;
    };
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[1].meltbp_long = /*_#DEPTH__L1*/ meltfnum[0];
      /*^apply.arg */
      argtab[2].meltbp_cstring = "block";
      /*_.OUTPUT_LOCATION__V8*/ meltfptr[7] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!OUTPUT_LOCATION */ meltfrout->tabval[1])),
		    (melt_ptr_t) ( /*_.OBI_LOC__V7*/ meltfptr[5]),
		    (MELTBPARSTR_PTR MELTBPARSTR_LONG MELTBPARSTR_CSTRING ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2810:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBLO_BODYL");
  /*_.BODYL__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2811:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBLO_EPIL");
  /*_.EPIL__V11*/ meltfptr[10] = slot;
    };
    ;
 /*_#I__L3*/ meltfnum[1] =
      (( /*_#DEPTH__L1*/ meltfnum[0]) + (1));;
    /*^compute */
 /*_.BOXDEPTHP1__V12*/ meltfptr[11] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[2])),
	( /*_#I__L3*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-outobj.melt:2813:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.OBLO__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OCOMBLO_COMMENT");
  /*_.COMS__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2814:/ quasiblock");


 /*_.SBU__V15*/ meltfptr[14] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			 (const char *) 0);;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2815:/ locexp");
      meltgc_add_strbuf_ccomment ((melt_ptr_t) ( /*_.SBU__V15*/ meltfptr[14]),
				  melt_string_str ((melt_ptr_t)
						   ( /*_.COMS__V13*/
						    meltfptr[12])));
    }
    ;
 /*_.STRBUF2STRING__V16*/ meltfptr[15] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[4])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.SBU__V15*/ meltfptr[14]))));;
    /*^compute */
    /*_.LET___V14*/ meltfptr[13] = /*_.STRBUF2STRING__V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-outobj.melt:2814:/ clear");
	   /*clear *//*_.SBU__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V16*/ meltfptr[15] = 0;
    /*_.COMSTR__V17*/ meltfptr[14] = /*_.LET___V14*/ meltfptr[13];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2819:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*com.block:"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2820:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.COMSTR__V17*/
					     meltfptr[14])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2821:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/{"));
    }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2822:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L4*/ meltfnum[3] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.BODYL__V10*/ meltfptr[9])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2822:/ cond");
    /*cond */ if ( /*_#IS_LIST__L4*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-outobj.melt:2823:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXDEPTHP1__V12*/ meltfptr[11];
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V19*/ meltfptr[18] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_CODE_INSTRUCTIONS_LIST */ meltfrout->
			    tabval[5])),
			  (melt_ptr_t) ( /*_.BODYL__V10*/ meltfptr[9]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  /*_.IF___V18*/ meltfptr[15] =
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V19*/ meltfptr[18];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2822:/ clear");
	     /*clear *//*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V19*/ meltfptr[18] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V18*/ meltfptr[15] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-outobj.melt:2824:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_LIST__L5*/ meltfnum[4] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.EPIL__V11*/ meltfptr[10])) ==
       MELTOBMAG_LIST);;
    MELT_LOCATION ("warmelt-outobj.melt:2824:/ cond");
    /*cond */ if ( /*_#IS_LIST__L5*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#GET_INT__L6*/ meltfnum[5] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.BOXDEPTHP1__V12*/ meltfptr[11])));;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2826:/ locexp");
	    meltgc_strbuf_add_indent ((melt_ptr_t)
				      ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				      ( /*_#GET_INT__L6*/ meltfnum[5]), 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2827:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 ("/*comp.epilog:"));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2828:/ locexp");
	    /*add2sbuf_string */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				 melt_string_str ((melt_ptr_t)
						  ( /*_.COMSTR__V17*/
						   meltfptr[14])));
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-outobj.melt:2829:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.IMPLBUF__V4*/ meltfptr[3]), ("*/"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2830:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DECLBUF__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.IMPLBUF__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.BOXDEPTHP1__V12*/ meltfptr[11];
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V21*/ meltfptr[20] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!OUTPUT_CODE_INSTRUCTIONS_LIST */ meltfrout->
			    tabval[5])),
			  (melt_ptr_t) ( /*_.EPIL__V11*/ meltfptr[10]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-outobj.melt:2825:/ quasiblock");


	  /*_.PROGN___V22*/ meltfptr[21] =
	    /*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V21*/ meltfptr[20];;
	  /*^compute */
	  /*_.IF___V20*/ meltfptr[18] = /*_.PROGN___V22*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-outobj.melt:2824:/ clear");
	     /*clear *//*_#GET_INT__L6*/ meltfnum[5] = 0;
	  /*^clear */
	     /*clear *//*_.OUTPUT_CODE_INSTRUCTIONS_LIST__V21*/ meltfptr[20] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V20*/ meltfptr[18] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2831:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("}"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2832:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("/*com.end block:"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2833:/ locexp");
      /*add2sbuf_string */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   melt_string_str ((melt_ptr_t)
					    ( /*_.COMSTR__V17*/
					     meltfptr[14])));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2834:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
			   ("*/"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2835:/ locexp");
      meltgc_strbuf_add_indent ((melt_ptr_t) ( /*_.IMPLBUF__V4*/ meltfptr[3]),
				( /*_#DEPTH__L1*/ meltfnum[0]), 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-outobj.melt:2836:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] =
	melt_strbuf_usedlength ((melt_ptr_t)
				( /*_.IMPLBUF__V4*/ meltfptr[3]));;
      MELT_LOCATION ("warmelt-outobj.melt:2837:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!BUFFER_LIMIT_CONT */
					    meltfrout->tabval[6])),
					  (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->tabval[7])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!BUFFER_LIMIT_CONT */ meltfrout->
			     tabval[6])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 0, "REFERENCED_VALUE");
     /*_.REFERENCED_VALUE__V24*/ meltfptr[21] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.REFERENCED_VALUE__V24*/ meltfptr[21] = NULL;;
	}
      ;
      /*^compute */
   /*_#GET_INT__L8*/ meltfnum[7] =
	(melt_get_int
	 ((melt_ptr_t) ( /*_.REFERENCED_VALUE__V24*/ meltfptr[21])));;
      /*^compute */
   /*_#I__L9*/ meltfnum[8] =
	(( /*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5]) <
	 ( /*_#GET_INT__L8*/ meltfnum[7]));;
      MELT_LOCATION ("warmelt-outobj.melt:2836:/ cond");
      /*cond */ if ( /*_#I__L9*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V25*/ meltfptr[24] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-outobj.melt:2836:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check limited implbuf"),
				  ("warmelt-outobj.melt")
				  ? ("warmelt-outobj.melt") : __FILE__,
				  (2836) ? (2836) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V25*/ meltfptr[24] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[20] = /*_.IFELSE___V25*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-outobj.melt:2836:/ clear");
	     /*clear *//*_#STRBUF_USEDLENGTH__L7*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.REFERENCED_VALUE__V24*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_#I__L9*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V25*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[20] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V9*/ meltfptr[8] = /*_.IFCPP___V23*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-outobj.melt:2810:/ clear");
	   /*clear *//*_.BODYL__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.EPIL__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_#I__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.BOXDEPTHP1__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.COMS__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.COMSTR__V17*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L4*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V18*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#IS_LIST__L5*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.IF___V20*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-outobj.melt:2807:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V9*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-outobj.melt:2807:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.OBI_LOC__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.OUTPUT_LOCATION__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V9*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("OUTPUCOD_OBJCOMMENTEDBLOCK", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_59_warmelt_outobj_OUTPUCOD_OBJCOMMENTEDBLOCK */



/**** end of warmelt-outobj+02.c ****/
