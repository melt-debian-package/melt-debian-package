/** GENERATED MELT DESCRIPTOR FILE meltbuild-sources/warmelt-outobj+meltdesc.c 
** NEVER EDIT OR MOVE THIS, IT IS GENERATED & PARSED! **/
/* These identifiers are generated in warmelt-outobj.melt 
 & handled in melt-runtime.c carefully. */

	     #ifdef __cplusplus
	     /* explicitly declare as extern "C" our dlsym-ed symbols */
	     extern "C" const char melt_versionmeltstr[]	    ;
	     extern "C" const char melt_genversionstr[]		    ;
	     extern "C" const char melt_modulename[]		    ;
	     extern "C" const char melt_modulerealpath[]	    ;
	     extern "C" const char melt_prepromd5meltrun[]	    ;
	     extern "C" const char melt_primaryhexmd5[]		    ;
	     extern "C" const char* const melt_secondaryhexmd5tab[] ;
	     extern "C" const int melt_lastsecfileindex		    ;
	     extern "C" const char melt_cumulated_hexmd5[]	    ;

	     extern "C" {
	     #endif /*__cplusplus */
	     
/* version of the GCC compiler & MELT runtime generating this */
const char melt_genversionstr[]="4.8.0 20121010 (experimental) [melt-branch revision 192289] MELT_0\
.9.7-rc4"

	     #ifdef __cplusplus
	     " (in C++)"
	     #else
	     " (in C)"
	     #endif
					;
	     
const char melt_versionmeltstr[]="0.9.7-rc4 [melt-branch_revision_192289]";

/* source name & real path of the module */
/*MELTMODULENAME meltbuild-sources/warmelt-outobj */
const char melt_modulename[]="warmelt-outobj";
const char melt_modulerealpath[]="/usr/local/libexec/gcc-melt/gcc/x86_64-unknown-linux-gnu/4.8.0/melt\
-modules/0.9.7-rc4/warmelt-outobj";

/* hash of preprocessed melt-run.h generating this */
const char melt_prepromd5meltrun[]="f66a30d9bc2c10de00b1532482ad91ed";
/* hexmd5checksum of primary C file */
const char melt_primaryhexmd5[]="f11f9b0bf0b64d13ed61397a1e4d432c";

/* hexmd5checksum of secondary C files */
const char* const melt_secondaryhexmd5tab[]={
 /*nosecfile*/ (const char*)0,
 /*sechexmd5checksum meltbuild-sources/warmelt-outobj+01.c #1 */ "73ee2a953e41a402e1378fe957dc0a46",
 /*sechexmd5checksum meltbuild-sources/warmelt-outobj+02.c #2 */ "8a75e629aed59638bdc7704168e17942",
 /*sechexmd5checksum meltbuild-sources/warmelt-outobj+03.c #3 */ "536159cd2af33ba52420220f7e9b38cb",
 /*sechexmd5checksum meltbuild-sources/warmelt-outobj+04.c #4 */ "930f45ce85e2f21982b35765ab9d6f78",
 /*sechexmd5checksum meltbuild-sources/warmelt-outobj+05.c #5 */ "a58268f8bb6de01c49bd4623638802bf",
 /*nosecfile*/ (const char*)0,
 (const char*)0 };

/* last index of secondary files */
const int melt_lastsecfileindex=5;

/* cumulated checksum of primary & secondary files */
const char melt_cumulated_hexmd5[]="1f0ccb716eae60bdd32304454eb7ab4c" ;

/* include the timestamp file */
#define meltmod_warmelt_outobj_mds__1f0ccb716eae60bdd32304454eb7ab4c 1
#include "warmelt-outobj+melttime.h"
	 

		 #ifdef __cplusplus
		 }	  /* end extern C descriptor */
		 #endif /*__cplusplus */
		 
/* end of melt descriptor file */
