/* GCC MELT GENERATED FILE warmelt-genobj+03.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #3 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f3[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-genobj+03.c declarations ****/


/***************************************************
***
    Copyright 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_genobj_MAKE_OBJLOCATEDEXP (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_genobj_MAKE_OBJCOMPUTE (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_genobj_MAKE_OBJEXPANDPUREVAL (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_genobj_COMPILOBJ_CATCHALL_NREP (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_genobj_PUTOBJDEST_CATCHALL_OBJCODE (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_genobj_PUTOBJDEST_CATCHALL_ANYDISCR (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_genobj_GETCTYPE_OBJVALUE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_genobj_GECTYP_OBJNIL (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_genobj_GECTYP_OBJINITSTRING (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_genobj_DBGOUT_ROUTINEOBJ (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_genobj_PUTOBJDEST_OBJDESTINSTR (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_genobj_PUTOBJDEST_OBJFINALRETURN (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_genobj_VARIADIC_IDSTR (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_genobj_VARIADIC_INDEX_IDSTR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_genobj_VARIADIC_LENGTH_IDSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_genobj_COMPILE2OBJ_PROCEDURE (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_genobj_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_genobj_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_genobj_APPEND_COMMENT (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_genobj_APPEND_COMMENTCONST (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_genobj_COMPILE2OBJ_INITPROC (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_genobj_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_genobj_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_genobj_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_genobj_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_genobj_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_genobj_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_genobj_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_genobj_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_genobj_COMPILE2OBJ_INITEXTENDPROC (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_genobj_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_genobj_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_genobj_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_genobj_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_genobj_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_genobj_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_genobj_GET_FREE_OBJLOCPTR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_genobj_GET_FREE_OBJLOCLONG (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_genobj_GET_FREE_OBJLOCTYPED (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_genobj_DISPOSE_OBJLOC (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_genobj_DISPOSE_BND_OBJ (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_genobj_COMPILOBJ_NREP_CHUNK (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_genobj_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_genobj_GETCTYPE_TYPEXPR (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_genobj_COMPILOBJ_NREP_NIL (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_genobj_COMPILOBJ_NREP_COMMENT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_genobj_COMPILOBJ_NREP_LOCSYMOCC (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_genobj_COMPILOBJ_NREP_CLOSEDOCC (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_genobj_COMPILOBJ_NREP_CONSTOCC (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_genobj_COMPILOBJ_NREP_IMPORTEDVAL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_genobj_COMPILOBJ_NREP_LITERALVALUE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_genobj_COMPILOBJ_NREP_DEFINEDCONSTANT (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_genobj_COMPILOBJ_NREP_QUASICONSTANT (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_genobj_COMPILOBJ_NREP_QUASICONST_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_genobj_COMPILOBJ_NREP_FOREVER (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_genobj_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_genobj_COMPILOBJ_NREP_EXIT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_genobj_COMPILOBJ_NREP_AGAIN (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_genobj_COMPILOBJ_DISCRANY (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_genobj_COMPILOBJ_NREP_LET (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_genobj_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_genobj_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_genobj_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_genobj_FAIL_COMPILETRECFILL (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_genobj_COMPILETREC_LAMBDA (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_genobj_COMPILETREC_TUPLE (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_genobj_COMPILETREC_PAIR (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_genobj_COMPILETREC_LIST (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_genobj_COMPILETREC_INSTANCE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_genobj_COMPILOBJ_NREP_LETREC (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_genobj_COMPILOBJ_NREP_CITERATION (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_genobj_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_genobj_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_genobj_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_genobj_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_genobj_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_genobj_COMPILOBJ_NREP_SETQ (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_genobj_COMPILOBJ_NREP_PROGN (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_genobj_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_genobj_COMPILOBJ_NREP_MULTACC (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_genobj_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_genobj_COMPILOBJ_NREP_FIELDACC (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_genobj_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_genobj_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_genobj_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_genobj_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_genobj_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_genobj_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_genobj_LAMBDA___35__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_genobj_LAMBDA___36__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_genobj_LAMBDA___37__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_genobj_LAMBDA___38__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_genobj_LAMBDA___39__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_genobj_LAMBDA___40__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_genobj_LAMBDA___41__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_genobj_PUTOBJDEST_STRING (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_genobj_PUTOBJDEST_NULL (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_genobj_COMPILOBJ_NREP_IF (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_genobj_COMPILOBJ_NREP_IFISA (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_genobj_COMPILOBJ_NREP_IFTUPLESIZED (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_genobj_COMPILOBJ_NREP_IFVARIADIC (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_genobj_GETCTYPE_IFVARIADIC (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_genobj_COMPILOBJ_NREP_VARIADIC_ARGUMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_genobj_COMPILOBJ_NREP_CONSUMEVARIADIC (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    *meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_genobj_PUTOBJDEST_OBJCOND (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_genobj_COMPILOBJ_NREP_CPPIF (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_genobj_PUTOBJDEST_OBJCPPIF (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_genobj_COMPILOBJ_NREP_RETURN (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_genobj_LAMBDA___42__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_genobj_COMPILOBJ_NREP_LAMBDA (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_genobj_LAMBDA___43__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_genobj_COMPILOBJ_NREP_MAKEINST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_genobj_LAMBDA___44__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_genobj_COMPILOBJ_ROUTPROC (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_genobj_COMPILOBJ_PREDEF (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_genobj_COMPIL_DATA_AND_SLOTS_FILL (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_genobj_LAMBDA___45__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_genobj_LAMBDA___46__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_genobj_LAMBDA___47__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_genobj_DISPOSE_DLOCBIND_AFTER_DATA_AND_SLOTS_FILL
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_genobj_LAMBDA___48__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_genobj_COMPILOBJ_DATASYMBOL (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_146_warmelt_genobj_COMPILOBJ_DATAINSTANCE (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_147_warmelt_genobj_COMPILOBJ_DATATUPLE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_148_warmelt_genobj_LAMBDA___49__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_149_warmelt_genobj_LAMBDA___50__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_150_warmelt_genobj_COMPILOBJ_DATASTRING (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_151_warmelt_genobj_COMPILOBJ_DATABOXEDINTEGER (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_152_warmelt_genobj_COMPILOBJ_DATACLOSURE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_153_warmelt_genobj_LAMBDA___51__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_154_warmelt_genobj_COMPILOBJ_DATAROUTINE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_155_warmelt_genobj_LAMBDA___52__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_156_warmelt_genobj_COMPILOBJ_QUASIDATA_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_157_warmelt_genobj_COMPILOBJ_QUASIDATA_PARENT_MODULE_ENVIRONMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_158_warmelt_genobj_COMPILOBJ_NREP_STORE_PREDEFINED (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_159_warmelt_genobj_COMPILOBJ_NREP_UPDATE_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_160_warmelt_genobj_LAMBDA___53__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_161_warmelt_genobj_COMPILOBJ_NREP_CHECK_RUNNING_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_162_warmelt_genobj_LAMBDA___54__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_163_warmelt_genobj_COMPILTST_ANYTESTER (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_164_warmelt_genobj_COMPILOBJ_NREP_MATCH (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_165_warmelt_genobj_LAMBDA___55__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_166_warmelt_genobj_COMPILOBJ_NREP_ALTMATCH (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_167_warmelt_genobj_LAMBDA___56__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_168_warmelt_genobj_COMPILOBJ_NREP_MATCHLABEL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_169_warmelt_genobj_COMPILOBJ_NREP_MATCHFLAG (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_170_warmelt_genobj_COMPILOBJ_NREP_MATCHDATAINIT (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_171_warmelt_genobj_COMPILOBJ_NREP_MATCHEDATA (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_172_warmelt_genobj_COMPILOBJ_NREP_MATCHJUMP (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_173_warmelt_genobj_NORMTESTER_LABELINSTR (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_174_warmelt_genobj_NORMTESTER_GOTOINSTR (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_175_warmelt_genobj_ENDMATCH_GOTOINSTR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_176_warmelt_genobj_TESTMATCH_GOTOINSTR (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_177_warmelt_genobj_NORMTESTER_FREE_OBJLOC_LIST (meltclosure_ptr_t
							 meltclosp_,
							 melt_ptr_t
							 meltfirstargp_,
							 const
							 melt_argdescr_cell_t
							 meltxargdescr_[],
							 union meltparam_un
							 *meltxargtab_,
							 const
							 melt_argdescr_cell_t
							 meltxresdescr_[],
							 union meltparam_un
							 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_178_warmelt_genobj_LAMBDA___57__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_179_warmelt_genobj_COMPILTST_NORMTESTER_ANY (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_180_warmelt_genobj_COMPILTST_NORMTESTER_MATCHER (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_181_warmelt_genobj_COMPILTST_NORMTESTER_INSTANCE (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_182_warmelt_genobj_COMPILTST_NORMTESTER_TUPLE (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_183_warmelt_genobj_COMPILTST_NORMTESTER_SAME (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_184_warmelt_genobj_COMPILTST_NORMTESTER_SUCCESS (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_185_warmelt_genobj_COMPILTST_NORMTESTER_ORCLEAR (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_186_warmelt_genobj_COMPILTST_NORMTESTER_ORTRANSMIT (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un
							     *meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un
							     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_187_warmelt_genobj_COMPILTST_NORMTESTER_DISJUNCTION
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_188_warmelt_genobj_COMPILMATCHER_CMATCHER (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_189_warmelt_genobj_LAMBDA___58__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_190_warmelt_genobj_LAMBDA___59__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_191_warmelt_genobj_LAMBDA___60__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_192_warmelt_genobj_LAMBDA___61__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_193_warmelt_genobj_COMPILMATCHER_FUNMATCHER (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un
						      *meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un
						      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_194_warmelt_genobj_LAMBDA___62__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_195_warmelt_genobj_LAMBDA___63__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_genobj__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_genobj__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_genobj__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_genobj__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_0 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_1 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_2 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_3 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_4 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_5 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_6 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_7 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_8 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_9 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_10 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_11 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_12 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_13 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_14 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_15 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_16 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_17 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_18 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_19 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_20 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_21 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_22 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_23 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_24 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_25 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_26 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_27 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_28 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_29 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_30 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_31 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_32 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_33 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_34 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_35 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_36 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_37 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_38 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_39 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_40 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_41 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_42 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_43 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_44 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__initialmeltchunk_45 (struct
					      frame_melt_start_this_module_st
					      *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_genobj__forward_or_mark_module_start_frame (struct
							     melt_callframe_st
							     *fp,
							     int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_genobj__forward_or_mark_module_start_frame



/**** warmelt-genobj+03.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_genobj_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_83_warmelt_genobj_LAMBDA___29___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_83_warmelt_genobj_LAMBDA___29___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_83_warmelt_genobj_LAMBDA___29__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_83_warmelt_genobj_LAMBDA___29___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_83_warmelt_genobj_LAMBDA___29__ nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
  MELT_LOCATION ("warmelt-genobj.melt:3331:/ block");
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:3332:/ quasiblock");


 /*_.MAKE_STRINGCONST__V3*/ meltfptr[2] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[0])),
	("/*fieldacc*/(melt_field_object((melt_ptr_t)(")));;
    /*^compute */
 /*_.MAKE_STRINGCONST__V4*/ meltfptr[3] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[0])), ("),")));;
    /*^compute */
 /*_.MAKE_STRINGCONST__V5*/ meltfptr[4] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t)
	(( /*!DISCR_VERBATIM_STRING */ meltfrout->tabval[0])), ("))")));;
    MELT_LOCATION ("warmelt-genobj.melt:3334:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (5) rtup_0__TUPLREC__x6;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x6 */
 /*_.TUPLREC___V7*/ meltfptr[6] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x6;
      meltletrec_1_ptr->rtup_0__TUPLREC__x6.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x6.nbval = 5;


      /*^putuple */
      /*putupl#15 */
      melt_assertmsg ("putupl [:3334] #15 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V7*/ meltfptr[6])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3334] #15 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V7*/
					      meltfptr[6]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V7*/ meltfptr[6]))->tabval[0] =
	(melt_ptr_t) ( /*_.MAKE_STRINGCONST__V3*/ meltfptr[2]);
      ;
      /*^putuple */
      /*putupl#16 */
      melt_assertmsg ("putupl [:3334] #16 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V7*/ meltfptr[6])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3334] #16 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V7*/
					      meltfptr[6]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V7*/ meltfptr[6]))->tabval[1] =
	(melt_ptr_t) (( /*~COBJ */ meltfclos->tabval[0]));
      ;
      /*^putuple */
      /*putupl#17 */
      melt_assertmsg ("putupl [:3334] #17 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V7*/ meltfptr[6])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3334] #17 checkoff",
		      (2 >= 0
		       && 2 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V7*/
					      meltfptr[6]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V7*/ meltfptr[6]))->tabval[2] =
	(melt_ptr_t) ( /*_.MAKE_STRINGCONST__V4*/ meltfptr[3]);
      ;
      /*^putuple */
      /*putupl#18 */
      melt_assertmsg ("putupl [:3334] #18 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V7*/ meltfptr[6])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3334] #18 checkoff",
		      (3 >= 0
		       && 3 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V7*/
					      meltfptr[6]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V7*/ meltfptr[6]))->tabval[3] =
	(melt_ptr_t) (( /*~BOXFLDOFF */ meltfclos->tabval[1]));
      ;
      /*^putuple */
      /*putupl#19 */
      melt_assertmsg ("putupl [:3334] #19 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V7*/ meltfptr[6])) ==
		      MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:3334] #19 checkoff",
		      (4 >= 0
		       && 4 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V7*/
					      meltfptr[6]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V7*/ meltfptr[6]))->tabval[4] =
	(melt_ptr_t) ( /*_.MAKE_STRINGCONST__V5*/ meltfptr[4]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V7*/ meltfptr[6]);
      ;
      /*_.TCONT__V6*/ meltfptr[5] = /*_.TUPLREC___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3334:/ clear");
	    /*clear *//*_.TUPLREC___V7*/ meltfptr[6] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V7*/ meltfptr[6] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3342:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJEXPV */ meltfrout->
					     tabval[1])), (2),
			      "CLASS_OBJEXPV");
  /*_.INST__V9*/ meltfptr[8] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V9*/ meltfptr[8])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V9*/ meltfptr[8]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[2])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBX_CONT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V9*/ meltfptr[8])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V9*/ meltfptr[8]), (1),
			  ( /*_.TCONT__V6*/ meltfptr[5]), "OBX_CONT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V9*/ meltfptr[8],
				  "newly made instance");
    ;
    /*_.RES__V8*/ meltfptr[6] = /*_.INST__V9*/ meltfptr[8];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3346:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3346:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3346:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3346;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_fieldacc makecompute res=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V8*/ meltfptr[6];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3346:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3346:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[9] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3346:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V2*/ meltfptr[1] = /*_.RES__V8*/ meltfptr[6];;

    MELT_LOCATION ("warmelt-genobj.melt:3332:/ clear");
	   /*clear *//*_.MAKE_STRINGCONST__V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_STRINGCONST__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_STRINGCONST__V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.TCONT__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.RES__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3331:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3331:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V2*/ meltfptr[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_83_warmelt_genobj_LAMBDA___29___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_83_warmelt_genobj_LAMBDA___29__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD (meltclosure_ptr_t
							    meltclosp_,
							    melt_ptr_t
							    meltfirstargp_,
							    const
							    melt_argdescr_cell_t
							    meltxargdescr_[],
							    union meltparam_un
							    * meltxargtab_,
							    const
							    melt_argdescr_cell_t
							    meltxresdescr_[],
							    union meltparam_un
							    * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 20
    melt_ptr_t mcfr_varptr[20];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 20; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD nbval 20*/
  meltfram__.mcfr_nbvar = 20 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_UNSAFE_GET_FIELD", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3372:/ getarg");
 /*_.NUGF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3373:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NUGF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_UNSAFE_GET_FIELD */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3373:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3373:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nugf"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3373) ? (3373) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3373:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3374:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3374:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3374:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3374) ? (3374) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3374:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3375:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3375:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3375:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3375;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_unsafe_get_field nugf=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NUGF__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3375:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3375:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3375:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3376:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NUGF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3377:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NUGF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NUGET_OBJ");
  /*_.NOBJ__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3378:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NUGF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NUGET_FIELD");
  /*_.NFIELD__V15*/ meltfptr[14] = slot;
    };
    ;
 /*_.DLIST__V16*/ meltfptr[15] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3380:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OOBJ__V17*/ meltfptr[16] =
	meltgc_send ((melt_ptr_t) ( /*_.NOBJ__V14*/ meltfptr[13]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3381:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJGETSLOT */
					     meltfrout->tabval[5])), (4),
			      "CLASS_OBJGETSLOT");
  /*_.INST__V19*/ meltfptr[18] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (0),
			  ( /*_.LOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (1),
			  ( /*_.DLIST__V16*/ meltfptr[15]), "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OGETSL_OBJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (2),
			  ( /*_.OOBJ__V17*/ meltfptr[16]), "OGETSL_OBJ");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OGETSL_FIELD",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (3),
			  ( /*_.NFIELD__V15*/ meltfptr[14]), "OGETSL_FIELD");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V19*/ meltfptr[18],
				  "newly made instance");
    ;
    /*_.OGETSLOT__V18*/ meltfptr[17] = /*_.INST__V19*/ meltfptr[18];;
    MELT_LOCATION ("warmelt-genobj.melt:3387:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OGETSLOT__V18*/ meltfptr[17];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3387:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V12*/ meltfptr[8] = /*_.RETURN___V20*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-genobj.melt:3376:/ clear");
	   /*clear *//*_.LOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NOBJ__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NFIELD__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.DLIST__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OOBJ__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OGETSLOT__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V20*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3372:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3372:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_UNSAFE_GET_FIELD", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_84_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_GET_FIELD */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS (meltclosure_ptr_t
							     meltclosp_,
							     melt_ptr_t
							     meltfirstargp_,
							     const
							     melt_argdescr_cell_t
							     meltxargdescr_[],
							     union
							     meltparam_un *
							     meltxargtab_,
							     const
							     melt_argdescr_cell_t
							     meltxresdescr_[],
							     union
							     meltparam_un *
							     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 28
    melt_ptr_t mcfr_varptr[28];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 28; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS nbval 28*/
  meltfram__.mcfr_nbvar = 28 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_UNSAFE_PUT_FIELDS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3394:/ getarg");
 /*_.NUPF__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3395:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NUPF__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_UNSAFE_PUT_FIELDS */ meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3395:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3395:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nupf"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3395) ? (3395) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3395:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3396:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3396:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3396:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3396) ? (3396) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3396:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3397:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3397:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3397:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3397;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_unsafe_put_fields nupf=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NUPF__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3397:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3397:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3397:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3398:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NUPF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3399:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NUPF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NUPUT_OBJ");
  /*_.NOBJ__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3400:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NUPF__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NUPUT_FIELDS");
  /*_.NFLDASS__V15*/ meltfptr[14] = slot;
    };
    ;
 /*_.OBODL__V16*/ meltfptr[15] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
    /*^compute */
 /*_.OEPIL__V17*/ meltfptr[16] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3403:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPLAINBLOCK */
					     meltfrout->tabval[4])), (3),
			      "CLASS_OBJPLAINBLOCK");
  /*_.INST__V19*/ meltfptr[18] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (0),
			  ( /*_.LOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_BODYL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (1),
			  ( /*_.OBODL__V16*/ meltfptr[15]), "OBLO_BODYL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_EPIL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (2),
			  ( /*_.OEPIL__V17*/ meltfptr[16]), "OBLO_EPIL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V19*/ meltfptr[18],
				  "newly made instance");
    ;
    /*_.OBLOCK__V18*/ meltfptr[17] = /*_.INST__V19*/ meltfptr[18];;
    MELT_LOCATION ("warmelt-genobj.melt:3407:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OOBJ__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.NOBJ__V14*/ meltfptr[13]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[5])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3411:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V22*/ meltfptr[21] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_13 */ meltfrout->
						tabval[13])), (4));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V22*/ meltfptr[21])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V22*/ meltfptr[21])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V22*/ meltfptr[21])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V22*/ meltfptr[21])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V22*/ meltfptr[21])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V22*/ meltfptr[21])->tabval[1] =
      (melt_ptr_t) ( /*_.LOC__V13*/ meltfptr[9]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V22*/ meltfptr[21])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V22*/ meltfptr[21])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V22*/ meltfptr[21])->tabval[2] =
      (melt_ptr_t) ( /*_.OOBJ__V20*/ meltfptr[19]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V22*/ meltfptr[21])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 3 >= 0
		    && 3 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V22*/ meltfptr[21])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V22*/ meltfptr[21])->tabval[3] =
      (melt_ptr_t) ( /*_.OBODL__V16*/ meltfptr[15]);
    ;
    /*_.LAMBDA___V21*/ meltfptr[20] = /*_.LAMBDA___V22*/ meltfptr[21];;
    MELT_LOCATION ("warmelt-genobj.melt:3409:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V21*/ meltfptr[20];
      /*_.MULTIPLE_EVERY__V23*/ meltfptr[22] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.NFLDASS__V15*/ meltfptr[14]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3430:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJTOUCH */ meltfrout->
					     tabval[14])), (3),
			      "CLASS_OBJTOUCH");
  /*_.INST__V25*/ meltfptr[24] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V25*/ meltfptr[24]), (0),
			  ( /*_.LOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OTOUCH_VAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V25*/ meltfptr[24]), (1),
			  ( /*_.OOBJ__V20*/ meltfptr[19]), "OTOUCH_VAL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V25*/ meltfptr[24],
				  "newly made instance");
    ;
    /*_.INST___V24*/ meltfptr[23] = /*_.INST__V25*/ meltfptr[24];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3430:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OBODL__V16*/ meltfptr[15]),
			  (melt_ptr_t) ( /*_.INST___V24*/ meltfptr[23]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3434:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJDBGTRACEWRITEOBJ */
					     meltfrout->tabval[15])), (3),
			      "CLASS_OBJDBGTRACEWRITEOBJ");
  /*_.INST__V27*/ meltfptr[26] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (0),
			  ( /*_.LOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDTW_WRITTENOBJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (1),
			  ( /*_.OOBJ__V20*/ meltfptr[19]),
			  "OBDTW_WRITTENOBJ");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDTW_MESSAGE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (2),
			  (( /*!konst_16 */ meltfrout->tabval[16])),
			  "OBDTW_MESSAGE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V27*/ meltfptr[26],
				  "newly made instance");
    ;
    /*_.INST___V26*/ meltfptr[25] = /*_.INST__V27*/ meltfptr[26];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3434:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OBODL__V16*/ meltfptr[15]),
			  (melt_ptr_t) ( /*_.INST___V26*/ meltfptr[25]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3439:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OBODL__V16*/ meltfptr[15]),
			  (melt_ptr_t) ( /*_.OOBJ__V20*/ meltfptr[19]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3440:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OBLOCK__V18*/ meltfptr[17];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3440:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V12*/ meltfptr[8] = /*_.RETURN___V28*/ meltfptr[27];;

    MELT_LOCATION ("warmelt-genobj.melt:3398:/ clear");
	   /*clear *//*_.LOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NOBJ__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NFLDASS__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OBODL__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OEPIL__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OBLOCK__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OOBJ__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.INST___V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.INST___V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V28*/ meltfptr[27] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3394:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3394:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_UNSAFE_PUT_FIELDS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_85_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_PUT_FIELDS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_genobj_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_86_warmelt_genobj_LAMBDA___30___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_86_warmelt_genobj_LAMBDA___30___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_86_warmelt_genobj_LAMBDA___30__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_86_warmelt_genobj_LAMBDA___30___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_86_warmelt_genobj_LAMBDA___30__ nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3411:/ getarg");
 /*_.NFA__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3412:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NFA__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_FIELDASSIGN */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3412:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3412:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nfa"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3412) ? (3412) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3412:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3413:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NFA__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.ALOC__V5*/ meltfptr[3] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3414:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NFA__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NFLA_FIELD");
  /*_.AFIELD__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3415:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NFA__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NFLA_VAL");
  /*_.AVAL__V7*/ meltfptr[6] = slot;
    };
    ;
 /*_#AOFF__L3*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.AFIELD__V6*/ meltfptr[5])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3418:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.AFIELD__V6*/ meltfptr[5]),
			     (melt_ptr_t) (( /*!CLASS_FIELD */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3418:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3418:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check afield"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3418) ? (3418) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[7] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3418:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3419:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:3420:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.OVAL__V10*/ meltfptr[8] =
	meltgc_send ((melt_ptr_t) ( /*_.AVAL__V7*/ meltfptr[6]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[2])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3421:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3422:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.ALOC__V5*/ meltfptr[3])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V11*/ meltfptr[10] = /*_.ALOC__V5*/ meltfptr[3];;
      }
    else
      {
	MELT_LOCATION ("warmelt-genobj.melt:3422:/ cond.else");

	/*_.IFELSE___V11*/ meltfptr[10] = ( /*~LOC */ meltfclos->tabval[1]);;
      }
    ;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V12*/ meltfptr[11] =
      (meltgc_new_int
       ((meltobject_ptr_t) (( /*!DISCR_INTEGER */ meltfrout->tabval[4])),
	( /*_#AOFF__L3*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-genobj.melt:3421:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPUTSLOT */
					     meltfrout->tabval[3])), (5),
			      "CLASS_OBJPUTSLOT");
  /*_.INST__V14*/ meltfptr[13] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V14*/ meltfptr[13])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V14*/ meltfptr[13]), (0),
			  ( /*_.IFELSE___V11*/ meltfptr[10]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OSLOT_ODATA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V14*/ meltfptr[13])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V14*/ meltfptr[13]), (1),
			  (( /*~OOBJ */ meltfclos->tabval[2])),
			  "OSLOT_ODATA");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OSLOT_FIELD",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V14*/ meltfptr[13])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V14*/ meltfptr[13]), (3),
			  ( /*_.AFIELD__V6*/ meltfptr[5]), "OSLOT_FIELD");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OSLOT_OFFSET",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V14*/ meltfptr[13])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V14*/ meltfptr[13]), (2),
			  ( /*_.MAKE_INTEGERBOX__V12*/ meltfptr[11]),
			  "OSLOT_OFFSET");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OSLOT_VALUE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V14*/ meltfptr[13])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V14*/ meltfptr[13]), (4),
			  ( /*_.OVAL__V10*/ meltfptr[8]), "OSLOT_VALUE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V14*/ meltfptr[13],
				  "newly made instance");
    ;
    /*_.OPUF__V13*/ meltfptr[12] = /*_.INST__V14*/ meltfptr[13];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3427:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L5*/ meltfnum[3] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OVAL__V10*/ meltfptr[8]),
			     (melt_ptr_t) (( /*!CLASS_NREP */ meltfrout->
					    tabval[5])));;
      /*^compute */
   /*_#NOT__L6*/ meltfnum[5] =
	(!( /*_#IS_A__L5*/ meltfnum[3]));;
      MELT_LOCATION ("warmelt-genobj.melt:3427:/ cond");
      /*cond */ if ( /*_#NOT__L6*/ meltfnum[5])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3427:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_nrep_unsafe_put_fields check oval not nrep"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (3427) ? (3427) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[14] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3427:/ clear");
	     /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L6*/ meltfnum[5] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3428:/ locexp");
      meltgc_append_list ((melt_ptr_t) (( /*~OBODL */ meltfclos->tabval[3])),
			  (melt_ptr_t) ( /*_.OPUF__V13*/ meltfptr[12]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:3419:/ clear");
	   /*clear *//*_.OVAL__V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.OPUF__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;

    MELT_LOCATION ("warmelt-genobj.melt:3413:/ clear");
	   /*clear *//*_.ALOC__V5*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.AFIELD__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.AVAL__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#AOFF__L3*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[7] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3411:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_86_warmelt_genobj_LAMBDA___30___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_86_warmelt_genobj_LAMBDA___30__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 18
    melt_ptr_t mcfr_varptr[18];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 18; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL nbval 18*/
  meltfram__.mcfr_nbvar = 18 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_CHECKSIGNAL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3446:/ getarg");
 /*_.NCHINT__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3447:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3447:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3447:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3447;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_checksignal nchint=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCHINT__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3447:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3447:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3447:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3448:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NCHINT__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_CHECKSIGNAL */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3448:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3448:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nchint"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3448) ? (3448) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3448:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3449:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:3450:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NCHINT__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3451:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCHECKSIGNAL */
					     meltfrout->tabval[2])), (1),
			      "CLASS_OBJCHECKSIGNAL");
  /*_.INST__V13*/ meltfptr[12] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V13*/ meltfptr[12])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V13*/ meltfptr[12]), (0),
			  ( /*_.NLOC__V11*/ meltfptr[10]), "OBI_LOC");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V13*/ meltfptr[12],
				  "newly made instance");
    ;
    /*_.OCHINT__V12*/ meltfptr[11] = /*_.INST__V13*/ meltfptr[12];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3454:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3454:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3454:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3454;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_checksignal ochint=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OCHINT__V12*/ meltfptr[11];
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V15*/ meltfptr[14] =
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3454:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V15*/ meltfptr[14] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3454:/ quasiblock");


      /*_.PROGN___V17*/ meltfptr[15] = /*_.IF___V15*/ meltfptr[14];;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[13] = /*_.PROGN___V17*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3454:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3455:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OCHINT__V12*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3455:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V10*/ meltfptr[5] = /*_.RETURN___V18*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-genobj.melt:3449:/ clear");
	   /*clear *//*_.NLOC__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.OCHINT__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V18*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3446:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V10*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3446:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[5] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_CHECKSIGNAL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_87_warmelt_genobj_COMPILOBJ_NREP_CHECKSIGNAL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT
  (meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_,
   const melt_argdescr_cell_t meltxargdescr_[],
   union meltparam_un * meltxargtab_,
   const melt_argdescr_cell_t meltxresdescr_[],
   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3462:/ getarg");
 /*_.NUNC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3463:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NUNC__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_UNSAFE_NTH_COMPONENT */ meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3463:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3463:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nunc"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3463) ? (3463) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3463:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3464:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3464:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3464:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3464) ? (3464) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3464:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3465:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3465:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3465:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3465;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_unsafe_nth_component nunc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NUNC__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3465:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3465:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3465:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3466:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NUNC__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.NLOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3467:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NUNC__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NUNTH_TUPLE");
  /*_.NTUP__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3468:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NUNC__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NUNTH_INDEX");
  /*_.NIDX__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3469:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OTUP__V16*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.NTUP__V14*/ meltfptr[13]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3470:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OIDX__V17*/ meltfptr[16] =
	meltgc_send ((melt_ptr_t) ( /*_.NIDX__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3471:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[6];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!CTYPE_VALUE */ meltfrout->tabval[5]);
      /*^apply.arg */
      argtab[1].meltbp_cstring = "/*unsafenthcomp*/((meltmultiple_ptr_t)(";
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.OTUP__V16*/ meltfptr[15];
      /*^apply.arg */
      argtab[3].meltbp_cstring = "))->tabval[";
      /*^apply.arg */
      argtab[4].meltbp_aptr = (melt_ptr_t *) & /*_.OIDX__V17*/ meltfptr[16];
      /*^apply.arg */
      argtab[5].meltbp_cstring = "]";
      /*_.OUNC__V18*/ meltfptr[17] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MAKE_OBJCOMPUTE */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.NLOC__V13*/ meltfptr[9]),
		    (MELTBPARSTR_PTR MELTBPARSTR_CSTRING MELTBPARSTR_PTR
		     MELTBPARSTR_CSTRING MELTBPARSTR_PTR MELTBPARSTR_CSTRING
		     ""), argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3474:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3474:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3474:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[9];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3474;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_unsafe_nth_component otup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OTUP__V16*/ meltfptr[15];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " oidx=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OIDX__V17*/ meltfptr[16];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = " ounc=";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.OUNC__V18*/ meltfptr[17];
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V20*/ meltfptr[19] =
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3474:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V20*/ meltfptr[19] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3474:/ quasiblock");


      /*_.PROGN___V22*/ meltfptr[20] = /*_.IF___V20*/ meltfptr[19];;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[18] = /*_.PROGN___V22*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3474:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V20*/ meltfptr[19] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3475:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OUNC__V18*/ meltfptr[17];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3475:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V12*/ meltfptr[8] = /*_.RETURN___V23*/ meltfptr[19];;

    MELT_LOCATION ("warmelt-genobj.melt:3466:/ clear");
	   /*clear *//*_.NLOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NTUP__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NIDX__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OTUP__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OIDX__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.OUNC__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V23*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3462:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3462:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_88_warmelt_genobj_COMPILOBJ_NREP_UNSAFE_NTH_COMPONENT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 27
    melt_ptr_t mcfr_varptr[27];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 27; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY nbval 27*/
  meltfram__.mcfr_nbvar = 27 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_APPLY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3481:/ getarg");
 /*_.NAPP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3482:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NAPP__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_APPLY */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3482:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3482:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check napp"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3482) ? (3482) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3482:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3483:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3483:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3483:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3483) ? (3483) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3483:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3484:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3485:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NAPP_FUN");
  /*_.FUN__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3486:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NEXPR_ARGS");
  /*_.ARGS__V11*/ meltfptr[10] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3488:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3488:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3488:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3488;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_apply napp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NAPP__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V13*/ meltfptr[12] =
	      /*_.MELT_DEBUG_FUN__V14*/ meltfptr[13];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3488:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V14*/ meltfptr[13] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V13*/ meltfptr[12] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3488:/ quasiblock");


      /*_.PROGN___V15*/ meltfptr[13] = /*_.IF___V13*/ meltfptr[12];;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.PROGN___V15*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3488:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V13*/ meltfptr[12] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V15*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3489:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OCLOS__V17*/ meltfptr[13] =
	meltgc_send ((melt_ptr_t) ( /*_.FUN__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3492:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V19*/ meltfptr[18] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->
						tabval[7])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V19*/ meltfptr[18])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V19*/ meltfptr[18])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V19*/ meltfptr[18])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V18*/ meltfptr[17] = /*_.LAMBDA___V19*/ meltfptr[18];;
    MELT_LOCATION ("warmelt-genobj.melt:3490:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V18*/ meltfptr[17];
      /*_.OARGS__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.ARGS__V11*/ meltfptr[10]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_.DLIST__V21*/ meltfptr[20] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[8]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3499:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJAPPLY */ meltfrout->
					     tabval[9])), (4),
			      "CLASS_OBJAPPLY");
  /*_.INST__V23*/ meltfptr[22] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (0),
			  ( /*_.LOC__V9*/ meltfptr[8]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (1),
			  ( /*_.DLIST__V21*/ meltfptr[20]), "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBAPP_CLOS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (2),
			  ( /*_.OCLOS__V17*/ meltfptr[13]), "OBAPP_CLOS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBAPP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V23*/ meltfptr[22])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V23*/ meltfptr[22]), (3),
			  ( /*_.OARGS__V20*/ meltfptr[19]), "OBAPP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V23*/ meltfptr[22],
				  "newly made instance");
    ;
    /*_.OAPP__V22*/ meltfptr[21] = /*_.INST__V23*/ meltfptr[22];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3506:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3506:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3506:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3506;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_apply return oapp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OAPP__V22*/ meltfptr[21];
	      /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V25*/ meltfptr[24] =
	      /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3506:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V25*/ meltfptr[24] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3506:/ quasiblock");


      /*_.PROGN___V27*/ meltfptr[25] = /*_.IF___V25*/ meltfptr[24];;
      /*^compute */
      /*_.IFCPP___V24*/ meltfptr[23] = /*_.PROGN___V27*/ meltfptr[25];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3506:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V27*/ meltfptr[25] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V24*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V16*/ meltfptr[12] = /*_.OAPP__V22*/ meltfptr[21];;

    MELT_LOCATION ("warmelt-genobj.melt:3489:/ clear");
	   /*clear *//*_.OCLOS__V17*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.OARGS__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.DLIST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.OAPP__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V24*/ meltfptr[23] = 0;
    /*_.LET___V8*/ meltfptr[6] = /*_.LET___V16*/ meltfptr[12];;

    MELT_LOCATION ("warmelt-genobj.melt:3484:/ clear");
	   /*clear *//*_.LOC__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.FUN__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.ARGS__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LET___V16*/ meltfptr[12] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3481:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3481:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_APPLY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_89_warmelt_genobj_COMPILOBJ_NREP_APPLY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_genobj_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_90_warmelt_genobj_LAMBDA___31___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_90_warmelt_genobj_LAMBDA___31___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 6
    melt_ptr_t mcfr_varptr[6];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_90_warmelt_genobj_LAMBDA___31__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_90_warmelt_genobj_LAMBDA___31___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 6; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_90_warmelt_genobj_LAMBDA___31__ nbval 6*/
  meltfram__.mcfr_nbvar = 6 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3492:/ getarg");
 /*_.COMP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:3493:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.OCOMP__V4*/ meltfptr[3] =
	meltgc_send ((melt_ptr_t) ( /*_.COMP__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[0])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3494:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OCOMP__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_OBJINSTR */ meltfrout->
					    tabval[1])));;
      /*^compute */
   /*_#NOT__L3*/ meltfnum[2] =
	(!( /*_#IS_A__L2*/ meltfnum[1]));;
      MELT_LOCATION ("warmelt-genobj.melt:3494:/ cond");
      /*cond */ if ( /*_#NOT__L3*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3494:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_nrep_apply check ocomp not objinstr"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (3494) ? (3494) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3494:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L3*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V3*/ meltfptr[2] = /*_.OCOMP__V4*/ meltfptr[3];;

    MELT_LOCATION ("warmelt-genobj.melt:3493:/ clear");
	   /*clear *//*_.OCOMP__V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3492:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3492:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.LET___V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_90_warmelt_genobj_LAMBDA___31___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_90_warmelt_genobj_LAMBDA___31__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 50
    melt_ptr_t mcfr_varptr[50];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 50; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY nbval 50*/
  meltfram__.mcfr_nbvar = 50 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_MULTIAPPLY", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3512:/ getarg");
 /*_.NMAPP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3513:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NMAPP__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_MULTIAPPLY */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3513:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3513:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check napp"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3513) ? (3513) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3513:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3514:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3514:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3514:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3514) ? (3514) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3514:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3515:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3515:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3515:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3515;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_multiapply nmapp=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NMAPP__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3515:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3515:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3515:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3516:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3517:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NMULAPP_BINDINGS");
  /*_.RBINDS__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3518:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NAPP_FUN");
  /*_.FUN__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3519:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NEXPR_ARGS");
  /*_.ARGS__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3520:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMAPP__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NMULAPP_BODY");
  /*_.NBODY__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3521:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "GNCX_LOCMAP");
  /*_.LOCMAP__V18*/ meltfptr[17] = slot;
    };
    ;
 /*_#NBRES__L5*/ meltfnum[3] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.RBINDS__V14*/ meltfptr[13])));;
    MELT_LOCATION ("warmelt-genobj.melt:3526:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V20*/ meltfptr[19] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->
						tabval[7])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V20*/ meltfptr[19])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V20*/ meltfptr[19])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V20*/ meltfptr[19])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V20*/ meltfptr[19])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V20*/ meltfptr[19])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V20*/ meltfptr[19])->tabval[1] =
      (melt_ptr_t) ( /*_.LOCMAP__V18*/ meltfptr[17]);
    ;
    /*_.LAMBDA___V19*/ meltfptr[18] = /*_.LAMBDA___V20*/ meltfptr[19];;
    MELT_LOCATION ("warmelt-genobj.melt:3524:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V19*/ meltfptr[18];
      /*_.RESLOCS__V21*/ meltfptr[20] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.RBINDS__V14*/ meltfptr[13]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_.OBODL__V22*/ meltfptr[21] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[8]))));;
    /*^compute */
 /*_.OEPIL__V23*/ meltfptr[22] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[8]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3541:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPLAINBLOCK */
					     meltfrout->tabval[9])), (3),
			      "CLASS_OBJPLAINBLOCK");
  /*_.INST__V25*/ meltfptr[24] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V25*/ meltfptr[24]), (0),
			  ( /*_.LOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_BODYL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V25*/ meltfptr[24]), (1),
			  ( /*_.OBODL__V22*/ meltfptr[21]), "OBLO_BODYL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_EPIL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V25*/ meltfptr[24]), (2),
			  ( /*_.OEPIL__V23*/ meltfptr[22]), "OBLO_EPIL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V25*/ meltfptr[24],
				  "newly made instance");
    ;
    /*_.OBLOCK__V24*/ meltfptr[23] = /*_.INST__V25*/ meltfptr[24];;
    MELT_LOCATION ("warmelt-genobj.melt:3545:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L6*/ meltfnum[0] =
      (( /*_#NBRES__L5*/ meltfnum[3]) > (1));;
    MELT_LOCATION ("warmelt-genobj.melt:3545:/ cond");
    /*cond */ if ( /*_#I__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#I__L7*/ meltfnum[6] =
	    (( /*_#NBRES__L5*/ meltfnum[3]) - (1));;
	  /*^compute */
   /*_.MAKE_MULTIPLE__V27*/ meltfptr[26] =
	    (meltgc_new_multiple
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MULTIPLE */ meltfrout->tabval[10])),
	      ( /*_#I__L7*/ meltfnum[6])));;
	  /*^compute */
	  /*_.OXRES__V26*/ meltfptr[25] =
	    /*_.MAKE_MULTIPLE__V27*/ meltfptr[26];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:3545:/ clear");
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_MULTIPLE__V27*/ meltfptr[26] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.OXRES__V26*/ meltfptr[25] = NULL;;
      }
    ;
    /*^compute */
 /*_.FIRSTRES__V28*/ meltfptr[26] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.RESLOCS__V21*/ meltfptr[20]), (0)));;
    MELT_LOCATION ("warmelt-genobj.melt:3547:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OFUN__V29*/ meltfptr[28] =
	meltgc_send ((melt_ptr_t) ( /*_.FUN__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
				    tabval[11])), (MELTBPARSTR_PTR ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_.RESLIST__V30*/ meltfptr[29] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[8]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3550:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V32*/ meltfptr[31] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_13 */ meltfrout->
						tabval[13])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V32*/ meltfptr[31])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V32*/ meltfptr[31])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V32*/ meltfptr[31])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V31*/ meltfptr[30] = /*_.LAMBDA___V32*/ meltfptr[31];;
    MELT_LOCATION ("warmelt-genobj.melt:3549:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V31*/ meltfptr[30];
      /*_.OARGS__V33*/ meltfptr[32] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.ARGS__V16*/ meltfptr[15]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3552:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OBODY__V34*/ meltfptr[33] =
	meltgc_send ((melt_ptr_t) ( /*_.NBODY__V17*/ meltfptr[16]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
				    tabval[11])), (MELTBPARSTR_PTR ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3553:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3556:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.COMPILE_OBJ__V35*/ meltfptr[34] =
	meltgc_send ((melt_ptr_t) ( /*_.FUN__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->
				    tabval[11])), (MELTBPARSTR_PTR ""),
		     argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3553:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJMULTIAPPLY */
					     meltfrout->tabval[14])), (5),
			      "CLASS_OBJMULTIAPPLY");
  /*_.INST__V37*/ meltfptr[36] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V37*/ meltfptr[36])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V37*/ meltfptr[36]), (0),
			  ( /*_.LOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V37*/ meltfptr[36])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V37*/ meltfptr[36]), (1),
			  ( /*_.RESLIST__V30*/ meltfptr[29]),
			  "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBAPP_CLOS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V37*/ meltfptr[36])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V37*/ meltfptr[36]), (2),
			  ( /*_.COMPILE_OBJ__V35*/ meltfptr[34]),
			  "OBAPP_CLOS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBAPP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V37*/ meltfptr[36])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V37*/ meltfptr[36]), (3),
			  ( /*_.OARGS__V33*/ meltfptr[32]), "OBAPP_ARGS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBMULTAPP_XRES",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V37*/ meltfptr[36])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V37*/ meltfptr[36]), (4),
			  ( /*_.OXRES__V26*/ meltfptr[25]), "OBMULTAPP_XRES");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V37*/ meltfptr[36],
				  "newly made instance");
    ;
    /*_.OMAPP__V36*/ meltfptr[35] = /*_.INST__V37*/ meltfptr[36];;
    MELT_LOCATION ("warmelt-genobj.melt:3560:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.FIRSTRES__V28*/ meltfptr[26])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.RESLIST__V30*/ meltfptr[29]),
				(melt_ptr_t) ( /*_.FIRSTRES__V28*/
					      meltfptr[26]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3564:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V39*/ meltfptr[38] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_18 */ meltfrout->
						tabval[18])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V39*/ meltfptr[38])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V39*/ meltfptr[38])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V39*/ meltfptr[38])->tabval[0] =
      (melt_ptr_t) ( /*_.OBODL__V22*/ meltfptr[21]);
    ;
    /*_.LAMBDA___V38*/ meltfptr[37] = /*_.LAMBDA___V39*/ meltfptr[38];;
    MELT_LOCATION ("warmelt-genobj.melt:3562:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V38*/ meltfptr[37];
      /*_.MULTIPLE_EVERY__V40*/ meltfptr[39] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[15])),
		    (melt_ptr_t) ( /*_.OARGS__V33*/ meltfptr[32]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3568:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OBODL__V22*/ meltfptr[21]),
			  (melt_ptr_t) ( /*_.OMAPP__V36*/ meltfptr[35]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3570:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OBODL__V22*/ meltfptr[21]),
			  (melt_ptr_t) ( /*_.OBODY__V34*/ meltfptr[33]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3574:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V42*/ meltfptr[41] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_20 */ meltfrout->
						tabval[20])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V42*/ meltfptr[41])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V42*/ meltfptr[41])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V42*/ meltfptr[41])->tabval[0] =
      (melt_ptr_t) ( /*_.LOC__V13*/ meltfptr[9]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V42*/ meltfptr[41])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V42*/ meltfptr[41])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V42*/ meltfptr[41])->tabval[1] =
      (melt_ptr_t) ( /*_.OEPIL__V23*/ meltfptr[22]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V42*/ meltfptr[41])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V42*/ meltfptr[41])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V42*/ meltfptr[41])->tabval[2] =
      (melt_ptr_t) ( /*_.OXRES__V26*/ meltfptr[25]);
    ;
    /*_.LAMBDA___V41*/ meltfptr[40] = /*_.LAMBDA___V42*/ meltfptr[41];;
    MELT_LOCATION ("warmelt-genobj.melt:3572:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V41*/ meltfptr[40];
      /*_.MULTIPLE_EVERY__V43*/ meltfptr[42] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[15])),
		    (melt_ptr_t) ( /*_.RESLOCS__V21*/ meltfptr[20]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3585:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V45*/ meltfptr[44] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_22 */ meltfrout->
						tabval[22])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V45*/ meltfptr[44])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V45*/ meltfptr[44])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V45*/ meltfptr[44])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V44*/ meltfptr[43] = /*_.LAMBDA___V45*/ meltfptr[44];;
    MELT_LOCATION ("warmelt-genobj.melt:3583:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V44*/ meltfptr[43];
      /*_.MULTIPLE_EVERY__V46*/ meltfptr[45] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[15])),
		    (melt_ptr_t) ( /*_.RBINDS__V14*/ meltfptr[13]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3586:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3586:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3586:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3586;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_multiapply final oblock=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBLOCK__V24*/ meltfptr[23];
	      /*_.MELT_DEBUG_FUN__V49*/ meltfptr[48] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V48*/ meltfptr[47] =
	      /*_.MELT_DEBUG_FUN__V49*/ meltfptr[48];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3586:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V49*/ meltfptr[48] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V48*/ meltfptr[47] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3586:/ quasiblock");


      /*_.PROGN___V50*/ meltfptr[48] = /*_.IF___V48*/ meltfptr[47];;
      /*^compute */
      /*_.IFCPP___V47*/ meltfptr[46] = /*_.PROGN___V50*/ meltfptr[48];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3586:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V48*/ meltfptr[47] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V50*/ meltfptr[48] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V47*/ meltfptr[46] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V12*/ meltfptr[8] = /*_.OBLOCK__V24*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-genobj.melt:3516:/ clear");
	   /*clear *//*_.LOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.RBINDS__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.FUN__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.ARGS__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NBODY__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LOCMAP__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_#NBRES__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.RESLOCS__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.OBODL__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.OEPIL__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.OBLOCK__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OXRES__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.FIRSTRES__V28*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.OFUN__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.RESLIST__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.OARGS__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.OBODY__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.COMPILE_OBJ__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.OMAPP__V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V38*/ meltfptr[37] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V44*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V46*/ meltfptr[45] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V47*/ meltfptr[46] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3512:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3512:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_MULTIAPPLY", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_91_warmelt_genobj_COMPILOBJ_NREP_MULTIAPPLY */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_genobj_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_92_warmelt_genobj_LAMBDA___32___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_92_warmelt_genobj_LAMBDA___32___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_92_warmelt_genobj_LAMBDA___32__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_92_warmelt_genobj_LAMBDA___32___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_92_warmelt_genobj_LAMBDA___32__ nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3526:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3527:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3527:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3527:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_nrep_multiapply check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3527) ? (3527) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3527:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3529:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.BDER__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3530:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "FBIND_TYPE");
  /*_.CTY__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3531:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.BDER__V6*/ meltfptr[5];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.CTY__V7*/ meltfptr[6];
      /*_.OBVA__V8*/ meltfptr[7] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCTYPED */ meltfrout->tabval[1])),
		    (melt_ptr_t) (( /*~GCX */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3533:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CTY__V7*/ meltfptr[6]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:3533:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3533:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_nrep_multiapply check cty"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3533) ? (3533) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3533:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3536:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OBVA__V8*/ meltfptr[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				   (( /*~LOCMAP */ meltfclos->tabval[1])),
				   (meltobject_ptr_t) ( /*_.BIND__V2*/
						       meltfptr[1]),
				   (melt_ptr_t) ( /*_.OBVA__V8*/
						 meltfptr[7]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V5*/ meltfptr[3] = /*_.OBVA__V8*/ meltfptr[7];;

    MELT_LOCATION ("warmelt-genobj.melt:3529:/ clear");
	   /*clear *//*_.BDER__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.CTY__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OBVA__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3526:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3526:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_92_warmelt_genobj_LAMBDA___32___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_92_warmelt_genobj_LAMBDA___32__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_genobj_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_93_warmelt_genobj_LAMBDA___33___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_93_warmelt_genobj_LAMBDA___33___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_93_warmelt_genobj_LAMBDA___33__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_93_warmelt_genobj_LAMBDA___33___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_93_warmelt_genobj_LAMBDA___33__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3550:/ getarg");
 /*_.COMP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:3551:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.COMPILE_OBJ__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.COMP__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[0])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3550:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.COMPILE_OBJ__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3550:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.COMPILE_OBJ__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_93_warmelt_genobj_LAMBDA___33___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_93_warmelt_genobj_LAMBDA___33__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_genobj_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_94_warmelt_genobj_LAMBDA___34___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_94_warmelt_genobj_LAMBDA___34___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_94_warmelt_genobj_LAMBDA___34__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_94_warmelt_genobj_LAMBDA___34___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_94_warmelt_genobj_LAMBDA___34__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3564:/ getarg");
 /*_.OCURARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:3565:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OCURARG__V2*/ meltfptr[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#IS_NOT_A__L3*/ meltfnum[2] =
	    !melt_is_instance_of ((melt_ptr_t)
				  ( /*_.OCURARG__V2*/ meltfptr[1]),
				  (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
						 meltfrout->tabval[1])));;
	  MELT_LOCATION ("warmelt-genobj.melt:3565:/ cond");
	  /*cond */ if ( /*_#IS_NOT_A__L3*/ meltfnum[2])	/*then */
	    {
	      /*^cond.then */
	      /*_#OR___L4*/ meltfnum[3] = /*_#IS_NOT_A__L3*/ meltfnum[2];;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-genobj.melt:3565:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {

     /*_#IS_A__L5*/ meltfnum[4] =
		  melt_is_instance_of ((melt_ptr_t)
				       ( /*_.OCURARG__V2*/ meltfptr[1]),
				       (melt_ptr_t) (( /*!CLASS_OBJINSTR */
						      meltfrout->
						      tabval[0])));;
		/*^compute */
		/*_#OR___L4*/ meltfnum[3] = /*_#IS_A__L5*/ meltfnum[4];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:3565:/ clear");
	       /*clear *//*_#IS_A__L5*/ meltfnum[4] = 0;
	      }
	      ;
	    }
	  ;
	  /*_#IF___L2*/ meltfnum[1] = /*_#OR___L4*/ meltfnum[3];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:3565:/ clear");
	     /*clear *//*_#IS_NOT_A__L3*/ meltfnum[2] = 0;
	  /*^clear */
	     /*clear *//*_#OR___L4*/ meltfnum[3] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_#IF___L2*/ meltfnum[1] = 0;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3565:/ cond");
    /*cond */ if ( /*_#IF___L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3566:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				(( /*~OBODL */ meltfclos->tabval[0])),
				(melt_ptr_t) ( /*_.OCURARG__V2*/
					      meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-genobj.melt:3564:/ clear");
	   /*clear *//*_#IF___L2*/ meltfnum[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_94_warmelt_genobj_LAMBDA___34___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_94_warmelt_genobj_LAMBDA___34__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_genobj_LAMBDA___35__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_95_warmelt_genobj_LAMBDA___35___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_95_warmelt_genobj_LAMBDA___35___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_95_warmelt_genobj_LAMBDA___35__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_95_warmelt_genobj_LAMBDA___35___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_95_warmelt_genobj_LAMBDA___35__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3574:/ getarg");
 /*_.RLOC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:3575:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L2*/ meltfnum[1] =
      (( /*_#IX__L1*/ meltfnum[0]) > (0));;
    MELT_LOCATION ("warmelt-genobj.melt:3575:/ cond");
    /*cond */ if ( /*_#I__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:3576:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJCLEAR */
						   meltfrout->tabval[0])),
				    (2), "CLASS_OBJCLEAR");
    /*_.INST__V4*/ meltfptr[3] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBI_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V4*/ meltfptr[3])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V4*/ meltfptr[3]), (0),
				(( /*~LOC */ meltfclos->tabval[0])),
				"OBI_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OCLR_VLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V4*/ meltfptr[3])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V4*/ meltfptr[3]), (1),
				( /*_.RLOC__V2*/ meltfptr[1]), "OCLR_VLOC");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V4*/ meltfptr[3],
					"newly made instance");
	  ;
	  /*_.OCLEAR__V3*/ meltfptr[2] = /*_.INST__V4*/ meltfptr[3];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3579:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				(( /*~OEPIL */ meltfclos->tabval[1])),
				(melt_ptr_t) ( /*_.OCLEAR__V3*/ meltfptr[2]));
	  }
	  ;
   /*_#I__L3*/ meltfnum[2] =
	    (( /*_#IX__L1*/ meltfnum[0]) - (1));;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3580:/ locexp");
	    meltgc_multiple_put_nth ((melt_ptr_t)
				     (( /*~OXRES */ meltfclos->tabval[2])),
				     ( /*_#I__L3*/ meltfnum[2]),
				     (melt_ptr_t) ( /*_.RLOC__V2*/
						   meltfptr[1]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:3576:/ clear");
	     /*clear *//*_.OCLEAR__V3*/ meltfptr[2] = 0;
	  /*^clear */
	     /*clear *//*_#I__L3*/ meltfnum[2] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-genobj.melt:3574:/ clear");
	   /*clear *//*_#I__L2*/ meltfnum[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_95_warmelt_genobj_LAMBDA___35___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_95_warmelt_genobj_LAMBDA___35__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_genobj_LAMBDA___36__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_96_warmelt_genobj_LAMBDA___36___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_96_warmelt_genobj_LAMBDA___36___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_96_warmelt_genobj_LAMBDA___36__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_96_warmelt_genobj_LAMBDA___36___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_96_warmelt_genobj_LAMBDA___36__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3585:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.DISPOSE_BND_OBJ__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!DISPOSE_BND_OBJ */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.DISPOSE_BND_OBJ__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3585:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.DISPOSE_BND_OBJ__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_96_warmelt_genobj_LAMBDA___36___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_96_warmelt_genobj_LAMBDA___36__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 33
    melt_ptr_t mcfr_varptr[33];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 33; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND nbval 33*/
  meltfram__.mcfr_nbvar = 33 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_MSEND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3594:/ getarg");
 /*_.NSND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3595:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NSND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_MSEND */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3595:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3595:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nsnd"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3595) ? (3595) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3595:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3596:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3596:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3596:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3596) ? (3596) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3596:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3597:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3598:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NSEND_SEL");
  /*_.NSEL__V10*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3599:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NSEND_RECV");
  /*_.NRECV__V11*/ meltfptr[10] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3600:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NEXPR_ARGS");
  /*_.NARGS__V12*/ meltfptr[11] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3602:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3602:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3602:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3602;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_msend nsnd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NSND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V14*/ meltfptr[13] =
	      /*_.MELT_DEBUG_FUN__V15*/ meltfptr[14];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3602:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V15*/ meltfptr[14] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V14*/ meltfptr[13] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3602:/ quasiblock");


      /*_.PROGN___V16*/ meltfptr[14] = /*_.IF___V14*/ meltfptr[13];;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.PROGN___V16*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3602:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V14*/ meltfptr[13] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V16*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3603:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.NRECV__V11*/ meltfptr[10])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-genobj.melt:3603:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3603:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nrecv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3603) ? (3603) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[13] = /*_.IFELSE___V18*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3603:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3604:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OSEL__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.NSEL__V10*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3605:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ORECV__V21*/ meltfptr[20] =
	meltgc_send ((melt_ptr_t) ( /*_.NRECV__V11*/ meltfptr[10]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3608:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V23*/ meltfptr[22] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->
						tabval[7])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V23*/ meltfptr[22])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V23*/ meltfptr[22])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V23*/ meltfptr[22])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V22*/ meltfptr[21] = /*_.LAMBDA___V23*/ meltfptr[22];;
    MELT_LOCATION ("warmelt-genobj.melt:3606:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V22*/ meltfptr[21];
      /*_.OARGS__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.NARGS__V12*/ meltfptr[11]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3617:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.MAKE_LIST__V25*/ meltfptr[24] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[9]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3617:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJMSEND */ meltfrout->
					     tabval[8])), (5),
			      "CLASS_OBJMSEND");
  /*_.INST__V27*/ meltfptr[26] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (0),
			  ( /*_.LOC__V9*/ meltfptr[8]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (1),
			  ( /*_.MAKE_LIST__V25*/ meltfptr[24]),
			  "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBMSND_SEL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (2),
			  ( /*_.OSEL__V20*/ meltfptr[19]), "OBMSND_SEL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBMSND_RECV",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (3),
			  ( /*_.ORECV__V21*/ meltfptr[20]), "OBMSND_RECV");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBMSND_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (4),
			  ( /*_.OARGS__V24*/ meltfptr[23]), "OBMSND_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V27*/ meltfptr[26],
				  "newly made instance");
    ;
    /*_.OSEND__V26*/ meltfptr[25] = /*_.INST__V27*/ meltfptr[26];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3626:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3626:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3626:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3626;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_msend osend=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OSEND__V26*/ meltfptr[25];
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V29*/ meltfptr[28] =
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3626:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V29*/ meltfptr[28] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3626:/ quasiblock");


      /*_.PROGN___V31*/ meltfptr[29] = /*_.IF___V29*/ meltfptr[28];;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[27] = /*_.PROGN___V31*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3626:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V29*/ meltfptr[28] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V31*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3627:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L8*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.ORECV__V21*/ meltfptr[20])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-genobj.melt:3627:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L8*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V33*/ meltfptr[29] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3627:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check orecv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3627) ? (3627) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V33*/ meltfptr[29] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V32*/ meltfptr[28] = /*_.IFELSE___V33*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3627:/ clear");
	     /*clear *//*_#IS_OBJECT__L8*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V33*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V32*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V19*/ meltfptr[14] = /*_.OSEND__V26*/ meltfptr[25];;

    MELT_LOCATION ("warmelt-genobj.melt:3604:/ clear");
	   /*clear *//*_.OSEL__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.ORECV__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.OARGS__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_LIST__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.OSEND__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V32*/ meltfptr[28] = 0;
    /*_.LET___V8*/ meltfptr[6] = /*_.LET___V19*/ meltfptr[14];;

    MELT_LOCATION ("warmelt-genobj.melt:3597:/ clear");
	   /*clear *//*_.LOC__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.NSEL__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.NRECV__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.NARGS__V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LET___V19*/ meltfptr[14] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3594:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3594:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_MSEND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_97_warmelt_genobj_COMPILOBJ_NREP_MSEND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_genobj_LAMBDA___37__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_98_warmelt_genobj_LAMBDA___37___melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_98_warmelt_genobj_LAMBDA___37___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 14
    melt_ptr_t mcfr_varptr[14];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_98_warmelt_genobj_LAMBDA___37__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_98_warmelt_genobj_LAMBDA___37___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 14; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_98_warmelt_genobj_LAMBDA___37__ nbval 14*/
  meltfram__.mcfr_nbvar = 14 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3608:/ getarg");
 /*_.NCURARG__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3609:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3609:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3609:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3609;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_msend ncurarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NCURARG__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " ix=";
	      /*^apply.arg */
	      argtab[6].meltbp_long = /*_#IX__L1*/ meltfnum[0];
	      /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V4*/ meltfptr[3] = /*_.MELT_DEBUG_FUN__V5*/ meltfptr[4];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3609:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V5*/ meltfptr[4] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V4*/ meltfptr[3] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3609:/ quasiblock");


      /*_.PROGN___V6*/ meltfptr[4] = /*_.IF___V4*/ meltfptr[3];;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.PROGN___V6*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3609:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V4*/ meltfptr[3] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V6*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3610:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.OCURARG__V8*/ meltfptr[4] =
	meltgc_send ((melt_ptr_t) ( /*_.NCURARG__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[1])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3612:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[2] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3612:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3612:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3612;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_msend ocurarg=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OCURARG__V8*/ meltfptr[4];
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V10*/ meltfptr[9] =
	      /*_.MELT_DEBUG_FUN__V11*/ meltfptr[10];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3612:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V11*/ meltfptr[10] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V10*/ meltfptr[9] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3612:/ quasiblock");


      /*_.PROGN___V12*/ meltfptr[10] = /*_.IF___V10*/ meltfptr[9];;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.PROGN___V12*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3612:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IF___V10*/ meltfptr[9] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V12*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3613:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE__L6*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.OCURARG__V8*/ meltfptr[4])) ==
	 MELTOBMAG_MULTIPLE);;
      /*^compute */
   /*_#NOT__L7*/ meltfnum[2] =
	(!( /*_#IS_MULTIPLE__L6*/ meltfnum[1]));;
      MELT_LOCATION ("warmelt-genobj.melt:3613:/ cond");
      /*cond */ if ( /*_#NOT__L7*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3613:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check ocurarg not tuple"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3613) ? (3613) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[9] = /*_.IFELSE___V14*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3613:/ clear");
	     /*clear *//*_#IS_MULTIPLE__L6*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_#NOT__L7*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V7*/ meltfptr[3] = /*_.OCURARG__V8*/ meltfptr[4];;

    MELT_LOCATION ("warmelt-genobj.melt:3610:/ clear");
	   /*clear *//*_.OCURARG__V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[9] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3608:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V7*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3608:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V7*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_98_warmelt_genobj_LAMBDA___37___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_98_warmelt_genobj_LAMBDA___37__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 51
    melt_ptr_t mcfr_varptr[51];
#define MELTFRAM_NBVARNUM 15
    long mcfr_varnum[15];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 51; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND nbval 51*/
  meltfram__.mcfr_nbvar = 51 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NREP_MULTIMSEND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3634:/ getarg");
 /*_.NMSND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3635:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.NMSND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NREP_MULTIMSEND */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3635:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3635:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nmsnd"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3635) ? (3635) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3635:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3636:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3636:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3636:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3636) ? (3636) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3636:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3637:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3637:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3637:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3637;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_nrep_multimsend nmsnd=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.NMSND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3637:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3637:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3637:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3638:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "NREP_LOC");
  /*_.LOC__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3639:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NMULSEND_BINDINGS");
  /*_.RBINDS__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3640:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NMULSEND_BODY");
  /*_.NBODY__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3641:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NSEND_SEL");
  /*_.NSEL__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3642:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NSEND_RECV");
  /*_.NRECV__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3643:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.NMSND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NEXPR_ARGS");
  /*_.NARGS__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3644:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "GNCX_LOCMAP");
  /*_.LOCMAP__V19*/ meltfptr[18] = slot;
    };
    ;
 /*_#NBRES__L5*/ meltfnum[3] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.RBINDS__V14*/ meltfptr[13])));;
    MELT_LOCATION ("warmelt-genobj.melt:3649:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V21*/ meltfptr[20] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->
						tabval[7])), (2));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V21*/ meltfptr[20])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V21*/ meltfptr[20])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V21*/ meltfptr[20])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V21*/ meltfptr[20])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V21*/ meltfptr[20])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V21*/ meltfptr[20])->tabval[1] =
      (melt_ptr_t) ( /*_.LOCMAP__V19*/ meltfptr[18]);
    ;
    /*_.LAMBDA___V20*/ meltfptr[19] = /*_.LAMBDA___V21*/ meltfptr[20];;
    MELT_LOCATION ("warmelt-genobj.melt:3647:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V20*/ meltfptr[19];
      /*_.RESLOCS__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.RBINDS__V14*/ meltfptr[13]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3662:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L6*/ meltfnum[0] =
      (( /*_#NBRES__L5*/ meltfnum[3]) > (1));;
    MELT_LOCATION ("warmelt-genobj.melt:3662:/ cond");
    /*cond */ if ( /*_#I__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_#I__L7*/ meltfnum[6] =
	    (( /*_#NBRES__L5*/ meltfnum[3]) - (1));;
	  /*^compute */
   /*_.MAKE_MULTIPLE__V24*/ meltfptr[23] =
	    (meltgc_new_multiple
	     ((meltobject_ptr_t)
	      (( /*!DISCR_MULTIPLE */ meltfrout->tabval[8])),
	      ( /*_#I__L7*/ meltfnum[6])));;
	  /*^compute */
	  /*_.OXRES__V23*/ meltfptr[22] =
	    /*_.MAKE_MULTIPLE__V24*/ meltfptr[23];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:3662:/ clear");
	     /*clear *//*_#I__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_.MAKE_MULTIPLE__V24*/ meltfptr[23] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.OXRES__V23*/ meltfptr[22] = NULL;;
      }
    ;
    /*^compute */
 /*_.FIRSTRES__V25*/ meltfptr[23] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.RESLOCS__V22*/ meltfptr[21]), (0)));;
    MELT_LOCATION ("warmelt-genobj.melt:3665:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OSEL__V26*/ meltfptr[25] =
	meltgc_send ((melt_ptr_t) ( /*_.NSEL__V16*/ meltfptr[15]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[9])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
 /*_.RESLIST__V27*/ meltfptr[26] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[10]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3667:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ORECV__V28*/ meltfptr[27] =
	meltgc_send ((melt_ptr_t) ( /*_.NRECV__V17*/ meltfptr[16]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[9])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3669:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V30*/ meltfptr[29] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_12 */ meltfrout->
						tabval[12])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V30*/ meltfptr[29])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V30*/ meltfptr[29])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V30*/ meltfptr[29])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V29*/ meltfptr[28] = /*_.LAMBDA___V30*/ meltfptr[29];;
    MELT_LOCATION ("warmelt-genobj.melt:3668:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V29*/ meltfptr[28];
      /*_.OARGS__V31*/ meltfptr[30] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_MAP */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.NARGS__V18*/ meltfptr[17]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3671:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OBODY__V32*/ meltfptr[31] =
	meltgc_send ((melt_ptr_t) ( /*_.NBODY__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[9])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
 /*_.OBODL__V33*/ meltfptr[32] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[10]))));;
    /*^compute */
 /*_.OEPIL__V34*/ meltfptr[33] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[10]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3674:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJPLAINBLOCK */
					     meltfrout->tabval[13])), (3),
			      "CLASS_OBJPLAINBLOCK");
  /*_.INST__V36*/ meltfptr[35] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V36*/ meltfptr[35])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V36*/ meltfptr[35]), (0),
			  ( /*_.LOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_BODYL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V36*/ meltfptr[35])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V36*/ meltfptr[35]), (1),
			  ( /*_.OBODL__V33*/ meltfptr[32]), "OBLO_BODYL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBLO_EPIL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V36*/ meltfptr[35])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V36*/ meltfptr[35]), (2),
			  ( /*_.OEPIL__V34*/ meltfptr[33]), "OBLO_EPIL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V36*/ meltfptr[35],
				  "newly made instance");
    ;
    /*_.OBLOCK__V35*/ meltfptr[34] = /*_.INST__V36*/ meltfptr[35];;
    MELT_LOCATION ("warmelt-genobj.melt:3678:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJMULTIMSEND */
					     meltfrout->tabval[14])), (6),
			      "CLASS_OBJMULTIMSEND");
  /*_.INST__V38*/ meltfptr[37] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBI_LOC",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (0),
			  ( /*_.LOC__V13*/ meltfptr[9]), "OBI_LOC");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (1),
			  ( /*_.RESLIST__V27*/ meltfptr[26]),
			  "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBMSND_SEL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (2),
			  ( /*_.OSEL__V26*/ meltfptr[25]), "OBMSND_SEL");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBMSND_RECV",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (3),
			  ( /*_.ORECV__V28*/ meltfptr[27]), "OBMSND_RECV");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBMSND_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (4),
			  ( /*_.OARGS__V31*/ meltfptr[30]), "OBMSND_ARGS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBMULTSND_XRES",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V38*/ meltfptr[37])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V38*/ meltfptr[37]), (5),
			  ( /*_.OXRES__V23*/ meltfptr[22]), "OBMULTSND_XRES");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V38*/ meltfptr[37],
				  "newly made instance");
    ;
    /*_.OMSEND__V37*/ meltfptr[36] = /*_.INST__V38*/ meltfptr[37];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3686:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L8*/ meltfnum[6] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.ORECV__V28*/ meltfptr[27])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-genobj.melt:3686:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L8*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V40*/ meltfptr[39] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3686:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check orecv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3686) ? (3686) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V40*/ meltfptr[39] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V39*/ meltfptr[38] = /*_.IFELSE___V40*/ meltfptr[39];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3686:/ clear");
	     /*clear *//*_#IS_OBJECT__L8*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V40*/ meltfptr[39] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V39*/ meltfptr[38] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3687:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.FIRSTRES__V25*/ meltfptr[23])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_append_list ((melt_ptr_t)
				( /*_.RESLIST__V27*/ meltfptr[26]),
				(melt_ptr_t) ( /*_.FIRSTRES__V25*/
					      meltfptr[23]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.OARGS__V31*/ meltfptr[30]);
      for ( /*_#IX__L9*/ meltfnum[6] = 0;
	   ( /*_#IX__L9*/ meltfnum[6] >= 0)
	   && ( /*_#IX__L9*/ meltfnum[6] < meltcit1__EACHTUP_ln);
	/*_#IX__L9*/ meltfnum[6]++)
	{
	  /*_.OCURARG__V41*/ meltfptr[39] =
	    melt_multiple_nth ((melt_ptr_t) ( /*_.OARGS__V31*/ meltfptr[30]),
			       /*_#IX__L9*/ meltfnum[6]);



	  MELT_LOCATION ("warmelt-genobj.melt:3692:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.OCURARG__V41*/ meltfptr[39])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

    /*_#IS_NOT_A__L11*/ meltfnum[10] =
		  !melt_is_instance_of ((melt_ptr_t)
					( /*_.OCURARG__V41*/ meltfptr[39]),
					(melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */ meltfrout->tabval[16])));;
		MELT_LOCATION ("warmelt-genobj.melt:3692:/ cond");
		/*cond */ if ( /*_#IS_NOT_A__L11*/ meltfnum[10])	/*then */
		  {
		    /*^cond.then */
		    /*_#OR___L12*/ meltfnum[11] =
		      /*_#IS_NOT_A__L11*/ meltfnum[10];;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-genobj.melt:3692:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {

      /*_#IS_A__L13*/ meltfnum[12] =
			melt_is_instance_of ((melt_ptr_t)
					     ( /*_.OCURARG__V41*/
					      meltfptr[39]),
					     (melt_ptr_t) (( /*!CLASS_OBJINSTR */ meltfrout->tabval[15])));;
		      /*^compute */
		      /*_#OR___L12*/ meltfnum[11] =
			/*_#IS_A__L13*/ meltfnum[12];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-genobj.melt:3692:/ clear");
		/*clear *//*_#IS_A__L13*/ meltfnum[12] = 0;
		    }
		    ;
		  }
		;
		/*_#IF___L10*/ meltfnum[9] = /*_#OR___L12*/ meltfnum[11];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:3692:/ clear");
	      /*clear *//*_#IS_NOT_A__L11*/ meltfnum[10] = 0;
		/*^clear */
	      /*clear *//*_#OR___L12*/ meltfnum[11] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

   /*_#IF___L10*/ meltfnum[9] = 0;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:3692:/ cond");
	  /*cond */ if ( /*_#IF___L10*/ meltfnum[9])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-genobj.melt:3693:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OBODL__V33*/ meltfptr[32]),
				      (melt_ptr_t) ( /*_.OCURARG__V41*/
						    meltfptr[39]));
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;
	  if ( /*_#IX__L9*/ meltfnum[6] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-genobj.melt:3689:/ clear");
	    /*clear *//*_.OCURARG__V41*/ meltfptr[39] = 0;
      /*^clear */
	    /*clear *//*_#IX__L9*/ meltfnum[6] = 0;
      /*^clear */
	    /*clear *//*_#IF___L10*/ meltfnum[9] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3695:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OBODL__V33*/ meltfptr[32]),
			  (melt_ptr_t) ( /*_.OMSEND__V37*/ meltfptr[36]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3697:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.OBODL__V33*/ meltfptr[32]),
			  (melt_ptr_t) ( /*_.OBODY__V32*/ meltfptr[31]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3701:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V43*/ meltfptr[42] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_19 */ meltfrout->
						tabval[19])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V43*/ meltfptr[42])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V43*/ meltfptr[42])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V43*/ meltfptr[42])->tabval[0] =
      (melt_ptr_t) ( /*_.LOC__V13*/ meltfptr[9]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V43*/ meltfptr[42])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V43*/ meltfptr[42])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V43*/ meltfptr[42])->tabval[1] =
      (melt_ptr_t) ( /*_.OEPIL__V34*/ meltfptr[33]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V43*/ meltfptr[42])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V43*/ meltfptr[42])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V43*/ meltfptr[42])->tabval[2] =
      (melt_ptr_t) ( /*_.OXRES__V23*/ meltfptr[22]);
    ;
    /*_.LAMBDA___V42*/ meltfptr[41] = /*_.LAMBDA___V43*/ meltfptr[42];;
    MELT_LOCATION ("warmelt-genobj.melt:3699:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V42*/ meltfptr[41];
      /*_.MULTIPLE_EVERY__V44*/ meltfptr[43] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[17])),
		    (melt_ptr_t) ( /*_.RESLOCS__V22*/ meltfptr[21]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3711:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V46*/ meltfptr[45] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_21 */ meltfrout->
						tabval[21])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V46*/ meltfptr[45])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V46*/ meltfptr[45])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V46*/ meltfptr[45])->tabval[0] =
      (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]);
    ;
    /*_.LAMBDA___V45*/ meltfptr[44] = /*_.LAMBDA___V46*/ meltfptr[45];;
    MELT_LOCATION ("warmelt-genobj.melt:3709:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V45*/ meltfptr[44];
      /*_.MULTIPLE_EVERY__V47*/ meltfptr[46] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MULTIPLE_EVERY */ meltfrout->tabval[17])),
		    (melt_ptr_t) ( /*_.RBINDS__V14*/ meltfptr[13]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3712:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L14*/ meltfnum[12] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3712:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[12])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3712:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L15*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3712;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_nrep_multimsend final oblock=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBLOCK__V35*/ meltfptr[34];
	      /*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V49*/ meltfptr[48] =
	      /*_.MELT_DEBUG_FUN__V50*/ meltfptr[49];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3712:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V50*/ meltfptr[49] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V49*/ meltfptr[48] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3712:/ quasiblock");


      /*_.PROGN___V51*/ meltfptr[49] = /*_.IF___V49*/ meltfptr[48];;
      /*^compute */
      /*_.IFCPP___V48*/ meltfptr[47] = /*_.PROGN___V51*/ meltfptr[49];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3712:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[12] = 0;
      /*^clear */
	     /*clear *//*_.IF___V49*/ meltfptr[48] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V51*/ meltfptr[49] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V48*/ meltfptr[47] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V12*/ meltfptr[8] = /*_.OBLOCK__V35*/ meltfptr[34];;

    MELT_LOCATION ("warmelt-genobj.melt:3638:/ clear");
	   /*clear *//*_.LOC__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.RBINDS__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NBODY__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NSEL__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NRECV__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.NARGS__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LOCMAP__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_#NBRES__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.RESLOCS__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#I__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.OXRES__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.FIRSTRES__V25*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.OSEL__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.RESLIST__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.ORECV__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.OARGS__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.OBODY__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.OBODL__V33*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.OEPIL__V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_.OBLOCK__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.OMSEND__V37*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V42*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V44*/ meltfptr[43] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V45*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.MULTIPLE_EVERY__V47*/ meltfptr[46] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V48*/ meltfptr[47] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3634:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3634:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NREP_MULTIMSEND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_99_warmelt_genobj_COMPILOBJ_NREP_MULTIMSEND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_genobj_LAMBDA___38__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_100_warmelt_genobj_LAMBDA___38___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_100_warmelt_genobj_LAMBDA___38___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 10
    melt_ptr_t mcfr_varptr[10];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_100_warmelt_genobj_LAMBDA___38__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_100_warmelt_genobj_LAMBDA___38___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 10; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_100_warmelt_genobj_LAMBDA___38__ nbval 10*/
  meltfram__.mcfr_nbvar = 10 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3649:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3650:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_FORMAL_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3650:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3650:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_nrep_multimsend check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3650) ? (3650) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3650:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3652:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.BDER__V6*/ meltfptr[5] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3653:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "FBIND_TYPE");
  /*_.CTY__V7*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3654:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.BDER__V6*/ meltfptr[5];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.CTY__V7*/ meltfptr[6];
      /*_.OBVA__V8*/ meltfptr[7] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!GET_FREE_OBJLOCTYPED */ meltfrout->tabval[1])),
		    (melt_ptr_t) (( /*~GCX */ meltfclos->tabval[0])),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3656:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.CTY__V7*/ meltfptr[6]),
			     (melt_ptr_t) (( /*!CLASS_CTYPE */ meltfrout->
					    tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:3656:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3656:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_nrep_multimsend check cty"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3656) ? (3656) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[8] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3656:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3659:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.OBVA__V8*/ meltfptr[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    meltgc_put_mapobjects ((meltmapobjects_ptr_t)
				   (( /*~LOCMAP */ meltfclos->tabval[1])),
				   (meltobject_ptr_t) ( /*_.BIND__V2*/
						       meltfptr[1]),
				   (melt_ptr_t) ( /*_.OBVA__V8*/
						 meltfptr[7]));
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*_.LET___V5*/ meltfptr[3] = /*_.OBVA__V8*/ meltfptr[7];;

    MELT_LOCATION ("warmelt-genobj.melt:3652:/ clear");
	   /*clear *//*_.BDER__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.CTY__V7*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.OBVA__V8*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[8] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3649:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3649:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_100_warmelt_genobj_LAMBDA___38___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_100_warmelt_genobj_LAMBDA___38__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_genobj_LAMBDA___39__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_101_warmelt_genobj_LAMBDA___39___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_101_warmelt_genobj_LAMBDA___39___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_101_warmelt_genobj_LAMBDA___39__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_101_warmelt_genobj_LAMBDA___39___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_101_warmelt_genobj_LAMBDA___39__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3669:/ getarg");
 /*_.COMP__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:3670:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.COMPILE_OBJ__V3*/ meltfptr[2] =
	meltgc_send ((melt_ptr_t) ( /*_.COMP__V2*/ meltfptr[1]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[0])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3669:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.COMPILE_OBJ__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3669:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.COMPILE_OBJ__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_101_warmelt_genobj_LAMBDA___39___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_101_warmelt_genobj_LAMBDA___39__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_genobj_LAMBDA___40__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_102_warmelt_genobj_LAMBDA___40___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_102_warmelt_genobj_LAMBDA___40___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 4
    melt_ptr_t mcfr_varptr[4];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_102_warmelt_genobj_LAMBDA___40__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_102_warmelt_genobj_LAMBDA___40___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 4; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_102_warmelt_genobj_LAMBDA___40__ nbval 4*/
  meltfram__.mcfr_nbvar = 4 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3701:/ getarg");
 /*_.RLOC__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-genobj.melt:3702:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L2*/ meltfnum[1] =
      (( /*_#IX__L1*/ meltfnum[0]) > (0));;
    MELT_LOCATION ("warmelt-genobj.melt:3702:/ cond");
    /*cond */ if ( /*_#I__L2*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:3703:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_OBJCLEAR */
						   meltfrout->tabval[0])),
				    (2), "CLASS_OBJCLEAR");
    /*_.INST__V4*/ meltfptr[3] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OBI_LOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V4*/ meltfptr[3])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V4*/ meltfptr[3]), (0),
				(( /*~LOC */ meltfclos->tabval[0])),
				"OBI_LOC");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @OCLR_VLOC",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V4*/ meltfptr[3])) ==
			  MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V4*/ meltfptr[3]), (1),
				( /*_.RLOC__V2*/ meltfptr[1]), "OCLR_VLOC");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V4*/ meltfptr[3],
					"newly made instance");
	  ;
	  /*_.OCLEAR__V3*/ meltfptr[2] = /*_.INST__V4*/ meltfptr[3];;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3706:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				(( /*~OEPIL */ meltfclos->tabval[1])),
				(melt_ptr_t) ( /*_.OCLEAR__V3*/ meltfptr[2]));
	  }
	  ;
   /*_#I__L3*/ meltfnum[2] =
	    (( /*_#IX__L1*/ meltfnum[0]) - (1));;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3707:/ locexp");
	    meltgc_multiple_put_nth ((melt_ptr_t)
				     (( /*~OXRES */ meltfclos->tabval[2])),
				     ( /*_#I__L3*/ meltfnum[2]),
				     (melt_ptr_t) ( /*_.RLOC__V2*/
						   meltfptr[1]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:3703:/ clear");
	     /*clear *//*_.OCLEAR__V3*/ meltfptr[2] = 0;
	  /*^clear */
	     /*clear *//*_#I__L3*/ meltfnum[2] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    /*epilog */

    MELT_LOCATION ("warmelt-genobj.melt:3701:/ clear");
	   /*clear *//*_#I__L2*/ meltfnum[1] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_102_warmelt_genobj_LAMBDA___40___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*noretval */ NULL);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_102_warmelt_genobj_LAMBDA___40__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_genobj_LAMBDA___41__ (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_103_warmelt_genobj_LAMBDA___41___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_103_warmelt_genobj_LAMBDA___41___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_103_warmelt_genobj_LAMBDA___41__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_103_warmelt_genobj_LAMBDA___41___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_103_warmelt_genobj_LAMBDA___41__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3711:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_LONG)
    goto lab_endgetargs;
 /*_#IX__L1*/ meltfnum[0] = meltxargtab_[0].meltbp_long;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~GCX */ meltfclos->tabval[0]);
      /*_.DISPOSE_BND_OBJ__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!DISPOSE_BND_OBJ */ meltfrout->tabval[0])),
		    (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.DISPOSE_BND_OBJ__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3711:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.DISPOSE_BND_OBJ__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_103_warmelt_genobj_LAMBDA___41___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_103_warmelt_genobj_LAMBDA___41__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 15
    melt_ptr_t mcfr_varptr[15];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 15; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING nbval 15*/
  meltfram__.mcfr_nbvar = 15 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_ANY_BINDING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3721:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3722:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_ANY_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3722:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3722:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3722) ? (3722) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3722:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3723:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3723:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3723:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3723) ? (3723) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3723:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3724:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3724:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3724:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3724;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_any_binding bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3724:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3724:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3724:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3725:/ locexp");
      melt_puts (stderr,
		 ("* compilobj unimplemented receiver binding class "));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3726:/ quasiblock");


 /*_.DISCR__V12*/ meltfptr[8] =
      ((melt_ptr_t)
       (melt_discr ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3726:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DISCR__V12*/ meltfptr[8]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V13*/ meltfptr[9] = slot;
    };
    ;

    {
      /*^locexp */
      melt_putstr (stderr, (melt_ptr_t) ( /*_.NAMED_NAME__V13*/ meltfptr[9]));
    }
    ;

    /*^clear */
	   /*clear *//*_.DISCR__V12*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V13*/ meltfptr[9] = 0;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3727:/ locexp");
      melt_newlineflush (stderr);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3728:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3728:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@@compile_obj should be implemented in anybinding-s subclasses"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (3728) ? (3728) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[8] = /*_.IFELSE___V15*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3728:/ clear");
	     /*clear *//*_.IFELSE___V15*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3721:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.IFCPP___V14*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3721:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_ANY_BINDING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_104_warmelt_genobj_COMPILOBJ_ANY_BINDING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_VALUE_BINDING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3734:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3735:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3735:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3735:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3735) ? (3735) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3735:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3736:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_INITIAL_GENERATION_CONTEXT */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3736:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3736:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3736) ? (3736) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3736:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3737:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3737:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3737:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3737;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_value_binding bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3737:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3737:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3737:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3738:/ quasiblock");


    /*^cond */
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.BIND__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_ANY_BINDING */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "BINDER");
   /*_.SYM__V13*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SYM__V13*/ meltfptr[9] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3739:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.GCX__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_INITIAL_GENERATION_CONTEXT */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 13, "IGNCX_IMPORTMAP");
   /*_.IMPORTMAP__V14*/ meltfptr[13] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.IMPORTMAP__V14*/ meltfptr[13] = NULL;;
      }
    ;
    /*^compute */
 /*_.OLOCV__V15*/ meltfptr[14] =
      /*mapobject_get */
      melt_get_mapobjects ((meltmapobjects_ptr_t)
			   ( /*_.IMPORTMAP__V14*/ meltfptr[13]),
			   (meltobject_ptr_t) ( /*_.SYM__V13*/ meltfptr[9]));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3742:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3742:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3742:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3742;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_value_binding olocv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OLOCV__V15*/ meltfptr[14];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " importmap=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.IMPORTMAP__V14*/ meltfptr[13];
	      /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V17*/ meltfptr[16] =
	      /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3742:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V17*/ meltfptr[16] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3742:/ quasiblock");


      /*_.PROGN___V19*/ meltfptr[17] = /*_.IF___V17*/ meltfptr[16];;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[15] = /*_.PROGN___V19*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3742:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V17*/ meltfptr[16] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V19*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3743:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L7*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.OLOCV__V15*/ meltfptr[14]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					    tabval[4])));;
      MELT_LOCATION ("warmelt-genobj.melt:3743:/ cond");
      /*cond */ if ( /*_#IS_A__L7*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V21*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3743:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check olocv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3743) ? (3743) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V21*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[16] = /*_.IFELSE___V21*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3743:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3744:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OLOCV__V15*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3744:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V12*/ meltfptr[8] = /*_.RETURN___V22*/ meltfptr[17];;

    MELT_LOCATION ("warmelt-genobj.melt:3738:/ clear");
	   /*clear *//*_.SYM__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IMPORTMAP__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.OLOCV__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V22*/ meltfptr[17] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3734:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3734:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_VALUE_BINDING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_105_warmelt_genobj_COMPILOBJ_VALUE_BINDING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_FIXED_BINDING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3750:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3751:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_FIXED_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3751:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3751:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3751) ? (3751) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3751:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3752:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3752:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3752:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3752) ? (3752) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3752:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3753:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3753:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3753:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3753;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_fixed_binding bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3753:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3753:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3753:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3754:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "FIXBIND_DATA");
  /*_.SBDATA__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3755:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OSDATA__V14*/ meltfptr[13] =
	meltgc_send ((melt_ptr_t) ( /*_.SBDATA__V13*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3756:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3756:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3756:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3756;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_fixed_binding sbdata=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SBDATA__V13*/ meltfptr[9];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " osdata=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.OSDATA__V14*/ meltfptr[13];
	      /*_.MELT_DEBUG_FUN__V17*/ meltfptr[16] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V16*/ meltfptr[15] =
	      /*_.MELT_DEBUG_FUN__V17*/ meltfptr[16];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3756:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V17*/ meltfptr[16] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V16*/ meltfptr[15] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3756:/ quasiblock");


      /*_.PROGN___V18*/ meltfptr[16] = /*_.IF___V16*/ meltfptr[15];;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[14] = /*_.PROGN___V18*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3756:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V16*/ meltfptr[15] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V18*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3757:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OSDATA__V14*/ meltfptr[13];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3757:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V12*/ meltfptr[8] = /*_.RETURN___V19*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-genobj.melt:3754:/ clear");
	   /*clear *//*_.SBDATA__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OSDATA__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V19*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3750:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3750:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_FIXED_BINDING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_106_warmelt_genobj_COMPILOBJ_FIXED_BINDING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un *
							  meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un *
							  meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_NORMAL_LET_BINDING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3763:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3764:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMAL_LET_BINDING */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3764:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3764:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3764) ? (3764) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3764:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3765:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3765:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3765:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3765) ? (3765) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3765:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3766:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3766:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3766:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3766;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_normal_let_binding bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3766:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3766:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3766:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3767:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "LETBIND_EXPR");
  /*_.NEXPR__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3768:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LETBIND_TYPE");
  /*_.CTYP__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3769:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.BNDER__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3770:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OEXPR__V16*/ meltfptr[15] =
	meltgc_send ((melt_ptr_t) ( /*_.NEXPR__V13*/ meltfptr[9]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3771:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3771:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3771:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3771;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_normal_let_binding oexpr=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OEXPR__V16*/ meltfptr[15];
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V18*/ meltfptr[17] =
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[18];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3771:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[18] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V18*/ meltfptr[17] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3771:/ quasiblock");


      /*_.PROGN___V20*/ meltfptr[18] = /*_.IF___V18*/ meltfptr[17];;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[16] = /*_.PROGN___V20*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3771:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V18*/ meltfptr[17] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V20*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3772:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V22*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3772:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("compilobj_normal_let_binding got here"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3772) ? (3772) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[17] = /*_.IFELSE___V22*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3772:/ clear");
	     /*clear *//*_.IFELSE___V22*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3773:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.OEXPR__V16*/ meltfptr[15];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3773:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V12*/ meltfptr[8] = /*_.RETURN___V23*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-genobj.melt:3767:/ clear");
	   /*clear *//*_.NEXPR__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.CTYP__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.BNDER__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OEXPR__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V23*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3763:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3763:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_NORMAL_LET_BINDING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_107_warmelt_genobj_COMPILOBJ_NORMAL_LET_BINDING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 36
    melt_ptr_t mcfr_varptr[36];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 36; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND nbval 36*/
  meltfram__.mcfr_nbvar = 36 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_CONSLAMBDABIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3779:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3780:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMAL_CONSTRUCTED_LAMBDA_BINDING */ meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3780:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3780:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3780) ? (3780) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3780:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3781:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3781:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3781:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3781) ? (3781) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3781:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3782:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3782:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3782:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3782;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_conslambdabind bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3782:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3782:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3782:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3783:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V13*/ meltfptr[9] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3784:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NCONSB_LOC");
  /*_.LOC__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3785:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NCONSB_DISCR");
  /*_.NDISCR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3786:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NLAMBDAB_NCLOSED");
  /*_.NCLOSED__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3787:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 6, "NLAMBDAB_DATAROUT");
  /*_.NDATAROUT__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3788:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NLAMBDAB_CONSTROUT");
  /*_.NCONSTROUT__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3789:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NCONSB_NLETREC");
  /*_.NLETREC__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3790:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.NDISCR__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
 /*_.NAMBUF__V21*/ meltfptr[20] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[4])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-genobj.melt:3792:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NLETREC__V19*/ meltfptr[18]),
					(melt_ptr_t) (( /*!CLASS_NREP_LETREC */ meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NLETREC__V19*/ meltfptr[18]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "NLETREC_LOCSYMS");
   /*_.NLOCSYMS__V22*/ meltfptr[21] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NLOCSYMS__V22*/ meltfptr[21] = NULL;;
      }
    ;
    /*^compute */
 /*_#BINDNUM__L5*/ meltfnum[3] =
      (melt_get_int ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1])));;
    /*^compute */
 /*_#NBCLOSED__L6*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.NCLOSED__V16*/ meltfptr[15])));;
    /*^compute */
 /*_.LOCSYMOCC__V23*/ meltfptr[22] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.NLOCSYMS__V22*/ meltfptr[21]),
	( /*_#BINDNUM__L5*/ meltfnum[3])));;
    MELT_LOCATION ("warmelt-genobj.melt:3797:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:3798:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OROUT__V25*/ meltfptr[24] =
	meltgc_send ((melt_ptr_t) ( /*_.NCONSTROUT__V18*/ meltfptr[17]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3799:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OLOC__V26*/ meltfptr[25] =
	meltgc_send ((melt_ptr_t) ( /*_.LOCSYMOCC__V23*/ meltfptr[22]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3801:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]),
			   ("rclo_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3802:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]),
			     ( /*_#BINDNUM__L5*/ meltfnum[3]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3803:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]),
			   ("__"));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3804:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[9]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V27*/ meltfptr[26] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_strbuf_cident ((melt_ptr_t)
				( /*_.NAMBUF__V21*/ meltfptr[20]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NAMED_NAME__V27*/
						  meltfptr[26])));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3805:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L7*/ meltfnum[6] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[9]),
			   (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
					  meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-genobj.melt:3805:/ cond");
    /*cond */ if ( /*_#IS_A__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3807:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.NAMBUF__V21*/ meltfptr[20]), ("_x"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:3808:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.SYMB__V13*/ meltfptr[9]),
					      (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */ meltfrout->tabval[6])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[9]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "CSYM_URANK");
     /*_.CSYM_URANK__V28*/ meltfptr[27] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.CSYM_URANK__V28*/ meltfptr[27] = NULL;;
	    }
	  ;
	  /*^compute */
   /*_#GET_INT__L8*/ meltfnum[7] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.CSYM_URANK__V28*/ meltfptr[27])));;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3808:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.NAMBUF__V21*/ meltfptr[20]),
				   ( /*_#GET_INT__L8*/ meltfnum[7]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:3806:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:3805:/ clear");
	     /*clear *//*_.CSYM_URANK__V28*/ meltfptr[27] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3809:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V30*/ meltfptr[29] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[9])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3809:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITCLOSURE */
					     meltfrout->tabval[7])), (6),
			      "CLASS_OBJINITCLOSURE");
  /*_.INST__V32*/ meltfptr[31] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[8])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (3),
			  ( /*_.ODISCR__V20*/ meltfptr[19]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (4),
			  ( /*_.OLOC__V26*/ meltfptr[25]), "OIE_LOCVAR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (1),
			  ( /*_.STRBUF2STRING__V30*/ meltfptr[29]),
			  "OIE_CNAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OICLO_ROUT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V32*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V32*/ meltfptr[31]), (5),
			  ( /*_.OROUT__V25*/ meltfptr[24]), "OICLO_ROUT");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V32*/ meltfptr[31],
				  "newly made instance");
    ;
    /*_.INICLOS__V31*/ meltfptr[30] = /*_.INST__V32*/ meltfptr[31];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3817:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.INICLOS__V31*/ meltfptr[30]),
		    ( /*_#NBCLOSED__L6*/ meltfnum[0]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3818:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3818:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3818:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3818;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_conslambdabind returning iniclos=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INICLOS__V31*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V34*/ meltfptr[33] =
	      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3818:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V34*/ meltfptr[33] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3818:/ quasiblock");


      /*_.PROGN___V36*/ meltfptr[34] = /*_.IF___V34*/ meltfptr[33];;
      /*^compute */
      /*_.IFCPP___V33*/ meltfptr[32] = /*_.PROGN___V36*/ meltfptr[34];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3818:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V34*/ meltfptr[33] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V36*/ meltfptr[34] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V33*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V29*/ meltfptr[27] = /*_.INICLOS__V31*/ meltfptr[30];;

    MELT_LOCATION ("warmelt-genobj.melt:3809:/ clear");
	   /*clear *//*_.STRBUF2STRING__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.INICLOS__V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V33*/ meltfptr[32] = 0;
    /*_.LET___V24*/ meltfptr[23] = /*_.LET___V29*/ meltfptr[27];;

    MELT_LOCATION ("warmelt-genobj.melt:3797:/ clear");
	   /*clear *//*_.OROUT__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.OLOC__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V29*/ meltfptr[27] = 0;
    /*_.LET___V12*/ meltfptr[8] = /*_.LET___V24*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-genobj.melt:3783:/ clear");
	   /*clear *//*_.SYMB__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NDISCR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NCLOSED__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NDATAROUT__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.NCONSTROUT__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.NLETREC__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.NAMBUF__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.NLOCSYMS__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#BINDNUM__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_#NBCLOSED__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.LOCSYMOCC__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.LET___V24*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3779:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3779:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_CONSLAMBDABIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_108_warmelt_genobj_COMPILOBJ_CONSLAMBDABIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 34
    melt_ptr_t mcfr_varptr[34];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 34; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND nbval 34*/
  meltfram__.mcfr_nbvar = 34 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_CONSTUPLEBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3824:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3825:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3825:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3825:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3825;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_constuplebind bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3825:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3825:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3825:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3826:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMAL_CONSTRUCTED_TUPLE_BINDING */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3826:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3826:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3826) ? (3826) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3826:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3827:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:3827:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3827:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3827) ? (3827) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3827:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3828:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3829:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NCONSB_LOC");
  /*_.NLOC__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3830:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NCONSB_DISCR");
  /*_.NDISCR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3831:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NCONSB_NLETREC");
  /*_.NLETREC__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3832:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NTUPB_COMP");
  /*_.NTUPB__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3833:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V18*/ meltfptr[17] =
	meltgc_send ((melt_ptr_t) ( /*_.NDISCR__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
 /*_.NAMBUF__V19*/ meltfptr[18] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[4])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-genobj.melt:3835:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NCONSB_NLETREC");
  /*_.NLETREC__V20*/ meltfptr[19] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3836:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NLETREC__V20*/ meltfptr[19]),
					(melt_ptr_t) (( /*!CLASS_NREP_LETREC */ meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NLETREC__V20*/ meltfptr[19]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "NLETREC_LOCSYMS");
   /*_.NLOCSYMS__V21*/ meltfptr[20] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NLOCSYMS__V21*/ meltfptr[20] = NULL;;
      }
    ;
    /*^compute */
 /*_#BINDNUM__L5*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1])));;
    /*^compute */
 /*_#LENTUP__L6*/ meltfnum[0] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.NTUPB__V17*/ meltfptr[16])));;
    /*^compute */
 /*_.LOCSYMOCC__V22*/ meltfptr[21] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.NLOCSYMS__V21*/ meltfptr[20]),
	( /*_#BINDNUM__L5*/ meltfnum[1])));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3841:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V19*/ meltfptr[18]),
			   ("rtup_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3842:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V19*/ meltfptr[18]),
			     ( /*_#BINDNUM__L5*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3843:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V19*/ meltfptr[18]),
			   ("__"));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3844:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[12]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V23*/ meltfptr[22] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_strbuf_cident ((melt_ptr_t)
				( /*_.NAMBUF__V19*/ meltfptr[18]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NAMED_NAME__V23*/
						  meltfptr[22])));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3845:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L7*/ meltfnum[6] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[12]),
			   (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
					  meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-genobj.melt:3845:/ cond");
    /*cond */ if ( /*_#IS_A__L7*/ meltfnum[6])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3847:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.NAMBUF__V19*/ meltfptr[18]), ("_x"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:3848:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.SYMB__V13*/ meltfptr[12]),
					      (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */ meltfrout->tabval[6])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[12]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "CSYM_URANK");
     /*_.CSYM_URANK__V24*/ meltfptr[23] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.CSYM_URANK__V24*/ meltfptr[23] = NULL;;
	    }
	  ;
	  /*^compute */
   /*_#GET_INT__L8*/ meltfnum[7] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.CSYM_URANK__V24*/ meltfptr[23])));;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3848:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.NAMBUF__V19*/ meltfptr[18]),
				   ( /*_#GET_INT__L8*/ meltfnum[7]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:3846:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:3845:/ clear");
	     /*clear *//*_.CSYM_URANK__V24*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L8*/ meltfnum[7] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3849:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:3850:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OLOC__V26*/ meltfptr[25] =
	meltgc_send ((melt_ptr_t) ( /*_.LOCSYMOCC__V22*/ meltfptr[21]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[3])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
 /*_.TUPVAL__V27*/ meltfptr[26] =
      (meltgc_new_multiple
       ((meltobject_ptr_t) (( /*!DISCR_MULTIPLE */ meltfrout->tabval[7])),
	( /*_#LENTUP__L6*/ meltfnum[0])));;
    MELT_LOCATION ("warmelt-genobj.melt:3852:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V28*/ meltfptr[27] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[10])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V19*/ meltfptr[18]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3852:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITMULTIPLE */
					     meltfrout->tabval[8])), (6),
			      "CLASS_OBJINITMULTIPLE");
  /*_.INST__V30*/ meltfptr[29] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[9])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (3),
			  ( /*_.ODISCR__V18*/ meltfptr[17]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (4),
			  ( /*_.OLOC__V26*/ meltfptr[25]), "OIE_LOCVAR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (1),
			  ( /*_.STRBUF2STRING__V28*/ meltfptr[27]),
			  "OIE_CNAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIM_TUPVAL",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (5),
			  ( /*_.TUPVAL__V27*/ meltfptr[26]), "OIM_TUPVAL");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V30*/ meltfptr[29],
				  "newly made instance");
    ;
    /*_.INITUP__V29*/ meltfptr[28] = /*_.INST__V30*/ meltfptr[29];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3860:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.INITUP__V29*/ meltfptr[28]),
		    ( /*_#LENTUP__L6*/ meltfnum[0]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3861:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3861:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3861:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3861;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_constuplebind result initup=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INITUP__V29*/ meltfptr[28];
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V32*/ meltfptr[31] =
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3861:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V32*/ meltfptr[31] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3861:/ quasiblock");


      /*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[31];;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[30] = /*_.PROGN___V34*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3861:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V25*/ meltfptr[23] = /*_.INITUP__V29*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-genobj.melt:3849:/ clear");
	   /*clear *//*_.OLOC__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.TUPVAL__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.INITUP__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[30] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V25*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-genobj.melt:3828:/ clear");
	   /*clear *//*_.SYMB__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NDISCR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NLETREC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NTUPB__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.NAMBUF__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.NLETREC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.NLOCSYMS__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#BINDNUM__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_#LENTUP__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.LOCSYMOCC__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L7*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V25*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3824:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3824:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_CONSTUPLEBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_109_warmelt_genobj_COMPILOBJ_CONSTUPLEBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 36
    melt_ptr_t mcfr_varptr[36];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 36; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND nbval 36*/
  meltfram__.mcfr_nbvar = 36 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_CONSPAIRBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3868:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3869:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3869:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3869:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3869;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_conspairbind bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3869:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3869:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3869:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3870:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMAL_CONSTRUCTED_PAIR_BINDING */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3870:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3870:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3870) ? (3870) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3870:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3871:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:3871:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3871:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3871) ? (3871) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3871:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3872:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3873:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NCONSB_LOC");
  /*_.NLOC__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3874:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NCONSB_DISCR");
  /*_.NDISCR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3875:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NCONSB_NLETREC");
  /*_.NLETREC__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3876:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NPAIRB_HEAD");
  /*_.NHEAD__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3877:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NPAIRB_TAIL");
  /*_.NTAIL__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3878:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NLETREC__V16*/ meltfptr[15]),
					(melt_ptr_t) (( /*!CLASS_NREP_LETREC */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NLETREC__V16*/ meltfptr[15]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "NLETREC_LOCSYMS");
   /*_.NLOCSYMS__V19*/ meltfptr[18] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NLOCSYMS__V19*/ meltfptr[18] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3879:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.NDISCR__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
 /*_.NAMBUF__V21*/ meltfptr[20] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[5])),
			 (const char *) 0);;
    /*^compute */
 /*_#BINDNUM__L5*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1])));;
    /*^compute */
 /*_.LOCSYMOCC__V22*/ meltfptr[21] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.NLOCSYMS__V19*/ meltfptr[18]),
	( /*_#BINDNUM__L5*/ meltfnum[1])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3884:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if ( /*_.NLETREC__V16*/ meltfptr[15])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V24*/ meltfptr[23] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3884:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check nletrec"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3884) ? (3884) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[22] = /*_.IFELSE___V24*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3884:/ clear");
	     /*clear *//*_.IFELSE___V24*/ meltfptr[23] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3885:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]),
			   ("rpair_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3886:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]),
			     ( /*_#BINDNUM__L5*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3887:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]),
			   ("__"));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3888:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[12]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V25*/ meltfptr[23] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_strbuf_cident ((melt_ptr_t)
				( /*_.NAMBUF__V21*/ meltfptr[20]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NAMED_NAME__V25*/
						  meltfptr[23])));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3889:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L6*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[12]),
			   (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */
					  meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-genobj.melt:3889:/ cond");
    /*cond */ if ( /*_#IS_A__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3891:/ locexp");
	    /*add2sbuf_strconst */
	      meltgc_add_strbuf ((melt_ptr_t)
				 ( /*_.NAMBUF__V21*/ meltfptr[20]), ("_x"));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:3892:/ cond");
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.SYMB__V13*/ meltfptr[12]),
					      (melt_ptr_t) (( /*!CLASS_CLONED_SYMBOL */ meltfrout->tabval[6])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[12]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 3, "CSYM_URANK");
     /*_.CSYM_URANK__V26*/ meltfptr[25] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.CSYM_URANK__V26*/ meltfptr[25] = NULL;;
	    }
	  ;
	  /*^compute */
   /*_#GET_INT__L7*/ meltfnum[6] =
	    (melt_get_int
	     ((melt_ptr_t) ( /*_.CSYM_URANK__V26*/ meltfptr[25])));;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:3892:/ locexp");
	    meltgc_add_strbuf_dec ((melt_ptr_t)
				   ( /*_.NAMBUF__V21*/ meltfptr[20]),
				   ( /*_#GET_INT__L7*/ meltfnum[6]));
	  }
	  ;
	  MELT_LOCATION ("warmelt-genobj.melt:3890:/ quasiblock");


	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:3889:/ clear");
	     /*clear *//*_.CSYM_URANK__V26*/ meltfptr[25] = 0;
	  /*^clear */
	     /*clear *//*_#GET_INT__L7*/ meltfnum[6] = 0;
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3893:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:3894:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OLOC__V28*/ meltfptr[27] =
	meltgc_send ((melt_ptr_t) ( /*_.LOCSYMOCC__V22*/ meltfptr[21]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3895:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V29*/ meltfptr[28] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[9])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3895:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITPAIR */
					     meltfrout->tabval[7])), (5),
			      "CLASS_OBJINITPAIR");
  /*_.INST__V31*/ meltfptr[30] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V31*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[8])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V31*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (3),
			  ( /*_.ODISCR__V20*/ meltfptr[19]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V31*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (4),
			  ( /*_.OLOC__V28*/ meltfptr[27]), "OIE_LOCVAR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V31*/ meltfptr[30])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (1),
			  ( /*_.STRBUF2STRING__V29*/ meltfptr[28]),
			  "OIE_CNAME");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V31*/ meltfptr[30],
				  "newly made instance");
    ;
    /*_.INIPAIR__V30*/ meltfptr[29] = /*_.INST__V31*/ meltfptr[30];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3901:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3901:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3901:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3901;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_conspairbind return inipair=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INIPAIR__V30*/ meltfptr[29];
	      /*_.MELT_DEBUG_FUN__V34*/ meltfptr[33] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V33*/ meltfptr[32] =
	      /*_.MELT_DEBUG_FUN__V34*/ meltfptr[33];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3901:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V34*/ meltfptr[33] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V33*/ meltfptr[32] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3901:/ quasiblock");


      /*_.PROGN___V35*/ meltfptr[33] = /*_.IF___V33*/ meltfptr[32];;
      /*^compute */
      /*_.IFCPP___V32*/ meltfptr[31] = /*_.PROGN___V35*/ meltfptr[33];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3901:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V33*/ meltfptr[32] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V35*/ meltfptr[33] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V32*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3902:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.INIPAIR__V30*/ meltfptr[29];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3902:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V27*/ meltfptr[25] = /*_.RETURN___V36*/ meltfptr[32];;

    MELT_LOCATION ("warmelt-genobj.melt:3893:/ clear");
	   /*clear *//*_.OLOC__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.INIPAIR__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V36*/ meltfptr[32] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V27*/ meltfptr[25];;

    MELT_LOCATION ("warmelt-genobj.melt:3872:/ clear");
	   /*clear *//*_.SYMB__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NDISCR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NLETREC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NHEAD__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.NTAIL__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.NLOCSYMS__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.NAMBUF__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#BINDNUM__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.LOCSYMOCC__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V25*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.LET___V27*/ meltfptr[25] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3868:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3868:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_CONSPAIRBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_110_warmelt_genobj_COMPILOBJ_CONSPAIRBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 33
    melt_ptr_t mcfr_varptr[33];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 33; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND nbval 33*/
  meltfram__.mcfr_nbvar = 33 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_CONSLISTBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3907:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3908:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3908:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3908:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3908;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_conslistbind bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3908:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3908:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3908:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3909:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMAL_CONSTRUCTED_LIST_BINDING */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3909:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3909:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3909) ? (3909) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3909:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3910:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:3910:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3910:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3910) ? (3910) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3910:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3911:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3912:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NCONSB_LOC");
  /*_.NLOC__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3913:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NCONSB_DISCR");
  /*_.NDISCR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3914:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NCONSB_NLETREC");
  /*_.NLETREC__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3915:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NLISTB_FIRST");
  /*_.NFIRST__V17*/ meltfptr[16] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3916:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 5, "NLISTB_LAST");
  /*_.NLAST__V18*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3917:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NLETREC__V16*/ meltfptr[15]),
					(melt_ptr_t) (( /*!CLASS_NREP_LETREC */ meltfrout->tabval[3])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NLETREC__V16*/ meltfptr[15]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "NLETREC_LOCSYMS");
   /*_.NLOCSYMS__V19*/ meltfptr[18] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NLOCSYMS__V19*/ meltfptr[18] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3918:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V20*/ meltfptr[19] =
	meltgc_send ((melt_ptr_t) ( /*_.NDISCR__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
 /*_.NAMBUF__V21*/ meltfptr[20] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[5])),
			 (const char *) 0);;
    /*^compute */
 /*_#BINDNUM__L5*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1])));;
    /*^compute */
 /*_.LOCSYMOCC__V22*/ meltfptr[21] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.NLOCSYMS__V19*/ meltfptr[18]),
	( /*_#BINDNUM__L5*/ meltfnum[1])));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3923:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]),
			   ("rlist_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3924:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]),
			     ( /*_#BINDNUM__L5*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3925:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]),
			   ("__"));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3926:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[12]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V23*/ meltfptr[22] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_strbuf_cident ((melt_ptr_t)
				( /*_.NAMBUF__V21*/ meltfptr[20]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NAMED_NAME__V23*/
						  meltfptr[22])));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3927:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:3928:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OLOC__V25*/ meltfptr[24] =
	meltgc_send ((melt_ptr_t) ( /*_.LOCSYMOCC__V22*/ meltfptr[21]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3929:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V26*/ meltfptr[25] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[8])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V21*/ meltfptr[20]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3929:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITLIST */
					     meltfrout->tabval[6])), (5),
			      "CLASS_OBJINITLIST");
  /*_.INST__V28*/ meltfptr[27] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[7])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (3),
			  ( /*_.ODISCR__V20*/ meltfptr[19]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (4),
			  ( /*_.OLOC__V25*/ meltfptr[24]), "OIE_LOCVAR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (1),
			  ( /*_.STRBUF2STRING__V26*/ meltfptr[25]),
			  "OIE_CNAME");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V28*/ meltfptr[27],
				  "newly made instance");
    ;
    /*_.INILIST__V27*/ meltfptr[26] = /*_.INST__V28*/ meltfptr[27];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3936:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3936:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3936:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3936;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_conslistbind return inilist=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.INILIST__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V30*/ meltfptr[29] =
	      /*_.MELT_DEBUG_FUN__V31*/ meltfptr[30];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3936:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V31*/ meltfptr[30] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V30*/ meltfptr[29] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3936:/ quasiblock");


      /*_.PROGN___V32*/ meltfptr[30] = /*_.IF___V30*/ meltfptr[29];;
      /*^compute */
      /*_.IFCPP___V29*/ meltfptr[28] = /*_.PROGN___V32*/ meltfptr[30];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3936:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V30*/ meltfptr[29] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V32*/ meltfptr[30] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V29*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3937:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.INILIST__V27*/ meltfptr[26];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3937:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V24*/ meltfptr[23] = /*_.RETURN___V33*/ meltfptr[29];;

    MELT_LOCATION ("warmelt-genobj.melt:3927:/ clear");
	   /*clear *//*_.OLOC__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.INILIST__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V33*/ meltfptr[29] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V24*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-genobj.melt:3911:/ clear");
	   /*clear *//*_.SYMB__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NDISCR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NLETREC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NFIRST__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.NLAST__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.NLOCSYMS__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.NAMBUF__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#BINDNUM__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.LOCSYMOCC__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.LET___V24*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3907:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3907:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_CONSLISTBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_111_warmelt_genobj_COMPILOBJ_CONSLISTBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un *
							meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un *
							meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 45
    melt_ptr_t mcfr_varptr[45];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct
	 frame_meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 45; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND nbval 45*/
  meltfram__.mcfr_nbvar = 45 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("COMPILOBJ_CONSINSTANCEBIND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3943:/ getarg");
 /*_.BIND__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.GCX__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2])) != NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3944:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3944:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3944:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3944;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "compilobj_consinstancebind bind=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.BIND__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V6*/ meltfptr[5];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3944:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V6*/ meltfptr[5] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3944:/ quasiblock");


      /*_.PROGN___V7*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V7*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3944:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V7*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3945:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_NORMAL_CONSTRUCTED_INSTANCE_BINDING */ meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3945:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3945:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check bind"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3945) ? (3945) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[4] = /*_.IFELSE___V9*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3945:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3946:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.GCX__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_C_GENERATION_CONTEXT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:3946:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3946:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check gcx"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3946) ? (3946) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[5] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3946:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3947:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "BINDER");
  /*_.SYMB__V13*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3948:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NCONSB_LOC");
  /*_.NLOC__V14*/ meltfptr[13] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3949:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "NCONSB_DISCR");
  /*_.NDISCR__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3950:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "NCONSB_NLETREC");
  /*_.NLETREC__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3951:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "NINSTB_SLOTS");
  /*_.NSLOTS__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.NAMBUF__V18*/ meltfptr[17] =
      (melt_ptr_t)
      meltgc_new_strbuf ((meltobject_ptr_t)
			 (( /*!DISCR_STRBUF */ meltfrout->tabval[3])),
			 (const char *) 0);;
    MELT_LOCATION ("warmelt-genobj.melt:3953:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.ODISCR__V19*/ meltfptr[18] =
	meltgc_send ((melt_ptr_t) ( /*_.NDISCR__V15*/ meltfptr[14]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3954:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.NLETREC__V16*/ meltfptr[15]),
					(melt_ptr_t) (( /*!CLASS_NREP_LETREC */ meltfrout->tabval[5])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.NLETREC__V16*/ meltfptr[15]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "NLETREC_LOCSYMS");
   /*_.NLOCSYMS__V20*/ meltfptr[19] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NLOCSYMS__V20*/ meltfptr[19] = NULL;;
      }
    ;
    /*^compute */
 /*_#BINDNUM__L5*/ meltfnum[1] =
      (melt_get_int ((melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1])));;
    /*^compute */
 /*_.LOCSYMOCC__V21*/ meltfptr[20] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.NLOCSYMS__V20*/ meltfptr[19]),
	( /*_#BINDNUM__L5*/ meltfnum[1])));;
    MELT_LOCATION ("warmelt-genobj.melt:3957:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.BIND__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_NORMAL_CONSTRUCTED_INSTANCE_BINDING */ meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.BIND__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 5, "NINSTB_CLABIND");
   /*_.NCLABIND__V22*/ meltfptr[21] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.NCLABIND__V22*/ meltfptr[21] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3959:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[17]),
			   ("rinst_"));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3960:/ locexp");
      meltgc_add_strbuf_dec ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[17]),
			     ( /*_#BINDNUM__L5*/ meltfnum[1]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3961:/ locexp");
      /*add2sbuf_strconst */
	meltgc_add_strbuf ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[17]),
			   ("__"));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3962:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SYMB__V13*/ meltfptr[12]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "NAMED_NAME");
  /*_.NAMED_NAME__V23*/ meltfptr[22] = slot;
    };
    ;

    {
      /*^locexp */
      meltgc_add_strbuf_cident ((melt_ptr_t)
				( /*_.NAMBUF__V18*/ meltfptr[17]),
				melt_string_str ((melt_ptr_t)
						 ( /*_.NAMED_NAME__V23*/
						  meltfptr[22])));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3963:/ quasiblock");


    MELT_LOCATION ("warmelt-genobj.melt:3964:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^msend */
    /*msend */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^ojbmsend.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.GCX__V3*/ meltfptr[2];
      /*_.OLOC__V25*/ meltfptr[24] =
	meltgc_send ((melt_ptr_t) ( /*_.LOCSYMOCC__V21*/ meltfptr[20]),
		     (melt_ptr_t) (( /*!COMPILE_OBJ */ meltfrout->tabval[4])),
		     (MELTBPARSTR_PTR ""), argtab, "",
		     (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3965:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L6*/ meltfnum[0] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.NCLABIND__V22*/ meltfptr[21]),
			   (melt_ptr_t) (( /*!CLASS_VALUE_BINDING */
					  meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-genobj.melt:3965:/ cond");
    /*cond */ if ( /*_#IS_A__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:3966:/ getslot");
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj = (melt_ptr_t) ( /*_.NCLABIND__V22*/ meltfptr[21]) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 1, "VBIND_VALUE");
    /*_.VBIND_VALUE__V27*/ meltfptr[26] = slot;
	  };
	  ;
	  /*_.CLAS__V26*/ meltfptr[25] = /*_.VBIND_VALUE__V27*/ meltfptr[26];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:3965:/ clear");
	     /*clear *//*_.VBIND_VALUE__V27*/ meltfptr[26] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:3967:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L7*/ meltfnum[6] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.NCLABIND__V22*/ meltfptr[21]),
				 (melt_ptr_t) (( /*!CLASS_CLASS_BINDING */
						meltfrout->tabval[7])));;
	  MELT_LOCATION ("warmelt-genobj.melt:3967:/ cond");
	  /*cond */ if ( /*_#IS_A__L7*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:3968:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.NCLABIND__V22*/ meltfptr[21]) /*=obj*/
		    ;
		  melt_object_get_field (slot, obj, 3, "CBIND_CLASS");
      /*_.CBIND_CLASS__V29*/ meltfptr[28] = slot;
		};
		;
		/*_.IFELSE___V28*/ meltfptr[26] =
		  /*_.CBIND_CLASS__V29*/ meltfptr[28];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:3967:/ clear");
	       /*clear *//*_.CBIND_CLASS__V29*/ meltfptr[28] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:3970:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L8*/ meltfnum[7] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:3970:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-genobj.melt:3970:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-genobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 3970;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "compilobj_consinstancebind bad nclabind=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.NCLABIND__V22*/ meltfptr[21];
			  /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[0])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V31*/ meltfptr[30] =
			  /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:3970:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V31*/ meltfptr[30] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-genobj.melt:3970:/ quasiblock");


		  /*_.PROGN___V33*/ meltfptr[31] =
		    /*_.IF___V31*/ meltfptr[30];;
		  /*^compute */
		  /*_.IFCPP___V30*/ meltfptr[28] =
		    /*_.PROGN___V33*/ meltfptr[31];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:3970:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[7] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V31*/ meltfptr[30] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V33*/ meltfptr[31] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V30*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:3971:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (( /*nil */ NULL))	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V35*/ meltfptr[31] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-genobj.melt:3971:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("unexpected class binding"),
					      ("warmelt-genobj.melt")
					      ? ("warmelt-genobj.melt") :
					      __FILE__,
					      (3971) ? (3971) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V35*/ meltfptr[31] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V34*/ meltfptr[30] =
		    /*_.IFELSE___V35*/ meltfptr[31];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:3971:/ clear");
		 /*clear *//*_.IFELSE___V35*/ meltfptr[31] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V34*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-genobj.melt:3969:/ quasiblock");


		/*_.PROGN___V36*/ meltfptr[31] =
		  /*_.IFCPP___V34*/ meltfptr[30];;
		/*^compute */
		/*_.IFELSE___V28*/ meltfptr[26] =
		  /*_.PROGN___V36*/ meltfptr[31];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:3967:/ clear");
	       /*clear *//*_.IFCPP___V30*/ meltfptr[28] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V34*/ meltfptr[30] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V36*/ meltfptr[31] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.CLAS__V26*/ meltfptr[25] = /*_.IFELSE___V28*/ meltfptr[26];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:3965:/ clear");
	     /*clear *//*_#IS_A__L7*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V28*/ meltfptr[26] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3972:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_.STRBUF2STRING__V37*/ meltfptr[28] =
      (meltgc_new_stringdup
       ((meltobject_ptr_t) (( /*!DISCR_STRING */ meltfrout->tabval[10])),
	melt_strbuf_str ((melt_ptr_t) ( /*_.NAMBUF__V18*/ meltfptr[17]))));;
    MELT_LOCATION ("warmelt-genobj.melt:3972:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJINITOBJECT */
					     meltfrout->tabval[8])), (7),
			      "CLASS_OBJINITOBJECT");
  /*_.INST__V39*/ meltfptr[31] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBV_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V39*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V39*/ meltfptr[31]), (0),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[9])),
			  "OBV_TYPE");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_DISCR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V39*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V39*/ meltfptr[31]), (3),
			  ( /*_.ODISCR__V19*/ meltfptr[18]), "OIE_DISCR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_LOCVAR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V39*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V39*/ meltfptr[31]), (4),
			  ( /*_.OLOC__V25*/ meltfptr[24]), "OIE_LOCVAR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIE_CNAME",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V39*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V39*/ meltfptr[31]), (1),
			  ( /*_.STRBUF2STRING__V37*/ meltfptr[28]),
			  "OIE_CNAME");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OIO_CLASS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V39*/ meltfptr[31])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V39*/ meltfptr[31]), (6),
			  ( /*_.CLAS__V26*/ meltfptr[25]), "OIO_CLASS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V39*/ meltfptr[31],
				  "newly made instance");
    ;
    /*_.ININST__V38*/ meltfptr[30] = /*_.INST__V39*/ meltfptr[31];;
    MELT_LOCATION ("warmelt-genobj.melt:3981:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.CLAS__V26*/ meltfptr[25]),
					(melt_ptr_t) (( /*!CLASS_CLASS */
						       meltfrout->
						       tabval[11])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.CLAS__V26*/ meltfptr[25]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 6, "CLASS_FIELDS");
   /*_.CLASS_FIELDS__V40*/ meltfptr[26] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.CLASS_FIELDS__V40*/ meltfptr[26] = NULL;;
      }
    ;
    /*^compute */
 /*_#MULTIPLE_LENGTH__L10*/ meltfnum[8] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.CLASS_FIELDS__V40*/ meltfptr[26])));;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3981:/ locexp");
      melt_put_int ((melt_ptr_t) ( /*_.ININST__V38*/ meltfptr[30]),
		    ( /*_#MULTIPLE_LENGTH__L10*/ meltfnum[8]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3982:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:3982:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[6] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:3982:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[6];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 3982;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"compilobj_consinstancebind return ininst=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.ININST__V38*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V42*/ meltfptr[41] =
	      /*_.MELT_DEBUG_FUN__V43*/ meltfptr[42];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:3982:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V43*/ meltfptr[42] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V42*/ meltfptr[41] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:3982:/ quasiblock");


      /*_.PROGN___V44*/ meltfptr[42] = /*_.IF___V42*/ meltfptr[41];;
      /*^compute */
      /*_.IFCPP___V41*/ meltfptr[40] = /*_.PROGN___V44*/ meltfptr[42];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3982:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V42*/ meltfptr[41] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V44*/ meltfptr[42] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V41*/ meltfptr[40] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3983:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.ININST__V38*/ meltfptr[30];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3983:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V24*/ meltfptr[23] = /*_.RETURN___V45*/ meltfptr[41];;

    MELT_LOCATION ("warmelt-genobj.melt:3963:/ clear");
	   /*clear *//*_.OLOC__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.CLAS__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.STRBUF2STRING__V37*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.ININST__V38*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.CLASS_FIELDS__V40*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L10*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V45*/ meltfptr[41] = 0;
    /*_.LET___V12*/ meltfptr[10] = /*_.LET___V24*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-genobj.melt:3947:/ clear");
	   /*clear *//*_.SYMB__V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.NLOC__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.NDISCR__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.NLETREC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.NSLOTS__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.NAMBUF__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.ODISCR__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.NLOCSYMS__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#BINDNUM__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.LOCSYMOCC__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.NCLABIND__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.NAMED_NAME__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.LET___V24*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3943:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3943:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("COMPILOBJ_CONSINSTANCEBIND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_112_warmelt_genobj_COMPILOBJ_CONSINSTANCEBIND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un *
						 meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un *
						 meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 42
    melt_ptr_t mcfr_varptr[42];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 42; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE nbval 42*/
  meltfram__.mcfr_nbvar = 42 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_OBJVALUE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:3988:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3989:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJVALUE */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:3989:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3989:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3989) ? (3989) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3989:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:3990:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:3990:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:3990:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check desto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (3990) ? (3990) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:3990:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3991:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBV_TYPE");
  /*_.TYPRECV__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3992:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DESTO__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_OBJVALUE */
						       meltfrout->tabval[0])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "OBV_TYPE");
   /*_.TYPDESTO__V10*/ meltfptr[9] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.TYPDESTO__V10*/ meltfptr[9] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:3996:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L3*/ meltfnum[0] =
      (( /*_.TYPRECV__V9*/ meltfptr[8]) ==
       (( /*!CTYPE_VOID */ meltfrout->tabval[2])));;
    MELT_LOCATION ("warmelt-genobj.melt:3996:/ cond");
    /*cond */ if ( /*_#__L3*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:3997:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L4*/ meltfnum[3] =
	    (( /*_.TYPDESTO__V10*/ meltfptr[9]) ==
	     (( /*!CTYPE_VOID */ meltfrout->tabval[2])));;
	  MELT_LOCATION ("warmelt-genobj.melt:3997:/ cond");
	  /*cond */ if ( /*_#__L4*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:3998:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = /*_.RECV__V2*/ meltfptr[1];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:3998:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.IFELSE___V12*/ meltfptr[11] =
		  /*_.RETURN___V13*/ meltfptr[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:3997:/ clear");
	       /*clear *//*_.RETURN___V13*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-genobj.melt:4001:/ quasiblock");


     /*_.OBODL__V15*/ meltfptr[14] =
		  (meltgc_new_list
		   ((meltobject_ptr_t)
		    (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
		MELT_LOCATION ("warmelt-genobj.melt:4003:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_OBJPLAINBLOCK */ meltfrout->tabval[4])), (3), "CLASS_OBJPLAINBLOCK");
      /*_.INST__V17*/ meltfptr[16] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBLO_BODYL",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V17*/
						   meltfptr[16])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (1),
				      ( /*_.OBODL__V15*/ meltfptr[14]),
				      "OBLO_BODYL");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBLO_EPIL",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V17*/
						   meltfptr[16])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (2),
				      (( /*nil */ NULL)), "OBLO_EPIL");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V17*/ meltfptr[16],
					      "newly made instance");
		;
		/*_.OBLK__V16*/ meltfptr[15] = /*_.INST__V17*/ meltfptr[16];;
		MELT_LOCATION ("warmelt-genobj.melt:4009:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_OBJCLEAR */
							 meltfrout->
							 tabval[5])), (2),
					  "CLASS_OBJCLEAR");
      /*_.INST__V19*/ meltfptr[18] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OCLR_VLOC",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V19*/
						   meltfptr[18])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (1),
				      ( /*_.DESTO__V3*/ meltfptr[2]),
				      "OCLR_VLOC");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V19*/ meltfptr[18],
					      "newly made instance");
		;
		/*_.OCLR__V18*/ meltfptr[17] = /*_.INST__V19*/ meltfptr[18];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4014:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OBODL__V15*/ meltfptr[14]),
				      (melt_ptr_t) ( /*_.RECV__V2*/
						    meltfptr[1]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4015:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OBODL__V15*/ meltfptr[14]),
				      (melt_ptr_t) ( /*_.OCLR__V18*/
						    meltfptr[17]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4018:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.OBODL__V15*/ meltfptr[14]),
				      (melt_ptr_t) ( /*_.DESTO__V3*/
						    meltfptr[2]));
		}
		;
		MELT_LOCATION ("warmelt-genobj.melt:4019:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = /*_.OBLK__V16*/ meltfptr[15];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4019:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.LET___V14*/ meltfptr[12] =
		  /*_.RETURN___V20*/ meltfptr[19];;

		MELT_LOCATION ("warmelt-genobj.melt:4001:/ clear");
	       /*clear *//*_.OBODL__V15*/ meltfptr[14] = 0;
		/*^clear */
	       /*clear *//*_.OBLK__V16*/ meltfptr[15] = 0;
		/*^clear */
	       /*clear *//*_.OCLR__V18*/ meltfptr[17] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V20*/ meltfptr[19] = 0;
		/*_.IFELSE___V12*/ meltfptr[11] =
		  /*_.LET___V14*/ meltfptr[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:3997:/ clear");
	       /*clear *//*_.LET___V14*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V11*/ meltfptr[10] = /*_.IFELSE___V12*/ meltfptr[11];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:3996:/ clear");
	     /*clear *//*_#__L4*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4022:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#__L5*/ meltfnum[3] =
	    (( /*_.TYPRECV__V9*/ meltfptr[8]) ==
	     ( /*_.TYPDESTO__V10*/ meltfptr[9]));;
	  MELT_LOCATION ("warmelt-genobj.melt:4022:/ cond");
	  /*cond */ if ( /*_#__L5*/ meltfnum[3])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:4023:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#IS_A__L6*/ meltfnum[5] =
		    melt_is_instance_of ((melt_ptr_t)
					 ( /*_.TYPRECV__V9*/ meltfptr[8]),
					 (melt_ptr_t) (( /*!CLASS_CTYPE */
							meltfrout->
							tabval[6])));;
		  MELT_LOCATION ("warmelt-genobj.melt:4023:/ cond");
		  /*cond */ if ( /*_#IS_A__L6*/ meltfnum[5])	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V23*/ meltfptr[17] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-genobj.melt:4023:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("check same typrecv&rtpdesto"),
					      ("warmelt-genobj.melt")
					      ? ("warmelt-genobj.melt") :
					      __FILE__,
					      (4023) ? (4023) : __LINE__,
					      __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V23*/ meltfptr[17] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V22*/ meltfptr[15] =
		    /*_.IFELSE___V23*/ meltfptr[17];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4023:/ clear");
		 /*clear *//*_#IS_A__L6*/ meltfnum[5] = 0;
		  /*^clear */
		 /*clear *//*_.IFELSE___V23*/ meltfptr[17] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V22*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-genobj.melt:4024:/ quasiblock");


     /*_.DESTLIS__V25*/ meltfptr[12] =
		  (meltgc_new_list
		   ((meltobject_ptr_t)
		    (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
		/*^compute */
     /*_.EXPLIS__V26*/ meltfptr[11] =
		  (meltgc_new_list
		   ((meltobject_ptr_t)
		    (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
		MELT_LOCATION ("warmelt-genobj.melt:4026:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*^rawallocobj */
		/*rawallocobj */
		{
		  melt_ptr_t newobj = 0;
		  melt_raw_object_create (newobj,
					  (melt_ptr_t) (( /*!CLASS_OBJCOMPUTE */ meltfrout->tabval[7])), (4), "CLASS_OBJCOMPUTE");
      /*_.INST__V28*/ meltfptr[27] =
		    newobj;
		};
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V28*/
						   meltfptr[27])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (1),
				      ( /*_.DESTLIS__V25*/ meltfptr[12]),
				      "OBDI_DESTLIST");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBCPT_EXPR",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V28*/
						   meltfptr[27])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (2),
				      ( /*_.EXPLIS__V26*/ meltfptr[11]),
				      "OBCPT_EXPR");
		;
		/*^putslot */
		/*putslot */
		melt_assertmsg ("putslot checkobj @OBCPT_TYPE",
				melt_magic_discr ((melt_ptr_t)
						  ( /*_.INST__V28*/
						   meltfptr[27])) ==
				MELTOBMAG_OBJECT);
		melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (3),
				      ( /*_.TYPRECV__V9*/ meltfptr[8]),
				      "OBCPT_TYPE");
		;
		/*^touchobj */

		melt_dbgtrace_written_object ( /*_.INST__V28*/ meltfptr[27],
					      "newly made instance");
		;
		/*_.OBC__V27*/ meltfptr[17] = /*_.INST__V28*/ meltfptr[27];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4033:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.DESTLIS__V25*/ meltfptr[12]),
				      (melt_ptr_t) ( /*_.DESTO__V3*/
						    meltfptr[2]));
		}
		;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4034:/ locexp");
		  meltgc_append_list ((melt_ptr_t)
				      ( /*_.EXPLIS__V26*/ meltfptr[11]),
				      (melt_ptr_t) ( /*_.RECV__V2*/
						    meltfptr[1]));
		}
		;
		MELT_LOCATION ("warmelt-genobj.melt:4035:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = /*_.OBC__V27*/ meltfptr[17];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4035:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.LET___V24*/ meltfptr[19] =
		  /*_.RETURN___V29*/ meltfptr[28];;

		MELT_LOCATION ("warmelt-genobj.melt:4024:/ clear");
	       /*clear *//*_.DESTLIS__V25*/ meltfptr[12] = 0;
		/*^clear */
	       /*clear *//*_.EXPLIS__V26*/ meltfptr[11] = 0;
		/*^clear */
	       /*clear *//*_.OBC__V27*/ meltfptr[17] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V29*/ meltfptr[28] = 0;
		MELT_LOCATION ("warmelt-genobj.melt:4022:/ quasiblock");


		/*_.PROGN___V30*/ meltfptr[12] =
		  /*_.LET___V24*/ meltfptr[19];;
		/*^compute */
		/*_.IFELSE___V21*/ meltfptr[14] =
		  /*_.PROGN___V30*/ meltfptr[12];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:4022:/ clear");
	       /*clear *//*_.IFCPP___V22*/ meltfptr[15] = 0;
		/*^clear */
	       /*clear *//*_.LET___V24*/ meltfptr[19] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V30*/ meltfptr[12] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:4038:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L7*/ meltfnum[5] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-genobj.melt:4038:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[5])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-genobj.melt:4038:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[11];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-genobj.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4038;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "putobjdest_objvalue mismatching recv=";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
			  /*^apply.arg */
			  argtab[5].meltbp_cstring = " typrecv=";
			  /*^apply.arg */
			  argtab[6].meltbp_aptr =
			    (melt_ptr_t *) & /*_.TYPRECV__V9*/ meltfptr[8];
			  /*^apply.arg */
			  argtab[7].meltbp_cstring = " desto=";
			  /*^apply.arg */
			  argtab[8].meltbp_aptr =
			    (melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
			  /*^apply.arg */
			  argtab[9].meltbp_cstring = " typdesto=";
			  /*^apply.arg */
			  argtab[10].meltbp_aptr =
			    (melt_ptr_t *) & /*_.TYPDESTO__V10*/ meltfptr[9];
			  /*_.MELT_DEBUG_FUN__V33*/ meltfptr[28] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[8])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V32*/ meltfptr[17] =
			  /*_.MELT_DEBUG_FUN__V33*/ meltfptr[28];;
			/*epilog */

			MELT_LOCATION ("warmelt-genobj.melt:4038:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[28] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V32*/ meltfptr[17] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-genobj.melt:4038:/ quasiblock");


		  /*_.PROGN___V34*/ meltfptr[15] =
		    /*_.IF___V32*/ meltfptr[17];;
		  /*^compute */
		  /*_.IFCPP___V31*/ meltfptr[11] =
		    /*_.PROGN___V34*/ meltfptr[15];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4038:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[5] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V32*/ meltfptr[17] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V34*/ meltfptr[15] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V31*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4039:/ locexp");
		  melt_puts (stderr,
			     ("putobjdest_objvalue type mismatch : recv <"));
		}
		;
		MELT_LOCATION ("warmelt-genobj.melt:4040:/ quasiblock");


     /*_.DISCR__V35*/ meltfptr[19] =
		  ((melt_ptr_t)
		   (melt_discr
		    ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]))));;
		MELT_LOCATION ("warmelt-genobj.melt:4040:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.DISCR__V35*/ meltfptr[19]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V36*/ meltfptr[12] = slot;
		};
		;

		{
		  /*^locexp */
		  melt_putstr (stderr,
			       (melt_ptr_t) ( /*_.NAMED_NAME__V36*/
					     meltfptr[12]));
		}
		;

		/*^clear */
	       /*clear *//*_.DISCR__V35*/ meltfptr[19] = 0;
		/*^clear */
	       /*clear *//*_.NAMED_NAME__V36*/ meltfptr[12] = 0;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4041:/ locexp");
		  melt_puts (stderr, ("> & desto<"));
		}
		;
		MELT_LOCATION ("warmelt-genobj.melt:4042:/ quasiblock");


     /*_.DISCR__V37*/ meltfptr[28] =
		  ((melt_ptr_t)
		   (melt_discr
		    ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]))));;
		MELT_LOCATION ("warmelt-genobj.melt:4042:/ getslot");
		{
		  melt_ptr_t slot = NULL, obj = NULL;
		  obj =
		    (melt_ptr_t) ( /*_.DISCR__V37*/ meltfptr[28]) /*=obj*/ ;
		  melt_object_get_field (slot, obj, 1, "NAMED_NAME");
      /*_.NAMED_NAME__V38*/ meltfptr[17] = slot;
		};
		;

		{
		  /*^locexp */
		  melt_putstr (stderr,
			       (melt_ptr_t) ( /*_.NAMED_NAME__V38*/
					     meltfptr[17]));
		}
		;

		/*^clear */
	       /*clear *//*_.DISCR__V37*/ meltfptr[28] = 0;
		/*^clear */
	       /*clear *//*_.NAMED_NAME__V38*/ meltfptr[17] = 0;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4043:/ locexp");
		  melt_puts (stderr, (">"));
		}
		;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4044:/ locexp");
		  melt_newlineflush (stderr);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-genobj.melt:4045:/ cppif.then");
		/*^block */
		/*anyblock */
		{

		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^cond */
		  /*cond */ if (( /*nil */ NULL))	/*then */
		    {
		      /*^cond.then */
		      /*_.IFELSE___V40*/ meltfptr[19] = ( /*nil */ NULL);;
		    }
		  else
		    {
		      MELT_LOCATION ("warmelt-genobj.melt:4045:/ cond.else");

		      /*^block */
		      /*anyblock */
		      {




			{
			  /*^locexp */
			  melt_assert_failed (("putobjdest_objvalue type mismatch"), ("warmelt-genobj.melt") ? ("warmelt-genobj.melt") : __FILE__, (4045) ? (4045) : __LINE__, __FUNCTION__);
			  ;
			}
			;
		   /*clear *//*_.IFELSE___V40*/ meltfptr[19] = 0;
			/*epilog */
		      }
		      ;
		    }
		  ;
		  /*^compute */
		  /*_.IFCPP___V39*/ meltfptr[15] =
		    /*_.IFELSE___V40*/ meltfptr[19];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-genobj.melt:4045:/ clear");
		 /*clear *//*_.IFELSE___V40*/ meltfptr[19] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V39*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-genobj.melt:4046:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = /*_.RECV__V2*/ meltfptr[1];;

		{
		  MELT_LOCATION ("warmelt-genobj.melt:4046:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-genobj.melt:4037:/ quasiblock");


		/*_.PROGN___V42*/ meltfptr[28] =
		  /*_.RETURN___V41*/ meltfptr[12];;
		/*^compute */
		/*_.IFELSE___V21*/ meltfptr[14] =
		  /*_.PROGN___V42*/ meltfptr[28];;
		/*epilog */

		MELT_LOCATION ("warmelt-genobj.melt:4022:/ clear");
	       /*clear *//*_.IFCPP___V31*/ meltfptr[11] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V39*/ meltfptr[15] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V41*/ meltfptr[12] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V42*/ meltfptr[28] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V11*/ meltfptr[10] = /*_.IFELSE___V21*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:3996:/ clear");
	     /*clear *//*_#__L5*/ meltfnum[3] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V21*/ meltfptr[14] = 0;
	}
	;
      }
    ;
    /*_.LET___V8*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[10];;

    MELT_LOCATION ("warmelt-genobj.melt:3991:/ clear");
	   /*clear *//*_.TYPRECV__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.TYPDESTO__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_#__L3*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:3988:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:3988:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_OBJVALUE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_113_warmelt_genobj_PUTOBJDEST_OBJVALUE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 24
    melt_ptr_t mcfr_varptr[24];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 24; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER nbval 24*/
  meltfram__.mcfr_nbvar = 24 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_INTEGER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4053:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4054:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4054:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    /*^compute */
     /*_.DISCRIM__V6*/ meltfptr[5] =
	      ((melt_ptr_t)
	       (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]))));;
	    MELT_LOCATION ("warmelt-genobj.melt:4054:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[11];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4054;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest_integer recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = "\n* desto=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
	      /*^apply.arg */
	      argtab[7].meltbp_cstring = "\n* of discrim:";
	      /*^apply.arg */
	      argtab[8].meltbp_aptr =
		(melt_ptr_t *) & /*_.DISCRIM__V6*/ meltfptr[5];
	      /*^apply.arg */
	      argtab[9].meltbp_cstring = "\n* class_objlocv=";
	      /*^apply.arg */
	      argtab[10].meltbp_aptr =
		(melt_ptr_t *) & ( /*!CLASS_OBJLOCV */ meltfrout->tabval[1]);
	      /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V5*/ meltfptr[4] = /*_.MELT_DEBUG_FUN__V7*/ meltfptr[6];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4054:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.DISCRIM__V6*/ meltfptr[5] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V7*/ meltfptr[6] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V5*/ meltfptr[4] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4054:/ quasiblock");


      /*_.PROGN___V8*/ meltfptr[5] = /*_.IF___V5*/ meltfptr[4];;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.PROGN___V8*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4054:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V5*/ meltfptr[4] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V8*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4056:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_INTEGERBOX__L3*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])) ==
	 MELTOBMAG_INT);;
      MELT_LOCATION ("warmelt-genobj.melt:4056:/ cond");
      /*cond */ if ( /*_#IS_INTEGERBOX__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4056:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4056) ? (4056) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[6] = /*_.IFELSE___V10*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4056:/ clear");
	     /*clear *//*_#IS_INTEGERBOX__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4057:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-genobj.melt:4057:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4057:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check desto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4057) ? (4057) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[5] = /*_.IFELSE___V12*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4057:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4058:/ quasiblock");


 /*_.DESTLIS__V14*/ meltfptr[13] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
    /*^compute */
 /*_.EXPLIS__V15*/ meltfptr[14] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[3]))));;
    MELT_LOCATION ("warmelt-genobj.melt:4060:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.DESTO__V3*/ meltfptr[2]),
					(melt_ptr_t) (( /*!CLASS_OBJVALUE */
						       meltfrout->tabval[4])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 0, "OBV_TYPE");
   /*_.TYPDESTO__V16*/ meltfptr[15] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.TYPDESTO__V16*/ meltfptr[15] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4061:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMPUTE */
					     meltfrout->tabval[5])), (4),
			      "CLASS_OBJCOMPUTE");
  /*_.INST__V18*/ meltfptr[17] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (1),
			  ( /*_.DESTLIS__V14*/ meltfptr[13]),
			  "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (2),
			  ( /*_.EXPLIS__V15*/ meltfptr[14]), "OBCPT_EXPR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V18*/ meltfptr[17])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V18*/ meltfptr[17]), (3),
			  (( /*!CTYPE_LONG */ meltfrout->tabval[6])),
			  "OBCPT_TYPE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V18*/ meltfptr[17],
				  "newly made instance");
    ;
    /*_.OBC__V17*/ meltfptr[16] = /*_.INST__V18*/ meltfptr[17];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4068:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#__L5*/ meltfnum[1] =
	(( /*_.TYPDESTO__V16*/ meltfptr[15]) ==
	 (( /*!CTYPE_LONG */ meltfrout->tabval[6])));;
      MELT_LOCATION ("warmelt-genobj.melt:4068:/ cond");
      /*cond */ if ( /*_#__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4068:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check typdesto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4068) ? (4068) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[18] = /*_.IFELSE___V20*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4068:/ clear");
	     /*clear *//*_#__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4069:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.DESTLIS__V14*/ meltfptr[13]),
			  (melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4070:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.EXPLIS__V15*/ meltfptr[14]),
			  (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4071:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4071:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4071:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4071;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest_integer return obc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBC__V17*/ meltfptr[16];
	      /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V22*/ meltfptr[21] =
	      /*_.MELT_DEBUG_FUN__V23*/ meltfptr[22];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4071:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V23*/ meltfptr[22] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V22*/ meltfptr[21] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4071:/ quasiblock");


      /*_.PROGN___V24*/ meltfptr[22] = /*_.IF___V22*/ meltfptr[21];;
      /*^compute */
      /*_.IFCPP___V21*/ meltfptr[19] = /*_.PROGN___V24*/ meltfptr[22];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4071:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V22*/ meltfptr[21] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V24*/ meltfptr[22] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V21*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V13*/ meltfptr[4] = /*_.OBC__V17*/ meltfptr[16];;

    MELT_LOCATION ("warmelt-genobj.melt:4058:/ clear");
	   /*clear *//*_.DESTLIS__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.EXPLIS__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.TYPDESTO__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.OBC__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V21*/ meltfptr[19] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4053:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V13*/ meltfptr[4];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4053:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V13*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_INTEGER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_114_warmelt_genobj_PUTOBJDEST_INTEGER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_genobj_PUTOBJDEST_STRING (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_115_warmelt_genobj_PUTOBJDEST_STRING_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_115_warmelt_genobj_PUTOBJDEST_STRING_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 23
    melt_ptr_t mcfr_varptr[23];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_115_warmelt_genobj_PUTOBJDEST_STRING is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_115_warmelt_genobj_PUTOBJDEST_STRING_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 23; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_115_warmelt_genobj_PUTOBJDEST_STRING nbval 23*/
  meltfram__.mcfr_nbvar = 23 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_STRING", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4080:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4081:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1])) ==
	 MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-genobj.melt:4081:/ cond");
      /*cond */ if ( /*_#IS_STRING__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4081:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4081) ? (4081) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4081:/ clear");
	     /*clear *//*_#IS_STRING__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4082:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4082:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4082:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check desto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4082) ? (4082) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4082:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4083:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4083:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4083:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4083;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest_string recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " desto=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4083:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4083:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4083:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4084:/ quasiblock");


 /*_.DESTLIS__V13*/ meltfptr[9] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
    /*^compute */
 /*_.EXPLIS__V14*/ meltfptr[13] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
    MELT_LOCATION ("warmelt-genobj.melt:4086:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBV_TYPE");
  /*_.TYPDESTO__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4087:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMPUTE */
					     meltfrout->tabval[3])), (4),
			      "CLASS_OBJCOMPUTE");
  /*_.INST__V17*/ meltfptr[16] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (1),
			  ( /*_.DESTLIS__V13*/ meltfptr[9]), "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (2),
			  ( /*_.EXPLIS__V14*/ meltfptr[13]), "OBCPT_EXPR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (3),
			  (( /*!CTYPE_CSTRING */ meltfrout->tabval[4])),
			  "OBCPT_TYPE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V17*/ meltfptr[16],
				  "newly made instance");
    ;
    /*_.OBC__V16*/ meltfptr[15] = /*_.INST__V17*/ meltfptr[16];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4094:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#__L5*/ meltfnum[3] =
	(( /*_.TYPDESTO__V15*/ meltfptr[14]) ==
	 (( /*!CTYPE_CSTRING */ meltfrout->tabval[4])));;
      MELT_LOCATION ("warmelt-genobj.melt:4094:/ cond");
      /*cond */ if ( /*_#__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4094:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check typdesto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4094) ? (4094) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[17] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4094:/ clear");
	     /*clear *//*_#__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4095:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.DESTLIS__V13*/ meltfptr[9]),
			  (melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]));
    }
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4096:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.EXPLIS__V14*/ meltfptr[13]),
			  (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4097:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4097:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4097:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4097;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest_string return obc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBC__V16*/ meltfptr[15];
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V21*/ meltfptr[20] =
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4097:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V21*/ meltfptr[20] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4097:/ quasiblock");


      /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[18] = /*_.PROGN___V23*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4097:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[18] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V12*/ meltfptr[8] = /*_.OBC__V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-genobj.melt:4084:/ clear");
	   /*clear *//*_.DESTLIS__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.EXPLIS__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.TYPDESTO__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OBC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4080:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4080:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_STRING", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_115_warmelt_genobj_PUTOBJDEST_STRING_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_115_warmelt_genobj_PUTOBJDEST_STRING */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_genobj_PUTOBJDEST_NULL (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_116_warmelt_genobj_PUTOBJDEST_NULL_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_116_warmelt_genobj_PUTOBJDEST_NULL_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 22
    melt_ptr_t mcfr_varptr[22];
#define MELTFRAM_NBVARNUM 8
    long mcfr_varnum[8];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_116_warmelt_genobj_PUTOBJDEST_NULL is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_116_warmelt_genobj_PUTOBJDEST_NULL_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 22; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_116_warmelt_genobj_PUTOBJDEST_NULL nbval 22*/
  meltfram__.mcfr_nbvar = 22 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_NULL", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4103:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4104:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#NULL__L1*/ meltfnum[0] =
	(( /*_.RECV__V2*/ meltfptr[1]) == NULL);;
      MELT_LOCATION ("warmelt-genobj.melt:4104:/ cond");
      /*cond */ if ( /*_#NULL__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4104:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4104) ? (4104) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4104:/ clear");
	     /*clear *//*_#NULL__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4105:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4105:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4105:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check desto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4105) ? (4105) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4105:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4106:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4106:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4106:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4106;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest_null recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " desto=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V9*/ meltfptr[8] =
	      /*_.MELT_DEBUG_FUN__V10*/ meltfptr[9];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4106:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V10*/ meltfptr[9] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V9*/ meltfptr[8] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4106:/ quasiblock");


      /*_.PROGN___V11*/ meltfptr[9] = /*_.IF___V9*/ meltfptr[8];;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.PROGN___V11*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4106:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V9*/ meltfptr[8] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V11*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4107:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MELT_NEED_DBG__L5*/ meltfnum[3] =
      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
      0				/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
      ;;
    MELT_LOCATION ("warmelt-genobj.melt:4107:/ cond");
    /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */

#if MELT_HAVE_DEBUG
	    if (melt_need_debug (0))
	      melt_dbgshortbacktrace (("putobjdest_null"), (15));
#endif
	    ;
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4108:/ quasiblock");


 /*_.DESTLIS__V13*/ meltfptr[9] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
    /*^compute */
 /*_.EXPLIS__V14*/ meltfptr[13] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
    MELT_LOCATION ("warmelt-genobj.melt:4110:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 0, "OBV_TYPE");
  /*_.TYPDESTO__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4111:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMPUTE */
					     meltfrout->tabval[3])), (4),
			      "CLASS_OBJCOMPUTE");
  /*_.INST__V17*/ meltfptr[16] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (1),
			  ( /*_.DESTLIS__V13*/ meltfptr[9]), "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (2),
			  ( /*_.EXPLIS__V14*/ meltfptr[13]), "OBCPT_EXPR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V17*/ meltfptr[16])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V17*/ meltfptr[16]), (3),
			  ( /*_.TYPDESTO__V15*/ meltfptr[14]), "OBCPT_TYPE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V17*/ meltfptr[16],
				  "newly made instance");
    ;
    /*_.OBC__V16*/ meltfptr[15] = /*_.INST__V17*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4118:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.DESTLIS__V13*/ meltfptr[9]),
			  (melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]));
    }
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4119:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#__L6*/ meltfnum[0] =
      (( /*_.TYPDESTO__V15*/ meltfptr[14]) ==
       (( /*!CTYPE_LONG */ meltfrout->tabval[4])));;
    MELT_LOCATION ("warmelt-genobj.melt:4119:/ cond");
    /*cond */ if ( /*_#__L6*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

   /*_.MAKE_INTEGERBOX__V18*/ meltfptr[17] =
	    (meltgc_new_int
	     ((meltobject_ptr_t)
	      (( /*!DISCR_INTEGER */ meltfrout->tabval[5])), (0)));;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4120:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.EXPLIS__V14*/ meltfptr[13]),
				(melt_ptr_t) ( /*_.MAKE_INTEGERBOX__V18*/
					      meltfptr[17]));
	  }
	  ;
	  /*epilog */

	  MELT_LOCATION ("warmelt-genobj.melt:4119:/ clear");
	     /*clear *//*_.MAKE_INTEGERBOX__V18*/ meltfptr[17] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4121:/ locexp");
	    meltgc_append_list ((melt_ptr_t)
				( /*_.EXPLIS__V14*/ meltfptr[13]),
				(melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]));
	  }
	  ;
	  /*epilog */
	}
	;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4122:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L7*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4122:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L7*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4122:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4122;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest_null return obc=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.OBC__V16*/ meltfptr[15];
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V20*/ meltfptr[19] =
	      /*_.MELT_DEBUG_FUN__V21*/ meltfptr[20];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4122:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L8*/ meltfnum[7] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V21*/ meltfptr[20] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V20*/ meltfptr[19] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4122:/ quasiblock");


      /*_.PROGN___V22*/ meltfptr[20] = /*_.IF___V20*/ meltfptr[19];;
      /*^compute */
      /*_.IFCPP___V19*/ meltfptr[17] = /*_.PROGN___V22*/ meltfptr[20];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4122:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L7*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V20*/ meltfptr[19] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V22*/ meltfptr[20] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V19*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V12*/ meltfptr[8] = /*_.OBC__V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-genobj.melt:4108:/ clear");
	   /*clear *//*_.DESTLIS__V13*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.EXPLIS__V14*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.TYPDESTO__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.OBC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_#__L6*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V19*/ meltfptr[17] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4103:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V12*/ meltfptr[8];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4103:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V12*/ meltfptr[8] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_NULL", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_116_warmelt_genobj_PUTOBJDEST_NULL_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_116_warmelt_genobj_PUTOBJDEST_NULL */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un *
						    meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un *
						    meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 17
    melt_ptr_t mcfr_varptr[17];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 17; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK nbval 17*/
  meltfram__.mcfr_nbvar = 17 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_OBJANYBLOCK", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4128:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4129:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJANYBLOCK */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4129:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4129:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4129) ? (4129) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4129:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4130:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4130:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4130:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check desto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4130) ? (4130) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4130:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4131:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "OBLO_BODYL");
  /*_.OBL__V9*/ meltfptr[8] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4132:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBLO_EPIL");
  /*_.OEP__V10*/ meltfptr[9] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4134:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST_OR_NULL__L3*/ meltfnum[0] =
	(( /*_.OBL__V9*/ meltfptr[8]) == NULL
	 ||
	 (melt_unsafe_magic_discr ((melt_ptr_t) ( /*_.OBL__V9*/ meltfptr[8]))
	  == MELTOBMAG_LIST));;
      MELT_LOCATION ("warmelt-genobj.melt:4134:/ cond");
      /*cond */ if ( /*_#IS_LIST_OR_NULL__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V12*/ meltfptr[11] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4134:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check obl"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4134) ? (4134) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[10] = /*_.IFELSE___V12*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4134:/ clear");
	     /*clear *//*_#IS_LIST_OR_NULL__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V12*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4135:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST_OR_NULL__L4*/ meltfnum[0] =
	(( /*_.OEP__V10*/ meltfptr[9]) == NULL
	 ||
	 (melt_unsafe_magic_discr ((melt_ptr_t) ( /*_.OEP__V10*/ meltfptr[9]))
	  == MELTOBMAG_LIST));;
      MELT_LOCATION ("warmelt-genobj.melt:4135:/ cond");
      /*cond */ if ( /*_#IS_LIST_OR_NULL__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4135:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check oep"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4135) ? (4135) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[11] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4135:/ clear");
	     /*clear *//*_#IS_LIST_OR_NULL__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4137:/ quasiblock");


 /*_.LPBY__V15*/ meltfptr[13] =
      (melt_list_last ((melt_ptr_t) ( /*_.OBL__V9*/ meltfptr[8])));;
    /*^compute */
 /*_.LASBP__V16*/ meltfptr[15] =
      (melt_pair_head ((melt_ptr_t) ( /*_.LPBY__V15*/ meltfptr[13])));;
    MELT_LOCATION ("warmelt-genobj.melt:4140:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^cond */
    /*cond */ if ( /*_.LASBP__V16*/ meltfptr[15])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-genobj.melt:4141:/ quasiblock");


	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
	    /*_.UPLASB__V17*/ meltfptr[16] =
	      meltgc_send ((melt_ptr_t) ( /*_.LASBP__V16*/ meltfptr[15]),
			   (melt_ptr_t) (( /*!PUT_OBJDEST */ meltfrout->
					  tabval[2])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;

	  {
	    MELT_LOCATION ("warmelt-genobj.melt:4142:/ locexp");
	    meltgc_pair_set_head ((melt_ptr_t)
				  ( /*_.LPBY__V15*/ meltfptr[13]),
				  ( /*_.UPLASB__V17*/ meltfptr[16]));
	  }
	  ;

	  MELT_LOCATION ("warmelt-genobj.melt:4141:/ clear");
	     /*clear *//*_.UPLASB__V17*/ meltfptr[16] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

    MELT_LOCATION ("warmelt-genobj.melt:4137:/ clear");
	   /*clear *//*_.LPBY__V15*/ meltfptr[13] = 0;
    /*^clear */
	   /*clear *//*_.LASBP__V16*/ meltfptr[15] = 0;
    /*_.LET___V8*/ meltfptr[6] = /*_.RECV__V2*/ meltfptr[1];;

    MELT_LOCATION ("warmelt-genobj.melt:4131:/ clear");
	   /*clear *//*_.OBL__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.OEP__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[11] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4128:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V8*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4128:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V8*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_OBJANYBLOCK", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_117_warmelt_genobj_PUTOBJDEST_OBJANYBLOCK */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK (meltclosure_ptr_t
						      meltclosp_,
						      melt_ptr_t
						      meltfirstargp_,
						      const
						      melt_argdescr_cell_t
						      meltxargdescr_[],
						      union meltparam_un *
						      meltxargtab_,
						      const
						      melt_argdescr_cell_t
						      meltxresdescr_[],
						      union meltparam_un *
						      meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 19
    melt_ptr_t mcfr_varptr[19];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 19; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK nbval 19*/
  meltfram__.mcfr_nbvar = 19 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_OBJMULTIBLOCK", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4153:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4154:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJMULTIBLOCK */
					    meltfrout->tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4154:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4154:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4154) ? (4154) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4154:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4155:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L2*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4155:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4155:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[7];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4155;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest_objmultiblock recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*^apply.arg */
	      argtab[5].meltbp_cstring = " desto=";
	      /*^apply.arg */
	      argtab[6].meltbp_aptr =
		(melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4155:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L3*/ meltfnum[2] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4155:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4155:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4156:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 3, "OMULBLO_SUBCOMP");
  /*_.OSUBCOMP__V11*/ meltfptr[7] = slot;
    };
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4157:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_MULTIPLE_OR_NULL__L4*/ meltfnum[2] =
	(( /*_.OSUBCOMP__V11*/ meltfptr[7]) == NULL
	 ||
	 (melt_unsafe_magic_discr
	  ((melt_ptr_t) ( /*_.OSUBCOMP__V11*/ meltfptr[7])) ==
	  MELTOBMAG_MULTIPLE));;
      MELT_LOCATION ("warmelt-genobj.melt:4157:/ cond");
      /*cond */ if ( /*_#IS_MULTIPLE_OR_NULL__L4*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4157:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check osubcomp"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4157) ? (4157) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[11] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4157:/ clear");
	     /*clear *//*_#IS_MULTIPLE_OR_NULL__L4*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[11] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*citerblock FOREACH_IN_MULTIPLE */
    {
      /* start foreach_in_multiple meltcit1__EACHTUP */
      long meltcit1__EACHTUP_ln =
	melt_multiple_length ((melt_ptr_t) /*_.OSUBCOMP__V11*/ meltfptr[7]);
      for ( /*_#CURIX__L5*/ meltfnum[0] = 0;
	   ( /*_#CURIX__L5*/ meltfnum[0] >= 0)
	   && ( /*_#CURIX__L5*/ meltfnum[0] < meltcit1__EACHTUP_ln);
	/*_#CURIX__L5*/ meltfnum[0]++)
	{
	  /*_.CURSUBCOMP__V14*/ meltfptr[12] =
	    melt_multiple_nth ((melt_ptr_t)
			       ( /*_.OSUBCOMP__V11*/ meltfptr[7]),
			       /*_#CURIX__L5*/ meltfnum[0]);



	  MELT_LOCATION ("warmelt-genobj.melt:4161:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^msend */
	  /*msend */
	  {
	    union meltparam_un argtab[1];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^ojbmsend.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.DESTO__V3*/ meltfptr[2];
	    /*_.PUT_OBJDEST__V15*/ meltfptr[14] =
	      meltgc_send ((melt_ptr_t) ( /*_.CURSUBCOMP__V14*/ meltfptr[12]),
			   (melt_ptr_t) (( /*!PUT_OBJDEST */ meltfrout->
					  tabval[2])), (MELTBPARSTR_PTR ""),
			   argtab, "", (union meltparam_un *) 0);
	  }
	  ;
	  if ( /*_#CURIX__L5*/ meltfnum[0] < 0)
	    break;
	}			/* end  foreach_in_multiple meltcit1__EACHTUP */

      /*citerepilog */

      MELT_LOCATION ("warmelt-genobj.melt:4158:/ clear");
	    /*clear *//*_.CURSUBCOMP__V14*/ meltfptr[12] = 0;
      /*^clear */
	    /*clear *//*_#CURIX__L5*/ meltfnum[0] = 0;
      /*^clear */
	    /*clear *//*_.PUT_OBJDEST__V15*/ meltfptr[14] = 0;
    }				/*endciterblock FOREACH_IN_MULTIPLE */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4162:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[2] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4162:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[2])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4162:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4162;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring =
		"putobjdest_objmultiblock done recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[1])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V17*/ meltfptr[16] =
	      /*_.MELT_DEBUG_FUN__V18*/ meltfptr[17];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4162:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V18*/ meltfptr[17] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V17*/ meltfptr[16] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4162:/ quasiblock");


      /*_.PROGN___V19*/ meltfptr[17] = /*_.IF___V17*/ meltfptr[16];;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[15] = /*_.PROGN___V19*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4162:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[2] = 0;
      /*^clear */
	     /*clear *//*_.IF___V17*/ meltfptr[16] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V19*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V10*/ meltfptr[6] = /*_.IFCPP___V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-genobj.melt:4156:/ clear");
	   /*clear *//*_.OSUBCOMP__V11*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-genobj.melt:4153:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V10*/ meltfptr[6];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4153:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LET___V10*/ meltfptr[6] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_OBJMULTIBLOCK", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_118_warmelt_genobj_PUTOBJDEST_OBJMULTIBLOCK */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 20
    melt_ptr_t mcfr_varptr[20];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 20; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP nbval 20*/
  meltfram__.mcfr_nbvar = 20 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_OBJLOOP", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4166:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4167:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOOP */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4167:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4167:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4167) ? (4167) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4167:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4168:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4168:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4168:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check desto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4168) ? (4168) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4168:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4169:/ quasiblock");


    /*^getslot */
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "OBLO_EPIL");
  /*_.EPIL__V8*/ meltfptr[6] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4170:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 4, "OBLOOP_RESV");
  /*_.RESV__V9*/ meltfptr[8] = slot;
    };
    ;
 /*_.DESTLIST__V10*/ meltfptr[9] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[2]))));;
    MELT_LOCATION ("warmelt-genobj.melt:4172:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_OBJCOMPUTE */
					     meltfrout->tabval[3])), (4),
			      "CLASS_OBJCOMPUTE");
  /*_.INST__V12*/ meltfptr[11] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBDI_DESTLIST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V12*/ meltfptr[11])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V12*/ meltfptr[11]), (1),
			  ( /*_.DESTLIST__V10*/ meltfptr[9]),
			  "OBDI_DESTLIST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V12*/ meltfptr[11])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V12*/ meltfptr[11]), (2),
			  ( /*_.RESV__V9*/ meltfptr[8]), "OBCPT_EXPR");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @OBCPT_TYPE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V12*/ meltfptr[11])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V12*/ meltfptr[11]), (3),
			  (( /*!CTYPE_VALUE */ meltfrout->tabval[4])),
			  "OBCPT_TYPE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V12*/ meltfptr[11],
				  "newly made instance");
    ;
    /*_.OBC__V11*/ meltfptr[10] = /*_.INST__V12*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4179:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.DESTLIST__V10*/ meltfptr[9]),
			  (melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]));
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4180:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_LIST__L3*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.EPIL__V8*/ meltfptr[6])) ==
	 MELTOBMAG_LIST);;
      MELT_LOCATION ("warmelt-genobj.melt:4180:/ cond");
      /*cond */ if ( /*_#IS_LIST__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V14*/ meltfptr[13] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4180:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check epil"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4180) ? (4180) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V13*/ meltfptr[12] = /*_.IFELSE___V14*/ meltfptr[13];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4180:/ clear");
	     /*clear *//*_#IS_LIST__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V14*/ meltfptr[13] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V13*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4181:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RESV__V9*/ meltfptr[8]),
			     (melt_ptr_t) (( /*!CLASS_OBJLOCV */ meltfrout->
					    tabval[5])));;
      MELT_LOCATION ("warmelt-genobj.melt:4181:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V16*/ meltfptr[15] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4181:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check resv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4181) ? (4181) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V15*/ meltfptr[13] = /*_.IFELSE___V16*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4181:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V16*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V15*/ meltfptr[13] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4182:/ locexp");
      meltgc_append_list ((melt_ptr_t) ( /*_.EPIL__V8*/ meltfptr[6]),
			  (melt_ptr_t) ( /*_.OBC__V11*/ meltfptr[10]));
    }
    ;

    MELT_LOCATION ("warmelt-genobj.melt:4169:/ clear");
	   /*clear *//*_.EPIL__V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.RESV__V9*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.DESTLIST__V10*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.OBC__V11*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V13*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V15*/ meltfptr[13] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4184:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-genobj.melt:4184:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-genobj.melt:4184:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-genobj.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4184;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "putobjdest loop updated recv=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RECV__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[8] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[6])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V18*/ meltfptr[6] =
	      /*_.MELT_DEBUG_FUN__V19*/ meltfptr[8];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-genobj.melt:4184:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V19*/ meltfptr[8] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V18*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-genobj.melt:4184:/ quasiblock");


      /*_.PROGN___V20*/ meltfptr[9] = /*_.IF___V18*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V17*/ meltfptr[15] = /*_.PROGN___V20*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4184:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V18*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V20*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V17*/ meltfptr[15] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4166:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RECV__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4166:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V17*/ meltfptr[15] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_OBJLOOP", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_119_warmelt_genobj_PUTOBJDEST_OBJLOOP */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un *
						meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un *
						meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PUTOBJDEST_OBJEXIT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-genobj.melt:4191:/ getarg");
 /*_.RECV__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.DESTO__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4192:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.RECV__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_OBJEXIT */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-genobj.melt:4192:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V5*/ meltfptr[4] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4192:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check recv"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4192) ? (4192) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V4*/ meltfptr[3] = /*_.IFELSE___V5*/ meltfptr[4];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4192:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V5*/ meltfptr[4] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V4*/ meltfptr[3] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-genobj.melt:4193:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.DESTO__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_OBJPUREVALUE */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-genobj.melt:4193:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-genobj.melt:4193:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check desto"),
				  ("warmelt-genobj.melt")
				  ? ("warmelt-genobj.melt") : __FILE__,
				  (4193) ? (4193) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[4] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-genobj.melt:4193:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-genobj.melt:4191:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RECV__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-genobj.melt:4191:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V4*/ meltfptr[3] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[4] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PUTOBJDEST_OBJEXIT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_120_warmelt_genobj_PUTOBJDEST_OBJEXIT */



/**** end of warmelt-genobj+03.c ****/
