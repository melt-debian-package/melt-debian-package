/* GCC MELT GENERATED FILE warmelt-macro+03.c - DO NOT EDIT */
/* secondary MELT generated C file of rank #3 */
#include "melt-run.h"


/* used hash from melt-run.h when compiling this file: */
MELT_EXTERN const char meltrun_used_md5_melt_f3[] =
  MELT_RUN_HASHMD5 /* from melt-run.h */ ;


/**** warmelt-macro+03.c declarations ****/


/***************************************************
***
    Copyright 2008 - 2012 Free Software Foundation, Inc.
    Contributed by Basile Starynkevitch <basile@starynkevitch.net>

    This file is part of GCC.

    GCC is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    GCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GCC; see the file COPYING3.  If not see
    <http://www.gnu.org/licenses/>.
***
****************************************************/

/* ordinary MELT module */
#define MELT_HAS_INITIAL_ENVIRONMENT 1	/*usual */

struct melt_callframe_st;	/*defined in melt-runtime.h */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_1_warmelt_macro_S_EXPR_WEIGHT (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_2_warmelt_macro_LAMBDA___1__ (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_3_warmelt_macro_EXPAND_RESTLIST_AS_LIST (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_4_warmelt_macro_EXPAND_RESTLIST_AS_TUPLE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_5_warmelt_macro_EXPAND_PAIRLIST_AS_LIST (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_6_warmelt_macro_EXPAND_PAIRLIST_AS_TUPLE (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_7_warmelt_macro_REGISTER_GENERATOR_DEVICE (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_8_warmelt_macro_EXPAND_APPLY (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_9_warmelt_macro_EXPAND_MSEND (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_10_warmelt_macro_EXPAND_FIELDEXPR (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_11_warmelt_macro_EXPAND_CITERATION (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_12_warmelt_macro_LAMBDA___2__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_13_warmelt_macro_EXPAND_CMATCHEXPR (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_14_warmelt_macro_EXPAND_FUNMATCHEXPR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_15_warmelt_macro_EXPAND_KEYWORDFUN (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_16_warmelt_macro_MACROEXPAND_1 (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_17_warmelt_macro_LAMBDA___3__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_18_warmelt_macro_EXPAND_PRIMITIVE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_19_warmelt_macro_PATTERNEXPAND_PAIRLIST_AS_TUPLE (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_20_warmelt_macro_LAMBDA___4__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_21_warmelt_macro_PATMACEXPAND_FOR_MATCHER (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_22_warmelt_macro_LAMBDA___5__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_23_warmelt_macro_LAMBDA___6__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_24_warmelt_macro_PATTERN_WEIGHT_TUPLE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_25_warmelt_macro_LAMBDA___7__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_26_warmelt_macro_PATTERNEXPAND_EXPR (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_27_warmelt_macro_PATTERNEXPAND_1 (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_28_warmelt_macro_MACROEXPAND_TOPLEVEL_LIST (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_29_warmelt_macro_LAMBDA_ARG_BINDINGS (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_30_warmelt_macro_INSTALL_INITIAL_MACRO (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_31_warmelt_macro_INSTALL_INITIAL_PATMACRO (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_32_warmelt_macro_WARN_IF_REDEFINED (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_33_warmelt_macro_FLATTEN_FOR_C_CODE_EXPANSION (meltclosure_ptr_t
							meltclosp_,
							melt_ptr_t
							meltfirstargp_,
							const
							melt_argdescr_cell_t
							meltxargdescr_[],
							union meltparam_un
							*meltxargtab_,
							const
							melt_argdescr_cell_t
							meltxresdescr_[],
							union meltparam_un
							*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_34_warmelt_macro_PARSE_PAIRLIST_C_CODE_EXPANSION (meltclosure_ptr_t
							   meltclosp_,
							   melt_ptr_t
							   meltfirstargp_,
							   const
							   melt_argdescr_cell_t
							   meltxargdescr_[],
							   union meltparam_un
							   *meltxargtab_,
							   const
							   melt_argdescr_cell_t
							   meltxresdescr_[],
							   union meltparam_un
							   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_35_warmelt_macro_CHECK_C_EXPANSION (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_36_warmelt_macro_MEXPAND_DEFPRIMITIVE (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_37_warmelt_macro_MEXPAND_DEFCITERATOR (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_38_warmelt_macro_MEXPAND_DEFCMATCHER (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_39_warmelt_macro_LAMBDA___8__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_40_warmelt_macro_MEXPAND_DEFUNMATCHER (meltclosure_ptr_t meltclosp_,
						melt_ptr_t meltfirstargp_,
						const melt_argdescr_cell_t
						meltxargdescr_[],
						union meltparam_un
						*meltxargtab_,
						const melt_argdescr_cell_t
						meltxresdescr_[],
						union meltparam_un
						*meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_41_warmelt_macro_LAMBDA___9__ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_42_warmelt_macro_MEXPAND_DEFUN (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_43_warmelt_macro_LAMBDA___10__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_44_warmelt_macro_MEXPAND_DEFINE (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_45_warmelt_macro_LAMBDA___11__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_46_warmelt_macro_SCAN_DEFCLASS (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_47_warmelt_macro_MEXPAND_DEFCLASS (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_48_warmelt_macro_LAMBDA___12__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_49_warmelt_macro_LAMBDA___13__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_50_warmelt_macro_PARSE_FIELD_ASSIGNMENT (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_51_warmelt_macro_LAMBDA___14__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_52_warmelt_macro_MEXPAND_DEFINSTANCE (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_53_warmelt_macro_MEXPAND_DEFSELECTOR (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_54_warmelt_macro_MEXPAND_INSTANCE (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_55_warmelt_macro_MEXPAND_LOAD (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_56_warmelt_macro_PARSE_FIELD_PATTERN (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_57_warmelt_macro_LAMBDA___15__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_58_warmelt_macro_PATEXPAND_INSTANCE (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_59_warmelt_macro_LAMBDA___16__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_60_warmelt_macro_PATEXPAND_OBJECT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_61_warmelt_macro_LAMBDA___17__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_62_warmelt_macro_MEXPAND_OBJECT (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_63_warmelt_macro_MEXPAND_CODE_CHUNK (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_64_warmelt_macro_MEXPAND_UNSAFE_PUT_FIELDS (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_65_warmelt_macro_MEXPAND_PUT_FIELDS (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_66_warmelt_macro_MEXPAND_UNSAFE_GET_FIELD (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_67_warmelt_macro_MEXPAND_GET_FIELD (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_68_warmelt_macro_PAIRLIST_TO_PROGN (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_69_warmelt_macro_LAMBDA___18__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_70_warmelt_macro_MEXPAND_SETQ (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_71_warmelt_macro_MEXPAND_IF (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_72_warmelt_macro_MEXPAND_WHEN (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_macro_MEXPAND_UNLESS (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_macro_MEXPAND_CPPIF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_macro_FILTERGCCVERSION (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_macro_MEXPAND_GCCIF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_macro_MEXPAND_COND (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_macro_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_macro_MEXPAND_AND (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un *meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_macro_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_macro_PATEXPAND_AS (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_macro_MEXPAND_AS (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_macro_PATEXPAND_WHEN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_macro_MEXPAND_WHEN (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_macro_PATEXPAND_AND (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_macro_MEXPAND_OR (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un *meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_macro_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_macro_PATEXPAND_OR (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_macro_MEXPAND_REFERENCE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_macro_PATEXPAND_REFERENCE (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_macro_MEXPANDOBSOLETE_CONTAINER (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_macro_PATEXPANDOBSOLETE_CONTAINER (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un
						       *meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un
						       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_macro_MEXPAND_DEREF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_macro_MEXPANDOBSOLETE_CONTENT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_macro_MEXPAND_SET_REF (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_macro_MEXPAND_TUPLE (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_macro_PATEXPAND_TUPLE (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_macro_MEXPAND_LIST (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_macro_PATEXPAND_LIST (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_macro_MEXPAND_MATCH (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_101_warmelt_macro_LAMBDA___22__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_102_warmelt_macro_MEXPAND_LETBINDING (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_103_warmelt_macro_MEXPAND_LET (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un *meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_104_warmelt_macro_LAMBDA___23__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_105_warmelt_macro_LAMBDA___24__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_106_warmelt_macro_LAMBDA___25__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_107_warmelt_macro_YES_RECURSIVELY_CONSTRUCTIBLE (meltclosure_ptr_t
							  meltclosp_,
							  melt_ptr_t
							  meltfirstargp_,
							  const
							  melt_argdescr_cell_t
							  meltxargdescr_[],
							  union meltparam_un
							  *meltxargtab_,
							  const
							  melt_argdescr_cell_t
							  meltxresdescr_[],
							  union meltparam_un
							  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_108_warmelt_macro_MEXPAND_LETREC (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_109_warmelt_macro_LAMBDA___26__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_110_warmelt_macro_LAMBDA___27__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_111_warmelt_macro_MEXPAND_LAMBDA (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_112_warmelt_macro_LAMBDA___28__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_113_warmelt_macro_MEXPAND_VARIADIC (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un
					     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_114_warmelt_macro_LAMBDA___29__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_115_warmelt_macro_LAMBDA___30__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_116_warmelt_macro_LAMBDA___31__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_117_warmelt_macro_LAMBDA___32__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_118_warmelt_macro_MEXPAND_MULTICALL (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_119_warmelt_macro_LAMBDA___33__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_120_warmelt_macro_LAMBDA___34__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_121_warmelt_macro_MEXPAND_QUOTE (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_122_warmelt_macro_MEXPAND_COMMENT (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_123_warmelt_macro_MEXPAND_CHEADER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_124_warmelt_macro_MEXPAND_USE_PACKAGE_FROM_PKG_CONFIG
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_125_warmelt_macro_PAIRLIST_TO_RETURN (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_126_warmelt_macro_LAMBDA___35__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_127_warmelt_macro_MEXPAND_PROGN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_128_warmelt_macro_MEXPAND_RETURN (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un *meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_129_warmelt_macro_MEXPAND_FOREVER (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un *meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_130_warmelt_macro_LAMBDA___36__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_131_warmelt_macro_MEXPAND_EXIT (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un *meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_132_warmelt_macro_LAMBDA___37__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_133_warmelt_macro_MEXPAND_AGAIN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_134_warmelt_macro_MEXPAND_COMPILE_WARNING (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_135_warmelt_macro_MEXPAND_ASSERT_MSG (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un
					       *meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un
					       *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_136_warmelt_macro_MEXPAND_DEBUG_MSG (meltclosure_ptr_t meltclosp_,
					      melt_ptr_t meltfirstargp_,
					      const melt_argdescr_cell_t
					      meltxargdescr_[],
					      union meltparam_un
					      *meltxargtab_,
					      const melt_argdescr_cell_t
					      meltxresdescr_[],
					      union meltparam_un
					      *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_137_warmelt_macro_MEXPAND_DEBUG (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_138_warmelt_macro_MEXPAND_EXPORT_VALUES (meltclosure_ptr_t
						  meltclosp_,
						  melt_ptr_t meltfirstargp_,
						  const melt_argdescr_cell_t
						  meltxargdescr_[],
						  union meltparam_un
						  *meltxargtab_,
						  const melt_argdescr_cell_t
						  meltxresdescr_[],
						  union meltparam_un
						  *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_139_warmelt_macro_LAMBDA___38__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_140_warmelt_macro_MEXPAND_EXPORT_MACRO (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_141_warmelt_macro_MEXPAND_EXPORT_PATMACRO (meltclosure_ptr_t
						    meltclosp_,
						    melt_ptr_t meltfirstargp_,
						    const melt_argdescr_cell_t
						    meltxargdescr_[],
						    union meltparam_un
						    *meltxargtab_,
						    const melt_argdescr_cell_t
						    meltxresdescr_[],
						    union meltparam_un
						    *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_142_warmelt_macro_MEXPAND_EXPORT_CLASS (meltclosure_ptr_t meltclosp_,
						 melt_ptr_t meltfirstargp_,
						 const melt_argdescr_cell_t
						 meltxargdescr_[],
						 union meltparam_un
						 *meltxargtab_,
						 const melt_argdescr_cell_t
						 meltxresdescr_[],
						 union meltparam_un
						 *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_143_warmelt_macro_LAMBDA___39__ (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_144_warmelt_macro_MEXPAND_EXPORT_SYNONYM (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un
						   *meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un
						   *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_145_warmelt_macro_MEXPAND_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_146_warmelt_macro_MEXPAND_PARENT_MODULE_ENVIRONMENT
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_147_warmelt_macro_MEXPAND_UPDATE_CURRENT_MODULE_ENVIRONMENT_CONTAINER
(meltclosure_ptr_t meltclosp_, melt_ptr_t meltfirstargp_, const melt_argdescr_cell_t meltxargdescr_[],
union meltparam_un *meltxargtab_, const melt_argdescr_cell_t meltxresdescr_[],
union meltparam_un *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_148_warmelt_macro_MEXPAND_FETCH_PREDEFINED (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_149_warmelt_macro_MEXPAND_STORE_PREDEFINED (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un
						     *meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un
						     *meltxrestab_);



MELT_EXTERN void *melt_start_this_module (void *);


/* define different names when debugging or not */
#if MELT_HAVE_DEBUG
MELT_EXTERN const char meltmodule_warmelt_macro__melt_have_debug_enabled[];
#define melt_have_debug_string meltmodule_warmelt_macro__melt_have_debug_enabled
#else /*!MELT_HAVE_DEBUG */
MELT_EXTERN const char meltmodule_warmelt_macro__melt_have_debug_disabled[];
#define melt_have_debug_string meltmodule_warmelt_macro__melt_have_debug_disabled
#endif /*!MELT_HAVE_DEBUG */


struct frame_melt_start_this_module_st;
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_0 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_1 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_2 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_3 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_4 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_5 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_6 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_7 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_8 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY meltmod__warmelt_macro__initialmeltchunk_9 (struct
									frame_melt_start_this_module_st
									*,
									char
									*);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_10 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_11 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_12 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_13 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_14 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_15 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_16 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_17 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_18 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_19 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_20 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_21 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_22 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_23 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_24 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_25 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_26 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_27 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_28 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_29 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_30 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_31 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_32 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_33 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_34 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_35 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_36 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_37 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_38 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__initialmeltchunk_39 (struct
					     frame_melt_start_this_module_st
					     *, char *);
void MELT_MODULE_VISIBILITY
meltmod__warmelt_macro__forward_or_mark_module_start_frame (struct
							    melt_callframe_st
							    *fp, int marking);
#define meltmarking_melt_start_this_module  meltmod__warmelt_macro__forward_or_mark_module_start_frame



/**** warmelt-macro+03.c implementations ****/




melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_73_warmelt_macro_MEXPAND_UNLESS (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un *meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un *meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_73_warmelt_macro_MEXPAND_UNLESS_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_73_warmelt_macro_MEXPAND_UNLESS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 35
    melt_ptr_t mcfr_varptr[35];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_73_warmelt_macro_MEXPAND_UNLESS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_73_warmelt_macro_MEXPAND_UNLESS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 35; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_73_warmelt_macro_MEXPAND_UNLESS nbval 35*/
  meltfram__.mcfr_nbvar = 35 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_UNLESS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4307:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4308:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4308:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4308:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4308;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_unless sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4308:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4308:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4308:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4309:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4309:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4309:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4309) ? (4309) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4309:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4310:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4310:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4310:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4310) ? (4310) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4310:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4311:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L5*/ meltfnum[1] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:4311:/ cond");
    /*cond */ if ( /*_#NULL__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V15*/ meltfptr[14] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[3]);;
	  /*_.IF___V14*/ meltfptr[12] = /*_.SETQ___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4311:/ clear");
	     /*clear *//*_.SETQ___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4312:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:4312:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4312:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4312) ? (4312) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4312:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4313:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4314:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4315:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.CURPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    /*^compute */
 /*_.CURIF__V23*/ meltfptr[22] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:4319:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_PAIR__L7*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])) ==
       MELTOBMAG_PAIR);;
    /*^compute */
 /*_#NOT__L8*/ meltfnum[7] =
      (!( /*_#IS_PAIR__L7*/ meltfnum[0]));;
    MELT_LOCATION ("warmelt-macro.melt:4319:/ cond");
    /*cond */ if ( /*_#NOT__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4320:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("missing condition in UNLESS"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
 /*_.PAIR_TAIL__V24*/ meltfptr[23] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:4321:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V25*/ meltfptr[24] =
      /*_.PAIR_TAIL__V24*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-macro.melt:4322:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_PAIR__L9*/ meltfnum[8] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])) ==
       MELTOBMAG_PAIR);;
    /*^compute */
 /*_#NOT__L10*/ meltfnum[9] =
      (!( /*_#IS_PAIR__L9*/ meltfnum[8]));;
    MELT_LOCATION ("warmelt-macro.melt:4322:/ cond");
    /*cond */ if ( /*_#NOT__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4323:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("missing body in UNLESS"), (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4324:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XCOND__V27*/ meltfptr[26] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.CURIF__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4325:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[4];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LOC__V20*/ meltfptr[19];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XPROGN__V28*/ meltfptr[27] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_PROGN */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4326:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_IFELSE */
					     meltfrout->tabval[5])), (5),
			      "CLASS_SOURCE_IFELSE");
  /*_.INST__V30*/ meltfptr[29] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIF_TEST",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (2),
			  ( /*_.XCOND__V27*/ meltfptr[26]), "SIF_TEST");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIF_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (3),
			  (( /*nil */ NULL)), "SIF_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIF_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V30*/ meltfptr[29])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V30*/ meltfptr[29]), (4),
			  ( /*_.XPROGN__V28*/ meltfptr[27]), "SIF_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V30*/ meltfptr[29],
				  "newly made instance");
    ;
    /*_.XUNLESS__V29*/ meltfptr[28] = /*_.INST__V30*/ meltfptr[29];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4332:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4332:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4332:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4332;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_unless return xunless=";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.XUNLESS__V29*/ meltfptr[28];
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V32*/ meltfptr[31] =
	      /*_.MELT_DEBUG_FUN__V33*/ meltfptr[32];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4332:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V33*/ meltfptr[32] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V32*/ meltfptr[31] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4332:/ quasiblock");


      /*_.PROGN___V34*/ meltfptr[32] = /*_.IF___V32*/ meltfptr[31];;
      /*^compute */
      /*_.IFCPP___V31*/ meltfptr[30] = /*_.PROGN___V34*/ meltfptr[32];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4332:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V32*/ meltfptr[31] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V34*/ meltfptr[32] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V31*/ meltfptr[30] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4333:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.XUNLESS__V29*/ meltfptr[28];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4333:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V26*/ meltfptr[25] = /*_.RETURN___V35*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-macro.melt:4324:/ clear");
	   /*clear *//*_.XCOND__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.XPROGN__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.XUNLESS__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V35*/ meltfptr[31] = 0;
    /*_.LET___V18*/ meltfptr[16] = /*_.LET___V26*/ meltfptr[25];;

    MELT_LOCATION ("warmelt-macro.melt:4313:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.CURIF__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#IS_PAIR__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#IS_PAIR__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V26*/ meltfptr[25] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4307:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4307:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_UNLESS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_73_warmelt_macro_MEXPAND_UNLESS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_73_warmelt_macro_MEXPAND_UNLESS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_74_warmelt_macro_MEXPAND_CPPIF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_74_warmelt_macro_MEXPAND_CPPIF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_74_warmelt_macro_MEXPAND_CPPIF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 63
    melt_ptr_t mcfr_varptr[63];
#define MELTFRAM_NBVARNUM 22
    long mcfr_varnum[22];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_74_warmelt_macro_MEXPAND_CPPIF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_74_warmelt_macro_MEXPAND_CPPIF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 63; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_74_warmelt_macro_MEXPAND_CPPIF nbval 63*/
  meltfram__.mcfr_nbvar = 63 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_CPPIF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4343:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4344:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4344:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4344:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4344) ? (4344) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4344:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4345:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4345:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4345:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4345) ? (4345) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4345:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4346:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L3*/ meltfnum[0] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:4346:/ cond");
    /*cond */ if ( /*_#NULL__L3*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V11*/ meltfptr[10] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[2]);;
	  /*_.IF___V10*/ meltfptr[8] = /*_.SETQ___V11*/ meltfptr[10];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4346:/ clear");
	     /*clear *//*_.SETQ___V11*/ meltfptr[10] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V10*/ meltfptr[8] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4347:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L4*/ meltfnum[3] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:4347:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L4*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4347:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4347) ? (4347) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[10] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4347:/ clear");
	     /*clear *//*_#IS_OBJECT__L4*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[10] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4348:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[3] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4348:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[3])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4348:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4348;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cppif sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V15*/ meltfptr[14] =
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4348:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V15*/ meltfptr[14] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4348:/ quasiblock");


      /*_.PROGN___V17*/ meltfptr[15] = /*_.IF___V15*/ meltfptr[14];;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.PROGN___V17*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4348:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[3] = 0;
      /*^clear */
	     /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4349:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4350:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4351:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[15])));;
    /*^compute */
 /*_.CURPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    /*^compute */
 /*_.CURIF__V23*/ meltfptr[22] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:4355:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_PAIR__L7*/ meltfnum[5] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])) ==
       MELTOBMAG_PAIR);;
    /*^compute */
 /*_#NOT__L8*/ meltfnum[3] =
      (!( /*_#IS_PAIR__L7*/ meltfnum[5]));;
    MELT_LOCATION ("warmelt-macro.melt:4355:/ cond");
    /*cond */ if ( /*_#NOT__L8*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4356:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("missing condition in CPPIF"), (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
 /*_.PAIR_TAIL__V24*/ meltfptr[23] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:4357:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V25*/ meltfptr[24] =
      /*_.PAIR_TAIL__V24*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-macro.melt:4358:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_PAIR__L9*/ meltfnum[8] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])) ==
       MELTOBMAG_PAIR);;
    /*^compute */
 /*_#NOT__L10*/ meltfnum[9] =
      (!( /*_#IS_PAIR__L9*/ meltfnum[8]));;
    MELT_LOCATION ("warmelt-macro.melt:4358:/ cond");
    /*cond */ if ( /*_#NOT__L10*/ meltfnum[9])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4359:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("missing then in CPPIF"), (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4360:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XCOND__V27*/ meltfptr[26] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.CURIF__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4361:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4361:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4361:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4361;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cppif xcond";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.XCOND__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V29*/ meltfptr[28] =
	      /*_.MELT_DEBUG_FUN__V30*/ meltfptr[29];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4361:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V30*/ meltfptr[29] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V29*/ meltfptr[28] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4361:/ quasiblock");


      /*_.PROGN___V31*/ meltfptr[29] = /*_.IF___V29*/ meltfptr[28];;
      /*^compute */
      /*_.IFCPP___V28*/ meltfptr[27] = /*_.PROGN___V31*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4361:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V29*/ meltfptr[28] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V31*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V28*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4363:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L13*/ meltfnum[11] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.XCOND__V27*/ meltfptr[26])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-macro.melt:4363:/ cond");
    /*cond */ if ( /*_#IS_STRING__L13*/ meltfnum[11])	/*then */
      {
	/*^cond.then */
	/*_.IFELSE___V32*/ meltfptr[28] = ( /*nil */ NULL);;
      }
    else
      {
	MELT_LOCATION ("warmelt-macro.melt:4363:/ cond.else");

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:4364:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_A__L14*/ meltfnum[10] =
	    melt_is_instance_of ((melt_ptr_t)
				 ( /*_.XCOND__V27*/ meltfptr[26]),
				 (melt_ptr_t) (( /*!CLASS_SYMBOL */
						meltfrout->tabval[4])));;
	  MELT_LOCATION ("warmelt-macro.melt:4364:/ cond");
	  /*cond */ if ( /*_#IS_A__L14*/ meltfnum[10])	/*then */
	    {
	      /*^cond.then */
	      /*_.IFELSE___V33*/ meltfptr[29] = ( /*nil */ NULL);;
	    }
	  else
	    {
	      MELT_LOCATION ("warmelt-macro.melt:4364:/ cond.else");

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:4366:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.LOC__V20*/ meltfptr[19]),
				    ("invalid cpp-condition in CPPIF - string or symbol expected"),
				    (melt_ptr_t) 0);
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:4367:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

		{
		  MELT_LOCATION ("warmelt-macro.melt:4367:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-macro.melt:4365:/ quasiblock");


		/*_.PROGN___V35*/ meltfptr[34] =
		  /*_.RETURN___V34*/ meltfptr[33];;
		/*^compute */
		/*_.IFELSE___V33*/ meltfptr[29] =
		  /*_.PROGN___V35*/ meltfptr[34];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:4364:/ clear");
	       /*clear *//*_.RETURN___V34*/ meltfptr[33] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V35*/ meltfptr[34] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V32*/ meltfptr[28] = /*_.IFELSE___V33*/ meltfptr[29];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4363:/ clear");
	     /*clear *//*_#IS_A__L14*/ meltfnum[10] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V33*/ meltfptr[29] = 0;
	}
	;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4368:/ quasiblock");


 /*_.CURTHEN__V37*/ meltfptr[34] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    /*^compute */
 /*_.PAIR_TAIL__V38*/ meltfptr[29] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:4369:/ compute");
    /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V39*/ meltfptr[38] =
      /*_.PAIR_TAIL__V38*/ meltfptr[29];;
    MELT_LOCATION ("warmelt-macro.melt:4370:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XTHEN__V41*/ meltfptr[40] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.CURTHEN__V37*/ meltfptr[34]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    /*_.XELSE__V42*/ meltfptr[41] = ( /*nil */ NULL);;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4373:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L15*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4373:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L15*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4373:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4373;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cppif xthen";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.XTHEN__V41*/ meltfptr[40];
	      /*_.MELT_DEBUG_FUN__V45*/ meltfptr[44] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V44*/ meltfptr[43] =
	      /*_.MELT_DEBUG_FUN__V45*/ meltfptr[44];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4373:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L16*/ meltfnum[15] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V45*/ meltfptr[44] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V44*/ meltfptr[43] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4373:/ quasiblock");


      /*_.PROGN___V46*/ meltfptr[44] = /*_.IF___V44*/ meltfptr[43];;
      /*^compute */
      /*_.IFCPP___V43*/ meltfptr[42] = /*_.PROGN___V46*/ meltfptr[44];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4373:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L15*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V44*/ meltfptr[43] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V46*/ meltfptr[44] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V43*/ meltfptr[42] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4374:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_PAIR__L17*/ meltfnum[15] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])) ==
       MELTOBMAG_PAIR);;
    MELT_LOCATION ("warmelt-macro.melt:4374:/ cond");
    /*cond */ if ( /*_#IS_PAIR__L17*/ meltfnum[15])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:4375:/ quasiblock");


   /*_.CURELSE__V47*/ meltfptr[43] =
	    (melt_pair_head
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
	  MELT_LOCATION ("warmelt-macro.melt:4376:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    union meltparam_un argtab[3];
	    memset (&argtab, 0, sizeof (argtab));
	    /*^apply.arg */
	    argtab[0].meltbp_aptr =
	      (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
	    /*^apply.arg */
	    argtab[1].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
	    /*^apply.arg */
	    argtab[2].meltbp_aptr =
	      (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
	    /*_.GOTXELSE__V48*/ meltfptr[44] =
	      melt_apply ((meltclosure_ptr_t)
			  ( /*_.MEXPANDER__V4*/ meltfptr[3]),
			  (melt_ptr_t) ( /*_.CURELSE__V47*/ meltfptr[43]),
			  (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			   ""), argtab, "", (union meltparam_un *) 0);
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:4377:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L18*/ meltfnum[10] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4377:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[10])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:4377:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-macro.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4377;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "mexpand_cppif gotxelse";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.GOTXELSE__V48*/ meltfptr[44];
		    /*_.MELT_DEBUG_FUN__V51*/ meltfptr[50] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[3])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V50*/ meltfptr[49] =
		    /*_.MELT_DEBUG_FUN__V51*/ meltfptr[50];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:4377:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V51*/ meltfptr[50] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V50*/ meltfptr[49] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-macro.melt:4377:/ quasiblock");


	    /*_.PROGN___V52*/ meltfptr[50] = /*_.IF___V50*/ meltfptr[49];;
	    /*^compute */
	    /*_.IFCPP___V49*/ meltfptr[48] = /*_.PROGN___V52*/ meltfptr[50];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4377:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V50*/ meltfptr[49] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V52*/ meltfptr[50] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V49*/ meltfptr[48] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  /*^compute */
   /*_.PAIR_TAIL__V53*/ meltfptr[49] =
	    (melt_pair_tail
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])));;
	  MELT_LOCATION ("warmelt-macro.melt:4378:/ compute");
	  /*_.CURPAIR__V22*/ meltfptr[21] = /*_.SETQ___V54*/ meltfptr[50] =
	    /*_.PAIR_TAIL__V53*/ meltfptr[49];;
	  MELT_LOCATION ("warmelt-macro.melt:4379:/ compute");
	  /*_.XELSE__V42*/ meltfptr[41] = /*_.SETQ___V55*/ meltfptr[54] =
	    /*_.GOTXELSE__V48*/ meltfptr[44];;
	  MELT_LOCATION ("warmelt-macro.melt:4380:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_PAIR__L20*/ meltfnum[18] =
	    (melt_magic_discr
	     ((melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21])) ==
	     MELTOBMAG_PAIR);;
	  MELT_LOCATION ("warmelt-macro.melt:4380:/ cond");
	  /*cond */ if ( /*_#IS_PAIR__L20*/ meltfnum[18])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:4381:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.LOC__V20*/ meltfptr[19]),
				    ("CPPIF with more than three sons"),
				    (melt_ptr_t) 0);
		}
		;
		/*epilog */
	      }
	      ;
	    }			/*noelse */
	  ;

	  MELT_LOCATION ("warmelt-macro.melt:4375:/ clear");
	     /*clear *//*_.CURELSE__V47*/ meltfptr[43] = 0;
	  /*^clear */
	     /*clear *//*_.GOTXELSE__V48*/ meltfptr[44] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V49*/ meltfptr[48] = 0;
	  /*^clear */
	     /*clear *//*_.PAIR_TAIL__V53*/ meltfptr[49] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V54*/ meltfptr[50] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V55*/ meltfptr[54] = 0;
	  /*^clear */
	     /*clear *//*_#IS_PAIR__L20*/ meltfnum[18] = 0;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4382:/ quasiblock");


    /*^checksignal */
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_CPPIF */
					     meltfrout->tabval[5])), (5),
			      "CLASS_SOURCE_CPPIF");
  /*_.INST__V58*/ meltfptr[48] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V58*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V58*/ meltfptr[48]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_COND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V58*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V58*/ meltfptr[48]), (2),
			  ( /*_.XCOND__V27*/ meltfptr[26]), "SIFP_COND");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_THEN",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V58*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V58*/ meltfptr[48]), (3),
			  ( /*_.XTHEN__V41*/ meltfptr[40]), "SIFP_THEN");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SIFP_ELSE",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V58*/ meltfptr[48])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V58*/ meltfptr[48]), (4),
			  ( /*_.XELSE__V42*/ meltfptr[41]), "SIFP_ELSE");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V58*/ meltfptr[48],
				  "newly made instance");
    ;
    /*_.RESP__V57*/ meltfptr[44] = /*_.INST__V58*/ meltfptr[48];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4389:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L21*/ meltfnum[10] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4389:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L21*/ meltfnum[10])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L22*/ meltfnum[18] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4389:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L22*/ meltfnum[18];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4389;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cppif return resp";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RESP__V57*/ meltfptr[44];
	      /*_.MELT_DEBUG_FUN__V61*/ meltfptr[54] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V60*/ meltfptr[50] =
	      /*_.MELT_DEBUG_FUN__V61*/ meltfptr[54];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4389:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L22*/ meltfnum[18] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V61*/ meltfptr[54] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V60*/ meltfptr[50] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4389:/ quasiblock");


      /*_.PROGN___V62*/ meltfptr[54] = /*_.IF___V60*/ meltfptr[50];;
      /*^compute */
      /*_.IFCPP___V59*/ meltfptr[49] = /*_.PROGN___V62*/ meltfptr[54];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4389:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L21*/ meltfnum[10] = 0;
      /*^clear */
	     /*clear *//*_.IF___V60*/ meltfptr[50] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V62*/ meltfptr[54] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V59*/ meltfptr[49] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4390:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RESP__V57*/ meltfptr[44];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4390:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V56*/ meltfptr[43] = /*_.RETURN___V63*/ meltfptr[50];;

    MELT_LOCATION ("warmelt-macro.melt:4382:/ clear");
	   /*clear *//*_.RESP__V57*/ meltfptr[44] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V59*/ meltfptr[49] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V63*/ meltfptr[50] = 0;
    /*_.LET___V40*/ meltfptr[39] = /*_.LET___V56*/ meltfptr[43];;

    MELT_LOCATION ("warmelt-macro.melt:4370:/ clear");
	   /*clear *//*_.XTHEN__V41*/ meltfptr[40] = 0;
    /*^clear */
	   /*clear *//*_.XELSE__V42*/ meltfptr[41] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V43*/ meltfptr[42] = 0;
    /*^clear */
	   /*clear *//*_#IS_PAIR__L17*/ meltfnum[15] = 0;
    /*^clear */
	   /*clear *//*_.LET___V56*/ meltfptr[43] = 0;
    /*_.LET___V36*/ meltfptr[33] = /*_.LET___V40*/ meltfptr[39];;

    MELT_LOCATION ("warmelt-macro.melt:4368:/ clear");
	   /*clear *//*_.CURTHEN__V37*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V38*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V39*/ meltfptr[38] = 0;
    /*^clear */
	   /*clear *//*_.LET___V40*/ meltfptr[39] = 0;
    /*_.LET___V26*/ meltfptr[25] = /*_.LET___V36*/ meltfptr[33];;

    MELT_LOCATION ("warmelt-macro.melt:4360:/ clear");
	   /*clear *//*_.XCOND__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L13*/ meltfnum[11] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V32*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.LET___V36*/ meltfptr[33] = 0;
    /*_.LET___V18*/ meltfptr[14] = /*_.LET___V26*/ meltfptr[25];;

    MELT_LOCATION ("warmelt-macro.melt:4349:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.CURIF__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_#IS_PAIR__L7*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L8*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.SETQ___V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#IS_PAIR__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#NOT__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V26*/ meltfptr[25] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4343:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4343:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L3*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_CPPIF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_74_warmelt_macro_MEXPAND_CPPIF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_74_warmelt_macro_MEXPAND_CPPIF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_75_warmelt_macro_FILTERGCCVERSION (meltclosure_ptr_t meltclosp_,
					    melt_ptr_t meltfirstargp_,
					    const melt_argdescr_cell_t
					    meltxargdescr_[],
					    union meltparam_un * meltxargtab_,
					    const melt_argdescr_cell_t
					    meltxresdescr_[],
					    union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_75_warmelt_macro_FILTERGCCVERSION_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_75_warmelt_macro_FILTERGCCVERSION_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 7
    melt_ptr_t mcfr_varptr[7];
#define MELTFRAM_NBVARNUM 2
    long mcfr_varnum[2];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_75_warmelt_macro_FILTERGCCVERSION is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_75_warmelt_macro_FILTERGCCVERSION_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 7; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_75_warmelt_macro_FILTERGCCVERSION nbval 7*/
  meltfram__.mcfr_nbvar = 7 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("FILTERGCCVERSION", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4400:/ getarg");
 /*_.VERSIONSTR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4401:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_STRING__L1*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.VERSIONSTR__V2*/ meltfptr[1]))
	 == MELTOBMAG_STRING);;
      MELT_LOCATION ("warmelt-macro.melt:4401:/ cond");
      /*cond */ if ( /*_#IS_STRING__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V4*/ meltfptr[3] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4401:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check versionstr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4401) ? (4401) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V3*/ meltfptr[2] = /*_.IFELSE___V4*/ meltfptr[3];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4401:/ clear");
	     /*clear *//*_#IS_STRING__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V4*/ meltfptr[3] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V3*/ meltfptr[2] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4402:/ quasiblock");


 /*_#VERSIONLEN__L2*/ meltfnum[0] =
      melt_string_length ((melt_ptr_t) ( /*_.VERSIONSTR__V2*/ meltfptr[1]));;
    /*^compute */
    /*_.RES__V6*/ meltfptr[5] = ( /*nil */ NULL);;

    {
      MELT_LOCATION ("warmelt-macro.melt:4407:/ locexp");
      if ( /*_#VERSIONLEN__L2*/ meltfnum[0] > 0	/*FILTERGCC__1 */
	  &&
	  !strncmp (melt_string_str
		    ((melt_ptr_t) /*_.VERSIONSTR__V2*/ meltfptr[1]),
		    melt_gccversionstr,
			 /*_#VERSIONLEN__L2*/ meltfnum[0]))
	/*_.RES__V6*/ meltfptr[5] = /*_.VERSIONSTR__V2*/ meltfptr[1];
      ;
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4413:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V6*/ meltfptr[5];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4413:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V5*/ meltfptr[3] = /*_.RETURN___V7*/ meltfptr[6];;

    MELT_LOCATION ("warmelt-macro.melt:4402:/ clear");
	   /*clear *//*_#VERSIONLEN__L2*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.RES__V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V7*/ meltfptr[6] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4400:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V5*/ meltfptr[3];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4400:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V3*/ meltfptr[2] = 0;
    /*^clear */
	   /*clear *//*_.LET___V5*/ meltfptr[3] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("FILTERGCCVERSION", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_75_warmelt_macro_FILTERGCCVERSION_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_75_warmelt_macro_FILTERGCCVERSION */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_76_warmelt_macro_MEXPAND_GCCIF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_76_warmelt_macro_MEXPAND_GCCIF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_76_warmelt_macro_MEXPAND_GCCIF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 72
    melt_ptr_t mcfr_varptr[72];
#define MELTFRAM_NBVARNUM 21
    long mcfr_varnum[21];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_76_warmelt_macro_MEXPAND_GCCIF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_76_warmelt_macro_MEXPAND_GCCIF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 72; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_76_warmelt_macro_MEXPAND_GCCIF nbval 72*/
  meltfram__.mcfr_nbvar = 72 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_GCCIF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4415:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4416:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4416:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4416:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4416) ? (4416) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4416:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4417:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4417:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4417:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4417) ? (4417) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4417:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4418:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4418:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4418:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4418;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_gccif sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4418:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4418:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4418:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4419:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4420:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V15*/ meltfptr[11] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4421:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.SLOC__V16*/ meltfptr[15] = slot;
    };
    ;
 /*_.LIST_FIRST__V17*/ meltfptr[16] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V15*/ meltfptr[11])));;
    /*^compute */
 /*_.CURPAIR__V18*/ meltfptr[17] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V17*/ meltfptr[16])));;
    /*^compute */
 /*_.CURIF__V19*/ meltfptr[18] =
      (melt_pair_head ((melt_ptr_t) ( /*_.CURPAIR__V18*/ meltfptr[17])));;
    /*^compute */
 /*_.RESTPAIR__V20*/ meltfptr[19] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.CURPAIR__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:4426:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L5*/ meltfnum[3] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.CURIF__V19*/ meltfptr[18]),
			   (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					  tabval[0])));;
    MELT_LOCATION ("warmelt-macro.melt:4426:/ cond");
    /*cond */ if ( /*_#IS_A__L5*/ meltfnum[3])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:4427:/ quasiblock");


	  /*^cond */
	  /*cond */ if (
			 /*ifisa */
			 melt_is_instance_of ((melt_ptr_t)
					      ( /*_.CURIF__V19*/
					       meltfptr[18]),
					      (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->tabval[0])))
	    )			/*then */
	    {
	      /*^cond.then */
	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj = (melt_ptr_t) ( /*_.CURIF__V19*/ meltfptr[18]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
     /*_.XCURIF__V23*/ meltfptr[22] = slot;
	      };
	      ;
	    }
	  else
	    {			/*^cond.else */

    /*_.XCURIF__V23*/ meltfptr[22] = NULL;;
	    }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:4429:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4429:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:4429:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-macro.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4429;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "mexpand_gccif xcurif=";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.XCURIF__V23*/ meltfptr[22];
		    /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[2])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V25*/ meltfptr[24] =
		    /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:4429:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V25*/ meltfptr[24] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-macro.melt:4429:/ quasiblock");


	    /*_.PROGN___V27*/ meltfptr[25] = /*_.IF___V25*/ meltfptr[24];;
	    /*^compute */
	    /*_.IFCPP___V24*/ meltfptr[23] = /*_.PROGN___V27*/ meltfptr[25];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4429:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V27*/ meltfptr[25] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V24*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4430:/ compute");
	  /*_.CURIF__V19*/ meltfptr[18] = /*_.SETQ___V28*/ meltfptr[24] =
	    /*_.XCURIF__V23*/ meltfptr[22];;
	  /*_.LET___V22*/ meltfptr[21] = /*_.SETQ___V28*/ meltfptr[24];;

	  MELT_LOCATION ("warmelt-macro.melt:4427:/ clear");
	     /*clear *//*_.XCURIF__V23*/ meltfptr[22] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V24*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.SETQ___V28*/ meltfptr[24] = 0;
	  /*_.IF___V21*/ meltfptr[20] = /*_.LET___V22*/ meltfptr[21];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4426:/ clear");
	     /*clear *//*_.LET___V22*/ meltfptr[21] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V21*/ meltfptr[20] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4432:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[6] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4432:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[6])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4432:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4432;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_gccif curif";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CURIF__V19*/ meltfptr[18];
	      /*_.MELT_DEBUG_FUN__V31*/ meltfptr[23] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V30*/ meltfptr[22] =
	      /*_.MELT_DEBUG_FUN__V31*/ meltfptr[23];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4432:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V31*/ meltfptr[23] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V30*/ meltfptr[22] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4432:/ quasiblock");


      /*_.PROGN___V32*/ meltfptr[24] = /*_.IF___V30*/ meltfptr[22];;
      /*^compute */
      /*_.IFCPP___V29*/ meltfptr[25] = /*_.PROGN___V32*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4432:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[6] = 0;
      /*^clear */
	     /*clear *//*_.IF___V30*/ meltfptr[22] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V32*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V29*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4434:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_STRING__L10*/ meltfnum[0] =
      (melt_magic_discr ((melt_ptr_t) ( /*_.CURIF__V19*/ meltfptr[18])) ==
       MELTOBMAG_STRING);;
    MELT_LOCATION ("warmelt-macro.melt:4434:/ cond");
    /*cond */ if ( /*_#IS_STRING__L10*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:4435:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^apply */
	  /*apply */
	  {
	    /*_.FILTERGCCVERSION__V34*/ meltfptr[23] =
	      melt_apply ((meltclosure_ptr_t)
			  (( /*!FILTERGCCVERSION */ meltfrout->tabval[3])),
			  (melt_ptr_t) ( /*_.CURIF__V19*/ meltfptr[18]), (""),
			  (union meltparam_un *) 0, "",
			  (union meltparam_un *) 0);
	  }
	  ;
	  /*^cond */
	  /*cond */ if ( /*_.FILTERGCCVERSION__V34*/ meltfptr[23])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:4436:/ quasiblock");


		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[3];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_aptr =
		    (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
		  /*^apply.arg */
		  argtab[1].meltbp_aptr =
		    (melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
		  /*^apply.arg */
		  argtab[2].meltbp_aptr =
		    (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
		  /*_.EXPREST__V37*/ meltfptr[36] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!EXPAND_PAIRLIST_AS_TUPLE */ meltfrout->
				  tabval[4])),
				(melt_ptr_t) ( /*_.RESTPAIR__V20*/
					      meltfptr[19]),
				(MELTBPARSTR_PTR MELTBPARSTR_PTR
				 MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;

#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-macro.melt:4438:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L11*/ meltfnum[6] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:4438:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[6])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-macro.melt:4438:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-macro.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4438;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "mexpand_gccif return exprest";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.EXPREST__V37*/ meltfptr[36];
			  /*_.MELT_DEBUG_FUN__V40*/ meltfptr[39] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V39*/ meltfptr[38] =
			  /*_.MELT_DEBUG_FUN__V40*/ meltfptr[39];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:4438:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V40*/ meltfptr[39] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V39*/ meltfptr[38] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-macro.melt:4438:/ quasiblock");


		  /*_.PROGN___V41*/ meltfptr[39] =
		    /*_.IF___V39*/ meltfptr[38];;
		  /*^compute */
		  /*_.IFCPP___V38*/ meltfptr[37] =
		    /*_.PROGN___V41*/ meltfptr[39];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:4438:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[6] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V39*/ meltfptr[38] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V41*/ meltfptr[39] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V38*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-macro.melt:4439:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^quasiblock */


		/*_.RETVAL___V1*/ meltfptr[0] =
		  /*_.EXPREST__V37*/ meltfptr[36];;

		{
		  MELT_LOCATION ("warmelt-macro.melt:4439:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		/*_.LET___V36*/ meltfptr[24] =
		  /*_.RETURN___V42*/ meltfptr[38];;

		MELT_LOCATION ("warmelt-macro.melt:4436:/ clear");
	       /*clear *//*_.EXPREST__V37*/ meltfptr[36] = 0;
		/*^clear */
	       /*clear *//*_.IFCPP___V38*/ meltfptr[37] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V42*/ meltfptr[38] = 0;
		/*_.IFELSE___V35*/ meltfptr[22] =
		  /*_.LET___V36*/ meltfptr[24];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:4435:/ clear");
	       /*clear *//*_.LET___V36*/ meltfptr[24] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


#if MELT_HAVE_DEBUG
		MELT_LOCATION ("warmelt-macro.melt:4441:/ cppif.then");
		/*^block */
		/*anyblock */
		{


		  {
		    /*^locexp */
		    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		    melt_dbgcounter++;
#endif
		    ;
		  }
		  ;
		  /*^checksignal */
		  MELT_CHECK_SIGNAL ();
		  ;
       /*_#MELT_NEED_DBG__L13*/ meltfnum[11] =
		    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		    0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:4441:/ cond");
		  /*cond */ if ( /*_#MELT_NEED_DBG__L13*/ meltfnum[11])	/*then */
		    {
		      /*^cond.then */
		      /*^block */
		      /*anyblock */
		      {

	 /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[6] =
#ifdef meltcallcount
			  meltcallcount	/* the_meltcallcount */
#else
			  0L
#endif /* meltcallcount the_meltcallcount */
			  ;;
			MELT_LOCATION
			  ("warmelt-macro.melt:4441:/ checksignal");
			MELT_CHECK_SIGNAL ();
			;
			/*^apply */
			/*apply */
			{
			  union meltparam_un argtab[5];
			  memset (&argtab, 0, sizeof (argtab));
			  /*^apply.arg */
			  argtab[0].meltbp_long =
			    /*_#THE_MELTCALLCOUNT__L14*/ meltfnum[6];
			  /*^apply.arg */
			  argtab[1].meltbp_cstring = "warmelt-macro.melt";
			  /*^apply.arg */
			  argtab[2].meltbp_long = 4441;
			  /*^apply.arg */
			  argtab[3].meltbp_cstring =
			    "mexpand_gccif sexpr gcc version mismatched";
			  /*^apply.arg */
			  argtab[4].meltbp_aptr =
			    (melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
			  /*_.MELT_DEBUG_FUN__V45*/ meltfptr[37] =
			    melt_apply ((meltclosure_ptr_t)
					(( /*!MELT_DEBUG_FUN */ meltfrout->
					  tabval[2])),
					(melt_ptr_t) (( /*nil */ NULL)),
					(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
					 MELTBPARSTR_PTR ""), argtab, "",
					(union meltparam_un *) 0);
			}
			;
			/*_.IF___V44*/ meltfptr[36] =
			  /*_.MELT_DEBUG_FUN__V45*/ meltfptr[37];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:4441:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L14*/ meltfnum[6] =
			  0;
			/*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V45*/ meltfptr[37] = 0;
		      }
		      ;
		    }
		  else
		    {		/*^cond.else */

	/*_.IF___V44*/ meltfptr[36] = NULL;;
		    }
		  ;
		  MELT_LOCATION ("warmelt-macro.melt:4441:/ quasiblock");


		  /*_.PROGN___V46*/ meltfptr[38] =
		    /*_.IF___V44*/ meltfptr[36];;
		  /*^compute */
		  /*_.IFCPP___V43*/ meltfptr[39] =
		    /*_.PROGN___V46*/ meltfptr[38];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:4441:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L13*/ meltfnum[11] = 0;
		  /*^clear */
		 /*clear *//*_.IF___V44*/ meltfptr[36] = 0;
		  /*^clear */
		 /*clear *//*_.PROGN___V46*/ meltfptr[38] = 0;
		}

#else /*MELT_HAVE_DEBUG */
		/*^cppif.else */
		/*_.IFCPP___V43*/ meltfptr[39] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		;
		MELT_LOCATION ("warmelt-macro.melt:4442:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-macro.melt:4442:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-macro.melt:4440:/ quasiblock");


		/*_.PROGN___V48*/ meltfptr[37] =
		  /*_.RETURN___V47*/ meltfptr[24];;
		/*^compute */
		/*_.IFELSE___V35*/ meltfptr[22] =
		  /*_.PROGN___V48*/ meltfptr[37];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:4435:/ clear");
	       /*clear *//*_.IFCPP___V43*/ meltfptr[39] = 0;
		/*^clear */
	       /*clear *//*_.RETURN___V47*/ meltfptr[24] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V48*/ meltfptr[37] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V33*/ meltfptr[21] = /*_.IFELSE___V35*/ meltfptr[22];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4434:/ clear");
	     /*clear *//*_.FILTERGCCVERSION__V34*/ meltfptr[23] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V35*/ meltfptr[22] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:4443:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
   /*_#IS_LIST__L15*/ meltfnum[6] =
	    (melt_magic_discr ((melt_ptr_t) ( /*_.CURIF__V19*/ meltfptr[18]))
	     == MELTOBMAG_LIST);;
	  MELT_LOCATION ("warmelt-macro.melt:4443:/ cond");
	  /*cond */ if ( /*_#IS_LIST__L15*/ meltfnum[6])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

		MELT_LOCATION ("warmelt-macro.melt:4444:/ quasiblock");


		/*_.OK__V51*/ meltfptr[39] = ( /*nil */ NULL);;
		/*citerblock FOREACH_IN_LIST */
		{
		  /* start foreach_in_list meltcit1__EACHLIST */
		  for ( /*_.CURPAIR__V52*/ meltfptr[24] =
		       melt_list_first ((melt_ptr_t) /*_.CURIF__V19*/
					meltfptr[18]);
		       melt_magic_discr ((melt_ptr_t) /*_.CURPAIR__V52*/
					 meltfptr[24]) == MELTOBMAG_PAIR;
		       /*_.CURPAIR__V52*/ meltfptr[24] =
		       melt_pair_tail ((melt_ptr_t) /*_.CURPAIR__V52*/
				       meltfptr[24]))
		    {
		      /*_.CURSTR__V53*/ meltfptr[37] =
			melt_pair_head ((melt_ptr_t) /*_.CURPAIR__V52*/
					meltfptr[24]);


		      MELT_LOCATION ("warmelt-macro.melt:4449:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
      /*_#IS_STRING__L16*/ meltfnum[11] =
			(melt_magic_discr
			 ((melt_ptr_t) ( /*_.CURSTR__V53*/ meltfptr[37])) ==
			 MELTOBMAG_STRING);;
		      /*^compute */
      /*_#NOT__L17*/ meltfnum[16] =
			(!( /*_#IS_STRING__L16*/ meltfnum[11]));;
		      MELT_LOCATION ("warmelt-macro.melt:4449:/ cond");
		      /*cond */ if ( /*_#NOT__L17*/ meltfnum[16])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {


			    {
			      MELT_LOCATION
				("warmelt-macro.melt:4450:/ locexp");
			      /* error_plain */
				melt_error_str ((melt_ptr_t)
						( /*_.SLOC__V16*/
						 meltfptr[15]),
						("GCCIF condition not a list of strings"),
						(melt_ptr_t) 0);
			    }
			    ;
			    /*epilog */
			  }
			  ;
			}	/*noelse */
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:4451:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			/*_.FILTERGCCVERSION__V54*/ meltfptr[23] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!FILTERGCCVERSION */ meltfrout->
					tabval[3])),
				      (melt_ptr_t) ( /*_.CURSTR__V53*/
						    meltfptr[37]), (""),
				      (union meltparam_un *) 0, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*^cond */
		      /*cond */ if ( /*_.FILTERGCCVERSION__V54*/ meltfptr[23])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

			    MELT_LOCATION
			      ("warmelt-macro.melt:4452:/ compute");
			    /*_.OK__V51*/ meltfptr[39] =
			      /*_.SETQ___V56*/ meltfptr[55] =
			      ( /*!konst_5_TRUE */ meltfrout->tabval[5]);;
			    /*_.IF___V55*/ meltfptr[22] =
			      /*_.SETQ___V56*/ meltfptr[55];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-macro.melt:4451:/ clear");
		  /*clear *//*_.SETQ___V56*/ meltfptr[55] = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

       /*_.IF___V55*/ meltfptr[22] = NULL;;
			}
		      ;
		    }		/* end foreach_in_list meltcit1__EACHLIST */
     /*_.CURPAIR__V52*/ meltfptr[24] = NULL;
     /*_.CURSTR__V53*/ meltfptr[37] = NULL;


		  /*citerepilog */

		  MELT_LOCATION ("warmelt-macro.melt:4446:/ clear");
		/*clear *//*_.CURPAIR__V52*/ meltfptr[24] = 0;
		  /*^clear */
		/*clear *//*_.CURSTR__V53*/ meltfptr[37] = 0;
		  /*^clear */
		/*clear *//*_#IS_STRING__L16*/ meltfnum[11] = 0;
		  /*^clear */
		/*clear *//*_#NOT__L17*/ meltfnum[16] = 0;
		  /*^clear */
		/*clear *//*_.FILTERGCCVERSION__V54*/ meltfptr[23] = 0;
		  /*^clear */
		/*clear *//*_.IF___V55*/ meltfptr[22] = 0;
		}		/*endciterblock FOREACH_IN_LIST */
		;
		MELT_LOCATION ("warmelt-macro.melt:4454:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.OK__V51*/ meltfptr[39])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

		      MELT_LOCATION ("warmelt-macro.melt:4455:/ quasiblock");


		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[3];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_aptr =
			  (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
			/*^apply.arg */
			argtab[1].meltbp_aptr =
			  (melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
			/*^apply.arg */
			argtab[2].meltbp_aptr =
			  (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
			/*_.EXPREST__V59*/ meltfptr[58] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!EXPAND_PAIRLIST_AS_TUPLE */
					meltfrout->tabval[4])),
				      (melt_ptr_t) ( /*_.RESTPAIR__V20*/
						    meltfptr[19]),
				      (MELTBPARSTR_PTR MELTBPARSTR_PTR
				       MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;

#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-macro.melt:4457:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L18*/ meltfnum[17] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-macro.melt:4457:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L18*/ meltfnum[17])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-macro.melt:4457:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L19*/ meltfnum[18];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-macro.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 4457;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "mexpand_gccif return exprest multicond";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.EXPREST__V59*/
				  meltfptr[58];
				/*_.MELT_DEBUG_FUN__V62*/ meltfptr[61] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[2])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V61*/ meltfptr[60] =
				/*_.MELT_DEBUG_FUN__V62*/ meltfptr[61];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-macro.melt:4457:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L19*/
				meltfnum[18] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V62*/ meltfptr[61]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V61*/ meltfptr[60] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-macro.melt:4457:/ quasiblock");


			/*_.PROGN___V63*/ meltfptr[61] =
			  /*_.IF___V61*/ meltfptr[60];;
			/*^compute */
			/*_.IFCPP___V60*/ meltfptr[59] =
			  /*_.PROGN___V63*/ meltfptr[61];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:4457:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L18*/ meltfnum[17] = 0;
			/*^clear */
		   /*clear *//*_.IF___V61*/ meltfptr[60] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V63*/ meltfptr[61] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V60*/ meltfptr[59] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:4458:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^quasiblock */


		      /*_.RETVAL___V1*/ meltfptr[0] =
			/*_.EXPREST__V59*/ meltfptr[58];;

		      {
			MELT_LOCATION ("warmelt-macro.melt:4458:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      /*_.LET___V58*/ meltfptr[57] =
			/*_.RETURN___V64*/ meltfptr[60];;

		      MELT_LOCATION ("warmelt-macro.melt:4455:/ clear");
		 /*clear *//*_.EXPREST__V59*/ meltfptr[58] = 0;
		      /*^clear */
		 /*clear *//*_.IFCPP___V60*/ meltfptr[59] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V64*/ meltfptr[60] = 0;
		      /*_.IFELSE___V57*/ meltfptr[55] =
			/*_.LET___V58*/ meltfptr[57];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:4454:/ clear");
		 /*clear *//*_.LET___V58*/ meltfptr[57] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

		    /*^block */
		    /*anyblock */
		    {


#if MELT_HAVE_DEBUG
		      MELT_LOCATION ("warmelt-macro.melt:4460:/ cppif.then");
		      /*^block */
		      /*anyblock */
		      {


			{
			  /*^locexp */
			  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			  melt_dbgcounter++;
#endif
			  ;
			}
			;
			/*^checksignal */
			MELT_CHECK_SIGNAL ();
			;
	 /*_#MELT_NEED_DBG__L20*/ meltfnum[18] =
			  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			  0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			  ;;
			MELT_LOCATION ("warmelt-macro.melt:4460:/ cond");
			/*cond */ if ( /*_#MELT_NEED_DBG__L20*/ meltfnum[18])	/*then */
			  {
			    /*^cond.then */
			    /*^block */
			    /*anyblock */
			    {

	   /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[17] =
#ifdef meltcallcount
				meltcallcount	/* the_meltcallcount */
#else
				0L
#endif /* meltcallcount the_meltcallcount */
				;;
			      MELT_LOCATION
				("warmelt-macro.melt:4460:/ checksignal");
			      MELT_CHECK_SIGNAL ();
			      ;
			      /*^apply */
			      /*apply */
			      {
				union meltparam_un argtab[5];
				memset (&argtab, 0, sizeof (argtab));
				/*^apply.arg */
				argtab[0].meltbp_long =
				  /*_#THE_MELTCALLCOUNT__L21*/ meltfnum[17];
				/*^apply.arg */
				argtab[1].meltbp_cstring =
				  "warmelt-macro.melt";
				/*^apply.arg */
				argtab[2].meltbp_long = 4460;
				/*^apply.arg */
				argtab[3].meltbp_cstring =
				  "mexpand_gccif sexpr gcc version multicond mismatched";
				/*^apply.arg */
				argtab[4].meltbp_aptr =
				  (melt_ptr_t *) & /*_.SEXPR__V2*/
				  meltfptr[1];
				/*_.MELT_DEBUG_FUN__V67*/ meltfptr[59] =
				  melt_apply ((meltclosure_ptr_t)
					      (( /*!MELT_DEBUG_FUN */
						meltfrout->tabval[2])),
					      (melt_ptr_t) (( /*nil */ NULL)),
					      (MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_LONG
					       MELTBPARSTR_CSTRING
					       MELTBPARSTR_PTR ""), argtab,
					      "", (union meltparam_un *) 0);
			      }
			      ;
			      /*_.IF___V66*/ meltfptr[58] =
				/*_.MELT_DEBUG_FUN__V67*/ meltfptr[59];;
			      /*epilog */

			      MELT_LOCATION
				("warmelt-macro.melt:4460:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L21*/
				meltfnum[17] = 0;
			      /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V67*/ meltfptr[59]
				= 0;
			    }
			    ;
			  }
			else
			  {	/*^cond.else */

	  /*_.IF___V66*/ meltfptr[58] = NULL;;
			  }
			;
			MELT_LOCATION
			  ("warmelt-macro.melt:4460:/ quasiblock");


			/*_.PROGN___V68*/ meltfptr[60] =
			  /*_.IF___V66*/ meltfptr[58];;
			/*^compute */
			/*_.IFCPP___V65*/ meltfptr[61] =
			  /*_.PROGN___V68*/ meltfptr[60];;
			/*epilog */

			MELT_LOCATION ("warmelt-macro.melt:4460:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L20*/ meltfnum[18] = 0;
			/*^clear */
		   /*clear *//*_.IF___V66*/ meltfptr[58] = 0;
			/*^clear */
		   /*clear *//*_.PROGN___V68*/ meltfptr[60] = 0;
		      }

#else /*MELT_HAVE_DEBUG */
		      /*^cppif.else */
		      /*_.IFCPP___V65*/ meltfptr[61] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:4461:/ quasiblock");


       /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		      {
			MELT_LOCATION ("warmelt-macro.melt:4461:/ locexp");
			/*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
			if (meltxresdescr_ && meltxresdescr_[0]
			    && meltxrestab_)
			  melt_warn_for_no_expected_secondary_results ();
			/* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
			;
		      }
		      ;
		      /*^finalreturn */
		      ;
		      /*finalret */ goto labend_rout;
		      MELT_LOCATION ("warmelt-macro.melt:4459:/ quasiblock");


		      /*_.PROGN___V70*/ meltfptr[59] =
			/*_.RETURN___V69*/ meltfptr[57];;
		      /*^compute */
		      /*_.IFELSE___V57*/ meltfptr[55] =
			/*_.PROGN___V70*/ meltfptr[59];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:4454:/ clear");
		 /*clear *//*_.IFCPP___V65*/ meltfptr[61] = 0;
		      /*^clear */
		 /*clear *//*_.RETURN___V69*/ meltfptr[57] = 0;
		      /*^clear */
		 /*clear *//*_.PROGN___V70*/ meltfptr[59] = 0;
		    }
		    ;
		  }
		;
		/*_.LET___V50*/ meltfptr[38] =
		  /*_.IFELSE___V57*/ meltfptr[55];;

		MELT_LOCATION ("warmelt-macro.melt:4444:/ clear");
	       /*clear *//*_.OK__V51*/ meltfptr[39] = 0;
		/*^clear */
	       /*clear *//*_.IFELSE___V57*/ meltfptr[55] = 0;
		/*_.IFELSE___V49*/ meltfptr[36] =
		  /*_.LET___V50*/ meltfptr[38];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:4443:/ clear");
	       /*clear *//*_.LET___V50*/ meltfptr[38] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

	      /*^block */
	      /*anyblock */
	      {


		{
		  MELT_LOCATION ("warmelt-macro.melt:4465:/ locexp");
		  /* error_plain */
		    melt_error_str ((melt_ptr_t)
				    ( /*_.SLOC__V16*/ meltfptr[15]),
				    ("GCC-IF bad condition, should be a string or a list of strings"),
				    (melt_ptr_t) 0);
		}
		;
		MELT_LOCATION ("warmelt-macro.melt:4466:/ quasiblock");


     /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

		{
		  MELT_LOCATION ("warmelt-macro.melt:4466:/ locexp");
		  /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
		  if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
		    melt_warn_for_no_expected_secondary_results ();
		  /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
		  ;
		}
		;
		/*^finalreturn */
		;
		/*finalret */ goto labend_rout;
		MELT_LOCATION ("warmelt-macro.melt:4464:/ quasiblock");


		/*_.PROGN___V72*/ meltfptr[60] =
		  /*_.RETURN___V71*/ meltfptr[58];;
		/*^compute */
		/*_.IFELSE___V49*/ meltfptr[36] =
		  /*_.PROGN___V72*/ meltfptr[60];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:4443:/ clear");
	       /*clear *//*_.RETURN___V71*/ meltfptr[58] = 0;
		/*^clear */
	       /*clear *//*_.PROGN___V72*/ meltfptr[60] = 0;
	      }
	      ;
	    }
	  ;
	  /*_.IFELSE___V33*/ meltfptr[21] = /*_.IFELSE___V49*/ meltfptr[36];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4434:/ clear");
	     /*clear *//*_#IS_LIST__L15*/ meltfnum[6] = 0;
	  /*^clear */
	     /*clear *//*_.IFELSE___V49*/ meltfptr[36] = 0;
	}
	;
      }
    ;
    /*_.LET___V14*/ meltfptr[10] = /*_.IFELSE___V33*/ meltfptr[21];;

    MELT_LOCATION ("warmelt-macro.melt:4419:/ clear");
	   /*clear *//*_.CONT__V15*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.SLOC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.CURIF__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.RESTPAIR__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V29*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_#IS_STRING__L10*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V33*/ meltfptr[21] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4415:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V14*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4415:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_GCCIF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_76_warmelt_macro_MEXPAND_GCCIF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_76_warmelt_macro_MEXPAND_GCCIF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_77_warmelt_macro_MEXPAND_COND (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_77_warmelt_macro_MEXPAND_COND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_77_warmelt_macro_MEXPAND_COND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 115
    melt_ptr_t mcfr_varptr[115];
#define MELTFRAM_NBVARNUM 35
    long mcfr_varnum[35];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_77_warmelt_macro_MEXPAND_COND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_77_warmelt_macro_MEXPAND_COND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 115; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_77_warmelt_macro_MEXPAND_COND nbval 115*/
  meltfram__.mcfr_nbvar = 115 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_COND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4478:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4479:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4479:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4479:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4479) ? (4479) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4479:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4480:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4480:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4480:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4480) ? (4480) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4480:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4481:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4481:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4481:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4481;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cond sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4481:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4481:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4481:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4482:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4483:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V15*/ meltfptr[11] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4484:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V16*/ meltfptr[15] = slot;
    };
    ;
 /*_.LIST_FIRST__V17*/ meltfptr[16] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V15*/ meltfptr[11])));;
    /*^compute */
 /*_.PAIR_TAIL__V18*/ meltfptr[17] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V17*/ meltfptr[16])));;
    MELT_LOCATION ("warmelt-macro.melt:4488:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V20*/ meltfptr[19] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_6 */ meltfrout->
						tabval[6])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V20*/ meltfptr[19])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V20*/ meltfptr[19])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V20*/ meltfptr[19])->tabval[0] =
      (melt_ptr_t) ( /*_.LOC__V16*/ meltfptr[15]);
    ;
    /*_.LAMBDA___V19*/ meltfptr[18] = /*_.LAMBDA___V20*/ meltfptr[19];;
    MELT_LOCATION ("warmelt-macro.melt:4485:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[4]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V19*/ meltfptr[18];
      /*_.CEXPTUPLE__V21*/ meltfptr[20] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.PAIR_TAIL__V18*/ meltfptr[17]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#NBCOND__L5*/ meltfnum[3] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.CEXPTUPLE__V21*/ meltfptr[20])));;
    MELT_LOCATION ("warmelt-macro.melt:4494:/ quasiblock");


 /*_.LX__V23*/ meltfptr[22] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.CEXPTUPLE__V21*/ meltfptr[20]), (-1)));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4495:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4495:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4495:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4495;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cond lastcexp lx";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.LX__V23*/ meltfptr[22];
	      /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V25*/ meltfptr[24] =
	      /*_.MELT_DEBUG_FUN__V26*/ meltfptr[25];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4495:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[6] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V26*/ meltfptr[25] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V25*/ meltfptr[24] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4495:/ quasiblock");


      /*_.PROGN___V27*/ meltfptr[25] = /*_.IF___V25*/ meltfptr[24];;
      /*^compute */
      /*_.IFCPP___V24*/ meltfptr[23] = /*_.PROGN___V27*/ meltfptr[25];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4495:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V25*/ meltfptr[24] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V27*/ meltfptr[25] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V24*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V22*/ meltfptr[21] = /*_.LX__V23*/ meltfptr[22];;

    MELT_LOCATION ("warmelt-macro.melt:4494:/ clear");
	   /*clear *//*_.LX__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V24*/ meltfptr[23] = 0;
    /*_.LASTCEXP__V28*/ meltfptr[24] = /*_.LET___V22*/ meltfptr[21];;
    /*^compute */
 /*_#IX__L8*/ meltfnum[6] =
      (( /*_#NBCOND__L5*/ meltfnum[3]) - (1));;
    /*^compute */
    /*_.RES__V29*/ meltfptr[25] = ( /*nil */ NULL);;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4500:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L9*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4500:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4500:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4500;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cond cexptuple";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CEXPTUPLE__V21*/ meltfptr[20];
	      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V31*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4500:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V31*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4500:/ quasiblock");


      /*_.PROGN___V33*/ meltfptr[31] = /*_.IF___V31*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V30*/ meltfptr[22] = /*_.PROGN___V33*/ meltfptr[31];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4500:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V31*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V33*/ meltfptr[31] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V30*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4501:/ loop");
    /*loop */
    {
    labloop_CONDLOOP_1:;	/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-macro.melt:4502:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#I__L11*/ meltfnum[9] =
	  (( /*_#IX__L8*/ meltfnum[6]) < (0));;
	MELT_LOCATION ("warmelt-macro.melt:4502:/ cond");
	/*cond */ if ( /*_#I__L11*/ meltfnum[9])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^quasiblock */


	      /*^compute */
     /*_.CONDLOOP__V35*/ meltfptr[31] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_CONDLOOP_1;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;
	MELT_LOCATION ("warmelt-macro.melt:4503:/ quasiblock");


   /*_.CURCOND__V37*/ meltfptr[36] =
	  (melt_multiple_nth
	   ((melt_ptr_t) ( /*_.CEXPTUPLE__V21*/ meltfptr[20]),
	    ( /*_#IX__L8*/ meltfnum[6])));;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-macro.melt:4504:/ cppif.then");
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	    melt_dbgcounter++;
#endif
	    ;
	  }
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#MELT_NEED_DBG__L12*/ meltfnum[0] =
	    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	    0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	    ;;
	  MELT_LOCATION ("warmelt-macro.melt:4504:/ cond");
	  /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[0])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

       /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
		  meltcallcount	/* the_meltcallcount */
#else
		  0L
#endif /* meltcallcount the_meltcallcount */
		  ;;
		MELT_LOCATION ("warmelt-macro.melt:4504:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[5];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_long =
		    /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
		  /*^apply.arg */
		  argtab[1].meltbp_cstring = "warmelt-macro.melt";
		  /*^apply.arg */
		  argtab[2].meltbp_long = 4504;
		  /*^apply.arg */
		  argtab[3].meltbp_cstring = "mexpand_cond curcond";
		  /*^apply.arg */
		  argtab[4].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CURCOND__V37*/ meltfptr[36];
		  /*_.MELT_DEBUG_FUN__V40*/ meltfptr[39] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!MELT_DEBUG_FUN */ meltfrout->
				  tabval[2])),
				(melt_ptr_t) (( /*nil */ NULL)),
				(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				 MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V39*/ meltfptr[38] =
		  /*_.MELT_DEBUG_FUN__V40*/ meltfptr[39];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:4504:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
		/*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V40*/ meltfptr[39] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

      /*_.IF___V39*/ meltfptr[38] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4504:/ quasiblock");


	  /*_.PROGN___V41*/ meltfptr[39] = /*_.IF___V39*/ meltfptr[38];;
	  /*^compute */
	  /*_.IFCPP___V38*/ meltfptr[37] = /*_.PROGN___V41*/ meltfptr[39];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4504:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[0] = 0;
	  /*^clear */
	       /*clear *//*_.IF___V39*/ meltfptr[38] = 0;
	  /*^clear */
	       /*clear *//*_.PROGN___V41*/ meltfptr[39] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V38*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	MELT_LOCATION ("warmelt-macro.melt:4506:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
   /*_#NULL__L14*/ meltfnum[12] =
	  (( /*_.RES__V29*/ meltfptr[25]) == NULL);;
	MELT_LOCATION ("warmelt-macro.melt:4506:/ cond");
	/*cond */ if ( /*_#NULL__L14*/ meltfnum[12])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#IS_A__L16*/ meltfnum[15] =
		melt_is_instance_of ((melt_ptr_t)
				     ( /*_.CURCOND__V37*/ meltfptr[36]),
				     (melt_ptr_t) (( /*!CLASS_SEXPR */
						    meltfrout->tabval[0])));;
	      MELT_LOCATION ("warmelt-macro.melt:4506:/ cond");
	      /*cond */ if ( /*_#IS_A__L16*/ meltfnum[15])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-macro.melt:4509:/ getslot");
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURCOND__V37*/ meltfptr[36])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
	/*_.SEXP_CONTENTS__V42*/ meltfptr[38] = slot;
		    };
		    ;
       /*_.LIST_FIRST__V43*/ meltfptr[39] =
		      (melt_list_first
		       ((melt_ptr_t)
			( /*_.SEXP_CONTENTS__V42*/ meltfptr[38])));;
		    /*^compute */
       /*_.PAIR_HEAD__V44*/ meltfptr[43] =
		      (melt_pair_head
		       ((melt_ptr_t) ( /*_.LIST_FIRST__V43*/ meltfptr[39])));;
		    /*^compute */
       /*_#__L18*/ meltfnum[17] =
		      ((( /*!konst_7_ELSE */ meltfrout->tabval[7])) ==
		       ( /*_.PAIR_HEAD__V44*/ meltfptr[43]));;
		    /*^compute */
		    /*_#IF___L17*/ meltfnum[16] = /*_#__L18*/ meltfnum[17];;
		    /*epilog */

		    MELT_LOCATION ("warmelt-macro.melt:4506:/ clear");
		 /*clear *//*_.SEXP_CONTENTS__V42*/ meltfptr[38] = 0;
		    /*^clear */
		 /*clear *//*_.LIST_FIRST__V43*/ meltfptr[39] = 0;
		    /*^clear */
		 /*clear *//*_.PAIR_HEAD__V44*/ meltfptr[43] = 0;
		    /*^clear */
		 /*clear *//*_#__L18*/ meltfnum[17] = 0;
		  }
		  ;
		}
	      else
		{		/*^cond.else */

      /*_#IF___L17*/ meltfnum[16] = 0;;
		}
	      ;
	      /*^compute */
	      /*_#IF___L15*/ meltfnum[0] = /*_#IF___L17*/ meltfnum[16];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-macro.melt:4506:/ clear");
	       /*clear *//*_#IS_A__L16*/ meltfnum[15] = 0;
	      /*^clear */
	       /*clear *//*_#IF___L17*/ meltfnum[16] = 0;
	    }
	    ;
	  }
	else
	  {			/*^cond.else */

    /*_#IF___L15*/ meltfnum[0] = 0;;
	  }
	;
	MELT_LOCATION ("warmelt-macro.melt:4506:/ cond");
	/*cond */ if ( /*_#IF___L15*/ meltfnum[0])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      MELT_LOCATION ("warmelt-macro.melt:4510:/ quasiblock");


	      MELT_LOCATION ("warmelt-macro.melt:4511:/ getslot");
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURCOND__V37*/ meltfptr[36]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
      /*_.SEXP_CONTENTS__V47*/ meltfptr[43] = slot;
	      };
	      ;
     /*_.LIST_FIRST__V48*/ meltfptr[47] =
		(melt_list_first
		 ((melt_ptr_t) ( /*_.SEXP_CONTENTS__V47*/ meltfptr[43])));;
	      /*^compute */
     /*_.RESTPAIRS__V49*/ meltfptr[48] =
		(melt_pair_tail
		 ((melt_ptr_t) ( /*_.LIST_FIRST__V48*/ meltfptr[47])));;
	      MELT_LOCATION ("warmelt-macro.melt:4513:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^cond */
	      /*cond */ if ( /*_.RESTPAIRS__V49*/ meltfptr[48])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-macro.melt:4515:/ getslot");
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURCOND__V37*/ meltfptr[36])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
	/*_.LOCA_LOCATION__V51*/ meltfptr[50] = slot;
		    };
		    ;
		    MELT_LOCATION ("warmelt-macro.melt:4514:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    /*^apply */
		    /*apply */
		    {
		      union meltparam_un argtab[4];
		      memset (&argtab, 0, sizeof (argtab));
		      /*^apply.arg */
		      argtab[0].meltbp_aptr =
			(melt_ptr_t *) & /*_.LOCA_LOCATION__V51*/
			meltfptr[50];
		      /*^apply.arg */
		      argtab[1].meltbp_aptr =
			(melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
		      /*^apply.arg */
		      argtab[2].meltbp_aptr =
			(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
		      /*^apply.arg */
		      argtab[3].meltbp_aptr =
			(melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
		      /*_.PAIRLIST_TO_PROGN__V52*/ meltfptr[51] =
			melt_apply ((meltclosure_ptr_t)
				    (( /*!PAIRLIST_TO_PROGN */ meltfrout->
				      tabval[8])),
				    (melt_ptr_t) ( /*_.RESTPAIRS__V49*/
						  meltfptr[48]),
				    (MELTBPARSTR_PTR MELTBPARSTR_PTR
				     MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
				    argtab, "", (union meltparam_un *) 0);
		    }
		    ;
		    /*^compute */
		    /*_.RES__V29*/ meltfptr[25] =
		      /*_.SETQ___V53*/ meltfptr[52] =
		      /*_.PAIRLIST_TO_PROGN__V52*/ meltfptr[51];;
		    /*_.IF___V50*/ meltfptr[49] =
		      /*_.SETQ___V53*/ meltfptr[52];;
		    /*epilog */

		    MELT_LOCATION ("warmelt-macro.melt:4513:/ clear");
		 /*clear *//*_.LOCA_LOCATION__V51*/ meltfptr[50] = 0;
		    /*^clear */
		 /*clear *//*_.PAIRLIST_TO_PROGN__V52*/ meltfptr[51] = 0;
		    /*^clear */
		 /*clear *//*_.SETQ___V53*/ meltfptr[52] = 0;
		  }
		  ;
		}
	      else
		{		/*^cond.else */

      /*_.IF___V50*/ meltfptr[49] = NULL;;
		}
	      ;

#if MELT_HAVE_DEBUG
	      MELT_LOCATION ("warmelt-macro.melt:4518:/ cppif.then");
	      /*^block */
	      /*anyblock */
	      {


		{
		  /*^locexp */
		  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		  melt_dbgcounter++;
#endif
		  ;
		}
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
       /*_#MELT_NEED_DBG__L19*/ meltfnum[17] =
		  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		  0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		  ;;
		MELT_LOCATION ("warmelt-macro.melt:4518:/ cond");
		/*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[17])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

	 /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[15] =
#ifdef meltcallcount
			meltcallcount	/* the_meltcallcount */
#else
			0L
#endif /* meltcallcount the_meltcallcount */
			;;
		      MELT_LOCATION ("warmelt-macro.melt:4518:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[5];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_long =
			  /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[15];
			/*^apply.arg */
			argtab[1].meltbp_cstring = "warmelt-macro.melt";
			/*^apply.arg */
			argtab[2].meltbp_long = 4518;
			/*^apply.arg */
			argtab[3].meltbp_cstring =
			  "mexpand_cond res for :else";
			/*^apply.arg */
			argtab[4].meltbp_aptr =
			  (melt_ptr_t *) & /*_.RES__V29*/ meltfptr[25];
			/*_.MELT_DEBUG_FUN__V56*/ meltfptr[52] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!MELT_DEBUG_FUN */ meltfrout->
					tabval[2])),
				      (melt_ptr_t) (( /*nil */ NULL)),
				      (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				       MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IF___V55*/ meltfptr[51] =
			/*_.MELT_DEBUG_FUN__V56*/ meltfptr[52];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:4518:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[15] = 0;
		      /*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V56*/ meltfptr[52] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

	/*_.IF___V55*/ meltfptr[51] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-macro.melt:4518:/ quasiblock");


		/*_.PROGN___V57*/ meltfptr[52] = /*_.IF___V55*/ meltfptr[51];;
		/*^compute */
		/*_.IFCPP___V54*/ meltfptr[50] =
		  /*_.PROGN___V57*/ meltfptr[52];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:4518:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[17] = 0;
		/*^clear */
		 /*clear *//*_.IF___V55*/ meltfptr[51] = 0;
		/*^clear */
		 /*clear *//*_.PROGN___V57*/ meltfptr[52] = 0;
	      }

#else /*MELT_HAVE_DEBUG */
	      /*^cppif.else */
	      /*_.IFCPP___V54*/ meltfptr[50] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	      ;
	      /*^compute */
	      /*_.LET___V46*/ meltfptr[39] = /*_.IFCPP___V54*/ meltfptr[50];;

	      MELT_LOCATION ("warmelt-macro.melt:4510:/ clear");
	       /*clear *//*_.SEXP_CONTENTS__V47*/ meltfptr[43] = 0;
	      /*^clear */
	       /*clear *//*_.LIST_FIRST__V48*/ meltfptr[47] = 0;
	      /*^clear */
	       /*clear *//*_.RESTPAIRS__V49*/ meltfptr[48] = 0;
	      /*^clear */
	       /*clear *//*_.IF___V50*/ meltfptr[49] = 0;
	      /*^clear */
	       /*clear *//*_.IFCPP___V54*/ meltfptr[50] = 0;
	      /*_.IFELSE___V45*/ meltfptr[38] = /*_.LET___V46*/ meltfptr[39];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-macro.melt:4506:/ clear");
	       /*clear *//*_.LET___V46*/ meltfptr[39] = 0;
	    }
	    ;
	  }
	else
	  {			/*^cond.else */

	    /*^block */
	    /*anyblock */
	    {

	      MELT_LOCATION ("warmelt-macro.melt:4520:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#IS_A__L21*/ meltfnum[16] =
		melt_is_instance_of ((melt_ptr_t)
				     ( /*_.CURCOND__V37*/ meltfptr[36]),
				     (melt_ptr_t) (( /*!CLASS_SEXPR */
						    meltfrout->tabval[0])));;
	      MELT_LOCATION ("warmelt-macro.melt:4520:/ cond");
	      /*cond */ if ( /*_#IS_A__L21*/ meltfnum[16])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-macro.melt:4521:/ quasiblock");


		    /*^getslot */
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURCOND__V37*/ meltfptr[36])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
	/*_.CURCONDCONT__V60*/ meltfptr[43] = slot;
		    };
		    ;
		    MELT_LOCATION ("warmelt-macro.melt:4522:/ getslot");
		    {
		      melt_ptr_t slot = NULL, obj = NULL;
		      obj =
			(melt_ptr_t) ( /*_.CURCOND__V37*/ meltfptr[36])
			/*=obj*/ ;
		      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
	/*_.CURCONDLOC__V61*/ meltfptr[47] = slot;
		    };
		    ;
       /*_.LIST_FIRST__V62*/ meltfptr[48] =
		      (melt_list_first
		       ((melt_ptr_t)
			( /*_.CURCONDCONT__V60*/ meltfptr[43])));;
		    /*^compute */
       /*_.CURCONDTEST__V63*/ meltfptr[49] =
		      (melt_pair_head
		       ((melt_ptr_t) ( /*_.LIST_FIRST__V62*/ meltfptr[48])));;
		    /*^compute */
       /*_.LIST_FIRST__V64*/ meltfptr[50] =
		      (melt_list_first
		       ((melt_ptr_t)
			( /*_.CURCONDCONT__V60*/ meltfptr[43])));;
		    /*^compute */
       /*_.CURCONDRESTPAIRS__V65*/ meltfptr[39] =
		      (melt_pair_tail
		       ((melt_ptr_t) ( /*_.LIST_FIRST__V64*/ meltfptr[50])));;
		    MELT_LOCATION ("warmelt-macro.melt:4526:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
       /*_#NULL__L22*/ meltfnum[15] =
		      (( /*_.CURCONDRESTPAIRS__V65*/ meltfptr[39]) == NULL);;
		    MELT_LOCATION ("warmelt-macro.melt:4526:/ cond");
		    /*cond */ if ( /*_#NULL__L22*/ meltfnum[15])	/*then */
		      {
			/*^cond.then */
			/*^block */
			/*anyblock */
			{

			  MELT_LOCATION
			    ("warmelt-macro.melt:4528:/ checksignal");
			  MELT_CHECK_SIGNAL ();
			  ;
			  MELT_LOCATION
			    ("warmelt-macro.melt:4532:/ checksignal");
			  MELT_CHECK_SIGNAL ();
			  ;
			  /*^apply */
			  /*apply */
			  {
			    union meltparam_un argtab[3];
			    memset (&argtab, 0, sizeof (argtab));
			    /*^apply.arg */
			    argtab[0].meltbp_aptr =
			      (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
			    /*^apply.arg */
			    argtab[1].meltbp_aptr =
			      (melt_ptr_t *) & /*_.MEXPANDER__V4*/
			      meltfptr[3];
			    /*^apply.arg */
			    argtab[2].meltbp_aptr =
			      (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
			    /*_.MEXPANDER__V67*/ meltfptr[66] =
			      melt_apply ((meltclosure_ptr_t)
					  ( /*_.MEXPANDER__V4*/ meltfptr[3]),
					  (melt_ptr_t) ( /*_.CURCONDTEST__V63*/ meltfptr[49]), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
			  }
			  ;
			  MELT_LOCATION
			    ("warmelt-macro.melt:4531:/ blockmultialloc");
			  /*multiallocblock */
			  {
			    struct meltletrec_1_st
			    {
			      struct MELT_MULTIPLE_STRUCT (2)
				rtup_0__TUPLREC__x2;
			      long meltletrec_1_endgap;
			    } *meltletrec_1_ptr = 0;
			    meltletrec_1_ptr =
			      (struct meltletrec_1_st *)
			      meltgc_allocate (sizeof
					       (struct meltletrec_1_st), 0);
			    /*^blockmultialloc.initfill */
			    /*inimult rtup_0__TUPLREC__x2 */
 /*_.TUPLREC___V69*/ meltfptr[68] =
			      (melt_ptr_t) &
			      meltletrec_1_ptr->rtup_0__TUPLREC__x2;
			    meltletrec_1_ptr->rtup_0__TUPLREC__x2.discr =
			      (meltobject_ptr_t) (((melt_ptr_t)
						   (MELT_PREDEF
						    (DISCR_MULTIPLE))));
			    meltletrec_1_ptr->rtup_0__TUPLREC__x2.nbval = 2;


			    /*^putuple */
			    /*putupl#2 */
			    melt_assertmsg ("putupl [:4531] #2 checktup",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.TUPLREC___V69*/ meltfptr[68])) == MELTOBMAG_MULTIPLE);
			    melt_assertmsg ("putupl [:4531] #2 checkoff",
					    (0 >= 0
					     && 0 <
					     melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V69*/ meltfptr[68]))));
			    ((meltmultiple_ptr_t)
			     ( /*_.TUPLREC___V69*/ meltfptr[68]))->tabval[0] =
	 (melt_ptr_t) ( /*_.MEXPANDER__V67*/ meltfptr[66]);
			    ;
			    /*^putuple */
			    /*putupl#3 */
			    melt_assertmsg ("putupl [:4531] #3 checktup",
					    melt_magic_discr ((melt_ptr_t)
							      ( /*_.TUPLREC___V69*/ meltfptr[68])) == MELTOBMAG_MULTIPLE);
			    melt_assertmsg ("putupl [:4531] #3 checkoff",
					    (1 >= 0
					     && 1 <
					     melt_multiple_length ((melt_ptr_t) ( /*_.TUPLREC___V69*/ meltfptr[68]))));
			    ((meltmultiple_ptr_t)
			     ( /*_.TUPLREC___V69*/ meltfptr[68]))->tabval[1] =
	 (melt_ptr_t) ( /*_.RES__V29*/ meltfptr[25]);
			    ;
			    /*^touch */
			    meltgc_touch ( /*_.TUPLREC___V69*/ meltfptr[68]);
			    ;
			    /*_.TUPLE___V68*/ meltfptr[67] =
			      /*_.TUPLREC___V69*/ meltfptr[68];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-macro.melt:4531:/ clear");
		    /*clear *//*_.TUPLREC___V69*/ meltfptr[68] = 0;
			    /*^clear */
		    /*clear *//*_.TUPLREC___V69*/ meltfptr[68] = 0;
			  }	/*end multiallocblock */
			  ;
			  MELT_LOCATION
			    ("warmelt-macro.melt:4528:/ quasiblock");


			  /*^rawallocobj */
			  /*rawallocobj */
			  {
			    melt_ptr_t newobj = 0;
			    melt_raw_object_create (newobj,
						    (melt_ptr_t) (( /*!CLASS_SOURCE_OR */ meltfrout->tabval[9])), (3), "CLASS_SOURCE_OR");
	  /*_.INST__V71*/ meltfptr[70] =
			      newobj;
			  };
			  ;
			  /*^putslot */
			  /*putslot */
			  melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
					  melt_magic_discr ((melt_ptr_t)
							    ( /*_.INST__V71*/
							     meltfptr[70])) ==
					  MELTOBMAG_OBJECT);
			  melt_putfield_object (( /*_.INST__V71*/
						 meltfptr[70]), (1),
						( /*_.CURCONDLOC__V61*/
						 meltfptr[47]),
						"LOCA_LOCATION");
			  ;
			  /*^putslot */
			  /*putslot */
			  melt_assertmsg ("putslot checkobj @SOR_DISJ",
					  melt_magic_discr ((melt_ptr_t)
							    ( /*_.INST__V71*/
							     meltfptr[70])) ==
					  MELTOBMAG_OBJECT);
			  melt_putfield_object (( /*_.INST__V71*/
						 meltfptr[70]), (2),
						( /*_.TUPLE___V68*/
						 meltfptr[67]), "SOR_DISJ");
			  ;
			  /*^touchobj */

			  melt_dbgtrace_written_object ( /*_.INST__V71*/
							meltfptr[70],
							"newly made instance");
			  ;
			  /*_.INST___V70*/ meltfptr[68] =
			    /*_.INST__V71*/ meltfptr[70];;
			  MELT_LOCATION ("warmelt-macro.melt:4527:/ compute");
			  /*_.RES__V29*/ meltfptr[25] =
			    /*_.SETQ___V72*/ meltfptr[71] =
			    /*_.INST___V70*/ meltfptr[68];;

#if MELT_HAVE_DEBUG
			  MELT_LOCATION
			    ("warmelt-macro.melt:4534:/ cppif.then");
			  /*^block */
			  /*anyblock */
			  {


			    {
			      /*^locexp */
			      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			      melt_dbgcounter++;
#endif
			      ;
			    }
			    ;
			    /*^checksignal */
			    MELT_CHECK_SIGNAL ();
			    ;
	   /*_#MELT_NEED_DBG__L23*/ meltfnum[17] =
			      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			      0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			      ;;
			    MELT_LOCATION ("warmelt-macro.melt:4534:/ cond");
			    /*cond */ if ( /*_#MELT_NEED_DBG__L23*/ meltfnum[17])	/*then */
			      {
				/*^cond.then */
				/*^block */
				/*anyblock */
				{

	     /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[23] =
#ifdef meltcallcount
				    meltcallcount	/* the_meltcallcount */
#else
				    0L
#endif /* meltcallcount the_meltcallcount */
				    ;;
				  MELT_LOCATION
				    ("warmelt-macro.melt:4534:/ checksignal");
				  MELT_CHECK_SIGNAL ();
				  ;
				  /*^apply */
				  /*apply */
				  {
				    union meltparam_un argtab[5];
				    memset (&argtab, 0, sizeof (argtab));
				    /*^apply.arg */
				    argtab[0].meltbp_long =
				      /*_#THE_MELTCALLCOUNT__L24*/
				      meltfnum[23];
				    /*^apply.arg */
				    argtab[1].meltbp_cstring =
				      "warmelt-macro.melt";
				    /*^apply.arg */
				    argtab[2].meltbp_long = 4534;
				    /*^apply.arg */
				    argtab[3].meltbp_cstring =
				      "mexpand_cond res for monoexp cond";
				    /*^apply.arg */
				    argtab[4].meltbp_aptr =
				      (melt_ptr_t *) & /*_.RES__V29*/
				      meltfptr[25];
				    /*_.MELT_DEBUG_FUN__V75*/ meltfptr[74] =
				      melt_apply ((meltclosure_ptr_t)
						  (( /*!MELT_DEBUG_FUN */
						    meltfrout->tabval[2])),
						  (melt_ptr_t) (( /*nil */
								 NULL)),
						  (MELTBPARSTR_LONG
						   MELTBPARSTR_CSTRING
						   MELTBPARSTR_LONG
						   MELTBPARSTR_CSTRING
						   MELTBPARSTR_PTR ""),
						  argtab, "",
						  (union meltparam_un *) 0);
				  }
				  ;
				  /*_.IF___V74*/ meltfptr[73] =
				    /*_.MELT_DEBUG_FUN__V75*/ meltfptr[74];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-macro.melt:4534:/ clear");
		       /*clear *//*_#THE_MELTCALLCOUNT__L24*/
				    meltfnum[23] = 0;
				  /*^clear */
		       /*clear *//*_.MELT_DEBUG_FUN__V75*/
				    meltfptr[74] = 0;
				}
				;
			      }
			    else
			      {	/*^cond.else */

	    /*_.IF___V74*/ meltfptr[73] = NULL;;
			      }
			    ;
			    MELT_LOCATION
			      ("warmelt-macro.melt:4534:/ quasiblock");


			    /*_.PROGN___V76*/ meltfptr[74] =
			      /*_.IF___V74*/ meltfptr[73];;
			    /*^compute */
			    /*_.IFCPP___V73*/ meltfptr[72] =
			      /*_.PROGN___V76*/ meltfptr[74];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-macro.melt:4534:/ clear");
		     /*clear *//*_#MELT_NEED_DBG__L23*/ meltfnum[17] =
			      0;
			    /*^clear */
		     /*clear *//*_.IF___V74*/ meltfptr[73] = 0;
			    /*^clear */
		     /*clear *//*_.PROGN___V76*/ meltfptr[74] = 0;
			  }

#else /*MELT_HAVE_DEBUG */
			  /*^cppif.else */
			  /*_.IFCPP___V73*/ meltfptr[72] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
			  ;
			  MELT_LOCATION
			    ("warmelt-macro.melt:4526:/ quasiblock");


			  /*_.PROGN___V77*/ meltfptr[73] =
			    /*_.IFCPP___V73*/ meltfptr[72];;
			  /*^compute */
			  /*_.IFELSE___V66*/ meltfptr[65] =
			    /*_.PROGN___V77*/ meltfptr[73];;
			  /*epilog */

			  MELT_LOCATION ("warmelt-macro.melt:4526:/ clear");
		   /*clear *//*_.MEXPANDER__V67*/ meltfptr[66] = 0;
			  /*^clear */
		   /*clear *//*_.TUPLE___V68*/ meltfptr[67] = 0;
			  /*^clear */
		   /*clear *//*_.INST___V70*/ meltfptr[68] = 0;
			  /*^clear */
		   /*clear *//*_.SETQ___V72*/ meltfptr[71] = 0;
			  /*^clear */
		   /*clear *//*_.IFCPP___V73*/ meltfptr[72] = 0;
			  /*^clear */
		   /*clear *//*_.PROGN___V77*/ meltfptr[73] = 0;
			}
			;
		      }
		    else
		      {		/*^cond.else */

			/*^block */
			/*anyblock */
			{

			  MELT_LOCATION
			    ("warmelt-macro.melt:4536:/ checksignal");
			  MELT_CHECK_SIGNAL ();
			  ;
	 /*_.PAIR_TAIL__V78*/ meltfptr[74] =
			    (melt_pair_tail
			     ((melt_ptr_t)
			      ( /*_.CURCONDRESTPAIRS__V65*/ meltfptr[39])));;
			  /*^compute */
	 /*_#NULL__L25*/ meltfnum[23] =
			    (( /*_.PAIR_TAIL__V78*/ meltfptr[74]) == NULL);;
			  MELT_LOCATION ("warmelt-macro.melt:4536:/ cond");
			  /*cond */ if ( /*_#NULL__L25*/ meltfnum[23])	/*then */
			    {
			      /*^cond.then */
			      /*^block */
			      /*anyblock */
			      {

				MELT_LOCATION
				  ("warmelt-macro.melt:4538:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
				MELT_LOCATION
				  ("warmelt-macro.melt:4540:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
				/*^apply */
				/*apply */
				{
				  union meltparam_un argtab[3];
				  memset (&argtab, 0, sizeof (argtab));
				  /*^apply.arg */
				  argtab[0].meltbp_aptr =
				    (melt_ptr_t *) & /*_.ENV__V3*/
				    meltfptr[2];
				  /*^apply.arg */
				  argtab[1].meltbp_aptr =
				    (melt_ptr_t *) & /*_.MEXPANDER__V4*/
				    meltfptr[3];
				  /*^apply.arg */
				  argtab[2].meltbp_aptr =
				    (melt_ptr_t *) & /*_.MODCTX__V5*/
				    meltfptr[4];
				  /*_.MEXPANDER__V80*/ meltfptr[67] =
				    melt_apply ((meltclosure_ptr_t)
						( /*_.MEXPANDER__V4*/
						 meltfptr[3]),
						(melt_ptr_t) ( /*_.CURCONDTEST__V63*/ meltfptr[49]), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
				}
				;
	   /*_.PAIR_HEAD__V81*/ meltfptr[68] =
				  (melt_pair_head
				   ((melt_ptr_t)
				    ( /*_.CURCONDRESTPAIRS__V65*/
				     meltfptr[39])));;
				MELT_LOCATION
				  ("warmelt-macro.melt:4541:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
				/*^apply */
				/*apply */
				{
				  union meltparam_un argtab[3];
				  memset (&argtab, 0, sizeof (argtab));
				  /*^apply.arg */
				  argtab[0].meltbp_aptr =
				    (melt_ptr_t *) & /*_.ENV__V3*/
				    meltfptr[2];
				  /*^apply.arg */
				  argtab[1].meltbp_aptr =
				    (melt_ptr_t *) & /*_.MEXPANDER__V4*/
				    meltfptr[3];
				  /*^apply.arg */
				  argtab[2].meltbp_aptr =
				    (melt_ptr_t *) & /*_.MODCTX__V5*/
				    meltfptr[4];
				  /*_.MEXPANDER__V82*/ meltfptr[71] =
				    melt_apply ((meltclosure_ptr_t)
						( /*_.MEXPANDER__V4*/
						 meltfptr[3]),
						(melt_ptr_t) ( /*_.PAIR_HEAD__V81*/ meltfptr[68]), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
				}
				;
				MELT_LOCATION
				  ("warmelt-macro.melt:4538:/ quasiblock");


				/*^rawallocobj */
				/*rawallocobj */
				{
				  melt_ptr_t newobj = 0;
				  melt_raw_object_create (newobj,
							  (melt_ptr_t) (( /*!CLASS_SOURCE_IFELSE */ meltfrout->tabval[10])), (5), "CLASS_SOURCE_IFELSE");
	    /*_.INST__V84*/ meltfptr[73] =
				    newobj;
				};
				;
				/*^putslot */
				/*putslot */
				melt_assertmsg
				  ("putslot checkobj @LOCA_LOCATION",
				   melt_magic_discr ((melt_ptr_t)
						     ( /*_.INST__V84*/
						      meltfptr[73])) ==
				   MELTOBMAG_OBJECT);
				melt_putfield_object (( /*_.INST__V84*/
						       meltfptr[73]), (1),
						      ( /*_.CURCONDLOC__V61*/
						       meltfptr[47]),
						      "LOCA_LOCATION");
				;
				/*^putslot */
				/*putslot */
				melt_assertmsg ("putslot checkobj @SIF_TEST",
						melt_magic_discr ((melt_ptr_t)
								  ( /*_.INST__V84*/ meltfptr[73])) == MELTOBMAG_OBJECT);
				melt_putfield_object (( /*_.INST__V84*/
						       meltfptr[73]), (2),
						      ( /*_.MEXPANDER__V80*/
						       meltfptr[67]),
						      "SIF_TEST");
				;
				/*^putslot */
				/*putslot */
				melt_assertmsg ("putslot checkobj @SIF_THEN",
						melt_magic_discr ((melt_ptr_t)
								  ( /*_.INST__V84*/ meltfptr[73])) == MELTOBMAG_OBJECT);
				melt_putfield_object (( /*_.INST__V84*/
						       meltfptr[73]), (3),
						      ( /*_.MEXPANDER__V82*/
						       meltfptr[71]),
						      "SIF_THEN");
				;
				/*^putslot */
				/*putslot */
				melt_assertmsg ("putslot checkobj @SIF_ELSE",
						melt_magic_discr ((melt_ptr_t)
								  ( /*_.INST__V84*/ meltfptr[73])) == MELTOBMAG_OBJECT);
				melt_putfield_object (( /*_.INST__V84*/
						       meltfptr[73]), (4),
						      ( /*_.RES__V29*/
						       meltfptr[25]),
						      "SIF_ELSE");
				;
				/*^touchobj */

				melt_dbgtrace_written_object ( /*_.INST__V84*/
							      meltfptr[73],
							      "newly made instance");
				;
				/*_.INST___V83*/ meltfptr[72] =
				  /*_.INST__V84*/ meltfptr[73];;
				MELT_LOCATION
				  ("warmelt-macro.melt:4537:/ compute");
				/*_.RES__V29*/ meltfptr[25] =
				  /*_.SETQ___V85*/ meltfptr[84] =
				  /*_.INST___V83*/ meltfptr[72];;

#if MELT_HAVE_DEBUG
				MELT_LOCATION
				  ("warmelt-macro.melt:4544:/ cppif.then");
				/*^block */
				/*anyblock */
				{


				  {
				    /*^locexp */
				    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				    melt_dbgcounter++;
#endif
				    ;
				  }
				  ;
				  /*^checksignal */
				  MELT_CHECK_SIGNAL ();
				  ;
	     /*_#MELT_NEED_DBG__L26*/ meltfnum[17] =
				    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				    ( /*melt_need_dbg */
				     melt_need_debug ((int) 0))
#else
				    0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				    ;;
				  MELT_LOCATION
				    ("warmelt-macro.melt:4544:/ cond");
				  /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[17])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {

	       /*_#THE_MELTCALLCOUNT__L27*/
					  meltfnum[26] =
#ifdef meltcallcount
					  meltcallcount	/* the_meltcallcount */
#else
					  0L
#endif /* meltcallcount the_meltcallcount */
					  ;;
					MELT_LOCATION
					  ("warmelt-macro.melt:4544:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*^apply */
					/*apply */
					{
					  union meltparam_un argtab[5];
					  memset (&argtab, 0,
						  sizeof (argtab));
					  /*^apply.arg */
					  argtab[0].meltbp_long =
					    /*_#THE_MELTCALLCOUNT__L27*/
					    meltfnum[26];
					  /*^apply.arg */
					  argtab[1].meltbp_cstring =
					    "warmelt-macro.melt";
					  /*^apply.arg */
					  argtab[2].meltbp_long = 4544;
					  /*^apply.arg */
					  argtab[3].meltbp_cstring =
					    "mexpand_cond res for biexp cond";
					  /*^apply.arg */
					  argtab[4].meltbp_aptr =
					    (melt_ptr_t *) & /*_.RES__V29*/
					    meltfptr[25];
					  /*_.MELT_DEBUG_FUN__V88*/
					    meltfptr[87] =
					    melt_apply ((meltclosure_ptr_t)
							(( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])), (melt_ptr_t) (( /*nil */ NULL)), (MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
					}
					;
					/*_.IF___V87*/ meltfptr[86] =
					  /*_.MELT_DEBUG_FUN__V88*/
					  meltfptr[87];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-macro.melt:4544:/ clear");
			 /*clear *//*_#THE_MELTCALLCOUNT__L27*/
					  meltfnum[26] = 0;
					/*^clear */
			 /*clear *//*_.MELT_DEBUG_FUN__V88*/
					  meltfptr[87] = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

	      /*_.IF___V87*/ meltfptr[86] = NULL;;
				    }
				  ;
				  MELT_LOCATION
				    ("warmelt-macro.melt:4544:/ quasiblock");


				  /*_.PROGN___V89*/ meltfptr[87] =
				    /*_.IF___V87*/ meltfptr[86];;
				  /*^compute */
				  /*_.IFCPP___V86*/ meltfptr[85] =
				    /*_.PROGN___V89*/ meltfptr[87];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-macro.melt:4544:/ clear");
		       /*clear *//*_#MELT_NEED_DBG__L26*/
				    meltfnum[17] = 0;
				  /*^clear */
		       /*clear *//*_.IF___V87*/ meltfptr[86] = 0;
				  /*^clear */
		       /*clear *//*_.PROGN___V89*/ meltfptr[87] =
				    0;
				}

#else /*MELT_HAVE_DEBUG */
				/*^cppif.else */
				/*_.IFCPP___V86*/ meltfptr[85] =
				  ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
				;
				MELT_LOCATION
				  ("warmelt-macro.melt:4536:/ quasiblock");


				/*_.PROGN___V90*/ meltfptr[86] =
				  /*_.IFCPP___V86*/ meltfptr[85];;
				/*^compute */
				/*_.IFELSE___V79*/ meltfptr[66] =
				  /*_.PROGN___V90*/ meltfptr[86];;
				/*epilog */

				MELT_LOCATION
				  ("warmelt-macro.melt:4536:/ clear");
		     /*clear *//*_.MEXPANDER__V80*/ meltfptr[67] =
				  0;
				/*^clear */
		     /*clear *//*_.PAIR_HEAD__V81*/ meltfptr[68] =
				  0;
				/*^clear */
		     /*clear *//*_.MEXPANDER__V82*/ meltfptr[71] =
				  0;
				/*^clear */
		     /*clear *//*_.INST___V83*/ meltfptr[72] = 0;
				/*^clear */
		     /*clear *//*_.SETQ___V85*/ meltfptr[84] = 0;
				/*^clear */
		     /*clear *//*_.IFCPP___V86*/ meltfptr[85] = 0;
				/*^clear */
		     /*clear *//*_.PROGN___V90*/ meltfptr[86] = 0;
			      }
			      ;
			    }
			  else
			    {	/*^cond.else */

			      /*^block */
			      /*anyblock */
			      {

				MELT_LOCATION
				  ("warmelt-macro.melt:4548:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
				MELT_LOCATION
				  ("warmelt-macro.melt:4550:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
				/*^apply */
				/*apply */
				{
				  union meltparam_un argtab[3];
				  memset (&argtab, 0, sizeof (argtab));
				  /*^apply.arg */
				  argtab[0].meltbp_aptr =
				    (melt_ptr_t *) & /*_.ENV__V3*/
				    meltfptr[2];
				  /*^apply.arg */
				  argtab[1].meltbp_aptr =
				    (melt_ptr_t *) & /*_.MEXPANDER__V4*/
				    meltfptr[3];
				  /*^apply.arg */
				  argtab[2].meltbp_aptr =
				    (melt_ptr_t *) & /*_.MODCTX__V5*/
				    meltfptr[4];
				  /*_.MEXPANDER__V91*/ meltfptr[87] =
				    melt_apply ((meltclosure_ptr_t)
						( /*_.MEXPANDER__V4*/
						 meltfptr[3]),
						(melt_ptr_t) ( /*_.CURCONDTEST__V63*/ meltfptr[49]), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
				}
				;
				MELT_LOCATION
				  ("warmelt-macro.melt:4551:/ checksignal");
				MELT_CHECK_SIGNAL ();
				;
				/*^apply */
				/*apply */
				{
				  union meltparam_un argtab[4];
				  memset (&argtab, 0, sizeof (argtab));
				  /*^apply.arg */
				  argtab[0].meltbp_aptr =
				    (melt_ptr_t *) & /*_.CURCONDLOC__V61*/
				    meltfptr[47];
				  /*^apply.arg */
				  argtab[1].meltbp_aptr =
				    (melt_ptr_t *) & /*_.ENV__V3*/
				    meltfptr[2];
				  /*^apply.arg */
				  argtab[2].meltbp_aptr =
				    (melt_ptr_t *) & /*_.MEXPANDER__V4*/
				    meltfptr[3];
				  /*^apply.arg */
				  argtab[3].meltbp_aptr =
				    (melt_ptr_t *) & /*_.MODCTX__V5*/
				    meltfptr[4];
				  /*_.PAIRLIST_TO_PROGN__V92*/ meltfptr[67] =
				    melt_apply ((meltclosure_ptr_t)
						(( /*!PAIRLIST_TO_PROGN */
						  meltfrout->tabval[8])),
						(melt_ptr_t) ( /*_.CURCONDRESTPAIRS__V65*/ meltfptr[39]), (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
				}
				;
				MELT_LOCATION
				  ("warmelt-macro.melt:4548:/ quasiblock");


				/*^rawallocobj */
				/*rawallocobj */
				{
				  melt_ptr_t newobj = 0;
				  melt_raw_object_create (newobj,
							  (melt_ptr_t) (( /*!CLASS_SOURCE_IFELSE */ meltfrout->tabval[10])), (5), "CLASS_SOURCE_IFELSE");
	    /*_.INST__V94*/ meltfptr[71] =
				    newobj;
				};
				;
				/*^putslot */
				/*putslot */
				melt_assertmsg
				  ("putslot checkobj @LOCA_LOCATION",
				   melt_magic_discr ((melt_ptr_t)
						     ( /*_.INST__V94*/
						      meltfptr[71])) ==
				   MELTOBMAG_OBJECT);
				melt_putfield_object (( /*_.INST__V94*/
						       meltfptr[71]), (1),
						      ( /*_.CURCONDLOC__V61*/
						       meltfptr[47]),
						      "LOCA_LOCATION");
				;
				/*^putslot */
				/*putslot */
				melt_assertmsg ("putslot checkobj @SIF_TEST",
						melt_magic_discr ((melt_ptr_t)
								  ( /*_.INST__V94*/ meltfptr[71])) == MELTOBMAG_OBJECT);
				melt_putfield_object (( /*_.INST__V94*/
						       meltfptr[71]), (2),
						      ( /*_.MEXPANDER__V91*/
						       meltfptr[87]),
						      "SIF_TEST");
				;
				/*^putslot */
				/*putslot */
				melt_assertmsg ("putslot checkobj @SIF_THEN",
						melt_magic_discr ((melt_ptr_t)
								  ( /*_.INST__V94*/ meltfptr[71])) == MELTOBMAG_OBJECT);
				melt_putfield_object (( /*_.INST__V94*/
						       meltfptr[71]), (3),
						      ( /*_.PAIRLIST_TO_PROGN__V92*/ meltfptr[67]), "SIF_THEN");
				;
				/*^putslot */
				/*putslot */
				melt_assertmsg ("putslot checkobj @SIF_ELSE",
						melt_magic_discr ((melt_ptr_t)
								  ( /*_.INST__V94*/ meltfptr[71])) == MELTOBMAG_OBJECT);
				melt_putfield_object (( /*_.INST__V94*/
						       meltfptr[71]), (4),
						      ( /*_.RES__V29*/
						       meltfptr[25]),
						      "SIF_ELSE");
				;
				/*^touchobj */

				melt_dbgtrace_written_object ( /*_.INST__V94*/
							      meltfptr[71],
							      "newly made instance");
				;
				/*_.INST___V93*/ meltfptr[68] =
				  /*_.INST__V94*/ meltfptr[71];;
				MELT_LOCATION
				  ("warmelt-macro.melt:4547:/ compute");
				/*_.RES__V29*/ meltfptr[25] =
				  /*_.SETQ___V95*/ meltfptr[72] =
				  /*_.INST___V93*/ meltfptr[68];;

#if MELT_HAVE_DEBUG
				MELT_LOCATION
				  ("warmelt-macro.melt:4553:/ cppif.then");
				/*^block */
				/*anyblock */
				{


				  {
				    /*^locexp */
				    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
				    melt_dbgcounter++;
#endif
				    ;
				  }
				  ;
				  /*^checksignal */
				  MELT_CHECK_SIGNAL ();
				  ;
	     /*_#MELT_NEED_DBG__L28*/ meltfnum[26] =
				    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
				    ( /*melt_need_dbg */
				     melt_need_debug ((int) 0))
#else
				    0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
				    ;;
				  MELT_LOCATION
				    ("warmelt-macro.melt:4553:/ cond");
				  /*cond */ if ( /*_#MELT_NEED_DBG__L28*/ meltfnum[26])	/*then */
				    {
				      /*^cond.then */
				      /*^block */
				      /*anyblock */
				      {

	       /*_#THE_MELTCALLCOUNT__L29*/
					  meltfnum[17] =
#ifdef meltcallcount
					  meltcallcount	/* the_meltcallcount */
#else
					  0L
#endif /* meltcallcount the_meltcallcount */
					  ;;
					MELT_LOCATION
					  ("warmelt-macro.melt:4553:/ checksignal");
					MELT_CHECK_SIGNAL ();
					;
					/*^apply */
					/*apply */
					{
					  union meltparam_un argtab[5];
					  memset (&argtab, 0,
						  sizeof (argtab));
					  /*^apply.arg */
					  argtab[0].meltbp_long =
					    /*_#THE_MELTCALLCOUNT__L29*/
					    meltfnum[17];
					  /*^apply.arg */
					  argtab[1].meltbp_cstring =
					    "warmelt-macro.melt";
					  /*^apply.arg */
					  argtab[2].meltbp_long = 4553;
					  /*^apply.arg */
					  argtab[3].meltbp_cstring =
					    "mexpand_cond res for manyexp cond";
					  /*^apply.arg */
					  argtab[4].meltbp_aptr =
					    (melt_ptr_t *) & /*_.RES__V29*/
					    meltfptr[25];
					  /*_.MELT_DEBUG_FUN__V98*/
					    meltfptr[86] =
					    melt_apply ((meltclosure_ptr_t)
							(( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])), (melt_ptr_t) (( /*nil */ NULL)), (MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_LONG MELTBPARSTR_CSTRING MELTBPARSTR_PTR ""), argtab, "", (union meltparam_un *) 0);
					}
					;
					/*_.IF___V97*/ meltfptr[85] =
					  /*_.MELT_DEBUG_FUN__V98*/
					  meltfptr[86];;
					/*epilog */

					MELT_LOCATION
					  ("warmelt-macro.melt:4553:/ clear");
			 /*clear *//*_#THE_MELTCALLCOUNT__L29*/
					  meltfnum[17] = 0;
					/*^clear */
			 /*clear *//*_.MELT_DEBUG_FUN__V98*/
					  meltfptr[86] = 0;
				      }
				      ;
				    }
				  else
				    {	/*^cond.else */

	      /*_.IF___V97*/ meltfptr[85] = NULL;;
				    }
				  ;
				  MELT_LOCATION
				    ("warmelt-macro.melt:4553:/ quasiblock");


				  /*_.PROGN___V99*/ meltfptr[86] =
				    /*_.IF___V97*/ meltfptr[85];;
				  /*^compute */
				  /*_.IFCPP___V96*/ meltfptr[84] =
				    /*_.PROGN___V99*/ meltfptr[86];;
				  /*epilog */

				  MELT_LOCATION
				    ("warmelt-macro.melt:4553:/ clear");
		       /*clear *//*_#MELT_NEED_DBG__L28*/
				    meltfnum[26] = 0;
				  /*^clear */
		       /*clear *//*_.IF___V97*/ meltfptr[85] = 0;
				  /*^clear */
		       /*clear *//*_.PROGN___V99*/ meltfptr[86] =
				    0;
				}

#else /*MELT_HAVE_DEBUG */
				/*^cppif.else */
				/*_.IFCPP___V96*/ meltfptr[84] =
				  ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
				;
				MELT_LOCATION
				  ("warmelt-macro.melt:4546:/ quasiblock");


				/*_.PROGN___V100*/ meltfptr[85] =
				  /*_.IFCPP___V96*/ meltfptr[84];;
				/*^compute */
				/*_.IFELSE___V79*/ meltfptr[66] =
				  /*_.PROGN___V100*/ meltfptr[85];;
				/*epilog */

				MELT_LOCATION
				  ("warmelt-macro.melt:4536:/ clear");
		     /*clear *//*_.MEXPANDER__V91*/ meltfptr[87] =
				  0;
				/*^clear */
		     /*clear *//*_.PAIRLIST_TO_PROGN__V92*/
				  meltfptr[67] = 0;
				/*^clear */
		     /*clear *//*_.INST___V93*/ meltfptr[68] = 0;
				/*^clear */
		     /*clear *//*_.SETQ___V95*/ meltfptr[72] = 0;
				/*^clear */
		     /*clear *//*_.IFCPP___V96*/ meltfptr[84] = 0;
				/*^clear */
		     /*clear *//*_.PROGN___V100*/ meltfptr[85] = 0;
			      }
			      ;
			    }
			  ;
			  /*_.IFELSE___V66*/ meltfptr[65] =
			    /*_.IFELSE___V79*/ meltfptr[66];;
			  /*epilog */

			  MELT_LOCATION ("warmelt-macro.melt:4526:/ clear");
		   /*clear *//*_.PAIR_TAIL__V78*/ meltfptr[74] = 0;
			  /*^clear */
		   /*clear *//*_#NULL__L25*/ meltfnum[23] = 0;
			  /*^clear */
		   /*clear *//*_.IFELSE___V79*/ meltfptr[66] = 0;
			}
			;
		      }
		    ;
		    /*_.LET___V59*/ meltfptr[52] =
		      /*_.IFELSE___V66*/ meltfptr[65];;

		    MELT_LOCATION ("warmelt-macro.melt:4521:/ clear");
		 /*clear *//*_.CURCONDCONT__V60*/ meltfptr[43] = 0;
		    /*^clear */
		 /*clear *//*_.CURCONDLOC__V61*/ meltfptr[47] = 0;
		    /*^clear */
		 /*clear *//*_.LIST_FIRST__V62*/ meltfptr[48] = 0;
		    /*^clear */
		 /*clear *//*_.CURCONDTEST__V63*/ meltfptr[49] = 0;
		    /*^clear */
		 /*clear *//*_.LIST_FIRST__V64*/ meltfptr[50] = 0;
		    /*^clear */
		 /*clear *//*_.CURCONDRESTPAIRS__V65*/ meltfptr[39] = 0;
		    /*^clear */
		 /*clear *//*_#NULL__L22*/ meltfnum[15] = 0;
		    /*^clear */
		 /*clear *//*_.IFELSE___V66*/ meltfptr[65] = 0;
		    /*_.IFELSE___V58*/ meltfptr[51] =
		      /*_.LET___V59*/ meltfptr[52];;
		    /*epilog */

		    MELT_LOCATION ("warmelt-macro.melt:4520:/ clear");
		 /*clear *//*_.LET___V59*/ meltfptr[52] = 0;
		  }
		  ;
		}
	      else
		{		/*^cond.else */

		  /*^block */
		  /*anyblock */
		  {

		    MELT_LOCATION ("warmelt-macro.melt:4557:/ checksignal");
		    MELT_CHECK_SIGNAL ();
		    ;
		    MELT_LOCATION
		      ("warmelt-macro.melt:4559:/ blockmultialloc");
		    /*multiallocblock */
		    {
		      struct meltletrec_2_st
		      {
			struct MELT_MULTIPLE_STRUCT (2) rtup_0__TUPLREC__x3;
			long meltletrec_2_endgap;
		      } *meltletrec_2_ptr = 0;
		      meltletrec_2_ptr =
			(struct meltletrec_2_st *)
			meltgc_allocate (sizeof (struct meltletrec_2_st), 0);
		      /*^blockmultialloc.initfill */
		      /*inimult rtup_0__TUPLREC__x3 */
 /*_.TUPLREC___V102*/ meltfptr[87] =
			(melt_ptr_t) & meltletrec_2_ptr->rtup_0__TUPLREC__x3;
		      meltletrec_2_ptr->rtup_0__TUPLREC__x3.discr =
			(meltobject_ptr_t) (((melt_ptr_t)
					     (MELT_PREDEF (DISCR_MULTIPLE))));
		      meltletrec_2_ptr->rtup_0__TUPLREC__x3.nbval = 2;


		      /*^putuple */
		      /*putupl#4 */
		      melt_assertmsg ("putupl [:4559] #4 checktup",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.TUPLREC___V102*/
							 meltfptr[87])) ==
				      MELTOBMAG_MULTIPLE);
		      melt_assertmsg ("putupl [:4559] #4 checkoff",
				      (0 >= 0
				       && 0 <
				       melt_multiple_length ((melt_ptr_t)
							     ( /*_.TUPLREC___V102*/ meltfptr[87]))));
		      ((meltmultiple_ptr_t)
		       ( /*_.TUPLREC___V102*/ meltfptr[87]))->tabval[0] =
   (melt_ptr_t) ( /*_.CURCOND__V37*/ meltfptr[36]);
		      ;
		      /*^putuple */
		      /*putupl#5 */
		      melt_assertmsg ("putupl [:4559] #5 checktup",
				      melt_magic_discr ((melt_ptr_t)
							( /*_.TUPLREC___V102*/
							 meltfptr[87])) ==
				      MELTOBMAG_MULTIPLE);
		      melt_assertmsg ("putupl [:4559] #5 checkoff",
				      (1 >= 0
				       && 1 <
				       melt_multiple_length ((melt_ptr_t)
							     ( /*_.TUPLREC___V102*/ meltfptr[87]))));
		      ((meltmultiple_ptr_t)
		       ( /*_.TUPLREC___V102*/ meltfptr[87]))->tabval[1] =
   (melt_ptr_t) ( /*_.RES__V29*/ meltfptr[25]);
		      ;
		      /*^touch */
		      meltgc_touch ( /*_.TUPLREC___V102*/ meltfptr[87]);
		      ;
		      /*_.TUPLE___V101*/ meltfptr[86] =
			/*_.TUPLREC___V102*/ meltfptr[87];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:4559:/ clear");
		  /*clear *//*_.TUPLREC___V102*/ meltfptr[87] = 0;
		      /*^clear */
		  /*clear *//*_.TUPLREC___V102*/ meltfptr[87] = 0;
		    }		/*end multiallocblock */
		    ;
		    MELT_LOCATION ("warmelt-macro.melt:4557:/ quasiblock");


		    /*^rawallocobj */
		    /*rawallocobj */
		    {
		      melt_ptr_t newobj = 0;
		      melt_raw_object_create (newobj,
					      (melt_ptr_t) (( /*!CLASS_SOURCE_OR */ meltfrout->tabval[9])), (3), "CLASS_SOURCE_OR");
	/*_.INST__V104*/ meltfptr[68] =
			newobj;
		    };
		    ;
		    /*^putslot */
		    /*putslot */
		    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
				    melt_magic_discr ((melt_ptr_t)
						      ( /*_.INST__V104*/
						       meltfptr[68])) ==
				    MELTOBMAG_OBJECT);
		    melt_putfield_object (( /*_.INST__V104*/ meltfptr[68]),
					  (1), ( /*_.LOC__V16*/ meltfptr[15]),
					  "LOCA_LOCATION");
		    ;
		    /*^putslot */
		    /*putslot */
		    melt_assertmsg ("putslot checkobj @SOR_DISJ",
				    melt_magic_discr ((melt_ptr_t)
						      ( /*_.INST__V104*/
						       meltfptr[68])) ==
				    MELTOBMAG_OBJECT);
		    melt_putfield_object (( /*_.INST__V104*/ meltfptr[68]),
					  (2),
					  ( /*_.TUPLE___V101*/ meltfptr[86]),
					  "SOR_DISJ");
		    ;
		    /*^touchobj */

		    melt_dbgtrace_written_object ( /*_.INST__V104*/
						  meltfptr[68],
						  "newly made instance");
		    ;
		    /*_.INST___V103*/ meltfptr[67] =
		      /*_.INST__V104*/ meltfptr[68];;
		    MELT_LOCATION ("warmelt-macro.melt:4557:/ compute");
		    /*_.RES__V29*/ meltfptr[25] =
		      /*_.SETQ___V105*/ meltfptr[72] =
		      /*_.INST___V103*/ meltfptr[67];;

#if MELT_HAVE_DEBUG
		    MELT_LOCATION ("warmelt-macro.melt:4560:/ cppif.then");
		    /*^block */
		    /*anyblock */
		    {


		      {
			/*^locexp */
			/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
			melt_dbgcounter++;
#endif
			;
		      }
		      ;
		      /*^checksignal */
		      MELT_CHECK_SIGNAL ();
		      ;
	 /*_#MELT_NEED_DBG__L30*/ meltfnum[17] =
			/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
			( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
			0	/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
			;;
		      MELT_LOCATION ("warmelt-macro.melt:4560:/ cond");
		      /*cond */ if ( /*_#MELT_NEED_DBG__L30*/ meltfnum[17])	/*then */
			{
			  /*^cond.then */
			  /*^block */
			  /*anyblock */
			  {

	   /*_#THE_MELTCALLCOUNT__L31*/ meltfnum[26] =
#ifdef meltcallcount
			      meltcallcount	/* the_meltcallcount */
#else
			      0L
#endif /* meltcallcount the_meltcallcount */
			      ;;
			    MELT_LOCATION
			      ("warmelt-macro.melt:4560:/ checksignal");
			    MELT_CHECK_SIGNAL ();
			    ;
			    /*^apply */
			    /*apply */
			    {
			      union meltparam_un argtab[5];
			      memset (&argtab, 0, sizeof (argtab));
			      /*^apply.arg */
			      argtab[0].meltbp_long =
				/*_#THE_MELTCALLCOUNT__L31*/ meltfnum[26];
			      /*^apply.arg */
			      argtab[1].meltbp_cstring = "warmelt-macro.melt";
			      /*^apply.arg */
			      argtab[2].meltbp_long = 4560;
			      /*^apply.arg */
			      argtab[3].meltbp_cstring =
				"mexpand_cond res for nonsexp cond";
			      /*^apply.arg */
			      argtab[4].meltbp_aptr =
				(melt_ptr_t *) & /*_.RES__V29*/ meltfptr[25];
			      /*_.MELT_DEBUG_FUN__V108*/ meltfptr[74] =
				melt_apply ((meltclosure_ptr_t)
					    (( /*!MELT_DEBUG_FUN */
					      meltfrout->tabval[2])),
					    (melt_ptr_t) (( /*nil */ NULL)),
					    (MELTBPARSTR_LONG
					     MELTBPARSTR_CSTRING
					     MELTBPARSTR_LONG
					     MELTBPARSTR_CSTRING
					     MELTBPARSTR_PTR ""), argtab, "",
					    (union meltparam_un *) 0);
			    }
			    ;
			    /*_.IF___V107*/ meltfptr[85] =
			      /*_.MELT_DEBUG_FUN__V108*/ meltfptr[74];;
			    /*epilog */

			    MELT_LOCATION ("warmelt-macro.melt:4560:/ clear");
		     /*clear *//*_#THE_MELTCALLCOUNT__L31*/
			      meltfnum[26] = 0;
			    /*^clear */
		     /*clear *//*_.MELT_DEBUG_FUN__V108*/ meltfptr[74]
			      = 0;
			  }
			  ;
			}
		      else
			{	/*^cond.else */

	  /*_.IF___V107*/ meltfptr[85] = NULL;;
			}
		      ;
		      MELT_LOCATION ("warmelt-macro.melt:4560:/ quasiblock");


		      /*_.PROGN___V109*/ meltfptr[66] =
			/*_.IF___V107*/ meltfptr[85];;
		      /*^compute */
		      /*_.IFCPP___V106*/ meltfptr[84] =
			/*_.PROGN___V109*/ meltfptr[66];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:4560:/ clear");
		   /*clear *//*_#MELT_NEED_DBG__L30*/ meltfnum[17] = 0;
		      /*^clear */
		   /*clear *//*_.IF___V107*/ meltfptr[85] = 0;
		      /*^clear */
		   /*clear *//*_.PROGN___V109*/ meltfptr[66] = 0;
		    }

#else /*MELT_HAVE_DEBUG */
		    /*^cppif.else */
		    /*_.IFCPP___V106*/ meltfptr[84] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
		    ;
		    MELT_LOCATION ("warmelt-macro.melt:4556:/ quasiblock");


		    /*_.PROGN___V110*/ meltfptr[43] =
		      /*_.IFCPP___V106*/ meltfptr[84];;
		    /*^compute */
		    /*_.IFELSE___V58*/ meltfptr[51] =
		      /*_.PROGN___V110*/ meltfptr[43];;
		    /*epilog */

		    MELT_LOCATION ("warmelt-macro.melt:4520:/ clear");
		 /*clear *//*_.TUPLE___V101*/ meltfptr[86] = 0;
		    /*^clear */
		 /*clear *//*_.INST___V103*/ meltfptr[67] = 0;
		    /*^clear */
		 /*clear *//*_.SETQ___V105*/ meltfptr[72] = 0;
		    /*^clear */
		 /*clear *//*_.IFCPP___V106*/ meltfptr[84] = 0;
		    /*^clear */
		 /*clear *//*_.PROGN___V110*/ meltfptr[43] = 0;
		  }
		  ;
		}
	      ;
	      /*_.IFELSE___V45*/ meltfptr[38] =
		/*_.IFELSE___V58*/ meltfptr[51];;
	      /*epilog */

	      MELT_LOCATION ("warmelt-macro.melt:4506:/ clear");
	       /*clear *//*_#IS_A__L21*/ meltfnum[16] = 0;
	      /*^clear */
	       /*clear *//*_.IFELSE___V58*/ meltfptr[51] = 0;
	    }
	    ;
	  }
	;
	/*_.LET___V36*/ meltfptr[35] = /*_.IFELSE___V45*/ meltfptr[38];;

	MELT_LOCATION ("warmelt-macro.melt:4503:/ clear");
	     /*clear *//*_.CURCOND__V37*/ meltfptr[36] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V38*/ meltfptr[37] = 0;
	/*^clear */
	     /*clear *//*_#NULL__L14*/ meltfnum[12] = 0;
	/*^clear */
	     /*clear *//*_#IF___L15*/ meltfnum[0] = 0;
	/*^clear */
	     /*clear *//*_.IFELSE___V45*/ meltfptr[38] = 0;
   /*_#I__L32*/ meltfnum[23] =
	  (( /*_#IX__L8*/ meltfnum[6]) - (1));;
	MELT_LOCATION ("warmelt-macro.melt:4564:/ compute");
	/*_#IX__L8*/ meltfnum[6] = /*_#SETQ___L33*/ meltfnum[15] =
	  /*_#I__L32*/ meltfnum[23];;
	MELT_LOCATION ("warmelt-macro.melt:4501:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*epilog */

	/*^clear */
	     /*clear *//*_#I__L11*/ meltfnum[9] = 0;
	/*^clear */
	     /*clear *//*_.LET___V36*/ meltfptr[35] = 0;
	/*^clear */
	     /*clear *//*_#I__L32*/ meltfnum[23] = 0;
	/*^clear */
	     /*clear *//*_#SETQ___L33*/ meltfnum[15] = 0;
      }
      ;
      ;
      goto labloop_CONDLOOP_1;
    labexit_CONDLOOP_1:;	/*^loopepilog */
      /*loopepilog */
      /*_.FOREVER___V34*/ meltfptr[23] = /*_.CONDLOOP__V35*/ meltfptr[31];;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4566:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L34*/ meltfnum[26] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4566:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L34*/ meltfnum[26])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L35*/ meltfnum[17] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4566:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L35*/ meltfnum[17];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4566;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_cond final res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V29*/ meltfptr[25];
	      /*_.MELT_DEBUG_FUN__V113*/ meltfptr[49] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V112*/ meltfptr[48] =
	      /*_.MELT_DEBUG_FUN__V113*/ meltfptr[49];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4566:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L35*/ meltfnum[17] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V113*/ meltfptr[49] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V112*/ meltfptr[48] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4566:/ quasiblock");


      /*_.PROGN___V114*/ meltfptr[50] = /*_.IF___V112*/ meltfptr[48];;
      /*^compute */
      /*_.IFCPP___V111*/ meltfptr[47] = /*_.PROGN___V114*/ meltfptr[50];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4566:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L34*/ meltfnum[26] = 0;
      /*^clear */
	     /*clear *//*_.IF___V112*/ meltfptr[48] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V114*/ meltfptr[50] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V111*/ meltfptr[47] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4567:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V29*/ meltfptr[25];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4567:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V14*/ meltfptr[10] = /*_.RETURN___V115*/ meltfptr[39];;

    MELT_LOCATION ("warmelt-macro.melt:4482:/ clear");
	   /*clear *//*_.CONT__V15*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.CEXPTUPLE__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_#NBCOND__L5*/ meltfnum[3] = 0;
    /*^clear */
	   /*clear *//*_.LET___V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LASTCEXP__V28*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#IX__L8*/ meltfnum[6] = 0;
    /*^clear */
	   /*clear *//*_.RES__V29*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V30*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V34*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V111*/ meltfptr[47] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V115*/ meltfptr[39] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4478:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V14*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4478:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_COND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_77_warmelt_macro_MEXPAND_COND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_77_warmelt_macro_MEXPAND_COND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_78_warmelt_macro_LAMBDA___19__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_78_warmelt_macro_LAMBDA___19___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_78_warmelt_macro_LAMBDA___19___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 2
    melt_ptr_t mcfr_varptr[2];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_78_warmelt_macro_LAMBDA___19__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_78_warmelt_macro_LAMBDA___19___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 2; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_78_warmelt_macro_LAMBDA___19__ nbval 2*/
  meltfram__.mcfr_nbvar = 2 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4488:/ getarg");
 /*_.C__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-macro.melt:4489:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L1*/ meltfnum[0] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.C__V2*/ meltfptr[1]),
			    (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					   tabval[0])));;
    MELT_LOCATION ("warmelt-macro.melt:4489:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L1*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4490:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t)
			      (( /*~LOC */ meltfclos->tabval[0])),
			      ("COND with non-sexpr"), (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4488:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.C__V2*/ meltfptr[1];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4488:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_#IS_NOT_A__L1*/ meltfnum[0] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_78_warmelt_macro_LAMBDA___19___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_78_warmelt_macro_LAMBDA___19__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_79_warmelt_macro_MEXPAND_AND (meltclosure_ptr_t meltclosp_,
				       melt_ptr_t meltfirstargp_,
				       const melt_argdescr_cell_t
				       meltxargdescr_[],
				       union meltparam_un * meltxargtab_,
				       const melt_argdescr_cell_t
				       meltxresdescr_[],
				       union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_79_warmelt_macro_MEXPAND_AND_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_79_warmelt_macro_MEXPAND_AND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 46
    melt_ptr_t mcfr_varptr[46];
#define MELTFRAM_NBVARNUM 17
    long mcfr_varnum[17];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_79_warmelt_macro_MEXPAND_AND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_79_warmelt_macro_MEXPAND_AND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 46; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_79_warmelt_macro_MEXPAND_AND nbval 46*/
  meltfram__.mcfr_nbvar = 46 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_AND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4583:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4584:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4584:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4584:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4584;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_and sexpr:";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4584:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4584:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4584:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4585:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4585:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4585:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4585) ? (4585) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4585:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4586:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4586:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4586:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4586) ? (4586) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4586:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4587:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L5*/ meltfnum[1] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:4587:/ cond");
    /*cond */ if ( /*_#NULL__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V15*/ meltfptr[14] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[3]);;
	  /*_.IF___V14*/ meltfptr[12] = /*_.SETQ___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4587:/ clear");
	     /*clear *//*_.SETQ___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4588:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:4588:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4588:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4588) ? (4588) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4588:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4589:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4590:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4591:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.CURPAIR__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    MELT_LOCATION ("warmelt-macro.melt:4596:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V24*/ meltfptr[23] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_6 */ meltfrout->
						tabval[6])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[1] =
      (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V23*/ meltfptr[22] = /*_.LAMBDA___V24*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-macro.melt:4593:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[5]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V23*/ meltfptr[22];
      /*_.CXTUP__V25*/ meltfptr[24] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.CURPAIR__V22*/ meltfptr[21]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#NBCOMP__L7*/ meltfnum[0] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.CXTUP__V25*/ meltfptr[24])));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4601:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4601:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4601:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4601;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_and cxtup";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CXTUP__V25*/ meltfptr[24];
	      /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V27*/ meltfptr[26] =
	      /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4601:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V27*/ meltfptr[26] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4601:/ quasiblock");


      /*_.PROGN___V29*/ meltfptr[27] = /*_.IF___V27*/ meltfptr[26];;
      /*^compute */
      /*_.IFCPP___V26*/ meltfptr[25] = /*_.PROGN___V29*/ meltfptr[27];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4601:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V27*/ meltfptr[26] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[27] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V26*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4602:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L10*/ meltfnum[8] =
      (( /*_#NBCOMP__L7*/ meltfnum[0]) < (1));;
    MELT_LOCATION ("warmelt-macro.melt:4602:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4604:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("AND without sons"), (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4605:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:4605:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:4603:/ quasiblock");


	  /*_.PROGN___V32*/ meltfptr[31] = /*_.RETURN___V31*/ meltfptr[27];;
	  /*^compute */
	  /*_.IFELSE___V30*/ meltfptr[26] = /*_.PROGN___V32*/ meltfptr[31];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4602:/ clear");
	     /*clear *//*_.RETURN___V31*/ meltfptr[27] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V32*/ meltfptr[31] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:4606:/ quasiblock");


   /*_#I__L11*/ meltfnum[7] =
	    (( /*_#NBCOMP__L7*/ meltfnum[0]) - (1));;
	  /*^compute */
   /*_.RES__V34*/ meltfptr[31] =
	    (melt_multiple_nth
	     ((melt_ptr_t) ( /*_.CXTUP__V25*/ meltfptr[24]),
	      ( /*_#I__L11*/ meltfnum[7])));;
	  /*^compute */
   /*_#IX__L12*/ meltfnum[11] =
	    (( /*_#NBCOMP__L7*/ meltfnum[0]) - (2));;
	  MELT_LOCATION ("warmelt-macro.melt:4608:/ loop");
	  /*loop */
	  {
	  labloop_REVLOOP_1:;	/*^loopbody */

	    /*^block */
	    /*anyblock */
	    {

	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      MELT_LOCATION ("warmelt-macro.melt:4609:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_#I__L13*/ meltfnum[12] =
		(( /*_#IX__L12*/ meltfnum[11]) < (0));;
	      MELT_LOCATION ("warmelt-macro.melt:4609:/ cond");
	      /*cond */ if ( /*_#I__L13*/ meltfnum[12])	/*then */
		{
		  /*^cond.then */
		  /*^block */
		  /*anyblock */
		  {

		    /*^quasiblock */


		    /*^compute */
       /*_.REVLOOP__V36*/ meltfptr[35] = NULL;;

		    /*^exit */
		    /*exit */
		    {
		      goto labexit_REVLOOP_1;
		    }
		    ;
		    /*epilog */
		  }
		  ;
		}		/*noelse */
	      ;
	      MELT_LOCATION ("warmelt-macro.melt:4610:/ quasiblock");


     /*_.CURC__V38*/ meltfptr[37] =
		(melt_multiple_nth
		 ((melt_ptr_t) ( /*_.CXTUP__V25*/ meltfptr[24]),
		  ( /*_#IX__L12*/ meltfnum[11])));;
	      MELT_LOCATION ("warmelt-macro.melt:4612:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^quasiblock */


	      /*^rawallocobj */
	      /*rawallocobj */
	      {
		melt_ptr_t newobj = 0;
		melt_raw_object_create (newobj,
					(melt_ptr_t) (( /*!CLASS_SOURCE_IF */
						       meltfrout->tabval[7])),
					(4), "CLASS_SOURCE_IF");
      /*_.INST__V40*/ meltfptr[39] =
		  newobj;
	      };
	      ;
	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
			      melt_magic_discr ((melt_ptr_t)
						( /*_.INST__V40*/
						 meltfptr[39])) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (1),
				    ( /*_.LOC__V20*/ meltfptr[19]),
				    "LOCA_LOCATION");
	      ;
	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @SIF_TEST",
			      melt_magic_discr ((melt_ptr_t)
						( /*_.INST__V40*/
						 meltfptr[39])) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (2),
				    ( /*_.CURC__V38*/ meltfptr[37]),
				    "SIF_TEST");
	      ;
	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @SIF_THEN",
			      melt_magic_discr ((melt_ptr_t)
						( /*_.INST__V40*/
						 meltfptr[39])) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object (( /*_.INST__V40*/ meltfptr[39]), (3),
				    ( /*_.RES__V34*/ meltfptr[31]),
				    "SIF_THEN");
	      ;
	      /*^touchobj */

	      melt_dbgtrace_written_object ( /*_.INST__V40*/ meltfptr[39],
					    "newly made instance");
	      ;
	      /*_.INST___V39*/ meltfptr[38] = /*_.INST__V40*/ meltfptr[39];;
	      MELT_LOCATION ("warmelt-macro.melt:4611:/ compute");
	      /*_.RES__V34*/ meltfptr[31] = /*_.SETQ___V41*/ meltfptr[40] =
		/*_.INST___V39*/ meltfptr[38];;
	      /*_.LET___V37*/ meltfptr[36] = /*_.SETQ___V41*/ meltfptr[40];;

	      MELT_LOCATION ("warmelt-macro.melt:4610:/ clear");
	       /*clear *//*_.CURC__V38*/ meltfptr[37] = 0;
	      /*^clear */
	       /*clear *//*_.INST___V39*/ meltfptr[38] = 0;
	      /*^clear */
	       /*clear *//*_.SETQ___V41*/ meltfptr[40] = 0;
     /*_#I__L14*/ meltfnum[13] =
		(( /*_#IX__L12*/ meltfnum[11]) - (1));;
	      MELT_LOCATION ("warmelt-macro.melt:4617:/ compute");
	      /*_#IX__L12*/ meltfnum[11] = /*_#SETQ___L15*/ meltfnum[14] =
		/*_#I__L14*/ meltfnum[13];;
	      MELT_LOCATION ("warmelt-macro.melt:4608:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*epilog */

	      /*^clear */
	       /*clear *//*_#I__L13*/ meltfnum[12] = 0;
	      /*^clear */
	       /*clear *//*_.LET___V37*/ meltfptr[36] = 0;
	      /*^clear */
	       /*clear *//*_#I__L14*/ meltfnum[13] = 0;
	      /*^clear */
	       /*clear *//*_#SETQ___L15*/ meltfnum[14] = 0;
	    }
	    ;
	    ;
	    goto labloop_REVLOOP_1;
	  labexit_REVLOOP_1:;	/*^loopepilog */
	    /*loopepilog */
	    /*_.FOREVER___V35*/ meltfptr[34] =
	      /*_.REVLOOP__V36*/ meltfptr[35];;
	  }
	  ;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:4619:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L16*/ meltfnum[12] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4619:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L16*/ meltfnum[12])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[13] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:4619:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L17*/ meltfnum[13];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-macro.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4619;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "mexpand_and res:";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.RES__V34*/ meltfptr[31];
		    /*_.MELT_DEBUG_FUN__V44*/ meltfptr[40] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V43*/ meltfptr[38] =
		    /*_.MELT_DEBUG_FUN__V44*/ meltfptr[40];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:4619:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L17*/ meltfnum[13] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V44*/ meltfptr[40] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V43*/ meltfptr[38] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-macro.melt:4619:/ quasiblock");


	    /*_.PROGN___V45*/ meltfptr[36] = /*_.IF___V43*/ meltfptr[38];;
	    /*^compute */
	    /*_.IFCPP___V42*/ meltfptr[37] = /*_.PROGN___V45*/ meltfptr[36];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4619:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L16*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V43*/ meltfptr[38] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V45*/ meltfptr[36] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V42*/ meltfptr[37] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4620:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V34*/ meltfptr[31];;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:4620:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.LET___V33*/ meltfptr[27] = /*_.RETURN___V46*/ meltfptr[40];;

	  MELT_LOCATION ("warmelt-macro.melt:4606:/ clear");
	     /*clear *//*_#I__L11*/ meltfnum[7] = 0;
	  /*^clear */
	     /*clear *//*_.RES__V34*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_#IX__L12*/ meltfnum[11] = 0;
	  /*^clear */
	     /*clear *//*_.FOREVER___V35*/ meltfptr[34] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V42*/ meltfptr[37] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V46*/ meltfptr[40] = 0;
	  /*_.IFELSE___V30*/ meltfptr[26] = /*_.LET___V33*/ meltfptr[27];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4602:/ clear");
	     /*clear *//*_.LET___V33*/ meltfptr[27] = 0;
	}
	;
      }
    ;
    /*_.LET___V18*/ meltfptr[16] = /*_.IFELSE___V30*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-macro.melt:4589:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.CXTUP__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#NBCOMP__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V30*/ meltfptr[26] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4583:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4583:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_AND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_79_warmelt_macro_MEXPAND_AND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_79_warmelt_macro_MEXPAND_AND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_80_warmelt_macro_LAMBDA___20__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_80_warmelt_macro_LAMBDA___20___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_80_warmelt_macro_LAMBDA___20___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_80_warmelt_macro_LAMBDA___20__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_80_warmelt_macro_LAMBDA___20___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_80_warmelt_macro_LAMBDA___20__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4596:/ getarg");
 /*_.C__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-macro.melt:4597:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~ENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.C__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4596:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4596:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_80_warmelt_macro_LAMBDA___20___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_80_warmelt_macro_LAMBDA___20__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_81_warmelt_macro_PATEXPAND_AS (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_81_warmelt_macro_PATEXPAND_AS_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_81_warmelt_macro_PATEXPAND_AS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 44
    melt_ptr_t mcfr_varptr[44];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_81_warmelt_macro_PATEXPAND_AS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_81_warmelt_macro_PATEXPAND_AS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 44; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_81_warmelt_macro_PATEXPAND_AS nbval 44*/
  meltfram__.mcfr_nbvar = 44 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PATEXPAND_AS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4624:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4625:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4625:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4625:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4625) ? (4625) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4625:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4626:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4626:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4626:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4626) ? (4626) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4626:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4627:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_PATTERN_EXPANSION_CONTEXT */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4627:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4627:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4627) ? (4627) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4627:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4628:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4628:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4628:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4628;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_as sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V12*/ meltfptr[11] =
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4628:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V12*/ meltfptr[11] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4628:/ quasiblock");


      /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.PROGN___V14*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4628:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4629:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4630:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V16*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4631:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.LIST_FIRST__V18*/ meltfptr[17] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V16*/ meltfptr[12])));;
    /*^compute */
 /*_.CURPAIR__V19*/ meltfptr[18] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:4633:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.PCTX__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.LOC__V17*/ meltfptr[16];
      /*_.ARGSP__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PATTERNEXPAND_PAIRLIST_AS_TUPLE */ meltfrout->
		      tabval[4])),
		    (melt_ptr_t) ( /*_.CURPAIR__V19*/ meltfptr[18]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4635:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MULTIPLE_LENGTH__L6*/ meltfnum[4] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.ARGSP__V20*/ meltfptr[19])));;
    /*^compute */
 /*_#I__L7*/ meltfnum[0] =
      (( /*_#MULTIPLE_LENGTH__L6*/ meltfnum[4]) != (2));;
    MELT_LOCATION ("warmelt-macro.melt:4635:/ cond");
    /*cond */ if ( /*_#I__L7*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4637:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V17*/ meltfptr[16]),
			      ("AS pattern expects two arguments: ?patvar subpattern"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4638:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:4638:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:4636:/ quasiblock");


	  /*_.PROGN___V23*/ meltfptr[22] = /*_.RETURN___V22*/ meltfptr[21];;
	  /*^compute */
	  /*_.IF___V21*/ meltfptr[20] = /*_.PROGN___V23*/ meltfptr[22];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4635:/ clear");
	     /*clear *//*_.RETURN___V22*/ meltfptr[21] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[22] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V21*/ meltfptr[20] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4639:/ quasiblock");


 /*_.ARGVAR__V25*/ meltfptr[22] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.ARGSP__V20*/ meltfptr[19]), (0)));;
    /*^compute */
 /*_.ARGSUB__V26*/ meltfptr[25] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.ARGSP__V20*/ meltfptr[19]), (1)));;
    MELT_LOCATION ("warmelt-macro.melt:4642:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_A__L8*/ meltfnum[7] =
      melt_is_instance_of ((melt_ptr_t) ( /*_.ARGVAR__V25*/ meltfptr[22]),
			   (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_JOKER_VARIABLE */ meltfrout->tabval[5])));;
    MELT_LOCATION ("warmelt-macro.melt:4642:/ cond");
    /*cond */ if ( /*_#IS_A__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4644:/ locexp");
	    melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V17*/ meltfptr[16]),
			      ("AS pattern with useless joker"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4645:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.ARGSUB__V26*/ meltfptr[25];;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:4645:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:4643:/ quasiblock");


	  /*_.PROGN___V29*/ meltfptr[28] = /*_.RETURN___V28*/ meltfptr[27];;
	  /*^compute */
	  /*_.IF___V27*/ meltfptr[26] = /*_.PROGN___V29*/ meltfptr[28];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4642:/ clear");
	     /*clear *//*_.RETURN___V28*/ meltfptr[27] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[28] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V27*/ meltfptr[26] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4646:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#IS_NOT_A__L9*/ meltfnum[8] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.ARGVAR__V25*/ meltfptr[22]),
			    (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_VARIABLE */ meltfrout->tabval[6])));;
    MELT_LOCATION ("warmelt-macro.melt:4646:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L9*/ meltfnum[8])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4648:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V17*/ meltfptr[16]),
			      ("AS pattern needs a pattern variable as first argument"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4649:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:4649:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:4647:/ quasiblock");


	  /*_.PROGN___V32*/ meltfptr[31] = /*_.RETURN___V31*/ meltfptr[28];;
	  /*^compute */
	  /*_.IF___V30*/ meltfptr[27] = /*_.PROGN___V32*/ meltfptr[31];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4646:/ clear");
	     /*clear *//*_.RETURN___V31*/ meltfptr[28] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V32*/ meltfptr[31] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V30*/ meltfptr[27] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-macro.melt:4650:/ locexp");
      melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V17*/ meltfptr[16]),
			("deprecated AS pattern - use AND pattern instead"),
			(melt_ptr_t) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4651:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4652:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.ARGSUB__V26*/ meltfptr[25]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN */ meltfrout->tabval[7])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.ARGSUB__V26*/ meltfptr[25]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "PAT_WEIGHT");
   /*_.PAT_WEIGHT__V34*/ meltfptr[31] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.PAT_WEIGHT__V34*/ meltfptr[31] = NULL;;
      }
    ;
    /*^compute */
 /*_#SUBW__L10*/ meltfnum[9] =
      (melt_get_int ((melt_ptr_t) ( /*_.PAT_WEIGHT__V34*/ meltfptr[31])));;
    MELT_LOCATION ("warmelt-macro.melt:4653:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L11*/ meltfnum[10] =
      ((2) + ( /*_#SUBW__L10*/ meltfnum[9]));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V35*/ meltfptr[34] =
      (meltgc_new_int
       ((meltobject_ptr_t)
	(( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[9])),
	( /*_#I__L11*/ meltfnum[10])));;
    MELT_LOCATION ("warmelt-macro.melt:4656:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (2) rtup_0__TUPLREC__x4;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x4 */
 /*_.TUPLREC___V37*/ meltfptr[36] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x4;
      meltletrec_1_ptr->rtup_0__TUPLREC__x4.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x4.nbval = 2;


      /*^putuple */
      /*putupl#6 */
      melt_assertmsg ("putupl [:4656] #6 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V37*/ meltfptr[36]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:4656] #6 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V37*/
					      meltfptr[36]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V37*/ meltfptr[36]))->tabval[0] =
	(melt_ptr_t) ( /*_.ARGVAR__V25*/ meltfptr[22]);
      ;
      /*^putuple */
      /*putupl#7 */
      melt_assertmsg ("putupl [:4656] #7 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V37*/ meltfptr[36]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:4656] #7 checkoff",
		      (1 >= 0
		       && 1 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V37*/
					      meltfptr[36]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V37*/ meltfptr[36]))->tabval[1] =
	(melt_ptr_t) ( /*_.ARGSUB__V26*/ meltfptr[25]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V37*/ meltfptr[36]);
      ;
      /*_.TUPLE___V36*/ meltfptr[35] = /*_.TUPLREC___V37*/ meltfptr[36];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4656:/ clear");
	    /*clear *//*_.TUPLREC___V37*/ meltfptr[36] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V37*/ meltfptr[36] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4653:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_AND */
					     meltfrout->tabval[8])), (4),
			      "CLASS_SOURCE_PATTERN_AND");
  /*_.INST__V39*/ meltfptr[38] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V39*/ meltfptr[38])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V39*/ meltfptr[38]), (1),
			  ( /*_.LOC__V17*/ meltfptr[16]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @PAT_WEIGHT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V39*/ meltfptr[38])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V39*/ meltfptr[38]), (2),
			  ( /*_.MAKE_INTEGERBOX__V35*/ meltfptr[34]),
			  "PAT_WEIGHT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @ANDPAT_CONJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V39*/ meltfptr[38])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V39*/ meltfptr[38]), (3),
			  ( /*_.TUPLE___V36*/ meltfptr[35]), "ANDPAT_CONJ");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V39*/ meltfptr[38],
				  "newly made instance");
    ;
    /*_.RES__V38*/ meltfptr[36] = /*_.INST__V39*/ meltfptr[38];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4658:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4658:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4658:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4658;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_as returns res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V38*/ meltfptr[36];
	      /*_.MELT_DEBUG_FUN__V42*/ meltfptr[41] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V41*/ meltfptr[40] =
	      /*_.MELT_DEBUG_FUN__V42*/ meltfptr[41];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4658:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V42*/ meltfptr[41] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V41*/ meltfptr[40] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4658:/ quasiblock");


      /*_.PROGN___V43*/ meltfptr[41] = /*_.IF___V41*/ meltfptr[40];;
      /*^compute */
      /*_.IFCPP___V40*/ meltfptr[39] = /*_.PROGN___V43*/ meltfptr[41];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4658:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V41*/ meltfptr[40] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V43*/ meltfptr[41] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V40*/ meltfptr[39] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4659:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V38*/ meltfptr[36];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4659:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V33*/ meltfptr[28] = /*_.RETURN___V44*/ meltfptr[40];;

    MELT_LOCATION ("warmelt-macro.melt:4651:/ clear");
	   /*clear *//*_.PAT_WEIGHT__V34*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_#SUBW__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_#I__L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V35*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V36*/ meltfptr[35] = 0;
    /*^clear */
	   /*clear *//*_.RES__V38*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V40*/ meltfptr[39] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V44*/ meltfptr[40] = 0;
    /*_.LET___V24*/ meltfptr[21] = /*_.LET___V33*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-macro.melt:4639:/ clear");
	   /*clear *//*_.ARGVAR__V25*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.ARGSUB__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_#IS_A__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IF___V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.IF___V30*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.LET___V33*/ meltfptr[28] = 0;
    /*_.LET___V15*/ meltfptr[11] = /*_.LET___V24*/ meltfptr[21];;

    MELT_LOCATION ("warmelt-macro.melt:4629:/ clear");
	   /*clear *//*_.CONT__V16*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ARGSP__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L6*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.LET___V24*/ meltfptr[21] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4624:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V15*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4624:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PATEXPAND_AS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_81_warmelt_macro_PATEXPAND_AS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_81_warmelt_macro_PATEXPAND_AS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_82_warmelt_macro_MEXPAND_AS (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un * meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_82_warmelt_macro_MEXPAND_AS_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_82_warmelt_macro_MEXPAND_AS_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_82_warmelt_macro_MEXPAND_AS is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_82_warmelt_macro_MEXPAND_AS_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_82_warmelt_macro_MEXPAND_AS nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_AS", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4662:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4663:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4663:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4663:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4663) ? (4663) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4663:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4664:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4664:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4664:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4664) ? (4664) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4664:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4665:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4665:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4665:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4665;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_as sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4665:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4665:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4665:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4666:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4667:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V15*/ meltfptr[11] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-macro.melt:4669:/ locexp");
      /* error_plain */
	melt_error_str ((melt_ptr_t) ( /*_.LOC__V15*/ meltfptr[11]),
			("AS cannot be macro-expanded in expression context"),
			(melt_ptr_t) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4670:/ quasiblock");


 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

    {
      MELT_LOCATION ("warmelt-macro.melt:4670:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V14*/ meltfptr[10] = /*_.RETURN___V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-macro.melt:4666:/ clear");
	   /*clear *//*_.LOC__V15*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V16*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4662:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V14*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4662:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_AS", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_82_warmelt_macro_MEXPAND_AS_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_82_warmelt_macro_MEXPAND_AS */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_83_warmelt_macro_PATEXPAND_WHEN (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_83_warmelt_macro_PATEXPAND_WHEN_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_83_warmelt_macro_PATEXPAND_WHEN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 20
    melt_ptr_t mcfr_varptr[20];
#define MELTFRAM_NBVARNUM 5
    long mcfr_varnum[5];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_83_warmelt_macro_PATEXPAND_WHEN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_83_warmelt_macro_PATEXPAND_WHEN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 20; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_83_warmelt_macro_PATEXPAND_WHEN nbval 20*/
  meltfram__.mcfr_nbvar = 20 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PATEXPAND_WHEN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4676:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4677:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4677:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4677:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4677) ? (4677) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4677:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4678:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4678:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4678:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4678) ? (4678) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4678:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4679:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_PATTERN_EXPANSION_CONTEXT */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4679:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4679:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4679) ? (4679) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4679:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4680:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4680:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4680:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4680;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_when sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V12*/ meltfptr[11] =
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4680:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V12*/ meltfptr[11] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4680:/ quasiblock");


      /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.PROGN___V14*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4680:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4681:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4682:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V16*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4683:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V17*/ meltfptr[16] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-macro.melt:4686:/ locexp");
      /* error_plain */
	melt_error_str ((melt_ptr_t) ( /*_.LOC__V17*/ meltfptr[16]),
			("WHEN is not yet implemented in patterns"),
			(melt_ptr_t) 0);
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4687:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      /*^cond */
      /*cond */ if (( /*nil */ NULL))	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V19*/ meltfptr[18] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4687:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("@$@ unimplemented patexpand_when"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4687) ? (4687) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[17] = /*_.IFELSE___V19*/ meltfptr[18];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4687:/ clear");
	     /*clear *//*_.IFELSE___V19*/ meltfptr[18] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[17] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4688:/ quasiblock");


 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

    {
      MELT_LOCATION ("warmelt-macro.melt:4688:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V15*/ meltfptr[11] = /*_.RETURN___V20*/ meltfptr[18];;

    MELT_LOCATION ("warmelt-macro.melt:4681:/ clear");
	   /*clear *//*_.CONT__V16*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V20*/ meltfptr[18] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4676:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V15*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4676:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PATEXPAND_WHEN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_83_warmelt_macro_PATEXPAND_WHEN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_83_warmelt_macro_PATEXPAND_WHEN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_84_warmelt_macro_MEXPAND_WHEN (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_84_warmelt_macro_MEXPAND_WHEN_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_84_warmelt_macro_MEXPAND_WHEN_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 16
    melt_ptr_t mcfr_varptr[16];
#define MELTFRAM_NBVARNUM 4
    long mcfr_varnum[4];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_84_warmelt_macro_MEXPAND_WHEN is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_84_warmelt_macro_MEXPAND_WHEN_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 16; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_84_warmelt_macro_MEXPAND_WHEN nbval 16*/
  meltfram__.mcfr_nbvar = 16 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_WHEN", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4691:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4692:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4692:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4692:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4692) ? (4692) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4692:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4693:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4693:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4693:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4693) ? (4693) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4693:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4694:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L3*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4694:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4694:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4694;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_when sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[2])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V11*/ meltfptr[10] =
	      /*_.MELT_DEBUG_FUN__V12*/ meltfptr[11];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4694:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L4*/ meltfnum[3] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V12*/ meltfptr[11] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V11*/ meltfptr[10] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4694:/ quasiblock");


      /*_.PROGN___V13*/ meltfptr[11] = /*_.IF___V11*/ meltfptr[10];;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.PROGN___V13*/ meltfptr[11];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4694:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V11*/ meltfptr[10] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V13*/ meltfptr[11] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4695:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4696:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V15*/ meltfptr[11] = slot;
    };
    ;

    {
      MELT_LOCATION ("warmelt-macro.melt:4698:/ locexp");
      /* error_plain */
	melt_error_str ((melt_ptr_t) ( /*_.LOC__V15*/ meltfptr[11]),
			("WHEN cannot be macro-expanded in expression context"),
			(melt_ptr_t) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4699:/ quasiblock");


 /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

    {
      MELT_LOCATION ("warmelt-macro.melt:4699:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V14*/ meltfptr[10] = /*_.RETURN___V16*/ meltfptr[15];;

    MELT_LOCATION ("warmelt-macro.melt:4695:/ clear");
	   /*clear *//*_.LOC__V15*/ meltfptr[11] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V16*/ meltfptr[15] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4691:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V14*/ meltfptr[10];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4691:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[10] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_WHEN", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_84_warmelt_macro_MEXPAND_WHEN_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_84_warmelt_macro_MEXPAND_WHEN */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_85_warmelt_macro_PATEXPAND_AND (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_85_warmelt_macro_PATEXPAND_AND_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_85_warmelt_macro_PATEXPAND_AND_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 29
    melt_ptr_t mcfr_varptr[29];
#define MELTFRAM_NBVARNUM 12
    long mcfr_varnum[12];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_85_warmelt_macro_PATEXPAND_AND is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_85_warmelt_macro_PATEXPAND_AND_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 29; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_85_warmelt_macro_PATEXPAND_AND nbval 29*/
  meltfram__.mcfr_nbvar = 29 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PATEXPAND_AND", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4706:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4707:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4707:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4707:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4707) ? (4707) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4707:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4708:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4708:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4708:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4708) ? (4708) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4708:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4709:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_PATTERN_EXPANSION_CONTEXT */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4709:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4709:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4709) ? (4709) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4709:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4710:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4710:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4710:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4710;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_and sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V12*/ meltfptr[11] =
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4710:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V12*/ meltfptr[11] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4710:/ quasiblock");


      /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.PROGN___V14*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4710:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4711:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4712:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V16*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4713:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.LIST_FIRST__V18*/ meltfptr[17] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V16*/ meltfptr[12])));;
    /*^compute */
 /*_.CURPAIR__V19*/ meltfptr[18] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:4715:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.PCTX__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.LOC__V17*/ meltfptr[16];
      /*_.ARGSP__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PATTERNEXPAND_PAIRLIST_AS_TUPLE */ meltfrout->
		      tabval[4])),
		    (melt_ptr_t) ( /*_.CURPAIR__V19*/ meltfptr[18]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4716:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_AND */
					     meltfrout->tabval[5])), (4),
			      "CLASS_SOURCE_PATTERN_AND");
  /*_.INST__V22*/ meltfptr[21] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (1),
			  ( /*_.LOC__V17*/ meltfptr[16]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @ANDPAT_CONJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (3),
			  ( /*_.ARGSP__V20*/ meltfptr[19]), "ANDPAT_CONJ");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V22*/ meltfptr[21],
				  "newly made instance");
    ;
    /*_.RES__V21*/ meltfptr[20] = /*_.INST__V22*/ meltfptr[21];;
    MELT_LOCATION ("warmelt-macro.melt:4722:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:4720:/ quasiblock");


    /*^multiapply */
    /*multiapply 1args, 3x.res */
    {

      union meltparam_un restab[3];
      memset (&restab, 0, sizeof (restab));
      /*^multiapply.xres */
      restab[0].meltbp_longptr = & /*_#IMAX__L6*/ meltfnum[4];
      /*^multiapply.xres */
      restab[1].meltbp_longptr = & /*_#IMIN__L7*/ meltfnum[0];
      /*^multiapply.xres */
      restab[2].meltbp_longptr = & /*_#ISUM__L8*/ meltfnum[7];
      /*^multiapply.appl */
      /*_.SUBPATW__V23*/ meltfptr[22] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PATTERN_WEIGHT_TUPLE */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.ARGSP__V20*/ meltfptr[19]), (""),
		    (union meltparam_un *) 0,
		    (MELTBPARSTR_LONG MELTBPARSTR_LONG MELTBPARSTR_LONG ""),
		    restab);
    }
    ;
    /*^quasiblock */


    MELT_LOCATION ("warmelt-macro.melt:4723:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L9*/ meltfnum[8] =
      ((1) + ( /*_#IMAX__L6*/ meltfnum[4]));;
    /*^compute */
 /*_#I__L10*/ meltfnum[9] =
      (( /*_#I__L9*/ meltfnum[8]) + ( /*_#ISUM__L8*/ meltfnum[7]));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V24*/ meltfptr[23] =
      (meltgc_new_int
       ((meltobject_ptr_t)
	(( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[7])),
	( /*_#I__L10*/ meltfnum[9])));;
    MELT_LOCATION ("warmelt-macro.melt:4723:/ quasiblock");


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @PAT_WEIGHT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.RES__V21*/ meltfptr[20])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.RES__V21*/ meltfptr[20]), (2),
			  ( /*_.MAKE_INTEGERBOX__V24*/ meltfptr[23]),
			  "PAT_WEIGHT");
    ;
    /*^touch */
    meltgc_touch ( /*_.RES__V21*/ meltfptr[20]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.RES__V21*/ meltfptr[20], "put-fields");
    ;


    MELT_LOCATION ("warmelt-macro.melt:4720:/ clear");
	   /*clear *//*_#I__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[9] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V24*/ meltfptr[23] = 0;

    /*^clear */
	   /*clear *//*_#IMAX__L6*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#IMIN__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#ISUM__L8*/ meltfnum[7] = 0;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4726:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[8] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4726:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[8])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[9] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4726:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[9];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4726;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_and res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V21*/ meltfptr[20];
	      /*_.MELT_DEBUG_FUN__V27*/ meltfptr[26] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V26*/ meltfptr[22] =
	      /*_.MELT_DEBUG_FUN__V27*/ meltfptr[26];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4726:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[9] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V27*/ meltfptr[26] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V26*/ meltfptr[22] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4726:/ quasiblock");


      /*_.PROGN___V28*/ meltfptr[26] = /*_.IF___V26*/ meltfptr[22];;
      /*^compute */
      /*_.IFCPP___V25*/ meltfptr[23] = /*_.PROGN___V28*/ meltfptr[26];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4726:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[8] = 0;
      /*^clear */
	     /*clear *//*_.IF___V26*/ meltfptr[22] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V28*/ meltfptr[26] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V25*/ meltfptr[23] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4727:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V21*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4727:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V15*/ meltfptr[11] = /*_.RETURN___V29*/ meltfptr[22];;

    MELT_LOCATION ("warmelt-macro.melt:4711:/ clear");
	   /*clear *//*_.CONT__V16*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ARGSP__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.RES__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V25*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V29*/ meltfptr[22] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4706:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V15*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4706:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PATEXPAND_AND", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_85_warmelt_macro_PATEXPAND_AND_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_85_warmelt_macro_PATEXPAND_AND */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_86_warmelt_macro_MEXPAND_OR (meltclosure_ptr_t meltclosp_,
				      melt_ptr_t meltfirstargp_,
				      const melt_argdescr_cell_t
				      meltxargdescr_[],
				      union meltparam_un * meltxargtab_,
				      const melt_argdescr_cell_t
				      meltxresdescr_[],
				      union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_86_warmelt_macro_MEXPAND_OR_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_86_warmelt_macro_MEXPAND_OR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 36
    melt_ptr_t mcfr_varptr[36];
#define MELTFRAM_NBVARNUM 10
    long mcfr_varnum[10];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_86_warmelt_macro_MEXPAND_OR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_86_warmelt_macro_MEXPAND_OR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 36; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_86_warmelt_macro_MEXPAND_OR nbval 36*/
  meltfram__.mcfr_nbvar = 36 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_OR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4743:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4744:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4744:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4744:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4744;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_or sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4744:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4744:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4744:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4745:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4745:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4745:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4745) ? (4745) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4745:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4746:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4746:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4746:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4746) ? (4746) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4746:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4747:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L5*/ meltfnum[1] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:4747:/ cond");
    /*cond */ if ( /*_#NULL__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V15*/ meltfptr[14] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[3]);;
	  /*_.IF___V14*/ meltfptr[12] = /*_.SETQ___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4747:/ clear");
	     /*clear *//*_.SETQ___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4748:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:4748:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4748:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4748) ? (4748) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4748:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4749:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4750:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[18] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4751:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[18])));;
    /*^compute */
 /*_.PAIR_TAIL__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    MELT_LOCATION ("warmelt-macro.melt:4755:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V24*/ meltfptr[23] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_6 */ meltfrout->
						tabval[6])), (3));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[0] =
      (melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 1 >= 0
		    && 1 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[1] =
      (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]);
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V24*/ meltfptr[23])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 2 >= 0
		    && 2 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V24*/ meltfptr[23])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V24*/ meltfptr[23])->tabval[2] =
      (melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4]);
    ;
    /*_.LAMBDA___V23*/ meltfptr[22] = /*_.LAMBDA___V24*/ meltfptr[23];;
    MELT_LOCATION ("warmelt-macro.melt:4752:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[5]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V23*/ meltfptr[22];
      /*_.CXTUP__V25*/ meltfptr[24] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.PAIR_TAIL__V22*/ meltfptr[21]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#NBCOMP__L7*/ meltfnum[0] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.CXTUP__V25*/ meltfptr[24])));;
    MELT_LOCATION ("warmelt-macro.melt:4760:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L8*/ meltfnum[7] =
      (( /*_#NBCOMP__L7*/ meltfnum[0]) < (1));;
    MELT_LOCATION ("warmelt-macro.melt:4760:/ cond");
    /*cond */ if ( /*_#I__L8*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4762:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			      ("OR without sons"), (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4763:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = ( /*nil */ NULL);;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:4763:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:4761:/ quasiblock");


	  /*_.PROGN___V28*/ meltfptr[27] = /*_.RETURN___V27*/ meltfptr[26];;
	  /*^compute */
	  /*_.IFELSE___V26*/ meltfptr[25] = /*_.PROGN___V28*/ meltfptr[27];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4760:/ clear");
	     /*clear *//*_.RETURN___V27*/ meltfptr[26] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V28*/ meltfptr[27] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

	/*^block */
	/*anyblock */
	{

	  MELT_LOCATION ("warmelt-macro.melt:4764:/ quasiblock");


	  MELT_LOCATION ("warmelt-macro.melt:4765:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*^rawallocobj */
	  /*rawallocobj */
	  {
	    melt_ptr_t newobj = 0;
	    melt_raw_object_create (newobj,
				    (melt_ptr_t) (( /*!CLASS_SOURCE_OR */
						   meltfrout->tabval[7])),
				    (3), "CLASS_SOURCE_OR");
    /*_.INST__V31*/ meltfptr[30] =
	      newobj;
	  };
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V31*/ meltfptr[30]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (1),
				( /*_.LOC__V20*/ meltfptr[19]),
				"LOCA_LOCATION");
	  ;
	  /*^putslot */
	  /*putslot */
	  melt_assertmsg ("putslot checkobj @SOR_DISJ",
			  melt_magic_discr ((melt_ptr_t)
					    ( /*_.INST__V31*/ meltfptr[30]))
			  == MELTOBMAG_OBJECT);
	  melt_putfield_object (( /*_.INST__V31*/ meltfptr[30]), (2),
				( /*_.CXTUP__V25*/ meltfptr[24]), "SOR_DISJ");
	  ;
	  /*^touchobj */

	  melt_dbgtrace_written_object ( /*_.INST__V31*/ meltfptr[30],
					"newly made instance");
	  ;
	  /*_.RES__V30*/ meltfptr[27] = /*_.INST__V31*/ meltfptr[30];;

#if MELT_HAVE_DEBUG
	  MELT_LOCATION ("warmelt-macro.melt:4768:/ cppif.then");
	  /*^block */
	  /*anyblock */
	  {


	    {
	      /*^locexp */
	      /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	      melt_dbgcounter++;
#endif
	      ;
	    }
	    ;
	    /*^checksignal */
	    MELT_CHECK_SIGNAL ();
	    ;
     /*_#MELT_NEED_DBG__L9*/ meltfnum[8] =
	      /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	      ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	      0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4768:/ cond");
	    /*cond */ if ( /*_#MELT_NEED_DBG__L9*/ meltfnum[8])	/*then */
	      {
		/*^cond.then */
		/*^block */
		/*anyblock */
		{

       /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] =
#ifdef meltcallcount
		    meltcallcount	/* the_meltcallcount */
#else
		    0L
#endif /* meltcallcount the_meltcallcount */
		    ;;
		  MELT_LOCATION ("warmelt-macro.melt:4768:/ checksignal");
		  MELT_CHECK_SIGNAL ();
		  ;
		  /*^apply */
		  /*apply */
		  {
		    union meltparam_un argtab[5];
		    memset (&argtab, 0, sizeof (argtab));
		    /*^apply.arg */
		    argtab[0].meltbp_long =
		      /*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9];
		    /*^apply.arg */
		    argtab[1].meltbp_cstring = "warmelt-macro.melt";
		    /*^apply.arg */
		    argtab[2].meltbp_long = 4768;
		    /*^apply.arg */
		    argtab[3].meltbp_cstring = "mexpand_or res";
		    /*^apply.arg */
		    argtab[4].meltbp_aptr =
		      (melt_ptr_t *) & /*_.RES__V30*/ meltfptr[27];
		    /*_.MELT_DEBUG_FUN__V34*/ meltfptr[33] =
		      melt_apply ((meltclosure_ptr_t)
				  (( /*!MELT_DEBUG_FUN */ meltfrout->
				    tabval[0])),
				  (melt_ptr_t) (( /*nil */ NULL)),
				  (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				   MELTBPARSTR_PTR ""), argtab, "",
				  (union meltparam_un *) 0);
		  }
		  ;
		  /*_.IF___V33*/ meltfptr[32] =
		    /*_.MELT_DEBUG_FUN__V34*/ meltfptr[33];;
		  /*epilog */

		  MELT_LOCATION ("warmelt-macro.melt:4768:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L10*/ meltfnum[9] = 0;
		  /*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V34*/ meltfptr[33] = 0;
		}
		;
	      }
	    else
	      {			/*^cond.else */

      /*_.IF___V33*/ meltfptr[32] = NULL;;
	      }
	    ;
	    MELT_LOCATION ("warmelt-macro.melt:4768:/ quasiblock");


	    /*_.PROGN___V35*/ meltfptr[33] = /*_.IF___V33*/ meltfptr[32];;
	    /*^compute */
	    /*_.IFCPP___V32*/ meltfptr[31] = /*_.PROGN___V35*/ meltfptr[33];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4768:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.IF___V33*/ meltfptr[32] = 0;
	    /*^clear */
	       /*clear *//*_.PROGN___V35*/ meltfptr[33] = 0;
	  }

#else /*MELT_HAVE_DEBUG */
	  /*^cppif.else */
	  /*_.IFCPP___V32*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4769:/ checksignal");
	  MELT_CHECK_SIGNAL ();
	  ;
	  /*^quasiblock */


	  /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V30*/ meltfptr[27];;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:4769:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  /*_.LET___V29*/ meltfptr[26] = /*_.RETURN___V36*/ meltfptr[32];;

	  MELT_LOCATION ("warmelt-macro.melt:4764:/ clear");
	     /*clear *//*_.RES__V30*/ meltfptr[27] = 0;
	  /*^clear */
	     /*clear *//*_.IFCPP___V32*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_.RETURN___V36*/ meltfptr[32] = 0;
	  /*_.IFELSE___V26*/ meltfptr[25] = /*_.LET___V29*/ meltfptr[26];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4760:/ clear");
	     /*clear *//*_.LET___V29*/ meltfptr[26] = 0;
	}
	;
      }
    ;
    /*_.LET___V18*/ meltfptr[16] = /*_.IFELSE___V26*/ meltfptr[25];;

    MELT_LOCATION ("warmelt-macro.melt:4749:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.CXTUP__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_#NBCOMP__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#I__L8*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IFELSE___V26*/ meltfptr[25] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4743:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4743:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_OR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_86_warmelt_macro_MEXPAND_OR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_86_warmelt_macro_MEXPAND_OR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_87_warmelt_macro_LAMBDA___21__ (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_87_warmelt_macro_LAMBDA___21___melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_87_warmelt_macro_LAMBDA___21___st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 3
    melt_ptr_t mcfr_varptr[3];
/*no varnum*/
#define MELTFRAM_NBVARNUM /*none*/0
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_87_warmelt_macro_LAMBDA___21__ is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_87_warmelt_macro_LAMBDA___21___st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 3; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_87_warmelt_macro_LAMBDA___21__ nbval 3*/
  meltfram__.mcfr_nbvar = 3 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("LAMBDA_", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4755:/ getarg");
 /*_.C__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;
  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {

    MELT_LOCATION ("warmelt-macro.melt:4756:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*~ENV */ meltfclos->tabval[1]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MEXPANDER */ meltfclos->tabval[0]);
      /*^apply.arg */
      argtab[2].meltbp_aptr =
	(melt_ptr_t *) & ( /*~MODCTX */ meltfclos->tabval[2]);
      /*_.MEXPANDER__V3*/ meltfptr[2] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*~MEXPANDER */ meltfclos->tabval[0])),
		    (melt_ptr_t) ( /*_.C__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4755:/ quasiblock");


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPANDER__V3*/ meltfptr[2];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4755:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.MEXPANDER__V3*/ meltfptr[2] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("LAMBDA_", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_87_warmelt_macro_LAMBDA___21___melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_87_warmelt_macro_LAMBDA___21__ */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_88_warmelt_macro_PATEXPAND_OR (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_88_warmelt_macro_PATEXPAND_OR_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_88_warmelt_macro_PATEXPAND_OR_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 30
    melt_ptr_t mcfr_varptr[30];
#define MELTFRAM_NBVARNUM 11
    long mcfr_varnum[11];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_88_warmelt_macro_PATEXPAND_OR is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_88_warmelt_macro_PATEXPAND_OR_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 30; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_88_warmelt_macro_PATEXPAND_OR nbval 30*/
  meltfram__.mcfr_nbvar = 30 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PATEXPAND_OR", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4773:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4774:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4774:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4774:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4774) ? (4774) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4774:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4775:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4775:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4775:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4775) ? (4775) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4775:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4776:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_PATTERN_EXPANSION_CONTEXT */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4776:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4776:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4776) ? (4776) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4776:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4777:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4777:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4777:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4777;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_or sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V12*/ meltfptr[11] =
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4777:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V12*/ meltfptr[11] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4777:/ quasiblock");


      /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.PROGN___V14*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4777:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4778:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4779:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V16*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4780:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.LIST_FIRST__V18*/ meltfptr[17] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V16*/ meltfptr[12])));;
    /*^compute */
 /*_.CURPAIR__V19*/ meltfptr[18] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:4782:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.PCTX__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.LOC__V17*/ meltfptr[16];
      /*_.ARGSP__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PATTERNEXPAND_PAIRLIST_AS_TUPLE */ meltfrout->
		      tabval[4])),
		    (melt_ptr_t) ( /*_.CURPAIR__V19*/ meltfptr[18]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4783:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_OR */
					     meltfrout->tabval[5])), (4),
			      "CLASS_SOURCE_PATTERN_OR");
  /*_.INST__V22*/ meltfptr[21] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (1),
			  ( /*_.LOC__V17*/ meltfptr[16]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @ORPAT_DISJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (3),
			  ( /*_.ARGSP__V20*/ meltfptr[19]), "ORPAT_DISJ");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V22*/ meltfptr[21],
				  "newly made instance");
    ;
    /*_.RES__V21*/ meltfptr[20] = /*_.INST__V22*/ meltfptr[21];;
    MELT_LOCATION ("warmelt-macro.melt:4789:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:4787:/ quasiblock");


    /*^multiapply */
    /*multiapply 1args, 3x.res */
    {

      union meltparam_un restab[3];
      memset (&restab, 0, sizeof (restab));
      /*^multiapply.xres */
      restab[0].meltbp_longptr = & /*_#IMAX__L6*/ meltfnum[4];
      /*^multiapply.xres */
      restab[1].meltbp_longptr = & /*_#IMIN__L7*/ meltfnum[0];
      /*^multiapply.xres */
      restab[2].meltbp_longptr = & /*_#ISUM__L8*/ meltfnum[7];
      /*^multiapply.appl */
      /*_.SUBPATW__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PATTERN_WEIGHT_TUPLE */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.ARGSP__V20*/ meltfptr[19]), (""),
		    (union meltparam_un *) 0,
		    (MELTBPARSTR_LONG MELTBPARSTR_LONG MELTBPARSTR_LONG ""),
		    restab);
    }
    ;
    /*^quasiblock */


    MELT_LOCATION ("warmelt-macro.melt:4790:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L9*/ meltfnum[8] =
      ((1) + ( /*_#IMIN__L7*/ meltfnum[0]));;
    /*^compute */
 /*_.MAKE_INTEGERBOX__V25*/ meltfptr[24] =
      (meltgc_new_int
       ((meltobject_ptr_t)
	(( /*!DISCR_CONSTANT_INTEGER */ meltfrout->tabval[7])),
	( /*_#I__L9*/ meltfnum[8])));;
    MELT_LOCATION ("warmelt-macro.melt:4790:/ quasiblock");


    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @PAT_WEIGHT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.RES__V21*/ meltfptr[20])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.RES__V21*/ meltfptr[20]), (2),
			  ( /*_.MAKE_INTEGERBOX__V25*/ meltfptr[24]),
			  "PAT_WEIGHT");
    ;
    /*^touch */
    meltgc_touch ( /*_.RES__V21*/ meltfptr[20]);
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.RES__V21*/ meltfptr[20], "put-fields");
    ;


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4793:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L10*/ meltfnum[9] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4793:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L10*/ meltfnum[9])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4793:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4793;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_or res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V21*/ meltfptr[20];
	      /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V27*/ meltfptr[26] =
	      /*_.MELT_DEBUG_FUN__V28*/ meltfptr[27];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4793:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L11*/ meltfnum[10] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V28*/ meltfptr[27] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V27*/ meltfptr[26] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4793:/ quasiblock");


      /*_.PROGN___V29*/ meltfptr[27] = /*_.IF___V27*/ meltfptr[26];;
      /*^compute */
      /*_.IFCPP___V26*/ meltfptr[25] = /*_.PROGN___V29*/ meltfptr[27];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4793:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L10*/ meltfnum[9] = 0;
      /*^clear */
	     /*clear *//*_.IF___V27*/ meltfptr[26] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[27] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V26*/ meltfptr[25] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4794:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V21*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4794:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    MELT_LOCATION ("warmelt-macro.melt:4787:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*_.MULTI___V23*/ meltfptr[22] = /*_.RETURN___V30*/ meltfptr[26];;

    MELT_LOCATION ("warmelt-macro.melt:4787:/ clear");
	   /*clear *//*_#I__L9*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.MAKE_INTEGERBOX__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V30*/ meltfptr[26] = 0;

    /*^clear */
	   /*clear *//*_#IMAX__L6*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#IMIN__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_#ISUM__L8*/ meltfnum[7] = 0;
    /*_.LET___V15*/ meltfptr[11] = /*_.MULTI___V23*/ meltfptr[22];;

    MELT_LOCATION ("warmelt-macro.melt:4778:/ clear");
	   /*clear *//*_.CONT__V16*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ARGSP__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.RES__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.MULTI___V23*/ meltfptr[22] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4773:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V15*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4773:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PATEXPAND_OR", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_88_warmelt_macro_PATEXPAND_OR_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_88_warmelt_macro_PATEXPAND_OR */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_89_warmelt_macro_MEXPAND_REFERENCE (meltclosure_ptr_t meltclosp_,
					     melt_ptr_t meltfirstargp_,
					     const melt_argdescr_cell_t
					     meltxargdescr_[],
					     union meltparam_un *
					     meltxargtab_,
					     const melt_argdescr_cell_t
					     meltxresdescr_[],
					     union meltparam_un *
					     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_89_warmelt_macro_MEXPAND_REFERENCE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_89_warmelt_macro_MEXPAND_REFERENCE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 41
    melt_ptr_t mcfr_varptr[41];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_89_warmelt_macro_MEXPAND_REFERENCE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_89_warmelt_macro_MEXPAND_REFERENCE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 41; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_89_warmelt_macro_MEXPAND_REFERENCE nbval 41*/
  meltfram__.mcfr_nbvar = 41 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_REFERENCE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4811:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4812:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4812:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4812:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4812;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_reference sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4812:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4812:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4812:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4813:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4813:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4813:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4813) ? (4813) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4813:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4814:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4814:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4814:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4814) ? (4814) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4814:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4815:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L5*/ meltfnum[1] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:4815:/ cond");
    /*cond */ if ( /*_#NULL__L5*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V15*/ meltfptr[14] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[3]);;
	  /*_.IF___V14*/ meltfptr[12] = /*_.SETQ___V15*/ meltfptr[14];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4815:/ clear");
	     /*clear *//*_.SETQ___V15*/ meltfptr[14] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V14*/ meltfptr[12] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4816:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L6*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:4816:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L6*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V17*/ meltfptr[16] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4816:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4816) ? (4816) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V17*/ meltfptr[16];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4816:/ clear");
	     /*clear *//*_#IS_OBJECT__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V17*/ meltfptr[16] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4817:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      MELT_LOCATION ("warmelt-macro.melt:4819:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!CLASS_REFERENCE */ meltfrout->
					    tabval[4])),
					  (melt_ptr_t) (( /*!CLASS_CLASS */
							 meltfrout->
							 tabval[5])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->
			     tabval[4])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 6, "CLASS_FIELDS");
     /*_.CLASS_FIELDS__V19*/ meltfptr[18] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.CLASS_FIELDS__V19*/ meltfptr[18] = NULL;;
	}
      ;
      /*^compute */
   /*_#MULTIPLE_LENGTH__L7*/ meltfnum[0] =
	(melt_multiple_length
	 ((melt_ptr_t) ( /*_.CLASS_FIELDS__V19*/ meltfptr[18])));;
      /*^compute */
   /*_#I__L8*/ meltfnum[7] =
	((1) == ( /*_#MULTIPLE_LENGTH__L7*/ meltfnum[0]));;
      MELT_LOCATION ("warmelt-macro.melt:4817:/ cond");
      /*cond */ if ( /*_#I__L8*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V20*/ meltfptr[19] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4817:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check class_reference has one field"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4817) ? (4817) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V18*/ meltfptr[16] = /*_.IFELSE___V20*/ meltfptr[19];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4817:/ clear");
	     /*clear *//*_.CLASS_FIELDS__V19*/ meltfptr[18] = 0;
      /*^clear */
	     /*clear *//*_#MULTIPLE_LENGTH__L7*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_#I__L8*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V20*/ meltfptr[19] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V18*/ meltfptr[16] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4820:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4821:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V22*/ meltfptr[19] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4822:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V23*/ meltfptr[22] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4823:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XARGTUP__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_RESTLIST_AS_TUPLE */ meltfrout->tabval[6])),
		    (melt_ptr_t) ( /*_.CONT__V22*/ meltfptr[19]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_#NBARG__L9*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.XARGTUP__V24*/ meltfptr[23])));;
    /*^compute */
 /*_.ARG1__V25*/ meltfptr[24] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.XARGTUP__V24*/ meltfptr[23]), (0)));;
    MELT_LOCATION ("warmelt-macro.melt:4826:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_FETCH_PREDEFINED */ meltfrout->tabval[7])), (3), "CLASS_SOURCE_FETCH_PREDEFINED");
  /*_.INST__V27*/ meltfptr[26] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (1),
			  ( /*_.LOC__V23*/ meltfptr[22]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SFEPD_PREDEF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V27*/ meltfptr[26])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V27*/ meltfptr[26]), (2),
			  (( /*!konst_8_CLASS_REFERENCE */ meltfrout->
			    tabval[8])), "SFEPD_PREDEF");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V27*/ meltfptr[26],
				  "newly made instance");
    ;
    /*_.SPREDCLASSCONT__V26*/ meltfptr[25] = /*_.INST__V27*/ meltfptr[26];;
    MELT_LOCATION ("warmelt-macro.melt:4829:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_8_CLASS_REFERENCE */ meltfrout->
			  tabval[8]);
      /*_.CLABIND__V28*/ meltfptr[27] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[9])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4830:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[6];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.LOC__V23*/ meltfptr[22];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_12_REFERENCED_VALUE */ meltfrout->
			  tabval[12]);
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.ARG1__V25*/ meltfptr[24];
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[4].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[5].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.FLDA__V29*/ meltfptr[28] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PARSE_FIELD_ASSIGNMENT */ meltfrout->tabval[11])),
		    (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->
				   tabval[4])),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4832:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:4837:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (1) rtup_0__TUPLREC__x5;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x5 */
 /*_.TUPLREC___V31*/ meltfptr[30] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x5;
      meltletrec_1_ptr->rtup_0__TUPLREC__x5.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x5.nbval = 1;


      /*^putuple */
      /*putupl#8 */
      melt_assertmsg ("putupl [:4837] #8 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V31*/ meltfptr[30]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:4837] #8 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V31*/
					      meltfptr[30]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->tabval[0] =
	(melt_ptr_t) ( /*_.FLDA__V29*/ meltfptr[28]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V31*/ meltfptr[30]);
      ;
      /*_.TUPLE___V30*/ meltfptr[29] = /*_.TUPLREC___V31*/ meltfptr[30];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4837:/ clear");
	    /*clear *//*_.TUPLREC___V31*/ meltfptr[30] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V31*/ meltfptr[30] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4832:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_INSTANCE */
					     meltfrout->tabval[13])), (5),
			      "CLASS_SOURCE_INSTANCE");
  /*_.INST__V33*/ meltfptr[32] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (1),
			  ( /*_.LOC__V23*/ meltfptr[22]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SMINS_CLASS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (2),
			  (( /*!CLASS_REFERENCE */ meltfrout->tabval[4])),
			  "SMINS_CLASS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SMINS_CLABIND",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (3),
			  ( /*_.CLABIND__V28*/ meltfptr[27]),
			  "SMINS_CLABIND");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SMINS_FIELDS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (4),
			  ( /*_.TUPLE___V30*/ meltfptr[29]), "SMINS_FIELDS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V33*/ meltfptr[32],
				  "newly made instance");
    ;
    /*_.SINST__V32*/ meltfptr[30] = /*_.INST__V33*/ meltfptr[32];;
    MELT_LOCATION ("warmelt-macro.melt:4839:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L10*/ meltfnum[7] =
      (( /*_#NBARG__L9*/ meltfnum[0]) != (1));;
    MELT_LOCATION ("warmelt-macro.melt:4839:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[7])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4841:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V23*/ meltfptr[22]),
			      ("(REFERENCE <value>) needs exactly one argument"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4842:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:4842:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:4840:/ quasiblock");


	  /*_.PROGN___V36*/ meltfptr[35] = /*_.RETURN___V35*/ meltfptr[34];;
	  /*^compute */
	  /*_.IF___V34*/ meltfptr[33] = /*_.PROGN___V36*/ meltfptr[35];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4839:/ clear");
	     /*clear *//*_.RETURN___V35*/ meltfptr[34] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V36*/ meltfptr[35] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V34*/ meltfptr[33] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4845:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L11*/ meltfnum[10] =
      (( /*_.CLABIND__V28*/ meltfptr[27]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:4845:/ cond");
    /*cond */ if ( /*_#NULL__L11*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4846:/ locexp");
	    melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V23*/ meltfptr[22]),
			      ("(REFERENCE <value>) where CLASS_REFERENCE is not visible"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4848:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4848:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4848:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4848;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_reference returns sinst";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SINST__V32*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V38*/ meltfptr[35] =
	      /*_.MELT_DEBUG_FUN__V39*/ meltfptr[38];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4848:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V39*/ meltfptr[38] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V38*/ meltfptr[35] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4848:/ quasiblock");


      /*_.PROGN___V40*/ meltfptr[38] = /*_.IF___V38*/ meltfptr[35];;
      /*^compute */
      /*_.IFCPP___V37*/ meltfptr[34] = /*_.PROGN___V40*/ meltfptr[38];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4848:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V38*/ meltfptr[35] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V40*/ meltfptr[38] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V37*/ meltfptr[34] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4849:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.SINST__V32*/ meltfptr[30];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4849:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V21*/ meltfptr[18] = /*_.RETURN___V41*/ meltfptr[35];;

    MELT_LOCATION ("warmelt-macro.melt:4820:/ clear");
	   /*clear *//*_.CONT__V22*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.XARGTUP__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#NBARG__L9*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.ARG1__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.SPREDCLASSCONT__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.CLABIND__V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.FLDA__V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.SINST__V32*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[7] = 0;
    /*^clear */
	   /*clear *//*_.IF___V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V37*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V41*/ meltfptr[35] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4811:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V21*/ meltfptr[18];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4811:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L5*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V18*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LET___V21*/ meltfptr[18] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_REFERENCE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_89_warmelt_macro_MEXPAND_REFERENCE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_89_warmelt_macro_MEXPAND_REFERENCE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_90_warmelt_macro_PATEXPAND_REFERENCE (meltclosure_ptr_t meltclosp_,
					       melt_ptr_t meltfirstargp_,
					       const melt_argdescr_cell_t
					       meltxargdescr_[],
					       union meltparam_un *
					       meltxargtab_,
					       const melt_argdescr_cell_t
					       meltxresdescr_[],
					       union meltparam_un *
					       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_90_warmelt_macro_PATEXPAND_REFERENCE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_90_warmelt_macro_PATEXPAND_REFERENCE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 34
    melt_ptr_t mcfr_varptr[34];
#define MELTFRAM_NBVARNUM 9
    long mcfr_varnum[9];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_90_warmelt_macro_PATEXPAND_REFERENCE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_90_warmelt_macro_PATEXPAND_REFERENCE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 34; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_90_warmelt_macro_PATEXPAND_REFERENCE nbval 34*/
  meltfram__.mcfr_nbvar = 34 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PATEXPAND_REFERENCE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4853:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4854:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4854:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4854:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4854) ? (4854) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4854:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4855:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4855:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4855:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4855) ? (4855) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4855:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4856:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_PATTERN_EXPANSION_CONTEXT */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4856:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4856:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4856) ? (4856) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4856:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4857:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4857:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4857:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4857;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_reference sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V12*/ meltfptr[11] =
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4857:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V12*/ meltfptr[11] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4857:/ quasiblock");


      /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.PROGN___V14*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4857:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4858:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4859:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V16*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4860:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.LIST_FIRST__V18*/ meltfptr[17] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V16*/ meltfptr[12])));;
    /*^compute */
 /*_.CURPAIR__V19*/ meltfptr[18] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:4862:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.PCTX__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.LOC__V17*/ meltfptr[16];
      /*_.ARGSP__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PATTERNEXPAND_PAIRLIST_AS_TUPLE */ meltfrout->
		      tabval[4])),
		    (melt_ptr_t) ( /*_.CURPAIR__V19*/ meltfptr[18]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_.ARG1__V21*/ meltfptr[20] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.ARGSP__V20*/ meltfptr[19]), (0)));;
    MELT_LOCATION ("warmelt-macro.melt:4864:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[5];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!CLASS_REFERENCE */ meltfrout->tabval[7]);
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.ARG1__V21*/ meltfptr[20];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[3].meltbp_aptr = (melt_ptr_t *) & /*_.PCTX__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[4].meltbp_aptr = (melt_ptr_t *) & /*_.LOC__V17*/ meltfptr[16];
      /*_.FLDP__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PARSE_FIELD_PATTERN */ meltfrout->tabval[5])),
		    (melt_ptr_t) (( /*!konst_6_REFERENCED_VALUE */ meltfrout->
				   tabval[6])),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
		     MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4865:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:4869:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (1) rtup_0__TUPLREC__x6;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x6 */
 /*_.TUPLREC___V24*/ meltfptr[23] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x6;
      meltletrec_1_ptr->rtup_0__TUPLREC__x6.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x6.nbval = 1;


      /*^putuple */
      /*putupl#9 */
      melt_assertmsg ("putupl [:4869] #9 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V24*/ meltfptr[23]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:4869] #9 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V24*/
					      meltfptr[23]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V24*/ meltfptr[23]))->tabval[0] =
	(melt_ptr_t) ( /*_.FLDP__V22*/ meltfptr[21]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V24*/ meltfptr[23]);
      ;
      /*_.TUPLE___V23*/ meltfptr[22] = /*_.TUPLREC___V24*/ meltfptr[23];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4869:/ clear");
	    /*clear *//*_.TUPLREC___V24*/ meltfptr[23] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V24*/ meltfptr[23] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4865:/ quasiblock");


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_INSTANCE */ meltfrout->tabval[8])), (5), "CLASS_SOURCE_PATTERN_INSTANCE");
  /*_.INST__V26*/ meltfptr[25] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (1),
			  ( /*_.LOC__V17*/ meltfptr[16]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @PAT_WEIGHT",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (2),
			  (( /*!konst_9 */ meltfrout->tabval[9])),
			  "PAT_WEIGHT");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SPAT_CLASS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (3),
			  (( /*!CLASS_REFERENCE */ meltfrout->tabval[7])),
			  "SPAT_CLASS");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SPAT_FIELDS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (4),
			  ( /*_.TUPLE___V23*/ meltfptr[22]), "SPAT_FIELDS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V26*/ meltfptr[25],
				  "newly made instance");
    ;
    /*_.RES__V25*/ meltfptr[23] = /*_.INST__V26*/ meltfptr[25];;
    MELT_LOCATION ("warmelt-macro.melt:4871:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#MULTIPLE_LENGTH__L6*/ meltfnum[4] =
      (melt_multiple_length ((melt_ptr_t) ( /*_.ARGSP__V20*/ meltfptr[19])));;
    /*^compute */
 /*_#I__L7*/ meltfnum[0] =
      (( /*_#MULTIPLE_LENGTH__L6*/ meltfnum[4]) != (1));;
    MELT_LOCATION ("warmelt-macro.melt:4871:/ cond");
    /*cond */ if ( /*_#I__L7*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4873:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V17*/ meltfptr[16]),
			      ("(REFERENCE <subpattern>) pattern needs one argument"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4874:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:4874:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:4872:/ quasiblock");


	  /*_.PROGN___V29*/ meltfptr[28] = /*_.RETURN___V28*/ meltfptr[27];;
	  /*^compute */
	  /*_.IF___V27*/ meltfptr[26] = /*_.PROGN___V29*/ meltfptr[28];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4871:/ clear");
	     /*clear *//*_.RETURN___V28*/ meltfptr[27] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V29*/ meltfptr[28] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V27*/ meltfptr[26] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4875:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[7] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4875:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[7])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4875:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4875;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_reference res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V25*/ meltfptr[23];
	      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V31*/ meltfptr[28] =
	      /*_.MELT_DEBUG_FUN__V32*/ meltfptr[31];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4875:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V32*/ meltfptr[31] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V31*/ meltfptr[28] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4875:/ quasiblock");


      /*_.PROGN___V33*/ meltfptr[31] = /*_.IF___V31*/ meltfptr[28];;
      /*^compute */
      /*_.IFCPP___V30*/ meltfptr[27] = /*_.PROGN___V33*/ meltfptr[31];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4875:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[7] = 0;
      /*^clear */
	     /*clear *//*_.IF___V31*/ meltfptr[28] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V33*/ meltfptr[31] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V30*/ meltfptr[27] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4876:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V25*/ meltfptr[23];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4876:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V15*/ meltfptr[11] = /*_.RETURN___V34*/ meltfptr[28];;

    MELT_LOCATION ("warmelt-macro.melt:4858:/ clear");
	   /*clear *//*_.CONT__V16*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ARGSP__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.ARG1__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.FLDP__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.TUPLE___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.RES__V25*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_#MULTIPLE_LENGTH__L6*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_#I__L7*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V30*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V34*/ meltfptr[28] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4853:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V15*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4853:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PATEXPAND_REFERENCE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_90_warmelt_macro_PATEXPAND_REFERENCE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_90_warmelt_macro_PATEXPAND_REFERENCE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_91_warmelt_macro_MEXPANDOBSOLETE_CONTAINER (meltclosure_ptr_t
						     meltclosp_,
						     melt_ptr_t
						     meltfirstargp_,
						     const
						     melt_argdescr_cell_t
						     meltxargdescr_[],
						     union meltparam_un *
						     meltxargtab_,
						     const
						     melt_argdescr_cell_t
						     meltxresdescr_[],
						     union meltparam_un *
						     meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_91_warmelt_macro_MEXPANDOBSOLETE_CONTAINER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_91_warmelt_macro_MEXPANDOBSOLETE_CONTAINER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_91_warmelt_macro_MEXPANDOBSOLETE_CONTAINER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_91_warmelt_macro_MEXPANDOBSOLETE_CONTAINER_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_91_warmelt_macro_MEXPANDOBSOLETE_CONTAINER nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPANDOBSOLETE_CONTAINER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4893:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4894:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4894:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4894:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4894;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpandobsolete_container sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4894:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4894:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4894:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4895:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4895:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4895:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4895) ? (4895) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4895:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4896:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.SEXPR__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_LOCATED */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
   /*_.LOCA_LOCATION__V12*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LOCA_LOCATION__V12*/ meltfptr[7] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-macro.melt:4896:/ locexp");
      melt_warning_str (0,
			(melt_ptr_t) ( /*_.LOCA_LOCATION__V12*/ meltfptr[7]),
			("obsolete use of CONTAINER in expression; use REFERENCE instead"),
			(melt_ptr_t) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4898:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.MEXPAND_REFERENCE__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MEXPAND_REFERENCE */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4893:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      /*_.MEXPAND_REFERENCE__V13*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4893:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LOCA_LOCATION__V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.MEXPAND_REFERENCE__V13*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPANDOBSOLETE_CONTAINER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_91_warmelt_macro_MEXPANDOBSOLETE_CONTAINER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_91_warmelt_macro_MEXPANDOBSOLETE_CONTAINER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_92_warmelt_macro_PATEXPANDOBSOLETE_CONTAINER (meltclosure_ptr_t
						       meltclosp_,
						       melt_ptr_t
						       meltfirstargp_,
						       const
						       melt_argdescr_cell_t
						       meltxargdescr_[],
						       union meltparam_un *
						       meltxargtab_,
						       const
						       melt_argdescr_cell_t
						       meltxresdescr_[],
						       union meltparam_un *
						       meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_92_warmelt_macro_PATEXPANDOBSOLETE_CONTAINER_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_92_warmelt_macro_PATEXPANDOBSOLETE_CONTAINER_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 8
    melt_ptr_t mcfr_varptr[8];
#define MELTFRAM_NBVARNUM 1
    long mcfr_varnum[1];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_92_warmelt_macro_PATEXPANDOBSOLETE_CONTAINER is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_92_warmelt_macro_PATEXPANDOBSOLETE_CONTAINER_st
	 *) meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 8; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_92_warmelt_macro_PATEXPANDOBSOLETE_CONTAINER nbval 8*/
  meltfram__.mcfr_nbvar = 8 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PATEXPANDOBSOLETE_CONTAINER", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4900:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4901:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:4901:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4901:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4901) ? (4901) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4901:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4902:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.SEXPR__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_LOCATED */
						       meltfrout->tabval[1])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
   /*_.LOCA_LOCATION__V7*/ meltfptr[5] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LOCA_LOCATION__V7*/ meltfptr[5] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-macro.melt:4902:/ locexp");
      melt_warning_str (0,
			(melt_ptr_t) ( /*_.LOCA_LOCATION__V7*/ meltfptr[5]),
			("obsolete use of CONTAINER in pattern; use REFERENCE instead"),
			(melt_ptr_t) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4904:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.PCTX__V4*/ meltfptr[3];
      /*_.PATEXPAND_REFERENCE__V8*/ meltfptr[7] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PATEXPAND_REFERENCE */ meltfrout->tabval[2])),
		    (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4900:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] =
      /*_.PATEXPAND_REFERENCE__V8*/ meltfptr[7];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4900:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.LOCA_LOCATION__V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.PATEXPAND_REFERENCE__V8*/ meltfptr[7] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PATEXPANDOBSOLETE_CONTAINER", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_92_warmelt_macro_PATEXPANDOBSOLETE_CONTAINER_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_92_warmelt_macro_PATEXPANDOBSOLETE_CONTAINER */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_93_warmelt_macro_MEXPAND_DEREF (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_93_warmelt_macro_MEXPAND_DEREF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_93_warmelt_macro_MEXPAND_DEREF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 38
    melt_ptr_t mcfr_varptr[38];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_93_warmelt_macro_MEXPAND_DEREF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_93_warmelt_macro_MEXPAND_DEREF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 38; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_93_warmelt_macro_MEXPAND_DEREF nbval 38*/
  meltfram__.mcfr_nbvar = 38 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_DEREF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4914:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4915:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4915:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4915:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4915;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_deref sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4915:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4915:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4915:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4916:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4916:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4916:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4916) ? (4916) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4916:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4917:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4917:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4917:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4917) ? (4917) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4917:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4918:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:4918:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4918:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4918) ? (4918) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4918:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4919:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      MELT_LOCATION ("warmelt-macro.melt:4921:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!CLASS_REFERENCE */ meltfrout->
					    tabval[3])),
					  (melt_ptr_t) (( /*!CLASS_CLASS */
							 meltfrout->
							 tabval[4])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->
			     tabval[3])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 6, "CLASS_FIELDS");
     /*_.CLASS_FIELDS__V17*/ meltfptr[16] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.CLASS_FIELDS__V17*/ meltfptr[16] = NULL;;
	}
      ;
      /*^compute */
   /*_#MULTIPLE_LENGTH__L6*/ meltfnum[0] =
	(melt_multiple_length
	 ((melt_ptr_t) ( /*_.CLASS_FIELDS__V17*/ meltfptr[16])));;
      /*^compute */
   /*_#I__L7*/ meltfnum[1] =
	((1) == ( /*_#MULTIPLE_LENGTH__L6*/ meltfnum[0]));;
      MELT_LOCATION ("warmelt-macro.melt:4919:/ cond");
      /*cond */ if ( /*_#I__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4919:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check class_reference has one field"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4919) ? (4919) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4919:/ clear");
	     /*clear *//*_.CLASS_FIELDS__V17*/ meltfptr[16] = 0;
      /*^clear */
	     /*clear *//*_#MULTIPLE_LENGTH__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_#I__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4922:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4923:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V20*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4924:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V21*/ meltfptr[20] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4925:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XARGTUP__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_RESTLIST_AS_TUPLE */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.CONT__V20*/ meltfptr[17]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_#NBARG__L8*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.XARGTUP__V22*/ meltfptr[21])));;
    /*^compute */
 /*_.ARG1__V23*/ meltfptr[22] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.XARGTUP__V22*/ meltfptr[21]), (0)));;
    MELT_LOCATION ("warmelt-macro.melt:4928:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_FETCH_PREDEFINED */ meltfrout->tabval[6])), (3), "CLASS_SOURCE_FETCH_PREDEFINED");
  /*_.INST__V25*/ meltfptr[24] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V25*/ meltfptr[24]), (1),
			  ( /*_.LOC__V21*/ meltfptr[20]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SFEPD_PREDEF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V25*/ meltfptr[24])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V25*/ meltfptr[24]), (2),
			  (( /*!konst_7_CLASS_REFERENCE */ meltfrout->
			    tabval[7])), "SFEPD_PREDEF");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V25*/ meltfptr[24],
				  "newly made instance");
    ;
    /*_.SPREDCLASSCONT__V24*/ meltfptr[23] = /*_.INST__V25*/ meltfptr[24];;
    MELT_LOCATION ("warmelt-macro.melt:4931:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_7_CLASS_REFERENCE */ meltfrout->
			  tabval[7]);
      /*_.CLABIND__V26*/ meltfptr[25] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4932:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_GET_FIELD */
					     meltfrout->tabval[10])), (4),
			      "CLASS_SOURCE_GET_FIELD");
  /*_.INST__V28*/ meltfptr[27] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (1),
			  ( /*_.LOC__V21*/ meltfptr[20]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SUGET_OBJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (2),
			  ( /*_.ARG1__V23*/ meltfptr[22]), "SUGET_OBJ");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SUGET_FIELD",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V28*/ meltfptr[27])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V28*/ meltfptr[27]), (3),
			  (( /*!REFERENCED_VALUE */ meltfrout->tabval[11])),
			  "SUGET_FIELD");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V28*/ meltfptr[27],
				  "newly made instance");
    ;
    /*_.SGET__V27*/ meltfptr[26] = /*_.INST__V28*/ meltfptr[27];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4938:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L9*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t)
			     (( /*!REFERENCED_VALUE */ meltfrout->
			       tabval[11])),
			     (melt_ptr_t) (( /*!CLASS_FIELD */ meltfrout->
					    tabval[12])));;
      MELT_LOCATION ("warmelt-macro.melt:4938:/ cond");
      /*cond */ if ( /*_#IS_A__L9*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V30*/ meltfptr[29] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4938:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check referenced_value"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4938) ? (4938) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V30*/ meltfptr[29] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V29*/ meltfptr[28] = /*_.IFELSE___V30*/ meltfptr[29];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4938:/ clear");
	     /*clear *//*_#IS_A__L9*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V30*/ meltfptr[29] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V29*/ meltfptr[28] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4939:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L10*/ meltfnum[1] =
      (( /*_#NBARG__L8*/ meltfnum[0]) != (1));;
    MELT_LOCATION ("warmelt-macro.melt:4939:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4941:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V21*/ meltfptr[20]),
			      ("(DEREF <value>) needs exactly one argument"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:4942:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:4942:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:4940:/ quasiblock");


	  /*_.PROGN___V33*/ meltfptr[32] = /*_.RETURN___V32*/ meltfptr[31];;
	  /*^compute */
	  /*_.IF___V31*/ meltfptr[29] = /*_.PROGN___V33*/ meltfptr[32];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:4939:/ clear");
	     /*clear *//*_.RETURN___V32*/ meltfptr[31] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V33*/ meltfptr[32] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V31*/ meltfptr[29] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4945:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L11*/ meltfnum[10] =
      (( /*_.CLABIND__V26*/ meltfptr[25]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:4945:/ cond");
    /*cond */ if ( /*_#NULL__L11*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:4946:/ locexp");
	    melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V21*/ meltfptr[20]),
			      ("(DEREF <value>) where CLASS_REFERENCE is not visible"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4947:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4947:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4947:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4947;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_deref returns sget";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SGET__V27*/ meltfptr[26];
	      /*_.MELT_DEBUG_FUN__V36*/ meltfptr[35] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V35*/ meltfptr[32] =
	      /*_.MELT_DEBUG_FUN__V36*/ meltfptr[35];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4947:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V36*/ meltfptr[35] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V35*/ meltfptr[32] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4947:/ quasiblock");


      /*_.PROGN___V37*/ meltfptr[35] = /*_.IF___V35*/ meltfptr[32];;
      /*^compute */
      /*_.IFCPP___V34*/ meltfptr[31] = /*_.PROGN___V37*/ meltfptr[35];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4947:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V35*/ meltfptr[32] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V37*/ meltfptr[35] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V34*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4948:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.SGET__V27*/ meltfptr[26];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4948:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V19*/ meltfptr[16] = /*_.RETURN___V38*/ meltfptr[32];;

    MELT_LOCATION ("warmelt-macro.melt:4922:/ clear");
	   /*clear *//*_.CONT__V20*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.XARGTUP__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#NBARG__L8*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.ARG1__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.SPREDCLASSCONT__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.CLABIND__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.SGET__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V29*/ meltfptr[28] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V31*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V34*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V38*/ meltfptr[32] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4914:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V19*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4914:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V19*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_DEREF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_93_warmelt_macro_MEXPAND_DEREF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_93_warmelt_macro_MEXPAND_DEREF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_94_warmelt_macro_MEXPANDOBSOLETE_CONTENT (meltclosure_ptr_t
						   meltclosp_,
						   melt_ptr_t meltfirstargp_,
						   const melt_argdescr_cell_t
						   meltxargdescr_[],
						   union meltparam_un *
						   meltxargtab_,
						   const melt_argdescr_cell_t
						   meltxresdescr_[],
						   union meltparam_un *
						   meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_94_warmelt_macro_MEXPANDOBSOLETE_CONTENT_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_94_warmelt_macro_MEXPANDOBSOLETE_CONTENT_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 13
    melt_ptr_t mcfr_varptr[13];
#define MELTFRAM_NBVARNUM 3
    long mcfr_varnum[3];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_94_warmelt_macro_MEXPANDOBSOLETE_CONTENT is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_94_warmelt_macro_MEXPANDOBSOLETE_CONTENT_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 13; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_94_warmelt_macro_MEXPANDOBSOLETE_CONTENT nbval 13*/
  meltfram__.mcfr_nbvar = 13 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPANDOBSOLETE_CONTENT", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4958:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4959:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4959:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4959:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4959;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpandobsolete_content sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4959:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4959:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4959:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4960:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4960:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4960:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4960) ? (4960) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4960:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4961:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.SEXPR__V2*/ meltfptr[1]),
					(melt_ptr_t) (( /*!CLASS_LOCATED */
						       meltfrout->tabval[2])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
   /*_.LOCA_LOCATION__V12*/ meltfptr[7] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.LOCA_LOCATION__V12*/ meltfptr[7] = NULL;;
      }
    ;

    {
      MELT_LOCATION ("warmelt-macro.melt:4961:/ locexp");
      melt_warning_str (0,
			(melt_ptr_t) ( /*_.LOCA_LOCATION__V12*/ meltfptr[7]),
			("obsolete use of CONTENT in expression; use DEREF instead"),
			(melt_ptr_t) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4963:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.MEXPAND_DEREF__V13*/ meltfptr[12] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!MEXPAND_DEREF */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4958:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.MEXPAND_DEREF__V13*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4958:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.LOCA_LOCATION__V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.MEXPAND_DEREF__V13*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPANDOBSOLETE_CONTENT", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_94_warmelt_macro_MEXPANDOBSOLETE_CONTENT_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_94_warmelt_macro_MEXPANDOBSOLETE_CONTENT */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_95_warmelt_macro_MEXPAND_SET_REF (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_95_warmelt_macro_MEXPAND_SET_REF_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_95_warmelt_macro_MEXPAND_SET_REF_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 43
    melt_ptr_t mcfr_varptr[43];
#define MELTFRAM_NBVARNUM 13
    long mcfr_varnum[13];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_95_warmelt_macro_MEXPAND_SET_REF is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_95_warmelt_macro_MEXPAND_SET_REF_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 43; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_95_warmelt_macro_MEXPAND_SET_REF nbval 43*/
  meltfram__.mcfr_nbvar = 43 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_SET_REF", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:4980:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4981:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:4981:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:4981:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 4981;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_set_ref sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:4981:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:4981:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4981:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4982:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:4982:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4982:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4982) ? (4982) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4982:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4983:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:4983:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4983:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4983) ? (4983) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4983:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4984:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L5*/ meltfnum[1] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:4984:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V15*/ meltfptr[14] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4984:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4984) ? (4984) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.IFELSE___V15*/ meltfptr[14];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4984:/ clear");
	     /*clear *//*_#IS_OBJECT__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V15*/ meltfptr[14] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:4985:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
      MELT_LOCATION ("warmelt-macro.melt:4987:/ cond");
      /*cond */ if (
		     /*ifisa */
		     melt_is_instance_of ((melt_ptr_t)
					  (( /*!CLASS_REFERENCE */ meltfrout->
					    tabval[3])),
					  (melt_ptr_t) (( /*!CLASS_CLASS */
							 meltfrout->
							 tabval[4])))
	)			/*then */
	{
	  /*^cond.then */
	  /*^getslot */
	  {
	    melt_ptr_t slot = NULL, obj = NULL;
	    obj =
	      (melt_ptr_t) (( /*!CLASS_REFERENCE */ meltfrout->
			     tabval[3])) /*=obj*/ ;
	    melt_object_get_field (slot, obj, 6, "CLASS_FIELDS");
     /*_.CLASS_FIELDS__V17*/ meltfptr[16] = slot;
	  };
	  ;
	}
      else
	{			/*^cond.else */

    /*_.CLASS_FIELDS__V17*/ meltfptr[16] = NULL;;
	}
      ;
      /*^compute */
   /*_#MULTIPLE_LENGTH__L6*/ meltfnum[0] =
	(melt_multiple_length
	 ((melt_ptr_t) ( /*_.CLASS_FIELDS__V17*/ meltfptr[16])));;
      /*^compute */
   /*_#I__L7*/ meltfnum[1] =
	((1) == ( /*_#MULTIPLE_LENGTH__L6*/ meltfnum[0]));;
      MELT_LOCATION ("warmelt-macro.melt:4985:/ cond");
      /*cond */ if ( /*_#I__L7*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V18*/ meltfptr[17] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:4985:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check class_reference has one field"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (4985) ? (4985) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V16*/ meltfptr[14] = /*_.IFELSE___V18*/ meltfptr[17];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4985:/ clear");
	     /*clear *//*_.CLASS_FIELDS__V17*/ meltfptr[16] = 0;
      /*^clear */
	     /*clear *//*_#MULTIPLE_LENGTH__L6*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_#I__L7*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V18*/ meltfptr[17] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V16*/ meltfptr[14] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:4988:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:4989:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V20*/ meltfptr[17] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4990:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V21*/ meltfptr[20] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:4991:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XARGTUP__V22*/ meltfptr[21] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_RESTLIST_AS_TUPLE */ meltfrout->tabval[5])),
		    (melt_ptr_t) ( /*_.CONT__V20*/ meltfptr[17]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_#NBARG__L8*/ meltfnum[0] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.XARGTUP__V22*/ meltfptr[21])));;
    /*^compute */
 /*_.ARG1__V23*/ meltfptr[22] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.XARGTUP__V22*/ meltfptr[21]), (0)));;
    /*^compute */
 /*_.ARG2__V24*/ meltfptr[23] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.XARGTUP__V22*/ meltfptr[21]), (1)));;
    MELT_LOCATION ("warmelt-macro.melt:4995:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_FETCH_PREDEFINED */ meltfrout->tabval[6])), (3), "CLASS_SOURCE_FETCH_PREDEFINED");
  /*_.INST__V26*/ meltfptr[25] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (1),
			  ( /*_.LOC__V21*/ meltfptr[20]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SFEPD_PREDEF",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V26*/ meltfptr[25])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V26*/ meltfptr[25]), (2),
			  (( /*!konst_7_CLASS_REFERENCE */ meltfrout->
			    tabval[7])), "SFEPD_PREDEF");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V26*/ meltfptr[25],
				  "newly made instance");
    ;
    /*_.SPREDCLASSCONT__V25*/ meltfptr[24] = /*_.INST__V26*/ meltfptr[25];;
    MELT_LOCATION ("warmelt-macro.melt:4998:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!konst_7_CLASS_REFERENCE */ meltfrout->
			  tabval[7]);
      /*_.CLABIND__V27*/ meltfptr[26] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!FIND_ENV */ meltfrout->tabval[8])),
		    (melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:4999:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_FIELDASSIGN */
					     meltfrout->tabval[10])), (4),
			      "CLASS_SOURCE_FIELDASSIGN");
  /*_.INST__V29*/ meltfptr[28] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (1),
			  ( /*_.LOC__V21*/ meltfptr[20]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SFLA_FIELD",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (2),
			  (( /*!REFERENCED_VALUE */ meltfrout->tabval[11])),
			  "SFLA_FIELD");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SFLA_EXPR",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V29*/ meltfptr[28])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V29*/ meltfptr[28]), (3),
			  ( /*_.ARG2__V24*/ meltfptr[23]), "SFLA_EXPR");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V29*/ meltfptr[28],
				  "newly made instance");
    ;
    /*_.INST___V28*/ meltfptr[27] = /*_.INST__V29*/ meltfptr[28];;
    MELT_LOCATION ("warmelt-macro.melt:4999:/ blockmultialloc");
    /*multiallocblock */
    {
      struct meltletrec_1_st
      {
	struct MELT_MULTIPLE_STRUCT (1) rtup_0__TUPLREC__x7;
	long meltletrec_1_endgap;
      } *meltletrec_1_ptr = 0;
      meltletrec_1_ptr =
	(struct meltletrec_1_st *)
	meltgc_allocate (sizeof (struct meltletrec_1_st), 0);
      /*^blockmultialloc.initfill */
      /*inimult rtup_0__TUPLREC__x7 */
 /*_.TUPLREC___V31*/ meltfptr[30] =
	(melt_ptr_t) & meltletrec_1_ptr->rtup_0__TUPLREC__x7;
      meltletrec_1_ptr->rtup_0__TUPLREC__x7.discr =
	(meltobject_ptr_t) (((melt_ptr_t) (MELT_PREDEF (DISCR_MULTIPLE))));
      meltletrec_1_ptr->rtup_0__TUPLREC__x7.nbval = 1;


      /*^putuple */
      /*putupl#10 */
      melt_assertmsg ("putupl [:4999] #10 checktup",
		      melt_magic_discr ((melt_ptr_t)
					( /*_.TUPLREC___V31*/ meltfptr[30]))
		      == MELTOBMAG_MULTIPLE);
      melt_assertmsg ("putupl [:4999] #10 checkoff",
		      (0 >= 0
		       && 0 <
		       melt_multiple_length ((melt_ptr_t)
					     ( /*_.TUPLREC___V31*/
					      meltfptr[30]))));
      ((meltmultiple_ptr_t) ( /*_.TUPLREC___V31*/ meltfptr[30]))->tabval[0] =
	(melt_ptr_t) ( /*_.INST___V28*/ meltfptr[27]);
      ;
      /*^touch */
      meltgc_touch ( /*_.TUPLREC___V31*/ meltfptr[30]);
      ;
      /*_.PUTUP__V30*/ meltfptr[29] = /*_.TUPLREC___V31*/ meltfptr[30];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:4999:/ clear");
	    /*clear *//*_.TUPLREC___V31*/ meltfptr[30] = 0;
      /*^clear */
	    /*clear *//*_.TUPLREC___V31*/ meltfptr[30] = 0;
    }				/*end multiallocblock */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5005:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PUT_FIELDS */
					     meltfrout->tabval[12])), (4),
			      "CLASS_SOURCE_PUT_FIELDS");
  /*_.INST__V33*/ meltfptr[32] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (1),
			  ( /*_.LOC__V21*/ meltfptr[20]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SUPUT_OBJ",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (2),
			  ( /*_.ARG1__V23*/ meltfptr[22]), "SUPUT_OBJ");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SUPUT_FIELDS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V33*/ meltfptr[32])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V33*/ meltfptr[32]), (3),
			  ( /*_.PUTUP__V30*/ meltfptr[29]), "SUPUT_FIELDS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V33*/ meltfptr[32],
				  "newly made instance");
    ;
    /*_.SPUT__V32*/ meltfptr[30] = /*_.INST__V33*/ meltfptr[32];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5011:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L9*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t)
			     (( /*!REFERENCED_VALUE */ meltfrout->
			       tabval[11])),
			     (melt_ptr_t) (( /*!CLASS_FIELD */ meltfrout->
					    tabval[13])));;
      MELT_LOCATION ("warmelt-macro.melt:5011:/ cond");
      /*cond */ if ( /*_#IS_A__L9*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V35*/ meltfptr[34] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5011:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check referenced_value"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5011) ? (5011) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V35*/ meltfptr[34] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V34*/ meltfptr[33] = /*_.IFELSE___V35*/ meltfptr[34];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5011:/ clear");
	     /*clear *//*_#IS_A__L9*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V35*/ meltfptr[34] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V34*/ meltfptr[33] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5012:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#I__L10*/ meltfnum[1] =
      (( /*_#NBARG__L8*/ meltfnum[0]) != (2));;
    MELT_LOCATION ("warmelt-macro.melt:5012:/ cond");
    /*cond */ if ( /*_#I__L10*/ meltfnum[1])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5014:/ locexp");
	    /* error_plain */
	      melt_error_str ((melt_ptr_t) ( /*_.LOC__V21*/ meltfptr[20]),
			      ("(SET_REF <ref> <value>) needs exactly two arguments"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5015:/ quasiblock");


   /*_.RETVAL___V1*/ meltfptr[0] = NULL;;

	  {
	    MELT_LOCATION ("warmelt-macro.melt:5015:/ locexp");
	    /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
	    if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	      melt_warn_for_no_expected_secondary_results ();
	    /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
	    ;
	  }
	  ;
	  /*^finalreturn */
	  ;
	  /*finalret */ goto labend_rout;
	  MELT_LOCATION ("warmelt-macro.melt:5013:/ quasiblock");


	  /*_.PROGN___V38*/ meltfptr[37] = /*_.RETURN___V37*/ meltfptr[36];;
	  /*^compute */
	  /*_.IF___V36*/ meltfptr[34] = /*_.PROGN___V38*/ meltfptr[37];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5012:/ clear");
	     /*clear *//*_.RETURN___V37*/ meltfptr[36] = 0;
	  /*^clear */
	     /*clear *//*_.PROGN___V38*/ meltfptr[37] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V36*/ meltfptr[34] = NULL;;
      }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5018:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L11*/ meltfnum[10] =
      (( /*_.CLABIND__V27*/ meltfptr[26]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:5018:/ cond");
    /*cond */ if ( /*_#NULL__L11*/ meltfnum[10])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5019:/ locexp");
	    melt_warning_str (0, (melt_ptr_t) ( /*_.LOC__V21*/ meltfptr[20]),
			      ("(SET_REF <ref> <value>) where CLASS_REFERENCE is not visible"),
			      (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5020:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L12*/ meltfnum[11] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5020:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L12*/ meltfnum[11])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5020:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5020;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_set_ref returns sput";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SPUT__V32*/ meltfptr[30];
	      /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V40*/ meltfptr[37] =
	      /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5020:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L13*/ meltfnum[12] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V40*/ meltfptr[37] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5020:/ quasiblock");


      /*_.PROGN___V42*/ meltfptr[40] = /*_.IF___V40*/ meltfptr[37];;
      /*^compute */
      /*_.IFCPP___V39*/ meltfptr[36] = /*_.PROGN___V42*/ meltfptr[40];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5020:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L12*/ meltfnum[11] = 0;
      /*^clear */
	     /*clear *//*_.IF___V40*/ meltfptr[37] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V42*/ meltfptr[40] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V39*/ meltfptr[36] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5021:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.SPUT__V32*/ meltfptr[30];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5021:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V19*/ meltfptr[16] = /*_.RETURN___V43*/ meltfptr[37];;

    MELT_LOCATION ("warmelt-macro.melt:4988:/ clear");
	   /*clear *//*_.CONT__V20*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.XARGTUP__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_#NBARG__L8*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.ARG1__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.ARG2__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.SPREDCLASSCONT__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.CLABIND__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.INST___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.PUTUP__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_.SPUT__V32*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V34*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#I__L10*/ meltfnum[1] = 0;
    /*^clear */
	   /*clear *//*_.IF___V36*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L11*/ meltfnum[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V39*/ meltfptr[36] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V43*/ meltfptr[37] = 0;
    MELT_LOCATION ("warmelt-macro.melt:4980:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V19*/ meltfptr[16];;

    {
      MELT_LOCATION ("warmelt-macro.melt:4980:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V16*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LET___V19*/ meltfptr[16] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_SET_REF", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_95_warmelt_macro_MEXPAND_SET_REF_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_95_warmelt_macro_MEXPAND_SET_REF */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_96_warmelt_macro_MEXPAND_TUPLE (meltclosure_ptr_t meltclosp_,
					 melt_ptr_t meltfirstargp_,
					 const melt_argdescr_cell_t
					 meltxargdescr_[],
					 union meltparam_un * meltxargtab_,
					 const melt_argdescr_cell_t
					 meltxresdescr_[],
					 union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_96_warmelt_macro_MEXPAND_TUPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_96_warmelt_macro_MEXPAND_TUPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 24
    melt_ptr_t mcfr_varptr[24];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_96_warmelt_macro_MEXPAND_TUPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_96_warmelt_macro_MEXPAND_TUPLE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 24; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_96_warmelt_macro_MEXPAND_TUPLE nbval 24*/
  meltfram__.mcfr_nbvar = 24 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_TUPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5032:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5033:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5033:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5033:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5033;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_tuple sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5033:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5033:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5033:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5034:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5034:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5034:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5034) ? (5034) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5034:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5035:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:5035:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5035:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5035) ? (5035) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5035:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5036:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5037:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5038:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5039:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XARGTUP__V17*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_RESTLIST_AS_TUPLE */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.CONT__V15*/ meltfptr[14]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5040:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_TUPLE */
					     meltfrout->tabval[4])), (3),
			      "CLASS_SOURCE_TUPLE");
  /*_.INST__V19*/ meltfptr[18] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (1),
			  ( /*_.LOC__V16*/ meltfptr[15]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SARGOP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (2),
			  ( /*_.XARGTUP__V17*/ meltfptr[16]), "SARGOP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V19*/ meltfptr[18],
				  "newly made instance");
    ;
    /*_.RES__V18*/ meltfptr[17] = /*_.INST__V19*/ meltfptr[18];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5044:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5044:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5044:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5044;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_tuple result";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V18*/ meltfptr[17];
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V21*/ meltfptr[20] =
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5044:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V21*/ meltfptr[20] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5044:/ quasiblock");


      /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[19] = /*_.PROGN___V23*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5044:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5045:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V18*/ meltfptr[17];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5045:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V14*/ meltfptr[12] = /*_.RETURN___V24*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-macro.melt:5036:/ clear");
	   /*clear *//*_.CONT__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.XARGTUP__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.RES__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V24*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5032:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V14*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5032:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_TUPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_96_warmelt_macro_MEXPAND_TUPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_96_warmelt_macro_MEXPAND_TUPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_97_warmelt_macro_PATEXPAND_TUPLE (meltclosure_ptr_t meltclosp_,
					   melt_ptr_t meltfirstargp_,
					   const melt_argdescr_cell_t
					   meltxargdescr_[],
					   union meltparam_un * meltxargtab_,
					   const melt_argdescr_cell_t
					   meltxresdescr_[],
					   union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_97_warmelt_macro_PATEXPAND_TUPLE_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_97_warmelt_macro_PATEXPAND_TUPLE_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 27
    melt_ptr_t mcfr_varptr[27];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_97_warmelt_macro_PATEXPAND_TUPLE is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_97_warmelt_macro_PATEXPAND_TUPLE_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 27; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_97_warmelt_macro_PATEXPAND_TUPLE nbval 27*/
  meltfram__.mcfr_nbvar = 27 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PATEXPAND_TUPLE", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5049:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5050:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5050:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5050:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5050) ? (5050) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5050:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5051:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5051:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5051:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5051) ? (5051) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5051:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5052:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_PATTERN_EXPANSION_CONTEXT */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:5052:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5052:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5052) ? (5052) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5052:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5053:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5053:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5053:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5053;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_tuple sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V12*/ meltfptr[11] =
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5053:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V12*/ meltfptr[11] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5053:/ quasiblock");


      /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.PROGN___V14*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5053:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5054:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5055:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V16*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5056:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.LIST_FIRST__V18*/ meltfptr[17] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V16*/ meltfptr[12])));;
    /*^compute */
 /*_.CURPAIR__V19*/ meltfptr[18] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:5058:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.PCTX__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.LOC__V17*/ meltfptr[16];
      /*_.ARGSP__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PATTERNEXPAND_PAIRLIST_AS_TUPLE */ meltfrout->
		      tabval[4])),
		    (melt_ptr_t) ( /*_.CURPAIR__V19*/ meltfptr[18]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5059:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_TUPLE */
					     meltfrout->tabval[5])), (4),
			      "CLASS_SOURCE_PATTERN_TUPLE");
  /*_.INST__V22*/ meltfptr[21] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (1),
			  ( /*_.LOC__V17*/ meltfptr[16]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @CTPAT_SUBPA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (3),
			  ( /*_.ARGSP__V20*/ meltfptr[19]), "CTPAT_SUBPA");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V22*/ meltfptr[21],
				  "newly made instance");
    ;
    /*_.RES__V21*/ meltfptr[20] = /*_.INST__V22*/ meltfptr[21];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5063:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[4] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5063:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5063:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5063;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_tuple res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V21*/ meltfptr[20];
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V24*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5063:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V24*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5063:/ quasiblock");


      /*_.PROGN___V26*/ meltfptr[24] = /*_.IF___V24*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[22] = /*_.PROGN___V26*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5063:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V26*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5064:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V21*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5064:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V15*/ meltfptr[11] = /*_.RETURN___V27*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-macro.melt:5054:/ clear");
	   /*clear *//*_.CONT__V16*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ARGSP__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.RES__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V27*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5049:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V15*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5049:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PATEXPAND_TUPLE", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_97_warmelt_macro_PATEXPAND_TUPLE_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_97_warmelt_macro_PATEXPAND_TUPLE */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_98_warmelt_macro_MEXPAND_LIST (meltclosure_ptr_t meltclosp_,
					melt_ptr_t meltfirstargp_,
					const melt_argdescr_cell_t
					meltxargdescr_[],
					union meltparam_un * meltxargtab_,
					const melt_argdescr_cell_t
					meltxresdescr_[],
					union meltparam_un * meltxrestab_)
{
  long current_blocklevel_signals_meltrout_98_warmelt_macro_MEXPAND_LIST_melt
    = melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_98_warmelt_macro_MEXPAND_LIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 24
    melt_ptr_t mcfr_varptr[24];
#define MELTFRAM_NBVARNUM 6
    long mcfr_varnum[6];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_98_warmelt_macro_MEXPAND_LIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_98_warmelt_macro_MEXPAND_LIST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 24; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_98_warmelt_macro_MEXPAND_LIST nbval 24*/
  meltfram__.mcfr_nbvar = 24 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_LIST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5079:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5080:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L1*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5080:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5080:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5080;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_list sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V7*/ meltfptr[6] = /*_.MELT_DEBUG_FUN__V8*/ meltfptr[7];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5080:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L2*/ meltfnum[1] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V8*/ meltfptr[7] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V7*/ meltfptr[6] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5080:/ quasiblock");


      /*_.PROGN___V9*/ meltfptr[7] = /*_.IF___V7*/ meltfptr[6];;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.PROGN___V9*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5080:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V7*/ meltfptr[6] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V9*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5081:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[1] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5081:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5081:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5081) ? (5081) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[6] = /*_.IFELSE___V11*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5081:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5082:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L4*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:5082:/ cond");
      /*cond */ if ( /*_#IS_A__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V13*/ meltfptr[12] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5082:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5082) ? (5082) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V12*/ meltfptr[7] = /*_.IFELSE___V13*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5082:/ clear");
	     /*clear *//*_#IS_A__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V13*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V12*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5083:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5084:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V15*/ meltfptr[14] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5085:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V16*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5086:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.XARGTUP__V17*/ meltfptr[16] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!EXPAND_RESTLIST_AS_TUPLE */ meltfrout->tabval[3])),
		    (melt_ptr_t) ( /*_.CONT__V15*/ meltfptr[14]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5087:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_LIST */
					     meltfrout->tabval[4])), (3),
			      "CLASS_SOURCE_LIST");
  /*_.INST__V19*/ meltfptr[18] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (1),
			  ( /*_.LOC__V16*/ meltfptr[15]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SARGOP_ARGS",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V19*/ meltfptr[18])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V19*/ meltfptr[18]), (2),
			  ( /*_.XARGTUP__V17*/ meltfptr[16]), "SARGOP_ARGS");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V19*/ meltfptr[18],
				  "newly made instance");
    ;
    /*_.RES__V18*/ meltfptr[17] = /*_.INST__V19*/ meltfptr[18];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5091:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[1] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5091:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[1])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5091:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5091;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_list result";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V18*/ meltfptr[17];
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[0])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V21*/ meltfptr[20] =
	      /*_.MELT_DEBUG_FUN__V22*/ meltfptr[21];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5091:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V22*/ meltfptr[21] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V21*/ meltfptr[20] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5091:/ quasiblock");


      /*_.PROGN___V23*/ meltfptr[21] = /*_.IF___V21*/ meltfptr[20];;
      /*^compute */
      /*_.IFCPP___V20*/ meltfptr[19] = /*_.PROGN___V23*/ meltfptr[21];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5091:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[1] = 0;
      /*^clear */
	     /*clear *//*_.IF___V21*/ meltfptr[20] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V23*/ meltfptr[21] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V20*/ meltfptr[19] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5092:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V18*/ meltfptr[17];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5092:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V14*/ meltfptr[12] = /*_.RETURN___V24*/ meltfptr[20];;

    MELT_LOCATION ("warmelt-macro.melt:5083:/ clear");
	   /*clear *//*_.CONT__V15*/ meltfptr[14] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V16*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.XARGTUP__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.RES__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V24*/ meltfptr[20] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5079:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V14*/ meltfptr[12];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5079:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V12*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.LET___V14*/ meltfptr[12] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_LIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_98_warmelt_macro_MEXPAND_LIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_98_warmelt_macro_MEXPAND_LIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_99_warmelt_macro_PATEXPAND_LIST (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_99_warmelt_macro_PATEXPAND_LIST_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_99_warmelt_macro_PATEXPAND_LIST_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 27
    melt_ptr_t mcfr_varptr[27];
#define MELTFRAM_NBVARNUM 7
    long mcfr_varnum[7];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_99_warmelt_macro_PATEXPAND_LIST is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_99_warmelt_macro_PATEXPAND_LIST_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 27; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_99_warmelt_macro_PATEXPAND_LIST nbval 27*/
  meltfram__.mcfr_nbvar = 27 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("PATEXPAND_LIST", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5096:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.PCTX__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5097:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5097:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V6*/ meltfptr[5] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5097:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5097) ? (5097) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V5*/ meltfptr[4] = /*_.IFELSE___V6*/ meltfptr[5];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5097:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V6*/ meltfptr[5] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V5*/ meltfptr[4] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5098:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5098:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V8*/ meltfptr[7] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5098:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5098) ? (5098) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V7*/ meltfptr[5] = /*_.IFELSE___V8*/ meltfptr[7];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5098:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V8*/ meltfptr[7] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V7*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5099:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L3*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.PCTX__V4*/ meltfptr[3]),
			     (melt_ptr_t) (( /*!CLASS_PATTERN_EXPANSION_CONTEXT */ meltfrout->tabval[2])));;
      MELT_LOCATION ("warmelt-macro.melt:5099:/ cond");
      /*cond */ if ( /*_#IS_A__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V10*/ meltfptr[9] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5099:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check pctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5099) ? (5099) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V9*/ meltfptr[7] = /*_.IFELSE___V10*/ meltfptr[9];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5099:/ clear");
	     /*clear *//*_#IS_A__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V10*/ meltfptr[9] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V9*/ meltfptr[7] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5100:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L4*/ meltfnum[0] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5100:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L4*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5100:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5100;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_or sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V12*/ meltfptr[11] =
	      /*_.MELT_DEBUG_FUN__V13*/ meltfptr[12];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5100:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L5*/ meltfnum[4] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V13*/ meltfptr[12] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V12*/ meltfptr[11] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5100:/ quasiblock");


      /*_.PROGN___V14*/ meltfptr[12] = /*_.IF___V12*/ meltfptr[11];;
      /*^compute */
      /*_.IFCPP___V11*/ meltfptr[9] = /*_.PROGN___V14*/ meltfptr[12];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5100:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L4*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IF___V12*/ meltfptr[11] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V14*/ meltfptr[12] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V11*/ meltfptr[9] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5101:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5102:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V16*/ meltfptr[12] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5103:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V17*/ meltfptr[16] = slot;
    };
    ;
 /*_.LIST_FIRST__V18*/ meltfptr[17] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V16*/ meltfptr[12])));;
    /*^compute */
 /*_.CURPAIR__V19*/ meltfptr[18] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V18*/ meltfptr[17])));;
    MELT_LOCATION ("warmelt-macro.melt:5105:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr = (melt_ptr_t *) & /*_.PCTX__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.LOC__V17*/ meltfptr[16];
      /*_.ARGSP__V20*/ meltfptr[19] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PATTERNEXPAND_PAIRLIST_AS_TUPLE */ meltfrout->
		      tabval[4])),
		    (melt_ptr_t) ( /*_.CURPAIR__V19*/ meltfptr[18]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
    MELT_LOCATION ("warmelt-macro.melt:5106:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_LIST */
					     meltfrout->tabval[5])), (4),
			      "CLASS_SOURCE_PATTERN_LIST");
  /*_.INST__V22*/ meltfptr[21] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (1),
			  ( /*_.LOC__V17*/ meltfptr[16]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @CTPAT_SUBPA",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V22*/ meltfptr[21])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V22*/ meltfptr[21]), (3),
			  ( /*_.ARGSP__V20*/ meltfptr[19]), "CTPAT_SUBPA");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V22*/ meltfptr[21],
				  "newly made instance");
    ;
    /*_.RES__V21*/ meltfptr[20] = /*_.INST__V22*/ meltfptr[21];;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5110:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L6*/ meltfnum[4] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5110:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L6*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5110:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5110;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "patexpand_list res";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.RES__V21*/ meltfptr[20];
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V24*/ meltfptr[23] =
	      /*_.MELT_DEBUG_FUN__V25*/ meltfptr[24];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5110:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L7*/ meltfnum[0] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V25*/ meltfptr[24] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V24*/ meltfptr[23] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5110:/ quasiblock");


      /*_.PROGN___V26*/ meltfptr[24] = /*_.IF___V24*/ meltfptr[23];;
      /*^compute */
      /*_.IFCPP___V23*/ meltfptr[22] = /*_.PROGN___V26*/ meltfptr[24];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5110:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L6*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IF___V24*/ meltfptr[23] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V26*/ meltfptr[24] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V23*/ meltfptr[22] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5111:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.RES__V21*/ meltfptr[20];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5111:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V15*/ meltfptr[11] = /*_.RETURN___V27*/ meltfptr[23];;

    MELT_LOCATION ("warmelt-macro.melt:5101:/ clear");
	   /*clear *//*_.CONT__V16*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V17*/ meltfptr[16] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V18*/ meltfptr[17] = 0;
    /*^clear */
	   /*clear *//*_.CURPAIR__V19*/ meltfptr[18] = 0;
    /*^clear */
	   /*clear *//*_.ARGSP__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.RES__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V27*/ meltfptr[23] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5096:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V15*/ meltfptr[11];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5096:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V5*/ meltfptr[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V7*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V9*/ meltfptr[7] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V11*/ meltfptr[9] = 0;
    /*^clear */
	   /*clear *//*_.LET___V15*/ meltfptr[11] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("PATEXPAND_LIST", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_99_warmelt_macro_PATEXPAND_LIST_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_99_warmelt_macro_PATEXPAND_LIST */





melt_ptr_t MELT_MODULE_VISIBILITY
meltrout_100_warmelt_macro_MEXPAND_MATCH (meltclosure_ptr_t meltclosp_,
					  melt_ptr_t meltfirstargp_,
					  const melt_argdescr_cell_t
					  meltxargdescr_[],
					  union meltparam_un * meltxargtab_,
					  const melt_argdescr_cell_t
					  meltxresdescr_[],
					  union meltparam_un * meltxrestab_)
{
  long
    current_blocklevel_signals_meltrout_100_warmelt_macro_MEXPAND_MATCH_melt =
    melt_blocklevel_signals;

#if MELT_HAVE_DEBUG
  static long call_counter__;
  long thiscallcounter__ ATTRIBUTE_UNUSED = ++call_counter__;
#undef meltcallcount
#define meltcallcount thiscallcounter__
#else
#undef meltcallcount
#define meltcallcount 0L
#endif

  struct frame_meltrout_100_warmelt_macro_MEXPAND_MATCH_st
  {
    int mcfr_nbvar;
#if MELT_HAVE_DEBUG
    const char *mcfr_flocs;
#else				/*!MELT_HAVE_DEBUG */
    const char *mcfr_unusedflocs;
#endif				/*MELT_HAVE_DEBUG */
    struct meltclosure_st *mcfr_clos;
    struct excepth_melt_st *mcfr_exh;
    struct melt_callframe_st *mcfr_prev;
#undef MELTFRAM_NBVARPTR
#undef MELTFRAM_NBVARNUM
#define MELTFRAM_NBVARPTR 89
    melt_ptr_t mcfr_varptr[89];
#define MELTFRAM_NBVARNUM 27
    long mcfr_varnum[27];
/*others*/
    long _spare_;
  }
   *meltframptr_ = 0, meltfram__;	/*declfrastruct */
#define meltframe meltfram__
  /*meltrout_100_warmelt_macro_MEXPAND_MATCH is not initial declstructinit */
  if (MELT_UNLIKELY (meltxargdescr_ == MELTPAR_MARKGGC))
    {				/*mark for ggc */
      int ix = 0;
      meltframptr_ =
	(struct frame_meltrout_100_warmelt_macro_MEXPAND_MATCH_st *)
	meltfirstargp_;
      /* use arguments output_curframe_declstruct_init */
      (void) meltclosp_;
      (void) meltfirstargp_;
      (void) meltxargdescr_;
      (void) meltxargtab_;
      (void) meltxresdescr_;
      (void) meltxrestab_;
      gt_ggc_mx_melt_un (meltframptr_->mcfr_clos);
      for (ix = 0; ix < 89; ix++)
	if (meltframptr_->mcfr_varptr[ix])
	  gt_ggc_mx_melt_un (meltframptr_->mcfr_varptr[ix]);
      return NULL;
    } /*end markggc */ ;
  memset (&meltfram__, 0, sizeof (meltfram__));
/* declstructinit plain routine meltrout_100_warmelt_macro_MEXPAND_MATCH nbval 89*/
  meltfram__.mcfr_nbvar = 89 /*nbval */ ;
  meltfram__.mcfr_clos = meltclosp_;
  meltfram__.mcfr_prev = (struct melt_callframe_st *) melt_topframe;
  melt_topframe = (struct melt_callframe_st *) &meltfram__;
  melt_trace_start ("MEXPAND_MATCH", meltcallcount);
/*getargs*/

  /*getarg#0 */
  MELT_LOCATION ("warmelt-macro.melt:5127:/ getarg");
 /*_.SEXPR__V2*/ meltfptr[1] = (melt_ptr_t) meltfirstargp_;

  /*getarg#1 */
  /*^getarg */
  if (meltxargdescr_[0] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.ENV__V3*/ meltfptr[2] =
    (meltxargtab_[0].meltbp_aptr) ? (*(meltxargtab_[0].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2])) != NULL);


  /*getarg#2 */
  /*^getarg */
  if (meltxargdescr_[1] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MEXPANDER__V4*/ meltfptr[3] =
    (meltxargtab_[1].meltbp_aptr) ? (*(meltxargtab_[1].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3])) !=
	      NULL);


  /*getarg#3 */
  /*^getarg */
  if (meltxargdescr_[2] != MELTBPAR_PTR)
    goto lab_endgetargs;
 /*_.MODCTX__V5*/ meltfptr[4] =
    (meltxargtab_[2].meltbp_aptr) ? (*(meltxargtab_[2].meltbp_aptr)) : NULL;
  gcc_assert (melt_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) !=
	      NULL);

  ;
  goto lab_endgetargs;
lab_endgetargs:;
/*body*/
/*^block*/
/*anyblock*/
  {


#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5128:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L1*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]),
			     (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					    tabval[0])));;
      MELT_LOCATION ("warmelt-macro.melt:5128:/ cond");
      /*cond */ if ( /*_#IS_A__L1*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V7*/ meltfptr[6] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5128:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check sexpr"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5128) ? (5128) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V6*/ meltfptr[5] = /*_.IFELSE___V7*/ meltfptr[6];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5128:/ clear");
	     /*clear *//*_#IS_A__L1*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V7*/ meltfptr[6] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V6*/ meltfptr[5] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5129:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_A__L2*/ meltfnum[0] =
	melt_is_instance_of ((melt_ptr_t) ( /*_.ENV__V3*/ meltfptr[2]),
			     (melt_ptr_t) (( /*!CLASS_ENVIRONMENT */
					    meltfrout->tabval[1])));;
      MELT_LOCATION ("warmelt-macro.melt:5129:/ cond");
      /*cond */ if ( /*_#IS_A__L2*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V9*/ meltfptr[8] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5129:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check env"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5129) ? (5129) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V8*/ meltfptr[6] = /*_.IFELSE___V9*/ meltfptr[8];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5129:/ clear");
	     /*clear *//*_#IS_A__L2*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V9*/ meltfptr[8] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V8*/ meltfptr[6] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5130:/ cppif.then");
    /*^block */
    /*anyblock */
    {

      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#IS_OBJECT__L3*/ meltfnum[0] =
	(melt_magic_discr ((melt_ptr_t) ( /*_.MODCTX__V5*/ meltfptr[4])) ==
	 MELTOBMAG_OBJECT);;
      MELT_LOCATION ("warmelt-macro.melt:5130:/ cond");
      /*cond */ if ( /*_#IS_OBJECT__L3*/ meltfnum[0])	/*then */
	{
	  /*^cond.then */
	  /*_.IFELSE___V11*/ meltfptr[10] = ( /*nil */ NULL);;
	}
      else
	{
	  MELT_LOCATION ("warmelt-macro.melt:5130:/ cond.else");

	  /*^block */
	  /*anyblock */
	  {




	    {
	      /*^locexp */
	      melt_assert_failed (("check modctx"),
				  ("warmelt-macro.melt")
				  ? ("warmelt-macro.melt") : __FILE__,
				  (5130) ? (5130) : __LINE__, __FUNCTION__);
	      ;
	    }
	    ;
	       /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
	    /*epilog */
	  }
	  ;
	}
      ;
      /*^compute */
      /*_.IFCPP___V10*/ meltfptr[8] = /*_.IFELSE___V11*/ meltfptr[10];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5130:/ clear");
	     /*clear *//*_#IS_OBJECT__L3*/ meltfnum[0] = 0;
      /*^clear */
	     /*clear *//*_.IFELSE___V11*/ meltfptr[10] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V10*/ meltfptr[8] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5131:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
 /*_#NULL__L4*/ meltfnum[0] =
      (( /*_.MEXPANDER__V4*/ meltfptr[3]) == NULL);;
    MELT_LOCATION ("warmelt-macro.melt:5131:/ cond");
    /*cond */ if ( /*_#NULL__L4*/ meltfnum[0])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{

	  /*^compute */
	  /*_.MEXPANDER__V4*/ meltfptr[3] = /*_.SETQ___V13*/ meltfptr[12] =
	    ( /*!MACROEXPAND_1 */ meltfrout->tabval[2]);;
	  /*_.IF___V12*/ meltfptr[10] = /*_.SETQ___V13*/ meltfptr[12];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5131:/ clear");
	     /*clear *//*_.SETQ___V13*/ meltfptr[12] = 0;
	}
	;
      }
    else
      {				/*^cond.else */

  /*_.IF___V12*/ meltfptr[10] = NULL;;
      }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5132:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L5*/ meltfnum[4] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5132:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L5*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5132:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5132;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_match sexpr";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SEXPR__V2*/ meltfptr[1];
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V15*/ meltfptr[14] =
	      /*_.MELT_DEBUG_FUN__V16*/ meltfptr[15];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5132:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L6*/ meltfnum[5] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V16*/ meltfptr[15] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V15*/ meltfptr[14] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5132:/ quasiblock");


      /*_.PROGN___V17*/ meltfptr[15] = /*_.IF___V15*/ meltfptr[14];;
      /*^compute */
      /*_.IFCPP___V14*/ meltfptr[12] = /*_.PROGN___V17*/ meltfptr[15];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5132:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L5*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IF___V15*/ meltfptr[14] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V17*/ meltfptr[15] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V14*/ meltfptr[12] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5133:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5134:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
  /*_.CONT__V19*/ meltfptr[15] = slot;
    };
    ;
    MELT_LOCATION ("warmelt-macro.melt:5135:/ getslot");
    {
      melt_ptr_t slot = NULL, obj = NULL;
      obj = (melt_ptr_t) ( /*_.SEXPR__V2*/ meltfptr[1]) /*=obj*/ ;
      melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
  /*_.LOC__V20*/ meltfptr[19] = slot;
    };
    ;
 /*_.LIST_FIRST__V21*/ meltfptr[20] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[15])));;
    /*^compute */
 /*_.PAIR_TAIL__V22*/ meltfptr[21] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V21*/ meltfptr[20])));;
    /*^compute */
 /*_.MSEXP__V23*/ meltfptr[22] =
      (melt_pair_head ((melt_ptr_t) ( /*_.PAIR_TAIL__V22*/ meltfptr[21])));;
    MELT_LOCATION ("warmelt-macro.melt:5137:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[3];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr = (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
      /*^apply.arg */
      argtab[2].meltbp_aptr = (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
      /*_.MATSX__V24*/ meltfptr[23] =
	melt_apply ((meltclosure_ptr_t) ( /*_.MEXPANDER__V4*/ meltfptr[3]),
		    (melt_ptr_t) ( /*_.MSEXP__V23*/ meltfptr[22]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR ""),
		    argtab, "", (union meltparam_un *) 0);
    }
    ;
 /*_.LIST_FIRST__V25*/ meltfptr[24] =
      (melt_list_first ((melt_ptr_t) ( /*_.CONT__V19*/ meltfptr[15])));;
    /*^compute */
 /*_.PAIR_TAIL__V26*/ meltfptr[25] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.LIST_FIRST__V25*/ meltfptr[24])));;
    /*^compute */
 /*_.PAIR_TAIL__V27*/ meltfptr[26] =
      (melt_pair_tail ((melt_ptr_t) ( /*_.PAIR_TAIL__V26*/ meltfptr[25])));;
    MELT_LOCATION ("warmelt-macro.melt:5141:/ quasiblock");


    /*^newclosure */
		 /*newclosure *//*_.LAMBDA___V29*/ meltfptr[28] =
      (melt_ptr_t)
      meltgc_new_closure ((meltobject_ptr_t)
			  (((melt_ptr_t) (MELT_PREDEF (DISCR_CLOSURE)))),
			  (meltroutine_ptr_t) (( /*!konst_7 */ meltfrout->
						tabval[7])), (1));
    ;
    /*^putclosedv */
    /*putclosv */
    melt_assertmsg ("putclosv checkclo",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.LAMBDA___V29*/ meltfptr[28])) ==
		    MELTOBMAG_CLOSURE);
    melt_assertmsg ("putclosv checkoff", 0 >= 0
		    && 0 <
		    melt_closure_size ((melt_ptr_t)
				       ( /*_.LAMBDA___V29*/ meltfptr[28])));
    ((meltclosure_ptr_t) /*_.LAMBDA___V29*/ meltfptr[28])->tabval[0] =
      (melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]);
    ;
    /*_.LAMBDA___V28*/ meltfptr[27] = /*_.LAMBDA___V29*/ meltfptr[28];;
    MELT_LOCATION ("warmelt-macro.melt:5138:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[2];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[5]);
      /*^apply.arg */
      argtab[1].meltbp_aptr =
	(melt_ptr_t *) & /*_.LAMBDA___V28*/ meltfptr[27];
      /*_.MEXPTUPLE__V30*/ meltfptr[29] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!PAIRLIST_TO_MULTIPLE */ meltfrout->tabval[4])),
		    (melt_ptr_t) ( /*_.PAIR_TAIL__V27*/ meltfptr[26]),
		    (MELTBPARSTR_PTR MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_#NBMATCH__L7*/ meltfnum[5] =
      (melt_multiple_length
       ((melt_ptr_t) ( /*_.MEXPTUPLE__V30*/ meltfptr[29])));;
    MELT_LOCATION ("warmelt-macro.melt:5147:/ quasiblock");


 /*_.LX__V32*/ meltfptr[31] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.MEXPTUPLE__V30*/ meltfptr[29]), (-1)));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5148:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L8*/ meltfnum[4] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5148:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L8*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5148:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long = /*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5148;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_match lastmexp lx";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.LX__V32*/ meltfptr[31];
	      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V34*/ meltfptr[33] =
	      /*_.MELT_DEBUG_FUN__V35*/ meltfptr[34];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5148:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L9*/ meltfnum[8] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V35*/ meltfptr[34] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V34*/ meltfptr[33] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5148:/ quasiblock");


      /*_.PROGN___V36*/ meltfptr[34] = /*_.IF___V34*/ meltfptr[33];;
      /*^compute */
      /*_.IFCPP___V33*/ meltfptr[32] = /*_.PROGN___V36*/ meltfptr[34];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5148:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L8*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IF___V34*/ meltfptr[33] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V36*/ meltfptr[34] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V33*/ meltfptr[32] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    /*^compute */
    /*_.LET___V31*/ meltfptr[30] = /*_.LX__V32*/ meltfptr[31];;

    MELT_LOCATION ("warmelt-macro.melt:5147:/ clear");
	   /*clear *//*_.LX__V32*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V33*/ meltfptr[32] = 0;
    /*_.LASTMEXP__V37*/ meltfptr[33] = /*_.LET___V31*/ meltfptr[30];;
    /*^compute */
 /*_#IX__L10*/ meltfnum[8] =
      (( /*_#NBMATCH__L7*/ meltfnum[5]) - (1));;
    /*^compute */
 /*_.CASELIST__V38*/ meltfptr[34] =
      (meltgc_new_list
       ((meltobject_ptr_t) (( /*!DISCR_LIST */ meltfrout->tabval[8]))));;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5153:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L11*/ meltfnum[4] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5153:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L11*/ meltfnum[4])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5153:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5153;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_match mexptuple";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.MEXPTUPLE__V30*/ meltfptr[29];
	      /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V40*/ meltfptr[32] =
	      /*_.MELT_DEBUG_FUN__V41*/ meltfptr[40];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5153:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L12*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V41*/ meltfptr[40] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V40*/ meltfptr[32] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5153:/ quasiblock");


      /*_.PROGN___V42*/ meltfptr[40] = /*_.IF___V40*/ meltfptr[32];;
      /*^compute */
      /*_.IFCPP___V39*/ meltfptr[31] = /*_.PROGN___V42*/ meltfptr[40];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5153:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L11*/ meltfnum[4] = 0;
      /*^clear */
	     /*clear *//*_.IF___V40*/ meltfptr[32] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V42*/ meltfptr[40] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V39*/ meltfptr[31] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5154:/ loop");
    /*loop */
    {
    labloop_MATCHLOOP_1:;	/*^loopbody */

      /*^block */
      /*anyblock */
      {

	/*^checksignal */
	MELT_CHECK_SIGNAL ();
	;
	MELT_LOCATION ("warmelt-macro.melt:5155:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#I__L13*/ meltfnum[11] =
	  (( /*_#IX__L10*/ meltfnum[8]) < (0));;
	MELT_LOCATION ("warmelt-macro.melt:5155:/ cond");
	/*cond */ if ( /*_#I__L13*/ meltfnum[11])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      /*^quasiblock */


	      /*^compute */
     /*_.MATCHLOOP__V44*/ meltfptr[40] = NULL;;

	      /*^exit */
	      /*exit */
	      {
		goto labexit_MATCHLOOP_1;
	      }
	      ;
	      /*epilog */
	    }
	    ;
	  }			/*noelse */
	;
	MELT_LOCATION ("warmelt-macro.melt:5156:/ quasiblock");


   /*_.CURMATCH__V45*/ meltfptr[44] =
	  (melt_multiple_nth
	   ((melt_ptr_t) ( /*_.MEXPTUPLE__V30*/ meltfptr[29]),
	    ( /*_#IX__L10*/ meltfnum[8])));;

#if MELT_HAVE_DEBUG
	MELT_LOCATION ("warmelt-macro.melt:5157:/ cppif.then");
	/*^block */
	/*anyblock */
	{


	  {
	    /*^locexp */
	    /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	    melt_dbgcounter++;
#endif
	    ;
	  }
	  ;
	  /*^checksignal */
	  MELT_CHECK_SIGNAL ();
	  ;
     /*_#MELT_NEED_DBG__L14*/ meltfnum[4] =
	    /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	    ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	    0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	    ;;
	  MELT_LOCATION ("warmelt-macro.melt:5157:/ cond");
	  /*cond */ if ( /*_#MELT_NEED_DBG__L14*/ meltfnum[4])	/*then */
	    {
	      /*^cond.then */
	      /*^block */
	      /*anyblock */
	      {

       /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14] =
#ifdef meltcallcount
		  meltcallcount	/* the_meltcallcount */
#else
		  0L
#endif /* meltcallcount the_meltcallcount */
		  ;;
		MELT_LOCATION ("warmelt-macro.melt:5157:/ checksignal");
		MELT_CHECK_SIGNAL ();
		;
		/*^apply */
		/*apply */
		{
		  union meltparam_un argtab[5];
		  memset (&argtab, 0, sizeof (argtab));
		  /*^apply.arg */
		  argtab[0].meltbp_long =
		    /*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14];
		  /*^apply.arg */
		  argtab[1].meltbp_cstring = "warmelt-macro.melt";
		  /*^apply.arg */
		  argtab[2].meltbp_long = 5157;
		  /*^apply.arg */
		  argtab[3].meltbp_cstring = "mexpand_match curmatch";
		  /*^apply.arg */
		  argtab[4].meltbp_aptr =
		    (melt_ptr_t *) & /*_.CURMATCH__V45*/ meltfptr[44];
		  /*_.MELT_DEBUG_FUN__V48*/ meltfptr[47] =
		    melt_apply ((meltclosure_ptr_t)
				(( /*!MELT_DEBUG_FUN */ meltfrout->
				  tabval[3])),
				(melt_ptr_t) (( /*nil */ NULL)),
				(MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				 MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				 MELTBPARSTR_PTR ""), argtab, "",
				(union meltparam_un *) 0);
		}
		;
		/*_.IF___V47*/ meltfptr[46] =
		  /*_.MELT_DEBUG_FUN__V48*/ meltfptr[47];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5157:/ clear");
		 /*clear *//*_#THE_MELTCALLCOUNT__L15*/ meltfnum[14] = 0;
		/*^clear */
		 /*clear *//*_.MELT_DEBUG_FUN__V48*/ meltfptr[47] = 0;
	      }
	      ;
	    }
	  else
	    {			/*^cond.else */

      /*_.IF___V47*/ meltfptr[46] = NULL;;
	    }
	  ;
	  MELT_LOCATION ("warmelt-macro.melt:5157:/ quasiblock");


	  /*_.PROGN___V49*/ meltfptr[47] = /*_.IF___V47*/ meltfptr[46];;
	  /*^compute */
	  /*_.IFCPP___V46*/ meltfptr[45] = /*_.PROGN___V49*/ meltfptr[47];;
	  /*epilog */

	  MELT_LOCATION ("warmelt-macro.melt:5157:/ clear");
	       /*clear *//*_#MELT_NEED_DBG__L14*/ meltfnum[4] = 0;
	  /*^clear */
	       /*clear *//*_.IF___V47*/ meltfptr[46] = 0;
	  /*^clear */
	       /*clear *//*_.PROGN___V49*/ meltfptr[47] = 0;
	}

#else /*MELT_HAVE_DEBUG */
	/*^cppif.else */
	/*_.IFCPP___V46*/ meltfptr[45] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	;
	MELT_LOCATION ("warmelt-macro.melt:5160:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
   /*_#IS_A__L16*/ meltfnum[14] =
	  melt_is_instance_of ((melt_ptr_t)
			       ( /*_.CURMATCH__V45*/ meltfptr[44]),
			       (melt_ptr_t) (( /*!CLASS_SEXPR */ meltfrout->
					      tabval[0])));;
	MELT_LOCATION ("warmelt-macro.melt:5160:/ cond");
	/*cond */ if ( /*_#IS_A__L16*/ meltfnum[14])	/*then */
	  {
	    /*^cond.then */
	    /*^block */
	    /*anyblock */
	    {

	      MELT_LOCATION ("warmelt-macro.melt:5161:/ quasiblock");


	      /*^getslot */
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURMATCH__V45*/ meltfptr[44]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 2, "SEXP_CONTENTS");
      /*_.CURMATCHCONT__V50*/ meltfptr[46] = slot;
	      };
	      ;
	      MELT_LOCATION ("warmelt-macro.melt:5162:/ getslot");
	      {
		melt_ptr_t slot = NULL, obj = NULL;
		obj =
		  (melt_ptr_t) ( /*_.CURMATCH__V45*/ meltfptr[44]) /*=obj*/ ;
		melt_object_get_field (slot, obj, 1, "LOCA_LOCATION");
      /*_.CURMATCHLOC__V51*/ meltfptr[47] = slot;
	      };
	      ;
     /*_.LIST_FIRST__V52*/ meltfptr[51] =
		(melt_list_first
		 ((melt_ptr_t) ( /*_.CURMATCHCONT__V50*/ meltfptr[46])));;
	      /*^compute */
     /*_.CURMATCHPATX__V53*/ meltfptr[52] =
		(melt_pair_head
		 ((melt_ptr_t) ( /*_.LIST_FIRST__V52*/ meltfptr[51])));;
	      /*^compute */
     /*_.LIST_FIRST__V54*/ meltfptr[53] =
		(melt_list_first
		 ((melt_ptr_t) ( /*_.CURMATCHCONT__V50*/ meltfptr[46])));;
	      /*^compute */
     /*_.CURMATCHRESTPAIRS__V55*/ meltfptr[54] =
		(melt_pair_tail
		 ((melt_ptr_t) ( /*_.LIST_FIRST__V54*/ meltfptr[53])));;
	      MELT_LOCATION ("warmelt-macro.melt:5165:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
     /*_.MAKE_MAPOBJECT__V56*/ meltfptr[55] =
		(meltgc_new_mapobjects
		 ((meltobject_ptr_t)
		  (( /*!DISCR_MAP_OBJECTS */ meltfrout->tabval[11])), (17)));;
	      MELT_LOCATION ("warmelt-macro.melt:5165:/ quasiblock");


	      /*^rawallocobj */
	      /*rawallocobj */
	      {
		melt_ptr_t newobj = 0;
		melt_raw_object_create (newobj,
					(melt_ptr_t) (( /*!CLASS_PATTERN_EXPANSION_CONTEXT */ meltfrout->tabval[9])), (4), "CLASS_PATTERN_EXPANSION_CONTEXT");
      /*_.INST__V58*/ meltfptr[57] =
		  newobj;
	      };
	      ;
	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @PCTX_MEXPANDER",
			      melt_magic_discr ((melt_ptr_t)
						( /*_.INST__V58*/
						 meltfptr[57])) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object (( /*_.INST__V58*/ meltfptr[57]), (0),
				    ( /*_.MEXPANDER__V4*/ meltfptr[3]),
				    "PCTX_MEXPANDER");
	      ;
	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @PCTX_PEXPANDER",
			      melt_magic_discr ((melt_ptr_t)
						( /*_.INST__V58*/
						 meltfptr[57])) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object (( /*_.INST__V58*/ meltfptr[57]), (1),
				    (( /*!PATTERNEXPAND_1 */ meltfrout->
				      tabval[10])), "PCTX_PEXPANDER");
	      ;
	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @PCTX_VARMAP",
			      melt_magic_discr ((melt_ptr_t)
						( /*_.INST__V58*/
						 meltfptr[57])) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object (( /*_.INST__V58*/ meltfptr[57]), (2),
				    ( /*_.MAKE_MAPOBJECT__V56*/ meltfptr[55]),
				    "PCTX_VARMAP");
	      ;
	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @PCTX_MODCTX",
			      melt_magic_discr ((melt_ptr_t)
						( /*_.INST__V58*/
						 meltfptr[57])) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object (( /*_.INST__V58*/ meltfptr[57]), (3),
				    ( /*_.MODCTX__V5*/ meltfptr[4]),
				    "PCTX_MODCTX");
	      ;
	      /*^touchobj */

	      melt_dbgtrace_written_object ( /*_.INST__V58*/ meltfptr[57],
					    "newly made instance");
	      ;
	      /*_.CURPATCTX__V57*/ meltfptr[56] =
		/*_.INST__V58*/ meltfptr[57];;
	      MELT_LOCATION ("warmelt-macro.melt:5171:/ quasiblock");


	      /*^checksignal */
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		union meltparam_un argtab[3];
		memset (&argtab, 0, sizeof (argtab));
		/*^apply.arg */
		argtab[0].meltbp_aptr =
		  (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
		/*^apply.arg */
		argtab[1].meltbp_aptr =
		  (melt_ptr_t *) & /*_.CURPATCTX__V57*/ meltfptr[56];
		/*^apply.arg */
		argtab[2].meltbp_aptr =
		  (melt_ptr_t *) & /*_.CURMATCHLOC__V51*/ meltfptr[47];
		/*_.CP__V60*/ meltfptr[59] =
		  melt_apply ((meltclosure_ptr_t)
			      (( /*!PATTERNEXPAND_1 */ meltfrout->
				tabval[10])),
			      (melt_ptr_t) ( /*_.CURMATCHPATX__V53*/
					    meltfptr[52]),
			      (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			       ""), argtab, "", (union meltparam_un *) 0);
	      }
	      ;

#if MELT_HAVE_DEBUG
	      MELT_LOCATION ("warmelt-macro.melt:5172:/ cppif.then");
	      /*^block */
	      /*anyblock */
	      {


		{
		  /*^locexp */
		  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		  melt_dbgcounter++;
#endif
		  ;
		}
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
       /*_#MELT_NEED_DBG__L17*/ meltfnum[4] =
		  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		  0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		  ;;
		MELT_LOCATION ("warmelt-macro.melt:5172:/ cond");
		/*cond */ if ( /*_#MELT_NEED_DBG__L17*/ meltfnum[4])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

	 /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17] =
#ifdef meltcallcount
			meltcallcount	/* the_meltcallcount */
#else
			0L
#endif /* meltcallcount the_meltcallcount */
			;;
		      MELT_LOCATION ("warmelt-macro.melt:5172:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[5];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_long =
			  /*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17];
			/*^apply.arg */
			argtab[1].meltbp_cstring = "warmelt-macro.melt";
			/*^apply.arg */
			argtab[2].meltbp_long = 5172;
			/*^apply.arg */
			argtab[3].meltbp_cstring = "mexpand_match curpat";
			/*^apply.arg */
			argtab[4].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CP__V60*/ meltfptr[59];
			/*_.MELT_DEBUG_FUN__V63*/ meltfptr[62] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!MELT_DEBUG_FUN */ meltfrout->
					tabval[3])),
				      (melt_ptr_t) (( /*nil */ NULL)),
				      (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				       MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IF___V62*/ meltfptr[61] =
			/*_.MELT_DEBUG_FUN__V63*/ meltfptr[62];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5172:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L18*/ meltfnum[17] = 0;
		      /*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V63*/ meltfptr[62] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

	/*_.IF___V62*/ meltfptr[61] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-macro.melt:5172:/ quasiblock");


		/*_.PROGN___V64*/ meltfptr[62] = /*_.IF___V62*/ meltfptr[61];;
		/*^compute */
		/*_.IFCPP___V61*/ meltfptr[60] =
		  /*_.PROGN___V64*/ meltfptr[62];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5172:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L17*/ meltfnum[4] = 0;
		/*^clear */
		 /*clear *//*_.IF___V62*/ meltfptr[61] = 0;
		/*^clear */
		 /*clear *//*_.PROGN___V64*/ meltfptr[62] = 0;
	      }

#else /*MELT_HAVE_DEBUG */
	      /*^cppif.else */
	      /*_.IFCPP___V61*/ meltfptr[60] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	      ;

#if MELT_HAVE_DEBUG
	      MELT_LOCATION ("warmelt-macro.melt:5173:/ cppif.then");
	      /*^block */
	      /*anyblock */
	      {

		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
		/*^cond */
		/*cond */ if ( /*_.CP__V60*/ meltfptr[59])	/*then */
		  {
		    /*^cond.then */
		    /*_.IFELSE___V66*/ meltfptr[62] = ( /*nil */ NULL);;
		  }
		else
		  {
		    MELT_LOCATION ("warmelt-macro.melt:5173:/ cond.else");

		    /*^block */
		    /*anyblock */
		    {




		      {
			/*^locexp */
			melt_assert_failed (("check curpat"),
					    ("warmelt-macro.melt")
					    ? ("warmelt-macro.melt") :
					    __FILE__,
					    (5173) ? (5173) : __LINE__,
					    __FUNCTION__);
			;
		      }
		      ;
		   /*clear *//*_.IFELSE___V66*/ meltfptr[62] = 0;
		      /*epilog */
		    }
		    ;
		  }
		;
		/*^compute */
		/*_.IFCPP___V65*/ meltfptr[61] =
		  /*_.IFELSE___V66*/ meltfptr[62];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5173:/ clear");
		 /*clear *//*_.IFELSE___V66*/ meltfptr[62] = 0;
	      }

#else /*MELT_HAVE_DEBUG */
	      /*^cppif.else */
	      /*_.IFCPP___V65*/ meltfptr[61] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	      ;
	      /*^compute */
	      /*_.LET___V59*/ meltfptr[58] = /*_.CP__V60*/ meltfptr[59];;

	      MELT_LOCATION ("warmelt-macro.melt:5171:/ clear");
	       /*clear *//*_.CP__V60*/ meltfptr[59] = 0;
	      /*^clear */
	       /*clear *//*_.IFCPP___V61*/ meltfptr[60] = 0;
	      /*^clear */
	       /*clear *//*_.IFCPP___V65*/ meltfptr[61] = 0;
	      /*_.CURPAT__V67*/ meltfptr[62] = /*_.LET___V59*/ meltfptr[58];;
	      MELT_LOCATION ("warmelt-macro.melt:5175:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^apply */
	      /*apply */
	      {
		union meltparam_un argtab[3];
		memset (&argtab, 0, sizeof (argtab));
		/*^apply.arg */
		argtab[0].meltbp_aptr =
		  (melt_ptr_t *) & /*_.ENV__V3*/ meltfptr[2];
		/*^apply.arg */
		argtab[1].meltbp_aptr =
		  (melt_ptr_t *) & /*_.MEXPANDER__V4*/ meltfptr[3];
		/*^apply.arg */
		argtab[2].meltbp_aptr =
		  (melt_ptr_t *) & /*_.MODCTX__V5*/ meltfptr[4];
		/*_.CURBODY__V68*/ meltfptr[59] =
		  melt_apply ((meltclosure_ptr_t)
			      (( /*!EXPAND_PAIRLIST_AS_TUPLE */ meltfrout->
				tabval[12])),
			      (melt_ptr_t) ( /*_.CURMATCHRESTPAIRS__V55*/
					    meltfptr[54]),
			      (MELTBPARSTR_PTR MELTBPARSTR_PTR MELTBPARSTR_PTR
			       ""), argtab, "", (union meltparam_un *) 0);
	      }
	      ;
	      MELT_LOCATION ("warmelt-macro.melt:5176:/ checksignal");
	      MELT_CHECK_SIGNAL ();
	      ;
	      /*^quasiblock */


	      /*^rawallocobj */
	      /*rawallocobj */
	      {
		melt_ptr_t newobj = 0;
		melt_raw_object_create (newobj,
					(melt_ptr_t) (( /*!CLASS_SOURCE_MATCH_CASE */ meltfrout->tabval[13])), (4), "CLASS_SOURCE_MATCH_CASE");
      /*_.INST__V70*/ meltfptr[61] =
		  newobj;
	      };
	      ;
	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
			      melt_magic_discr ((melt_ptr_t)
						( /*_.INST__V70*/
						 meltfptr[61])) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object (( /*_.INST__V70*/ meltfptr[61]), (1),
				    ( /*_.CURMATCHLOC__V51*/ meltfptr[47]),
				    "LOCA_LOCATION");
	      ;
	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @SCAM_PATT",
			      melt_magic_discr ((melt_ptr_t)
						( /*_.INST__V70*/
						 meltfptr[61])) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object (( /*_.INST__V70*/ meltfptr[61]), (2),
				    ( /*_.CURPAT__V67*/ meltfptr[62]),
				    "SCAM_PATT");
	      ;
	      /*^putslot */
	      /*putslot */
	      melt_assertmsg ("putslot checkobj @SCAM_BODY",
			      melt_magic_discr ((melt_ptr_t)
						( /*_.INST__V70*/
						 meltfptr[61])) ==
			      MELTOBMAG_OBJECT);
	      melt_putfield_object (( /*_.INST__V70*/ meltfptr[61]), (3),
				    ( /*_.CURBODY__V68*/ meltfptr[59]),
				    "SCAM_BODY");
	      ;
	      /*^touchobj */

	      melt_dbgtrace_written_object ( /*_.INST__V70*/ meltfptr[61],
					    "newly made instance");
	      ;
	      /*_.CURCASE__V69*/ meltfptr[60] = /*_.INST__V70*/ meltfptr[61];;

#if MELT_HAVE_DEBUG
	      MELT_LOCATION ("warmelt-macro.melt:5181:/ cppif.then");
	      /*^block */
	      /*anyblock */
	      {


		{
		  /*^locexp */
		  /*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
		  melt_dbgcounter++;
#endif
		  ;
		}
		;
		/*^checksignal */
		MELT_CHECK_SIGNAL ();
		;
       /*_#MELT_NEED_DBG__L19*/ meltfnum[17] =
		  /*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
		  ( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
		  0		/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
		  ;;
		MELT_LOCATION ("warmelt-macro.melt:5181:/ cond");
		/*cond */ if ( /*_#MELT_NEED_DBG__L19*/ meltfnum[17])	/*then */
		  {
		    /*^cond.then */
		    /*^block */
		    /*anyblock */
		    {

	 /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[4] =
#ifdef meltcallcount
			meltcallcount	/* the_meltcallcount */
#else
			0L
#endif /* meltcallcount the_meltcallcount */
			;;
		      MELT_LOCATION ("warmelt-macro.melt:5181:/ checksignal");
		      MELT_CHECK_SIGNAL ();
		      ;
		      /*^apply */
		      /*apply */
		      {
			union meltparam_un argtab[5];
			memset (&argtab, 0, sizeof (argtab));
			/*^apply.arg */
			argtab[0].meltbp_long =
			  /*_#THE_MELTCALLCOUNT__L20*/ meltfnum[4];
			/*^apply.arg */
			argtab[1].meltbp_cstring = "warmelt-macro.melt";
			/*^apply.arg */
			argtab[2].meltbp_long = 5181;
			/*^apply.arg */
			argtab[3].meltbp_cstring = "mexpand_match curcase";
			/*^apply.arg */
			argtab[4].meltbp_aptr =
			  (melt_ptr_t *) & /*_.CURCASE__V69*/ meltfptr[60];
			/*_.MELT_DEBUG_FUN__V73*/ meltfptr[72] =
			  melt_apply ((meltclosure_ptr_t)
				      (( /*!MELT_DEBUG_FUN */ meltfrout->
					tabval[3])),
				      (melt_ptr_t) (( /*nil */ NULL)),
				      (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				       MELTBPARSTR_LONG MELTBPARSTR_CSTRING
				       MELTBPARSTR_PTR ""), argtab, "",
				      (union meltparam_un *) 0);
		      }
		      ;
		      /*_.IF___V72*/ meltfptr[71] =
			/*_.MELT_DEBUG_FUN__V73*/ meltfptr[72];;
		      /*epilog */

		      MELT_LOCATION ("warmelt-macro.melt:5181:/ clear");
		   /*clear *//*_#THE_MELTCALLCOUNT__L20*/ meltfnum[4] = 0;
		      /*^clear */
		   /*clear *//*_.MELT_DEBUG_FUN__V73*/ meltfptr[72] = 0;
		    }
		    ;
		  }
		else
		  {		/*^cond.else */

	/*_.IF___V72*/ meltfptr[71] = NULL;;
		  }
		;
		MELT_LOCATION ("warmelt-macro.melt:5181:/ quasiblock");


		/*_.PROGN___V74*/ meltfptr[72] = /*_.IF___V72*/ meltfptr[71];;
		/*^compute */
		/*_.IFCPP___V71*/ meltfptr[70] =
		  /*_.PROGN___V74*/ meltfptr[72];;
		/*epilog */

		MELT_LOCATION ("warmelt-macro.melt:5181:/ clear");
		 /*clear *//*_#MELT_NEED_DBG__L19*/ meltfnum[17] = 0;
		/*^clear */
		 /*clear *//*_.IF___V72*/ meltfptr[71] = 0;
		/*^clear */
		 /*clear *//*_.PROGN___V74*/ meltfptr[72] = 0;
	      }

#else /*MELT_HAVE_DEBUG */
	      /*^cppif.else */
	      /*_.IFCPP___V71*/ meltfptr[70] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
	      ;

	      {
		MELT_LOCATION ("warmelt-macro.melt:5182:/ locexp");
		meltgc_prepend_list ((melt_ptr_t)
				     ( /*_.CASELIST__V38*/ meltfptr[34]),
				     (melt_ptr_t) ( /*_.CURCASE__V69*/
						   meltfptr[60]));
	      }
	      ;

	      MELT_LOCATION ("warmelt-macro.melt:5161:/ clear");
	       /*clear *//*_.CURMATCHCONT__V50*/ meltfptr[46] = 0;
	      /*^clear */
	       /*clear *//*_.CURMATCHLOC__V51*/ meltfptr[47] = 0;
	      /*^clear */
	       /*clear *//*_.LIST_FIRST__V52*/ meltfptr[51] = 0;
	      /*^clear */
	       /*clear *//*_.CURMATCHPATX__V53*/ meltfptr[52] = 0;
	      /*^clear */
	       /*clear *//*_.LIST_FIRST__V54*/ meltfptr[53] = 0;
	      /*^clear */
	       /*clear *//*_.CURMATCHRESTPAIRS__V55*/ meltfptr[54] = 0;
	      /*^clear */
	       /*clear *//*_.MAKE_MAPOBJECT__V56*/ meltfptr[55] = 0;
	      /*^clear */
	       /*clear *//*_.CURPATCTX__V57*/ meltfptr[56] = 0;
	      /*^clear */
	       /*clear *//*_.LET___V59*/ meltfptr[58] = 0;
	      /*^clear */
	       /*clear *//*_.CURPAT__V67*/ meltfptr[62] = 0;
	      /*^clear */
	       /*clear *//*_.CURBODY__V68*/ meltfptr[59] = 0;
	      /*^clear */
	       /*clear *//*_.CURCASE__V69*/ meltfptr[60] = 0;
	      /*^clear */
	       /*clear *//*_.IFCPP___V71*/ meltfptr[70] = 0;
	      /*epilog */
	    }
	    ;
	  }
	else
	  {
	    MELT_LOCATION ("warmelt-macro.melt:5160:/ cond.else");

	    /*^block */
	    /*anyblock */
	    {


	      {
		MELT_LOCATION ("warmelt-macro.melt:5186:/ locexp");
		/* error_plain */
		  melt_error_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
				  ("(MATCH <expr> <match-case>...) with non sexpr matchcase"),
				  (melt_ptr_t) 0);
	      }
	      ;
	      MELT_LOCATION ("warmelt-macro.melt:5185:/ quasiblock");


	      /*epilog */
	    }
	    ;
	  }
	;

	MELT_LOCATION ("warmelt-macro.melt:5156:/ clear");
	     /*clear *//*_.CURMATCH__V45*/ meltfptr[44] = 0;
	/*^clear */
	     /*clear *//*_.IFCPP___V46*/ meltfptr[45] = 0;
	/*^clear */
	     /*clear *//*_#IS_A__L16*/ meltfnum[14] = 0;
   /*_#I__L21*/ meltfnum[4] =
	  (( /*_#IX__L10*/ meltfnum[8]) - (1));;
	MELT_LOCATION ("warmelt-macro.melt:5188:/ compute");
	/*_#IX__L10*/ meltfnum[8] = /*_#SETQ___L22*/ meltfnum[17] =
	  /*_#I__L21*/ meltfnum[4];;
	MELT_LOCATION ("warmelt-macro.melt:5154:/ checksignal");
	MELT_CHECK_SIGNAL ();
	;
	/*epilog */

	/*^clear */
	     /*clear *//*_#I__L13*/ meltfnum[11] = 0;
	/*^clear */
	     /*clear *//*_#I__L21*/ meltfnum[4] = 0;
	/*^clear */
	     /*clear *//*_#SETQ___L22*/ meltfnum[17] = 0;
      }
      ;
      ;
      goto labloop_MATCHLOOP_1;
    labexit_MATCHLOOP_1:;	/*^loopepilog */
      /*loopepilog */
      /*_.FOREVER___V43*/ meltfptr[32] = /*_.MATCHLOOP__V44*/ meltfptr[40];;
    }
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5189:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L23*/ meltfnum[14] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5189:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L23*/ meltfnum[14])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L24*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5189:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L24*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5189;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_match caselist";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.CASELIST__V38*/ meltfptr[34];
	      /*_.MELT_DEBUG_FUN__V77*/ meltfptr[46] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V76*/ meltfptr[72] =
	      /*_.MELT_DEBUG_FUN__V77*/ meltfptr[46];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5189:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L24*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V77*/ meltfptr[46] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V76*/ meltfptr[72] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5189:/ quasiblock");


      /*_.PROGN___V78*/ meltfptr[47] = /*_.IF___V76*/ meltfptr[72];;
      /*^compute */
      /*_.IFCPP___V75*/ meltfptr[71] = /*_.PROGN___V78*/ meltfptr[47];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5189:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L23*/ meltfnum[14] = 0;
      /*^clear */
	     /*clear *//*_.IF___V76*/ meltfptr[72] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V78*/ meltfptr[47] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V75*/ meltfptr[71] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5190:/ quasiblock");


    MELT_LOCATION ("warmelt-macro.melt:5191:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^apply */
    /*apply */
    {
      union meltparam_un argtab[1];
      memset (&argtab, 0, sizeof (argtab));
      /*^apply.arg */
      argtab[0].meltbp_aptr =
	(melt_ptr_t *) & ( /*!DISCR_MULTIPLE */ meltfrout->tabval[5]);
      /*_.CASETUPL__V80*/ meltfptr[52] =
	melt_apply ((meltclosure_ptr_t)
		    (( /*!LIST_TO_MULTIPLE */ meltfrout->tabval[14])),
		    (melt_ptr_t) ( /*_.CASELIST__V38*/ meltfptr[34]),
		    (MELTBPARSTR_PTR ""), argtab, "",
		    (union meltparam_un *) 0);
    }
    ;
 /*_.LASTCASE__V81*/ meltfptr[53] =
      (melt_multiple_nth
       ((melt_ptr_t) ( /*_.CASETUPL__V80*/ meltfptr[52]), (-1)));;
    MELT_LOCATION ("warmelt-macro.melt:5193:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*^rawallocobj */
    /*rawallocobj */
    {
      melt_ptr_t newobj = 0;
      melt_raw_object_create (newobj,
			      (melt_ptr_t) (( /*!CLASS_SOURCE_MATCH */
					     meltfrout->tabval[15])), (4),
			      "CLASS_SOURCE_MATCH");
  /*_.INST__V83*/ meltfptr[55] =
	newobj;
    };
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @LOCA_LOCATION",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V83*/ meltfptr[55])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V83*/ meltfptr[55]), (1),
			  ( /*_.LOC__V20*/ meltfptr[19]), "LOCA_LOCATION");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SMAT_MATCHEDX",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V83*/ meltfptr[55])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V83*/ meltfptr[55]), (2),
			  ( /*_.MATSX__V24*/ meltfptr[23]), "SMAT_MATCHEDX");
    ;
    /*^putslot */
    /*putslot */
    melt_assertmsg ("putslot checkobj @SMAT_CASES",
		    melt_magic_discr ((melt_ptr_t)
				      ( /*_.INST__V83*/ meltfptr[55])) ==
		    MELTOBMAG_OBJECT);
    melt_putfield_object (( /*_.INST__V83*/ meltfptr[55]), (3),
			  ( /*_.CASETUPL__V80*/ meltfptr[52]), "SMAT_CASES");
    ;
    /*^touchobj */

    melt_dbgtrace_written_object ( /*_.INST__V83*/ meltfptr[55],
				  "newly made instance");
    ;
    /*_.SMAT__V82*/ meltfptr[54] = /*_.INST__V83*/ meltfptr[55];;
    MELT_LOCATION ("warmelt-macro.melt:5200:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    MELT_LOCATION ("warmelt-macro.melt:5201:/ cond");
    /*cond */ if (
		   /*ifisa */
		   melt_is_instance_of ((melt_ptr_t)
					( /*_.LASTCASE__V81*/ meltfptr[53]),
					(melt_ptr_t) (( /*!CLASS_SOURCE_MATCH_CASE */ meltfrout->tabval[13])))
      )				/*then */
      {
	/*^cond.then */
	/*^getslot */
	{
	  melt_ptr_t slot = NULL, obj = NULL;
	  obj = (melt_ptr_t) ( /*_.LASTCASE__V81*/ meltfptr[53]) /*=obj*/ ;
	  melt_object_get_field (slot, obj, 2, "SCAM_PATT");
   /*_.SCAM_PATT__V84*/ meltfptr[56] = slot;
	};
	;
      }
    else
      {				/*^cond.else */

  /*_.SCAM_PATT__V84*/ meltfptr[56] = NULL;;
      }
    ;
    /*^compute */
 /*_#IS_NOT_A__L25*/ meltfnum[4] =
      !melt_is_instance_of ((melt_ptr_t) ( /*_.SCAM_PATT__V84*/ meltfptr[56]),
			    (melt_ptr_t) (( /*!CLASS_SOURCE_PATTERN_JOKER_VARIABLE */ meltfrout->tabval[16])));;
    MELT_LOCATION ("warmelt-macro.melt:5200:/ cond");
    /*cond */ if ( /*_#IS_NOT_A__L25*/ meltfnum[4])	/*then */
      {
	/*^cond.then */
	/*^block */
	/*anyblock */
	{


	  {
	    MELT_LOCATION ("warmelt-macro.melt:5203:/ locexp");
	    melt_inform_str ((melt_ptr_t) ( /*_.LOC__V20*/ meltfptr[19]),
			     ("last (MATCH ...) clause is not a joker"),
			     (melt_ptr_t) 0);
	  }
	  ;
	  /*epilog */
	}
	;
      }				/*noelse */
    ;

#if MELT_HAVE_DEBUG
    MELT_LOCATION ("warmelt-macro.melt:5205:/ cppif.then");
    /*^block */
    /*anyblock */
    {


      {
	/*^locexp */
	/*melt_increment_dbgcounter */
#if MELT_HAVE_DEBUG
	melt_dbgcounter++;
#endif
	;
      }
      ;
      /*^checksignal */
      MELT_CHECK_SIGNAL ();
      ;
   /*_#MELT_NEED_DBG__L26*/ meltfnum[17] =
	/*MELT_NEED_DBG */
#if MELT_HAVE_DEBUG
	( /*melt_need_dbg */ melt_need_debug ((int) 0))
#else
	0			/* no melt_need_dbg */
#endif /*MELT_HAVE_DEBUG */
	;;
      MELT_LOCATION ("warmelt-macro.melt:5205:/ cond");
      /*cond */ if ( /*_#MELT_NEED_DBG__L26*/ meltfnum[17])	/*then */
	{
	  /*^cond.then */
	  /*^block */
	  /*anyblock */
	  {

     /*_#THE_MELTCALLCOUNT__L27*/ meltfnum[11] =
#ifdef meltcallcount
	      meltcallcount	/* the_meltcallcount */
#else
	      0L
#endif /* meltcallcount the_meltcallcount */
	      ;;
	    MELT_LOCATION ("warmelt-macro.melt:5205:/ checksignal");
	    MELT_CHECK_SIGNAL ();
	    ;
	    /*^apply */
	    /*apply */
	    {
	      union meltparam_un argtab[5];
	      memset (&argtab, 0, sizeof (argtab));
	      /*^apply.arg */
	      argtab[0].meltbp_long =
		/*_#THE_MELTCALLCOUNT__L27*/ meltfnum[11];
	      /*^apply.arg */
	      argtab[1].meltbp_cstring = "warmelt-macro.melt";
	      /*^apply.arg */
	      argtab[2].meltbp_long = 5205;
	      /*^apply.arg */
	      argtab[3].meltbp_cstring = "mexpand_match result smat";
	      /*^apply.arg */
	      argtab[4].meltbp_aptr =
		(melt_ptr_t *) & /*_.SMAT__V82*/ meltfptr[54];
	      /*_.MELT_DEBUG_FUN__V87*/ meltfptr[59] =
		melt_apply ((meltclosure_ptr_t)
			    (( /*!MELT_DEBUG_FUN */ meltfrout->tabval[3])),
			    (melt_ptr_t) (( /*nil */ NULL)),
			    (MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_LONG MELTBPARSTR_CSTRING
			     MELTBPARSTR_PTR ""), argtab, "",
			    (union meltparam_un *) 0);
	    }
	    ;
	    /*_.IF___V86*/ meltfptr[62] =
	      /*_.MELT_DEBUG_FUN__V87*/ meltfptr[59];;
	    /*epilog */

	    MELT_LOCATION ("warmelt-macro.melt:5205:/ clear");
	       /*clear *//*_#THE_MELTCALLCOUNT__L27*/ meltfnum[11] = 0;
	    /*^clear */
	       /*clear *//*_.MELT_DEBUG_FUN__V87*/ meltfptr[59] = 0;
	  }
	  ;
	}
      else
	{			/*^cond.else */

    /*_.IF___V86*/ meltfptr[62] = NULL;;
	}
      ;
      MELT_LOCATION ("warmelt-macro.melt:5205:/ quasiblock");


      /*_.PROGN___V88*/ meltfptr[60] = /*_.IF___V86*/ meltfptr[62];;
      /*^compute */
      /*_.IFCPP___V85*/ meltfptr[58] = /*_.PROGN___V88*/ meltfptr[60];;
      /*epilog */

      MELT_LOCATION ("warmelt-macro.melt:5205:/ clear");
	     /*clear *//*_#MELT_NEED_DBG__L26*/ meltfnum[17] = 0;
      /*^clear */
	     /*clear *//*_.IF___V86*/ meltfptr[62] = 0;
      /*^clear */
	     /*clear *//*_.PROGN___V88*/ meltfptr[60] = 0;
    }

#else /*MELT_HAVE_DEBUG */
    /*^cppif.else */
    /*_.IFCPP___V85*/ meltfptr[58] = ( /*nil */ NULL);
#endif /*MELT_HAVE_DEBUG */
    ;
    MELT_LOCATION ("warmelt-macro.melt:5206:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.SMAT__V82*/ meltfptr[54];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5206:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*_.LET___V79*/ meltfptr[51] = /*_.RETURN___V89*/ meltfptr[70];;

    MELT_LOCATION ("warmelt-macro.melt:5190:/ clear");
	   /*clear *//*_.CASETUPL__V80*/ meltfptr[52] = 0;
    /*^clear */
	   /*clear *//*_.LASTCASE__V81*/ meltfptr[53] = 0;
    /*^clear */
	   /*clear *//*_.SMAT__V82*/ meltfptr[54] = 0;
    /*^clear */
	   /*clear *//*_.SCAM_PATT__V84*/ meltfptr[56] = 0;
    /*^clear */
	   /*clear *//*_#IS_NOT_A__L25*/ meltfnum[4] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V85*/ meltfptr[58] = 0;
    /*^clear */
	   /*clear *//*_.RETURN___V89*/ meltfptr[70] = 0;
    /*_.LET___V18*/ meltfptr[14] = /*_.LET___V79*/ meltfptr[51];;

    MELT_LOCATION ("warmelt-macro.melt:5133:/ clear");
	   /*clear *//*_.CONT__V19*/ meltfptr[15] = 0;
    /*^clear */
	   /*clear *//*_.LOC__V20*/ meltfptr[19] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V21*/ meltfptr[20] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V22*/ meltfptr[21] = 0;
    /*^clear */
	   /*clear *//*_.MSEXP__V23*/ meltfptr[22] = 0;
    /*^clear */
	   /*clear *//*_.MATSX__V24*/ meltfptr[23] = 0;
    /*^clear */
	   /*clear *//*_.LIST_FIRST__V25*/ meltfptr[24] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V26*/ meltfptr[25] = 0;
    /*^clear */
	   /*clear *//*_.PAIR_TAIL__V27*/ meltfptr[26] = 0;
    /*^clear */
	   /*clear *//*_.LAMBDA___V28*/ meltfptr[27] = 0;
    /*^clear */
	   /*clear *//*_.MEXPTUPLE__V30*/ meltfptr[29] = 0;
    /*^clear */
	   /*clear *//*_#NBMATCH__L7*/ meltfnum[5] = 0;
    /*^clear */
	   /*clear *//*_.LET___V31*/ meltfptr[30] = 0;
    /*^clear */
	   /*clear *//*_.LASTMEXP__V37*/ meltfptr[33] = 0;
    /*^clear */
	   /*clear *//*_#IX__L10*/ meltfnum[8] = 0;
    /*^clear */
	   /*clear *//*_.CASELIST__V38*/ meltfptr[34] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V39*/ meltfptr[31] = 0;
    /*^clear */
	   /*clear *//*_.FOREVER___V43*/ meltfptr[32] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V75*/ meltfptr[71] = 0;
    /*^clear */
	   /*clear *//*_.LET___V79*/ meltfptr[51] = 0;
    MELT_LOCATION ("warmelt-macro.melt:5127:/ checksignal");
    MELT_CHECK_SIGNAL ();
    ;
    /*^quasiblock */


    /*_.RETVAL___V1*/ meltfptr[0] = /*_.LET___V18*/ meltfptr[14];;

    {
      MELT_LOCATION ("warmelt-macro.melt:5127:/ locexp");
      /*ochecknores compilobj_nrep_return */
#if MELT_HAVE_DEBUG
      if (meltxresdescr_ && meltxresdescr_[0] && meltxrestab_)
	melt_warn_for_no_expected_secondary_results ();
      /* we warned when secondary results are expected but not returned. */
#endif /*MELT_HAVE_DEBUG */
      ;
    }
    ;
    /*^finalreturn */
    ;
    /*finalret */ goto labend_rout;
    /*epilog */

    /*^clear */
	   /*clear *//*_.IFCPP___V6*/ meltfptr[5] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V8*/ meltfptr[6] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V10*/ meltfptr[8] = 0;
    /*^clear */
	   /*clear *//*_#NULL__L4*/ meltfnum[0] = 0;
    /*^clear */
	   /*clear *//*_.IF___V12*/ meltfptr[10] = 0;
    /*^clear */
	   /*clear *//*_.IFCPP___V14*/ meltfptr[12] = 0;
    /*^clear */
	   /*clear *//*_.LET___V18*/ meltfptr[14] = 0;
  }

  ;
  goto labend_rout;
labend_rout:
  melt_trace_end ("MEXPAND_MATCH", meltcallcount);
  MELT_TRACE_EXIT_LOCATION ();
  melt_blocklevel_signals =
    current_blocklevel_signals_meltrout_100_warmelt_macro_MEXPAND_MATCH_melt;
  melt_topframe = (struct melt_callframe_st *) meltfram__.mcfr_prev;
  return (melt_ptr_t) ( /*_.RETVAL___V1*/ meltfptr[0]);
#undef meltcallcount
#undef meltfram__
#undef MELTFRAM_NBVARNUM
#undef MELTFRAM_NBVARPTR
}				/*end meltrout_100_warmelt_macro_MEXPAND_MATCH */



/**** end of warmelt-macro+03.c ****/
